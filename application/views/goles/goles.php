<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
        <div class="card-block">
			<div class="table-responsive">
			<table  style="width:100%; font-size: 10px;" class="table table-bordered col-md-12 text-center" id="tabla">
				<thead style="background-color:#ccc;">
					<tr>
						<th class="align-middle" scope="col"><a href="<?php echo site_url('Jugadores/jugadores'); ?>">jugadores</a></th>

						<?php  
						// 	$alumnos = json_decode(json_encode($alumnos),true);
						// 	foreach ($alumnos as $alumno)
						// 	{
						// 	echo '<tr>';
						// 	echo '<th class="align-middle" scope="col">'.$alumno['codigo'].'</th>';
						// 	echo '<th class="align-middle" scope="col">'.$alumno['apellidos'].'</th>';
						// 	echo '<th class="align-middle" scope="col">'.$alumno['nombres'].'</th>';
						// 	echo '<th class="align-middle" scope="col">'.$alumno['dni'].'</th>';
						// 	echo '<th class="align-middle" scope="col">'.$alumno['direccion'].'</th>';
						// 	echo '</tr>';
						// }
						?>
					</tr>
				</thead>
				<tbody>
				</tbody>
				</table>
			</div>
		</div>
</body>
</html> -->


<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
	<title>Mundial Qatar - 2022</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.svg" />

    <!-- ========================= CSS ========================= -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/LineIcons.3.0.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/tiny-slider.css" />
    <link rel="stylesheet" href="assets/css/glightbox.min.css" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <script src="jquery-1.3.2.min.js" type="text/javascript"></script>   

</head>

<body>

    <!-- Empezador -->
    <div class="preloader">
        <div class="preloader-inner">
            <div class="preloader-icon">
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <!--Terminado -->

    <!-- Empezado -->
    <header class="header navbar-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="nav-inner">
                        <nav class="navbar navbar-expand-lg">
                            <a class="navbar-brand" href="index.html">
                                <!-- <img src="https://digitalhub.fifa.com/transform/412435a1-ebae-4d3c-be0b-b24b30b13c74/FWC-2" alt="Logo"> -->
                            </a>
                            <button class="navbar-toggler mobile-menu-btn" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                                <span class="toggler-icon"></span>
                                <span class="toggler-icon"></span>
                                <span class="toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
                                <ul id="nav" class="navbar-nav ms-auto">
                                    <li class="nav-item">
                                    <a href="http://proyecto_db.test/" class="active" aria-label="Toggle navigation">INICIO</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="dd-menu collapsed" href="javascript:void(0)" data-bs-toggle="collapse"
                                            data-bs-target="#submenu-1-1" aria-controls="navbarSupportedContent"
                                            aria-expanded="false" aria-label="Toggle navigation">TABLAS</a>
                                        <ul class="sub-menu collapse" id="submenu-1-1">
                                            <li class="nav-item"><a href="<?php echo site_url('Jugadores/jugadores'); ?>">Arbitros</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Estadios/estadios'); ?>">Estadios</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Fases/fases'); ?>">Fases</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Goles/goles'); ?>">Goles</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Grupos/grupos'); ?>">Grupos</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Jugadores2/jugadores2'); ?>">Jugadores</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Partidos/partidos'); ?>">Partidos</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Poscicionesjugador/poscicionesjugador'); ?>">Posciciones</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Puntaje/puntaje'); ?>">Puntaje</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Selecciones/selecciones'); ?>">Selecciones</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Tecnicos/tecnicos'); ?>">Tecnicos</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item">
                                        <a class="dd-menu collapsed" href="javascript:void(0)" data-bs-toggle="collapse"
                                            data-bs-target="#submenu-1-2" aria-controls="navbarSupportedContent"
                                            aria-expanded="false" aria-label="Toggle navigation">PARTIDOS</a>
                                        <ul class="sub-menu collapse" id="submenu-1-2">
                                            <li class="nav-item"><a href="shared-hosting.html">Partidos de Hoy</a></li>
                                            <li class="nav-item"><a href="dedicated-hosting.html">Alineaciones</a>
                                            </li>
                                            <li class="nav-item"><a href="vps-hosting.html">Convocados</a></li>
                                            <li class="nav-item"><a href="reseller-hosting.html">Mejores Goleadores del mundial</a>
                                        </ul>
                                    </li>
                                    <li class="nav-item">
                                        <a class="dd-menu collapsed" href="javascript:void(0)" data-bs-toggle="collapse"
                                            data-bs-target="#submenu-1-3" aria-controls="navbarSupportedContent"
                                            aria-expanded="false" aria-label="Toggle navigation">NOTICIAS</a>
                                        <ul class="sub-menu collapse" id="submenu-1-3">
                                            <li class="nav-item"><a href="blog-grid.html">Ptrocinadores del mundial</a>
                                            </li>
                                            <li class="nav-item"><a href="blog-single.html">Comentarios de los Jugadores</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item">
                                        <a href="contact.html" aria-label="Toggle navigation">SOBRE NOSOTROS</a>
                                    </li>
                                </ul>
                            </div> 
                            <div class="button">
                                <a href="signup.html" class="btn">INICIAR SESIÓN</a>
                            </div>
                        </nav>
                   
                    </div>
                </div>
            </div> 
        </div> 
    </header>
    <!-- Terminado -->

    <!-- Empezado -->
    <section class="hero-area" style="background-color:#000;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-8 offset-lg-2 col-md-12 col-12">
                    <div class="hero-content">
                        <h4 class="wow fadeInUp" data-wow-delay=".2s">50% de descuento por Black Friday en nuestros serivicios
                        </h4>
                        <h1 class="wow fadeInUp" data-wow-delay=".4s">
							BIENVENIDO A QATAR - 2022 
                        </h1>
                        <p class="wow fadeInUp" data-wow-delay=".6s">explora e informate los últimos acontecimientos en el mundial más esperado por los fans.</p>
                        <h5 class="wow fadeInUp" data-wow-delay=".8s">Empieza a vivirlo solo por S/. 15/Mes</h5>
                        <div class="button wow zoomIn" data-wow-delay="1s">
                            <a href="#pricing"><i class="lni lni-arrow-down-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Termiando -->


    
<!-- [ breadcrumb ] end -->
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <!-- Page-body start -->
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Inline editable card start -->
                              <div class="card">
                                  <div class="card-header border border-bottom bg-light">
                                      <div class="row w-100 m-0">
                                        <div class="col-md-11">
                                            <h5 class="align-middle"><i class="fas fa-edit"></i> Mundial Qatar - Goles</h5>
                                        </div>
                                        <div class="col-md-1">
                                          <div class="pull-right">
                                                  <a href="javascript:;" onclick="nuevo_registro()" class="btn btn-outline-secondary btn-sm font-10"><i class="fa fa-plus"></i> Nuevo Gol</a>                                    
                                              </div>
                                        </div>
                                      </div>
                                  </div>
                                  <br>
                                  <div class="card-block">
                                    <form action="#" class="font-12" id="frmgoles" name="frmgoles" style="width: 100%; display: inline-block;">
                                          <div class="row">

                                            <div class="col-md-2"> 
                                              <div class="form-group "> 
                                                <label class="control-label">Desde: <span class="symbol required"></span></label> 
                                                <input type="date" class="form-control-sm form-control" name="desde" id="desde" value="<?php echo date("Y-m").'-01';?>">
                                              </div> 
                                            </div>
                                            <div class="col-md-2"> 
                                              <div class="form-group "> 
                                                <label class="control-label">Hasta: <span class="symbol required"></span></label> 
                                                <input type="date" class="form-control-sm form-control" name="hasta" id="hasta" value="<?php echo date('Y-m-d')?>">
                                              </div> 
                                            </div>



                                            <div class="col-md-2"> 
                                              <div class="form-group "> 
                                                <label class="control-label">Estado: <span class="symbol required"></span></label> 
                                                <select name="estado" id="estado" class="form-control form-control-sm">
                                                  <option value="">SELECCIONE</option>
                                                  <option value="1">ACTIVO</option>
                                                  <option value="0">ANULADO</option>
                                                </select>
                                              </div> 
                                            </div>



                                            <div id="content_saldos" style="width:100%;display:none;">
                                            <div style="width:100%;display:flex;justify-content:flex-end;">
                                              <div style="margin-right:10px;" id="content_saldo_soles" class="alert alert-success" role="alert">
                                                
                                              </div>
                                              <div style="margin-right:10px;" id="content_saldo_dolares" class="alert alert-danger" role="alert">
                                                
                                              </div>
  
                                            </div>

                                          </div>
                                            
                                          </div>

                                            
                                            <div class="col-md-5 f-l" style="margin-top:30px;">
                                              
                                            <button id="btnguardararbitro" name="btnguardararbitro" onclick="buscarFases()" type="button" class="btn btn-info btn-sm btn_arbitro"><span class="fa fa-search"></span> Búscar  </button>
                                            <button type="button" id="limpiar" class="btn btn-danger btn-sm">Limpiar</button>
                                            </div>
                                          </form>
                                  </div>

                                  <div class="card-block" style="margin-top:30px;">
                                      <div class="table-responsive">
                                        <table style="width:100%; font-size: 10px;" class="table table-bordered col-md-12 text-center" id="tabla">
                                          <thead style="background-color:#9d0e0ec1;">
                                              <tr>
                                                  <th class="align-middle" scope="col">#</th>
                                                  <th class="align-middle" scope="col">id_partido</th>
                                                  <th class="align-middle" scope="col">id_jugador</th>
                                                  <th class="align-middle" scope="col">minuto</th>
                                                  <th class="align-middle" scope="col">Estado</th>
                                                  <th class="align-middle" scope="col">Fecha Creación</th>
                                                  <th class="align-middle" scope="col">Fecha Eliminación</th>
                                                  <th class="align-middle" scope="col">Acciones</th>
                                          </thead>
                                          <tbody>
                                            </tbody>
                                          </table>
                                        </div>
                                    </div>
                                </div>
                        <!-- Inline editable card end -->
                    </div>
                </div>
            </div>
            <!-- Page-body end -->
        </div>
    </div>
</div>

<script src="https://kit.fontawesome.com/fadf6c33df.js" crossorigin="anonymous"></script>

    <!-- <div class="card-block">
			<div class="table-responsive">
			<table  style="width:100%; font-size: 10px;" class="table table-bordered col-md-12 text-center" id="tabla">
				<thead style="background-color:#ccc;">
					<tr>
						<th class="align-middle" scope="col"><a href="<?php echo site_url('Jugadores/jugadores'); ?>">jugadores</a></th>

						<?php  
						// 	$arbitros = json_decode(json_encode($arbitros),true);
						// 	foreach ($arbitros as $arbitro)
						// 	{
						// 	echo '<tr>';
						// 	echo '<th class="align-middle" scope="col">'.$arbitro['id_arbitros'].'</th>';
						// 	echo '</tr>';
						// }
						?>
					</tr>
				</thead>
				<tbody>
				</tbody>
				</table>
			</div>
		</div> -->


    <!-- Iniciado -->
    <footer class="footer section">
        <div class="footer-top">
            <div class="container">
                <div class="inner-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="single-footer f-about">
                                <div class="logo">
                                    <a href="index.html">
                                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVFBcUFRUYGBcaGxodGxobHCEbGxsbGhsYGh0gICQbISwkHR0pIBsbJjYmKS4wMzMzGyI5PjkxPSwyMzIBCwsLEA4QHhISHj4pJCkyMjIyMjQyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMv/AABEIAKgBLAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAEAQIDBQYABwj/xABCEAACAQIEAwUEBwYGAgIDAAABAhEAAwQSITEFQVETImFxgQYykaFCUrHB0eHwFCNicoKSBxUzU6Lx0uJjwhY0sv/EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EACkRAAICAgICAQQCAgMAAAAAAAABAhESIQNBMVETBCJhcTKhgZEUscH/2gAMAwEAAhEDEQA/APMSkU2Ktb9kaA0NfwwG1RGdnVPjaBAKkUNypu1TWh0qmZpi27bE61OLHganw1ljttV7ZwYySfjXNycmLo6+PjyVlAmGU71P/lyESCas3srlmNY0HlSYDBFzA0qM35sv41dUVh4dFN7ONK1XZlCUNsMPHUz1B6VXXcCSdBrVR5L8ky468FWlqNTRlnChiAoNGpw0mQR8KJ4XgmBlgRFRPk0XCFMMwnCgFkgTQmLwzM0DRRz2FXTPAyLqSN+QFB/5Y894+OhrKL7Zc96RFhrIRWmNdAd4qbCQWkrI5fnRmHwaqVJOnQ7afbVlZS0xOqxHIUZCaKm9w8tqoGs6D86Hbh4+rBrRY60gQuWCxuxMQOpPKs0/HLKGFJeOYELp4mqybM/tiGpgmgSJHKrHheGK3Q5WBzjx0qms8fY6ra08GB++rzA+0FokC4rIepGny+6spS6KyibezaAGlOxWDt3UKXEV0O6sAR8+fjQWExRZQyMrL1Bkf9+FF9oxrTjaXRxTi7PMvab/AAz7xfBMfG0508kc/Y0+dYG9bu4e4bd1GtuN1YQY6jqPEaV9JIRVTxzhFrFLkuojryzDvA9VYaqfKtraVsywt60ePcF9oblqAIa3IJRvdMdI1U7bGNBpW84XxWzfAZS+dQsqcvaKqCe79B1nmZOp5xGd45/hvetS+FbtF+odHHkdn+R86ytt3ttlcFWU6ggqykfMEfGk0pbKUnHTPSr5QzIiYBObuN9JpkZlJk6KI21Owis4a0e6R4lCF0JiSvUAMdWP4VPwDFLcWHXtDEF/pnmQRp2gMAamY60b/l6kwMpWTpyB55dJRsxiCp20NYSTOiPIiMkMFg517veEBlMrlUlYjqQokePOO5g7bEKzchDqCJBy7iTqzTq3d0nrNl2ES06jdgO+BOuYbx3QoMrzihmwcNIyqW88j+4CV0ieQEMZ671nTRSmn2DNwjYsVB1y3AREw3OBsOS6Sd6JGCX/AE7gggyDGnvFwBOYKYQM2bXXbUVLbDKpUAkQAULe6O7pJ3VROkgE0ObRKyQXtwdCO8g0B215sSQJHWkK2/LOtYa3myMYeO6wbVjlgssEMT70CAoAJ2AonslbuPAfdSJg5cpEEAaKSoypqSOk0GQBpcgoZhp2MRrmkBve1kwBECnPeM5GLAnZwCvLTUEQR3oBgaTFNV2DT9hVnBrJVxLCTJAgwTrLZgDEZiZPeIjWpDLA2rk5oBzaS0gjMASe9ox7wAUQY5UiWswyOAG+gwg8swI2IgjaBOSZiqzjfGUQdmcpuz3CIKjUMGltttdy0HWK3g0lZk7bC72LVAUusAd1MjvKNVIjKYWIEAL3CxOtUV7juIUhbeXKBANx2RiORgFZkRqRqZ1Ig1T3eLhyTcJa5mBBABGmxXMd9Nz6FRUv/wCWuui21UeZk+LEMJY8yZPjTzQ8WYXE4VwJOlBOCN694497G2LiFkUhgugGxMk69N68q43wG5aMFY+fzrrjLpkv7toy0UbgrbMYUT4UxsM2bWils5WhSR1pyZUI7st8E6KgJGvMVMvEBDDL5DrXcNwPjNS4ezbRjm9K5aTb7OzJpLorHxhnUEb0dw7iCAqIO2tNxzoW7qiIiiMNdtJlIUk860lxtx0jCPKlJ2y1waMHzuwM8vCn3ipZgomenKhf82X6NoepmkPEmJkAL5VEeKV7RU/qIeEyyGKTIyyQ4EbaiPOq8Y25sH08hrTMViu0gxrGp01qJFraPGkto5p80pPTLIYm4BoVPWAKT9odjJMzQ1sUSi0YJdC+ST7C8NhWfTXancQsph7ZuXW20AG7NyUDqaZauMNiR5VmOP4xrt7ISSqBguv0pGc+ew9DWUo1s1jO1QFjOJXL7A3GhV91OSxPxPjRuBsc9+Wmuvnt+FV+Qgb/AI8/yo+3dIG+o0gSJ0G/h4c+e0HGTsYattS0IveDHM0lZEQVYjU8tRJBFTFVBGUE6sMoYuHUAgk9odAG0neRAmqtMTf94oCOitBgchO3PbrVlgLbvbVl7RlZm7pmVjRxAEFvdnLoRBMTpGLYPRZ8NxdzDsGQwDupMq3WdBOswRqK9DwGLF1A66TuOh6V5wcO8BgM6gmQCSRlAM6DXZtfGtL7K4nK2WZVtvs+2iLcZb8Ce0avKaeqVLFIFrrxMchVFVXHPZ3D4tYuoM3J17rr5HmPAyKtxS1aiQ2eZYr2VxuEJNhhdt8iNHUeK7HzE+lVWC4hdt3C2Zlee8Dz/mDaNvzr2SqrifArN85nSH+suh9evrUPiraHGXsocDxlHAD/ALthoNTl+qMrboY5GVJNWmQdBBgnSVPe0kbTpo0QdT0rP8X9lbqCbXfA5DRvhVTgONXcOwtuCVB9xpBHkdx5beFRS8SRT/BtIBgQdB3Ru6CCJUz3100g+JnaiCB7wgTHeHusBsGB5947jfWeVA4HGW7igry7xU6QZ3ge6f4l06wNKMzkbk+JiSQNDmGzrB94a9elJQFZXYq2cxHMjW22oaTJy+HvdQZkgChbmS2pO6AmV+kvWNdRsYEe9O2lE47EQpCoXbfKpkD+K22/Q5fPyqiTilsS7h2edCAB/S2ug5TvsfLmmknR0QtoNZrlxQM37jQ5z7wA5k/MnRt5G1SNw9ez7EAEMYF1xOp+rtL+Ok85nLRuC7yhu7JglFjIRvI+sw11POdvep967bRYYzbPdKH3l6QN2A6DVdCOZO0Y6tmcnukU1j2Ww4/d3FYv9Fix70RMZYEg8txIME6CG5wPBAxcY2n5qLigH+IdoZg/LbcGre9dYgAnKumV5BboJOwHLnvGm1MzRoyEnmQMwPjJM/GT406j6HcvZXcM9pmWQxzbaHaBp9lXWOexibTSVRogLpJO4g+hry+3cHLTwOo/H7ae+MYaTH2GuvD0ZZdsmu4a2jspXblzOo/GqtmUk92NdBUjgmSTJ8fx3FQXcZr3xMaQd/Q86a40tsp87ekHYbFi2NN/yoGZpUAbVdR0O9Koq4QjHaMuTllKk+hVWpEWuQVMi1ZByLUqLXKtTIlS2CORKnRK5EqdEqGykORKnRKREohEqGyxD3VLdAT8BNZHB2WZwdzlJJ8XMfOD6itrcs5kZeqsPiCKpOEoDatqgBa4EBbMAVYqgffYTOmsZhGu+HI9G3GU2IVRAJ3MDxOp9dJ+Bp2FTQAmfs9K6/dGXtEOZC5VWgHUkgaHckD1mm2MQoyzz8vx2/EVztOjVNF9gsODR911tslpTIRWUiAQzu+e4ZMiFOQAwdUB5UDhsQoGu33UBw3HBU7Md7K5WZPup7vXXKY8fUyuLyw5PBr8NZt2radq0G4GCREADWRvGhMjodANqm4aQtw5PdlQhn3gQDPwIHXasDiMS93FWUDEqkAg6BczuWIA5QwPWCRyrdcIgEJmzZXfIcsd1znAgaCFZRyA202q+UzSN2pkDypRSJsPKlrrXgwOBpRXCuqkAtOrq6qJOqv4hwm1eH7xATybZh61YUxmqZVWxo8z9qOH3cIwZCxt8nEgqehI2PjUXD/azNC3TpOrrv6gbea6+Br01kDAhgGB0IIkH051iOP/AOH6PNzDP2b75G9xvIjVPmKxx1rwVkWOGdHSbZBRuY1Jjy2PIkaidRTWwNrLBtr8ATHj9ceI19a83/aMTgbmV1a0x6iUcD/i48Rt4VqeFe1Fq5pdIR+pPdPltB8zPidqmo14Gm/YXiOEnMDZm2pH1zqZ3Q78tiRtyiguwvYdu2JV+RedfJp1B/WlX63M22x6iZ8wB89D1BqJ3UEEmTyJGYkdNPfH8uorJxXlGqk/DKTEe0BEhLYBIMqxlT1gQNKsrIVlBF1XBG5J08IHugdOVQ4vFI65Sq5eU6x/Idh5Gqh8FZJkkk9Zy/KKW/2Vr9GWVydI1/XKnvjFXS4N+cSPxqtvYovoqHMOYk/CBOtF2OGscrXHQ+DMQfsn5V6ZyEOZmY9nOXx2H4VJbwwH+oDPXcfL76J7FVOgH9Lfon4U9ddj8fxH4UCB/wBnjVSRHKfsjan2xGjfr7qe4Ych57fkagfFMvIEc5qhbLHC4VnmNfkY9aI/Y2GkVL7L+0S2c6lFKMBIaTseU7VucJisJiSSSEJIgDYispTaZaijBiwRyqUW63mO9mu7KQdzG3Sstj+H3Lf0Pj4VLn7GoX4AUSjLFtTuYqou8QyGCAD03rrXFtdFY+kCplIuPHsvxZHIg1Mlug8DiHaO4w9AaunAEcp35f8AVZfJ0aPirZFbSsNxVXw9x0Uaq3aW/wCWSwjlKywmNwNta9GsoDrVP7X8Kt3LQuFgjp7jHnP0NN55eNJyTEk0ZS9aS4jW7a5R2CFYOUSqsWPjBGbr9lZsYiFGbcSCmndIkEabQZ+PPatBhcUbdy1c0KoyhlP0gShjyOo9azftIiWsZetKe4H7p6qQCjeqlSTzMnnT443aHKVBS4wxlfvpzGoOvQiPn1qBbnYXGyMYABB9QwkA7wW2+VILFzLtpzMHYSfXagsOxvXFVQACZPIATVRitvoHLrs1/sth7t1rl5GVbzGUnQAlgq+Zll+Fb72PwUvccABc7gfyqzKPHYD4VmuE20tFXGUsqultfp9pkOZ9dAAGAnqhr0vgS2+yBQAb5gNYbdp9T8651986/wAhJ4osjSUppK6mzFCilFIKUU0xCinUyaRWmrUl4FRJTGWaU1C93L7x061M5JefAJN+BlxmX3RmH2fjTUxKkSWHiNoPSKmVswkbGg8ZhVOuobcMDseVc87SuL16f/hpGnpjsdg7d5Cly2roeTCfUdD471517Qf4flJfBvm/+Jzr/S+3o3xrbtibisM5GTqv386lTK4zTCyfDb7Kx+W9Iv468nkGG41iMOTZuZhG6OIZf5SRIHTcdK0FvjVu4oVM73GjuxLEzz3nz18xWo9r+FWrmEus6KTbtuyM3vAqpYZTvy251gPZO/btvcDgnuKVghYKv1IPWtLuNsS1KkX68Mv3N4tTEDR7hE5ToDAjxJjTajLXstZjvmWkyWdpMEidBHLlpVVd4sVzZWMnNJnXRpGp1qvbHsSTpv1rD5fSNvj9srrt84G52d/Dtbb6yXDlboQD7w9TXXOLWrvu3GBPJ+fxigsH7e4pVyXcl+3zW6gefXQ/GaHx3EcJfaVw62NNRbMqT1g6DyFer+0cmumH/sxOq6/y/h+FPtWW845GVb9etQ8HtM027LI8wYzC24I5gvI9OdWl3h2OCOWslmtjN9W5l6oUzJc8hB8KLXiwxZT4nFBQTMjms6j0/KosPhWuSxzKu4UqxH4AVDgMVZd899tdtULKfPs2HjV7duJcEW3VgPqk6D4mqdoSBraWzvHwgfr1qxw1zIQbZynz/R+FCJgXOp26nUfEbVDxB+y7pAkx3DMN4g7VD2NaNphfbC4hW0RJEeOk79flV/iOO2ntkMoLZTryn0rz3g+Gt2lDXGYO28MrCOW06VoLFy2w0dW8CYPz/Csmi0zLYzBOWZxHOSJ2+B+6p+CY+2CQTtzykj4jlVlx3iBtJlQlXfRQVDAg6EhoJBE9fSi+CcPtJbVGRCw3dNyeuon51Ela2aRm4vQdhbwcAqy+m/20fZtTpvVRxDC2FWczDzUyPGRP21H7GY452d7oZMxAQtGg5wZgHpUYUNzbNTZwsbCPlVV7Y4MthHYDVGR/gYP/ABYn0rVjH2shaRA3jWKTGYVL1m5bOgdGXyzqRPzow9MjN9ninvZlGhKBgehU/carvaeyty3ZxQSCALdxZ2ZNUMeKgrH8HKRU1+4UvJbYZWC3VOuqsuWR5gqabh7AdHVx7+VgDOUPLTEarBLa67CRrTi8NlyV6JLKn/Lr7nNoqMhzQFZ2RCpEd8MJOsRkH1qD9lsCxfNbQu7tkRYjM2pAk6DxO2hpb3BibbqTNyE30XSCxBMQIH0oETOsVcLZfDYYXFUQmbI3dZWuAZQNTIKZhc2IIU6jm7TVLt/0TW7J/ZpLd3GdmrsMqQhP0zobmcD/AHEFxoGxdd4r1T2WWLW531B5bGPEa/8AdeN+zKC1cS8SSUdCY1PdMtHUkV7RwIZUzOe+5zka6Tyg6gCaytfIqHNNRLY11AY3iAt5ZU95gs8gTtoJO+m3Om44XSkoRI1y7BgNxO+vpWkproyUX2H9oJidf18KcTI0/KgbFy3cQFSGU9Ov3Hz1qLDYplbs3IB0yRqWXXfxAGugo+SvPY8fQZZxInIx745dR1HhFTOJ5xQ1+1OoIVhs0SabZxM90ghhA1+kfDqKWfTDHtEqX9SGEQOfPyp73BHWhbtnPow15dR8KjGIFskOZPUbActBsazfI0qfj2VhfjyFKhAGXb6tP0I1+FVd3jiL7vz/AC0qkx/GnbwFZS5Yx/jv/o0jwyl50X93HW03afAbfHaqDG8YTMCgywZgGRPXXSqfE3idyaFbrXPKcpfg6I8UY7JuMY97lu4CSZRx13UjnWO4W37zzRvtU/dWqzgqwnUg/ZWQ4bei4h6hh8VJ+6ungX2STMuXUlRdXdDHn9lQFz1Pz/GmvdJ+X2RTRbboaSSQ22U+L4OgYgKRG4IyMD0Ikifh5UA/CmiRMeK16jhsbwm8AFxSxsFuOVYfytcAuDyDR4Ut/wBkm1bC3VdCNQxGvhKgo48wK9Vc671+ziwfR5G2HcGN/LX5b/Kj8D7QYmxpbvOo+qSY/tbT5Vssb7Juffw5U/WtgXF8yqksP6T/AE1Q4/2da33nRinWWKjznVD4MBVpwl4YqlEa/tNbuf8A7WEs3TzcL2dw/wBSfhTVbAt3ka7abkCFuL8SQ3rNQXrFjvRYdFkwUusSo5TnVgx9B6VXvg0Mm3cMDcXFC/8AJSQfWKWHrQZPvZscGzXLTvbxA7cEZEV1y3F551uSVbyMHw3qst4i/ZudpdsFo299FXyKyPnWYa0wE6EeBqXDcRuW/cuOvkxH30sJIeUTbWeLW8Q3uup10YhpjXQasTUi3LIkC6snkZtt8HIB8qyeF49dRxcBBcEEMwBYEbEHekvcRW4zPcSWYliQdyxkmNRvUUO0ajg2Fe5cN0EhUkJGYDoSBBHhuKtbuLvgwRI8R+IFYO1dtgyjPbPUb/FSKvRx8rbRbV582vaFzIJnuhcwaFjqd6lopFrxXHOtvUEFhCkNGp+IIojBOtq2ikttp7v/ANfxrOXOLXnKszW3KmRssnlOUiryxxsuhe5ZQlYEB2zMTMZRJJAjU8qhoYXxLGjswqsO+VG5TmPQ+taTDcUcKFzgnT6Un4nesPieNW2a3Nm4qo0mDJMAx9U79atBj8MQLxNxFzRqukgBiO428GocSrKT2lss+NW7BA7SGMR3nRfSSAT46mqvg99WtlYkqwdT1kEePNQfH0EXuIxtg28QQ4LFlZFIImCuvujxPLaqXg+CNu27iSunejQGNAes5W0qZfxplR8lxgbQZR3Q0lQBO5ZgFAnQzOk6TFTe1RlcHYuBVWyrBxmHvoLYJEA6ZXWNCQBrMUHwXHKjoxgqt3DuQdVKM4tsfSQem1a/iFqx+0t27ardvi3mWVBY2rup2BDZAJj3qiP2psJbkiv9geDL2ga4AwWWCyDkaYAb+LnHKR109A4kwQLcAHcOsn6J0PwmfSq7htqbb3AVzZsxYCMzBsx0M76epOpqwxN1YlriBCIMsBuPMVMLad9ik9i4lu0QrJAYbjfwIzUJw7GkqUuHvpo2YgkxHegbAyKqcPi7OQq98SjEBsxIYDUHSZ0jn4Ux+L2BcFy2GcRlfu5Q0aqe8RJ3GvWhvspR6Le3iVt3NDNtz4KqOdT4wY+PnRWIts/dHcI1RhEg/AkCs/e9pkIKqgAP1mkz4BdBHnQj+1F0nKGgbaDXz6/OocktFYN7NXaugL+8IVl0IJPxndid9OtB47jNse5q42YACsrjOIm5uxPjyoftJ0XU/L8h41GcmtItcau2aO9xi44MNlH6/XOqm9iW5mq8X2BhTPiNR86Rsx1J+NZSW/uZrGl4QWt4kx19TTb1wAx061EmKVFIDQx3YHvR0HQUIcXbGwJ/XjRrodk1y6SdyfKnMGI2jzoC7xI8gBQlzHufpfDSjbFaLa2ApJaD06DxrF8PYK9udgYPwIqzuXidzVKh7w/m/wDtXX9PF4tM5uZq0y/ONUbDp4bVF/mB6D50G2n68aZmqfjHmVNzgzCQVk/zZfkymfiKFFm9ZabfaoeqEqfihrXqlwDu5bqj6sOOZ23Gi9BQrPr7oHhr99ezSZwAuE9p+II627eMa5pP7yI2kgm+J0+fKatsL/iTjgcr27N0bGBlkc9VbL8qBZEacw+U06xg8PJ7Swt0HoSjiOalT9oI8Kzlwxflf0Wptdh49t8Jc/1uGgGYJtsJn0CmfWpsPxzhYbMpxOHYiCCpYEHcMO9mHgapcbwbCqQ1m4+/utmR0PmJVvMEeQoLE4W44h71xwNsz5v/AOqS4fTa/wAhm+y/xnD+H3Gz4fG20B+hcEQfAnKQPAg1XY3hDW4i5h7oOwR8x89tKz9/h2UAyT10mOmxpP8AL9CUOaNSMrKfnv6U8ZLv/aHlF9GqxHDmOEtBbILl2ZmUljljQRlEDbmdqr8Bwe5cupb7PvEjRpQQN5J2Ec6phduCMrOkDk7D76suHYLE3xce3my21zXLjuMijxZhuenhSakvIXFj+I4S1auNbt3BcVTGfLlkjeNTpPOosLhHuOEtqzMdgPz5eNDDF3PrD4CjuGX8S7OlkTKnPl7oyc85kAL5mk7S8FaGXLQXQEkj3jynoI3HjXIuk5pjcDQgCNdfOoziiPoqfKalw19mm2lrMWMwoLOcsnQxMb/Cpf6DXscl5uTH4mizduBVYsYaY1n3YBoBcQpMZI9fyom9cGVAZgA5QGB0LGeU71m0vRSf5JTfJENEHwEmKvFxwXCW8MgGVc7Pp7zuDlP9Ihf6RWfw2KVGDgNoecQfA+lWGEfKpBHvCIPTQ/dWM0ax2M7MKA4giNQOjGSOh70MPM8q9I9ocMobtEXtFdc/Qd9MjvPIDJZMeJ8a81/ZnFt3SQERGOpAIQuhEct10rdcE4nOECHN3UvBCYOZGRmCqdJZWCjLv6bZT/iFbshfiKhFXIIA0knmSSdI3JNVeL4mWgQsDQCDA+NRY3G2yE0YQgGka7676flVZcvKVJA0HVlBPpufSs4xLYU2ObYHygAfZSdsTqSfU0Eb4jZf7p6dPP5GlOKbkR5gfjVOP4BMs0J6U9HA95gPCaqWuMd2NNBpYSHki6/arY5k+QgUrcRABCr8TVWrDrJppuUvjb8srMsH4g55x5UwXmI1JNN/ZQi5rjZPqpu7eY+iPEmg2xJ5aDwpfEhZhV4R4eFQ9pQ7Xaja941a4kLMJL1GzVAblXvAOCG8e0uaWh13by8PGrUCXIrsNgbt33LbkfWCkj47VR3e67RydoPkxrZe0ntEMpw9ghbYEMy6A+A/h8edYi4fe863440Y8j0Gs9Nz1CXpM1GI2zVD2RxgOloyOj2//OpxwTH7NYNwdHNpuYO/aZuQ51CnH740F66B0Brj7QYj/fu/3Gk/q5v1/owuJIPZm8d8NdtnqrW3XkNi8/WO/MUNiPZq+hAKbidSAQBvInSnn2gv/wC/d/uNBXseXUozMytuCZBPjO9NfVS7BuPoI/yXFR3bZeNwBnimtwTEc8Ld9FYfcRQ2ExXZEm0TbJ3Kd2Y2mKKPHb3+9d/vNN/Vy6Qk49kFzgV3/aur/Nbb7QDQHEOH3bS5jYaACWJbKRAkmCNqs24xeOhu3T/WaHN9iSe8SRE7kjQx5bUL6uXaBuPQDiOHHs7dxSozxKlhoTJ3Mcq4YFUX95fADa5LYNwnziEnzajLkkd5CR4iR9lIE0gWz/b+VP8A5T9AmgLtLKe5YzH611if+CZR8SaHvYi4y5JIQmci91J6wNDVx2Z52z/b+VOWyf8AaP8Aafwo/wCS/Q7RmWtNvBrSXbQweHCgJcxN5ZZ0cOLFs7ppIFxtieQmKeLR52vl6Uq2iBpbA9AP1zpP6lvoaZm7TOCIEEajTpr5VdcfwVuybS2iGBs22uFWDDtDmzeXLSi8n/xr8v1zpypof3a/FR8al/UP0OwD2fW29xrNwALdUoHY5Rbub23J6SIP81TPZZchJG63NDIZTodt9M3wNSYhYRu4mx5qdxpoKnxOIythWtgns7dvMD9I953H8p7RhHSKmXJn1Rtx+GG8FxNthew76ZrT78xKmB1JAOn20Z7HYleyuWbhAMBkU7500YDkCQInz8apvZS7ZGJi8oIzSCw0ytvEjdScw/lFX/E+D9jiv3S57eYNAO9t1lmQj3gJbbUFZ2Omc19pSezMYhcpZQScrETETHrQDPrNa/E5QWUi2zCe8dZ1MGQNjVViGU/Qtf8AL8KFyP0S5FJJqVGJ0EUQ8dbfpm/CooWfeT4NT+R+hZDi4Gglj1ggenM1GXPM+lLnH1h/afxpmZfrf8fzoyfoeQVbUxJqNzJ0Pp/3UfbD6x/t/wDamG4vU/2/+1Df4DImYGuAP6/7qHtR9f8A4/nSFx9cfBqVseaJGQ+P69aYyge8Y8OdNz/xr/y/Cmn+ZPn94qk2JyQVgLtkOGu5io1ygb+eu1XPFfaVLidnbUouxM5SR00BgVmyPFPiPvppU/w/3LTtk5EhtdBP9U/dQN3dh5fYKKCNyA9CtB4iQxnQ6frStONuyZyTROF0Gg5czXZPAfP8aagaBCsdBsJ+6lyv9Vvh+VFsFJGgN7eEQxyyLJGn1hvqP7hTmuEHQJEBp7NBoSOqkCJG9CPcGUAnMBMkHcxmy9N8o0+t1ipWWcxB159SJ7voTAnXWB1rKkYjhfaJhQIOoRY5fw+PWKcbz6gbhfqgeH4n4daZccKwU6CZDbETtpOkgTufeNQYhgI0IzCTCxsGbXOein0mnQBLYi5zY6zyG7cvh9lNW85nvvz68h18p+NNUyQ0RM9GIOXQDpoAZ12PoOjhu6VMR0Jg5lkCNpJ0J5gDzKAJTE3Nf3j7a94+HQ+B/LWkbF3Ni77CJLTuOp20+ZqF9QufQkiATGb/AMRmJHp8Va2D3dCsDVRoDMw3MaTttJ6U6QHNcYmC7HoCT6bnmdPvpC5iZJ20G/j99DK8L3m3E6ypEggwJOkHXlJXoI4OOU90gQAe62uozR3oBJ5b08RWEvPMxznlv41GHMa7kmPHxpjPppvsQTBENzleYkmPvmms7BgTqZmJXu6wZ2MTpTxQ7JlYDx0n7uXnXZxMaA+fidNvA61Amu7Eg6DWJ+lHTloeg6Vy3Z7zAgZjrGhU5dI5biBoN55UYILJReEkHoT4xHz6U8XROWYMxE6yf186GcgATJgxO5OpPmPdn50ouL3SFjSdZBA8fnpRgikywwaNcdbaRmaV/uDCDHgTQ9vFZQpKmBA08NANdOW9P4Zj2w57cPbtkAqhYFmOcFcyhVMNAfVtBPlTsFjltqiXbgS26l/9PtGZXXKDGnIGJI3mKiUfRtCcUtskucPe4+W2jvcMtbKazCs8EfR90wfIbit57P32xWDVgoNyye6s6lZ1Qc10Aynl3NdKw93iNhStvtGAEiVXs2brmgxGsRMRUycYwphLd5lJAQZg0DUmO6PrHeoqWNUVnF7TR3tS3Z4hokIwVkPVCoHrDBh6VTXMQTrvWix2Jt4jBOAQb+Gl8wLMHQkdp/qd7lmPIFehNY53EH56+Jq4RtGc3vQR2k01rlCG511/IeHnTGvfZV4EZBjXKb2lB9pXdpRgGQZnppeh81OCsZMEgbkA6TtPSjAeRKXpO0qZeE4gjN2NyNNcpgSQBM+7JI36ijE9msRk7RkFtZgdowUmNSQDuB1oxQZFb2lIblWd7g1u3lFzFWZJIYW27TJBESRodDr0g7017WBUD99cdo1yp3SY5Ewd/DY70YoLK03KTOOtWR4jg1VcmFZ21zNcuGNzsEgdPnUR4/lXLbw9lP4suZjHKWOx5g6eFVi/QWCIjNooLHoBJ+VRYhGQwylTGxBB+dHXvafFsI7UpuP3arbJmJkoATsPhVVfvs5LOxYncsSSfU1cItMluyyw2Aa4uYXLSiCe84B0nl1MaDnRFvAW472LRT0CXGHnIWs9J5Uv7Q3U/GqwFZsbbW90YBN2IEhScsxmEDSI0OvkBUcq8z3JGckptOp2AGXWeXIeFSvZykZ++vv5QCe4xAb4rm1EwVMUlwZsqG6kMe8rAZgqieZ18uWo8+RMQhXtA3fHM6EQCGiQx2U5gTJ+mBuTTbzm2EgkgiCxZXTLmgEAnVgI7s/fCJf72Zlm2x0kglQFz7e7EwQI1gdKkYAkBSYcsyZp74cjNPRgVB0FPwIHCsB3AAGBgkbnvBY7uqsGzaxsp2WpLVi3pKkNlBBDEsHEkkqNCDMHpGkxNQXLYVAMwD5mUkAoe6AsAcozRl0kDTxa+IVViUBWZyTmEE7azm0OhjUzyqv0AQ12QAQFXckxoInWZGuXaYMxG8txDMBMBWYsJmZEgZgG1AjKdYEtOoiR8NZWA4DAtK5equWg6kAMC+w3Ck86KtYe2GcsS4+jlQCAywRocxWNARtEeRpADsSqks7lG8pBDfwx9Ajbw16pZd8isACo0BYgKAq97aTzGu5kge9pE95gykWCBlGXMdrfdg9+dyG105da5LhOVHcSZa5J0VnzAe8CARmBjXUTymrAlxL5STlZg0wIIUE5D3sugygcxrr1Ndh2YsoiBmbMcpIyr3RuTmMlj0g7aRQuJ4gqkS2do5SM3STJJ0JGYHURtFQ28aCuuYxqJJXXuyBrtoN/mNKai6EGXbep0gFfpNMSGGmTcwIOmmXaRoxdBqYCmBpJbVs/eOhIy5Z18oFApxAqJBfXQCYA0jlG6mCfHwqM3Hczbtw0kmBMEyT8N/QdKrBhZYq8C3DGCylhEEhhMAjQHvRyGszvU2AQXbotLPaEhRAlv4iQJ0/MzzFC7sTJJg7Dx10EbCSaN4Li2s3syauJVQd+8OuwMaetNw0NM9gs+yfCzbi9bZ2SFdmuMIKoDMq4yqAR0ncg70NxD2Y4MLbXEWWiFi/cyltFUSX0HeXyFYC57VXbedDbV9WzktuWOuwgryHhFV17jtxmgyqmTGbbNvqRJ0j4VCjJLRdwL/j2DwS3CoV5UKDLsYZQqnYCBCk7tMjbaqmzZwxICWyWnSGZtfCedVmI4kzEmBrvrNJw7iXZsWCBpiATAGsn0PunwJpLilXn+ysoLwv6PQsBwtLJe5ctd11Crlc5kziCrKSARJy6ydPE1g+JYRrVy5bgwD3SRplOoP3EjmDVnd9sbjKVa2m0e8SfPz50FxDjZuqPdBYFXkSY1jX6upMeFHHCUXsJyi1obhOC3bjKO6ivOViSVhRM7bbDzIop/Zm8BLPaPeCDLcWMzgFNTGhmqZsXc075AGwB0GhEjpoKGJjxEfL8ta1pszs0uC4dhhlN7EL3TFxEkuO8R3SRkePA6gyDOlT4HFcNtsTcS7dGYEbABTErEiW/i08qyjafrlXLbJBI2ETz3pYe2GVGqve0OGtz+y4QDVsr3SLhClg2UrEGNQDMgddTUQ9tMQqlba2bYy5e5bEwNhLSdDqOkDoKzLGkn0qlBDyLW7x/EsMjXrhXTSYAyiBoOcGgHvsd2JiYkkxO+/p8KhLeNIW+NNRQrJc1IXqPNSA08Qslz0maos1caKFY/PXF6jBpadBY8mm5qSkINOgNut2YtK6pbYyYBMHKAdfpa8vGkxODlv3mfM0KGAmFQkMxAgjSI611dXn9jI7llmgIirI7MZB3nLd4ajYwRqehFEpYZLkFGBRC3vhSWnvEESSRsR4V1dQw6Kp7oZVi3mfNCk5iSywQOkcpqTDsTklQozMGRe65aT011zQDMa0ldWtKhE/ftWywt9xCM5Oj9/Mq6jeGQgHqabeS4bpd84hUGY93W5OWcomIJ+FdXUKKH2MxGGxbEFbcLnGpWEJkgb7roPhXcd9nb1myt13BZiS6qsBAunvc4JGnjS11UnQPwM4TwoXEBdgikNrIzTGsTygg0i8Ow6NLXCw+iJ1nWeXQCurqL2yUDXHw5tyWbPoDOuYEz8R1pV4hbthzb0JIGgjTYn1gn1rq6roAC9jVLo4QDLvE94jY+elNTiBVy6qJ3M+X50tdV4oQK95ixJ3Jn4kekUwAwdzr+vvrq6mvAxWQ6jmKQrGvL9RXV1NdAd4T8OlKTt8uWvSurqGAtwa6ncb7Tp/0PSm5jG/w5/r8a6upLwBxbr1/CnpfiV3HmR9h18qWupuKEyNj0PKm11dTGdSTXV1ACUprq6gDuVJXV1ADmWOYPiPA/o+tIxrq6gB0RSV1dQB//9k=" alt="#">
                                    </a>
                                </div>
                                <p>La mejor experiencia, en la mejor epoca del año.
                                </p>
                                <a class="call" href="tel:8884014678"><i class="lni lni-phone-set"></i>01 - 497 - 1991</a>
                                <div class="payments">
                                    <img src="assets/images/footer/cards.png" alt="#">
                                </div>
                                <ul class="social">
                                    <li><a href="javascript:void(0)"><i class="lni lni-facebook-filled"></i></a></li>
                                    <li><a href="javascript:void(0)"><i class="lni lni-instagram"></i></a></li>
                                    <li><a href="javascript:void(0)"><i class="lni lni-twitter-original"></i></a></li>
                                    <li><a href="javascript:void(0)"><i class="lni lni-linkedin-original"></i></a></li>
                                    <li><a href="javascript:void(0)"><i class="lni lni-youtube"></i></a></li>
                                </ul>
                                <p class="copyright-text">© 2023 Mundial Qatar.<br>
                                    Diseñado y Desarrollado por : Dave Ryan, Yeffer Sime y Aaron Mendez</p>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-12">
                            <div class="single-footer f-link">
                                <h3>Páginas</h3>
                                <ul>
                                    <li><a href="javascript:void(0)">Sobre Nosotros</a></li>
                                    <li><a href="javascript:void(0)">Servicios</a></li>
                                    <li><a href="javascript:void(0)">Versiones <span style="margin-left: 4px;"
                                                class="badge bg-success rounded text-white">Comprame</span></a></li>
                                    <li><a href="javascript:void(0)">Contactanos</a></li>
                                </ul>
                                <h4 class="mt-40 mb-20 text-white" style="font-size: 18px;">Tablas</h4>
                                <ul>
                                    <li><a href="javascript:void(0)">Tablas Generales</a></li>
                                    <li><a href="javascript:void(0)">Posciciones</a></li>
                                    <li><a href="javascript:void(0)">Goles</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-12">
                            <div class="single-footer f-link">
                                <h3>Security</h3>
                                <ul>
                                    <li><a href="javascript:void(0)">Privacidad y Politicas</a></li>
                                    <li><a href="javascript:void(0)">Terminos y Condiciones</a></li>
                                    <li><a href="javascript:void(0)">Declaraciones</a></li>
                                    <li><a href="javascript:void(0)">FACTORES</a></li>
                                </ul>
                                <h4 class="mt-40 mb-20 text-white" style="font-size: 18px;">SOPORTE</h4>
                                <ul>
                                    <li><a href="javascript:void(0)">Centro de soporte</a></li>
                                    <li><a href="javascript:void(0)">Actualizaciones </a></li>
                                    <li><a href="javascript:void(0)">Base de conocimientos</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="single-footer latest-news">
                                <h3>Últimas Noticias</h3>
                                <div class="single-head">
                                    <div class="single-news">
                                        <span class="date"><a href="javascript:void(0)">NOVEMBER 25, 2022</a></span>
                                        <h4 class="post-title"><a href="blog-single.html">Hoy Argentina se viste de negro, Messi no podrá jugar en el mundial</a>
                                        </h4>
                                    </div>
                                    <div class="single-news">
                                        <span class="date"><a href="javascript:void(0)">NOVEMBER 26, 2022</a></span>
                                        <h4 class="post-title"><a href="blog-single.html">Cristiano Ronaldo declara: Me siento decepcionado de mi equipo.</a>
                                        </h4>
                                    </div>
                                    <div class="single-news">
                                        <span class="date"><a href="javascript:void(0)">NOVEMBER 18, 2022</a></span>
                                        <h4 class="post-title"><a href="blog-single.html">Los mejores Goles del Mundial ya ¡ DISPONIBLES ES NUESTRO SITIO WEB !</a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--tERMINADO -->
        <!-- Iniciado -->
        <div class="footer-bottom-area">
            <div class="container">
                <div class="inner-content">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6 col-12">
                            <p class="text">Nuestras Ofertas Son Validas Solo Hasta el Fin del Black Friday. <a href="javascript:void(0)">Click Aquí</a>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--Terminado -->

    <!-- ========================= scroll ========================= -->
    <a href="#" class="scroll-top">
        <i class="lni lni-arrow-up-circle"></i>
    </a>

    <!-- ========================= JS ========================= -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/tiny-slider.js"></script>
    <script src="assets/js/glightbox.min.js"></script>
    <script src="assets/js/count-up.min.js"></script>
    <script src="assets/js/main.js"></script>
    
</body>

</html>

<!-- <script rel="javascript" type="text/javascript" href="js/jquery-1.11.3.min.js"></script> -->



<script>

// function buscarArbitros()
// {
//     event.preventDefault();

//     var acceso 	= $("#frmarbitros #desde").val();
    
//     console.log(acceso);

// }
    
function buscarFases()
{
	// var acceso 	= $("#tipoacceso").val();
	// var hoy 	= $("#fechactual").val();

	jQuery.ajax({
	       url:"buscarGolesall",
	       method:"POST",
	       data:$("#frmgoles").serialize(),
	       dataType:'json',
	       success:function(response){
	       	
	       	var html = '';
	       	var i = 1;

			$('#tabla').show();
			$('#tabla').find('tbody').html("");
	       	if (response.TOTAL > 0)
	       	{
	       		$.each( response.LISTA, function( key, val )
	       		{
	                  html+='<tr>';
	                  html+='<td class="align-middle text-left">'+(i)+'</td>';
	                  html+='<td class="align-middle text-left">'+val.id_partido+'</td>';
	                  html+='<td class="align-middle text-left">'+val.id_jugador+'</td>';
	                  html+='<td class="align-middle text-left">'+val.minuto+'</td>';
	                  if(val.estado == '1')
							{
								estado_socio = "ACTIVA";
								html+='<td class="align-middle text-left" style="color:green;" ><span class="label label-success"><i class="fa fa-check align-middle text-center"></i>'+estado_socio+'</span></td>';
						  }else if(val.estado == '0'){
							  
							  estado_socio = "ANULADO";
							  html+='<td class="align-middle text-left" style="color:red;"><span class="label label-danger"><i class="fa fa-check align-middle text-center"></i>'+estado_socio+'</span></td>';
						  }
	                  html+='<td class="align-middle text-left">'+val.created_at+'</td>';
                      if(val.updated_at != null){
                          html+='<td class="align-middle text-left">'+val.updated_at+'</td>';
                          
                        }else{
                          html+='<td class="align-middle text-center">-</td>';

                      }
                      html+="<td class='align-middle text-center' scope='col' align='center'><a href='javascript:;' id='editararbitro"+val.id_goles+"' data-toggle='modal' onclick='editar_registro("+val.id_goles+")'  style='padding:4px 15px ;font-size:12px; background-color:green;border-color:green;'  data-id='"+val.id_goles+"' class='btn btn-sm btn-primary editar_form_contro_ip'>Editar</a><a href='javascript:;'  onclick='eliminar_fase("+val.id_goles+")' data-action='eliminarArbitro' data-id='"+val.id_goles+"' id='eliminacionarbitrario"+val.id_goles+"' style='padding:4px 15px ;font-size:12px;margin-left:5px;' class='btn btn-sm btn-primary eliminar_arbitro'>eliminar</a></td>";
                      html+='</tr>';	
					  i++;
					  
					});
				
				console.log(html);
				$('#tabla').show();
	       		//$('#tabla').dataTable().fnDestroy();
				$('#tabla').find('tbody').html(html);
				// $('.table-responsive').dataTable();
				//reinizializaTabla();
	       	}
	       }
	    });
}



function eliminar_fase(a)
{
	event.preventDefault();
	// var _this = $(this);
    var id = $('#eliminacionarbitrario'+a+'').attr("data-id")

	// var id  = _this.attr('data-id');
    console.log(id);

    $.ajax({
	       url:"eliminar_fase",
	       method:"POST",
	       data: {id:id},
	       dataType:'json',
           success:function(response)
            {
            }
        })
        
    location.reload();

}

function nuevo_registro(){
    var nombres_arbitro = prompt('Inserta el id_partidos: ');
    console.log(nombres_arbitro);
    var id_jugador = prompt('Inserta el id_jugador: ');
    console.log(id_jugador);
    var minuto = prompt('Inserta el minuto: ');
    console.log(minuto);
    alert("Se ah registrado exitosamente");

    $.ajax({
	       url:"registrarnewFases",
	       method:"POST",
	       data: {nombres_arbitro:nombres_arbitro,id_jugador:id_jugador,minuto,minuto},
	       dataType:'json',
           success:function(response)
            {
            }
        })
        location.reload();
}


function editar_registro(a){
    var id = $('#editararbitro'+a+'').attr("data-id")
    console.log(id);
    var nombres_arbitro = prompt('Inserta el id_partidos: ');
    console.log(nombres_arbitro);
    var id_jugador = prompt('Inserta el id_jugador: ');
    console.log(id_jugador);
    var minuto = prompt('Inserta el minuto: ');
    console.log(minuto);
    var estado = prompt('Inserta el estado:');
    console.log(estado);
    alert("Se ah registrado exitosamente");

    $.ajax({
	       url:"actualizandoFases",
	       method:"POST",
	       data: {id:id,nombres_arbitro:nombres_arbitro,id_jugador:id_jugador,minuto:minuto,
            estado:estado},
	       dataType:'json',
           success:function(response)
            {
            }
        })
        location.reload();
}
</script>


<script>
    /*!
 * jQuery JavaScript Library v3.6.1
 * https://jquery.com/
 *
 * Includes Sizzle.js
 * https://sizzlejs.com/
 *
 * Copyright OpenJS Foundation and other contributors
 * Released under the MIT license
 * https://jquery.org/license
 *
 * Date: 2022-08-26T17:52Z
 */
( function( global, factory ) {

"use strict";

if ( typeof module === "object" && typeof module.exports === "object" ) {

    // For CommonJS and CommonJS-like environments where a proper `window`
    // is present, execute the factory and get jQuery.
    // For environments that do not have a `window` with a `document`
    // (such as Node.js), expose a factory as module.exports.
    // This accentuates the need for the creation of a real `window`.
    // e.g. var jQuery = require("jquery")(window);
    // See ticket trac-14549 for more info.
    module.exports = global.document ?
        factory( global, true ) :
        function( w ) {
            if ( !w.document ) {
                throw new Error( "jQuery requires a window with a document" );
            }
            return factory( w );
        };
} else {
    factory( global );
}

// Pass this if window is not defined yet
} )( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Edge <= 12 - 13+, Firefox <=18 - 45+, IE 10 - 11, Safari 5.1 - 9+, iOS 6 - 9.1
// throw exceptions when non-strict code (e.g., ASP.NET 4.5) accesses strict mode
// arguments.callee.caller (trac-13335). But as of jQuery 3.0 (2016), strict mode should be common
// enough that all such attempts are guarded in a try block.
"use strict";

var arr = [];

var getProto = Object.getPrototypeOf;

var slice = arr.slice;

var flat = arr.flat ? function( array ) {
return arr.flat.call( array );
} : function( array ) {
return arr.concat.apply( [], array );
};


var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var fnToString = hasOwn.toString;

var ObjectFunctionString = fnToString.call( Object );

var support = {};

var isFunction = function isFunction( obj ) {

    // Support: Chrome <=57, Firefox <=52
    // In some browsers, typeof returns "function" for HTML <object> elements
    // (i.e., `typeof document.createElement( "object" ) === "function"`).
    // We don't want to classify *any* DOM node as a function.
    // Support: QtWeb <=3.8.5, WebKit <=534.34, wkhtmltopdf tool <=0.12.5
    // Plus for old WebKit, typeof returns "function" for HTML collections
    // (e.g., `typeof document.getElementsByTagName("div") === "function"`). (gh-4756)
    return typeof obj === "function" && typeof obj.nodeType !== "number" &&
        typeof obj.item !== "function";
};


var isWindow = function isWindow( obj ) {
    return obj != null && obj === obj.window;
};


var document = window.document;



var preservedScriptAttributes = {
    type: true,
    src: true,
    nonce: true,
    noModule: true
};

function DOMEval( code, node, doc ) {
    doc = doc || document;

    var i, val,
        script = doc.createElement( "script" );

    script.text = code;
    if ( node ) {
        for ( i in preservedScriptAttributes ) {

            // Support: Firefox 64+, Edge 18+
            // Some browsers don't support the "nonce" property on scripts.
            // On the other hand, just using `getAttribute` is not enough as
            // the `nonce` attribute is reset to an empty string whenever it
            // becomes browsing-context connected.
            // See https://github.com/whatwg/html/issues/2369
            // See https://html.spec.whatwg.org/#nonce-attributes
            // The `node.getAttribute` check was added for the sake of
            // `jQuery.globalEval` so that it can fake a nonce-containing node
            // via an object.
            val = node[ i ] || node.getAttribute && node.getAttribute( i );
            if ( val ) {
                script.setAttribute( i, val );
            }
        }
    }
    doc.head.appendChild( script ).parentNode.removeChild( script );
}


function toType( obj ) {
if ( obj == null ) {
    return obj + "";
}

// Support: Android <=2.3 only (functionish RegExp)
return typeof obj === "object" || typeof obj === "function" ?
    class2type[ toString.call( obj ) ] || "object" :
    typeof obj;
}
/* global Symbol */
// Defining this global in .eslintrc.json would create a danger of using the global
// unguarded in another place, it seems safer to define global only for this module



var
version = "3.6.1",

// Define a local copy of jQuery
jQuery = function( selector, context ) {

    // The jQuery object is actually just the init constructor 'enhanced'
    // Need init if jQuery is called (just allow error to be thrown if not included)
    return new jQuery.fn.init( selector, context );
};

jQuery.fn = jQuery.prototype = {

// The current version of jQuery being used
jquery: version,

constructor: jQuery,

// The default length of a jQuery object is 0
length: 0,

toArray: function() {
    return slice.call( this );
},

// Get the Nth element in the matched element set OR
// Get the whole matched element set as a clean array
get: function( num ) {

    // Return all the elements in a clean array
    if ( num == null ) {
        return slice.call( this );
    }

    // Return just the one element from the set
    return num < 0 ? this[ num + this.length ] : this[ num ];
},

// Take an array of elements and push it onto the stack
// (returning the new matched element set)
pushStack: function( elems ) {

    // Build a new jQuery matched element set
    var ret = jQuery.merge( this.constructor(), elems );

    // Add the old object onto the stack (as a reference)
    ret.prevObject = this;

    // Return the newly-formed element set
    return ret;
},

// Execute a callback for every element in the matched set.
each: function( callback ) {
    return jQuery.each( this, callback );
},

map: function( callback ) {
    return this.pushStack( jQuery.map( this, function( elem, i ) {
        return callback.call( elem, i, elem );
    } ) );
},

slice: function() {
    return this.pushStack( slice.apply( this, arguments ) );
},

first: function() {
    return this.eq( 0 );
},

last: function() {
    return this.eq( -1 );
},

even: function() {
    return this.pushStack( jQuery.grep( this, function( _elem, i ) {
        return ( i + 1 ) % 2;
    } ) );
},

odd: function() {
    return this.pushStack( jQuery.grep( this, function( _elem, i ) {
        return i % 2;
    } ) );
},

eq: function( i ) {
    var len = this.length,
        j = +i + ( i < 0 ? len : 0 );
    return this.pushStack( j >= 0 && j < len ? [ this[ j ] ] : [] );
},

end: function() {
    return this.prevObject || this.constructor();
},

// For internal use only.
// Behaves like an Array's method, not like a jQuery method.
push: push,
sort: arr.sort,
splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
var options, name, src, copy, copyIsArray, clone,
    target = arguments[ 0 ] || {},
    i = 1,
    length = arguments.length,
    deep = false;

// Handle a deep copy situation
if ( typeof target === "boolean" ) {
    deep = target;

    // Skip the boolean and the target
    target = arguments[ i ] || {};
    i++;
}

// Handle case when target is a string or something (possible in deep copy)
if ( typeof target !== "object" && !isFunction( target ) ) {
    target = {};
}

// Extend jQuery itself if only one argument is passed
if ( i === length ) {
    target = this;
    i--;
}

for ( ; i < length; i++ ) {

    // Only deal with non-null/undefined values
    if ( ( options = arguments[ i ] ) != null ) {

        // Extend the base object
        for ( name in options ) {
            copy = options[ name ];

            // Prevent Object.prototype pollution
            // Prevent never-ending loop
            if ( name === "__proto__" || target === copy ) {
                continue;
            }

            // Recurse if we're merging plain objects or arrays
            if ( deep && copy && ( jQuery.isPlainObject( copy ) ||
                ( copyIsArray = Array.isArray( copy ) ) ) ) {
                src = target[ name ];

                // Ensure proper type for the source value
                if ( copyIsArray && !Array.isArray( src ) ) {
                    clone = [];
                } else if ( !copyIsArray && !jQuery.isPlainObject( src ) ) {
                    clone = {};
                } else {
                    clone = src;
                }
                copyIsArray = false;

                // Never move original objects, clone them
                target[ name ] = jQuery.extend( deep, clone, copy );

            // Don't bring in undefined values
            } else if ( copy !== undefined ) {
                target[ name ] = copy;
            }
        }
    }
}

// Return the modified object
return target;
};

jQuery.extend( {

// Unique for each copy of jQuery on the page
expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

// Assume jQuery is ready without the ready module
isReady: true,

error: function( msg ) {
    throw new Error( msg );
},

noop: function() {},

isPlainObject: function( obj ) {
    var proto, Ctor;

    // Detect obvious negatives
    // Use toString instead of jQuery.type to catch host objects
    if ( !obj || toString.call( obj ) !== "[object Object]" ) {
        return false;
    }

    proto = getProto( obj );

    // Objects with no prototype (e.g., `Object.create( null )`) are plain
    if ( !proto ) {
        return true;
    }

    // Objects with prototype are plain iff they were constructed by a global Object function
    Ctor = hasOwn.call( proto, "constructor" ) && proto.constructor;
    return typeof Ctor === "function" && fnToString.call( Ctor ) === ObjectFunctionString;
},

isEmptyObject: function( obj ) {
    var name;

    for ( name in obj ) {
        return false;
    }
    return true;
},

// Evaluates a script in a provided context; falls back to the global one
// if not specified.
globalEval: function( code, options, doc ) {
    DOMEval( code, { nonce: options && options.nonce }, doc );
},

each: function( obj, callback ) {
    var length, i = 0;

    if ( isArrayLike( obj ) ) {
        length = obj.length;
        for ( ; i < length; i++ ) {
            if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
                break;
            }
        }
    } else {
        for ( i in obj ) {
            if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
                break;
            }
        }
    }

    return obj;
},

// results is for internal usage only
makeArray: function( arr, results ) {
    var ret = results || [];

    if ( arr != null ) {
        if ( isArrayLike( Object( arr ) ) ) {
            jQuery.merge( ret,
                typeof arr === "string" ?
                    [ arr ] : arr
            );
        } else {
            push.call( ret, arr );
        }
    }

    return ret;
},

inArray: function( elem, arr, i ) {
    return arr == null ? -1 : indexOf.call( arr, elem, i );
},

// Support: Android <=4.0 only, PhantomJS 1 only
// push.apply(_, arraylike) throws on ancient WebKit
merge: function( first, second ) {
    var len = +second.length,
        j = 0,
        i = first.length;

    for ( ; j < len; j++ ) {
        first[ i++ ] = second[ j ];
    }

    first.length = i;

    return first;
},

grep: function( elems, callback, invert ) {
    var callbackInverse,
        matches = [],
        i = 0,
        length = elems.length,
        callbackExpect = !invert;

    // Go through the array, only saving the items
    // that pass the validator function
    for ( ; i < length; i++ ) {
        callbackInverse = !callback( elems[ i ], i );
        if ( callbackInverse !== callbackExpect ) {
            matches.push( elems[ i ] );
        }
    }

    return matches;
},

// arg is for internal usage only
map: function( elems, callback, arg ) {
    var length, value,
        i = 0,
        ret = [];

    // Go through the array, translating each of the items to their new values
    if ( isArrayLike( elems ) ) {
        length = elems.length;
        for ( ; i < length; i++ ) {
            value = callback( elems[ i ], i, arg );

            if ( value != null ) {
                ret.push( value );
            }
        }

    // Go through every key on the object,
    } else {
        for ( i in elems ) {
            value = callback( elems[ i ], i, arg );

            if ( value != null ) {
                ret.push( value );
            }
        }
    }

    // Flatten any nested arrays
    return flat( ret );
},

// A global GUID counter for objects
guid: 1,

// jQuery.support is not used in Core but other projects attach their
// properties to it so it needs to exist.
support: support
} );

if ( typeof Symbol === "function" ) {
jQuery.fn[ Symbol.iterator ] = arr[ Symbol.iterator ];
}

// Populate the class2type map
jQuery.each( "Boolean Number String Function Array Date RegExp Object Error Symbol".split( " " ),
function( _i, name ) {
    class2type[ "[object " + name + "]" ] = name.toLowerCase();
} );

function isArrayLike( obj ) {

// Support: real iOS 8.2 only (not reproducible in simulator)
// `in` check used to prevent JIT error (gh-2145)
// hasOwn isn't used here due to false negatives
// regarding Nodelist length in IE
var length = !!obj && "length" in obj && obj.length,
    type = toType( obj );

if ( isFunction( obj ) || isWindow( obj ) ) {
    return false;
}

return type === "array" || length === 0 ||
    typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
* Sizzle CSS Selector Engine v2.3.6
* https://sizzlejs.com/
*
* Copyright JS Foundation and other contributors
* Released under the MIT license
* https://js.foundation/
*
* Date: 2021-02-16
*/
( function( window ) {
var i,
support,
Expr,
getText,
isXML,
tokenize,
compile,
select,
outermostContext,
sortInput,
hasDuplicate,

// Local document vars
setDocument,
document,
docElem,
documentIsHTML,
rbuggyQSA,
rbuggyMatches,
matches,
contains,

// Instance-specific data
expando = "sizzle" + 1 * new Date(),
preferredDoc = window.document,
dirruns = 0,
done = 0,
classCache = createCache(),
tokenCache = createCache(),
compilerCache = createCache(),
nonnativeSelectorCache = createCache(),
sortOrder = function( a, b ) {
    if ( a === b ) {
        hasDuplicate = true;
    }
    return 0;
},

// Instance methods
hasOwn = ( {} ).hasOwnProperty,
arr = [],
pop = arr.pop,
pushNative = arr.push,
push = arr.push,
slice = arr.slice,

// Use a stripped-down indexOf as it's faster than native
// https://jsperf.com/thor-indexof-vs-for/5
indexOf = function( list, elem ) {
    var i = 0,
        len = list.length;
    for ( ; i < len; i++ ) {
        if ( list[ i ] === elem ) {
            return i;
        }
    }
    return -1;
},

booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|" +
    "ismap|loop|multiple|open|readonly|required|scoped",

// Regular expressions

// http://www.w3.org/TR/css3-selectors/#whitespace
whitespace = "[\\x20\\t\\r\\n\\f]",

// https://www.w3.org/TR/css-syntax-3/#ident-token-diagram
identifier = "(?:\\\\[\\da-fA-F]{1,6}" + whitespace +
    "?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",

// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +

    // Operator (capture 2)
    "*([*^$|!~]?=)" + whitespace +

    // "Attribute values must be CSS identifiers [capture 5]
    // or strings [capture 3 or capture 4]"
    "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" +
    whitespace + "*\\]",

pseudos = ":(" + identifier + ")(?:\\((" +

    // To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
    // 1. quoted (capture 3; capture 4 or capture 5)
    "('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +

    // 2. simple (capture 6)
    "((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +

    // 3. anything else (capture 2)
    ".*" +
    ")\\)|)",

// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
rwhitespace = new RegExp( whitespace + "+", "g" ),
rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" +
    whitespace + "+$", "g" ),

rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace +
    "*" ),
rdescend = new RegExp( whitespace + "|>" ),

rpseudo = new RegExp( pseudos ),
ridentifier = new RegExp( "^" + identifier + "$" ),

matchExpr = {
    "ID": new RegExp( "^#(" + identifier + ")" ),
    "CLASS": new RegExp( "^\\.(" + identifier + ")" ),
    "TAG": new RegExp( "^(" + identifier + "|[*])" ),
    "ATTR": new RegExp( "^" + attributes ),
    "PSEUDO": new RegExp( "^" + pseudos ),
    "CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" +
        whitespace + "*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" +
        whitespace + "*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
    "bool": new RegExp( "^(?:" + booleans + ")$", "i" ),

    // For use in libraries implementing .is()
    // We use this for POS matching in `select`
    "needsContext": new RegExp( "^" + whitespace +
        "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + whitespace +
        "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
},

rhtml = /HTML$/i,
rinputs = /^(?:input|select|textarea|button)$/i,
rheader = /^h\d$/i,

rnative = /^[^{]+\{\s*\[native \w/,

// Easily-parseable/retrievable ID or TAG or CLASS selectors
rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

rsibling = /[+~]/,

// CSS escapes
// http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
runescape = new RegExp( "\\\\[\\da-fA-F]{1,6}" + whitespace + "?|\\\\([^\\r\\n\\f])", "g" ),
funescape = function( escape, nonHex ) {
    var high = "0x" + escape.slice( 1 ) - 0x10000;

    return nonHex ?

        // Strip the backslash prefix from a non-hex escape sequence
        nonHex :

        // Replace a hexadecimal escape sequence with the encoded Unicode code point
        // Support: IE <=11+
        // For values outside the Basic Multilingual Plane (BMP), manually construct a
        // surrogate pair
        high < 0 ?
            String.fromCharCode( high + 0x10000 ) :
            String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
},

// CSS string/identifier serialization
// https://drafts.csswg.org/cssom/#common-serializing-idioms
rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
fcssescape = function( ch, asCodePoint ) {
    if ( asCodePoint ) {

        // U+0000 NULL becomes U+FFFD REPLACEMENT CHARACTER
        if ( ch === "\0" ) {
            return "\uFFFD";
        }

        // Control characters and (dependent upon position) numbers get escaped as code points
        return ch.slice( 0, -1 ) + "\\" +
            ch.charCodeAt( ch.length - 1 ).toString( 16 ) + " ";
    }

    // Other potentially-special ASCII characters get backslash-escaped
    return "\\" + ch;
},

// Used for iframes
// See setDocument()
// Removing the function wrapper causes a "Permission Denied"
// error in IE
unloadHandler = function() {
    setDocument();
},

inDisabledFieldset = addCombinator(
    function( elem ) {
        return elem.disabled === true && elem.nodeName.toLowerCase() === "fieldset";
    },
    { dir: "parentNode", next: "legend" }
);

// Optimize for push.apply( _, NodeList )
try {
push.apply(
    ( arr = slice.call( preferredDoc.childNodes ) ),
    preferredDoc.childNodes
);

// Support: Android<4.0
// Detect silently failing push.apply
// eslint-disable-next-line no-unused-expressions
arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
push = { apply: arr.length ?

    // Leverage slice if possible
    function( target, els ) {
        pushNative.apply( target, slice.call( els ) );
    } :

    // Support: IE<9
    // Otherwise append directly
    function( target, els ) {
        var j = target.length,
            i = 0;

        // Can't trust NodeList.length
        while ( ( target[ j++ ] = els[ i++ ] ) ) {}
        target.length = j - 1;
    }
};
}

function Sizzle( selector, context, results, seed ) {
var m, i, elem, nid, match, groups, newSelector,
    newContext = context && context.ownerDocument,

    // nodeType defaults to 9, since context defaults to document
    nodeType = context ? context.nodeType : 9;

results = results || [];

// Return early from calls with invalid selector or context
if ( typeof selector !== "string" || !selector ||
    nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

    return results;
}

// Try to shortcut find operations (as opposed to filters) in HTML documents
if ( !seed ) {
    setDocument( context );
    context = context || document;

    if ( documentIsHTML ) {

        // If the selector is sufficiently simple, try using a "get*By*" DOM method
        // (excepting DocumentFragment context, where the methods don't exist)
        if ( nodeType !== 11 && ( match = rquickExpr.exec( selector ) ) ) {

            // ID selector
            if ( ( m = match[ 1 ] ) ) {

                // Document context
                if ( nodeType === 9 ) {
                    if ( ( elem = context.getElementById( m ) ) ) {

                        // Support: IE, Opera, Webkit
                        // TODO: identify versions
                        // getElementById can match elements by name instead of ID
                        if ( elem.id === m ) {
                            results.push( elem );
                            return results;
                        }
                    } else {
                        return results;
                    }

                // Element context
                } else {

                    // Support: IE, Opera, Webkit
                    // TODO: identify versions
                    // getElementById can match elements by name instead of ID
                    if ( newContext && ( elem = newContext.getElementById( m ) ) &&
                        contains( context, elem ) &&
                        elem.id === m ) {

                        results.push( elem );
                        return results;
                    }
                }

            // Type selector
            } else if ( match[ 2 ] ) {
                push.apply( results, context.getElementsByTagName( selector ) );
                return results;

            // Class selector
            } else if ( ( m = match[ 3 ] ) && support.getElementsByClassName &&
                context.getElementsByClassName ) {

                push.apply( results, context.getElementsByClassName( m ) );
                return results;
            }
        }

        // Take advantage of querySelectorAll
        if ( support.qsa &&
            !nonnativeSelectorCache[ selector + " " ] &&
            ( !rbuggyQSA || !rbuggyQSA.test( selector ) ) &&

            // Support: IE 8 only
            // Exclude object elements
            ( nodeType !== 1 || context.nodeName.toLowerCase() !== "object" ) ) {

            newSelector = selector;
            newContext = context;

            // qSA considers elements outside a scoping root when evaluating child or
            // descendant combinators, which is not what we want.
            // In such cases, we work around the behavior by prefixing every selector in the
            // list with an ID selector referencing the scope context.
            // The technique has to be used as well when a leading combinator is used
            // as such selectors are not recognized by querySelectorAll.
            // Thanks to Andrew Dupont for this technique.
            if ( nodeType === 1 &&
                ( rdescend.test( selector ) || rcombinators.test( selector ) ) ) {

                // Expand context for sibling selectors
                newContext = rsibling.test( selector ) && testContext( context.parentNode ) ||
                    context;

                // We can use :scope instead of the ID hack if the browser
                // supports it & if we're not changing the context.
                if ( newContext !== context || !support.scope ) {

                    // Capture the context ID, setting it first if necessary
                    if ( ( nid = context.getAttribute( "id" ) ) ) {
                        nid = nid.replace( rcssescape, fcssescape );
                    } else {
                        context.setAttribute( "id", ( nid = expando ) );
                    }
                }

                // Prefix every selector in the list
                groups = tokenize( selector );
                i = groups.length;
                while ( i-- ) {
                    groups[ i ] = ( nid ? "#" + nid : ":scope" ) + " " +
                        toSelector( groups[ i ] );
                }
                newSelector = groups.join( "," );
            }

            try {
                push.apply( results,
                    newContext.querySelectorAll( newSelector )
                );
                return results;
            } catch ( qsaError ) {
                nonnativeSelectorCache( selector, true );
            } finally {
                if ( nid === expando ) {
                    context.removeAttribute( "id" );
                }
            }
        }
    }
}

// All others
return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
* Create key-value caches of limited size
* @returns {function(string, object)} Returns the Object data after storing it on itself with
*	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
*	deleting the oldest entry
*/
function createCache() {
var keys = [];

function cache( key, value ) {

    // Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
    if ( keys.push( key + " " ) > Expr.cacheLength ) {

        // Only keep the most recent entries
        delete cache[ keys.shift() ];
    }
    return ( cache[ key + " " ] = value );
}
return cache;
}

/**
* Mark a function for special use by Sizzle
* @param {Function} fn The function to mark
*/
function markFunction( fn ) {
fn[ expando ] = true;
return fn;
}

/**
* Support testing using an element
* @param {Function} fn Passed the created element and returns a boolean result
*/
function assert( fn ) {
var el = document.createElement( "fieldset" );

try {
    return !!fn( el );
} catch ( e ) {
    return false;
} finally {

    // Remove from its parent by default
    if ( el.parentNode ) {
        el.parentNode.removeChild( el );
    }

    // release memory in IE
    el = null;
}
}

/**
* Adds the same handler for all of the specified attrs
* @param {String} attrs Pipe-separated list of attributes
* @param {Function} handler The method that will be applied
*/
function addHandle( attrs, handler ) {
var arr = attrs.split( "|" ),
    i = arr.length;

while ( i-- ) {
    Expr.attrHandle[ arr[ i ] ] = handler;
}
}

/**
* Checks document order of two siblings
* @param {Element} a
* @param {Element} b
* @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
*/
function siblingCheck( a, b ) {
var cur = b && a,
    diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
        a.sourceIndex - b.sourceIndex;

// Use IE sourceIndex if available on both nodes
if ( diff ) {
    return diff;
}

// Check if b follows a
if ( cur ) {
    while ( ( cur = cur.nextSibling ) ) {
        if ( cur === b ) {
            return -1;
        }
    }
}

return a ? 1 : -1;
}

/**
* Returns a function to use in pseudos for input types
* @param {String} type
*/
function createInputPseudo( type ) {
return function( elem ) {
    var name = elem.nodeName.toLowerCase();
    return name === "input" && elem.type === type;
};
}

/**
* Returns a function to use in pseudos for buttons
* @param {String} type
*/
function createButtonPseudo( type ) {
return function( elem ) {
    var name = elem.nodeName.toLowerCase();
    return ( name === "input" || name === "button" ) && elem.type === type;
};
}

/**
* Returns a function to use in pseudos for :enabled/:disabled
* @param {Boolean} disabled true for :disabled; false for :enabled
*/
function createDisabledPseudo( disabled ) {

// Known :disabled false positives: fieldset[disabled] > legend:nth-of-type(n+2) :can-disable
return function( elem ) {

    // Only certain elements can match :enabled or :disabled
    // https://html.spec.whatwg.org/multipage/scripting.html#selector-enabled
    // https://html.spec.whatwg.org/multipage/scripting.html#selector-disabled
    if ( "form" in elem ) {

        // Check for inherited disabledness on relevant non-disabled elements:
        // * listed form-associated elements in a disabled fieldset
        //   https://html.spec.whatwg.org/multipage/forms.html#category-listed
        //   https://html.spec.whatwg.org/multipage/forms.html#concept-fe-disabled
        // * option elements in a disabled optgroup
        //   https://html.spec.whatwg.org/multipage/forms.html#concept-option-disabled
        // All such elements have a "form" property.
        if ( elem.parentNode && elem.disabled === false ) {

            // Option elements defer to a parent optgroup if present
            if ( "label" in elem ) {
                if ( "label" in elem.parentNode ) {
                    return elem.parentNode.disabled === disabled;
                } else {
                    return elem.disabled === disabled;
                }
            }

            // Support: IE 6 - 11
            // Use the isDisabled shortcut property to check for disabled fieldset ancestors
            return elem.isDisabled === disabled ||

                // Where there is no isDisabled, check manually
                /* jshint -W018 */
                elem.isDisabled !== !disabled &&
                inDisabledFieldset( elem ) === disabled;
        }

        return elem.disabled === disabled;

    // Try to winnow out elements that can't be disabled before trusting the disabled property.
    // Some victims get caught in our net (label, legend, menu, track), but it shouldn't
    // even exist on them, let alone have a boolean value.
    } else if ( "label" in elem ) {
        return elem.disabled === disabled;
    }

    // Remaining elements are neither :enabled nor :disabled
    return false;
};
}

/**
* Returns a function to use in pseudos for positionals
* @param {Function} fn
*/
function createPositionalPseudo( fn ) {
return markFunction( function( argument ) {
    argument = +argument;
    return markFunction( function( seed, matches ) {
        var j,
            matchIndexes = fn( [], seed.length, argument ),
            i = matchIndexes.length;

        // Match elements found at the specified indexes
        while ( i-- ) {
            if ( seed[ ( j = matchIndexes[ i ] ) ] ) {
                seed[ j ] = !( matches[ j ] = seed[ j ] );
            }
        }
    } );
} );
}

/**
* Checks a node for validity as a Sizzle context
* @param {Element|Object=} context
* @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
*/
function testContext( context ) {
return context && typeof context.getElementsByTagName !== "undefined" && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
* Detects XML nodes
* @param {Element|Object} elem An element or a document
* @returns {Boolean} True iff elem is a non-HTML XML node
*/
isXML = Sizzle.isXML = function( elem ) {
var namespace = elem && elem.namespaceURI,
    docElem = elem && ( elem.ownerDocument || elem ).documentElement;

// Support: IE <=8
// Assume HTML when documentElement doesn't yet exist, such as inside loading iframes
// https://bugs.jquery.com/ticket/4833
return !rhtml.test( namespace || docElem && docElem.nodeName || "HTML" );
};

/**
* Sets document-related variables once based on the current document
* @param {Element|Object} [doc] An element or document object to use to set the document
* @returns {Object} Returns the current document
*/
setDocument = Sizzle.setDocument = function( node ) {
var hasCompare, subWindow,
    doc = node ? node.ownerDocument || node : preferredDoc;

// Return early if doc is invalid or already selected
// Support: IE 11+, Edge 17 - 18+
// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
// two documents; shallow comparisons work.
// eslint-disable-next-line eqeqeq
if ( doc == document || doc.nodeType !== 9 || !doc.documentElement ) {
    return document;
}

// Update global variables
document = doc;
docElem = document.documentElement;
documentIsHTML = !isXML( document );

// Support: IE 9 - 11+, Edge 12 - 18+
// Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
// Support: IE 11+, Edge 17 - 18+
// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
// two documents; shallow comparisons work.
// eslint-disable-next-line eqeqeq
if ( preferredDoc != document &&
    ( subWindow = document.defaultView ) && subWindow.top !== subWindow ) {

    // Support: IE 11, Edge
    if ( subWindow.addEventListener ) {
        subWindow.addEventListener( "unload", unloadHandler, false );

    // Support: IE 9 - 10 only
    } else if ( subWindow.attachEvent ) {
        subWindow.attachEvent( "onunload", unloadHandler );
    }
}

// Support: IE 8 - 11+, Edge 12 - 18+, Chrome <=16 - 25 only, Firefox <=3.6 - 31 only,
// Safari 4 - 5 only, Opera <=11.6 - 12.x only
// IE/Edge & older browsers don't support the :scope pseudo-class.
// Support: Safari 6.0 only
// Safari 6.0 supports :scope but it's an alias of :root there.
support.scope = assert( function( el ) {
    docElem.appendChild( el ).appendChild( document.createElement( "div" ) );
    return typeof el.querySelectorAll !== "undefined" &&
        !el.querySelectorAll( ":scope fieldset div" ).length;
} );

/* Attributes
---------------------------------------------------------------------- */

// Support: IE<8
// Verify that getAttribute really returns attributes and not properties
// (excepting IE8 booleans)
support.attributes = assert( function( el ) {
    el.className = "i";
    return !el.getAttribute( "className" );
} );

/* getElement(s)By*
---------------------------------------------------------------------- */

// Check if getElementsByTagName("*") returns only elements
support.getElementsByTagName = assert( function( el ) {
    el.appendChild( document.createComment( "" ) );
    return !el.getElementsByTagName( "*" ).length;
} );

// Support: IE<9
support.getElementsByClassName = rnative.test( document.getElementsByClassName );

// Support: IE<10
// Check if getElementById returns elements by name
// The broken getElementById methods don't pick up programmatically-set names,
// so use a roundabout getElementsByName test
support.getById = assert( function( el ) {
    docElem.appendChild( el ).id = expando;
    return !document.getElementsByName || !document.getElementsByName( expando ).length;
} );

// ID filter and find
if ( support.getById ) {
    Expr.filter[ "ID" ] = function( id ) {
        var attrId = id.replace( runescape, funescape );
        return function( elem ) {
            return elem.getAttribute( "id" ) === attrId;
        };
    };
    Expr.find[ "ID" ] = function( id, context ) {
        if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
            var elem = context.getElementById( id );
            return elem ? [ elem ] : [];
        }
    };
} else {
    Expr.filter[ "ID" ] =  function( id ) {
        var attrId = id.replace( runescape, funescape );
        return function( elem ) {
            var node = typeof elem.getAttributeNode !== "undefined" &&
                elem.getAttributeNode( "id" );
            return node && node.value === attrId;
        };
    };

    // Support: IE 6 - 7 only
    // getElementById is not reliable as a find shortcut
    Expr.find[ "ID" ] = function( id, context ) {
        if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
            var node, i, elems,
                elem = context.getElementById( id );

            if ( elem ) {

                // Verify the id attribute
                node = elem.getAttributeNode( "id" );
                if ( node && node.value === id ) {
                    return [ elem ];
                }

                // Fall back on getElementsByName
                elems = context.getElementsByName( id );
                i = 0;
                while ( ( elem = elems[ i++ ] ) ) {
                    node = elem.getAttributeNode( "id" );
                    if ( node && node.value === id ) {
                        return [ elem ];
                    }
                }
            }

            return [];
        }
    };
}

// Tag
Expr.find[ "TAG" ] = support.getElementsByTagName ?
    function( tag, context ) {
        if ( typeof context.getElementsByTagName !== "undefined" ) {
            return context.getElementsByTagName( tag );

        // DocumentFragment nodes don't have gEBTN
        } else if ( support.qsa ) {
            return context.querySelectorAll( tag );
        }
    } :

    function( tag, context ) {
        var elem,
            tmp = [],
            i = 0,

            // By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
            results = context.getElementsByTagName( tag );

        // Filter out possible comments
        if ( tag === "*" ) {
            while ( ( elem = results[ i++ ] ) ) {
                if ( elem.nodeType === 1 ) {
                    tmp.push( elem );
                }
            }

            return tmp;
        }
        return results;
    };

// Class
Expr.find[ "CLASS" ] = support.getElementsByClassName && function( className, context ) {
    if ( typeof context.getElementsByClassName !== "undefined" && documentIsHTML ) {
        return context.getElementsByClassName( className );
    }
};

/* QSA/matchesSelector
---------------------------------------------------------------------- */

// QSA and matchesSelector support

// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
rbuggyMatches = [];

// qSa(:focus) reports false when true (Chrome 21)
// We allow this because of a bug in IE8/9 that throws an error
// whenever `document.activeElement` is accessed on an iframe
// So, we allow :focus to pass through QSA all the time to avoid the IE error
// See https://bugs.jquery.com/ticket/13378
rbuggyQSA = [];

if ( ( support.qsa = rnative.test( document.querySelectorAll ) ) ) {

    // Build QSA regex
    // Regex strategy adopted from Diego Perini
    assert( function( el ) {

        var input;

        // Select is set to empty string on purpose
        // This is to test IE's treatment of not explicitly
        // setting a boolean content attribute,
        // since its presence should be enough
        // https://bugs.jquery.com/ticket/12359
        docElem.appendChild( el ).innerHTML = "<a id='" + expando + "'></a>" +
            "<select id='" + expando + "-\r\\' msallowcapture=''>" +
            "<option selected=''></option></select>";

        // Support: IE8, Opera 11-12.16
        // Nothing should be selected when empty strings follow ^= or $= or *=
        // The test attribute must be unknown in Opera but "safe" for WinRT
        // https://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
        if ( el.querySelectorAll( "[msallowcapture^='']" ).length ) {
            rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
        }

        // Support: IE8
        // Boolean attributes and "value" are not treated correctly
        if ( !el.querySelectorAll( "[selected]" ).length ) {
            rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
        }

        // Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+
        if ( !el.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
            rbuggyQSA.push( "~=" );
        }

        // Support: IE 11+, Edge 15 - 18+
        // IE 11/Edge don't find elements on a `[name='']` query in some cases.
        // Adding a temporary attribute to the document before the selection works
        // around the issue.
        // Interestingly, IE 10 & older don't seem to have the issue.
        input = document.createElement( "input" );
        input.setAttribute( "name", "" );
        el.appendChild( input );
        if ( !el.querySelectorAll( "[name='']" ).length ) {
            rbuggyQSA.push( "\\[" + whitespace + "*name" + whitespace + "*=" +
                whitespace + "*(?:''|\"\")" );
        }

        // Webkit/Opera - :checked should return selected option elements
        // http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
        // IE8 throws error here and will not see later tests
        if ( !el.querySelectorAll( ":checked" ).length ) {
            rbuggyQSA.push( ":checked" );
        }

        // Support: Safari 8+, iOS 8+
        // https://bugs.webkit.org/show_bug.cgi?id=136851
        // In-page `selector#id sibling-combinator selector` fails
        if ( !el.querySelectorAll( "a#" + expando + "+*" ).length ) {
            rbuggyQSA.push( ".#.+[+~]" );
        }

        // Support: Firefox <=3.6 - 5 only
        // Old Firefox doesn't throw on a badly-escaped identifier.
        el.querySelectorAll( "\\\f" );
        rbuggyQSA.push( "[\\r\\n\\f]" );
    } );

    assert( function( el ) {
        el.innerHTML = "<a href='' disabled='disabled'></a>" +
            "<select disabled='disabled'><option/></select>";

        // Support: Windows 8 Native Apps
        // The type and name attributes are restricted during .innerHTML assignment
        var input = document.createElement( "input" );
        input.setAttribute( "type", "hidden" );
        el.appendChild( input ).setAttribute( "name", "D" );

        // Support: IE8
        // Enforce case-sensitivity of name attribute
        if ( el.querySelectorAll( "[name=d]" ).length ) {
            rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
        }

        // FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
        // IE8 throws error here and will not see later tests
        if ( el.querySelectorAll( ":enabled" ).length !== 2 ) {
            rbuggyQSA.push( ":enabled", ":disabled" );
        }

        // Support: IE9-11+
        // IE's :disabled selector does not pick up the children of disabled fieldsets
        docElem.appendChild( el ).disabled = true;
        if ( el.querySelectorAll( ":disabled" ).length !== 2 ) {
            rbuggyQSA.push( ":enabled", ":disabled" );
        }

        // Support: Opera 10 - 11 only
        // Opera 10-11 does not throw on post-comma invalid pseudos
        el.querySelectorAll( "*,:x" );
        rbuggyQSA.push( ",.*:" );
    } );
}

if ( ( support.matchesSelector = rnative.test( ( matches = docElem.matches ||
    docElem.webkitMatchesSelector ||
    docElem.mozMatchesSelector ||
    docElem.oMatchesSelector ||
    docElem.msMatchesSelector ) ) ) ) {

    assert( function( el ) {

        // Check to see if it's possible to do matchesSelector
        // on a disconnected node (IE 9)
        support.disconnectedMatch = matches.call( el, "*" );

        // This should fail with an exception
        // Gecko does not error, returns false instead
        matches.call( el, "[s!='']:x" );
        rbuggyMatches.push( "!=", pseudos );
    } );
}

rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join( "|" ) );
rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join( "|" ) );

/* Contains
---------------------------------------------------------------------- */
hasCompare = rnative.test( docElem.compareDocumentPosition );

// Element contains another
// Purposefully self-exclusive
// As in, an element does not contain itself
contains = hasCompare || rnative.test( docElem.contains ) ?
    function( a, b ) {
        var adown = a.nodeType === 9 ? a.documentElement : a,
            bup = b && b.parentNode;
        return a === bup || !!( bup && bup.nodeType === 1 && (
            adown.contains ?
                adown.contains( bup ) :
                a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
        ) );
    } :
    function( a, b ) {
        if ( b ) {
            while ( ( b = b.parentNode ) ) {
                if ( b === a ) {
                    return true;
                }
            }
        }
        return false;
    };

/* Sorting
---------------------------------------------------------------------- */

// Document order sorting
sortOrder = hasCompare ?
function( a, b ) {

    // Flag for duplicate removal
    if ( a === b ) {
        hasDuplicate = true;
        return 0;
    }

    // Sort on method existence if only one input has compareDocumentPosition
    var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
    if ( compare ) {
        return compare;
    }

    // Calculate position if both inputs belong to the same document
    // Support: IE 11+, Edge 17 - 18+
    // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
    // two documents; shallow comparisons work.
    // eslint-disable-next-line eqeqeq
    compare = ( a.ownerDocument || a ) == ( b.ownerDocument || b ) ?
        a.compareDocumentPosition( b ) :

        // Otherwise we know they are disconnected
        1;

    // Disconnected nodes
    if ( compare & 1 ||
        ( !support.sortDetached && b.compareDocumentPosition( a ) === compare ) ) {

        // Choose the first element that is related to our preferred document
        // Support: IE 11+, Edge 17 - 18+
        // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
        // two documents; shallow comparisons work.
        // eslint-disable-next-line eqeqeq
        if ( a == document || a.ownerDocument == preferredDoc &&
            contains( preferredDoc, a ) ) {
            return -1;
        }

        // Support: IE 11+, Edge 17 - 18+
        // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
        // two documents; shallow comparisons work.
        // eslint-disable-next-line eqeqeq
        if ( b == document || b.ownerDocument == preferredDoc &&
            contains( preferredDoc, b ) ) {
            return 1;
        }

        // Maintain original order
        return sortInput ?
            ( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
            0;
    }

    return compare & 4 ? -1 : 1;
} :
function( a, b ) {

    // Exit early if the nodes are identical
    if ( a === b ) {
        hasDuplicate = true;
        return 0;
    }

    var cur,
        i = 0,
        aup = a.parentNode,
        bup = b.parentNode,
        ap = [ a ],
        bp = [ b ];

    // Parentless nodes are either documents or disconnected
    if ( !aup || !bup ) {

        // Support: IE 11+, Edge 17 - 18+
        // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
        // two documents; shallow comparisons work.
        /* eslint-disable eqeqeq */
        return a == document ? -1 :
            b == document ? 1 :
            /* eslint-enable eqeqeq */
            aup ? -1 :
            bup ? 1 :
            sortInput ?
            ( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
            0;

    // If the nodes are siblings, we can do a quick check
    } else if ( aup === bup ) {
        return siblingCheck( a, b );
    }

    // Otherwise we need full lists of their ancestors for comparison
    cur = a;
    while ( ( cur = cur.parentNode ) ) {
        ap.unshift( cur );
    }
    cur = b;
    while ( ( cur = cur.parentNode ) ) {
        bp.unshift( cur );
    }

    // Walk down the tree looking for a discrepancy
    while ( ap[ i ] === bp[ i ] ) {
        i++;
    }

    return i ?

        // Do a sibling check if the nodes have a common ancestor
        siblingCheck( ap[ i ], bp[ i ] ) :

        // Otherwise nodes in our document sort first
        // Support: IE 11+, Edge 17 - 18+
        // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
        // two documents; shallow comparisons work.
        /* eslint-disable eqeqeq */
        ap[ i ] == preferredDoc ? -1 :
        bp[ i ] == preferredDoc ? 1 :
        /* eslint-enable eqeqeq */
        0;
};

return document;
};

Sizzle.matches = function( expr, elements ) {
return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
setDocument( elem );

if ( support.matchesSelector && documentIsHTML &&
    !nonnativeSelectorCache[ expr + " " ] &&
    ( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
    ( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

    try {
        var ret = matches.call( elem, expr );

        // IE 9's matchesSelector returns false on disconnected nodes
        if ( ret || support.disconnectedMatch ||

            // As well, disconnected nodes are said to be in a document
            // fragment in IE 9
            elem.document && elem.document.nodeType !== 11 ) {
            return ret;
        }
    } catch ( e ) {
        nonnativeSelectorCache( expr, true );
    }
}

return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {

// Set document vars if needed
// Support: IE 11+, Edge 17 - 18+
// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
// two documents; shallow comparisons work.
// eslint-disable-next-line eqeqeq
if ( ( context.ownerDocument || context ) != document ) {
    setDocument( context );
}
return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {

// Set document vars if needed
// Support: IE 11+, Edge 17 - 18+
// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
// two documents; shallow comparisons work.
// eslint-disable-next-line eqeqeq
if ( ( elem.ownerDocument || elem ) != document ) {
    setDocument( elem );
}

var fn = Expr.attrHandle[ name.toLowerCase() ],

    // Don't get fooled by Object.prototype properties (jQuery #13807)
    val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
        fn( elem, name, !documentIsHTML ) :
        undefined;

return val !== undefined ?
    val :
    support.attributes || !documentIsHTML ?
        elem.getAttribute( name ) :
        ( val = elem.getAttributeNode( name ) ) && val.specified ?
            val.value :
            null;
};

Sizzle.escape = function( sel ) {
return ( sel + "" ).replace( rcssescape, fcssescape );
};

Sizzle.error = function( msg ) {
throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
* Document sorting and removing duplicates
* @param {ArrayLike} results
*/
Sizzle.uniqueSort = function( results ) {
var elem,
    duplicates = [],
    j = 0,
    i = 0;

// Unless we *know* we can detect duplicates, assume their presence
hasDuplicate = !support.detectDuplicates;
sortInput = !support.sortStable && results.slice( 0 );
results.sort( sortOrder );

if ( hasDuplicate ) {
    while ( ( elem = results[ i++ ] ) ) {
        if ( elem === results[ i ] ) {
            j = duplicates.push( i );
        }
    }
    while ( j-- ) {
        results.splice( duplicates[ j ], 1 );
    }
}

// Clear input after sorting to release objects
// See https://github.com/jquery/sizzle/pull/225
sortInput = null;

return results;
};

/**
* Utility function for retrieving the text value of an array of DOM nodes
* @param {Array|Element} elem
*/
getText = Sizzle.getText = function( elem ) {
var node,
    ret = "",
    i = 0,
    nodeType = elem.nodeType;

if ( !nodeType ) {

    // If no nodeType, this is expected to be an array
    while ( ( node = elem[ i++ ] ) ) {

        // Do not traverse comment nodes
        ret += getText( node );
    }
} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {

    // Use textContent for elements
    // innerText usage removed for consistency of new lines (jQuery #11153)
    if ( typeof elem.textContent === "string" ) {
        return elem.textContent;
    } else {

        // Traverse its children
        for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
            ret += getText( elem );
        }
    }
} else if ( nodeType === 3 || nodeType === 4 ) {
    return elem.nodeValue;
}

// Do not include comment or processing instruction nodes

return ret;
};

Expr = Sizzle.selectors = {

// Can be adjusted by the user
cacheLength: 50,

createPseudo: markFunction,

match: matchExpr,

attrHandle: {},

find: {},

relative: {
    ">": { dir: "parentNode", first: true },
    " ": { dir: "parentNode" },
    "+": { dir: "previousSibling", first: true },
    "~": { dir: "previousSibling" }
},

preFilter: {
    "ATTR": function( match ) {
        match[ 1 ] = match[ 1 ].replace( runescape, funescape );

        // Move the given value to match[3] whether quoted or unquoted
        match[ 3 ] = ( match[ 3 ] || match[ 4 ] ||
            match[ 5 ] || "" ).replace( runescape, funescape );

        if ( match[ 2 ] === "~=" ) {
            match[ 3 ] = " " + match[ 3 ] + " ";
        }

        return match.slice( 0, 4 );
    },

    "CHILD": function( match ) {

        /* matches from matchExpr["CHILD"]
            1 type (only|nth|...)
            2 what (child|of-type)
            3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
            4 xn-component of xn+y argument ([+-]?\d*n|)
            5 sign of xn-component
            6 x of xn-component
            7 sign of y-component
            8 y of y-component
        */
        match[ 1 ] = match[ 1 ].toLowerCase();

        if ( match[ 1 ].slice( 0, 3 ) === "nth" ) {

            // nth-* requires argument
            if ( !match[ 3 ] ) {
                Sizzle.error( match[ 0 ] );
            }

            // numeric x and y parameters for Expr.filter.CHILD
            // remember that false/true cast respectively to 0/1
            match[ 4 ] = +( match[ 4 ] ?
                match[ 5 ] + ( match[ 6 ] || 1 ) :
                2 * ( match[ 3 ] === "even" || match[ 3 ] === "odd" ) );
            match[ 5 ] = +( ( match[ 7 ] + match[ 8 ] ) || match[ 3 ] === "odd" );

            // other types prohibit arguments
        } else if ( match[ 3 ] ) {
            Sizzle.error( match[ 0 ] );
        }

        return match;
    },

    "PSEUDO": function( match ) {
        var excess,
            unquoted = !match[ 6 ] && match[ 2 ];

        if ( matchExpr[ "CHILD" ].test( match[ 0 ] ) ) {
            return null;
        }

        // Accept quoted arguments as-is
        if ( match[ 3 ] ) {
            match[ 2 ] = match[ 4 ] || match[ 5 ] || "";

        // Strip excess characters from unquoted arguments
        } else if ( unquoted && rpseudo.test( unquoted ) &&

            // Get excess from tokenize (recursively)
            ( excess = tokenize( unquoted, true ) ) &&

            // advance to the next closing parenthesis
            ( excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length ) ) {

            // excess is a negative index
            match[ 0 ] = match[ 0 ].slice( 0, excess );
            match[ 2 ] = unquoted.slice( 0, excess );
        }

        // Return only captures needed by the pseudo filter method (type and argument)
        return match.slice( 0, 3 );
    }
},

filter: {

    "TAG": function( nodeNameSelector ) {
        var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
        return nodeNameSelector === "*" ?
            function() {
                return true;
            } :
            function( elem ) {
                return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
            };
    },

    "CLASS": function( className ) {
        var pattern = classCache[ className + " " ];

        return pattern ||
            ( pattern = new RegExp( "(^|" + whitespace +
                ")" + className + "(" + whitespace + "|$)" ) ) && classCache(
                    className, function( elem ) {
                        return pattern.test(
                            typeof elem.className === "string" && elem.className ||
                            typeof elem.getAttribute !== "undefined" &&
                                elem.getAttribute( "class" ) ||
                            ""
                        );
            } );
    },

    "ATTR": function( name, operator, check ) {
        return function( elem ) {
            var result = Sizzle.attr( elem, name );

            if ( result == null ) {
                return operator === "!=";
            }
            if ( !operator ) {
                return true;
            }

            result += "";

            /* eslint-disable max-len */

            return operator === "=" ? result === check :
                operator === "!=" ? result !== check :
                operator === "^=" ? check && result.indexOf( check ) === 0 :
                operator === "*=" ? check && result.indexOf( check ) > -1 :
                operator === "$=" ? check && result.slice( -check.length ) === check :
                operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
                operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
                false;
            /* eslint-enable max-len */

        };
    },

    "CHILD": function( type, what, _argument, first, last ) {
        var simple = type.slice( 0, 3 ) !== "nth",
            forward = type.slice( -4 ) !== "last",
            ofType = what === "of-type";

        return first === 1 && last === 0 ?

            // Shortcut for :nth-*(n)
            function( elem ) {
                return !!elem.parentNode;
            } :

            function( elem, _context, xml ) {
                var cache, uniqueCache, outerCache, node, nodeIndex, start,
                    dir = simple !== forward ? "nextSibling" : "previousSibling",
                    parent = elem.parentNode,
                    name = ofType && elem.nodeName.toLowerCase(),
                    useCache = !xml && !ofType,
                    diff = false;

                if ( parent ) {

                    // :(first|last|only)-(child|of-type)
                    if ( simple ) {
                        while ( dir ) {
                            node = elem;
                            while ( ( node = node[ dir ] ) ) {
                                if ( ofType ?
                                    node.nodeName.toLowerCase() === name :
                                    node.nodeType === 1 ) {

                                    return false;
                                }
                            }

                            // Reverse direction for :only-* (if we haven't yet done so)
                            start = dir = type === "only" && !start && "nextSibling";
                        }
                        return true;
                    }

                    start = [ forward ? parent.firstChild : parent.lastChild ];

                    // non-xml :nth-child(...) stores cache data on `parent`
                    if ( forward && useCache ) {

                        // Seek `elem` from a previously-cached index

                        // ...in a gzip-friendly way
                        node = parent;
                        outerCache = node[ expando ] || ( node[ expando ] = {} );

                        // Support: IE <9 only
                        // Defend against cloned attroperties (jQuery gh-1709)
                        uniqueCache = outerCache[ node.uniqueID ] ||
                            ( outerCache[ node.uniqueID ] = {} );

                        cache = uniqueCache[ type ] || [];
                        nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
                        diff = nodeIndex && cache[ 2 ];
                        node = nodeIndex && parent.childNodes[ nodeIndex ];

                        while ( ( node = ++nodeIndex && node && node[ dir ] ||

                            // Fallback to seeking `elem` from the start
                            ( diff = nodeIndex = 0 ) || start.pop() ) ) {

                            // When found, cache indexes on `parent` and break
                            if ( node.nodeType === 1 && ++diff && node === elem ) {
                                uniqueCache[ type ] = [ dirruns, nodeIndex, diff ];
                                break;
                            }
                        }

                    } else {

                        // Use previously-cached element index if available
                        if ( useCache ) {

                            // ...in a gzip-friendly way
                            node = elem;
                            outerCache = node[ expando ] || ( node[ expando ] = {} );

                            // Support: IE <9 only
                            // Defend against cloned attroperties (jQuery gh-1709)
                            uniqueCache = outerCache[ node.uniqueID ] ||
                                ( outerCache[ node.uniqueID ] = {} );

                            cache = uniqueCache[ type ] || [];
                            nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
                            diff = nodeIndex;
                        }

                        // xml :nth-child(...)
                        // or :nth-last-child(...) or :nth(-last)?-of-type(...)
                        if ( diff === false ) {

                            // Use the same loop as above to seek `elem` from the start
                            while ( ( node = ++nodeIndex && node && node[ dir ] ||
                                ( diff = nodeIndex = 0 ) || start.pop() ) ) {

                                if ( ( ofType ?
                                    node.nodeName.toLowerCase() === name :
                                    node.nodeType === 1 ) &&
                                    ++diff ) {

                                    // Cache the index of each encountered element
                                    if ( useCache ) {
                                        outerCache = node[ expando ] ||
                                            ( node[ expando ] = {} );

                                        // Support: IE <9 only
                                        // Defend against cloned attroperties (jQuery gh-1709)
                                        uniqueCache = outerCache[ node.uniqueID ] ||
                                            ( outerCache[ node.uniqueID ] = {} );

                                        uniqueCache[ type ] = [ dirruns, diff ];
                                    }

                                    if ( node === elem ) {
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    // Incorporate the offset, then check against cycle size
                    diff -= last;
                    return diff === first || ( diff % first === 0 && diff / first >= 0 );
                }
            };
    },

    "PSEUDO": function( pseudo, argument ) {

        // pseudo-class names are case-insensitive
        // http://www.w3.org/TR/selectors/#pseudo-classes
        // Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
        // Remember that setFilters inherits from pseudos
        var args,
            fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
                Sizzle.error( "unsupported pseudo: " + pseudo );

        // The user may use createPseudo to indicate that
        // arguments are needed to create the filter function
        // just as Sizzle does
        if ( fn[ expando ] ) {
            return fn( argument );
        }

        // But maintain support for old signatures
        if ( fn.length > 1 ) {
            args = [ pseudo, pseudo, "", argument ];
            return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
                markFunction( function( seed, matches ) {
                    var idx,
                        matched = fn( seed, argument ),
                        i = matched.length;
                    while ( i-- ) {
                        idx = indexOf( seed, matched[ i ] );
                        seed[ idx ] = !( matches[ idx ] = matched[ i ] );
                    }
                } ) :
                function( elem ) {
                    return fn( elem, 0, args );
                };
        }

        return fn;
    }
},

pseudos: {

    // Potentially complex pseudos
    "not": markFunction( function( selector ) {

        // Trim the selector passed to compile
        // to avoid treating leading and trailing
        // spaces as combinators
        var input = [],
            results = [],
            matcher = compile( selector.replace( rtrim, "$1" ) );

        return matcher[ expando ] ?
            markFunction( function( seed, matches, _context, xml ) {
                var elem,
                    unmatched = matcher( seed, null, xml, [] ),
                    i = seed.length;

                // Match elements unmatched by `matcher`
                while ( i-- ) {
                    if ( ( elem = unmatched[ i ] ) ) {
                        seed[ i ] = !( matches[ i ] = elem );
                    }
                }
            } ) :
            function( elem, _context, xml ) {
                input[ 0 ] = elem;
                matcher( input, null, xml, results );

                // Don't keep the element (issue #299)
                input[ 0 ] = null;
                return !results.pop();
            };
    } ),

    "has": markFunction( function( selector ) {
        return function( elem ) {
            return Sizzle( selector, elem ).length > 0;
        };
    } ),

    "contains": markFunction( function( text ) {
        text = text.replace( runescape, funescape );
        return function( elem ) {
            return ( elem.textContent || getText( elem ) ).indexOf( text ) > -1;
        };
    } ),

    // "Whether an element is represented by a :lang() selector
    // is based solely on the element's language value
    // being equal to the identifier C,
    // or beginning with the identifier C immediately followed by "-".
    // The matching of C against the element's language value is performed case-insensitively.
    // The identifier C does not have to be a valid language name."
    // http://www.w3.org/TR/selectors/#lang-pseudo
    "lang": markFunction( function( lang ) {

        // lang value must be a valid identifier
        if ( !ridentifier.test( lang || "" ) ) {
            Sizzle.error( "unsupported lang: " + lang );
        }
        lang = lang.replace( runescape, funescape ).toLowerCase();
        return function( elem ) {
            var elemLang;
            do {
                if ( ( elemLang = documentIsHTML ?
                    elem.lang :
                    elem.getAttribute( "xml:lang" ) || elem.getAttribute( "lang" ) ) ) {

                    elemLang = elemLang.toLowerCase();
                    return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
                }
            } while ( ( elem = elem.parentNode ) && elem.nodeType === 1 );
            return false;
        };
    } ),

    // Miscellaneous
    "target": function( elem ) {
        var hash = window.location && window.location.hash;
        return hash && hash.slice( 1 ) === elem.id;
    },

    "root": function( elem ) {
        return elem === docElem;
    },

    "focus": function( elem ) {
        return elem === document.activeElement &&
            ( !document.hasFocus || document.hasFocus() ) &&
            !!( elem.type || elem.href || ~elem.tabIndex );
    },

    // Boolean properties
    "enabled": createDisabledPseudo( false ),
    "disabled": createDisabledPseudo( true ),

    "checked": function( elem ) {

        // In CSS3, :checked should return both checked and selected elements
        // http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
        var nodeName = elem.nodeName.toLowerCase();
        return ( nodeName === "input" && !!elem.checked ) ||
            ( nodeName === "option" && !!elem.selected );
    },

    "selected": function( elem ) {

        // Accessing this property makes selected-by-default
        // options in Safari work properly
        if ( elem.parentNode ) {
            // eslint-disable-next-line no-unused-expressions
            elem.parentNode.selectedIndex;
        }

        return elem.selected === true;
    },

    // Contents
    "empty": function( elem ) {

        // http://www.w3.org/TR/selectors/#empty-pseudo
        // :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
        //   but not by others (comment: 8; processing instruction: 7; etc.)
        // nodeType < 6 works because attributes (2) do not appear as children
        for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
            if ( elem.nodeType < 6 ) {
                return false;
            }
        }
        return true;
    },

    "parent": function( elem ) {
        return !Expr.pseudos[ "empty" ]( elem );
    },

    // Element/input types
    "header": function( elem ) {
        return rheader.test( elem.nodeName );
    },

    "input": function( elem ) {
        return rinputs.test( elem.nodeName );
    },

    "button": function( elem ) {
        var name = elem.nodeName.toLowerCase();
        return name === "input" && elem.type === "button" || name === "button";
    },

    "text": function( elem ) {
        var attr;
        return elem.nodeName.toLowerCase() === "input" &&
            elem.type === "text" &&

            // Support: IE<8
            // New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
            ( ( attr = elem.getAttribute( "type" ) ) == null ||
                attr.toLowerCase() === "text" );
    },

    // Position-in-collection
    "first": createPositionalPseudo( function() {
        return [ 0 ];
    } ),

    "last": createPositionalPseudo( function( _matchIndexes, length ) {
        return [ length - 1 ];
    } ),

    "eq": createPositionalPseudo( function( _matchIndexes, length, argument ) {
        return [ argument < 0 ? argument + length : argument ];
    } ),

    "even": createPositionalPseudo( function( matchIndexes, length ) {
        var i = 0;
        for ( ; i < length; i += 2 ) {
            matchIndexes.push( i );
        }
        return matchIndexes;
    } ),

    "odd": createPositionalPseudo( function( matchIndexes, length ) {
        var i = 1;
        for ( ; i < length; i += 2 ) {
            matchIndexes.push( i );
        }
        return matchIndexes;
    } ),

    "lt": createPositionalPseudo( function( matchIndexes, length, argument ) {
        var i = argument < 0 ?
            argument + length :
            argument > length ?
                length :
                argument;
        for ( ; --i >= 0; ) {
            matchIndexes.push( i );
        }
        return matchIndexes;
    } ),

    "gt": createPositionalPseudo( function( matchIndexes, length, argument ) {
        var i = argument < 0 ? argument + length : argument;
        for ( ; ++i < length; ) {
            matchIndexes.push( i );
        }
        return matchIndexes;
    } )
}
};

Expr.pseudos[ "nth" ] = Expr.pseudos[ "eq" ];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
var matched, match, tokens, type,
    soFar, groups, preFilters,
    cached = tokenCache[ selector + " " ];

if ( cached ) {
    return parseOnly ? 0 : cached.slice( 0 );
}

soFar = selector;
groups = [];
preFilters = Expr.preFilter;

while ( soFar ) {

    // Comma and first run
    if ( !matched || ( match = rcomma.exec( soFar ) ) ) {
        if ( match ) {

            // Don't consume trailing commas as valid
            soFar = soFar.slice( match[ 0 ].length ) || soFar;
        }
        groups.push( ( tokens = [] ) );
    }

    matched = false;

    // Combinators
    if ( ( match = rcombinators.exec( soFar ) ) ) {
        matched = match.shift();
        tokens.push( {
            value: matched,

            // Cast descendant combinators to space
            type: match[ 0 ].replace( rtrim, " " )
        } );
        soFar = soFar.slice( matched.length );
    }

    // Filters
    for ( type in Expr.filter ) {
        if ( ( match = matchExpr[ type ].exec( soFar ) ) && ( !preFilters[ type ] ||
            ( match = preFilters[ type ]( match ) ) ) ) {
            matched = match.shift();
            tokens.push( {
                value: matched,
                type: type,
                matches: match
            } );
            soFar = soFar.slice( matched.length );
        }
    }

    if ( !matched ) {
        break;
    }
}

// Return the length of the invalid excess
// if we're just parsing
// Otherwise, throw an error or return tokens
return parseOnly ?
    soFar.length :
    soFar ?
        Sizzle.error( selector ) :

        // Cache the tokens
        tokenCache( selector, groups ).slice( 0 );
};

function toSelector( tokens ) {
var i = 0,
    len = tokens.length,
    selector = "";
for ( ; i < len; i++ ) {
    selector += tokens[ i ].value;
}
return selector;
}

function addCombinator( matcher, combinator, base ) {
var dir = combinator.dir,
    skip = combinator.next,
    key = skip || dir,
    checkNonElements = base && key === "parentNode",
    doneName = done++;

return combinator.first ?

    // Check against closest ancestor/preceding element
    function( elem, context, xml ) {
        while ( ( elem = elem[ dir ] ) ) {
            if ( elem.nodeType === 1 || checkNonElements ) {
                return matcher( elem, context, xml );
            }
        }
        return false;
    } :

    // Check against all ancestor/preceding elements
    function( elem, context, xml ) {
        var oldCache, uniqueCache, outerCache,
            newCache = [ dirruns, doneName ];

        // We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching
        if ( xml ) {
            while ( ( elem = elem[ dir ] ) ) {
                if ( elem.nodeType === 1 || checkNonElements ) {
                    if ( matcher( elem, context, xml ) ) {
                        return true;
                    }
                }
            }
        } else {
            while ( ( elem = elem[ dir ] ) ) {
                if ( elem.nodeType === 1 || checkNonElements ) {
                    outerCache = elem[ expando ] || ( elem[ expando ] = {} );

                    // Support: IE <9 only
                    // Defend against cloned attroperties (jQuery gh-1709)
                    uniqueCache = outerCache[ elem.uniqueID ] ||
                        ( outerCache[ elem.uniqueID ] = {} );

                    if ( skip && skip === elem.nodeName.toLowerCase() ) {
                        elem = elem[ dir ] || elem;
                    } else if ( ( oldCache = uniqueCache[ key ] ) &&
                        oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

                        // Assign to newCache so results back-propagate to previous elements
                        return ( newCache[ 2 ] = oldCache[ 2 ] );
                    } else {

                        // Reuse newcache so results back-propagate to previous elements
                        uniqueCache[ key ] = newCache;

                        // A match means we're done; a fail means we have to keep checking
                        if ( ( newCache[ 2 ] = matcher( elem, context, xml ) ) ) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    };
}

function elementMatcher( matchers ) {
return matchers.length > 1 ?
    function( elem, context, xml ) {
        var i = matchers.length;
        while ( i-- ) {
            if ( !matchers[ i ]( elem, context, xml ) ) {
                return false;
            }
        }
        return true;
    } :
    matchers[ 0 ];
}

function multipleContexts( selector, contexts, results ) {
var i = 0,
    len = contexts.length;
for ( ; i < len; i++ ) {
    Sizzle( selector, contexts[ i ], results );
}
return results;
}

function condense( unmatched, map, filter, context, xml ) {
var elem,
    newUnmatched = [],
    i = 0,
    len = unmatched.length,
    mapped = map != null;

for ( ; i < len; i++ ) {
    if ( ( elem = unmatched[ i ] ) ) {
        if ( !filter || filter( elem, context, xml ) ) {
            newUnmatched.push( elem );
            if ( mapped ) {
                map.push( i );
            }
        }
    }
}

return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
if ( postFilter && !postFilter[ expando ] ) {
    postFilter = setMatcher( postFilter );
}
if ( postFinder && !postFinder[ expando ] ) {
    postFinder = setMatcher( postFinder, postSelector );
}
return markFunction( function( seed, results, context, xml ) {
    var temp, i, elem,
        preMap = [],
        postMap = [],
        preexisting = results.length,

        // Get initial elements from seed or context
        elems = seed || multipleContexts(
            selector || "*",
            context.nodeType ? [ context ] : context,
            []
        ),

        // Prefilter to get matcher input, preserving a map for seed-results synchronization
        matcherIn = preFilter && ( seed || !selector ) ?
            condense( elems, preMap, preFilter, context, xml ) :
            elems,

        matcherOut = matcher ?

            // If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
            postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

                // ...intermediate processing is necessary
                [] :

                // ...otherwise use results directly
                results :
            matcherIn;

    // Find primary matches
    if ( matcher ) {
        matcher( matcherIn, matcherOut, context, xml );
    }

    // Apply postFilter
    if ( postFilter ) {
        temp = condense( matcherOut, postMap );
        postFilter( temp, [], context, xml );

        // Un-match failing elements by moving them back to matcherIn
        i = temp.length;
        while ( i-- ) {
            if ( ( elem = temp[ i ] ) ) {
                matcherOut[ postMap[ i ] ] = !( matcherIn[ postMap[ i ] ] = elem );
            }
        }
    }

    if ( seed ) {
        if ( postFinder || preFilter ) {
            if ( postFinder ) {

                // Get the final matcherOut by condensing this intermediate into postFinder contexts
                temp = [];
                i = matcherOut.length;
                while ( i-- ) {
                    if ( ( elem = matcherOut[ i ] ) ) {

                        // Restore matcherIn since elem is not yet a final match
                        temp.push( ( matcherIn[ i ] = elem ) );
                    }
                }
                postFinder( null, ( matcherOut = [] ), temp, xml );
            }

            // Move matched elements from seed to results to keep them synchronized
            i = matcherOut.length;
            while ( i-- ) {
                if ( ( elem = matcherOut[ i ] ) &&
                    ( temp = postFinder ? indexOf( seed, elem ) : preMap[ i ] ) > -1 ) {

                    seed[ temp ] = !( results[ temp ] = elem );
                }
            }
        }

    // Add elements to results, through postFinder if defined
    } else {
        matcherOut = condense(
            matcherOut === results ?
                matcherOut.splice( preexisting, matcherOut.length ) :
                matcherOut
        );
        if ( postFinder ) {
            postFinder( null, results, matcherOut, xml );
        } else {
            push.apply( results, matcherOut );
        }
    }
} );
}

function matcherFromTokens( tokens ) {
var checkContext, matcher, j,
    len = tokens.length,
    leadingRelative = Expr.relative[ tokens[ 0 ].type ],
    implicitRelative = leadingRelative || Expr.relative[ " " ],
    i = leadingRelative ? 1 : 0,

    // The foundational matcher ensures that elements are reachable from top-level context(s)
    matchContext = addCombinator( function( elem ) {
        return elem === checkContext;
    }, implicitRelative, true ),
    matchAnyContext = addCombinator( function( elem ) {
        return indexOf( checkContext, elem ) > -1;
    }, implicitRelative, true ),
    matchers = [ function( elem, context, xml ) {
        var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
            ( checkContext = context ).nodeType ?
                matchContext( elem, context, xml ) :
                matchAnyContext( elem, context, xml ) );

        // Avoid hanging onto element (issue #299)
        checkContext = null;
        return ret;
    } ];

for ( ; i < len; i++ ) {
    if ( ( matcher = Expr.relative[ tokens[ i ].type ] ) ) {
        matchers = [ addCombinator( elementMatcher( matchers ), matcher ) ];
    } else {
        matcher = Expr.filter[ tokens[ i ].type ].apply( null, tokens[ i ].matches );

        // Return special upon seeing a positional matcher
        if ( matcher[ expando ] ) {

            // Find the next relative operator (if any) for proper handling
            j = ++i;
            for ( ; j < len; j++ ) {
                if ( Expr.relative[ tokens[ j ].type ] ) {
                    break;
                }
            }
            return setMatcher(
                i > 1 && elementMatcher( matchers ),
                i > 1 && toSelector(

                // If the preceding token was a descendant combinator, insert an implicit any-element `*`
                tokens
                    .slice( 0, i - 1 )
                    .concat( { value: tokens[ i - 2 ].type === " " ? "*" : "" } )
                ).replace( rtrim, "$1" ),
                matcher,
                i < j && matcherFromTokens( tokens.slice( i, j ) ),
                j < len && matcherFromTokens( ( tokens = tokens.slice( j ) ) ),
                j < len && toSelector( tokens )
            );
        }
        matchers.push( matcher );
    }
}

return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
var bySet = setMatchers.length > 0,
    byElement = elementMatchers.length > 0,
    superMatcher = function( seed, context, xml, results, outermost ) {
        var elem, j, matcher,
            matchedCount = 0,
            i = "0",
            unmatched = seed && [],
            setMatched = [],
            contextBackup = outermostContext,

            // We must always have either seed elements or outermost context
            elems = seed || byElement && Expr.find[ "TAG" ]( "*", outermost ),

            // Use integer dirruns iff this is the outermost matcher
            dirrunsUnique = ( dirruns += contextBackup == null ? 1 : Math.random() || 0.1 ),
            len = elems.length;

        if ( outermost ) {

            // Support: IE 11+, Edge 17 - 18+
            // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
            // two documents; shallow comparisons work.
            // eslint-disable-next-line eqeqeq
            outermostContext = context == document || context || outermost;
        }

        // Add elements passing elementMatchers directly to results
        // Support: IE<9, Safari
        // Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
        for ( ; i !== len && ( elem = elems[ i ] ) != null; i++ ) {
            if ( byElement && elem ) {
                j = 0;

                // Support: IE 11+, Edge 17 - 18+
                // IE/Edge sometimes throw a "Permission denied" error when strict-comparing
                // two documents; shallow comparisons work.
                // eslint-disable-next-line eqeqeq
                if ( !context && elem.ownerDocument != document ) {
                    setDocument( elem );
                    xml = !documentIsHTML;
                }
                while ( ( matcher = elementMatchers[ j++ ] ) ) {
                    if ( matcher( elem, context || document, xml ) ) {
                        results.push( elem );
                        break;
                    }
                }
                if ( outermost ) {
                    dirruns = dirrunsUnique;
                }
            }

            // Track unmatched elements for set filters
            if ( bySet ) {

                // They will have gone through all possible matchers
                if ( ( elem = !matcher && elem ) ) {
                    matchedCount--;
                }

                // Lengthen the array for every element, matched or not
                if ( seed ) {
                    unmatched.push( elem );
                }
            }
        }

        // `i` is now the count of elements visited above, and adding it to `matchedCount`
        // makes the latter nonnegative.
        matchedCount += i;

        // Apply set filters to unmatched elements
        // NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
        // equals `i`), unless we didn't visit _any_ elements in the above loop because we have
        // no element matchers and no seed.
        // Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
        // case, which will result in a "00" `matchedCount` that differs from `i` but is also
        // numerically zero.
        if ( bySet && i !== matchedCount ) {
            j = 0;
            while ( ( matcher = setMatchers[ j++ ] ) ) {
                matcher( unmatched, setMatched, context, xml );
            }

            if ( seed ) {

                // Reintegrate element matches to eliminate the need for sorting
                if ( matchedCount > 0 ) {
                    while ( i-- ) {
                        if ( !( unmatched[ i ] || setMatched[ i ] ) ) {
                            setMatched[ i ] = pop.call( results );
                        }
                    }
                }

                // Discard index placeholder values to get only actual matches
                setMatched = condense( setMatched );
            }

            // Add matches to results
            push.apply( results, setMatched );

            // Seedless set matches succeeding multiple successful matchers stipulate sorting
            if ( outermost && !seed && setMatched.length > 0 &&
                ( matchedCount + setMatchers.length ) > 1 ) {

                Sizzle.uniqueSort( results );
            }
        }

        // Override manipulation of globals by nested matchers
        if ( outermost ) {
            dirruns = dirrunsUnique;
            outermostContext = contextBackup;
        }

        return unmatched;
    };

return bySet ?
    markFunction( superMatcher ) :
    superMatcher;
}

compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
var i,
    setMatchers = [],
    elementMatchers = [],
    cached = compilerCache[ selector + " " ];

if ( !cached ) {

    // Generate a function of recursive functions that can be used to check each element
    if ( !match ) {
        match = tokenize( selector );
    }
    i = match.length;
    while ( i-- ) {
        cached = matcherFromTokens( match[ i ] );
        if ( cached[ expando ] ) {
            setMatchers.push( cached );
        } else {
            elementMatchers.push( cached );
        }
    }

    // Cache the compiled function
    cached = compilerCache(
        selector,
        matcherFromGroupMatchers( elementMatchers, setMatchers )
    );

    // Save selector and tokenization
    cached.selector = selector;
}
return cached;
};

/**
* A low-level selection function that works with Sizzle's compiled
*  selector functions
* @param {String|Function} selector A selector or a pre-compiled
*  selector function built with Sizzle.compile
* @param {Element} context
* @param {Array} [results]
* @param {Array} [seed] A set of elements to match against
*/
select = Sizzle.select = function( selector, context, results, seed ) {
var i, tokens, token, type, find,
    compiled = typeof selector === "function" && selector,
    match = !seed && tokenize( ( selector = compiled.selector || selector ) );

results = results || [];

// Try to minimize operations if there is only one selector in the list and no seed
// (the latter of which guarantees us context)
if ( match.length === 1 ) {

    // Reduce context if the leading compound selector is an ID
    tokens = match[ 0 ] = match[ 0 ].slice( 0 );
    if ( tokens.length > 2 && ( token = tokens[ 0 ] ).type === "ID" &&
        context.nodeType === 9 && documentIsHTML && Expr.relative[ tokens[ 1 ].type ] ) {

        context = ( Expr.find[ "ID" ]( token.matches[ 0 ]
            .replace( runescape, funescape ), context ) || [] )[ 0 ];
        if ( !context ) {
            return results;

        // Precompiled matchers will still verify ancestry, so step up a level
        } else if ( compiled ) {
            context = context.parentNode;
        }

        selector = selector.slice( tokens.shift().value.length );
    }

    // Fetch a seed set for right-to-left matching
    i = matchExpr[ "needsContext" ].test( selector ) ? 0 : tokens.length;
    while ( i-- ) {
        token = tokens[ i ];

        // Abort if we hit a combinator
        if ( Expr.relative[ ( type = token.type ) ] ) {
            break;
        }
        if ( ( find = Expr.find[ type ] ) ) {

            // Search, expanding context for leading sibling combinators
            if ( ( seed = find(
                token.matches[ 0 ].replace( runescape, funescape ),
                rsibling.test( tokens[ 0 ].type ) && testContext( context.parentNode ) ||
                    context
            ) ) ) {

                // If seed is empty or no tokens remain, we can return early
                tokens.splice( i, 1 );
                selector = seed.length && toSelector( tokens );
                if ( !selector ) {
                    push.apply( results, seed );
                    return results;
                }

                break;
            }
        }
    }
}

// Compile and execute a filtering function if one is not provided
// Provide `match` to avoid retokenization if we modified the selector above
( compiled || compile( selector, match ) )(
    seed,
    context,
    !documentIsHTML,
    results,
    !context || rsibling.test( selector ) && testContext( context.parentNode ) || context
);
return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split( "" ).sort( sortOrder ).join( "" ) === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert( function( el ) {

// Should return 1, but returns 4 (following)
return el.compareDocumentPosition( document.createElement( "fieldset" ) ) & 1;
} );

// Support: IE<8
// Prevent attribute/property "interpolation"
// https://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert( function( el ) {
el.innerHTML = "<a href='#'></a>";
return el.firstChild.getAttribute( "href" ) === "#";
} ) ) {
addHandle( "type|href|height|width", function( elem, name, isXML ) {
    if ( !isXML ) {
        return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
    }
} );
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert( function( el ) {
el.innerHTML = "<input/>";
el.firstChild.setAttribute( "value", "" );
return el.firstChild.getAttribute( "value" ) === "";
} ) ) {
addHandle( "value", function( elem, _name, isXML ) {
    if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
        return elem.defaultValue;
    }
} );
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert( function( el ) {
return el.getAttribute( "disabled" ) == null;
} ) ) {
addHandle( booleans, function( elem, name, isXML ) {
    var val;
    if ( !isXML ) {
        return elem[ name ] === true ? name.toLowerCase() :
            ( val = elem.getAttributeNode( name ) ) && val.specified ?
                val.value :
                null;
    }
} );
}

return Sizzle;

} )( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;

// Deprecated
jQuery.expr[ ":" ] = jQuery.expr.pseudos;
jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;
jQuery.escapeSelector = Sizzle.escape;




var dir = function( elem, dir, until ) {
var matched = [],
    truncate = until !== undefined;

while ( ( elem = elem[ dir ] ) && elem.nodeType !== 9 ) {
    if ( elem.nodeType === 1 ) {
        if ( truncate && jQuery( elem ).is( until ) ) {
            break;
        }
        matched.push( elem );
    }
}
return matched;
};


var siblings = function( n, elem ) {
var matched = [];

for ( ; n; n = n.nextSibling ) {
    if ( n.nodeType === 1 && n !== elem ) {
        matched.push( n );
    }
}

return matched;
};


var rneedsContext = jQuery.expr.match.needsContext;



function nodeName( elem, name ) {

return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();

}
var rsingleTag = ( /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i );



// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
if ( isFunction( qualifier ) ) {
    return jQuery.grep( elements, function( elem, i ) {
        return !!qualifier.call( elem, i, elem ) !== not;
    } );
}

// Single element
if ( qualifier.nodeType ) {
    return jQuery.grep( elements, function( elem ) {
        return ( elem === qualifier ) !== not;
    } );
}

// Arraylike of elements (jQuery, arguments, Array)
if ( typeof qualifier !== "string" ) {
    return jQuery.grep( elements, function( elem ) {
        return ( indexOf.call( qualifier, elem ) > -1 ) !== not;
    } );
}

// Filtered directly for both simple and complex selectors
return jQuery.filter( qualifier, elements, not );
}

jQuery.filter = function( expr, elems, not ) {
var elem = elems[ 0 ];

if ( not ) {
    expr = ":not(" + expr + ")";
}

if ( elems.length === 1 && elem.nodeType === 1 ) {
    return jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [];
}

return jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
    return elem.nodeType === 1;
} ) );
};

jQuery.fn.extend( {
find: function( selector ) {
    var i, ret,
        len = this.length,
        self = this;

    if ( typeof selector !== "string" ) {
        return this.pushStack( jQuery( selector ).filter( function() {
            for ( i = 0; i < len; i++ ) {
                if ( jQuery.contains( self[ i ], this ) ) {
                    return true;
                }
            }
        } ) );
    }

    ret = this.pushStack( [] );

    for ( i = 0; i < len; i++ ) {
        jQuery.find( selector, self[ i ], ret );
    }

    return len > 1 ? jQuery.uniqueSort( ret ) : ret;
},
filter: function( selector ) {
    return this.pushStack( winnow( this, selector || [], false ) );
},
not: function( selector ) {
    return this.pushStack( winnow( this, selector || [], true ) );
},
is: function( selector ) {
    return !!winnow(
        this,

        // If this is a positional/relative selector, check membership in the returned set
        // so $("p:first").is("p:last") won't return true for a doc with two "p".
        typeof selector === "string" && rneedsContext.test( selector ) ?
            jQuery( selector ) :
            selector || [],
        false
    ).length;
}
} );


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

// A simple way to check for HTML strings
// Prioritize #id over <tag> to avoid XSS via location.hash (trac-9521)
// Strict HTML recognition (trac-11290: must start with <)
// Shortcut simple #id case for speed
rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,

init = jQuery.fn.init = function( selector, context, root ) {
    var match, elem;

    // HANDLE: $(""), $(null), $(undefined), $(false)
    if ( !selector ) {
        return this;
    }

    // Method init() accepts an alternate rootjQuery
    // so migrate can support jQuery.sub (gh-2101)
    root = root || rootjQuery;

    // Handle HTML strings
    if ( typeof selector === "string" ) {
        if ( selector[ 0 ] === "<" &&
            selector[ selector.length - 1 ] === ">" &&
            selector.length >= 3 ) {

            // Assume that strings that start and end with <> are HTML and skip the regex check
            match = [ null, selector, null ];

        } else {
            match = rquickExpr.exec( selector );
        }

        // Match html or make sure no context is specified for #id
        if ( match && ( match[ 1 ] || !context ) ) {

            // HANDLE: $(html) -> $(array)
            if ( match[ 1 ] ) {
                context = context instanceof jQuery ? context[ 0 ] : context;

                // Option to run scripts is true for back-compat
                // Intentionally let the error be thrown if parseHTML is not present
                jQuery.merge( this, jQuery.parseHTML(
                    match[ 1 ],
                    context && context.nodeType ? context.ownerDocument || context : document,
                    true
                ) );

                // HANDLE: $(html, props)
                if ( rsingleTag.test( match[ 1 ] ) && jQuery.isPlainObject( context ) ) {
                    for ( match in context ) {

                        // Properties of context are called as methods if possible
                        if ( isFunction( this[ match ] ) ) {
                            this[ match ]( context[ match ] );

                        // ...and otherwise set as attributes
                        } else {
                            this.attr( match, context[ match ] );
                        }
                    }
                }

                return this;

            // HANDLE: $(#id)
            } else {
                elem = document.getElementById( match[ 2 ] );

                if ( elem ) {

                    // Inject the element directly into the jQuery object
                    this[ 0 ] = elem;
                    this.length = 1;
                }
                return this;
            }

        // HANDLE: $(expr, $(...))
        } else if ( !context || context.jquery ) {
            return ( context || root ).find( selector );

        // HANDLE: $(expr, context)
        // (which is just equivalent to: $(context).find(expr)
        } else {
            return this.constructor( context ).find( selector );
        }

    // HANDLE: $(DOMElement)
    } else if ( selector.nodeType ) {
        this[ 0 ] = selector;
        this.length = 1;
        return this;

    // HANDLE: $(function)
    // Shortcut for document ready
    } else if ( isFunction( selector ) ) {
        return root.ready !== undefined ?
            root.ready( selector ) :

            // Execute immediately if ready is not present
            selector( jQuery );
    }

    return jQuery.makeArray( selector, this );
};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,

// Methods guaranteed to produce a unique set when starting from a unique set
guaranteedUnique = {
    children: true,
    contents: true,
    next: true,
    prev: true
};

jQuery.fn.extend( {
has: function( target ) {
    var targets = jQuery( target, this ),
        l = targets.length;

    return this.filter( function() {
        var i = 0;
        for ( ; i < l; i++ ) {
            if ( jQuery.contains( this, targets[ i ] ) ) {
                return true;
            }
        }
    } );
},

closest: function( selectors, context ) {
    var cur,
        i = 0,
        l = this.length,
        matched = [],
        targets = typeof selectors !== "string" && jQuery( selectors );

    // Positional selectors never match, since there's no _selection_ context
    if ( !rneedsContext.test( selectors ) ) {
        for ( ; i < l; i++ ) {
            for ( cur = this[ i ]; cur && cur !== context; cur = cur.parentNode ) {

                // Always skip document fragments
                if ( cur.nodeType < 11 && ( targets ?
                    targets.index( cur ) > -1 :

                    // Don't pass non-elements to Sizzle
                    cur.nodeType === 1 &&
                        jQuery.find.matchesSelector( cur, selectors ) ) ) {

                    matched.push( cur );
                    break;
                }
            }
        }
    }

    return this.pushStack( matched.length > 1 ? jQuery.uniqueSort( matched ) : matched );
},

// Determine the position of an element within the set
index: function( elem ) {

    // No argument, return index in parent
    if ( !elem ) {
        return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
    }

    // Index in selector
    if ( typeof elem === "string" ) {
        return indexOf.call( jQuery( elem ), this[ 0 ] );
    }

    // Locate the position of the desired element
    return indexOf.call( this,

        // If it receives a jQuery object, the first element is used
        elem.jquery ? elem[ 0 ] : elem
    );
},

add: function( selector, context ) {
    return this.pushStack(
        jQuery.uniqueSort(
            jQuery.merge( this.get(), jQuery( selector, context ) )
        )
    );
},

addBack: function( selector ) {
    return this.add( selector == null ?
        this.prevObject : this.prevObject.filter( selector )
    );
}
} );

function sibling( cur, dir ) {
while ( ( cur = cur[ dir ] ) && cur.nodeType !== 1 ) {}
return cur;
}

jQuery.each( {
parent: function( elem ) {
    var parent = elem.parentNode;
    return parent && parent.nodeType !== 11 ? parent : null;
},
parents: function( elem ) {
    return dir( elem, "parentNode" );
},
parentsUntil: function( elem, _i, until ) {
    return dir( elem, "parentNode", until );
},
next: function( elem ) {
    return sibling( elem, "nextSibling" );
},
prev: function( elem ) {
    return sibling( elem, "previousSibling" );
},
nextAll: function( elem ) {
    return dir( elem, "nextSibling" );
},
prevAll: function( elem ) {
    return dir( elem, "previousSibling" );
},
nextUntil: function( elem, _i, until ) {
    return dir( elem, "nextSibling", until );
},
prevUntil: function( elem, _i, until ) {
    return dir( elem, "previousSibling", until );
},
siblings: function( elem ) {
    return siblings( ( elem.parentNode || {} ).firstChild, elem );
},
children: function( elem ) {
    return siblings( elem.firstChild );
},
contents: function( elem ) {
    if ( elem.contentDocument != null &&

        // Support: IE 11+
        // <object> elements with no `data` attribute has an object
        // `contentDocument` with a `null` prototype.
        getProto( elem.contentDocument ) ) {

        return elem.contentDocument;
    }

    // Support: IE 9 - 11 only, iOS 7 only, Android Browser <=4.3 only
    // Treat the template element as a regular one in browsers that
    // don't support it.
    if ( nodeName( elem, "template" ) ) {
        elem = elem.content || elem;
    }

    return jQuery.merge( [], elem.childNodes );
}
}, function( name, fn ) {
jQuery.fn[ name ] = function( until, selector ) {
    var matched = jQuery.map( this, fn, until );

    if ( name.slice( -5 ) !== "Until" ) {
        selector = until;
    }

    if ( selector && typeof selector === "string" ) {
        matched = jQuery.filter( selector, matched );
    }

    if ( this.length > 1 ) {

        // Remove duplicates
        if ( !guaranteedUnique[ name ] ) {
            jQuery.uniqueSort( matched );
        }

        // Reverse order for parents* and prev-derivatives
        if ( rparentsprev.test( name ) ) {
            matched.reverse();
        }
    }

    return this.pushStack( matched );
};
} );
var rnothtmlwhite = ( /[^\x20\t\r\n\f]+/g );



// Convert String-formatted options into Object-formatted ones
function createOptions( options ) {
var object = {};
jQuery.each( options.match( rnothtmlwhite ) || [], function( _, flag ) {
    object[ flag ] = true;
} );
return object;
}

/*
* Create a callback list using the following parameters:
*
*	options: an optional list of space-separated options that will change how
*			the callback list behaves or a more traditional option object
*
* By default a callback list will act like an event callback list and can be
* "fired" multiple times.
*
* Possible options:
*
*	once:			will ensure the callback list can only be fired once (like a Deferred)
*
*	memory:			will keep track of previous values and will call any callback added
*					after the list has been fired right away with the latest "memorized"
*					values (like a Deferred)
*
*	unique:			will ensure a callback can only be added once (no duplicate in the list)
*
*	stopOnFalse:	interrupt callings when a callback returns false
*
*/
jQuery.Callbacks = function( options ) {

// Convert options from String-formatted to Object-formatted if needed
// (we check in cache first)
options = typeof options === "string" ?
    createOptions( options ) :
    jQuery.extend( {}, options );

var // Flag to know if list is currently firing
    firing,

    // Last fire value for non-forgettable lists
    memory,

    // Flag to know if list was already fired
    fired,

    // Flag to prevent firing
    locked,

    // Actual callback list
    list = [],

    // Queue of execution data for repeatable lists
    queue = [],

    // Index of currently firing callback (modified by add/remove as needed)
    firingIndex = -1,

    // Fire callbacks
    fire = function() {

        // Enforce single-firing
        locked = locked || options.once;

        // Execute callbacks for all pending executions,
        // respecting firingIndex overrides and runtime changes
        fired = firing = true;
        for ( ; queue.length; firingIndex = -1 ) {
            memory = queue.shift();
            while ( ++firingIndex < list.length ) {

                // Run callback and check for early termination
                if ( list[ firingIndex ].apply( memory[ 0 ], memory[ 1 ] ) === false &&
                    options.stopOnFalse ) {

                    // Jump to end and forget the data so .add doesn't re-fire
                    firingIndex = list.length;
                    memory = false;
                }
            }
        }

        // Forget the data if we're done with it
        if ( !options.memory ) {
            memory = false;
        }

        firing = false;

        // Clean up if we're done firing for good
        if ( locked ) {

            // Keep an empty list if we have data for future add calls
            if ( memory ) {
                list = [];

            // Otherwise, this object is spent
            } else {
                list = "";
            }
        }
    },

    // Actual Callbacks object
    self = {

        // Add a callback or a collection of callbacks to the list
        add: function() {
            if ( list ) {

                // If we have memory from a past run, we should fire after adding
                if ( memory && !firing ) {
                    firingIndex = list.length - 1;
                    queue.push( memory );
                }

                ( function add( args ) {
                    jQuery.each( args, function( _, arg ) {
                        if ( isFunction( arg ) ) {
                            if ( !options.unique || !self.has( arg ) ) {
                                list.push( arg );
                            }
                        } else if ( arg && arg.length && toType( arg ) !== "string" ) {

                            // Inspect recursively
                            add( arg );
                        }
                    } );
                } )( arguments );

                if ( memory && !firing ) {
                    fire();
                }
            }
            return this;
        },

        // Remove a callback from the list
        remove: function() {
            jQuery.each( arguments, function( _, arg ) {
                var index;
                while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
                    list.splice( index, 1 );

                    // Handle firing indexes
                    if ( index <= firingIndex ) {
                        firingIndex--;
                    }
                }
            } );
            return this;
        },

        // Check if a given callback is in the list.
        // If no argument is given, return whether or not list has callbacks attached.
        has: function( fn ) {
            return fn ?
                jQuery.inArray( fn, list ) > -1 :
                list.length > 0;
        },

        // Remove all callbacks from the list
        empty: function() {
            if ( list ) {
                list = [];
            }
            return this;
        },

        // Disable .fire and .add
        // Abort any current/pending executions
        // Clear all callbacks and values
        disable: function() {
            locked = queue = [];
            list = memory = "";
            return this;
        },
        disabled: function() {
            return !list;
        },

        // Disable .fire
        // Also disable .add unless we have memory (since it would have no effect)
        // Abort any pending executions
        lock: function() {
            locked = queue = [];
            if ( !memory && !firing ) {
                list = memory = "";
            }
            return this;
        },
        locked: function() {
            return !!locked;
        },

        // Call all callbacks with the given context and arguments
        fireWith: function( context, args ) {
            if ( !locked ) {
                args = args || [];
                args = [ context, args.slice ? args.slice() : args ];
                queue.push( args );
                if ( !firing ) {
                    fire();
                }
            }
            return this;
        },

        // Call all the callbacks with the given arguments
        fire: function() {
            self.fireWith( this, arguments );
            return this;
        },

        // To know if the callbacks have already been called at least once
        fired: function() {
            return !!fired;
        }
    };

return self;
};


function Identity( v ) {
return v;
}
function Thrower( ex ) {
throw ex;
}

function adoptValue( value, resolve, reject, noValue ) {
var method;

try {

    // Check for promise aspect first to privilege synchronous behavior
    if ( value && isFunction( ( method = value.promise ) ) ) {
        method.call( value ).done( resolve ).fail( reject );

    // Other thenables
    } else if ( value && isFunction( ( method = value.then ) ) ) {
        method.call( value, resolve, reject );

    // Other non-thenables
    } else {

        // Control `resolve` arguments by letting Array#slice cast boolean `noValue` to integer:
        // * false: [ value ].slice( 0 ) => resolve( value )
        // * true: [ value ].slice( 1 ) => resolve()
        resolve.apply( undefined, [ value ].slice( noValue ) );
    }

// For Promises/A+, convert exceptions into rejections
// Since jQuery.when doesn't unwrap thenables, we can skip the extra checks appearing in
// Deferred#then to conditionally suppress rejection.
} catch ( value ) {

    // Support: Android 4.0 only
    // Strict mode functions invoked without .call/.apply get global-object context
    reject.apply( undefined, [ value ] );
}
}

jQuery.extend( {

Deferred: function( func ) {
    var tuples = [

            // action, add listener, callbacks,
            // ... .then handlers, argument index, [final state]
            [ "notify", "progress", jQuery.Callbacks( "memory" ),
                jQuery.Callbacks( "memory" ), 2 ],
            [ "resolve", "done", jQuery.Callbacks( "once memory" ),
                jQuery.Callbacks( "once memory" ), 0, "resolved" ],
            [ "reject", "fail", jQuery.Callbacks( "once memory" ),
                jQuery.Callbacks( "once memory" ), 1, "rejected" ]
        ],
        state = "pending",
        promise = {
            state: function() {
                return state;
            },
            always: function() {
                deferred.done( arguments ).fail( arguments );
                return this;
            },
            "catch": function( fn ) {
                return promise.then( null, fn );
            },

            // Keep pipe for back-compat
            pipe: function( /* fnDone, fnFail, fnProgress */ ) {
                var fns = arguments;

                return jQuery.Deferred( function( newDefer ) {
                    jQuery.each( tuples, function( _i, tuple ) {

                        // Map tuples (progress, done, fail) to arguments (done, fail, progress)
                        var fn = isFunction( fns[ tuple[ 4 ] ] ) && fns[ tuple[ 4 ] ];

                        // deferred.progress(function() { bind to newDefer or newDefer.notify })
                        // deferred.done(function() { bind to newDefer or newDefer.resolve })
                        // deferred.fail(function() { bind to newDefer or newDefer.reject })
                        deferred[ tuple[ 1 ] ]( function() {
                            var returned = fn && fn.apply( this, arguments );
                            if ( returned && isFunction( returned.promise ) ) {
                                returned.promise()
                                    .progress( newDefer.notify )
                                    .done( newDefer.resolve )
                                    .fail( newDefer.reject );
                            } else {
                                newDefer[ tuple[ 0 ] + "With" ](
                                    this,
                                    fn ? [ returned ] : arguments
                                );
                            }
                        } );
                    } );
                    fns = null;
                } ).promise();
            },
            then: function( onFulfilled, onRejected, onProgress ) {
                var maxDepth = 0;
                function resolve( depth, deferred, handler, special ) {
                    return function() {
                        var that = this,
                            args = arguments,
                            mightThrow = function() {
                                var returned, then;

                                // Support: Promises/A+ section 2.3.3.3.3
                                // https://promisesaplus.com/#point-59
                                // Ignore double-resolution attempts
                                if ( depth < maxDepth ) {
                                    return;
                                }

                                returned = handler.apply( that, args );

                                // Support: Promises/A+ section 2.3.1
                                // https://promisesaplus.com/#point-48
                                if ( returned === deferred.promise() ) {
                                    throw new TypeError( "Thenable self-resolution" );
                                }

                                // Support: Promises/A+ sections 2.3.3.1, 3.5
                                // https://promisesaplus.com/#point-54
                                // https://promisesaplus.com/#point-75
                                // Retrieve `then` only once
                                then = returned &&

                                    // Support: Promises/A+ section 2.3.4
                                    // https://promisesaplus.com/#point-64
                                    // Only check objects and functions for thenability
                                    ( typeof returned === "object" ||
                                        typeof returned === "function" ) &&
                                    returned.then;

                                // Handle a returned thenable
                                if ( isFunction( then ) ) {

                                    // Special processors (notify) just wait for resolution
                                    if ( special ) {
                                        then.call(
                                            returned,
                                            resolve( maxDepth, deferred, Identity, special ),
                                            resolve( maxDepth, deferred, Thrower, special )
                                        );

                                    // Normal processors (resolve) also hook into progress
                                    } else {

                                        // ...and disregard older resolution values
                                        maxDepth++;

                                        then.call(
                                            returned,
                                            resolve( maxDepth, deferred, Identity, special ),
                                            resolve( maxDepth, deferred, Thrower, special ),
                                            resolve( maxDepth, deferred, Identity,
                                                deferred.notifyWith )
                                        );
                                    }

                                // Handle all other returned values
                                } else {

                                    // Only substitute handlers pass on context
                                    // and multiple values (non-spec behavior)
                                    if ( handler !== Identity ) {
                                        that = undefined;
                                        args = [ returned ];
                                    }

                                    // Process the value(s)
                                    // Default process is resolve
                                    ( special || deferred.resolveWith )( that, args );
                                }
                            },

                            // Only normal processors (resolve) catch and reject exceptions
                            process = special ?
                                mightThrow :
                                function() {
                                    try {
                                        mightThrow();
                                    } catch ( e ) {

                                        if ( jQuery.Deferred.exceptionHook ) {
                                            jQuery.Deferred.exceptionHook( e,
                                                process.stackTrace );
                                        }

                                        // Support: Promises/A+ section 2.3.3.3.4.1
                                        // https://promisesaplus.com/#point-61
                                        // Ignore post-resolution exceptions
                                        if ( depth + 1 >= maxDepth ) {

                                            // Only substitute handlers pass on context
                                            // and multiple values (non-spec behavior)
                                            if ( handler !== Thrower ) {
                                                that = undefined;
                                                args = [ e ];
                                            }

                                            deferred.rejectWith( that, args );
                                        }
                                    }
                                };

                        // Support: Promises/A+ section 2.3.3.3.1
                        // https://promisesaplus.com/#point-57
                        // Re-resolve promises immediately to dodge false rejection from
                        // subsequent errors
                        if ( depth ) {
                            process();
                        } else {

                            // Call an optional hook to record the stack, in case of exception
                            // since it's otherwise lost when execution goes async
                            if ( jQuery.Deferred.getStackHook ) {
                                process.stackTrace = jQuery.Deferred.getStackHook();
                            }
                            window.setTimeout( process );
                        }
                    };
                }

                return jQuery.Deferred( function( newDefer ) {

                    // progress_handlers.add( ... )
                    tuples[ 0 ][ 3 ].add(
                        resolve(
                            0,
                            newDefer,
                            isFunction( onProgress ) ?
                                onProgress :
                                Identity,
                            newDefer.notifyWith
                        )
                    );

                    // fulfilled_handlers.add( ... )
                    tuples[ 1 ][ 3 ].add(
                        resolve(
                            0,
                            newDefer,
                            isFunction( onFulfilled ) ?
                                onFulfilled :
                                Identity
                        )
                    );

                    // rejected_handlers.add( ... )
                    tuples[ 2 ][ 3 ].add(
                        resolve(
                            0,
                            newDefer,
                            isFunction( onRejected ) ?
                                onRejected :
                                Thrower
                        )
                    );
                } ).promise();
            },

            // Get a promise for this deferred
            // If obj is provided, the promise aspect is added to the object
            promise: function( obj ) {
                return obj != null ? jQuery.extend( obj, promise ) : promise;
            }
        },
        deferred = {};

    // Add list-specific methods
    jQuery.each( tuples, function( i, tuple ) {
        var list = tuple[ 2 ],
            stateString = tuple[ 5 ];

        // promise.progress = list.add
        // promise.done = list.add
        // promise.fail = list.add
        promise[ tuple[ 1 ] ] = list.add;

        // Handle state
        if ( stateString ) {
            list.add(
                function() {

                    // state = "resolved" (i.e., fulfilled)
                    // state = "rejected"
                    state = stateString;
                },

                // rejected_callbacks.disable
                // fulfilled_callbacks.disable
                tuples[ 3 - i ][ 2 ].disable,

                // rejected_handlers.disable
                // fulfilled_handlers.disable
                tuples[ 3 - i ][ 3 ].disable,

                // progress_callbacks.lock
                tuples[ 0 ][ 2 ].lock,

                // progress_handlers.lock
                tuples[ 0 ][ 3 ].lock
            );
        }

        // progress_handlers.fire
        // fulfilled_handlers.fire
        // rejected_handlers.fire
        list.add( tuple[ 3 ].fire );

        // deferred.notify = function() { deferred.notifyWith(...) }
        // deferred.resolve = function() { deferred.resolveWith(...) }
        // deferred.reject = function() { deferred.rejectWith(...) }
        deferred[ tuple[ 0 ] ] = function() {
            deferred[ tuple[ 0 ] + "With" ]( this === deferred ? undefined : this, arguments );
            return this;
        };

        // deferred.notifyWith = list.fireWith
        // deferred.resolveWith = list.fireWith
        // deferred.rejectWith = list.fireWith
        deferred[ tuple[ 0 ] + "With" ] = list.fireWith;
    } );

    // Make the deferred a promise
    promise.promise( deferred );

    // Call given func if any
    if ( func ) {
        func.call( deferred, deferred );
    }

    // All done!
    return deferred;
},

// Deferred helper
when: function( singleValue ) {
    var

        // count of uncompleted subordinates
        remaining = arguments.length,

        // count of unprocessed arguments
        i = remaining,

        // subordinate fulfillment data
        resolveContexts = Array( i ),
        resolveValues = slice.call( arguments ),

        // the primary Deferred
        primary = jQuery.Deferred(),

        // subordinate callback factory
        updateFunc = function( i ) {
            return function( value ) {
                resolveContexts[ i ] = this;
                resolveValues[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
                if ( !( --remaining ) ) {
                    primary.resolveWith( resolveContexts, resolveValues );
                }
            };
        };

    // Single- and empty arguments are adopted like Promise.resolve
    if ( remaining <= 1 ) {
        adoptValue( singleValue, primary.done( updateFunc( i ) ).resolve, primary.reject,
            !remaining );

        // Use .then() to unwrap secondary thenables (cf. gh-3000)
        if ( primary.state() === "pending" ||
            isFunction( resolveValues[ i ] && resolveValues[ i ].then ) ) {

            return primary.then();
        }
    }

    // Multiple arguments are aggregated like Promise.all array elements
    while ( i-- ) {
        adoptValue( resolveValues[ i ], updateFunc( i ), primary.reject );
    }

    return primary.promise();
}
} );


// These usually indicate a programmer mistake during development,
// warn about them ASAP rather than swallowing them by default.
var rerrorNames = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;

jQuery.Deferred.exceptionHook = function( error, stack ) {

// Support: IE 8 - 9 only
// Console exists when dev tools are open, which can happen at any time
if ( window.console && window.console.warn && error && rerrorNames.test( error.name ) ) {
    window.console.warn( "jQuery.Deferred exception: " + error.message, error.stack, stack );
}
};




jQuery.readyException = function( error ) {
window.setTimeout( function() {
    throw error;
} );
};




// The deferred used on DOM ready
var readyList = jQuery.Deferred();

jQuery.fn.ready = function( fn ) {

readyList
    .then( fn )

    // Wrap jQuery.readyException in a function so that the lookup
    // happens at the time of error handling instead of callback
    // registration.
    .catch( function( error ) {
        jQuery.readyException( error );
    } );

return this;
};

jQuery.extend( {

// Is the DOM ready to be used? Set to true once it occurs.
isReady: false,

// A counter to track how many items to wait for before
// the ready event fires. See trac-6781
readyWait: 1,

// Handle when the DOM is ready
ready: function( wait ) {

    // Abort if there are pending holds or we're already ready
    if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
        return;
    }

    // Remember that the DOM is ready
    jQuery.isReady = true;

    // If a normal DOM Ready event fired, decrement, and wait if need be
    if ( wait !== true && --jQuery.readyWait > 0 ) {
        return;
    }

    // If there are functions bound, to execute
    readyList.resolveWith( document, [ jQuery ] );
}
} );

jQuery.ready.then = readyList.then;

// The ready event handler and self cleanup method
function completed() {
document.removeEventListener( "DOMContentLoaded", completed );
window.removeEventListener( "load", completed );
jQuery.ready();
}

// Catch cases where $(document).ready() is called
// after the browser event has already occurred.
// Support: IE <=9 - 10 only
// Older IE sometimes signals "interactive" too soon
if ( document.readyState === "complete" ||
( document.readyState !== "loading" && !document.documentElement.doScroll ) ) {

// Handle it asynchronously to allow scripts the opportunity to delay ready
window.setTimeout( jQuery.ready );

} else {

// Use the handy event callback
document.addEventListener( "DOMContentLoaded", completed );

// A fallback to window.onload, that will always work
window.addEventListener( "load", completed );
}




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
var i = 0,
    len = elems.length,
    bulk = key == null;

// Sets many values
if ( toType( key ) === "object" ) {
    chainable = true;
    for ( i in key ) {
        access( elems, fn, i, key[ i ], true, emptyGet, raw );
    }

// Sets one value
} else if ( value !== undefined ) {
    chainable = true;

    if ( !isFunction( value ) ) {
        raw = true;
    }

    if ( bulk ) {

        // Bulk operations run against the entire set
        if ( raw ) {
            fn.call( elems, value );
            fn = null;

        // ...except when executing function values
        } else {
            bulk = fn;
            fn = function( elem, _key, value ) {
                return bulk.call( jQuery( elem ), value );
            };
        }
    }

    if ( fn ) {
        for ( ; i < len; i++ ) {
            fn(
                elems[ i ], key, raw ?
                    value :
                    value.call( elems[ i ], i, fn( elems[ i ], key ) )
            );
        }
    }
}

if ( chainable ) {
    return elems;
}

// Gets
if ( bulk ) {
    return fn.call( elems );
}

return len ? fn( elems[ 0 ], key ) : emptyGet;
};


// Matches dashed string for camelizing
var rmsPrefix = /^-ms-/,
rdashAlpha = /-([a-z])/g;

// Used by camelCase as callback to replace()
function fcamelCase( _all, letter ) {
return letter.toUpperCase();
}

// Convert dashed to camelCase; used by the css and data modules
// Support: IE <=9 - 11, Edge 12 - 15
// Microsoft forgot to hump their vendor prefix (trac-9572)
function camelCase( string ) {
return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
}
var acceptData = function( owner ) {

// Accepts only:
//  - Node
//    - Node.ELEMENT_NODE
//    - Node.DOCUMENT_NODE
//  - Object
//    - Any
return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};




function Data() {
this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;

Data.prototype = {

cache: function( owner ) {

    // Check if the owner object already has a cache
    var value = owner[ this.expando ];

    // If not, create one
    if ( !value ) {
        value = {};

        // We can accept data for non-element nodes in modern browsers,
        // but we should not, see trac-8335.
        // Always return an empty object.
        if ( acceptData( owner ) ) {

            // If it is a node unlikely to be stringify-ed or looped over
            // use plain assignment
            if ( owner.nodeType ) {
                owner[ this.expando ] = value;

            // Otherwise secure it in a non-enumerable property
            // configurable must be true to allow the property to be
            // deleted when data is removed
            } else {
                Object.defineProperty( owner, this.expando, {
                    value: value,
                    configurable: true
                } );
            }
        }
    }

    return value;
},
set: function( owner, data, value ) {
    var prop,
        cache = this.cache( owner );

    // Handle: [ owner, key, value ] args
    // Always use camelCase key (gh-2257)
    if ( typeof data === "string" ) {
        cache[ camelCase( data ) ] = value;

    // Handle: [ owner, { properties } ] args
    } else {

        // Copy the properties one-by-one to the cache object
        for ( prop in data ) {
            cache[ camelCase( prop ) ] = data[ prop ];
        }
    }
    return cache;
},
get: function( owner, key ) {
    return key === undefined ?
        this.cache( owner ) :

        // Always use camelCase key (gh-2257)
        owner[ this.expando ] && owner[ this.expando ][ camelCase( key ) ];
},
access: function( owner, key, value ) {

    // In cases where either:
    //
    //   1. No key was specified
    //   2. A string key was specified, but no value provided
    //
    // Take the "read" path and allow the get method to determine
    // which value to return, respectively either:
    //
    //   1. The entire cache object
    //   2. The data stored at the key
    //
    if ( key === undefined ||
            ( ( key && typeof key === "string" ) && value === undefined ) ) {

        return this.get( owner, key );
    }

    // When the key is not a string, or both a key and value
    // are specified, set or extend (existing objects) with either:
    //
    //   1. An object of properties
    //   2. A key and value
    //
    this.set( owner, key, value );

    // Since the "set" path can have two possible entry points
    // return the expected data based on which path was taken[*]
    return value !== undefined ? value : key;
},
remove: function( owner, key ) {
    var i,
        cache = owner[ this.expando ];

    if ( cache === undefined ) {
        return;
    }

    if ( key !== undefined ) {

        // Support array or space separated string of keys
        if ( Array.isArray( key ) ) {

            // If key is an array of keys...
            // We always set camelCase keys, so remove that.
            key = key.map( camelCase );
        } else {
            key = camelCase( key );

            // If a key with the spaces exists, use it.
            // Otherwise, create an array by matching non-whitespace
            key = key in cache ?
                [ key ] :
                ( key.match( rnothtmlwhite ) || [] );
        }

        i = key.length;

        while ( i-- ) {
            delete cache[ key[ i ] ];
        }
    }

    // Remove the expando if there's no more data
    if ( key === undefined || jQuery.isEmptyObject( cache ) ) {

        // Support: Chrome <=35 - 45
        // Webkit & Blink performance suffers when deleting properties
        // from DOM nodes, so set to undefined instead
        // https://bugs.chromium.org/p/chromium/issues/detail?id=378607 (bug restricted)
        if ( owner.nodeType ) {
            owner[ this.expando ] = undefined;
        } else {
            delete owner[ this.expando ];
        }
    }
},
hasData: function( owner ) {
    var cache = owner[ this.expando ];
    return cache !== undefined && !jQuery.isEmptyObject( cache );
}
};
var dataPriv = new Data();

var dataUser = new Data();



//	Implementation Summary
//
//	1. Enforce API surface and semantic compatibility with 1.9.x branch
//	2. Improve the module's maintainability by reducing the storage
//		paths to a single mechanism.
//	3. Use the same single mechanism to support "private" and "user" data.
//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//	5. Avoid exposing implementation details on user objects (eg. expando properties)
//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
rmultiDash = /[A-Z]/g;

function getData( data ) {
if ( data === "true" ) {
    return true;
}

if ( data === "false" ) {
    return false;
}

if ( data === "null" ) {
    return null;
}

// Only convert to a number if it doesn't change the string
if ( data === +data + "" ) {
    return +data;
}

if ( rbrace.test( data ) ) {
    return JSON.parse( data );
}

return data;
}

function dataAttr( elem, key, data ) {
var name;

// If nothing was found internally, try to fetch any
// data from the HTML5 data-* attribute
if ( data === undefined && elem.nodeType === 1 ) {
    name = "data-" + key.replace( rmultiDash, "-$&" ).toLowerCase();
    data = elem.getAttribute( name );

    if ( typeof data === "string" ) {
        try {
            data = getData( data );
        } catch ( e ) {}

        // Make sure we set the data so it isn't changed later
        dataUser.set( elem, key, data );
    } else {
        data = undefined;
    }
}
return data;
}

jQuery.extend( {
hasData: function( elem ) {
    return dataUser.hasData( elem ) || dataPriv.hasData( elem );
},

data: function( elem, name, data ) {
    return dataUser.access( elem, name, data );
},

removeData: function( elem, name ) {
    dataUser.remove( elem, name );
},

// TODO: Now that all calls to _data and _removeData have been replaced
// with direct calls to dataPriv methods, these can be deprecated.
_data: function( elem, name, data ) {
    return dataPriv.access( elem, name, data );
},

_removeData: function( elem, name ) {
    dataPriv.remove( elem, name );
}
} );

jQuery.fn.extend( {
data: function( key, value ) {
    var i, name, data,
        elem = this[ 0 ],
        attrs = elem && elem.attributes;

    // Gets all values
    if ( key === undefined ) {
        if ( this.length ) {
            data = dataUser.get( elem );

            if ( elem.nodeType === 1 && !dataPriv.get( elem, "hasDataAttrs" ) ) {
                i = attrs.length;
                while ( i-- ) {

                    // Support: IE 11 only
                    // The attrs elements can be null (trac-14894)
                    if ( attrs[ i ] ) {
                        name = attrs[ i ].name;
                        if ( name.indexOf( "data-" ) === 0 ) {
                            name = camelCase( name.slice( 5 ) );
                            dataAttr( elem, name, data[ name ] );
                        }
                    }
                }
                dataPriv.set( elem, "hasDataAttrs", true );
            }
        }

        return data;
    }

    // Sets multiple values
    if ( typeof key === "object" ) {
        return this.each( function() {
            dataUser.set( this, key );
        } );
    }

    return access( this, function( value ) {
        var data;

        // The calling jQuery object (element matches) is not empty
        // (and therefore has an element appears at this[ 0 ]) and the
        // `value` parameter was not undefined. An empty jQuery object
        // will result in `undefined` for elem = this[ 0 ] which will
        // throw an exception if an attempt to read a data cache is made.
        if ( elem && value === undefined ) {

            // Attempt to get data from the cache
            // The key will always be camelCased in Data
            data = dataUser.get( elem, key );
            if ( data !== undefined ) {
                return data;
            }

            // Attempt to "discover" the data in
            // HTML5 custom data-* attrs
            data = dataAttr( elem, key );
            if ( data !== undefined ) {
                return data;
            }

            // We tried really hard, but the data doesn't exist.
            return;
        }

        // Set the data...
        this.each( function() {

            // We always store the camelCased key
            dataUser.set( this, key, value );
        } );
    }, null, value, arguments.length > 1, null, true );
},

removeData: function( key ) {
    return this.each( function() {
        dataUser.remove( this, key );
    } );
}
} );


jQuery.extend( {
queue: function( elem, type, data ) {
    var queue;

    if ( elem ) {
        type = ( type || "fx" ) + "queue";
        queue = dataPriv.get( elem, type );

        // Speed up dequeue by getting out quickly if this is just a lookup
        if ( data ) {
            if ( !queue || Array.isArray( data ) ) {
                queue = dataPriv.access( elem, type, jQuery.makeArray( data ) );
            } else {
                queue.push( data );
            }
        }
        return queue || [];
    }
},

dequeue: function( elem, type ) {
    type = type || "fx";

    var queue = jQuery.queue( elem, type ),
        startLength = queue.length,
        fn = queue.shift(),
        hooks = jQuery._queueHooks( elem, type ),
        next = function() {
            jQuery.dequeue( elem, type );
        };

    // If the fx queue is dequeued, always remove the progress sentinel
    if ( fn === "inprogress" ) {
        fn = queue.shift();
        startLength--;
    }

    if ( fn ) {

        // Add a progress sentinel to prevent the fx queue from being
        // automatically dequeued
        if ( type === "fx" ) {
            queue.unshift( "inprogress" );
        }

        // Clear up the last queue stop function
        delete hooks.stop;
        fn.call( elem, next, hooks );
    }

    if ( !startLength && hooks ) {
        hooks.empty.fire();
    }
},

// Not public - generate a queueHooks object, or return the current one
_queueHooks: function( elem, type ) {
    var key = type + "queueHooks";
    return dataPriv.get( elem, key ) || dataPriv.access( elem, key, {
        empty: jQuery.Callbacks( "once memory" ).add( function() {
            dataPriv.remove( elem, [ type + "queue", key ] );
        } )
    } );
}
} );

jQuery.fn.extend( {
queue: function( type, data ) {
    var setter = 2;

    if ( typeof type !== "string" ) {
        data = type;
        type = "fx";
        setter--;
    }

    if ( arguments.length < setter ) {
        return jQuery.queue( this[ 0 ], type );
    }

    return data === undefined ?
        this :
        this.each( function() {
            var queue = jQuery.queue( this, type, data );

            // Ensure a hooks for this queue
            jQuery._queueHooks( this, type );

            if ( type === "fx" && queue[ 0 ] !== "inprogress" ) {
                jQuery.dequeue( this, type );
            }
        } );
},
dequeue: function( type ) {
    return this.each( function() {
        jQuery.dequeue( this, type );
    } );
},
clearQueue: function( type ) {
    return this.queue( type || "fx", [] );
},

// Get a promise resolved when queues of a certain type
// are emptied (fx is the type by default)
promise: function( type, obj ) {
    var tmp,
        count = 1,
        defer = jQuery.Deferred(),
        elements = this,
        i = this.length,
        resolve = function() {
            if ( !( --count ) ) {
                defer.resolveWith( elements, [ elements ] );
            }
        };

    if ( typeof type !== "string" ) {
        obj = type;
        type = undefined;
    }
    type = type || "fx";

    while ( i-- ) {
        tmp = dataPriv.get( elements[ i ], type + "queueHooks" );
        if ( tmp && tmp.empty ) {
            count++;
            tmp.empty.add( resolve );
        }
    }
    resolve();
    return defer.promise( obj );
}
} );
var pnum = ( /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/ ).source;

var rcssNum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" );


var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var documentElement = document.documentElement;



var isAttached = function( elem ) {
        return jQuery.contains( elem.ownerDocument, elem );
    },
    composed = { composed: true };

// Support: IE 9 - 11+, Edge 12 - 18+, iOS 10.0 - 10.2 only
// Check attachment across shadow DOM boundaries when possible (gh-3504)
// Support: iOS 10.0-10.2 only
// Early iOS 10 versions support `attachShadow` but not `getRootNode`,
// leading to errors. We need to check for `getRootNode`.
if ( documentElement.getRootNode ) {
    isAttached = function( elem ) {
        return jQuery.contains( elem.ownerDocument, elem ) ||
            elem.getRootNode( composed ) === elem.ownerDocument;
    };
}
var isHiddenWithinTree = function( elem, el ) {

    // isHiddenWithinTree might be called from jQuery#filter function;
    // in that case, element will be second argument
    elem = el || elem;

    // Inline style trumps all
    return elem.style.display === "none" ||
        elem.style.display === "" &&

        // Otherwise, check computed style
        // Support: Firefox <=43 - 45
        // Disconnected elements can have computed display: none, so first confirm that elem is
        // in the document.
        isAttached( elem ) &&

        jQuery.css( elem, "display" ) === "none";
};



function adjustCSS( elem, prop, valueParts, tween ) {
var adjusted, scale,
    maxIterations = 20,
    currentValue = tween ?
        function() {
            return tween.cur();
        } :
        function() {
            return jQuery.css( elem, prop, "" );
        },
    initial = currentValue(),
    unit = valueParts && valueParts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

    // Starting value computation is required for potential unit mismatches
    initialInUnit = elem.nodeType &&
        ( jQuery.cssNumber[ prop ] || unit !== "px" && +initial ) &&
        rcssNum.exec( jQuery.css( elem, prop ) );

if ( initialInUnit && initialInUnit[ 3 ] !== unit ) {

    // Support: Firefox <=54
    // Halve the iteration target value to prevent interference from CSS upper bounds (gh-2144)
    initial = initial / 2;

    // Trust units reported by jQuery.css
    unit = unit || initialInUnit[ 3 ];

    // Iteratively approximate from a nonzero starting point
    initialInUnit = +initial || 1;

    while ( maxIterations-- ) {

        // Evaluate and update our best guess (doubling guesses that zero out).
        // Finish if the scale equals or crosses 1 (making the old*new product non-positive).
        jQuery.style( elem, prop, initialInUnit + unit );
        if ( ( 1 - scale ) * ( 1 - ( scale = currentValue() / initial || 0.5 ) ) <= 0 ) {
            maxIterations = 0;
        }
        initialInUnit = initialInUnit / scale;

    }

    initialInUnit = initialInUnit * 2;
    jQuery.style( elem, prop, initialInUnit + unit );

    // Make sure we update the tween properties later on
    valueParts = valueParts || [];
}

if ( valueParts ) {
    initialInUnit = +initialInUnit || +initial || 0;

    // Apply relative offset (+=/-=) if specified
    adjusted = valueParts[ 1 ] ?
        initialInUnit + ( valueParts[ 1 ] + 1 ) * valueParts[ 2 ] :
        +valueParts[ 2 ];
    if ( tween ) {
        tween.unit = unit;
        tween.start = initialInUnit;
        tween.end = adjusted;
    }
}
return adjusted;
}


var defaultDisplayMap = {};

function getDefaultDisplay( elem ) {
var temp,
    doc = elem.ownerDocument,
    nodeName = elem.nodeName,
    display = defaultDisplayMap[ nodeName ];

if ( display ) {
    return display;
}

temp = doc.body.appendChild( doc.createElement( nodeName ) );
display = jQuery.css( temp, "display" );

temp.parentNode.removeChild( temp );

if ( display === "none" ) {
    display = "block";
}
defaultDisplayMap[ nodeName ] = display;

return display;
}

function showHide( elements, show ) {
var display, elem,
    values = [],
    index = 0,
    length = elements.length;

// Determine new display value for elements that need to change
for ( ; index < length; index++ ) {
    elem = elements[ index ];
    if ( !elem.style ) {
        continue;
    }

    display = elem.style.display;
    if ( show ) {

        // Since we force visibility upon cascade-hidden elements, an immediate (and slow)
        // check is required in this first loop unless we have a nonempty display value (either
        // inline or about-to-be-restored)
        if ( display === "none" ) {
            values[ index ] = dataPriv.get( elem, "display" ) || null;
            if ( !values[ index ] ) {
                elem.style.display = "";
            }
        }
        if ( elem.style.display === "" && isHiddenWithinTree( elem ) ) {
            values[ index ] = getDefaultDisplay( elem );
        }
    } else {
        if ( display !== "none" ) {
            values[ index ] = "none";

            // Remember what we're overwriting
            dataPriv.set( elem, "display", display );
        }
    }
}

// Set the display of the elements in a second loop to avoid constant reflow
for ( index = 0; index < length; index++ ) {
    if ( values[ index ] != null ) {
        elements[ index ].style.display = values[ index ];
    }
}

return elements;
}

jQuery.fn.extend( {
show: function() {
    return showHide( this, true );
},
hide: function() {
    return showHide( this );
},
toggle: function( state ) {
    if ( typeof state === "boolean" ) {
        return state ? this.show() : this.hide();
    }

    return this.each( function() {
        if ( isHiddenWithinTree( this ) ) {
            jQuery( this ).show();
        } else {
            jQuery( this ).hide();
        }
    } );
}
} );
var rcheckableType = ( /^(?:checkbox|radio)$/i );

var rtagName = ( /<([a-z][^\/\0>\x20\t\r\n\f]*)/i );

var rscriptType = ( /^$|^module$|\/(?:java|ecma)script/i );



( function() {
var fragment = document.createDocumentFragment(),
    div = fragment.appendChild( document.createElement( "div" ) ),
    input = document.createElement( "input" );

// Support: Android 4.0 - 4.3 only
// Check state lost if the name is set (trac-11217)
// Support: Windows Web Apps (WWA)
// `name` and `type` must use .setAttribute for WWA (trac-14901)
input.setAttribute( "type", "radio" );
input.setAttribute( "checked", "checked" );
input.setAttribute( "name", "t" );

div.appendChild( input );

// Support: Android <=4.1 only
// Older WebKit doesn't clone checked state correctly in fragments
support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

// Support: IE <=11 only
// Make sure textarea (and checkbox) defaultValue is properly cloned
div.innerHTML = "<textarea>x</textarea>";
support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;

// Support: IE <=9 only
// IE <=9 replaces <option> tags with their contents when inserted outside of
// the select element.
div.innerHTML = "<option></option>";
support.option = !!div.lastChild;
} )();


// We have to close these tags to support XHTML (trac-13200)
var wrapMap = {

// XHTML parsers do not magically insert elements in the
// same way that tag soup parsers do. So we cannot shorten
// this by omitting <tbody> or other required elements.
thead: [ 1, "<table>", "</table>" ],
col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
tr: [ 2, "<table><tbody>", "</tbody></table>" ],
td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

_default: [ 0, "", "" ]
};

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;

// Support: IE <=9 only
if ( !support.option ) {
wrapMap.optgroup = wrapMap.option = [ 1, "<select multiple='multiple'>", "</select>" ];
}


function getAll( context, tag ) {

// Support: IE <=9 - 11 only
// Use typeof to avoid zero-argument method invocation on host objects (trac-15151)
var ret;

if ( typeof context.getElementsByTagName !== "undefined" ) {
    ret = context.getElementsByTagName( tag || "*" );

} else if ( typeof context.querySelectorAll !== "undefined" ) {
    ret = context.querySelectorAll( tag || "*" );

} else {
    ret = [];
}

if ( tag === undefined || tag && nodeName( context, tag ) ) {
    return jQuery.merge( [ context ], ret );
}

return ret;
}


// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
var i = 0,
    l = elems.length;

for ( ; i < l; i++ ) {
    dataPriv.set(
        elems[ i ],
        "globalEval",
        !refElements || dataPriv.get( refElements[ i ], "globalEval" )
    );
}
}


var rhtml = /<|&#?\w+;/;

function buildFragment( elems, context, scripts, selection, ignored ) {
var elem, tmp, tag, wrap, attached, j,
    fragment = context.createDocumentFragment(),
    nodes = [],
    i = 0,
    l = elems.length;

for ( ; i < l; i++ ) {
    elem = elems[ i ];

    if ( elem || elem === 0 ) {

        // Add nodes directly
        if ( toType( elem ) === "object" ) {

            // Support: Android <=4.0 only, PhantomJS 1 only
            // push.apply(_, arraylike) throws on ancient WebKit
            jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

        // Convert non-html into a text node
        } else if ( !rhtml.test( elem ) ) {
            nodes.push( context.createTextNode( elem ) );

        // Convert html into DOM nodes
        } else {
            tmp = tmp || fragment.appendChild( context.createElement( "div" ) );

            // Deserialize a standard representation
            tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
            wrap = wrapMap[ tag ] || wrapMap._default;
            tmp.innerHTML = wrap[ 1 ] + jQuery.htmlPrefilter( elem ) + wrap[ 2 ];

            // Descend through wrappers to the right content
            j = wrap[ 0 ];
            while ( j-- ) {
                tmp = tmp.lastChild;
            }

            // Support: Android <=4.0 only, PhantomJS 1 only
            // push.apply(_, arraylike) throws on ancient WebKit
            jQuery.merge( nodes, tmp.childNodes );

            // Remember the top-level container
            tmp = fragment.firstChild;

            // Ensure the created nodes are orphaned (trac-12392)
            tmp.textContent = "";
        }
    }
}

// Remove wrapper from fragment
fragment.textContent = "";

i = 0;
while ( ( elem = nodes[ i++ ] ) ) {

    // Skip elements already in the context collection (trac-4087)
    if ( selection && jQuery.inArray( elem, selection ) > -1 ) {
        if ( ignored ) {
            ignored.push( elem );
        }
        continue;
    }

    attached = isAttached( elem );

    // Append to fragment
    tmp = getAll( fragment.appendChild( elem ), "script" );

    // Preserve script evaluation history
    if ( attached ) {
        setGlobalEval( tmp );
    }

    // Capture executables
    if ( scripts ) {
        j = 0;
        while ( ( elem = tmp[ j++ ] ) ) {
            if ( rscriptType.test( elem.type || "" ) ) {
                scripts.push( elem );
            }
        }
    }
}

return fragment;
}


var rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

function returnTrue() {
return true;
}

function returnFalse() {
return false;
}

// Support: IE <=9 - 11+
// focus() and blur() are asynchronous, except when they are no-op.
// So expect focus to be synchronous when the element is already active,
// and blur to be synchronous when the element is not already active.
// (focus and blur are always synchronous in other supported browsers,
// this just defines when we can count on it).
function expectSync( elem, type ) {
return ( elem === safeActiveElement() ) === ( type === "focus" );
}

// Support: IE <=9 only
// Accessing document.activeElement can throw unexpectedly
// https://bugs.jquery.com/ticket/13393
function safeActiveElement() {
try {
    return document.activeElement;
} catch ( err ) { }
}

function on( elem, types, selector, data, fn, one ) {
var origFn, type;

// Types can be a map of types/handlers
if ( typeof types === "object" ) {

    // ( types-Object, selector, data )
    if ( typeof selector !== "string" ) {

        // ( types-Object, data )
        data = data || selector;
        selector = undefined;
    }
    for ( type in types ) {
        on( elem, type, selector, data, types[ type ], one );
    }
    return elem;
}

if ( data == null && fn == null ) {

    // ( types, fn )
    fn = selector;
    data = selector = undefined;
} else if ( fn == null ) {
    if ( typeof selector === "string" ) {

        // ( types, selector, fn )
        fn = data;
        data = undefined;
    } else {

        // ( types, data, fn )
        fn = data;
        data = selector;
        selector = undefined;
    }
}
if ( fn === false ) {
    fn = returnFalse;
} else if ( !fn ) {
    return elem;
}

if ( one === 1 ) {
    origFn = fn;
    fn = function( event ) {

        // Can use an empty set, since event contains the info
        jQuery().off( event );
        return origFn.apply( this, arguments );
    };

    // Use same guid so caller can remove using origFn
    fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
}
return elem.each( function() {
    jQuery.event.add( this, types, fn, data, selector );
} );
}

/*
* Helper functions for managing events -- not part of the public interface.
* Props to Dean Edwards' addEvent library for many of the ideas.
*/
jQuery.event = {

global: {},

add: function( elem, types, handler, data, selector ) {

    var handleObjIn, eventHandle, tmp,
        events, t, handleObj,
        special, handlers, type, namespaces, origType,
        elemData = dataPriv.get( elem );

    // Only attach events to objects that accept data
    if ( !acceptData( elem ) ) {
        return;
    }

    // Caller can pass in an object of custom data in lieu of the handler
    if ( handler.handler ) {
        handleObjIn = handler;
        handler = handleObjIn.handler;
        selector = handleObjIn.selector;
    }

    // Ensure that invalid selectors throw exceptions at attach time
    // Evaluate against documentElement in case elem is a non-element node (e.g., document)
    if ( selector ) {
        jQuery.find.matchesSelector( documentElement, selector );
    }

    // Make sure that the handler has a unique ID, used to find/remove it later
    if ( !handler.guid ) {
        handler.guid = jQuery.guid++;
    }

    // Init the element's event structure and main handler, if this is the first
    if ( !( events = elemData.events ) ) {
        events = elemData.events = Object.create( null );
    }
    if ( !( eventHandle = elemData.handle ) ) {
        eventHandle = elemData.handle = function( e ) {

            // Discard the second event of a jQuery.event.trigger() and
            // when an event is called after a page has unloaded
            return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ?
                jQuery.event.dispatch.apply( elem, arguments ) : undefined;
        };
    }

    // Handle multiple events separated by a space
    types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
    t = types.length;
    while ( t-- ) {
        tmp = rtypenamespace.exec( types[ t ] ) || [];
        type = origType = tmp[ 1 ];
        namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

        // There *must* be a type, no attaching namespace-only handlers
        if ( !type ) {
            continue;
        }

        // If event changes its type, use the special event handlers for the changed type
        special = jQuery.event.special[ type ] || {};

        // If selector defined, determine special event api type, otherwise given type
        type = ( selector ? special.delegateType : special.bindType ) || type;

        // Update special based on newly reset type
        special = jQuery.event.special[ type ] || {};

        // handleObj is passed to all event handlers
        handleObj = jQuery.extend( {
            type: type,
            origType: origType,
            data: data,
            handler: handler,
            guid: handler.guid,
            selector: selector,
            needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
            namespace: namespaces.join( "." )
        }, handleObjIn );

        // Init the event handler queue if we're the first
        if ( !( handlers = events[ type ] ) ) {
            handlers = events[ type ] = [];
            handlers.delegateCount = 0;

            // Only use addEventListener if the special events handler returns false
            if ( !special.setup ||
                special.setup.call( elem, data, namespaces, eventHandle ) === false ) {

                if ( elem.addEventListener ) {
                    elem.addEventListener( type, eventHandle );
                }
            }
        }

        if ( special.add ) {
            special.add.call( elem, handleObj );

            if ( !handleObj.handler.guid ) {
                handleObj.handler.guid = handler.guid;
            }
        }

        // Add to the element's handler list, delegates in front
        if ( selector ) {
            handlers.splice( handlers.delegateCount++, 0, handleObj );
        } else {
            handlers.push( handleObj );
        }

        // Keep track of which events have ever been used, for event optimization
        jQuery.event.global[ type ] = true;
    }

},

// Detach an event or set of events from an element
remove: function( elem, types, handler, selector, mappedTypes ) {

    var j, origCount, tmp,
        events, t, handleObj,
        special, handlers, type, namespaces, origType,
        elemData = dataPriv.hasData( elem ) && dataPriv.get( elem );

    if ( !elemData || !( events = elemData.events ) ) {
        return;
    }

    // Once for each type.namespace in types; type may be omitted
    types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
    t = types.length;
    while ( t-- ) {
        tmp = rtypenamespace.exec( types[ t ] ) || [];
        type = origType = tmp[ 1 ];
        namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

        // Unbind all events (on this namespace, if provided) for the element
        if ( !type ) {
            for ( type in events ) {
                jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
            }
            continue;
        }

        special = jQuery.event.special[ type ] || {};
        type = ( selector ? special.delegateType : special.bindType ) || type;
        handlers = events[ type ] || [];
        tmp = tmp[ 2 ] &&
            new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" );

        // Remove matching events
        origCount = j = handlers.length;
        while ( j-- ) {
            handleObj = handlers[ j ];

            if ( ( mappedTypes || origType === handleObj.origType ) &&
                ( !handler || handler.guid === handleObj.guid ) &&
                ( !tmp || tmp.test( handleObj.namespace ) ) &&
                ( !selector || selector === handleObj.selector ||
                    selector === "**" && handleObj.selector ) ) {
                handlers.splice( j, 1 );

                if ( handleObj.selector ) {
                    handlers.delegateCount--;
                }
                if ( special.remove ) {
                    special.remove.call( elem, handleObj );
                }
            }
        }

        // Remove generic event handler if we removed something and no more handlers exist
        // (avoids potential for endless recursion during removal of special event handlers)
        if ( origCount && !handlers.length ) {
            if ( !special.teardown ||
                special.teardown.call( elem, namespaces, elemData.handle ) === false ) {

                jQuery.removeEvent( elem, type, elemData.handle );
            }

            delete events[ type ];
        }
    }

    // Remove data and the expando if it's no longer used
    if ( jQuery.isEmptyObject( events ) ) {
        dataPriv.remove( elem, "handle events" );
    }
},

dispatch: function( nativeEvent ) {

    var i, j, ret, matched, handleObj, handlerQueue,
        args = new Array( arguments.length ),

        // Make a writable jQuery.Event from the native event object
        event = jQuery.event.fix( nativeEvent ),

        handlers = (
            dataPriv.get( this, "events" ) || Object.create( null )
        )[ event.type ] || [],
        special = jQuery.event.special[ event.type ] || {};

    // Use the fix-ed jQuery.Event rather than the (read-only) native event
    args[ 0 ] = event;

    for ( i = 1; i < arguments.length; i++ ) {
        args[ i ] = arguments[ i ];
    }

    event.delegateTarget = this;

    // Call the preDispatch hook for the mapped type, and let it bail if desired
    if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
        return;
    }

    // Determine handlers
    handlerQueue = jQuery.event.handlers.call( this, event, handlers );

    // Run delegates first; they may want to stop propagation beneath us
    i = 0;
    while ( ( matched = handlerQueue[ i++ ] ) && !event.isPropagationStopped() ) {
        event.currentTarget = matched.elem;

        j = 0;
        while ( ( handleObj = matched.handlers[ j++ ] ) &&
            !event.isImmediatePropagationStopped() ) {

            // If the event is namespaced, then each handler is only invoked if it is
            // specially universal or its namespaces are a superset of the event's.
            if ( !event.rnamespace || handleObj.namespace === false ||
                event.rnamespace.test( handleObj.namespace ) ) {

                event.handleObj = handleObj;
                event.data = handleObj.data;

                ret = ( ( jQuery.event.special[ handleObj.origType ] || {} ).handle ||
                    handleObj.handler ).apply( matched.elem, args );

                if ( ret !== undefined ) {
                    if ( ( event.result = ret ) === false ) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                }
            }
        }
    }

    // Call the postDispatch hook for the mapped type
    if ( special.postDispatch ) {
        special.postDispatch.call( this, event );
    }

    return event.result;
},

handlers: function( event, handlers ) {
    var i, handleObj, sel, matchedHandlers, matchedSelectors,
        handlerQueue = [],
        delegateCount = handlers.delegateCount,
        cur = event.target;

    // Find delegate handlers
    if ( delegateCount &&

        // Support: IE <=9
        // Black-hole SVG <use> instance trees (trac-13180)
        cur.nodeType &&

        // Support: Firefox <=42
        // Suppress spec-violating clicks indicating a non-primary pointer button (trac-3861)
        // https://www.w3.org/TR/DOM-Level-3-Events/#event-type-click
        // Support: IE 11 only
        // ...but not arrow key "clicks" of radio inputs, which can have `button` -1 (gh-2343)
        !( event.type === "click" && event.button >= 1 ) ) {

        for ( ; cur !== this; cur = cur.parentNode || this ) {

            // Don't check non-elements (trac-13208)
            // Don't process clicks on disabled elements (trac-6911, trac-8165, trac-11382, trac-11764)
            if ( cur.nodeType === 1 && !( event.type === "click" && cur.disabled === true ) ) {
                matchedHandlers = [];
                matchedSelectors = {};
                for ( i = 0; i < delegateCount; i++ ) {
                    handleObj = handlers[ i ];

                    // Don't conflict with Object.prototype properties (trac-13203)
                    sel = handleObj.selector + " ";

                    if ( matchedSelectors[ sel ] === undefined ) {
                        matchedSelectors[ sel ] = handleObj.needsContext ?
                            jQuery( sel, this ).index( cur ) > -1 :
                            jQuery.find( sel, this, null, [ cur ] ).length;
                    }
                    if ( matchedSelectors[ sel ] ) {
                        matchedHandlers.push( handleObj );
                    }
                }
                if ( matchedHandlers.length ) {
                    handlerQueue.push( { elem: cur, handlers: matchedHandlers } );
                }
            }
        }
    }

    // Add the remaining (directly-bound) handlers
    cur = this;
    if ( delegateCount < handlers.length ) {
        handlerQueue.push( { elem: cur, handlers: handlers.slice( delegateCount ) } );
    }

    return handlerQueue;
},

addProp: function( name, hook ) {
    Object.defineProperty( jQuery.Event.prototype, name, {
        enumerable: true,
        configurable: true,

        get: isFunction( hook ) ?
            function() {
                if ( this.originalEvent ) {
                    return hook( this.originalEvent );
                }
            } :
            function() {
                if ( this.originalEvent ) {
                    return this.originalEvent[ name ];
                }
            },

        set: function( value ) {
            Object.defineProperty( this, name, {
                enumerable: true,
                configurable: true,
                writable: true,
                value: value
            } );
        }
    } );
},

fix: function( originalEvent ) {
    return originalEvent[ jQuery.expando ] ?
        originalEvent :
        new jQuery.Event( originalEvent );
},

special: {
    load: {

        // Prevent triggered image.load events from bubbling to window.load
        noBubble: true
    },
    click: {

        // Utilize native event to ensure correct state for checkable inputs
        setup: function( data ) {

            // For mutual compressibility with _default, replace `this` access with a local var.
            // `|| data` is dead code meant only to preserve the variable through minification.
            var el = this || data;

            // Claim the first handler
            if ( rcheckableType.test( el.type ) &&
                el.click && nodeName( el, "input" ) ) {

                // dataPriv.set( el, "click", ... )
                leverageNative( el, "click", returnTrue );
            }

            // Return false to allow normal processing in the caller
            return false;
        },
        trigger: function( data ) {

            // For mutual compressibility with _default, replace `this` access with a local var.
            // `|| data` is dead code meant only to preserve the variable through minification.
            var el = this || data;

            // Force setup before triggering a click
            if ( rcheckableType.test( el.type ) &&
                el.click && nodeName( el, "input" ) ) {

                leverageNative( el, "click" );
            }

            // Return non-false to allow normal event-path propagation
            return true;
        },

        // For cross-browser consistency, suppress native .click() on links
        // Also prevent it if we're currently inside a leveraged native-event stack
        _default: function( event ) {
            var target = event.target;
            return rcheckableType.test( target.type ) &&
                target.click && nodeName( target, "input" ) &&
                dataPriv.get( target, "click" ) ||
                nodeName( target, "a" );
        }
    },

    beforeunload: {
        postDispatch: function( event ) {

            // Support: Firefox 20+
            // Firefox doesn't alert if the returnValue field is not set.
            if ( event.result !== undefined && event.originalEvent ) {
                event.originalEvent.returnValue = event.result;
            }
        }
    }
}
};

// Ensure the presence of an event listener that handles manually-triggered
// synthetic events by interrupting progress until reinvoked in response to
// *native* events that it fires directly, ensuring that state changes have
// already occurred before other listeners are invoked.
function leverageNative( el, type, expectSync ) {

// Missing expectSync indicates a trigger call, which must force setup through jQuery.event.add
if ( !expectSync ) {
    if ( dataPriv.get( el, type ) === undefined ) {
        jQuery.event.add( el, type, returnTrue );
    }
    return;
}

// Register the controller as a special universal handler for all event namespaces
dataPriv.set( el, type, false );
jQuery.event.add( el, type, {
    namespace: false,
    handler: function( event ) {
        var notAsync, result,
            saved = dataPriv.get( this, type );

        if ( ( event.isTrigger & 1 ) && this[ type ] ) {

            // Interrupt processing of the outer synthetic .trigger()ed event
            // Saved data should be false in such cases, but might be a leftover capture object
            // from an async native handler (gh-4350)
            if ( !saved.length ) {

                // Store arguments for use when handling the inner native event
                // There will always be at least one argument (an event object), so this array
                // will not be confused with a leftover capture object.
                saved = slice.call( arguments );
                dataPriv.set( this, type, saved );

                // Trigger the native event and capture its result
                // Support: IE <=9 - 11+
                // focus() and blur() are asynchronous
                notAsync = expectSync( this, type );
                this[ type ]();
                result = dataPriv.get( this, type );
                if ( saved !== result || notAsync ) {
                    dataPriv.set( this, type, false );
                } else {
                    result = {};
                }
                if ( saved !== result ) {

                    // Cancel the outer synthetic event
                    event.stopImmediatePropagation();
                    event.preventDefault();

                    // Support: Chrome 86+
                    // In Chrome, if an element having a focusout handler is blurred by
                    // clicking outside of it, it invokes the handler synchronously. If
                    // that handler calls `.remove()` on the element, the data is cleared,
                    // leaving `result` undefined. We need to guard against this.
                    return result && result.value;
                }

            // If this is an inner synthetic event for an event with a bubbling surrogate
            // (focus or blur), assume that the surrogate already propagated from triggering the
            // native event and prevent that from happening again here.
            // This technically gets the ordering wrong w.r.t. to `.trigger()` (in which the
            // bubbling surrogate propagates *after* the non-bubbling base), but that seems
            // less bad than duplication.
            } else if ( ( jQuery.event.special[ type ] || {} ).delegateType ) {
                event.stopPropagation();
            }

        // If this is a native event triggered above, everything is now in order
        // Fire an inner synthetic event with the original arguments
        } else if ( saved.length ) {

            // ...and capture the result
            dataPriv.set( this, type, {
                value: jQuery.event.trigger(

                    // Support: IE <=9 - 11+
                    // Extend with the prototype to reset the above stopImmediatePropagation()
                    jQuery.extend( saved[ 0 ], jQuery.Event.prototype ),
                    saved.slice( 1 ),
                    this
                )
            } );

            // Abort handling of the native event
            event.stopImmediatePropagation();
        }
    }
} );
}

jQuery.removeEvent = function( elem, type, handle ) {

// This "if" is needed for plain objects
if ( elem.removeEventListener ) {
    elem.removeEventListener( type, handle );
}
};

jQuery.Event = function( src, props ) {

// Allow instantiation without the 'new' keyword
if ( !( this instanceof jQuery.Event ) ) {
    return new jQuery.Event( src, props );
}

// Event object
if ( src && src.type ) {
    this.originalEvent = src;
    this.type = src.type;

    // Events bubbling up the document may have been marked as prevented
    // by a handler lower down the tree; reflect the correct value.
    this.isDefaultPrevented = src.defaultPrevented ||
            src.defaultPrevented === undefined &&

            // Support: Android <=2.3 only
            src.returnValue === false ?
        returnTrue :
        returnFalse;

    // Create target properties
    // Support: Safari <=6 - 7 only
    // Target should not be a text node (trac-504, trac-13143)
    this.target = ( src.target && src.target.nodeType === 3 ) ?
        src.target.parentNode :
        src.target;

    this.currentTarget = src.currentTarget;
    this.relatedTarget = src.relatedTarget;

// Event type
} else {
    this.type = src;
}

// Put explicitly provided properties onto the event object
if ( props ) {
    jQuery.extend( this, props );
}

// Create a timestamp if incoming event doesn't have one
this.timeStamp = src && src.timeStamp || Date.now();

// Mark it as fixed
this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// https://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
constructor: jQuery.Event,
isDefaultPrevented: returnFalse,
isPropagationStopped: returnFalse,
isImmediatePropagationStopped: returnFalse,
isSimulated: false,

preventDefault: function() {
    var e = this.originalEvent;

    this.isDefaultPrevented = returnTrue;

    if ( e && !this.isSimulated ) {
        e.preventDefault();
    }
},
stopPropagation: function() {
    var e = this.originalEvent;

    this.isPropagationStopped = returnTrue;

    if ( e && !this.isSimulated ) {
        e.stopPropagation();
    }
},
stopImmediatePropagation: function() {
    var e = this.originalEvent;

    this.isImmediatePropagationStopped = returnTrue;

    if ( e && !this.isSimulated ) {
        e.stopImmediatePropagation();
    }

    this.stopPropagation();
}
};

// Includes all common event props including KeyEvent and MouseEvent specific props
jQuery.each( {
altKey: true,
bubbles: true,
cancelable: true,
changedTouches: true,
ctrlKey: true,
detail: true,
eventPhase: true,
metaKey: true,
pageX: true,
pageY: true,
shiftKey: true,
view: true,
"char": true,
code: true,
charCode: true,
key: true,
keyCode: true,
button: true,
buttons: true,
clientX: true,
clientY: true,
offsetX: true,
offsetY: true,
pointerId: true,
pointerType: true,
screenX: true,
screenY: true,
targetTouches: true,
toElement: true,
touches: true,
which: true
}, jQuery.event.addProp );

jQuery.each( { focus: "focusin", blur: "focusout" }, function( type, delegateType ) {
jQuery.event.special[ type ] = {

    // Utilize native event if possible so blur/focus sequence is correct
    setup: function() {

        // Claim the first handler
        // dataPriv.set( this, "focus", ... )
        // dataPriv.set( this, "blur", ... )
        leverageNative( this, type, expectSync );

        // Return false to allow normal processing in the caller
        return false;
    },
    trigger: function() {

        // Force setup before trigger
        leverageNative( this, type );

        // Return non-false to allow normal event-path propagation
        return true;
    },

    // Suppress native focus or blur if we're currently inside
    // a leveraged native-event stack
    _default: function( event ) {
        return dataPriv.get( event.target, type );
    },

    delegateType: delegateType
};
} );

// Create mouseenter/leave events using mouseover/out and event-time checks
// so that event delegation works in jQuery.
// Do the same for pointerenter/pointerleave and pointerover/pointerout
//
// Support: Safari 7 only
// Safari sends mouseenter too often; see:
// https://bugs.chromium.org/p/chromium/issues/detail?id=470258
// for the description of the bug (it existed in older Chrome versions as well).
jQuery.each( {
mouseenter: "mouseover",
mouseleave: "mouseout",
pointerenter: "pointerover",
pointerleave: "pointerout"
}, function( orig, fix ) {
jQuery.event.special[ orig ] = {
    delegateType: fix,
    bindType: fix,

    handle: function( event ) {
        var ret,
            target = this,
            related = event.relatedTarget,
            handleObj = event.handleObj;

        // For mouseenter/leave call the handler if related is outside the target.
        // NB: No relatedTarget if the mouse left/entered the browser window
        if ( !related || ( related !== target && !jQuery.contains( target, related ) ) ) {
            event.type = handleObj.origType;
            ret = handleObj.handler.apply( this, arguments );
            event.type = fix;
        }
        return ret;
    }
};
} );

jQuery.fn.extend( {

on: function( types, selector, data, fn ) {
    return on( this, types, selector, data, fn );
},
one: function( types, selector, data, fn ) {
    return on( this, types, selector, data, fn, 1 );
},
off: function( types, selector, fn ) {
    var handleObj, type;
    if ( types && types.preventDefault && types.handleObj ) {

        // ( event )  dispatched jQuery.Event
        handleObj = types.handleObj;
        jQuery( types.delegateTarget ).off(
            handleObj.namespace ?
                handleObj.origType + "." + handleObj.namespace :
                handleObj.origType,
            handleObj.selector,
            handleObj.handler
        );
        return this;
    }
    if ( typeof types === "object" ) {

        // ( types-object [, selector] )
        for ( type in types ) {
            this.off( type, selector, types[ type ] );
        }
        return this;
    }
    if ( selector === false || typeof selector === "function" ) {

        // ( types [, fn] )
        fn = selector;
        selector = undefined;
    }
    if ( fn === false ) {
        fn = returnFalse;
    }
    return this.each( function() {
        jQuery.event.remove( this, types, fn, selector );
    } );
}
} );


var

// Support: IE <=10 - 11, Edge 12 - 13 only
// In IE/Edge using regex groups here causes severe slowdowns.
// See https://connect.microsoft.com/IE/feedback/details/1736512/
rnoInnerhtml = /<script|<style|<link/i,

// checked="checked" or checked
rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,

rcleanScript = /^\s*<!\[CDATA\[|\]\]>\s*$/g;

// Prefer a tbody over its parent table for containing new rows
function manipulationTarget( elem, content ) {
if ( nodeName( elem, "table" ) &&
    nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ) {

    return jQuery( elem ).children( "tbody" )[ 0 ] || elem;
}

return elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
elem.type = ( elem.getAttribute( "type" ) !== null ) + "/" + elem.type;
return elem;
}
function restoreScript( elem ) {
if ( ( elem.type || "" ).slice( 0, 5 ) === "true/" ) {
    elem.type = elem.type.slice( 5 );
} else {
    elem.removeAttribute( "type" );
}

return elem;
}

function cloneCopyEvent( src, dest ) {
var i, l, type, pdataOld, udataOld, udataCur, events;

if ( dest.nodeType !== 1 ) {
    return;
}

// 1. Copy private data: events, handlers, etc.
if ( dataPriv.hasData( src ) ) {
    pdataOld = dataPriv.get( src );
    events = pdataOld.events;

    if ( events ) {
        dataPriv.remove( dest, "handle events" );

        for ( type in events ) {
            for ( i = 0, l = events[ type ].length; i < l; i++ ) {
                jQuery.event.add( dest, type, events[ type ][ i ] );
            }
        }
    }
}

// 2. Copy user data
if ( dataUser.hasData( src ) ) {
    udataOld = dataUser.access( src );
    udataCur = jQuery.extend( {}, udataOld );

    dataUser.set( dest, udataCur );
}
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
var nodeName = dest.nodeName.toLowerCase();

// Fails to persist the checked state of a cloned checkbox or radio button.
if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
    dest.checked = src.checked;

// Fails to return the selected option to the default selected state when cloning options
} else if ( nodeName === "input" || nodeName === "textarea" ) {
    dest.defaultValue = src.defaultValue;
}
}

function domManip( collection, args, callback, ignored ) {

// Flatten any nested arrays
args = flat( args );

var fragment, first, scripts, hasScripts, node, doc,
    i = 0,
    l = collection.length,
    iNoClone = l - 1,
    value = args[ 0 ],
    valueIsFunction = isFunction( value );

// We can't cloneNode fragments that contain checked, in WebKit
if ( valueIsFunction ||
        ( l > 1 && typeof value === "string" &&
            !support.checkClone && rchecked.test( value ) ) ) {
    return collection.each( function( index ) {
        var self = collection.eq( index );
        if ( valueIsFunction ) {
            args[ 0 ] = value.call( this, index, self.html() );
        }
        domManip( self, args, callback, ignored );
    } );
}

if ( l ) {
    fragment = buildFragment( args, collection[ 0 ].ownerDocument, false, collection, ignored );
    first = fragment.firstChild;

    if ( fragment.childNodes.length === 1 ) {
        fragment = first;
    }

    // Require either new content or an interest in ignored elements to invoke the callback
    if ( first || ignored ) {
        scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
        hasScripts = scripts.length;

        // Use the original fragment for the last item
        // instead of the first because it can end up
        // being emptied incorrectly in certain situations (trac-8070).
        for ( ; i < l; i++ ) {
            node = fragment;

            if ( i !== iNoClone ) {
                node = jQuery.clone( node, true, true );

                // Keep references to cloned scripts for later restoration
                if ( hasScripts ) {

                    // Support: Android <=4.0 only, PhantomJS 1 only
                    // push.apply(_, arraylike) throws on ancient WebKit
                    jQuery.merge( scripts, getAll( node, "script" ) );
                }
            }

            callback.call( collection[ i ], node, i );
        }

        if ( hasScripts ) {
            doc = scripts[ scripts.length - 1 ].ownerDocument;

            // Reenable scripts
            jQuery.map( scripts, restoreScript );

            // Evaluate executable scripts on first document insertion
            for ( i = 0; i < hasScripts; i++ ) {
                node = scripts[ i ];
                if ( rscriptType.test( node.type || "" ) &&
                    !dataPriv.access( node, "globalEval" ) &&
                    jQuery.contains( doc, node ) ) {

                    if ( node.src && ( node.type || "" ).toLowerCase()  !== "module" ) {

                        // Optional AJAX dependency, but won't run scripts if not present
                        if ( jQuery._evalUrl && !node.noModule ) {
                            jQuery._evalUrl( node.src, {
                                nonce: node.nonce || node.getAttribute( "nonce" )
                            }, doc );
                        }
                    } else {

                        // Unwrap a CDATA section containing script contents. This shouldn't be
                        // needed as in XML documents they're already not visible when
                        // inspecting element contents and in HTML documents they have no
                        // meaning but we're preserving that logic for backwards compatibility.
                        // This will be removed completely in 4.0. See gh-4904.
                        DOMEval( node.textContent.replace( rcleanScript, "" ), node, doc );
                    }
                }
            }
        }
    }
}

return collection;
}

function remove( elem, selector, keepData ) {
var node,
    nodes = selector ? jQuery.filter( selector, elem ) : elem,
    i = 0;

for ( ; ( node = nodes[ i ] ) != null; i++ ) {
    if ( !keepData && node.nodeType === 1 ) {
        jQuery.cleanData( getAll( node ) );
    }

    if ( node.parentNode ) {
        if ( keepData && isAttached( node ) ) {
            setGlobalEval( getAll( node, "script" ) );
        }
        node.parentNode.removeChild( node );
    }
}

return elem;
}

jQuery.extend( {
htmlPrefilter: function( html ) {
    return html;
},

clone: function( elem, dataAndEvents, deepDataAndEvents ) {
    var i, l, srcElements, destElements,
        clone = elem.cloneNode( true ),
        inPage = isAttached( elem );

    // Fix IE cloning issues
    if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
            !jQuery.isXMLDoc( elem ) ) {

        // We eschew Sizzle here for performance reasons: https://jsperf.com/getall-vs-sizzle/2
        destElements = getAll( clone );
        srcElements = getAll( elem );

        for ( i = 0, l = srcElements.length; i < l; i++ ) {
            fixInput( srcElements[ i ], destElements[ i ] );
        }
    }

    // Copy the events from the original to the clone
    if ( dataAndEvents ) {
        if ( deepDataAndEvents ) {
            srcElements = srcElements || getAll( elem );
            destElements = destElements || getAll( clone );

            for ( i = 0, l = srcElements.length; i < l; i++ ) {
                cloneCopyEvent( srcElements[ i ], destElements[ i ] );
            }
        } else {
            cloneCopyEvent( elem, clone );
        }
    }

    // Preserve script evaluation history
    destElements = getAll( clone, "script" );
    if ( destElements.length > 0 ) {
        setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
    }

    // Return the cloned set
    return clone;
},

cleanData: function( elems ) {
    var data, elem, type,
        special = jQuery.event.special,
        i = 0;

    for ( ; ( elem = elems[ i ] ) !== undefined; i++ ) {
        if ( acceptData( elem ) ) {
            if ( ( data = elem[ dataPriv.expando ] ) ) {
                if ( data.events ) {
                    for ( type in data.events ) {
                        if ( special[ type ] ) {
                            jQuery.event.remove( elem, type );

                        // This is a shortcut to avoid jQuery.event.remove's overhead
                        } else {
                            jQuery.removeEvent( elem, type, data.handle );
                        }
                    }
                }

                // Support: Chrome <=35 - 45+
                // Assign undefined instead of using delete, see Data#remove
                elem[ dataPriv.expando ] = undefined;
            }
            if ( elem[ dataUser.expando ] ) {

                // Support: Chrome <=35 - 45+
                // Assign undefined instead of using delete, see Data#remove
                elem[ dataUser.expando ] = undefined;
            }
        }
    }
}
} );

jQuery.fn.extend( {
detach: function( selector ) {
    return remove( this, selector, true );
},

remove: function( selector ) {
    return remove( this, selector );
},

text: function( value ) {
    return access( this, function( value ) {
        return value === undefined ?
            jQuery.text( this ) :
            this.empty().each( function() {
                if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
                    this.textContent = value;
                }
            } );
    }, null, value, arguments.length );
},

append: function() {
    return domManip( this, arguments, function( elem ) {
        if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
            var target = manipulationTarget( this, elem );
            target.appendChild( elem );
        }
    } );
},

prepend: function() {
    return domManip( this, arguments, function( elem ) {
        if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
            var target = manipulationTarget( this, elem );
            target.insertBefore( elem, target.firstChild );
        }
    } );
},

before: function() {
    return domManip( this, arguments, function( elem ) {
        if ( this.parentNode ) {
            this.parentNode.insertBefore( elem, this );
        }
    } );
},

after: function() {
    return domManip( this, arguments, function( elem ) {
        if ( this.parentNode ) {
            this.parentNode.insertBefore( elem, this.nextSibling );
        }
    } );
},

empty: function() {
    var elem,
        i = 0;

    for ( ; ( elem = this[ i ] ) != null; i++ ) {
        if ( elem.nodeType === 1 ) {

            // Prevent memory leaks
            jQuery.cleanData( getAll( elem, false ) );

            // Remove any remaining nodes
            elem.textContent = "";
        }
    }

    return this;
},

clone: function( dataAndEvents, deepDataAndEvents ) {
    dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
    deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

    return this.map( function() {
        return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
    } );
},

html: function( value ) {
    return access( this, function( value ) {
        var elem = this[ 0 ] || {},
            i = 0,
            l = this.length;

        if ( value === undefined && elem.nodeType === 1 ) {
            return elem.innerHTML;
        }

        // See if we can take a shortcut and just use innerHTML
        if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
            !wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

            value = jQuery.htmlPrefilter( value );

            try {
                for ( ; i < l; i++ ) {
                    elem = this[ i ] || {};

                    // Remove element nodes and prevent memory leaks
                    if ( elem.nodeType === 1 ) {
                        jQuery.cleanData( getAll( elem, false ) );
                        elem.innerHTML = value;
                    }
                }

                elem = 0;

            // If using innerHTML throws an exception, use the fallback method
            } catch ( e ) {}
        }

        if ( elem ) {
            this.empty().append( value );
        }
    }, null, value, arguments.length );
},

replaceWith: function() {
    var ignored = [];

    // Make the changes, replacing each non-ignored context element with the new content
    return domManip( this, arguments, function( elem ) {
        var parent = this.parentNode;

        if ( jQuery.inArray( this, ignored ) < 0 ) {
            jQuery.cleanData( getAll( this ) );
            if ( parent ) {
                parent.replaceChild( elem, this );
            }
        }

    // Force callback invocation
    }, ignored );
}
} );

jQuery.each( {
appendTo: "append",
prependTo: "prepend",
insertBefore: "before",
insertAfter: "after",
replaceAll: "replaceWith"
}, function( name, original ) {
jQuery.fn[ name ] = function( selector ) {
    var elems,
        ret = [],
        insert = jQuery( selector ),
        last = insert.length - 1,
        i = 0;

    for ( ; i <= last; i++ ) {
        elems = i === last ? this : this.clone( true );
        jQuery( insert[ i ] )[ original ]( elems );

        // Support: Android <=4.0 only, PhantomJS 1 only
        // .get() because push.apply(_, arraylike) throws on ancient WebKit
        push.apply( ret, elems.get() );
    }

    return this.pushStack( ret );
};
} );
var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var rcustomProp = /^--/;


var getStyles = function( elem ) {

    // Support: IE <=11 only, Firefox <=30 (trac-15098, trac-14150)
    // IE throws on elements created in popups
    // FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
    var view = elem.ownerDocument.defaultView;

    if ( !view || !view.opener ) {
        view = window;
    }

    return view.getComputedStyle( elem );
};

var swap = function( elem, options, callback ) {
var ret, name,
    old = {};

// Remember the old values, and insert the new ones
for ( name in options ) {
    old[ name ] = elem.style[ name ];
    elem.style[ name ] = options[ name ];
}

ret = callback.call( elem );

// Revert the old values
for ( name in options ) {
    elem.style[ name ] = old[ name ];
}

return ret;
};


var rboxStyle = new RegExp( cssExpand.join( "|" ), "i" );

var whitespace = "[\\x20\\t\\r\\n\\f]";


var rtrimCSS = new RegExp(
"^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$",
"g"
);




( function() {

// Executing both pixelPosition & boxSizingReliable tests require only one layout
// so they're executed at the same time to save the second computation.
function computeStyleTests() {

    // This is a singleton, we need to execute it only once
    if ( !div ) {
        return;
    }

    container.style.cssText = "position:absolute;left:-11111px;width:60px;" +
        "margin-top:1px;padding:0;border:0";
    div.style.cssText =
        "position:relative;display:block;box-sizing:border-box;overflow:scroll;" +
        "margin:auto;border:1px;padding:1px;" +
        "width:60%;top:1%";
    documentElement.appendChild( container ).appendChild( div );

    var divStyle = window.getComputedStyle( div );
    pixelPositionVal = divStyle.top !== "1%";

    // Support: Android 4.0 - 4.3 only, Firefox <=3 - 44
    reliableMarginLeftVal = roundPixelMeasures( divStyle.marginLeft ) === 12;

    // Support: Android 4.0 - 4.3 only, Safari <=9.1 - 10.1, iOS <=7.0 - 9.3
    // Some styles come back with percentage values, even though they shouldn't
    div.style.right = "60%";
    pixelBoxStylesVal = roundPixelMeasures( divStyle.right ) === 36;

    // Support: IE 9 - 11 only
    // Detect misreporting of content dimensions for box-sizing:border-box elements
    boxSizingReliableVal = roundPixelMeasures( divStyle.width ) === 36;

    // Support: IE 9 only
    // Detect overflow:scroll screwiness (gh-3699)
    // Support: Chrome <=64
    // Don't get tricked when zoom affects offsetWidth (gh-4029)
    div.style.position = "absolute";
    scrollboxSizeVal = roundPixelMeasures( div.offsetWidth / 3 ) === 12;

    documentElement.removeChild( container );

    // Nullify the div so it wouldn't be stored in the memory and
    // it will also be a sign that checks already performed
    div = null;
}

function roundPixelMeasures( measure ) {
    return Math.round( parseFloat( measure ) );
}

var pixelPositionVal, boxSizingReliableVal, scrollboxSizeVal, pixelBoxStylesVal,
    reliableTrDimensionsVal, reliableMarginLeftVal,
    container = document.createElement( "div" ),
    div = document.createElement( "div" );

// Finish early in limited (non-browser) environments
if ( !div.style ) {
    return;
}

// Support: IE <=9 - 11 only
// Style of cloned element affects source element cloned (trac-8908)
div.style.backgroundClip = "content-box";
div.cloneNode( true ).style.backgroundClip = "";
support.clearCloneStyle = div.style.backgroundClip === "content-box";

jQuery.extend( support, {
    boxSizingReliable: function() {
        computeStyleTests();
        return boxSizingReliableVal;
    },
    pixelBoxStyles: function() {
        computeStyleTests();
        return pixelBoxStylesVal;
    },
    pixelPosition: function() {
        computeStyleTests();
        return pixelPositionVal;
    },
    reliableMarginLeft: function() {
        computeStyleTests();
        return reliableMarginLeftVal;
    },
    scrollboxSize: function() {
        computeStyleTests();
        return scrollboxSizeVal;
    },

    // Support: IE 9 - 11+, Edge 15 - 18+
    // IE/Edge misreport `getComputedStyle` of table rows with width/height
    // set in CSS while `offset*` properties report correct values.
    // Behavior in IE 9 is more subtle than in newer versions & it passes
    // some versions of this test; make sure not to make it pass there!
    //
    // Support: Firefox 70+
    // Only Firefox includes border widths
    // in computed dimensions. (gh-4529)
    reliableTrDimensions: function() {
        var table, tr, trChild, trStyle;
        if ( reliableTrDimensionsVal == null ) {
            table = document.createElement( "table" );
            tr = document.createElement( "tr" );
            trChild = document.createElement( "div" );

            table.style.cssText = "position:absolute;left:-11111px;border-collapse:separate";
            tr.style.cssText = "border:1px solid";

            // Support: Chrome 86+
            // Height set through cssText does not get applied.
            // Computed height then comes back as 0.
            tr.style.height = "1px";
            trChild.style.height = "9px";

            // Support: Android 8 Chrome 86+
            // In our bodyBackground.html iframe,
            // display for all div elements is set to "inline",
            // which causes a problem only in Android 8 Chrome 86.
            // Ensuring the div is display: block
            // gets around this issue.
            trChild.style.display = "block";

            documentElement
                .appendChild( table )
                .appendChild( tr )
                .appendChild( trChild );

            trStyle = window.getComputedStyle( tr );
            reliableTrDimensionsVal = ( parseInt( trStyle.height, 10 ) +
                parseInt( trStyle.borderTopWidth, 10 ) +
                parseInt( trStyle.borderBottomWidth, 10 ) ) === tr.offsetHeight;

            documentElement.removeChild( table );
        }
        return reliableTrDimensionsVal;
    }
} );
} )();


function curCSS( elem, name, computed ) {
var width, minWidth, maxWidth, ret,
    isCustomProp = rcustomProp.test( name ),

    // Support: Firefox 51+
    // Retrieving style before computed somehow
    // fixes an issue with getting wrong values
    // on detached elements
    style = elem.style;

computed = computed || getStyles( elem );

// getPropertyValue is needed for:
//   .css('filter') (IE 9 only, trac-12537)
//   .css('--customProperty) (gh-3144)
if ( computed ) {
    ret = computed.getPropertyValue( name ) || computed[ name ];

    // trim whitespace for custom property (issue gh-4926)
    if ( isCustomProp ) {

        // rtrim treats U+000D CARRIAGE RETURN and U+000C FORM FEED
        // as whitespace while CSS does not, but this is not a problem
        // because CSS preprocessing replaces them with U+000A LINE FEED
        // (which *is* CSS whitespace)
        // https://www.w3.org/TR/css-syntax-3/#input-preprocessing
        ret = ret.replace( rtrimCSS, "$1" );
    }

    if ( ret === "" && !isAttached( elem ) ) {
        ret = jQuery.style( elem, name );
    }

    // A tribute to the "awesome hack by Dean Edwards"
    // Android Browser returns percentage for some values,
    // but width seems to be reliably pixels.
    // This is against the CSSOM draft spec:
    // https://drafts.csswg.org/cssom/#resolved-values
    if ( !support.pixelBoxStyles() && rnumnonpx.test( ret ) && rboxStyle.test( name ) ) {

        // Remember the original values
        width = style.width;
        minWidth = style.minWidth;
        maxWidth = style.maxWidth;

        // Put in the new values to get a computed value out
        style.minWidth = style.maxWidth = style.width = ret;
        ret = computed.width;

        // Revert the changed values
        style.width = width;
        style.minWidth = minWidth;
        style.maxWidth = maxWidth;
    }
}

return ret !== undefined ?

    // Support: IE <=9 - 11 only
    // IE returns zIndex value as an integer.
    ret + "" :
    ret;
}


function addGetHookIf( conditionFn, hookFn ) {

// Define the hook, we'll check on the first run if it's really needed.
return {
    get: function() {
        if ( conditionFn() ) {

            // Hook not needed (or it's not possible to use it due
            // to missing dependency), remove it.
            delete this.get;
            return;
        }

        // Hook needed; redefine it so that the support test is not executed again.
        return ( this.get = hookFn ).apply( this, arguments );
    }
};
}


var cssPrefixes = [ "Webkit", "Moz", "ms" ],
emptyStyle = document.createElement( "div" ).style,
vendorProps = {};

// Return a vendor-prefixed property or undefined
function vendorPropName( name ) {

// Check for vendor prefixed names
var capName = name[ 0 ].toUpperCase() + name.slice( 1 ),
    i = cssPrefixes.length;

while ( i-- ) {
    name = cssPrefixes[ i ] + capName;
    if ( name in emptyStyle ) {
        return name;
    }
}
}

// Return a potentially-mapped jQuery.cssProps or vendor prefixed property
function finalPropName( name ) {
var final = jQuery.cssProps[ name ] || vendorProps[ name ];

if ( final ) {
    return final;
}
if ( name in emptyStyle ) {
    return name;
}
return vendorProps[ name ] = vendorPropName( name ) || name;
}


var

// Swappable if display is none or starts with table
// except "table", "table-cell", or "table-caption"
// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
rdisplayswap = /^(none|table(?!-c[ea]).+)/,
cssShow = { position: "absolute", visibility: "hidden", display: "block" },
cssNormalTransform = {
    letterSpacing: "0",
    fontWeight: "400"
};

function setPositiveNumber( _elem, value, subtract ) {

// Any relative (+/-) values have already been
// normalized at this point
var matches = rcssNum.exec( value );
return matches ?

    // Guard against undefined "subtract", e.g., when used as in cssHooks
    Math.max( 0, matches[ 2 ] - ( subtract || 0 ) ) + ( matches[ 3 ] || "px" ) :
    value;
}

function boxModelAdjustment( elem, dimension, box, isBorderBox, styles, computedVal ) {
var i = dimension === "width" ? 1 : 0,
    extra = 0,
    delta = 0;

// Adjustment may not be necessary
if ( box === ( isBorderBox ? "border" : "content" ) ) {
    return 0;
}

for ( ; i < 4; i += 2 ) {

    // Both box models exclude margin
    if ( box === "margin" ) {
        delta += jQuery.css( elem, box + cssExpand[ i ], true, styles );
    }

    // If we get here with a content-box, we're seeking "padding" or "border" or "margin"
    if ( !isBorderBox ) {

        // Add padding
        delta += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

        // For "border" or "margin", add border
        if ( box !== "padding" ) {
            delta += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );

        // But still keep track of it otherwise
        } else {
            extra += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
        }

    // If we get here with a border-box (content + padding + border), we're seeking "content" or
    // "padding" or "margin"
    } else {

        // For "content", subtract padding
        if ( box === "content" ) {
            delta -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
        }

        // For "content" or "padding", subtract border
        if ( box !== "margin" ) {
            delta -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
        }
    }
}

// Account for positive content-box scroll gutter when requested by providing computedVal
if ( !isBorderBox && computedVal >= 0 ) {

    // offsetWidth/offsetHeight is a rounded sum of content, padding, scroll gutter, and border
    // Assuming integer scroll gutter, subtract the rest and round down
    delta += Math.max( 0, Math.ceil(
        elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 ) ] -
        computedVal -
        delta -
        extra -
        0.5

    // If offsetWidth/offsetHeight is unknown, then we can't determine content-box scroll gutter
    // Use an explicit zero to avoid NaN (gh-3964)
    ) ) || 0;
}

return delta;
}

function getWidthOrHeight( elem, dimension, extra ) {

// Start with computed style
var styles = getStyles( elem ),

    // To avoid forcing a reflow, only fetch boxSizing if we need it (gh-4322).
    // Fake content-box until we know it's needed to know the true value.
    boxSizingNeeded = !support.boxSizingReliable() || extra,
    isBorderBox = boxSizingNeeded &&
        jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
    valueIsBorderBox = isBorderBox,

    val = curCSS( elem, dimension, styles ),
    offsetProp = "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 );

// Support: Firefox <=54
// Return a confounding non-pixel value or feign ignorance, as appropriate.
if ( rnumnonpx.test( val ) ) {
    if ( !extra ) {
        return val;
    }
    val = "auto";
}


// Support: IE 9 - 11 only
// Use offsetWidth/offsetHeight for when box sizing is unreliable.
// In those cases, the computed value can be trusted to be border-box.
if ( ( !support.boxSizingReliable() && isBorderBox ||

    // Support: IE 10 - 11+, Edge 15 - 18+
    // IE/Edge misreport `getComputedStyle` of table rows with width/height
    // set in CSS while `offset*` properties report correct values.
    // Interestingly, in some cases IE 9 doesn't suffer from this issue.
    !support.reliableTrDimensions() && nodeName( elem, "tr" ) ||

    // Fall back to offsetWidth/offsetHeight when value is "auto"
    // This happens for inline elements with no explicit setting (gh-3571)
    val === "auto" ||

    // Support: Android <=4.1 - 4.3 only
    // Also use offsetWidth/offsetHeight for misreported inline dimensions (gh-3602)
    !parseFloat( val ) && jQuery.css( elem, "display", false, styles ) === "inline" ) &&

    // Make sure the element is visible & connected
    elem.getClientRects().length ) {

    isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

    // Where available, offsetWidth/offsetHeight approximate border box dimensions.
    // Where not available (e.g., SVG), assume unreliable box-sizing and interpret the
    // retrieved value as a content box dimension.
    valueIsBorderBox = offsetProp in elem;
    if ( valueIsBorderBox ) {
        val = elem[ offsetProp ];
    }
}

// Normalize "" and auto
val = parseFloat( val ) || 0;

// Adjust for the element's box model
return ( val +
    boxModelAdjustment(
        elem,
        dimension,
        extra || ( isBorderBox ? "border" : "content" ),
        valueIsBorderBox,
        styles,

        // Provide the current computed size to request scroll gutter calculation (gh-3589)
        val
    )
) + "px";
}

jQuery.extend( {

// Add in style property hooks for overriding the default
// behavior of getting and setting a style property
cssHooks: {
    opacity: {
        get: function( elem, computed ) {
            if ( computed ) {

                // We should always get a number back from opacity
                var ret = curCSS( elem, "opacity" );
                return ret === "" ? "1" : ret;
            }
        }
    }
},

// Don't automatically add "px" to these possibly-unitless properties
cssNumber: {
    "animationIterationCount": true,
    "columnCount": true,
    "fillOpacity": true,
    "flexGrow": true,
    "flexShrink": true,
    "fontWeight": true,
    "gridArea": true,
    "gridColumn": true,
    "gridColumnEnd": true,
    "gridColumnStart": true,
    "gridRow": true,
    "gridRowEnd": true,
    "gridRowStart": true,
    "lineHeight": true,
    "opacity": true,
    "order": true,
    "orphans": true,
    "widows": true,
    "zIndex": true,
    "zoom": true
},

// Add in properties whose names you wish to fix before
// setting or getting the value
cssProps: {},

// Get and set the style property on a DOM Node
style: function( elem, name, value, extra ) {

    // Don't set styles on text and comment nodes
    if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
        return;
    }

    // Make sure that we're working with the right name
    var ret, type, hooks,
        origName = camelCase( name ),
        isCustomProp = rcustomProp.test( name ),
        style = elem.style;

    // Make sure that we're working with the right name. We don't
    // want to query the value if it is a CSS custom property
    // since they are user-defined.
    if ( !isCustomProp ) {
        name = finalPropName( origName );
    }

    // Gets hook for the prefixed version, then unprefixed version
    hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

    // Check if we're setting a value
    if ( value !== undefined ) {
        type = typeof value;

        // Convert "+=" or "-=" to relative numbers (trac-7345)
        if ( type === "string" && ( ret = rcssNum.exec( value ) ) && ret[ 1 ] ) {
            value = adjustCSS( elem, name, ret );

            // Fixes bug trac-9237
            type = "number";
        }

        // Make sure that null and NaN values aren't set (trac-7116)
        if ( value == null || value !== value ) {
            return;
        }

        // If a number was passed in, add the unit (except for certain CSS properties)
        // The isCustomProp check can be removed in jQuery 4.0 when we only auto-append
        // "px" to a few hardcoded values.
        if ( type === "number" && !isCustomProp ) {
            value += ret && ret[ 3 ] || ( jQuery.cssNumber[ origName ] ? "" : "px" );
        }

        // background-* props affect original clone's values
        if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
            style[ name ] = "inherit";
        }

        // If a hook was provided, use that value, otherwise just set the specified value
        if ( !hooks || !( "set" in hooks ) ||
            ( value = hooks.set( elem, value, extra ) ) !== undefined ) {

            if ( isCustomProp ) {
                style.setProperty( name, value );
            } else {
                style[ name ] = value;
            }
        }

    } else {

        // If a hook was provided get the non-computed value from there
        if ( hooks && "get" in hooks &&
            ( ret = hooks.get( elem, false, extra ) ) !== undefined ) {

            return ret;
        }

        // Otherwise just get the value from the style object
        return style[ name ];
    }
},

css: function( elem, name, extra, styles ) {
    var val, num, hooks,
        origName = camelCase( name ),
        isCustomProp = rcustomProp.test( name );

    // Make sure that we're working with the right name. We don't
    // want to modify the value if it is a CSS custom property
    // since they are user-defined.
    if ( !isCustomProp ) {
        name = finalPropName( origName );
    }

    // Try prefixed name followed by the unprefixed name
    hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

    // If a hook was provided get the computed value from there
    if ( hooks && "get" in hooks ) {
        val = hooks.get( elem, true, extra );
    }

    // Otherwise, if a way to get the computed value exists, use that
    if ( val === undefined ) {
        val = curCSS( elem, name, styles );
    }

    // Convert "normal" to computed value
    if ( val === "normal" && name in cssNormalTransform ) {
        val = cssNormalTransform[ name ];
    }

    // Make numeric if forced or a qualifier was provided and val looks numeric
    if ( extra === "" || extra ) {
        num = parseFloat( val );
        return extra === true || isFinite( num ) ? num || 0 : val;
    }

    return val;
}
} );

jQuery.each( [ "height", "width" ], function( _i, dimension ) {
jQuery.cssHooks[ dimension ] = {
    get: function( elem, computed, extra ) {
        if ( computed ) {

            // Certain elements can have dimension info if we invisibly show them
            // but it must have a current display style that would benefit
            return rdisplayswap.test( jQuery.css( elem, "display" ) ) &&

                // Support: Safari 8+
                // Table columns in Safari have non-zero offsetWidth & zero
                // getBoundingClientRect().width unless display is changed.
                // Support: IE <=11 only
                // Running getBoundingClientRect on a disconnected node
                // in IE throws an error.
                ( !elem.getClientRects().length || !elem.getBoundingClientRect().width ) ?
                swap( elem, cssShow, function() {
                    return getWidthOrHeight( elem, dimension, extra );
                } ) :
                getWidthOrHeight( elem, dimension, extra );
        }
    },

    set: function( elem, value, extra ) {
        var matches,
            styles = getStyles( elem ),

            // Only read styles.position if the test has a chance to fail
            // to avoid forcing a reflow.
            scrollboxSizeBuggy = !support.scrollboxSize() &&
                styles.position === "absolute",

            // To avoid forcing a reflow, only fetch boxSizing if we need it (gh-3991)
            boxSizingNeeded = scrollboxSizeBuggy || extra,
            isBorderBox = boxSizingNeeded &&
                jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
            subtract = extra ?
                boxModelAdjustment(
                    elem,
                    dimension,
                    extra,
                    isBorderBox,
                    styles
                ) :
                0;

        // Account for unreliable border-box dimensions by comparing offset* to computed and
        // faking a content-box to get border and padding (gh-3699)
        if ( isBorderBox && scrollboxSizeBuggy ) {
            subtract -= Math.ceil(
                elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 ) ] -
                parseFloat( styles[ dimension ] ) -
                boxModelAdjustment( elem, dimension, "border", false, styles ) -
                0.5
            );
        }

        // Convert to pixels if value adjustment is needed
        if ( subtract && ( matches = rcssNum.exec( value ) ) &&
            ( matches[ 3 ] || "px" ) !== "px" ) {

            elem.style[ dimension ] = value;
            value = jQuery.css( elem, dimension );
        }

        return setPositiveNumber( elem, value, subtract );
    }
};
} );

jQuery.cssHooks.marginLeft = addGetHookIf( support.reliableMarginLeft,
function( elem, computed ) {
    if ( computed ) {
        return ( parseFloat( curCSS( elem, "marginLeft" ) ) ||
            elem.getBoundingClientRect().left -
                swap( elem, { marginLeft: 0 }, function() {
                    return elem.getBoundingClientRect().left;
                } )
        ) + "px";
    }
}
);

// These hooks are used by animate to expand properties
jQuery.each( {
margin: "",
padding: "",
border: "Width"
}, function( prefix, suffix ) {
jQuery.cssHooks[ prefix + suffix ] = {
    expand: function( value ) {
        var i = 0,
            expanded = {},

            // Assumes a single number if not a string
            parts = typeof value === "string" ? value.split( " " ) : [ value ];

        for ( ; i < 4; i++ ) {
            expanded[ prefix + cssExpand[ i ] + suffix ] =
                parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
        }

        return expanded;
    }
};

if ( prefix !== "margin" ) {
    jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
}
} );

jQuery.fn.extend( {
css: function( name, value ) {
    return access( this, function( elem, name, value ) {
        var styles, len,
            map = {},
            i = 0;

        if ( Array.isArray( name ) ) {
            styles = getStyles( elem );
            len = name.length;

            for ( ; i < len; i++ ) {
                map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
            }

            return map;
        }

        return value !== undefined ?
            jQuery.style( elem, name, value ) :
            jQuery.css( elem, name );
    }, name, value, arguments.length > 1 );
}
} );


function Tween( elem, options, prop, end, easing ) {
return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
constructor: Tween,
init: function( elem, options, prop, end, easing, unit ) {
    this.elem = elem;
    this.prop = prop;
    this.easing = easing || jQuery.easing._default;
    this.options = options;
    this.start = this.now = this.cur();
    this.end = end;
    this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
},
cur: function() {
    var hooks = Tween.propHooks[ this.prop ];

    return hooks && hooks.get ?
        hooks.get( this ) :
        Tween.propHooks._default.get( this );
},
run: function( percent ) {
    var eased,
        hooks = Tween.propHooks[ this.prop ];

    if ( this.options.duration ) {
        this.pos = eased = jQuery.easing[ this.easing ](
            percent, this.options.duration * percent, 0, 1, this.options.duration
        );
    } else {
        this.pos = eased = percent;
    }
    this.now = ( this.end - this.start ) * eased + this.start;

    if ( this.options.step ) {
        this.options.step.call( this.elem, this.now, this );
    }

    if ( hooks && hooks.set ) {
        hooks.set( this );
    } else {
        Tween.propHooks._default.set( this );
    }
    return this;
}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
_default: {
    get: function( tween ) {
        var result;

        // Use a property on the element directly when it is not a DOM element,
        // or when there is no matching style property that exists.
        if ( tween.elem.nodeType !== 1 ||
            tween.elem[ tween.prop ] != null && tween.elem.style[ tween.prop ] == null ) {
            return tween.elem[ tween.prop ];
        }

        // Passing an empty string as a 3rd parameter to .css will automatically
        // attempt a parseFloat and fallback to a string if the parse fails.
        // Simple values such as "10px" are parsed to Float;
        // complex values such as "rotate(1rad)" are returned as-is.
        result = jQuery.css( tween.elem, tween.prop, "" );

        // Empty strings, null, undefined and "auto" are converted to 0.
        return !result || result === "auto" ? 0 : result;
    },
    set: function( tween ) {

        // Use step hook for back compat.
        // Use cssHook if its there.
        // Use .style if available and use plain properties where available.
        if ( jQuery.fx.step[ tween.prop ] ) {
            jQuery.fx.step[ tween.prop ]( tween );
        } else if ( tween.elem.nodeType === 1 && (
            jQuery.cssHooks[ tween.prop ] ||
                tween.elem.style[ finalPropName( tween.prop ) ] != null ) ) {
            jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
        } else {
            tween.elem[ tween.prop ] = tween.now;
        }
    }
}
};

// Support: IE <=9 only
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
set: function( tween ) {
    if ( tween.elem.nodeType && tween.elem.parentNode ) {
        tween.elem[ tween.prop ] = tween.now;
    }
}
};

jQuery.easing = {
linear: function( p ) {
    return p;
},
swing: function( p ) {
    return 0.5 - Math.cos( p * Math.PI ) / 2;
},
_default: "swing"
};

jQuery.fx = Tween.prototype.init;

// Back compat <1.8 extension point
jQuery.fx.step = {};




var
fxNow, inProgress,
rfxtypes = /^(?:toggle|show|hide)$/,
rrun = /queueHooks$/;

function schedule() {
if ( inProgress ) {
    if ( document.hidden === false && window.requestAnimationFrame ) {
        window.requestAnimationFrame( schedule );
    } else {
        window.setTimeout( schedule, jQuery.fx.interval );
    }

    jQuery.fx.tick();
}
}

// Animations created synchronously will run synchronously
function createFxNow() {
window.setTimeout( function() {
    fxNow = undefined;
} );
return ( fxNow = Date.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
var which,
    i = 0,
    attrs = { height: type };

// If we include width, step value is 1 to do all cssExpand values,
// otherwise step value is 2 to skip over Left and Right
includeWidth = includeWidth ? 1 : 0;
for ( ; i < 4; i += 2 - includeWidth ) {
    which = cssExpand[ i ];
    attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
}

if ( includeWidth ) {
    attrs.opacity = attrs.width = type;
}

return attrs;
}

function createTween( value, prop, animation ) {
var tween,
    collection = ( Animation.tweeners[ prop ] || [] ).concat( Animation.tweeners[ "*" ] ),
    index = 0,
    length = collection.length;
for ( ; index < length; index++ ) {
    if ( ( tween = collection[ index ].call( animation, prop, value ) ) ) {

        // We're done with this property
        return tween;
    }
}
}

function defaultPrefilter( elem, props, opts ) {
var prop, value, toggle, hooks, oldfire, propTween, restoreDisplay, display,
    isBox = "width" in props || "height" in props,
    anim = this,
    orig = {},
    style = elem.style,
    hidden = elem.nodeType && isHiddenWithinTree( elem ),
    dataShow = dataPriv.get( elem, "fxshow" );

// Queue-skipping animations hijack the fx hooks
if ( !opts.queue ) {
    hooks = jQuery._queueHooks( elem, "fx" );
    if ( hooks.unqueued == null ) {
        hooks.unqueued = 0;
        oldfire = hooks.empty.fire;
        hooks.empty.fire = function() {
            if ( !hooks.unqueued ) {
                oldfire();
            }
        };
    }
    hooks.unqueued++;

    anim.always( function() {

        // Ensure the complete handler is called before this completes
        anim.always( function() {
            hooks.unqueued--;
            if ( !jQuery.queue( elem, "fx" ).length ) {
                hooks.empty.fire();
            }
        } );
    } );
}

// Detect show/hide animations
for ( prop in props ) {
    value = props[ prop ];
    if ( rfxtypes.test( value ) ) {
        delete props[ prop ];
        toggle = toggle || value === "toggle";
        if ( value === ( hidden ? "hide" : "show" ) ) {

            // Pretend to be hidden if this is a "show" and
            // there is still data from a stopped show/hide
            if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
                hidden = true;

            // Ignore all other no-op show/hide data
            } else {
                continue;
            }
        }
        orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );
    }
}

// Bail out if this is a no-op like .hide().hide()
propTween = !jQuery.isEmptyObject( props );
if ( !propTween && jQuery.isEmptyObject( orig ) ) {
    return;
}

// Restrict "overflow" and "display" styles during box animations
if ( isBox && elem.nodeType === 1 ) {

    // Support: IE <=9 - 11, Edge 12 - 15
    // Record all 3 overflow attributes because IE does not infer the shorthand
    // from identically-valued overflowX and overflowY and Edge just mirrors
    // the overflowX value there.
    opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

    // Identify a display type, preferring old show/hide data over the CSS cascade
    restoreDisplay = dataShow && dataShow.display;
    if ( restoreDisplay == null ) {
        restoreDisplay = dataPriv.get( elem, "display" );
    }
    display = jQuery.css( elem, "display" );
    if ( display === "none" ) {
        if ( restoreDisplay ) {
            display = restoreDisplay;
        } else {

            // Get nonempty value(s) by temporarily forcing visibility
            showHide( [ elem ], true );
            restoreDisplay = elem.style.display || restoreDisplay;
            display = jQuery.css( elem, "display" );
            showHide( [ elem ] );
        }
    }

    // Animate inline elements as inline-block
    if ( display === "inline" || display === "inline-block" && restoreDisplay != null ) {
        if ( jQuery.css( elem, "float" ) === "none" ) {

            // Restore the original display value at the end of pure show/hide animations
            if ( !propTween ) {
                anim.done( function() {
                    style.display = restoreDisplay;
                } );
                if ( restoreDisplay == null ) {
                    display = style.display;
                    restoreDisplay = display === "none" ? "" : display;
                }
            }
            style.display = "inline-block";
        }
    }
}

if ( opts.overflow ) {
    style.overflow = "hidden";
    anim.always( function() {
        style.overflow = opts.overflow[ 0 ];
        style.overflowX = opts.overflow[ 1 ];
        style.overflowY = opts.overflow[ 2 ];
    } );
}

// Implement show/hide animations
propTween = false;
for ( prop in orig ) {

    // General show/hide setup for this element animation
    if ( !propTween ) {
        if ( dataShow ) {
            if ( "hidden" in dataShow ) {
                hidden = dataShow.hidden;
            }
        } else {
            dataShow = dataPriv.access( elem, "fxshow", { display: restoreDisplay } );
        }

        // Store hidden/visible for toggle so `.stop().toggle()` "reverses"
        if ( toggle ) {
            dataShow.hidden = !hidden;
        }

        // Show elements before animating them
        if ( hidden ) {
            showHide( [ elem ], true );
        }

        /* eslint-disable no-loop-func */

        anim.done( function() {

            /* eslint-enable no-loop-func */

            // The final step of a "hide" animation is actually hiding the element
            if ( !hidden ) {
                showHide( [ elem ] );
            }
            dataPriv.remove( elem, "fxshow" );
            for ( prop in orig ) {
                jQuery.style( elem, prop, orig[ prop ] );
            }
        } );
    }

    // Per-property setup
    propTween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );
    if ( !( prop in dataShow ) ) {
        dataShow[ prop ] = propTween.start;
        if ( hidden ) {
            propTween.end = propTween.start;
            propTween.start = 0;
        }
    }
}
}

function propFilter( props, specialEasing ) {
var index, name, easing, value, hooks;

// camelCase, specialEasing and expand cssHook pass
for ( index in props ) {
    name = camelCase( index );
    easing = specialEasing[ name ];
    value = props[ index ];
    if ( Array.isArray( value ) ) {
        easing = value[ 1 ];
        value = props[ index ] = value[ 0 ];
    }

    if ( index !== name ) {
        props[ name ] = value;
        delete props[ index ];
    }

    hooks = jQuery.cssHooks[ name ];
    if ( hooks && "expand" in hooks ) {
        value = hooks.expand( value );
        delete props[ name ];

        // Not quite $.extend, this won't overwrite existing keys.
        // Reusing 'index' because we have the correct "name"
        for ( index in value ) {
            if ( !( index in props ) ) {
                props[ index ] = value[ index ];
                specialEasing[ index ] = easing;
            }
        }
    } else {
        specialEasing[ name ] = easing;
    }
}
}

function Animation( elem, properties, options ) {
var result,
    stopped,
    index = 0,
    length = Animation.prefilters.length,
    deferred = jQuery.Deferred().always( function() {

        // Don't match elem in the :animated selector
        delete tick.elem;
    } ),
    tick = function() {
        if ( stopped ) {
            return false;
        }
        var currentTime = fxNow || createFxNow(),
            remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),

            // Support: Android 2.3 only
            // Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (trac-12497)
            temp = remaining / animation.duration || 0,
            percent = 1 - temp,
            index = 0,
            length = animation.tweens.length;

        for ( ; index < length; index++ ) {
            animation.tweens[ index ].run( percent );
        }

        deferred.notifyWith( elem, [ animation, percent, remaining ] );

        // If there's more to do, yield
        if ( percent < 1 && length ) {
            return remaining;
        }

        // If this was an empty animation, synthesize a final progress notification
        if ( !length ) {
            deferred.notifyWith( elem, [ animation, 1, 0 ] );
        }

        // Resolve the animation and report its conclusion
        deferred.resolveWith( elem, [ animation ] );
        return false;
    },
    animation = deferred.promise( {
        elem: elem,
        props: jQuery.extend( {}, properties ),
        opts: jQuery.extend( true, {
            specialEasing: {},
            easing: jQuery.easing._default
        }, options ),
        originalProperties: properties,
        originalOptions: options,
        startTime: fxNow || createFxNow(),
        duration: options.duration,
        tweens: [],
        createTween: function( prop, end ) {
            var tween = jQuery.Tween( elem, animation.opts, prop, end,
                animation.opts.specialEasing[ prop ] || animation.opts.easing );
            animation.tweens.push( tween );
            return tween;
        },
        stop: function( gotoEnd ) {
            var index = 0,

                // If we are going to the end, we want to run all the tweens
                // otherwise we skip this part
                length = gotoEnd ? animation.tweens.length : 0;
            if ( stopped ) {
                return this;
            }
            stopped = true;
            for ( ; index < length; index++ ) {
                animation.tweens[ index ].run( 1 );
            }

            // Resolve when we played the last frame; otherwise, reject
            if ( gotoEnd ) {
                deferred.notifyWith( elem, [ animation, 1, 0 ] );
                deferred.resolveWith( elem, [ animation, gotoEnd ] );
            } else {
                deferred.rejectWith( elem, [ animation, gotoEnd ] );
            }
            return this;
        }
    } ),
    props = animation.props;

propFilter( props, animation.opts.specialEasing );

for ( ; index < length; index++ ) {
    result = Animation.prefilters[ index ].call( animation, elem, props, animation.opts );
    if ( result ) {
        if ( isFunction( result.stop ) ) {
            jQuery._queueHooks( animation.elem, animation.opts.queue ).stop =
                result.stop.bind( result );
        }
        return result;
    }
}

jQuery.map( props, createTween, animation );

if ( isFunction( animation.opts.start ) ) {
    animation.opts.start.call( elem, animation );
}

// Attach callbacks from options
animation
    .progress( animation.opts.progress )
    .done( animation.opts.done, animation.opts.complete )
    .fail( animation.opts.fail )
    .always( animation.opts.always );

jQuery.fx.timer(
    jQuery.extend( tick, {
        elem: elem,
        anim: animation,
        queue: animation.opts.queue
    } )
);

return animation;
}

jQuery.Animation = jQuery.extend( Animation, {

tweeners: {
    "*": [ function( prop, value ) {
        var tween = this.createTween( prop, value );
        adjustCSS( tween.elem, prop, rcssNum.exec( value ), tween );
        return tween;
    } ]
},

tweener: function( props, callback ) {
    if ( isFunction( props ) ) {
        callback = props;
        props = [ "*" ];
    } else {
        props = props.match( rnothtmlwhite );
    }

    var prop,
        index = 0,
        length = props.length;

    for ( ; index < length; index++ ) {
        prop = props[ index ];
        Animation.tweeners[ prop ] = Animation.tweeners[ prop ] || [];
        Animation.tweeners[ prop ].unshift( callback );
    }
},

prefilters: [ defaultPrefilter ],

prefilter: function( callback, prepend ) {
    if ( prepend ) {
        Animation.prefilters.unshift( callback );
    } else {
        Animation.prefilters.push( callback );
    }
}
} );

jQuery.speed = function( speed, easing, fn ) {
var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
    complete: fn || !fn && easing ||
        isFunction( speed ) && speed,
    duration: speed,
    easing: fn && easing || easing && !isFunction( easing ) && easing
};

// Go to the end state if fx are off
if ( jQuery.fx.off ) {
    opt.duration = 0;

} else {
    if ( typeof opt.duration !== "number" ) {
        if ( opt.duration in jQuery.fx.speeds ) {
            opt.duration = jQuery.fx.speeds[ opt.duration ];

        } else {
            opt.duration = jQuery.fx.speeds._default;
        }
    }
}

// Normalize opt.queue - true/undefined/null -> "fx"
if ( opt.queue == null || opt.queue === true ) {
    opt.queue = "fx";
}

// Queueing
opt.old = opt.complete;

opt.complete = function() {
    if ( isFunction( opt.old ) ) {
        opt.old.call( this );
    }

    if ( opt.queue ) {
        jQuery.dequeue( this, opt.queue );
    }
};

return opt;
};

jQuery.fn.extend( {
fadeTo: function( speed, to, easing, callback ) {

    // Show any hidden elements after setting opacity to 0
    return this.filter( isHiddenWithinTree ).css( "opacity", 0 ).show()

        // Animate to the value specified
        .end().animate( { opacity: to }, speed, easing, callback );
},
animate: function( prop, speed, easing, callback ) {
    var empty = jQuery.isEmptyObject( prop ),
        optall = jQuery.speed( speed, easing, callback ),
        doAnimation = function() {

            // Operate on a copy of prop so per-property easing won't be lost
            var anim = Animation( this, jQuery.extend( {}, prop ), optall );

            // Empty animations, or finishing resolves immediately
            if ( empty || dataPriv.get( this, "finish" ) ) {
                anim.stop( true );
            }
        };

    doAnimation.finish = doAnimation;

    return empty || optall.queue === false ?
        this.each( doAnimation ) :
        this.queue( optall.queue, doAnimation );
},
stop: function( type, clearQueue, gotoEnd ) {
    var stopQueue = function( hooks ) {
        var stop = hooks.stop;
        delete hooks.stop;
        stop( gotoEnd );
    };

    if ( typeof type !== "string" ) {
        gotoEnd = clearQueue;
        clearQueue = type;
        type = undefined;
    }
    if ( clearQueue ) {
        this.queue( type || "fx", [] );
    }

    return this.each( function() {
        var dequeue = true,
            index = type != null && type + "queueHooks",
            timers = jQuery.timers,
            data = dataPriv.get( this );

        if ( index ) {
            if ( data[ index ] && data[ index ].stop ) {
                stopQueue( data[ index ] );
            }
        } else {
            for ( index in data ) {
                if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
                    stopQueue( data[ index ] );
                }
            }
        }

        for ( index = timers.length; index--; ) {
            if ( timers[ index ].elem === this &&
                ( type == null || timers[ index ].queue === type ) ) {

                timers[ index ].anim.stop( gotoEnd );
                dequeue = false;
                timers.splice( index, 1 );
            }
        }

        // Start the next in the queue if the last step wasn't forced.
        // Timers currently will call their complete callbacks, which
        // will dequeue but only if they were gotoEnd.
        if ( dequeue || !gotoEnd ) {
            jQuery.dequeue( this, type );
        }
    } );
},
finish: function( type ) {
    if ( type !== false ) {
        type = type || "fx";
    }
    return this.each( function() {
        var index,
            data = dataPriv.get( this ),
            queue = data[ type + "queue" ],
            hooks = data[ type + "queueHooks" ],
            timers = jQuery.timers,
            length = queue ? queue.length : 0;

        // Enable finishing flag on private data
        data.finish = true;

        // Empty the queue first
        jQuery.queue( this, type, [] );

        if ( hooks && hooks.stop ) {
            hooks.stop.call( this, true );
        }

        // Look for any active animations, and finish them
        for ( index = timers.length; index--; ) {
            if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
                timers[ index ].anim.stop( true );
                timers.splice( index, 1 );
            }
        }

        // Look for any animations in the old queue and finish them
        for ( index = 0; index < length; index++ ) {
            if ( queue[ index ] && queue[ index ].finish ) {
                queue[ index ].finish.call( this );
            }
        }

        // Turn off finishing flag
        delete data.finish;
    } );
}
} );

jQuery.each( [ "toggle", "show", "hide" ], function( _i, name ) {
var cssFn = jQuery.fn[ name ];
jQuery.fn[ name ] = function( speed, easing, callback ) {
    return speed == null || typeof speed === "boolean" ?
        cssFn.apply( this, arguments ) :
        this.animate( genFx( name, true ), speed, easing, callback );
};
} );

// Generate shortcuts for custom animations
jQuery.each( {
slideDown: genFx( "show" ),
slideUp: genFx( "hide" ),
slideToggle: genFx( "toggle" ),
fadeIn: { opacity: "show" },
fadeOut: { opacity: "hide" },
fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
jQuery.fn[ name ] = function( speed, easing, callback ) {
    return this.animate( props, speed, easing, callback );
};
} );

jQuery.timers = [];
jQuery.fx.tick = function() {
var timer,
    i = 0,
    timers = jQuery.timers;

fxNow = Date.now();

for ( ; i < timers.length; i++ ) {
    timer = timers[ i ];

    // Run the timer and safely remove it when done (allowing for external removal)
    if ( !timer() && timers[ i ] === timer ) {
        timers.splice( i--, 1 );
    }
}

if ( !timers.length ) {
    jQuery.fx.stop();
}
fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
jQuery.timers.push( timer );
jQuery.fx.start();
};

jQuery.fx.interval = 13;
jQuery.fx.start = function() {
if ( inProgress ) {
    return;
}

inProgress = true;
schedule();
};

jQuery.fx.stop = function() {
inProgress = null;
};

jQuery.fx.speeds = {
slow: 600,
fast: 200,

// Default speed
_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
jQuery.fn.delay = function( time, type ) {
time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
type = type || "fx";

return this.queue( type, function( next, hooks ) {
    var timeout = window.setTimeout( next, time );
    hooks.stop = function() {
        window.clearTimeout( timeout );
    };
} );
};


( function() {
var input = document.createElement( "input" ),
    select = document.createElement( "select" ),
    opt = select.appendChild( document.createElement( "option" ) );

input.type = "checkbox";

// Support: Android <=4.3 only
// Default value for a checkbox should be "on"
support.checkOn = input.value !== "";

// Support: IE <=11 only
// Must access selectedIndex to make default options select
support.optSelected = opt.selected;

// Support: IE <=11 only
// An input loses its value after becoming a radio
input = document.createElement( "input" );
input.value = "t";
input.type = "radio";
support.radioValue = input.value === "t";
} )();


var boolHook,
attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend( {
attr: function( name, value ) {
    return access( this, jQuery.attr, name, value, arguments.length > 1 );
},

removeAttr: function( name ) {
    return this.each( function() {
        jQuery.removeAttr( this, name );
    } );
}
} );

jQuery.extend( {
attr: function( elem, name, value ) {
    var ret, hooks,
        nType = elem.nodeType;

    // Don't get/set attributes on text, comment and attribute nodes
    if ( nType === 3 || nType === 8 || nType === 2 ) {
        return;
    }

    // Fallback to prop when attributes are not supported
    if ( typeof elem.getAttribute === "undefined" ) {
        return jQuery.prop( elem, name, value );
    }

    // Attribute hooks are determined by the lowercase version
    // Grab necessary hook if one is defined
    if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
        hooks = jQuery.attrHooks[ name.toLowerCase() ] ||
            ( jQuery.expr.match.bool.test( name ) ? boolHook : undefined );
    }

    if ( value !== undefined ) {
        if ( value === null ) {
            jQuery.removeAttr( elem, name );
            return;
        }

        if ( hooks && "set" in hooks &&
            ( ret = hooks.set( elem, value, name ) ) !== undefined ) {
            return ret;
        }

        elem.setAttribute( name, value + "" );
        return value;
    }

    if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
        return ret;
    }

    ret = jQuery.find.attr( elem, name );

    // Non-existent attributes return null, we normalize to undefined
    return ret == null ? undefined : ret;
},

attrHooks: {
    type: {
        set: function( elem, value ) {
            if ( !support.radioValue && value === "radio" &&
                nodeName( elem, "input" ) ) {
                var val = elem.value;
                elem.setAttribute( "type", value );
                if ( val ) {
                    elem.value = val;
                }
                return value;
            }
        }
    }
},

removeAttr: function( elem, value ) {
    var name,
        i = 0,

        // Attribute names can contain non-HTML whitespace characters
        // https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
        attrNames = value && value.match( rnothtmlwhite );

    if ( attrNames && elem.nodeType === 1 ) {
        while ( ( name = attrNames[ i++ ] ) ) {
            elem.removeAttribute( name );
        }
    }
}
} );

// Hooks for boolean attributes
boolHook = {
set: function( elem, value, name ) {
    if ( value === false ) {

        // Remove boolean attributes when set to false
        jQuery.removeAttr( elem, name );
    } else {
        elem.setAttribute( name, name );
    }
    return name;
}
};

jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( _i, name ) {
var getter = attrHandle[ name ] || jQuery.find.attr;

attrHandle[ name ] = function( elem, name, isXML ) {
    var ret, handle,
        lowercaseName = name.toLowerCase();

    if ( !isXML ) {

        // Avoid an infinite loop by temporarily removing this function from the getter
        handle = attrHandle[ lowercaseName ];
        attrHandle[ lowercaseName ] = ret;
        ret = getter( elem, name, isXML ) != null ?
            lowercaseName :
            null;
        attrHandle[ lowercaseName ] = handle;
    }
    return ret;
};
} );




var rfocusable = /^(?:input|select|textarea|button)$/i,
rclickable = /^(?:a|area)$/i;

jQuery.fn.extend( {
prop: function( name, value ) {
    return access( this, jQuery.prop, name, value, arguments.length > 1 );
},

removeProp: function( name ) {
    return this.each( function() {
        delete this[ jQuery.propFix[ name ] || name ];
    } );
}
} );

jQuery.extend( {
prop: function( elem, name, value ) {
    var ret, hooks,
        nType = elem.nodeType;

    // Don't get/set properties on text, comment and attribute nodes
    if ( nType === 3 || nType === 8 || nType === 2 ) {
        return;
    }

    if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {

        // Fix name and attach hooks
        name = jQuery.propFix[ name ] || name;
        hooks = jQuery.propHooks[ name ];
    }

    if ( value !== undefined ) {
        if ( hooks && "set" in hooks &&
            ( ret = hooks.set( elem, value, name ) ) !== undefined ) {
            return ret;
        }

        return ( elem[ name ] = value );
    }

    if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
        return ret;
    }

    return elem[ name ];
},

propHooks: {
    tabIndex: {
        get: function( elem ) {

            // Support: IE <=9 - 11 only
            // elem.tabIndex doesn't always return the
            // correct value when it hasn't been explicitly set
            // Use proper attribute retrieval (trac-12072)
            var tabindex = jQuery.find.attr( elem, "tabindex" );

            if ( tabindex ) {
                return parseInt( tabindex, 10 );
            }

            if (
                rfocusable.test( elem.nodeName ) ||
                rclickable.test( elem.nodeName ) &&
                elem.href
            ) {
                return 0;
            }

            return -1;
        }
    }
},

propFix: {
    "for": "htmlFor",
    "class": "className"
}
} );

// Support: IE <=11 only
// Accessing the selectedIndex property
// forces the browser to respect setting selected
// on the option
// The getter ensures a default option is selected
// when in an optgroup
// eslint rule "no-unused-expressions" is disabled for this code
// since it considers such accessions noop
if ( !support.optSelected ) {
jQuery.propHooks.selected = {
    get: function( elem ) {

        /* eslint no-unused-expressions: "off" */

        var parent = elem.parentNode;
        if ( parent && parent.parentNode ) {
            parent.parentNode.selectedIndex;
        }
        return null;
    },
    set: function( elem ) {

        /* eslint no-unused-expressions: "off" */

        var parent = elem.parentNode;
        if ( parent ) {
            parent.selectedIndex;

            if ( parent.parentNode ) {
                parent.parentNode.selectedIndex;
            }
        }
    }
};
}

jQuery.each( [
"tabIndex",
"readOnly",
"maxLength",
"cellSpacing",
"cellPadding",
"rowSpan",
"colSpan",
"useMap",
"frameBorder",
"contentEditable"
], function() {
jQuery.propFix[ this.toLowerCase() ] = this;
} );




// Strip and collapse whitespace according to HTML spec
// https://infra.spec.whatwg.org/#strip-and-collapse-ascii-whitespace
function stripAndCollapse( value ) {
    var tokens = value.match( rnothtmlwhite ) || [];
    return tokens.join( " " );
}


function getClass( elem ) {
return elem.getAttribute && elem.getAttribute( "class" ) || "";
}

function classesToArray( value ) {
if ( Array.isArray( value ) ) {
    return value;
}
if ( typeof value === "string" ) {
    return value.match( rnothtmlwhite ) || [];
}
return [];
}

jQuery.fn.extend( {
addClass: function( value ) {
    var classNames, cur, curValue, className, i, finalValue;

    if ( isFunction( value ) ) {
        return this.each( function( j ) {
            jQuery( this ).addClass( value.call( this, j, getClass( this ) ) );
        } );
    }

    classNames = classesToArray( value );

    if ( classNames.length ) {
        return this.each( function() {
            curValue = getClass( this );
            cur = this.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

            if ( cur ) {
                for ( i = 0; i < classNames.length; i++ ) {
                    className = classNames[ i ];
                    if ( cur.indexOf( " " + className + " " ) < 0 ) {
                        cur += className + " ";
                    }
                }

                // Only assign if different to avoid unneeded rendering.
                finalValue = stripAndCollapse( cur );
                if ( curValue !== finalValue ) {
                    this.setAttribute( "class", finalValue );
                }
            }
        } );
    }

    return this;
},

removeClass: function( value ) {
    var classNames, cur, curValue, className, i, finalValue;

    if ( isFunction( value ) ) {
        return this.each( function( j ) {
            jQuery( this ).removeClass( value.call( this, j, getClass( this ) ) );
        } );
    }

    if ( !arguments.length ) {
        return this.attr( "class", "" );
    }

    classNames = classesToArray( value );

    if ( classNames.length ) {
        return this.each( function() {
            curValue = getClass( this );

            // This expression is here for better compressibility (see addClass)
            cur = this.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

            if ( cur ) {
                for ( i = 0; i < classNames.length; i++ ) {
                    className = classNames[ i ];

                    // Remove *all* instances
                    while ( cur.indexOf( " " + className + " " ) > -1 ) {
                        cur = cur.replace( " " + className + " ", " " );
                    }
                }

                // Only assign if different to avoid unneeded rendering.
                finalValue = stripAndCollapse( cur );
                if ( curValue !== finalValue ) {
                    this.setAttribute( "class", finalValue );
                }
            }
        } );
    }

    return this;
},

toggleClass: function( value, stateVal ) {
    var classNames, className, i, self,
        type = typeof value,
        isValidValue = type === "string" || Array.isArray( value );

    if ( isFunction( value ) ) {
        return this.each( function( i ) {
            jQuery( this ).toggleClass(
                value.call( this, i, getClass( this ), stateVal ),
                stateVal
            );
        } );
    }

    if ( typeof stateVal === "boolean" && isValidValue ) {
        return stateVal ? this.addClass( value ) : this.removeClass( value );
    }

    classNames = classesToArray( value );

    return this.each( function() {
        if ( isValidValue ) {

            // Toggle individual class names
            self = jQuery( this );

            for ( i = 0; i < classNames.length; i++ ) {
                className = classNames[ i ];

                // Check each className given, space separated list
                if ( self.hasClass( className ) ) {
                    self.removeClass( className );
                } else {
                    self.addClass( className );
                }
            }

        // Toggle whole class name
        } else if ( value === undefined || type === "boolean" ) {
            className = getClass( this );
            if ( className ) {

                // Store className if set
                dataPriv.set( this, "__className__", className );
            }

            // If the element has a class name or if we're passed `false`,
            // then remove the whole classname (if there was one, the above saved it).
            // Otherwise bring back whatever was previously saved (if anything),
            // falling back to the empty string if nothing was stored.
            if ( this.setAttribute ) {
                this.setAttribute( "class",
                    className || value === false ?
                        "" :
                        dataPriv.get( this, "__className__" ) || ""
                );
            }
        }
    } );
},

hasClass: function( selector ) {
    var className, elem,
        i = 0;

    className = " " + selector + " ";
    while ( ( elem = this[ i++ ] ) ) {
        if ( elem.nodeType === 1 &&
            ( " " + stripAndCollapse( getClass( elem ) ) + " " ).indexOf( className ) > -1 ) {
            return true;
        }
    }

    return false;
}
} );




var rreturn = /\r/g;

jQuery.fn.extend( {
val: function( value ) {
    var hooks, ret, valueIsFunction,
        elem = this[ 0 ];

    if ( !arguments.length ) {
        if ( elem ) {
            hooks = jQuery.valHooks[ elem.type ] ||
                jQuery.valHooks[ elem.nodeName.toLowerCase() ];

            if ( hooks &&
                "get" in hooks &&
                ( ret = hooks.get( elem, "value" ) ) !== undefined
            ) {
                return ret;
            }

            ret = elem.value;

            // Handle most common string cases
            if ( typeof ret === "string" ) {
                return ret.replace( rreturn, "" );
            }

            // Handle cases where value is null/undef or number
            return ret == null ? "" : ret;
        }

        return;
    }

    valueIsFunction = isFunction( value );

    return this.each( function( i ) {
        var val;

        if ( this.nodeType !== 1 ) {
            return;
        }

        if ( valueIsFunction ) {
            val = value.call( this, i, jQuery( this ).val() );
        } else {
            val = value;
        }

        // Treat null/undefined as ""; convert numbers to string
        if ( val == null ) {
            val = "";

        } else if ( typeof val === "number" ) {
            val += "";

        } else if ( Array.isArray( val ) ) {
            val = jQuery.map( val, function( value ) {
                return value == null ? "" : value + "";
            } );
        }

        hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

        // If set returns undefined, fall back to normal setting
        if ( !hooks || !( "set" in hooks ) || hooks.set( this, val, "value" ) === undefined ) {
            this.value = val;
        }
    } );
}
} );

jQuery.extend( {
valHooks: {
    option: {
        get: function( elem ) {

            var val = jQuery.find.attr( elem, "value" );
            return val != null ?
                val :

                // Support: IE <=10 - 11 only
                // option.text throws exceptions (trac-14686, trac-14858)
                // Strip and collapse whitespace
                // https://html.spec.whatwg.org/#strip-and-collapse-whitespace
                stripAndCollapse( jQuery.text( elem ) );
        }
    },
    select: {
        get: function( elem ) {
            var value, option, i,
                options = elem.options,
                index = elem.selectedIndex,
                one = elem.type === "select-one",
                values = one ? null : [],
                max = one ? index + 1 : options.length;

            if ( index < 0 ) {
                i = max;

            } else {
                i = one ? index : 0;
            }

            // Loop through all the selected options
            for ( ; i < max; i++ ) {
                option = options[ i ];

                // Support: IE <=9 only
                // IE8-9 doesn't update selected after form reset (trac-2551)
                if ( ( option.selected || i === index ) &&

                        // Don't return options that are disabled or in a disabled optgroup
                        !option.disabled &&
                        ( !option.parentNode.disabled ||
                            !nodeName( option.parentNode, "optgroup" ) ) ) {

                    // Get the specific value for the option
                    value = jQuery( option ).val();

                    // We don't need an array for one selects
                    if ( one ) {
                        return value;
                    }

                    // Multi-Selects return an array
                    values.push( value );
                }
            }

            return values;
        },

        set: function( elem, value ) {
            var optionSet, option,
                options = elem.options,
                values = jQuery.makeArray( value ),
                i = options.length;

            while ( i-- ) {
                option = options[ i ];

                /* eslint-disable no-cond-assign */

                if ( option.selected =
                    jQuery.inArray( jQuery.valHooks.option.get( option ), values ) > -1
                ) {
                    optionSet = true;
                }

                /* eslint-enable no-cond-assign */
            }

            // Force browsers to behave consistently when non-matching value is set
            if ( !optionSet ) {
                elem.selectedIndex = -1;
            }
            return values;
        }
    }
}
} );

// Radios and checkboxes getter/setter
jQuery.each( [ "radio", "checkbox" ], function() {
jQuery.valHooks[ this ] = {
    set: function( elem, value ) {
        if ( Array.isArray( value ) ) {
            return ( elem.checked = jQuery.inArray( jQuery( elem ).val(), value ) > -1 );
        }
    }
};
if ( !support.checkOn ) {
    jQuery.valHooks[ this ].get = function( elem ) {
        return elem.getAttribute( "value" ) === null ? "on" : elem.value;
    };
}
} );




// Return jQuery for attributes-only inclusion


support.focusin = "onfocusin" in window;


var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
stopPropagationCallback = function( e ) {
    e.stopPropagation();
};

jQuery.extend( jQuery.event, {

trigger: function( event, data, elem, onlyHandlers ) {

    var i, cur, tmp, bubbleType, ontype, handle, special, lastElement,
        eventPath = [ elem || document ],
        type = hasOwn.call( event, "type" ) ? event.type : event,
        namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split( "." ) : [];

    cur = lastElement = tmp = elem = elem || document;

    // Don't do events on text and comment nodes
    if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
        return;
    }

    // focus/blur morphs to focusin/out; ensure we're not firing them right now
    if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
        return;
    }

    if ( type.indexOf( "." ) > -1 ) {

        // Namespaced trigger; create a regexp to match event type in handle()
        namespaces = type.split( "." );
        type = namespaces.shift();
        namespaces.sort();
    }
    ontype = type.indexOf( ":" ) < 0 && "on" + type;

    // Caller can pass in a jQuery.Event object, Object, or just an event type string
    event = event[ jQuery.expando ] ?
        event :
        new jQuery.Event( type, typeof event === "object" && event );

    // Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
    event.isTrigger = onlyHandlers ? 2 : 3;
    event.namespace = namespaces.join( "." );
    event.rnamespace = event.namespace ?
        new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ) :
        null;

    // Clean up the event in case it is being reused
    event.result = undefined;
    if ( !event.target ) {
        event.target = elem;
    }

    // Clone any incoming data and prepend the event, creating the handler arg list
    data = data == null ?
        [ event ] :
        jQuery.makeArray( data, [ event ] );

    // Allow special events to draw outside the lines
    special = jQuery.event.special[ type ] || {};
    if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
        return;
    }

    // Determine event propagation path in advance, per W3C events spec (trac-9951)
    // Bubble up to document, then to window; watch for a global ownerDocument var (trac-9724)
    if ( !onlyHandlers && !special.noBubble && !isWindow( elem ) ) {

        bubbleType = special.delegateType || type;
        if ( !rfocusMorph.test( bubbleType + type ) ) {
            cur = cur.parentNode;
        }
        for ( ; cur; cur = cur.parentNode ) {
            eventPath.push( cur );
            tmp = cur;
        }

        // Only add window if we got to document (e.g., not plain obj or detached DOM)
        if ( tmp === ( elem.ownerDocument || document ) ) {
            eventPath.push( tmp.defaultView || tmp.parentWindow || window );
        }
    }

    // Fire handlers on the event path
    i = 0;
    while ( ( cur = eventPath[ i++ ] ) && !event.isPropagationStopped() ) {
        lastElement = cur;
        event.type = i > 1 ?
            bubbleType :
            special.bindType || type;

        // jQuery handler
        handle = ( dataPriv.get( cur, "events" ) || Object.create( null ) )[ event.type ] &&
            dataPriv.get( cur, "handle" );
        if ( handle ) {
            handle.apply( cur, data );
        }

        // Native handler
        handle = ontype && cur[ ontype ];
        if ( handle && handle.apply && acceptData( cur ) ) {
            event.result = handle.apply( cur, data );
            if ( event.result === false ) {
                event.preventDefault();
            }
        }
    }
    event.type = type;

    // If nobody prevented the default action, do it now
    if ( !onlyHandlers && !event.isDefaultPrevented() ) {

        if ( ( !special._default ||
            special._default.apply( eventPath.pop(), data ) === false ) &&
            acceptData( elem ) ) {

            // Call a native DOM method on the target with the same name as the event.
            // Don't do default actions on window, that's where global variables be (trac-6170)
            if ( ontype && isFunction( elem[ type ] ) && !isWindow( elem ) ) {

                // Don't re-trigger an onFOO event when we call its FOO() method
                tmp = elem[ ontype ];

                if ( tmp ) {
                    elem[ ontype ] = null;
                }

                // Prevent re-triggering of the same event, since we already bubbled it above
                jQuery.event.triggered = type;

                if ( event.isPropagationStopped() ) {
                    lastElement.addEventListener( type, stopPropagationCallback );
                }

                elem[ type ]();

                if ( event.isPropagationStopped() ) {
                    lastElement.removeEventListener( type, stopPropagationCallback );
                }

                jQuery.event.triggered = undefined;

                if ( tmp ) {
                    elem[ ontype ] = tmp;
                }
            }
        }
    }

    return event.result;
},

// Piggyback on a donor event to simulate a different one
// Used only for `focus(in | out)` events
simulate: function( type, elem, event ) {
    var e = jQuery.extend(
        new jQuery.Event(),
        event,
        {
            type: type,
            isSimulated: true
        }
    );

    jQuery.event.trigger( e, null, elem );
}

} );

jQuery.fn.extend( {

trigger: function( type, data ) {
    return this.each( function() {
        jQuery.event.trigger( type, data, this );
    } );
},
triggerHandler: function( type, data ) {
    var elem = this[ 0 ];
    if ( elem ) {
        return jQuery.event.trigger( type, data, elem, true );
    }
}
} );


// Support: Firefox <=44
// Firefox doesn't have focus(in | out) events
// Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
//
// Support: Chrome <=48 - 49, Safari <=9.0 - 9.1
// focus(in | out) events fire after focus & blur events,
// which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
// Related ticket - https://bugs.chromium.org/p/chromium/issues/detail?id=449857
if ( !support.focusin ) {
jQuery.each( { focus: "focusin", blur: "focusout" }, function( orig, fix ) {

    // Attach a single capturing handler on the document while someone wants focusin/focusout
    var handler = function( event ) {
        jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ) );
    };

    jQuery.event.special[ fix ] = {
        setup: function() {

            // Handle: regular nodes (via `this.ownerDocument`), window
            // (via `this.document`) & document (via `this`).
            var doc = this.ownerDocument || this.document || this,
                attaches = dataPriv.access( doc, fix );

            if ( !attaches ) {
                doc.addEventListener( orig, handler, true );
            }
            dataPriv.access( doc, fix, ( attaches || 0 ) + 1 );
        },
        teardown: function() {
            var doc = this.ownerDocument || this.document || this,
                attaches = dataPriv.access( doc, fix ) - 1;

            if ( !attaches ) {
                doc.removeEventListener( orig, handler, true );
                dataPriv.remove( doc, fix );

            } else {
                dataPriv.access( doc, fix, attaches );
            }
        }
    };
} );
}
var location = window.location;

var nonce = { guid: Date.now() };

var rquery = ( /\?/ );



// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
var xml, parserErrorElem;
if ( !data || typeof data !== "string" ) {
    return null;
}

// Support: IE 9 - 11 only
// IE throws on parseFromString with invalid input.
try {
    xml = ( new window.DOMParser() ).parseFromString( data, "text/xml" );
} catch ( e ) {}

parserErrorElem = xml && xml.getElementsByTagName( "parsererror" )[ 0 ];
if ( !xml || parserErrorElem ) {
    jQuery.error( "Invalid XML: " + (
        parserErrorElem ?
            jQuery.map( parserErrorElem.childNodes, function( el ) {
                return el.textContent;
            } ).join( "\n" ) :
            data
    ) );
}
return xml;
};


var
rbracket = /\[\]$/,
rCRLF = /\r?\n/g,
rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
var name;

if ( Array.isArray( obj ) ) {

    // Serialize array item.
    jQuery.each( obj, function( i, v ) {
        if ( traditional || rbracket.test( prefix ) ) {

            // Treat each array item as a scalar.
            add( prefix, v );

        } else {

            // Item is non-scalar (array or object), encode its numeric index.
            buildParams(
                prefix + "[" + ( typeof v === "object" && v != null ? i : "" ) + "]",
                v,
                traditional,
                add
            );
        }
    } );

} else if ( !traditional && toType( obj ) === "object" ) {

    // Serialize object item.
    for ( name in obj ) {
        buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
    }

} else {

    // Serialize scalar item.
    add( prefix, obj );
}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
var prefix,
    s = [],
    add = function( key, valueOrFunction ) {

        // If value is a function, invoke it and use its return value
        var value = isFunction( valueOrFunction ) ?
            valueOrFunction() :
            valueOrFunction;

        s[ s.length ] = encodeURIComponent( key ) + "=" +
            encodeURIComponent( value == null ? "" : value );
    };

if ( a == null ) {
    return "";
}

// If an array was passed in, assume that it is an array of form elements.
if ( Array.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {

    // Serialize the form elements
    jQuery.each( a, function() {
        add( this.name, this.value );
    } );

} else {

    // If traditional, encode the "old" way (the way 1.3.2 or older
    // did it), otherwise encode params recursively.
    for ( prefix in a ) {
        buildParams( prefix, a[ prefix ], traditional, add );
    }
}

// Return the resulting serialization
return s.join( "&" );
};

jQuery.fn.extend( {
serialize: function() {
    return jQuery.param( this.serializeArray() );
},
serializeArray: function() {
    return this.map( function() {

        // Can add propHook for "elements" to filter or add form elements
        var elements = jQuery.prop( this, "elements" );
        return elements ? jQuery.makeArray( elements ) : this;
    } ).filter( function() {
        var type = this.type;

        // Use .is( ":disabled" ) so that fieldset[disabled] works
        return this.name && !jQuery( this ).is( ":disabled" ) &&
            rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
            ( this.checked || !rcheckableType.test( type ) );
    } ).map( function( _i, elem ) {
        var val = jQuery( this ).val();

        if ( val == null ) {
            return null;
        }

        if ( Array.isArray( val ) ) {
            return jQuery.map( val, function( val ) {
                return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
            } );
        }

        return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
    } ).get();
}
} );


var
r20 = /%20/g,
rhash = /#.*$/,
rantiCache = /([?&])_=[^&]*/,
rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,

// trac-7653, trac-8125, trac-8152: local protocol detection
rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
rnoContent = /^(?:GET|HEAD)$/,
rprotocol = /^\/\//,

/* Prefilters
 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
 * 2) These are called:
 *    - BEFORE asking for a transport
 *    - AFTER param serialization (s.data is a string if s.processData is true)
 * 3) key is the dataType
 * 4) the catchall symbol "*" can be used
 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
 */
prefilters = {},

/* Transports bindings
 * 1) key is the dataType
 * 2) the catchall symbol "*" can be used
 * 3) selection will start with transport dataType and THEN go to "*" if needed
 */
transports = {},

// Avoid comment-prolog char sequence (trac-10098); must appease lint and evade compression
allTypes = "*/".concat( "*" ),

// Anchor tag for parsing the document origin
originAnchor = document.createElement( "a" );

originAnchor.href = location.href;

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

// dataTypeExpression is optional and defaults to "*"
return function( dataTypeExpression, func ) {

    if ( typeof dataTypeExpression !== "string" ) {
        func = dataTypeExpression;
        dataTypeExpression = "*";
    }

    var dataType,
        i = 0,
        dataTypes = dataTypeExpression.toLowerCase().match( rnothtmlwhite ) || [];

    if ( isFunction( func ) ) {

        // For each dataType in the dataTypeExpression
        while ( ( dataType = dataTypes[ i++ ] ) ) {

            // Prepend if requested
            if ( dataType[ 0 ] === "+" ) {
                dataType = dataType.slice( 1 ) || "*";
                ( structure[ dataType ] = structure[ dataType ] || [] ).unshift( func );

            // Otherwise append
            } else {
                ( structure[ dataType ] = structure[ dataType ] || [] ).push( func );
            }
        }
    }
};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

var inspected = {},
    seekingTransport = ( structure === transports );

function inspect( dataType ) {
    var selected;
    inspected[ dataType ] = true;
    jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
        var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
        if ( typeof dataTypeOrTransport === "string" &&
            !seekingTransport && !inspected[ dataTypeOrTransport ] ) {

            options.dataTypes.unshift( dataTypeOrTransport );
            inspect( dataTypeOrTransport );
            return false;
        } else if ( seekingTransport ) {
            return !( selected = dataTypeOrTransport );
        }
    } );
    return selected;
}

return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes trac-9887
function ajaxExtend( target, src ) {
var key, deep,
    flatOptions = jQuery.ajaxSettings.flatOptions || {};

for ( key in src ) {
    if ( src[ key ] !== undefined ) {
        ( flatOptions[ key ] ? target : ( deep || ( deep = {} ) ) )[ key ] = src[ key ];
    }
}
if ( deep ) {
    jQuery.extend( true, target, deep );
}

return target;
}

/* Handles responses to an ajax request:
* - finds the right dataType (mediates between content-type and expected dataType)
* - returns the corresponding response
*/
function ajaxHandleResponses( s, jqXHR, responses ) {

var ct, type, finalDataType, firstDataType,
    contents = s.contents,
    dataTypes = s.dataTypes;

// Remove auto dataType and get content-type in the process
while ( dataTypes[ 0 ] === "*" ) {
    dataTypes.shift();
    if ( ct === undefined ) {
        ct = s.mimeType || jqXHR.getResponseHeader( "Content-Type" );
    }
}

// Check if we're dealing with a known content-type
if ( ct ) {
    for ( type in contents ) {
        if ( contents[ type ] && contents[ type ].test( ct ) ) {
            dataTypes.unshift( type );
            break;
        }
    }
}

// Check to see if we have a response for the expected dataType
if ( dataTypes[ 0 ] in responses ) {
    finalDataType = dataTypes[ 0 ];
} else {

    // Try convertible dataTypes
    for ( type in responses ) {
        if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[ 0 ] ] ) {
            finalDataType = type;
            break;
        }
        if ( !firstDataType ) {
            firstDataType = type;
        }
    }

    // Or just use first one
    finalDataType = finalDataType || firstDataType;
}

// If we found a dataType
// We add the dataType to the list if needed
// and return the corresponding response
if ( finalDataType ) {
    if ( finalDataType !== dataTypes[ 0 ] ) {
        dataTypes.unshift( finalDataType );
    }
    return responses[ finalDataType ];
}
}

/* Chain conversions given the request and the original response
* Also sets the responseXXX fields on the jqXHR instance
*/
function ajaxConvert( s, response, jqXHR, isSuccess ) {
var conv2, current, conv, tmp, prev,
    converters = {},

    // Work with a copy of dataTypes in case we need to modify it for conversion
    dataTypes = s.dataTypes.slice();

// Create converters map with lowercased keys
if ( dataTypes[ 1 ] ) {
    for ( conv in s.converters ) {
        converters[ conv.toLowerCase() ] = s.converters[ conv ];
    }
}

current = dataTypes.shift();

// Convert to each sequential dataType
while ( current ) {

    if ( s.responseFields[ current ] ) {
        jqXHR[ s.responseFields[ current ] ] = response;
    }

    // Apply the dataFilter if provided
    if ( !prev && isSuccess && s.dataFilter ) {
        response = s.dataFilter( response, s.dataType );
    }

    prev = current;
    current = dataTypes.shift();

    if ( current ) {

        // There's only work to do if current dataType is non-auto
        if ( current === "*" ) {

            current = prev;

        // Convert response if prev dataType is non-auto and differs from current
        } else if ( prev !== "*" && prev !== current ) {

            // Seek a direct converter
            conv = converters[ prev + " " + current ] || converters[ "* " + current ];

            // If none found, seek a pair
            if ( !conv ) {
                for ( conv2 in converters ) {

                    // If conv2 outputs current
                    tmp = conv2.split( " " );
                    if ( tmp[ 1 ] === current ) {

                        // If prev can be converted to accepted input
                        conv = converters[ prev + " " + tmp[ 0 ] ] ||
                            converters[ "* " + tmp[ 0 ] ];
                        if ( conv ) {

                            // Condense equivalence converters
                            if ( conv === true ) {
                                conv = converters[ conv2 ];

                            // Otherwise, insert the intermediate dataType
                            } else if ( converters[ conv2 ] !== true ) {
                                current = tmp[ 0 ];
                                dataTypes.unshift( tmp[ 1 ] );
                            }
                            break;
                        }
                    }
                }
            }

            // Apply converter (if not an equivalence)
            if ( conv !== true ) {

                // Unless errors are allowed to bubble, catch and return them
                if ( conv && s.throws ) {
                    response = conv( response );
                } else {
                    try {
                        response = conv( response );
                    } catch ( e ) {
                        return {
                            state: "parsererror",
                            error: conv ? e : "No conversion from " + prev + " to " + current
                        };
                    }
                }
            }
        }
    }
}

return { state: "success", data: response };
}

jQuery.extend( {

// Counter for holding the number of active queries
active: 0,

// Last-Modified header cache for next request
lastModified: {},
etag: {},

ajaxSettings: {
    url: location.href,
    type: "GET",
    isLocal: rlocalProtocol.test( location.protocol ),
    global: true,
    processData: true,
    async: true,
    contentType: "application/x-www-form-urlencoded; charset=UTF-8",

    /*
    timeout: 0,
    data: null,
    dataType: null,
    username: null,
    password: null,
    cache: null,
    throws: false,
    traditional: false,
    headers: {},
    */

    accepts: {
        "*": allTypes,
        text: "text/plain",
        html: "text/html",
        xml: "application/xml, text/xml",
        json: "application/json, text/javascript"
    },

    contents: {
        xml: /\bxml\b/,
        html: /\bhtml/,
        json: /\bjson\b/
    },

    responseFields: {
        xml: "responseXML",
        text: "responseText",
        json: "responseJSON"
    },

    // Data converters
    // Keys separate source (or catchall "*") and destination types with a single space
    converters: {

        // Convert anything to text
        "* text": String,

        // Text to html (true = no transformation)
        "text html": true,

        // Evaluate text as a json expression
        "text json": JSON.parse,

        // Parse text as xml
        "text xml": jQuery.parseXML
    },

    // For options that shouldn't be deep extended:
    // you can add your own custom options here if
    // and when you create one that shouldn't be
    // deep extended (see ajaxExtend)
    flatOptions: {
        url: true,
        context: true
    }
},

// Creates a full fledged settings object into target
// with both ajaxSettings and settings fields.
// If target is omitted, writes into ajaxSettings.
ajaxSetup: function( target, settings ) {
    return settings ?

        // Building a settings object
        ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

        // Extending ajaxSettings
        ajaxExtend( jQuery.ajaxSettings, target );
},

ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
ajaxTransport: addToPrefiltersOrTransports( transports ),

// Main method
ajax: function( url, options ) {

    // If url is an object, simulate pre-1.5 signature
    if ( typeof url === "object" ) {
        options = url;
        url = undefined;
    }

    // Force options to be an object
    options = options || {};

    var transport,

        // URL without anti-cache param
        cacheURL,

        // Response headers
        responseHeadersString,
        responseHeaders,

        // timeout handle
        timeoutTimer,

        // Url cleanup var
        urlAnchor,

        // Request state (becomes false upon send and true upon completion)
        completed,

        // To know if global events are to be dispatched
        fireGlobals,

        // Loop variable
        i,

        // uncached part of the url
        uncached,

        // Create the final options object
        s = jQuery.ajaxSetup( {}, options ),

        // Callbacks context
        callbackContext = s.context || s,

        // Context for global events is callbackContext if it is a DOM node or jQuery collection
        globalEventContext = s.context &&
            ( callbackContext.nodeType || callbackContext.jquery ) ?
            jQuery( callbackContext ) :
            jQuery.event,

        // Deferreds
        deferred = jQuery.Deferred(),
        completeDeferred = jQuery.Callbacks( "once memory" ),

        // Status-dependent callbacks
        statusCode = s.statusCode || {},

        // Headers (they are sent all at once)
        requestHeaders = {},
        requestHeadersNames = {},

        // Default abort message
        strAbort = "canceled",

        // Fake xhr
        jqXHR = {
            readyState: 0,

            // Builds headers hashtable if needed
            getResponseHeader: function( key ) {
                var match;
                if ( completed ) {
                    if ( !responseHeaders ) {
                        responseHeaders = {};
                        while ( ( match = rheaders.exec( responseHeadersString ) ) ) {
                            responseHeaders[ match[ 1 ].toLowerCase() + " " ] =
                                ( responseHeaders[ match[ 1 ].toLowerCase() + " " ] || [] )
                                    .concat( match[ 2 ] );
                        }
                    }
                    match = responseHeaders[ key.toLowerCase() + " " ];
                }
                return match == null ? null : match.join( ", " );
            },

            // Raw string
            getAllResponseHeaders: function() {
                return completed ? responseHeadersString : null;
            },

            // Caches the header
            setRequestHeader: function( name, value ) {
                if ( completed == null ) {
                    name = requestHeadersNames[ name.toLowerCase() ] =
                        requestHeadersNames[ name.toLowerCase() ] || name;
                    requestHeaders[ name ] = value;
                }
                return this;
            },

            // Overrides response content-type header
            overrideMimeType: function( type ) {
                if ( completed == null ) {
                    s.mimeType = type;
                }
                return this;
            },

            // Status-dependent callbacks
            statusCode: function( map ) {
                var code;
                if ( map ) {
                    if ( completed ) {

                        // Execute the appropriate callbacks
                        jqXHR.always( map[ jqXHR.status ] );
                    } else {

                        // Lazy-add the new callbacks in a way that preserves old ones
                        for ( code in map ) {
                            statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
                        }
                    }
                }
                return this;
            },

            // Cancel the request
            abort: function( statusText ) {
                var finalText = statusText || strAbort;
                if ( transport ) {
                    transport.abort( finalText );
                }
                done( 0, finalText );
                return this;
            }
        };

    // Attach deferreds
    deferred.promise( jqXHR );

    // Add protocol if not provided (prefilters might expect it)
    // Handle falsy url in the settings object (trac-10093: consistency with old signature)
    // We also use the url parameter if available
    s.url = ( ( url || s.url || location.href ) + "" )
        .replace( rprotocol, location.protocol + "//" );

    // Alias method option to type as per ticket trac-12004
    s.type = options.method || options.type || s.method || s.type;

    // Extract dataTypes list
    s.dataTypes = ( s.dataType || "*" ).toLowerCase().match( rnothtmlwhite ) || [ "" ];

    // A cross-domain request is in order when the origin doesn't match the current origin.
    if ( s.crossDomain == null ) {
        urlAnchor = document.createElement( "a" );

        // Support: IE <=8 - 11, Edge 12 - 15
        // IE throws exception on accessing the href property if url is malformed,
        // e.g. http://example.com:80x/
        try {
            urlAnchor.href = s.url;

            // Support: IE <=8 - 11 only
            // Anchor's host property isn't correctly set when s.url is relative
            urlAnchor.href = urlAnchor.href;
            s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !==
                urlAnchor.protocol + "//" + urlAnchor.host;
        } catch ( e ) {

            // If there is an error parsing the URL, assume it is crossDomain,
            // it can be rejected by the transport if it is invalid
            s.crossDomain = true;
        }
    }

    // Convert data if not already a string
    if ( s.data && s.processData && typeof s.data !== "string" ) {
        s.data = jQuery.param( s.data, s.traditional );
    }

    // Apply prefilters
    inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

    // If request was aborted inside a prefilter, stop there
    if ( completed ) {
        return jqXHR;
    }

    // We can fire global events as of now if asked to
    // Don't fire events if jQuery.event is undefined in an AMD-usage scenario (trac-15118)
    fireGlobals = jQuery.event && s.global;

    // Watch for a new set of requests
    if ( fireGlobals && jQuery.active++ === 0 ) {
        jQuery.event.trigger( "ajaxStart" );
    }

    // Uppercase the type
    s.type = s.type.toUpperCase();

    // Determine if request has content
    s.hasContent = !rnoContent.test( s.type );

    // Save the URL in case we're toying with the If-Modified-Since
    // and/or If-None-Match header later on
    // Remove hash to simplify url manipulation
    cacheURL = s.url.replace( rhash, "" );

    // More options handling for requests with no content
    if ( !s.hasContent ) {

        // Remember the hash so we can put it back
        uncached = s.url.slice( cacheURL.length );

        // If data is available and should be processed, append data to url
        if ( s.data && ( s.processData || typeof s.data === "string" ) ) {
            cacheURL += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data;

            // trac-9682: remove data so that it's not used in an eventual retry
            delete s.data;
        }

        // Add or update anti-cache param if needed
        if ( s.cache === false ) {
            cacheURL = cacheURL.replace( rantiCache, "$1" );
            uncached = ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + ( nonce.guid++ ) +
                uncached;
        }

        // Put hash and anti-cache on the URL that will be requested (gh-1732)
        s.url = cacheURL + uncached;

    // Change '%20' to '+' if this is encoded form body content (gh-2658)
    } else if ( s.data && s.processData &&
        ( s.contentType || "" ).indexOf( "application/x-www-form-urlencoded" ) === 0 ) {
        s.data = s.data.replace( r20, "+" );
    }

    // Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
    if ( s.ifModified ) {
        if ( jQuery.lastModified[ cacheURL ] ) {
            jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
        }
        if ( jQuery.etag[ cacheURL ] ) {
            jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
        }
    }

    // Set the correct header, if data is being sent
    if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
        jqXHR.setRequestHeader( "Content-Type", s.contentType );
    }

    // Set the Accepts header for the server, depending on the dataType
    jqXHR.setRequestHeader(
        "Accept",
        s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[ 0 ] ] ?
            s.accepts[ s.dataTypes[ 0 ] ] +
                ( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
            s.accepts[ "*" ]
    );

    // Check for headers option
    for ( i in s.headers ) {
        jqXHR.setRequestHeader( i, s.headers[ i ] );
    }

    // Allow custom headers/mimetypes and early abort
    if ( s.beforeSend &&
        ( s.beforeSend.call( callbackContext, jqXHR, s ) === false || completed ) ) {

        // Abort if not done already and return
        return jqXHR.abort();
    }

    // Aborting is no longer a cancellation
    strAbort = "abort";

    // Install callbacks on deferreds
    completeDeferred.add( s.complete );
    jqXHR.done( s.success );
    jqXHR.fail( s.error );

    // Get transport
    transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

    // If no transport, we auto-abort
    if ( !transport ) {
        done( -1, "No Transport" );
    } else {
        jqXHR.readyState = 1;

        // Send global event
        if ( fireGlobals ) {
            globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
        }

        // If request was aborted inside ajaxSend, stop there
        if ( completed ) {
            return jqXHR;
        }

        // Timeout
        if ( s.async && s.timeout > 0 ) {
            timeoutTimer = window.setTimeout( function() {
                jqXHR.abort( "timeout" );
            }, s.timeout );
        }

        try {
            completed = false;
            transport.send( requestHeaders, done );
        } catch ( e ) {

            // Rethrow post-completion exceptions
            if ( completed ) {
                throw e;
            }

            // Propagate others as results
            done( -1, e );
        }
    }

    // Callback for when everything is done
    function done( status, nativeStatusText, responses, headers ) {
        var isSuccess, success, error, response, modified,
            statusText = nativeStatusText;

        // Ignore repeat invocations
        if ( completed ) {
            return;
        }

        completed = true;

        // Clear timeout if it exists
        if ( timeoutTimer ) {
            window.clearTimeout( timeoutTimer );
        }

        // Dereference transport for early garbage collection
        // (no matter how long the jqXHR object will be used)
        transport = undefined;

        // Cache response headers
        responseHeadersString = headers || "";

        // Set readyState
        jqXHR.readyState = status > 0 ? 4 : 0;

        // Determine if successful
        isSuccess = status >= 200 && status < 300 || status === 304;

        // Get response data
        if ( responses ) {
            response = ajaxHandleResponses( s, jqXHR, responses );
        }

        // Use a noop converter for missing script but not if jsonp
        if ( !isSuccess &&
            jQuery.inArray( "script", s.dataTypes ) > -1 &&
            jQuery.inArray( "json", s.dataTypes ) < 0 ) {
            s.converters[ "text script" ] = function() {};
        }

        // Convert no matter what (that way responseXXX fields are always set)
        response = ajaxConvert( s, response, jqXHR, isSuccess );

        // If successful, handle type chaining
        if ( isSuccess ) {

            // Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
            if ( s.ifModified ) {
                modified = jqXHR.getResponseHeader( "Last-Modified" );
                if ( modified ) {
                    jQuery.lastModified[ cacheURL ] = modified;
                }
                modified = jqXHR.getResponseHeader( "etag" );
                if ( modified ) {
                    jQuery.etag[ cacheURL ] = modified;
                }
            }

            // if no content
            if ( status === 204 || s.type === "HEAD" ) {
                statusText = "nocontent";

            // if not modified
            } else if ( status === 304 ) {
                statusText = "notmodified";

            // If we have data, let's convert it
            } else {
                statusText = response.state;
                success = response.data;
                error = response.error;
                isSuccess = !error;
            }
        } else {

            // Extract error from statusText and normalize for non-aborts
            error = statusText;
            if ( status || !statusText ) {
                statusText = "error";
                if ( status < 0 ) {
                    status = 0;
                }
            }
        }

        // Set data for the fake xhr object
        jqXHR.status = status;
        jqXHR.statusText = ( nativeStatusText || statusText ) + "";

        // Success/Error
        if ( isSuccess ) {
            deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
        } else {
            deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
        }

        // Status-dependent callbacks
        jqXHR.statusCode( statusCode );
        statusCode = undefined;

        if ( fireGlobals ) {
            globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
                [ jqXHR, s, isSuccess ? success : error ] );
        }

        // Complete
        completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

        if ( fireGlobals ) {
            globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );

            // Handle the global AJAX counter
            if ( !( --jQuery.active ) ) {
                jQuery.event.trigger( "ajaxStop" );
            }
        }
    }

    return jqXHR;
},

getJSON: function( url, data, callback ) {
    return jQuery.get( url, data, callback, "json" );
},

getScript: function( url, callback ) {
    return jQuery.get( url, undefined, callback, "script" );
}
} );

jQuery.each( [ "get", "post" ], function( _i, method ) {
jQuery[ method ] = function( url, data, callback, type ) {

    // Shift arguments if data argument was omitted
    if ( isFunction( data ) ) {
        type = type || callback;
        callback = data;
        data = undefined;
    }

    // The url can be an options object (which then must have .url)
    return jQuery.ajax( jQuery.extend( {
        url: url,
        type: method,
        dataType: type,
        data: data,
        success: callback
    }, jQuery.isPlainObject( url ) && url ) );
};
} );

jQuery.ajaxPrefilter( function( s ) {
var i;
for ( i in s.headers ) {
    if ( i.toLowerCase() === "content-type" ) {
        s.contentType = s.headers[ i ] || "";
    }
}
} );


jQuery._evalUrl = function( url, options, doc ) {
return jQuery.ajax( {
    url: url,

    // Make this explicit, since user can override this through ajaxSetup (trac-11264)
    type: "GET",
    dataType: "script",
    cache: true,
    async: false,
    global: false,

    // Only evaluate the response if it is successful (gh-4126)
    // dataFilter is not invoked for failure responses, so using it instead
    // of the default converter is kludgy but it works.
    converters: {
        "text script": function() {}
    },
    dataFilter: function( response ) {
        jQuery.globalEval( response, options, doc );
    }
} );
};


jQuery.fn.extend( {
wrapAll: function( html ) {
    var wrap;

    if ( this[ 0 ] ) {
        if ( isFunction( html ) ) {
            html = html.call( this[ 0 ] );
        }

        // The elements to wrap the target around
        wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

        if ( this[ 0 ].parentNode ) {
            wrap.insertBefore( this[ 0 ] );
        }

        wrap.map( function() {
            var elem = this;

            while ( elem.firstElementChild ) {
                elem = elem.firstElementChild;
            }

            return elem;
        } ).append( this );
    }

    return this;
},

wrapInner: function( html ) {
    if ( isFunction( html ) ) {
        return this.each( function( i ) {
            jQuery( this ).wrapInner( html.call( this, i ) );
        } );
    }

    return this.each( function() {
        var self = jQuery( this ),
            contents = self.contents();

        if ( contents.length ) {
            contents.wrapAll( html );

        } else {
            self.append( html );
        }
    } );
},

wrap: function( html ) {
    var htmlIsFunction = isFunction( html );

    return this.each( function( i ) {
        jQuery( this ).wrapAll( htmlIsFunction ? html.call( this, i ) : html );
    } );
},

unwrap: function( selector ) {
    this.parent( selector ).not( "body" ).each( function() {
        jQuery( this ).replaceWith( this.childNodes );
    } );
    return this;
}
} );


jQuery.expr.pseudos.hidden = function( elem ) {
return !jQuery.expr.pseudos.visible( elem );
};
jQuery.expr.pseudos.visible = function( elem ) {
return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
};




jQuery.ajaxSettings.xhr = function() {
try {
    return new window.XMLHttpRequest();
} catch ( e ) {}
};

var xhrSuccessStatus = {

    // File protocol always yields status code 0, assume 200
    0: 200,

    // Support: IE <=9 only
    // trac-1450: sometimes IE returns 1223 when it should be 204
    1223: 204
},
xhrSupported = jQuery.ajaxSettings.xhr();

support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
support.ajax = xhrSupported = !!xhrSupported;

jQuery.ajaxTransport( function( options ) {
var callback, errorCallback;

// Cross domain only allowed if supported through XMLHttpRequest
if ( support.cors || xhrSupported && !options.crossDomain ) {
    return {
        send: function( headers, complete ) {
            var i,
                xhr = options.xhr();

            xhr.open(
                options.type,
                options.url,
                options.async,
                options.username,
                options.password
            );

            // Apply custom fields if provided
            if ( options.xhrFields ) {
                for ( i in options.xhrFields ) {
                    xhr[ i ] = options.xhrFields[ i ];
                }
            }

            // Override mime type if needed
            if ( options.mimeType && xhr.overrideMimeType ) {
                xhr.overrideMimeType( options.mimeType );
            }

            // X-Requested-With header
            // For cross-domain requests, seeing as conditions for a preflight are
            // akin to a jigsaw puzzle, we simply never set it to be sure.
            // (it can always be set on a per-request basis or even using ajaxSetup)
            // For same-domain requests, won't change header if already provided.
            if ( !options.crossDomain && !headers[ "X-Requested-With" ] ) {
                headers[ "X-Requested-With" ] = "XMLHttpRequest";
            }

            // Set headers
            for ( i in headers ) {
                xhr.setRequestHeader( i, headers[ i ] );
            }

            // Callback
            callback = function( type ) {
                return function() {
                    if ( callback ) {
                        callback = errorCallback = xhr.onload =
                            xhr.onerror = xhr.onabort = xhr.ontimeout =
                                xhr.onreadystatechange = null;

                        if ( type === "abort" ) {
                            xhr.abort();
                        } else if ( type === "error" ) {

                            // Support: IE <=9 only
                            // On a manual native abort, IE9 throws
                            // errors on any property access that is not readyState
                            if ( typeof xhr.status !== "number" ) {
                                complete( 0, "error" );
                            } else {
                                complete(

                                    // File: protocol always yields status 0; see trac-8605, trac-14207
                                    xhr.status,
                                    xhr.statusText
                                );
                            }
                        } else {
                            complete(
                                xhrSuccessStatus[ xhr.status ] || xhr.status,
                                xhr.statusText,

                                // Support: IE <=9 only
                                // IE9 has no XHR2 but throws on binary (trac-11426)
                                // For XHR2 non-text, let the caller handle it (gh-2498)
                                ( xhr.responseType || "text" ) !== "text"  ||
                                typeof xhr.responseText !== "string" ?
                                    { binary: xhr.response } :
                                    { text: xhr.responseText },
                                xhr.getAllResponseHeaders()
                            );
                        }
                    }
                };
            };

            // Listen to events
            xhr.onload = callback();
            errorCallback = xhr.onerror = xhr.ontimeout = callback( "error" );

            // Support: IE 9 only
            // Use onreadystatechange to replace onabort
            // to handle uncaught aborts
            if ( xhr.onabort !== undefined ) {
                xhr.onabort = errorCallback;
            } else {
                xhr.onreadystatechange = function() {

                    // Check readyState before timeout as it changes
                    if ( xhr.readyState === 4 ) {

                        // Allow onerror to be called first,
                        // but that will not handle a native abort
                        // Also, save errorCallback to a variable
                        // as xhr.onerror cannot be accessed
                        window.setTimeout( function() {
                            if ( callback ) {
                                errorCallback();
                            }
                        } );
                    }
                };
            }

            // Create the abort callback
            callback = callback( "abort" );

            try {

                // Do send the request (this may raise an exception)
                xhr.send( options.hasContent && options.data || null );
            } catch ( e ) {

                // trac-14683: Only rethrow if this hasn't been notified as an error yet
                if ( callback ) {
                    throw e;
                }
            }
        },

        abort: function() {
            if ( callback ) {
                callback();
            }
        }
    };
}
} );




// Prevent auto-execution of scripts when no explicit dataType was provided (See gh-2432)
jQuery.ajaxPrefilter( function( s ) {
if ( s.crossDomain ) {
    s.contents.script = false;
}
} );

// Install script dataType
jQuery.ajaxSetup( {
accepts: {
    script: "text/javascript, application/javascript, " +
        "application/ecmascript, application/x-ecmascript"
},
contents: {
    script: /\b(?:java|ecma)script\b/
},
converters: {
    "text script": function( text ) {
        jQuery.globalEval( text );
        return text;
    }
}
} );

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
if ( s.cache === undefined ) {
    s.cache = false;
}
if ( s.crossDomain ) {
    s.type = "GET";
}
} );

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {

// This transport only deals with cross domain or forced-by-attrs requests
if ( s.crossDomain || s.scriptAttrs ) {
    var script, callback;
    return {
        send: function( _, complete ) {
            script = jQuery( "<script>" )
                .attr( s.scriptAttrs || {} )
                .prop( { charset: s.scriptCharset, src: s.url } )
                .on( "load error", callback = function( evt ) {
                    script.remove();
                    callback = null;
                    if ( evt ) {
                        complete( evt.type === "error" ? 404 : 200, evt.type );
                    }
                } );

            // Use native DOM manipulation to avoid our domManip AJAX trickery
            document.head.appendChild( script[ 0 ] );
        },
        abort: function() {
            if ( callback ) {
                callback();
            }
        }
    };
}
} );




var oldCallbacks = [],
rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup( {
jsonp: "callback",
jsonpCallback: function() {
    var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce.guid++ ) );
    this[ callback ] = true;
    return callback;
}
} );

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

var callbackName, overwritten, responseContainer,
    jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
        "url" :
        typeof s.data === "string" &&
            ( s.contentType || "" )
                .indexOf( "application/x-www-form-urlencoded" ) === 0 &&
            rjsonp.test( s.data ) && "data"
    );

// Handle iff the expected data type is "jsonp" or we have a parameter to set
if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

    // Get callback name, remembering preexisting value associated with it
    callbackName = s.jsonpCallback = isFunction( s.jsonpCallback ) ?
        s.jsonpCallback() :
        s.jsonpCallback;

    // Insert callback into url or form data
    if ( jsonProp ) {
        s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
    } else if ( s.jsonp !== false ) {
        s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
    }

    // Use data converter to retrieve json after script execution
    s.converters[ "script json" ] = function() {
        if ( !responseContainer ) {
            jQuery.error( callbackName + " was not called" );
        }
        return responseContainer[ 0 ];
    };

    // Force json dataType
    s.dataTypes[ 0 ] = "json";

    // Install callback
    overwritten = window[ callbackName ];
    window[ callbackName ] = function() {
        responseContainer = arguments;
    };

    // Clean-up function (fires after converters)
    jqXHR.always( function() {

        // If previous value didn't exist - remove it
        if ( overwritten === undefined ) {
            jQuery( window ).removeProp( callbackName );

        // Otherwise restore preexisting value
        } else {
            window[ callbackName ] = overwritten;
        }

        // Save back as free
        if ( s[ callbackName ] ) {

            // Make sure that re-using the options doesn't screw things around
            s.jsonpCallback = originalSettings.jsonpCallback;

            // Save the callback name for future use
            oldCallbacks.push( callbackName );
        }

        // Call if it was a function and we have a response
        if ( responseContainer && isFunction( overwritten ) ) {
            overwritten( responseContainer[ 0 ] );
        }

        responseContainer = overwritten = undefined;
    } );

    // Delegate to script
    return "script";
}
} );




// Support: Safari 8 only
// In Safari 8 documents created via document.implementation.createHTMLDocument
// collapse sibling forms: the second one becomes a child of the first one.
// Because of that, this security measure has to be disabled in Safari 8.
// https://bugs.webkit.org/show_bug.cgi?id=137337
support.createHTMLDocument = ( function() {
var body = document.implementation.createHTMLDocument( "" ).body;
body.innerHTML = "<form></form><form></form>";
return body.childNodes.length === 2;
} )();


// Argument "data" should be string of html
// context (optional): If specified, the fragment will be created in this context,
// defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
if ( typeof data !== "string" ) {
    return [];
}
if ( typeof context === "boolean" ) {
    keepScripts = context;
    context = false;
}

var base, parsed, scripts;

if ( !context ) {

    // Stop scripts or inline event handlers from being executed immediately
    // by using document.implementation
    if ( support.createHTMLDocument ) {
        context = document.implementation.createHTMLDocument( "" );

        // Set the base href for the created document
        // so any parsed elements with URLs
        // are based on the document's URL (gh-2965)
        base = context.createElement( "base" );
        base.href = document.location.href;
        context.head.appendChild( base );
    } else {
        context = document;
    }
}

parsed = rsingleTag.exec( data );
scripts = !keepScripts && [];

// Single tag
if ( parsed ) {
    return [ context.createElement( parsed[ 1 ] ) ];
}

parsed = buildFragment( [ data ], context, scripts );

if ( scripts && scripts.length ) {
    jQuery( scripts ).remove();
}

return jQuery.merge( [], parsed.childNodes );
};


/**
* Load a url into a page
*/
jQuery.fn.load = function( url, params, callback ) {
var selector, type, response,
    self = this,
    off = url.indexOf( " " );

if ( off > -1 ) {
    selector = stripAndCollapse( url.slice( off ) );
    url = url.slice( 0, off );
}

// If it's a function
if ( isFunction( params ) ) {

    // We assume that it's the callback
    callback = params;
    params = undefined;

// Otherwise, build a param string
} else if ( params && typeof params === "object" ) {
    type = "POST";
}

// If we have elements to modify, make the request
if ( self.length > 0 ) {
    jQuery.ajax( {
        url: url,

        // If "type" variable is undefined, then "GET" method will be used.
        // Make value of this field explicit since
        // user can override it through ajaxSetup method
        type: type || "GET",
        dataType: "html",
        data: params
    } ).done( function( responseText ) {

        // Save response for use in complete callback
        response = arguments;

        self.html( selector ?

            // If a selector was specified, locate the right elements in a dummy div
            // Exclude scripts to avoid IE 'Permission Denied' errors
            jQuery( "<div>" ).append( jQuery.parseHTML( responseText ) ).find( selector ) :

            // Otherwise use the full result
            responseText );

    // If the request succeeds, this function gets "data", "status", "jqXHR"
    // but they are ignored because response was set above.
    // If it fails, this function gets "jqXHR", "status", "error"
    } ).always( callback && function( jqXHR, status ) {
        self.each( function() {
            callback.apply( this, response || [ jqXHR.responseText, status, jqXHR ] );
        } );
    } );
}

return this;
};




jQuery.expr.pseudos.animated = function( elem ) {
return jQuery.grep( jQuery.timers, function( fn ) {
    return elem === fn.elem;
} ).length;
};




jQuery.offset = {
setOffset: function( elem, options, i ) {
    var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
        position = jQuery.css( elem, "position" ),
        curElem = jQuery( elem ),
        props = {};

    // Set position first, in-case top/left are set even on static elem
    if ( position === "static" ) {
        elem.style.position = "relative";
    }

    curOffset = curElem.offset();
    curCSSTop = jQuery.css( elem, "top" );
    curCSSLeft = jQuery.css( elem, "left" );
    calculatePosition = ( position === "absolute" || position === "fixed" ) &&
        ( curCSSTop + curCSSLeft ).indexOf( "auto" ) > -1;

    // Need to be able to calculate position if either
    // top or left is auto and position is either absolute or fixed
    if ( calculatePosition ) {
        curPosition = curElem.position();
        curTop = curPosition.top;
        curLeft = curPosition.left;

    } else {
        curTop = parseFloat( curCSSTop ) || 0;
        curLeft = parseFloat( curCSSLeft ) || 0;
    }

    if ( isFunction( options ) ) {

        // Use jQuery.extend here to allow modification of coordinates argument (gh-1848)
        options = options.call( elem, i, jQuery.extend( {}, curOffset ) );
    }

    if ( options.top != null ) {
        props.top = ( options.top - curOffset.top ) + curTop;
    }
    if ( options.left != null ) {
        props.left = ( options.left - curOffset.left ) + curLeft;
    }

    if ( "using" in options ) {
        options.using.call( elem, props );

    } else {
        curElem.css( props );
    }
}
};

jQuery.fn.extend( {

// offset() relates an element's border box to the document origin
offset: function( options ) {

    // Preserve chaining for setter
    if ( arguments.length ) {
        return options === undefined ?
            this :
            this.each( function( i ) {
                jQuery.offset.setOffset( this, options, i );
            } );
    }

    var rect, win,
        elem = this[ 0 ];

    if ( !elem ) {
        return;
    }

    // Return zeros for disconnected and hidden (display: none) elements (gh-2310)
    // Support: IE <=11 only
    // Running getBoundingClientRect on a
    // disconnected node in IE throws an error
    if ( !elem.getClientRects().length ) {
        return { top: 0, left: 0 };
    }

    // Get document-relative position by adding viewport scroll to viewport-relative gBCR
    rect = elem.getBoundingClientRect();
    win = elem.ownerDocument.defaultView;
    return {
        top: rect.top + win.pageYOffset,
        left: rect.left + win.pageXOffset
    };
},

// position() relates an element's margin box to its offset parent's padding box
// This corresponds to the behavior of CSS absolute positioning
position: function() {
    if ( !this[ 0 ] ) {
        return;
    }

    var offsetParent, offset, doc,
        elem = this[ 0 ],
        parentOffset = { top: 0, left: 0 };

    // position:fixed elements are offset from the viewport, which itself always has zero offset
    if ( jQuery.css( elem, "position" ) === "fixed" ) {

        // Assume position:fixed implies availability of getBoundingClientRect
        offset = elem.getBoundingClientRect();

    } else {
        offset = this.offset();

        // Account for the *real* offset parent, which can be the document or its root element
        // when a statically positioned element is identified
        doc = elem.ownerDocument;
        offsetParent = elem.offsetParent || doc.documentElement;
        while ( offsetParent &&
            ( offsetParent === doc.body || offsetParent === doc.documentElement ) &&
            jQuery.css( offsetParent, "position" ) === "static" ) {

            offsetParent = offsetParent.parentNode;
        }
        if ( offsetParent && offsetParent !== elem && offsetParent.nodeType === 1 ) {

            // Incorporate borders into its offset, since they are outside its content origin
            parentOffset = jQuery( offsetParent ).offset();
            parentOffset.top += jQuery.css( offsetParent, "borderTopWidth", true );
            parentOffset.left += jQuery.css( offsetParent, "borderLeftWidth", true );
        }
    }

    // Subtract parent offsets and element margins
    return {
        top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
        left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
    };
},

// This method will return documentElement in the following cases:
// 1) For the element inside the iframe without offsetParent, this method will return
//    documentElement of the parent window
// 2) For the hidden or detached element
// 3) For body or html element, i.e. in case of the html node - it will return itself
//
// but those exceptions were never presented as a real life use-cases
// and might be considered as more preferable results.
//
// This logic, however, is not guaranteed and can change at any point in the future
offsetParent: function() {
    return this.map( function() {
        var offsetParent = this.offsetParent;

        while ( offsetParent && jQuery.css( offsetParent, "position" ) === "static" ) {
            offsetParent = offsetParent.offsetParent;
        }

        return offsetParent || documentElement;
    } );
}
} );

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
var top = "pageYOffset" === prop;

jQuery.fn[ method ] = function( val ) {
    return access( this, function( elem, method, val ) {

        // Coalesce documents and windows
        var win;
        if ( isWindow( elem ) ) {
            win = elem;
        } else if ( elem.nodeType === 9 ) {
            win = elem.defaultView;
        }

        if ( val === undefined ) {
            return win ? win[ prop ] : elem[ method ];
        }

        if ( win ) {
            win.scrollTo(
                !top ? val : win.pageXOffset,
                top ? val : win.pageYOffset
            );

        } else {
            elem[ method ] = val;
        }
    }, method, val, arguments.length );
};
} );

// Support: Safari <=7 - 9.1, Chrome <=37 - 49
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://bugs.chromium.org/p/chromium/issues/detail?id=589347
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( _i, prop ) {
jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
    function( elem, computed ) {
        if ( computed ) {
            computed = curCSS( elem, prop );

            // If curCSS returns percentage, fallback to offset
            return rnumnonpx.test( computed ) ?
                jQuery( elem ).position()[ prop ] + "px" :
                computed;
        }
    }
);
} );


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
jQuery.each( {
    padding: "inner" + name,
    content: type,
    "": "outer" + name
}, function( defaultExtra, funcName ) {

    // Margin is only for outerHeight, outerWidth
    jQuery.fn[ funcName ] = function( margin, value ) {
        var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
            extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

        return access( this, function( elem, type, value ) {
            var doc;

            if ( isWindow( elem ) ) {

                // $( window ).outerWidth/Height return w/h including scrollbars (gh-1729)
                return funcName.indexOf( "outer" ) === 0 ?
                    elem[ "inner" + name ] :
                    elem.document.documentElement[ "client" + name ];
            }

            // Get document width or height
            if ( elem.nodeType === 9 ) {
                doc = elem.documentElement;

                // Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
                // whichever is greatest
                return Math.max(
                    elem.body[ "scroll" + name ], doc[ "scroll" + name ],
                    elem.body[ "offset" + name ], doc[ "offset" + name ],
                    doc[ "client" + name ]
                );
            }

            return value === undefined ?

                // Get width or height on the element, requesting but not forcing parseFloat
                jQuery.css( elem, type, extra ) :

                // Set width or height on the element
                jQuery.style( elem, type, value, extra );
        }, type, chainable ? margin : undefined, chainable );
    };
} );
} );


jQuery.each( [
"ajaxStart",
"ajaxStop",
"ajaxComplete",
"ajaxError",
"ajaxSuccess",
"ajaxSend"
], function( _i, type ) {
jQuery.fn[ type ] = function( fn ) {
    return this.on( type, fn );
};
} );




jQuery.fn.extend( {

bind: function( types, data, fn ) {
    return this.on( types, null, data, fn );
},
unbind: function( types, fn ) {
    return this.off( types, null, fn );
},

delegate: function( selector, types, data, fn ) {
    return this.on( types, selector, data, fn );
},
undelegate: function( selector, types, fn ) {

    // ( namespace ) or ( selector, types [, fn] )
    return arguments.length === 1 ?
        this.off( selector, "**" ) :
        this.off( types, selector || "**", fn );
},

hover: function( fnOver, fnOut ) {
    return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
}
} );

jQuery.each(
( "blur focus focusin focusout resize scroll click dblclick " +
"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
"change select submit keydown keypress keyup contextmenu" ).split( " " ),
function( _i, name ) {

    // Handle event binding
    jQuery.fn[ name ] = function( data, fn ) {
        return arguments.length > 0 ?
            this.on( name, null, data, fn ) :
            this.trigger( name );
    };
}
);




// Support: Android <=4.0 only
// Make sure we trim BOM and NBSP
// Require that the "whitespace run" starts from a non-whitespace
// to avoid O(N^2) behavior when the engine would try matching "\s+$" at each space position.
var rtrim = /^[\s\uFEFF\xA0]+|([^\s\uFEFF\xA0])[\s\uFEFF\xA0]+$/g;

// Bind a function to a context, optionally partially applying any
// arguments.
// jQuery.proxy is deprecated to promote standards (specifically Function#bind)
// However, it is not slated for removal any time soon
jQuery.proxy = function( fn, context ) {
var tmp, args, proxy;

if ( typeof context === "string" ) {
    tmp = fn[ context ];
    context = fn;
    fn = tmp;
}

// Quick check to determine if target is callable, in the spec
// this throws a TypeError, but we will just return undefined.
if ( !isFunction( fn ) ) {
    return undefined;
}

// Simulated bind
args = slice.call( arguments, 2 );
proxy = function() {
    return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
};

// Set the guid of unique handler to the same of original handler, so it can be removed
proxy.guid = fn.guid = fn.guid || jQuery.guid++;

return proxy;
};

jQuery.holdReady = function( hold ) {
if ( hold ) {
    jQuery.readyWait++;
} else {
    jQuery.ready( true );
}
};
jQuery.isArray = Array.isArray;
jQuery.parseJSON = JSON.parse;
jQuery.nodeName = nodeName;
jQuery.isFunction = isFunction;
jQuery.isWindow = isWindow;
jQuery.camelCase = camelCase;
jQuery.type = toType;

jQuery.now = Date.now;

jQuery.isNumeric = function( obj ) {

// As of jQuery 3.0, isNumeric is limited to
// strings and numbers (primitives or objects)
// that can be coerced to finite numbers (gh-2662)
var type = jQuery.type( obj );
return ( type === "number" || type === "string" ) &&

    // parseFloat NaNs numeric-cast false positives ("")
    // ...but misinterprets leading-number strings, particularly hex literals ("0x...")
    // subtraction forces infinities to NaN
    !isNaN( obj - parseFloat( obj ) );
};

jQuery.trim = function( text ) {
return text == null ?
    "" :
    ( text + "" ).replace( rtrim, "$1" );
};



// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( typeof define === "function" && define.amd ) {
define( "jquery", [], function() {
    return jQuery;
} );
}




var

// Map over jQuery in case of overwrite
_jQuery = window.jQuery,

// Map over the $ in case of overwrite
_$ = window.$;

jQuery.noConflict = function( deep ) {
if ( window.$ === jQuery ) {
    window.$ = _$;
}

if ( deep && window.jQuery === jQuery ) {
    window.jQuery = _jQuery;
}

return jQuery;
};

// Expose jQuery and $ identifiers, even in AMD
// (trac-7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (trac-13566)
if ( typeof noGlobal === "undefined" ) {
window.jQuery = window.$ = jQuery;
}




return jQuery;
} );
</script>

<script>
    /*! WOW wow.js - v1.3.0 - 2016-10-04
* https://wowjs.uk
* Copyright (c) 2016 Thomas Grainger; Licensed MIT */!function(a,b){if("function"==typeof define&&define.amd)define(["module","exports"],b);else if("undefined"!=typeof exports)b(module,exports);else{var c={exports:{}};b(c,c.exports),a.WOW=c.exports}}(this,function(a,b){"use strict";function c(a,b){if(!(a instanceof b))throw new TypeError("Cannot call a class as a function")}function d(a,b){return b.indexOf(a)>=0}function e(a,b){for(var c in b)if(null==a[c]){var d=b[c];a[c]=d}return a}function f(a){return/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(a)}function g(a){var b=arguments.length<=1||void 0===arguments[1]?!1:arguments[1],c=arguments.length<=2||void 0===arguments[2]?!1:arguments[2],d=arguments.length<=3||void 0===arguments[3]?null:arguments[3],e=void 0;return null!=document.createEvent?(e=document.createEvent("CustomEvent"),e.initCustomEvent(a,b,c,d)):null!=document.createEventObject?(e=document.createEventObject(),e.eventType=a):e.eventName=a,e}function h(a,b){null!=a.dispatchEvent?a.dispatchEvent(b):b in(null!=a)?a[b]():"on"+b in(null!=a)&&a["on"+b]()}function i(a,b,c){null!=a.addEventListener?a.addEventListener(b,c,!1):null!=a.attachEvent?a.attachEvent("on"+b,c):a[b]=c}function j(a,b,c){null!=a.removeEventListener?a.removeEventListener(b,c,!1):null!=a.detachEvent?a.detachEvent("on"+b,c):delete a[b]}function k(){return"innerHeight"in window?window.innerHeight:document.documentElement.clientHeight}Object.defineProperty(b,"__esModule",{value:!0});var l,m,n=function(){function a(a,b){for(var c=0;c<b.length;c++){var d=b[c];d.enumerable=d.enumerable||!1,d.configurable=!0,"value"in d&&(d.writable=!0),Object.defineProperty(a,d.key,d)}}return function(b,c,d){return c&&a(b.prototype,c),d&&a(b,d),b}}(),o=window.WeakMap||window.MozWeakMap||function(){function a(){c(this,a),this.keys=[],this.values=[]}return n(a,[{key:"get",value:function(a){for(var b=0;b<this.keys.length;b++){var c=this.keys[b];if(c===a)return this.values[b]}}},{key:"set",value:function(a,b){for(var c=0;c<this.keys.length;c++){var d=this.keys[c];if(d===a)return this.values[c]=b,this}return this.keys.push(a),this.values.push(b),this}}]),a}(),p=window.MutationObserver||window.WebkitMutationObserver||window.MozMutationObserver||(m=l=function(){function a(){c(this,a),"undefined"!=typeof console&&null!==console&&(console.warn("MutationObserver is not supported by your browser."),console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content."))}return n(a,[{key:"observe",value:function(){}}]),a}(),l.notSupported=!0,m),q=window.getComputedStyle||function(a){var b=/(\-([a-z]){1})/g;return{getPropertyValue:function(c){"float"===c&&(c="styleFloat"),b.test(c)&&c.replace(b,function(a,b){return b.toUpperCase()});var d=a.currentStyle;return(null!=d?d[c]:void 0)||null}}},r=function(){function a(){var b=arguments.length<=0||void 0===arguments[0]?{}:arguments[0];c(this,a),this.defaults={boxClass:"wow",animateClass:"animated",offset:0,mobile:!0,live:!0,callback:null,scrollContainer:null,resetAnimation:!0},this.animate=function(){return"requestAnimationFrame"in window?function(a){return window.requestAnimationFrame(a)}:function(a){return a()}}(),this.vendors=["moz","webkit"],this.start=this.start.bind(this),this.resetAnimation=this.resetAnimation.bind(this),this.scrollHandler=this.scrollHandler.bind(this),this.scrollCallback=this.scrollCallback.bind(this),this.scrolled=!0,this.config=e(b,this.defaults),null!=b.scrollContainer&&(this.config.scrollContainer=document.querySelector(b.scrollContainer)),this.animationNameCache=new o,this.wowEvent=g(this.config.boxClass)}return n(a,[{key:"init",value:function(){this.element=window.document.documentElement,d(document.readyState,["interactive","complete"])?this.start():i(document,"DOMContentLoaded",this.start),this.finished=[]}},{key:"start",value:function(){var a=this;if(this.stopped=!1,this.boxes=[].slice.call(this.element.querySelectorAll("."+this.config.boxClass)),this.all=this.boxes.slice(0),this.boxes.length)if(this.disabled())this.resetStyle();else for(var b=0;b<this.boxes.length;b++){var c=this.boxes[b];this.applyStyle(c,!0)}if(this.disabled()||(i(this.config.scrollContainer||window,"scroll",this.scrollHandler),i(window,"resize",this.scrollHandler),this.interval=setInterval(this.scrollCallback,50)),this.config.live){var d=new p(function(b){for(var c=0;c<b.length;c++)for(var d=b[c],e=0;e<d.addedNodes.length;e++){var f=d.addedNodes[e];a.doSync(f)}});d.observe(document.body,{childList:!0,subtree:!0})}}},{key:"stop",value:function(){this.stopped=!0,j(this.config.scrollContainer||window,"scroll",this.scrollHandler),j(window,"resize",this.scrollHandler),null!=this.interval&&clearInterval(this.interval)}},{key:"sync",value:function(){p.notSupported&&this.doSync(this.element)}},{key:"doSync",value:function(a){if("undefined"!=typeof a&&null!==a||(a=this.element),1===a.nodeType){a=a.parentNode||a;for(var b=a.querySelectorAll("."+this.config.boxClass),c=0;c<b.length;c++){var e=b[c];d(e,this.all)||(this.boxes.push(e),this.all.push(e),this.stopped||this.disabled()?this.resetStyle():this.applyStyle(e,!0),this.scrolled=!0)}}}},{key:"show",value:function(a){return this.applyStyle(a),a.className=a.className+" "+this.config.animateClass,null!=this.config.callback&&this.config.callback(a),h(a,this.wowEvent),this.config.resetAnimation&&(i(a,"animationend",this.resetAnimation),i(a,"oanimationend",this.resetAnimation),i(a,"webkitAnimationEnd",this.resetAnimation),i(a,"MSAnimationEnd",this.resetAnimation)),a}},{key:"applyStyle",value:function(a,b){var c=this,d=a.getAttribute("data-wow-duration"),e=a.getAttribute("data-wow-delay"),f=a.getAttribute("data-wow-iteration");return this.animate(function(){return c.customStyle(a,b,d,e,f)})}},{key:"resetStyle",value:function(){for(var a=0;a<this.boxes.length;a++){var b=this.boxes[a];b.style.visibility="visible"}}},{key:"resetAnimation",value:function(a){if(a.type.toLowerCase().indexOf("animationend")>=0){var b=a.target||a.srcElement;b.className=b.className.replace(this.config.animateClass,"").trim()}}},{key:"customStyle",value:function(a,b,c,d,e){return b&&this.cacheAnimationName(a),a.style.visibility=b?"hidden":"visible",c&&this.vendorSet(a.style,{animationDuration:c}),d&&this.vendorSet(a.style,{animationDelay:d}),e&&this.vendorSet(a.style,{animationIterationCount:e}),this.vendorSet(a.style,{animationName:b?"none":this.cachedAnimationName(a)}),a}},{key:"vendorSet",value:function(a,b){for(var c in b)if(b.hasOwnProperty(c)){var d=b[c];a[""+c]=d;for(var e=0;e<this.vendors.length;e++){var f=this.vendors[e];a[""+f+c.charAt(0).toUpperCase()+c.substr(1)]=d}}}},{key:"vendorCSS",value:function(a,b){for(var c=q(a),d=c.getPropertyCSSValue(b),e=0;e<this.vendors.length;e++){var f=this.vendors[e];d=d||c.getPropertyCSSValue("-"+f+"-"+b)}return d}},{key:"animationName",value:function(a){var b=void 0;try{b=this.vendorCSS(a,"animation-name").cssText}catch(c){b=q(a).getPropertyValue("animation-name")}return"none"===b?"":b}},{key:"cacheAnimationName",value:function(a){return this.animationNameCache.set(a,this.animationName(a))}},{key:"cachedAnimationName",value:function(a){return this.animationNameCache.get(a)}},{key:"scrollHandler",value:function(){this.scrolled=!0}},{key:"scrollCallback",value:function(){if(this.scrolled){this.scrolled=!1;for(var a=[],b=0;b<this.boxes.length;b++){var c=this.boxes[b];if(c){if(this.isVisible(c)){this.show(c);continue}a.push(c)}}this.boxes=a,this.boxes.length||this.config.live||this.stop()}}},{key:"offsetTop",value:function(a){for(;void 0===a.offsetTop;)a=a.parentNode;for(var b=a.offsetTop;a.offsetParent;)a=a.offsetParent,b+=a.offsetTop;return b}},{key:"isVisible",value:function(a){var b=a.getAttribute("data-wow-offset")||this.config.offset,c=this.config.scrollContainer&&this.config.scrollContainer.scrollTop||window.pageYOffset,d=c+Math.min(this.element.clientHeight,k())-b,e=this.offsetTop(a),f=e+a.clientHeight;return d>=e&&f>=c}},{key:"disabled",value:function(){return!this.config.mobile&&f(navigator.userAgent)}}]),a}();b["default"]=r,a.exports=b["default"]});
</script>


<script>
    var tns=function(){var t=window,Ai=t.requestAnimationFrame||t.webkitRequestAnimationFrame||t.mozRequestAnimationFrame||t.msRequestAnimationFrame||function(t){return setTimeout(t,16)},e=window,Ni=e.cancelAnimationFrame||e.mozCancelAnimationFrame||function(t){clearTimeout(t)};function Li(){for(var t,e,n,i=arguments[0]||{},a=1,r=arguments.length;a<r;a++)if(null!==(t=arguments[a]))for(e in t)i!==(n=t[e])&&void 0!==n&&(i[e]=n);return i}function Bi(t){return 0<=["true","false"].indexOf(t)?JSON.parse(t):t}function Si(t,e,n,i){if(i)try{t.setItem(e,n)}catch(t){}return n}function Hi(){var t=document,e=t.body;return e||((e=t.createElement("body")).fake=!0),e}var n=document.documentElement;function Oi(t){var e="";return t.fake&&(e=n.style.overflow,t.style.background="",t.style.overflow=n.style.overflow="hidden",n.appendChild(t)),e}function Di(t,e){t.fake&&(t.remove(),n.style.overflow=e,n.offsetHeight)}function ki(t,e,n,i){"insertRule"in t?t.insertRule(e+"{"+n+"}",i):t.addRule(e,n,i)}function Ri(t){return("insertRule"in t?t.cssRules:t.rules).length}function Ii(t,e,n){for(var i=0,a=t.length;i<a;i++)e.call(n,t[i],i)}var i="classList"in document.createElement("_"),Pi=i?function(t,e){return t.classList.contains(e)}:function(t,e){return 0<=t.className.indexOf(e)},zi=i?function(t,e){Pi(t,e)||t.classList.add(e)}:function(t,e){Pi(t,e)||(t.className+=" "+e)},Wi=i?function(t,e){Pi(t,e)&&t.classList.remove(e)}:function(t,e){Pi(t,e)&&(t.className=t.className.replace(e,""))};function qi(t,e){return t.hasAttribute(e)}function Fi(t,e){return t.getAttribute(e)}function r(t){return void 0!==t.item}function ji(t,e){if(t=r(t)||t instanceof Array?t:[t],"[object Object]"===Object.prototype.toString.call(e))for(var n=t.length;n--;)for(var i in e)t[n].setAttribute(i,e[i])}function Vi(t,e){t=r(t)||t instanceof Array?t:[t];for(var n=(e=e instanceof Array?e:[e]).length,i=t.length;i--;)for(var a=n;a--;)t[i].removeAttribute(e[a])}function Gi(t){for(var e=[],n=0,i=t.length;n<i;n++)e.push(t[n]);return e}function Qi(t,e){"none"!==t.style.display&&(t.style.display="none")}function Xi(t,e){"none"===t.style.display&&(t.style.display="")}function Yi(t){return"none"!==window.getComputedStyle(t).display}function Ki(e){if("string"==typeof e){var n=[e],i=e.charAt(0).toUpperCase()+e.substr(1);["Webkit","Moz","ms","O"].forEach(function(t){"ms"===t&&"transform"!==e||n.push(t+i)}),e=n}for(var t=document.createElement("fakeelement"),a=(e.length,0);a<e.length;a++){var r=e[a];if(void 0!==t.style[r])return r}return!1}function Ji(t,e){var n=!1;return/^Webkit/.test(t)?n="webkit"+e+"End":/^O/.test(t)?n="o"+e+"End":t&&(n=e.toLowerCase()+"end"),n}var a=!1;try{var o=Object.defineProperty({},"passive",{get:function(){a=!0}});window.addEventListener("test",null,o)}catch(t){}var u=!!a&&{passive:!0};function Ui(t,e,n){for(var i in e){var a=0<=["touchstart","touchmove"].indexOf(i)&&!n&&u;t.addEventListener(i,e[i],a)}}function _i(t,e){for(var n in e){var i=0<=["touchstart","touchmove"].indexOf(n)&&u;t.removeEventListener(n,e[n],i)}}function Zi(){return{topics:{},on:function(t,e){this.topics[t]=this.topics[t]||[],this.topics[t].push(e)},off:function(t,e){if(this.topics[t])for(var n=0;n<this.topics[t].length;n++)if(this.topics[t][n]===e){this.topics[t].splice(n,1);break}},emit:function(e,n){n.type=e,this.topics[e]&&this.topics[e].forEach(function(t){t(n,e)})}}}Object.keys||(Object.keys=function(t){var e=[];for(var n in t)Object.prototype.hasOwnProperty.call(t,n)&&e.push(n);return e}),"remove"in Element.prototype||(Element.prototype.remove=function(){this.parentNode&&this.parentNode.removeChild(this)});var $i=function(H){H=Li({container:".slider",mode:"carousel",axis:"horizontal",items:1,gutter:0,edgePadding:0,fixedWidth:!1,autoWidth:!1,viewportMax:!1,slideBy:1,center:!1,controls:!0,controlsPosition:"top",controlsText:["prev","next"],controlsContainer:!1,prevButton:!1,nextButton:!1,nav:!0,navPosition:"top",navContainer:!1,navAsThumbnails:!1,arrowKeys:!1,speed:300,autoplay:!1,autoplayPosition:"top",autoplayTimeout:5e3,autoplayDirection:"forward",autoplayText:["start","stop"],autoplayHoverPause:!1,autoplayButton:!1,autoplayButtonOutput:!0,autoplayResetOnVisibility:!0,animateIn:"tns-fadeIn",animateOut:"tns-fadeOut",animateNormal:"tns-normal",animateDelay:!1,loop:!0,rewind:!1,autoHeight:!1,responsive:!1,lazyload:!1,lazyloadSelector:".tns-lazy-img",touch:!0,mouseDrag:!1,swipeAngle:15,nested:!1,preventActionWhenRunning:!1,preventScrollOnTouch:!1,freezable:!0,onInit:!1,useLocalStorage:!0,nonce:!1},H||{});var O=document,m=window,a={ENTER:13,SPACE:32,LEFT:37,RIGHT:39},e={},n=H.useLocalStorage;if(n){var t=navigator.userAgent,i=new Date;try{(e=m.localStorage)?(e.setItem(i,i),n=e.getItem(i)==i,e.removeItem(i)):n=!1,n||(e={})}catch(t){n=!1}n&&(e.tnsApp&&e.tnsApp!==t&&["tC","tPL","tMQ","tTf","t3D","tTDu","tTDe","tADu","tADe","tTE","tAE"].forEach(function(t){e.removeItem(t)}),localStorage.tnsApp=t)}var y=e.tC?Bi(e.tC):Si(e,"tC",function(){var t=document,e=Hi(),n=Oi(e),i=t.createElement("div"),a=!1;e.appendChild(i);try{for(var r,o="(10px * 10)",u=["calc"+o,"-moz-calc"+o,"-webkit-calc"+o],l=0;l<3;l++)if(r=u[l],i.style.width=r,100===i.offsetWidth){a=r.replace(o,"");break}}catch(t){}return e.fake?Di(e,n):i.remove(),a}(),n),g=e.tPL?Bi(e.tPL):Si(e,"tPL",function(){var t,e=document,n=Hi(),i=Oi(n),a=e.createElement("div"),r=e.createElement("div"),o="";a.className="tns-t-subp2",r.className="tns-t-ct";for(var u=0;u<70;u++)o+="<div></div>";return r.innerHTML=o,a.appendChild(r),n.appendChild(a),t=Math.abs(a.getBoundingClientRect().left-r.children[67].getBoundingClientRect().left)<2,n.fake?Di(n,i):a.remove(),t}(),n),D=e.tMQ?Bi(e.tMQ):Si(e,"tMQ",function(){if(window.matchMedia||window.msMatchMedia)return!0;var t,e=document,n=Hi(),i=Oi(n),a=e.createElement("div"),r=e.createElement("style"),o="@media all and (min-width:1px){.tns-mq-test{position:absolute}}";return r.type="text/css",a.className="tns-mq-test",n.appendChild(r),n.appendChild(a),r.styleSheet?r.styleSheet.cssText=o:r.appendChild(e.createTextNode(o)),t=window.getComputedStyle?window.getComputedStyle(a).position:a.currentStyle.position,n.fake?Di(n,i):a.remove(),"absolute"===t}(),n),r=e.tTf?Bi(e.tTf):Si(e,"tTf",Ki("transform"),n),o=e.t3D?Bi(e.t3D):Si(e,"t3D",function(t){if(!t)return!1;if(!window.getComputedStyle)return!1;var e,n=document,i=Hi(),a=Oi(i),r=n.createElement("p"),o=9<t.length?"-"+t.slice(0,-9).toLowerCase()+"-":"";return o+="transform",i.insertBefore(r,null),r.style[t]="translate3d(1px,1px,1px)",e=window.getComputedStyle(r).getPropertyValue(o),i.fake?Di(i,a):r.remove(),void 0!==e&&0<e.length&&"none"!==e}(r),n),x=e.tTDu?Bi(e.tTDu):Si(e,"tTDu",Ki("transitionDuration"),n),u=e.tTDe?Bi(e.tTDe):Si(e,"tTDe",Ki("transitionDelay"),n),b=e.tADu?Bi(e.tADu):Si(e,"tADu",Ki("animationDuration"),n),l=e.tADe?Bi(e.tADe):Si(e,"tADe",Ki("animationDelay"),n),s=e.tTE?Bi(e.tTE):Si(e,"tTE",Ji(x,"Transition"),n),c=e.tAE?Bi(e.tAE):Si(e,"tAE",Ji(b,"Animation"),n),f=m.console&&"function"==typeof m.console.warn,d=["container","controlsContainer","prevButton","nextButton","navContainer","autoplayButton"],v={};if(d.forEach(function(t){if("string"==typeof H[t]){var e=H[t],n=O.querySelector(e);if(v[t]=e,!n||!n.nodeName)return void(f&&console.warn("Can't find",H[t]));H[t]=n}}),!(H.container.children.length<1)){var k=H.responsive,R=H.nested,I="carousel"===H.mode;if(k){0 in k&&(H=Li(H,k[0]),delete k[0]);var p={};for(var h in k){var w=k[h];w="number"==typeof w?{items:w}:w,p[h]=w}k=p,p=null}if(I||function t(e){for(var n in e)I||("slideBy"===n&&(e[n]="page"),"edgePadding"===n&&(e[n]=!1),"autoHeight"===n&&(e[n]=!1)),"responsive"===n&&t(e[n])}(H),!I){H.axis="horizontal",H.slideBy="page",H.edgePadding=!1;var P=H.animateIn,z=H.animateOut,C=H.animateDelay,W=H.animateNormal}var M,q,F="horizontal"===H.axis,T=O.createElement("div"),j=O.createElement("div"),V=H.container,E=V.parentNode,A=V.outerHTML,G=V.children,Q=G.length,X=rn(),Y=!1;k&&En(),I&&(V.className+=" tns-vpfix");var N,L,B,S,K,J,U,_,Z,$=H.autoWidth,tt=sn("fixedWidth"),et=sn("edgePadding"),nt=sn("gutter"),it=un(),at=sn("center"),rt=$?1:Math.floor(sn("items")),ot=sn("slideBy"),ut=H.viewportMax||H.fixedWidthViewportWidth,lt=sn("arrowKeys"),st=sn("speed"),ct=H.rewind,ft=!ct&&H.loop,dt=sn("autoHeight"),vt=sn("controls"),pt=sn("controlsText"),ht=sn("nav"),mt=sn("touch"),yt=sn("mouseDrag"),gt=sn("autoplay"),xt=sn("autoplayTimeout"),bt=sn("autoplayText"),wt=sn("autoplayHoverPause"),Ct=sn("autoplayResetOnVisibility"),Mt=(U=null,_=sn("nonce"),Z=document.createElement("style"),U&&Z.setAttribute("media",U),_&&Z.setAttribute("nonce",_),document.querySelector("head").appendChild(Z),Z.sheet?Z.sheet:Z.styleSheet),Tt=H.lazyload,Et=H.lazyloadSelector,At=[],Nt=ft?(K=function(){{if($||tt&&!ut)return Q-1;var t=tt?"fixedWidth":"items",e=[];if((tt||H[t]<Q)&&e.push(H[t]),k)for(var n in k){var i=k[n][t];i&&(tt||i<Q)&&e.push(i)}return e.length||e.push(0),Math.ceil(tt?ut/Math.min.apply(null,e):Math.max.apply(null,e))}}(),J=I?Math.ceil((5*K-Q)/2):4*K-Q,J=Math.max(K,J),ln("edgePadding")?J+1:J):0,Lt=I?Q+2*Nt:Q+Nt,Bt=!(!tt&&!$||ft),St=tt?_n():null,Ht=!I||!ft,Ot=F?"left":"top",Dt="",kt="",Rt=tt?function(){return at&&!ft?Q-1:Math.ceil(-St/(tt+nt))}:$?function(){for(var t=0;t<Lt;t++)if(N[t]>=-St)return t}:function(){return at&&I&&!ft?Q-1:ft||I?Math.max(0,Lt-Math.ceil(rt)):Lt-1},It=en(sn("startIndex")),Pt=It,zt=(tn(),0),Wt=$?null:Rt(),qt=H.preventActionWhenRunning,Ft=H.swipeAngle,jt=!Ft||"?",Vt=!1,Gt=H.onInit,Qt=new Zi,Xt=" tns-slider tns-"+H.mode,Yt=V.id||(S=window.tnsId,window.tnsId=S?S+1:1,"tns"+window.tnsId),Kt=sn("disable"),Jt=!1,Ut=H.freezable,_t=!(!Ut||$)&&Tn(),Zt=!1,$t={click:oi,keydown:function(t){t=pi(t);var e=[a.LEFT,a.RIGHT].indexOf(t.keyCode);0<=e&&(0===e?we.disabled||oi(t,-1):Ce.disabled||oi(t,1))}},te={click:function(t){if(Vt){if(qt)return;ai()}var e=hi(t=pi(t));for(;e!==Ae&&!qi(e,"data-nav");)e=e.parentNode;if(qi(e,"data-nav")){var n=Se=Number(Fi(e,"data-nav")),i=tt||$?n*Q/Le:n*rt,a=le?n:Math.min(Math.ceil(i),Q-1);ri(a,t),He===n&&(Pe&&fi(),Se=-1)}},keydown:function(t){t=pi(t);var e=O.activeElement;if(!qi(e,"data-nav"))return;var n=[a.LEFT,a.RIGHT,a.ENTER,a.SPACE].indexOf(t.keyCode),i=Number(Fi(e,"data-nav"));0<=n&&(0===n?0<i&&vi(Ee[i-1]):1===n?i<Le-1&&vi(Ee[i+1]):ri(Se=i,t))}},ee={mouseover:function(){Pe&&(li(),ze=!0)},mouseout:function(){ze&&(ui(),ze=!1)}},ne={visibilitychange:function(){O.hidden?Pe&&(li(),qe=!0):qe&&(ui(),qe=!1)}},ie={keydown:function(t){t=pi(t);var e=[a.LEFT,a.RIGHT].indexOf(t.keyCode);0<=e&&oi(t,0===e?-1:1)}},ae={touchstart:xi,touchmove:bi,touchend:wi,touchcancel:wi},re={mousedown:xi,mousemove:bi,mouseup:wi,mouseleave:wi},oe=ln("controls"),ue=ln("nav"),le=!!$||H.navAsThumbnails,se=ln("autoplay"),ce=ln("touch"),fe=ln("mouseDrag"),de="tns-slide-active",ve="tns-slide-cloned",pe="tns-complete",he={load:function(t){kn(hi(t))},error:function(t){e=hi(t),zi(e,"failed"),Rn(e);var e}},me="force"===H.preventScrollOnTouch;if(oe)var ye,ge,xe=H.controlsContainer,be=H.controlsContainer?H.controlsContainer.outerHTML:"",we=H.prevButton,Ce=H.nextButton,Me=H.prevButton?H.prevButton.outerHTML:"",Te=H.nextButton?H.nextButton.outerHTML:"";if(ue)var Ee,Ae=H.navContainer,Ne=H.navContainer?H.navContainer.outerHTML:"",Le=$?Q:Mi(),Be=0,Se=-1,He=an(),Oe=He,De="tns-nav-active",ke="Carousel Page ",Re=" (Current Slide)";if(se)var Ie,Pe,ze,We,qe,Fe="forward"===H.autoplayDirection?1:-1,je=H.autoplayButton,Ve=H.autoplayButton?H.autoplayButton.outerHTML:"",Ge=["<span class='tns-visually-hidden'>"," animation</span>"];if(ce||fe)var Qe,Xe,Ye={},Ke={},Je=!1,Ue=F?function(t,e){return t.x-e.x}:function(t,e){return t.y-e.y};$||$e(Kt||_t),r&&(Ot=r,Dt="translate",o?(Dt+=F?"3d(":"3d(0px, ",kt=F?", 0px, 0px)":", 0px)"):(Dt+=F?"X(":"Y(",kt=")")),I&&(V.className=V.className.replace("tns-vpfix","")),function(){ln("gutter");T.className="tns-outer",j.className="tns-inner",T.id=Yt+"-ow",j.id=Yt+"-iw",""===V.id&&(V.id=Yt);Xt+=g||$?" tns-subpixel":" tns-no-subpixel",Xt+=y?" tns-calc":" tns-no-calc",$&&(Xt+=" tns-autowidth");Xt+=" tns-"+H.axis,V.className+=Xt,I?((M=O.createElement("div")).id=Yt+"-mw",M.className="tns-ovh",T.appendChild(M),M.appendChild(j)):T.appendChild(j);if(dt){var t=M||j;t.className+=" tns-ah"}if(E.insertBefore(T,V),j.appendChild(V),Ii(G,function(t,e){zi(t,"tns-item"),t.id||(t.id=Yt+"-item"+e),!I&&W&&zi(t,W),ji(t,{"aria-hidden":"true",tabindex:"-1"})}),Nt){for(var e=O.createDocumentFragment(),n=O.createDocumentFragment(),i=Nt;i--;){var a=i%Q,r=G[a].cloneNode(!0);if(zi(r,ve),Vi(r,"id"),n.insertBefore(r,n.firstChild),I){var o=G[Q-1-a].cloneNode(!0);zi(o,ve),Vi(o,"id"),e.appendChild(o)}}V.insertBefore(e,V.firstChild),V.appendChild(n),G=V.children}}(),function(){if(!I)for(var t=It,e=It+Math.min(Q,rt);t<e;t++){var n=G[t];n.style.left=100*(t-It)/rt+"%",zi(n,P),Wi(n,W)}F&&(g||$?(ki(Mt,"#"+Yt+" > .tns-item","font-size:"+m.getComputedStyle(G[0]).fontSize+";",Ri(Mt)),ki(Mt,"#"+Yt,"font-size:0;",Ri(Mt))):I&&Ii(G,function(t,e){var n;t.style.marginLeft=(n=e,y?y+"("+100*n+"% / "+Lt+")":100*n/Lt+"%")}));if(D){if(x){var i=M&&H.autoHeight?hn(H.speed):"";ki(Mt,"#"+Yt+"-mw",i,Ri(Mt))}i=cn(H.edgePadding,H.gutter,H.fixedWidth,H.speed,H.autoHeight),ki(Mt,"#"+Yt+"-iw",i,Ri(Mt)),I&&(i=F&&!$?"width:"+fn(H.fixedWidth,H.gutter,H.items)+";":"",x&&(i+=hn(st)),ki(Mt,"#"+Yt,i,Ri(Mt))),i=F&&!$?dn(H.fixedWidth,H.gutter,H.items):"",H.gutter&&(i+=vn(H.gutter)),I||(x&&(i+=hn(st)),b&&(i+=mn(st))),i&&ki(Mt,"#"+Yt+" > .tns-item",i,Ri(Mt))}else{I&&dt&&(M.style[x]=st/1e3+"s"),j.style.cssText=cn(et,nt,tt,dt),I&&F&&!$&&(V.style.width=fn(tt,nt,rt));var i=F&&!$?dn(tt,nt,rt):"";nt&&(i+=vn(nt)),i&&ki(Mt,"#"+Yt+" > .tns-item",i,Ri(Mt))}if(k&&D)for(var a in k){a=parseInt(a);var r=k[a],i="",o="",u="",l="",s="",c=$?null:sn("items",a),f=sn("fixedWidth",a),d=sn("speed",a),v=sn("edgePadding",a),p=sn("autoHeight",a),h=sn("gutter",a);x&&M&&sn("autoHeight",a)&&"speed"in r&&(o="#"+Yt+"-mw{"+hn(d)+"}"),("edgePadding"in r||"gutter"in r)&&(u="#"+Yt+"-iw{"+cn(v,h,f,d,p)+"}"),I&&F&&!$&&("fixedWidth"in r||"items"in r||tt&&"gutter"in r)&&(l="width:"+fn(f,h,c)+";"),x&&"speed"in r&&(l+=hn(d)),l&&(l="#"+Yt+"{"+l+"}"),("fixedWidth"in r||tt&&"gutter"in r||!I&&"items"in r)&&(s+=dn(f,h,c)),"gutter"in r&&(s+=vn(h)),!I&&"speed"in r&&(x&&(s+=hn(d)),b&&(s+=mn(d))),s&&(s="#"+Yt+" > .tns-item{"+s+"}"),(i=o+u+l+s)&&Mt.insertRule("@media (min-width: "+a/16+"em) {"+i+"}",Mt.cssRules.length)}}(),yn();var _e=ft?I?function(){var t=zt,e=Wt;t+=ot,e-=ot,et?(t+=1,e-=1):tt&&(it+nt)%(tt+nt)&&(e-=1),Nt&&(e<It?It-=Q:It<t&&(It+=Q))}:function(){if(Wt<It)for(;zt+Q<=It;)It-=Q;else if(It<zt)for(;It<=Wt-Q;)It+=Q}:function(){It=Math.max(zt,Math.min(Wt,It))},Ze=I?function(){var e,n,i,a,t,r,o,u,l,s,c;Jn(V,""),x||!st?(ti(),st&&Yi(V)||ai()):(e=V,n=Ot,i=Dt,a=kt,t=Zn(),r=st,o=ai,u=Math.min(r,10),l=0<=t.indexOf("%")?"%":"px",t=t.replace(l,""),s=Number(e.style[n].replace(i,"").replace(a,"").replace(l,"")),c=(t-s)/r*u,setTimeout(function t(){r-=u,s+=c,e.style[n]=i+s+l+a,0<r?setTimeout(t,u):o()},u)),F||Ci()}:function(){At=[];var t={};t[s]=t[c]=ai,_i(G[Pt],t),Ui(G[It],t),ei(Pt,P,z,!0),ei(It,W,P),s&&c&&st&&Yi(V)||ai()};return{version:"2.9.2",getInfo:Ei,events:Qt,goTo:ri,play:function(){gt&&!Pe&&(ci(),We=!1)},pause:function(){Pe&&(fi(),We=!0)},isOn:Y,updateSliderHeight:Fn,refresh:yn,destroy:function(){if(Mt.disabled=!0,Mt.ownerNode&&Mt.ownerNode.remove(),_i(m,{resize:Cn}),lt&&_i(O,ie),xe&&_i(xe,$t),Ae&&_i(Ae,te),_i(V,ee),_i(V,ne),je&&_i(je,{click:di}),gt&&clearInterval(Ie),I&&s){var t={};t[s]=ai,_i(V,t)}mt&&_i(V,ae),yt&&_i(V,re);var r=[A,be,Me,Te,Ne,Ve];for(var e in d.forEach(function(t,e){var n="container"===t?T:H[t];if("object"==typeof n&&n){var i=!!n.previousElementSibling&&n.previousElementSibling,a=n.parentNode;n.outerHTML=r[e],H[t]=i?i.nextElementSibling:a.firstElementChild}}),d=P=z=C=W=F=T=j=V=E=A=G=Q=q=X=$=tt=et=nt=it=rt=ot=ut=lt=st=ct=ft=dt=Mt=Tt=N=At=Nt=Lt=Bt=St=Ht=Ot=Dt=kt=Rt=It=Pt=zt=Wt=Ft=jt=Vt=Gt=Qt=Xt=Yt=Kt=Jt=Ut=_t=Zt=$t=te=ee=ne=ie=ae=re=oe=ue=le=se=ce=fe=de=pe=he=L=vt=pt=xe=be=we=Ce=ye=ge=ht=Ae=Ne=Ee=Le=Be=Se=He=Oe=De=ke=Re=gt=xt=Fe=bt=wt=je=Ve=Ct=Ge=Ie=Pe=ze=We=qe=Ye=Ke=Qe=Je=Xe=Ue=mt=yt=null,this)"rebuild"!==e&&(this[e]=null);Y=!1},rebuild:function(){return $i(Li(H,v))}}}function $e(t){t&&(vt=ht=mt=yt=lt=gt=wt=Ct=!1)}function tn(){for(var t=I?It-Nt:It;t<0;)t+=Q;return t%Q+1}function en(t){return t=t?Math.max(0,Math.min(ft?Q-1:Q-rt,t)):0,I?t+Nt:t}function nn(t){for(null==t&&(t=It),I&&(t-=Nt);t<0;)t+=Q;return Math.floor(t%Q)}function an(){var t,e=nn();return t=le?e:tt||$?Math.ceil((e+1)*Le/Q-1):Math.floor(e/rt),!ft&&I&&It===Wt&&(t=Le-1),t}function rn(){return m.innerWidth||O.documentElement.clientWidth||O.body.clientWidth}function on(t){return"top"===t?"afterbegin":"beforeend"}function un(){var t=et?2*et-nt:0;return function t(e){if(null!=e){var n,i,a=O.createElement("div");return e.appendChild(a),i=(n=a.getBoundingClientRect()).right-n.left,a.remove(),i||t(e.parentNode)}}(E)-t}function ln(t){if(H[t])return!0;if(k)for(var e in k)if(k[e][t])return!0;return!1}function sn(t,e){if(null==e&&(e=X),"items"===t&&tt)return Math.floor((it+nt)/(tt+nt))||1;var n=H[t];if(k)for(var i in k)e>=parseInt(i)&&t in k[i]&&(n=k[i][t]);return"slideBy"===t&&"page"===n&&(n=sn("items")),I||"slideBy"!==t&&"items"!==t||(n=Math.floor(n)),n}function cn(t,e,n,i,a){var r="";if(void 0!==t){var o=t;e&&(o-=e),r=F?"margin: 0 "+o+"px 0 "+t+"px;":"margin: "+t+"px 0 "+o+"px 0;"}else if(e&&!n){var u="-"+e+"px";r="margin: 0 "+(F?u+" 0 0":"0 "+u+" 0")+";"}return!I&&a&&x&&i&&(r+=hn(i)),r}function fn(t,e,n){return t?(t+e)*Lt+"px":y?y+"("+100*Lt+"% / "+n+")":100*Lt/n+"%"}function dn(t,e,n){var i;if(t)i=t+e+"px";else{I||(n=Math.floor(n));var a=I?Lt:n;i=y?y+"(100% / "+a+")":100/a+"%"}return i="width:"+i,"inner"!==R?i+";":i+" !important;"}function vn(t){var e="";!1!==t&&(e=(F?"padding-":"margin-")+(F?"right":"bottom")+": "+t+"px;");return e}function pn(t,e){var n=t.substring(0,t.length-e).toLowerCase();return n&&(n="-"+n+"-"),n}function hn(t){return pn(x,18)+"transition-duration:"+t/1e3+"s;"}function mn(t){return pn(b,17)+"animation-duration:"+t/1e3+"s;"}function yn(){if(ln("autoHeight")||$||!F){var t=V.querySelectorAll("img");Ii(t,function(t){var e=t.src;Tt||(e&&e.indexOf("data:image")<0?(t.src="",Ui(t,he),zi(t,"loading"),t.src=e):kn(t))}),Ai(function(){zn(Gi(t),function(){L=!0})}),ln("autoHeight")&&(t=In(It,Math.min(It+rt-1,Lt-1))),Tt?gn():Ai(function(){zn(Gi(t),gn)})}else I&&$n(),bn(),wn()}function gn(){if($&&1<Q){var i=ft?It:Q-1;!function t(){var e=G[i].getBoundingClientRect().left,n=G[i-1].getBoundingClientRect().right;Math.abs(e-n)<=1?xn():setTimeout(function(){t()},16)}()}else xn()}function xn(){F&&!$||(jn(),$?(St=_n(),Ut&&(_t=Tn()),Wt=Rt(),$e(Kt||_t)):Ci()),I&&$n(),bn(),wn()}function bn(){if(Vn(),T.insertAdjacentHTML("afterbegin",'<div class="tns-liveregion tns-visually-hidden" aria-live="polite" aria-atomic="true">slide <span class="current">'+Hn()+"</span>  of "+Q+"</div>"),B=T.querySelector(".tns-liveregion .current"),se){var t=gt?"stop":"start";je?ji(je,{"data-action":t}):H.autoplayButtonOutput&&(T.insertAdjacentHTML(on(H.autoplayPosition),'<button type="button" data-action="'+t+'">'+Ge[0]+t+Ge[1]+bt[0]+"</button>"),je=T.querySelector("[data-action]")),je&&Ui(je,{click:di}),gt&&(ci(),wt&&Ui(V,ee),Ct&&Ui(V,ne))}if(ue){if(Ae)ji(Ae,{"aria-label":"Carousel Pagination"}),Ii(Ee=Ae.children,function(t,e){ji(t,{"data-nav":e,tabindex:"-1","aria-label":ke+(e+1),"aria-controls":Yt})});else{for(var e="",n=le?"":'style="display:none"',i=0;i<Q;i++)e+='<button type="button" data-nav="'+i+'" tabindex="-1" aria-controls="'+Yt+'" '+n+' aria-label="'+ke+(i+1)+'"></button>';e='<div class="tns-nav" aria-label="Carousel Pagination">'+e+"</div>",T.insertAdjacentHTML(on(H.navPosition),e),Ae=T.querySelector(".tns-nav"),Ee=Ae.children}if(Ti(),x){var a=x.substring(0,x.length-18).toLowerCase(),r="transition: all "+st/1e3+"s";a&&(r="-"+a+"-"+r),ki(Mt,"[aria-controls^="+Yt+"-item]",r,Ri(Mt))}ji(Ee[He],{"aria-label":ke+(He+1)+Re}),Vi(Ee[He],"tabindex"),zi(Ee[He],De),Ui(Ae,te)}oe&&(xe||we&&Ce||(T.insertAdjacentHTML(on(H.controlsPosition),'<div class="tns-controls" aria-label="Carousel Navigation" tabindex="0"><button type="button" data-controls="prev" tabindex="-1" aria-controls="'+Yt+'">'+pt[0]+'</button><button type="button" data-controls="next" tabindex="-1" aria-controls="'+Yt+'">'+pt[1]+"</button></div>"),xe=T.querySelector(".tns-controls")),we&&Ce||(we=xe.children[0],Ce=xe.children[1]),H.controlsContainer&&ji(xe,{"aria-label":"Carousel Navigation",tabindex:"0"}),(H.controlsContainer||H.prevButton&&H.nextButton)&&ji([we,Ce],{"aria-controls":Yt,tabindex:"-1"}),(H.controlsContainer||H.prevButton&&H.nextButton)&&(ji(we,{"data-controls":"prev"}),ji(Ce,{"data-controls":"next"})),ye=Qn(we),ge=Qn(Ce),Kn(),xe?Ui(xe,$t):(Ui(we,$t),Ui(Ce,$t))),An()}function wn(){if(I&&s){var t={};t[s]=ai,Ui(V,t)}mt&&Ui(V,ae,H.preventScrollOnTouch),yt&&Ui(V,re),lt&&Ui(O,ie),"inner"===R?Qt.on("outerResized",function(){Mn(),Qt.emit("innerLoaded",Ei())}):(k||tt||$||dt||!F)&&Ui(m,{resize:Cn}),dt&&("outer"===R?Qt.on("innerLoaded",Pn):Kt||Pn()),Dn(),Kt?Bn():_t&&Ln(),Qt.on("indexChanged",Wn),"inner"===R&&Qt.emit("innerLoaded",Ei()),"function"==typeof Gt&&Gt(Ei()),Y=!0}function Cn(t){Ai(function(){Mn(pi(t))})}function Mn(t){if(Y){"outer"===R&&Qt.emit("outerResized",Ei(t)),X=rn();var e,n=q,i=!1;k&&(En(),(e=n!==q)&&Qt.emit("newBreakpointStart",Ei(t)));var a,r,o,u,l=rt,s=Kt,c=_t,f=lt,d=vt,v=ht,p=mt,h=yt,m=gt,y=wt,g=Ct,x=It;if(e){var b=tt,w=dt,C=pt,M=at,T=bt;if(!D)var E=nt,A=et}if(lt=sn("arrowKeys"),vt=sn("controls"),ht=sn("nav"),mt=sn("touch"),at=sn("center"),yt=sn("mouseDrag"),gt=sn("autoplay"),wt=sn("autoplayHoverPause"),Ct=sn("autoplayResetOnVisibility"),e&&(Kt=sn("disable"),tt=sn("fixedWidth"),st=sn("speed"),dt=sn("autoHeight"),pt=sn("controlsText"),bt=sn("autoplayText"),xt=sn("autoplayTimeout"),D||(et=sn("edgePadding"),nt=sn("gutter"))),$e(Kt),it=un(),F&&!$||Kt||(jn(),F||(Ci(),i=!0)),(tt||$)&&(St=_n(),Wt=Rt()),(e||tt)&&(rt=sn("items"),ot=sn("slideBy"),(r=rt!==l)&&(tt||$||(Wt=Rt()),_e())),e&&Kt!==s&&(Kt?Bn():function(){if(!Jt)return;if(Mt.disabled=!1,V.className+=Xt,$n(),ft)for(var t=Nt;t--;)I&&Xi(G[t]),Xi(G[Lt-t-1]);if(!I)for(var e=It,n=It+Q;e<n;e++){var i=G[e],a=e<It+rt?P:W;i.style.left=100*(e-It)/rt+"%",zi(i,a)}Nn(),Jt=!1}()),Ut&&(e||tt||$)&&(_t=Tn())!==c&&(_t?(ti(Zn(en(0))),Ln()):(!function(){if(!Zt)return;et&&D&&(j.style.margin="");if(Nt)for(var t="tns-transparent",e=Nt;e--;)I&&Wi(G[e],t),Wi(G[Lt-e-1],t);Nn(),Zt=!1}(),i=!0)),$e(Kt||_t),gt||(wt=Ct=!1),lt!==f&&(lt?Ui(O,ie):_i(O,ie)),vt!==d&&(vt?xe?Xi(xe):(we&&Xi(we),Ce&&Xi(Ce)):xe?Qi(xe):(we&&Qi(we),Ce&&Qi(Ce))),ht!==v&&(ht?(Xi(Ae),Ti()):Qi(Ae)),mt!==p&&(mt?Ui(V,ae,H.preventScrollOnTouch):_i(V,ae)),yt!==h&&(yt?Ui(V,re):_i(V,re)),gt!==m&&(gt?(je&&Xi(je),Pe||We||ci()):(je&&Qi(je),Pe&&fi())),wt!==y&&(wt?Ui(V,ee):_i(V,ee)),Ct!==g&&(Ct?Ui(O,ne):_i(O,ne)),e){if(tt===b&&at===M||(i=!0),dt!==w&&(dt||(j.style.height="")),vt&&pt!==C&&(we.innerHTML=pt[0],Ce.innerHTML=pt[1]),je&&bt!==T){var N=gt?1:0,L=je.innerHTML,B=L.length-T[N].length;L.substring(B)===T[N]&&(je.innerHTML=L.substring(0,B)+bt[N])}}else at&&(tt||$)&&(i=!0);if((r||tt&&!$)&&(Le=Mi(),Ti()),(a=It!==x)?(Qt.emit("indexChanged",Ei()),i=!0):r?a||Wn():(tt||$)&&(Dn(),Vn(),Sn()),r&&!I&&function(){for(var t=It+Math.min(Q,rt),e=Lt;e--;){var n=G[e];It<=e&&e<t?(zi(n,"tns-moving"),n.style.left=100*(e-It)/rt+"%",zi(n,P),Wi(n,W)):n.style.left&&(n.style.left="",zi(n,W),Wi(n,P)),Wi(n,z)}setTimeout(function(){Ii(G,function(t){Wi(t,"tns-moving")})},300)}(),!Kt&&!_t){if(e&&!D&&(et===A&&nt===E||(j.style.cssText=cn(et,nt,tt,st,dt)),F)){I&&(V.style.width=fn(tt,nt,rt));var S=dn(tt,nt,rt)+vn(nt);u=Ri(o=Mt)-1,"deleteRule"in o?o.deleteRule(u):o.removeRule(u),ki(Mt,"#"+Yt+" > .tns-item",S,Ri(Mt))}dt&&Pn(),i&&($n(),Pt=It)}e&&Qt.emit("newBreakpointEnd",Ei(t))}}function Tn(){if(!tt&&!$)return Q<=(at?rt-(rt-1)/2:rt);var t=tt?(tt+nt)*Q:N[Q],e=et?it+2*et:it+nt;return at&&(e-=tt?(it-tt)/2:(it-(N[It+1]-N[It]-nt))/2),t<=e}function En(){for(var t in q=0,k)(t=parseInt(t))<=X&&(q=t)}function An(){!gt&&je&&Qi(je),!ht&&Ae&&Qi(Ae),vt||(xe?Qi(xe):(we&&Qi(we),Ce&&Qi(Ce)))}function Nn(){gt&&je&&Xi(je),ht&&Ae&&Xi(Ae),vt&&(xe?Xi(xe):(we&&Xi(we),Ce&&Xi(Ce)))}function Ln(){if(!Zt){if(et&&(j.style.margin="0px"),Nt)for(var t="tns-transparent",e=Nt;e--;)I&&zi(G[e],t),zi(G[Lt-e-1],t);An(),Zt=!0}}function Bn(){if(!Jt){if(Mt.disabled=!0,V.className=V.className.replace(Xt.substring(1),""),Vi(V,["style"]),ft)for(var t=Nt;t--;)I&&Qi(G[t]),Qi(G[Lt-t-1]);if(F&&I||Vi(j,["style"]),!I)for(var e=It,n=It+Q;e<n;e++){var i=G[e];Vi(i,["style"]),Wi(i,P),Wi(i,W)}An(),Jt=!0}}function Sn(){var t=Hn();B.innerHTML!==t&&(B.innerHTML=t)}function Hn(){var t=On(),e=t[0]+1,n=t[1]+1;return e===n?e+"":e+" to "+n}function On(t){null==t&&(t=Zn());var n,i,a,r=It;if(at||et?($||tt)&&(i=-(parseFloat(t)+et),a=i+it+2*et):$&&(i=N[It],a=i+it),$)N.forEach(function(t,e){e<Lt&&((at||et)&&t<=i+.5&&(r=e),.5<=a-t&&(n=e))});else{if(tt){var e=tt+nt;at||et?(r=Math.floor(i/e),n=Math.ceil(a/e-1)):n=r+Math.ceil(it/e)-1}else if(at||et){var o=rt-1;if(at?(r-=o/2,n=It+o/2):n=It+o,et){var u=et*rt/it;r-=u,n+=u}r=Math.floor(r),n=Math.ceil(n)}else n=r+rt-1;r=Math.max(r,0),n=Math.min(n,Lt-1)}return[r,n]}function Dn(){if(Tt&&!Kt){var t=On();t.push(Et),In.apply(null,t).forEach(function(t){if(!Pi(t,pe)){var e={};e[s]=function(t){t.stopPropagation()},Ui(t,e),Ui(t,he),t.src=Fi(t,"data-src");var n=Fi(t,"data-srcset");n&&(t.srcset=n),zi(t,"loading")}})}}function kn(t){zi(t,"loaded"),Rn(t)}function Rn(t){zi(t,pe),Wi(t,"loading"),_i(t,he)}function In(t,e,n){var i=[];for(n||(n="img");t<=e;)Ii(G[t].querySelectorAll(n),function(t){i.push(t)}),t++;return i}function Pn(){var t=In.apply(null,On());Ai(function(){zn(t,Fn)})}function zn(n,t){return L?t():(n.forEach(function(t,e){!Tt&&t.complete&&Rn(t),Pi(t,pe)&&n.splice(e,1)}),n.length?void Ai(function(){zn(n,t)}):t())}function Wn(){Dn(),Vn(),Sn(),Kn(),function(){if(ht&&(He=0<=Se?Se:an(),Se=-1,He!==Oe)){var t=Ee[Oe],e=Ee[He];ji(t,{tabindex:"-1","aria-label":ke+(Oe+1)}),Wi(t,De),ji(e,{"aria-label":ke+(He+1)+Re}),Vi(e,"tabindex"),zi(e,De),Oe=He}}()}function qn(t,e){for(var n=[],i=t,a=Math.min(t+e,Lt);i<a;i++)n.push(G[i].offsetHeight);return Math.max.apply(null,n)}function Fn(){var t=dt?qn(It,rt):qn(Nt,Q),e=M||j;e.style.height!==t&&(e.style.height=t+"px")}function jn(){N=[0];var n=F?"left":"top",i=F?"right":"bottom",a=G[0].getBoundingClientRect()[n];Ii(G,function(t,e){e&&N.push(t.getBoundingClientRect()[n]-a),e===Lt-1&&N.push(t.getBoundingClientRect()[i]-a)})}function Vn(){var t=On(),n=t[0],i=t[1];Ii(G,function(t,e){n<=e&&e<=i?qi(t,"aria-hidden")&&(Vi(t,["aria-hidden","tabindex"]),zi(t,de)):qi(t,"aria-hidden")||(ji(t,{"aria-hidden":"true",tabindex:"-1"}),Wi(t,de))})}function Gn(t){return t.nodeName.toLowerCase()}function Qn(t){return"button"===Gn(t)}function Xn(t){return"true"===t.getAttribute("aria-disabled")}function Yn(t,e,n){t?e.disabled=n:e.setAttribute("aria-disabled",n.toString())}function Kn(){if(vt&&!ct&&!ft){var t=ye?we.disabled:Xn(we),e=ge?Ce.disabled:Xn(Ce),n=It<=zt,i=!ct&&Wt<=It;n&&!t&&Yn(ye,we,!0),!n&&t&&Yn(ye,we,!1),i&&!e&&Yn(ge,Ce,!0),!i&&e&&Yn(ge,Ce,!1)}}function Jn(t,e){x&&(t.style[x]=e)}function Un(t){return null==t&&(t=It),$?(it-(et?nt:0)-(N[t+1]-N[t]-nt))/2:tt?(it-tt)/2:(rt-1)/2}function _n(){var t=it+(et?nt:0)-(tt?(tt+nt)*Lt:N[Lt]);return at&&!ft&&(t=tt?-(tt+nt)*(Lt-1)-Un():Un(Lt-1)-N[Lt-1]),0<t&&(t=0),t}function Zn(t){var e;if(null==t&&(t=It),F&&!$)if(tt)e=-(tt+nt)*t,at&&(e+=Un());else{var n=r?Lt:rt;at&&(t-=Un()),e=100*-t/n}else e=-N[t],at&&$&&(e+=Un());return Bt&&(e=Math.max(e,St)),e+=!F||$||tt?"px":"%"}function $n(t){Jn(V,"0s"),ti(t)}function ti(t){null==t&&(t=Zn()),V.style[Ot]=Dt+t+kt}function ei(t,e,n,i){var a=t+rt;ft||(a=Math.min(a,Lt));for(var r=t;r<a;r++){var o=G[r];i||(o.style.left=100*(r-It)/rt+"%"),C&&u&&(o.style[u]=o.style[l]=C*(r-t)/1e3+"s"),Wi(o,e),zi(o,n),i&&At.push(o)}}function ni(t,e){Ht&&_e(),(It!==Pt||e)&&(Qt.emit("indexChanged",Ei()),Qt.emit("transitionStart",Ei()),dt&&Pn(),Pe&&t&&0<=["click","keydown"].indexOf(t.type)&&fi(),Vt=!0,Ze())}function ii(t){return t.toLowerCase().replace(/-/g,"")}function ai(t){if(I||Vt){if(Qt.emit("transitionEnd",Ei(t)),!I&&0<At.length)for(var e=0;e<At.length;e++){var n=At[e];n.style.left="",l&&u&&(n.style[l]="",n.style[u]=""),Wi(n,z),zi(n,W)}if(!t||!I&&t.target.parentNode===V||t.target===V&&ii(t.propertyName)===ii(Ot)){if(!Ht){var i=It;_e(),It!==i&&(Qt.emit("indexChanged",Ei()),$n())}"inner"===R&&Qt.emit("innerLoaded",Ei()),Vt=!1,Pt=It}}}function ri(t,e){if(!_t)if("prev"===t)oi(e,-1);else if("next"===t)oi(e,1);else{if(Vt){if(qt)return;ai()}var n=nn(),i=0;if("first"===t?i=-n:"last"===t?i=I?Q-rt-n:Q-1-n:("number"!=typeof t&&(t=parseInt(t)),isNaN(t)||(e||(t=Math.max(0,Math.min(Q-1,t))),i=t-n)),!I&&i&&Math.abs(i)<rt){var a=0<i?1:-1;i+=zt<=It+i-Q?Q*a:2*Q*a*-1}It+=i,I&&ft&&(It<zt&&(It+=Q),Wt<It&&(It-=Q)),nn(It)!==nn(Pt)&&ni(e)}}function oi(t,e){if(Vt){if(qt)return;ai()}var n;if(!e){for(var i=hi(t=pi(t));i!==xe&&[we,Ce].indexOf(i)<0;)i=i.parentNode;var a=[we,Ce].indexOf(i);0<=a&&(n=!0,e=0===a?-1:1)}if(ct){if(It===zt&&-1===e)return void ri("last",t);if(It===Wt&&1===e)return void ri("first",t)}e&&(It+=ot*e,$&&(It=Math.floor(It)),ni(n||t&&"keydown"===t.type?t:null))}function ui(){Ie=setInterval(function(){oi(null,Fe)},xt),Pe=!0}function li(){clearInterval(Ie),Pe=!1}function si(t,e){ji(je,{"data-action":t}),je.innerHTML=Ge[0]+t+Ge[1]+e}function ci(){ui(),je&&si("stop",bt[1])}function fi(){li(),je&&si("start",bt[0])}function di(){Pe?(fi(),We=!0):(ci(),We=!1)}function vi(t){t.focus()}function pi(t){return mi(t=t||m.event)?t.changedTouches[0]:t}function hi(t){return t.target||m.event.srcElement}function mi(t){return 0<=t.type.indexOf("touch")}function yi(t){t.preventDefault?t.preventDefault():t.returnValue=!1}function gi(){return a=Ke.y-Ye.y,r=Ke.x-Ye.x,t=Math.atan2(a,r)*(180/Math.PI),e=Ft,n=!1,i=Math.abs(90-Math.abs(t)),90-e<=i?n="horizontal":i<=e&&(n="vertical"),n===H.axis;var t,e,n,i,a,r}function xi(t){if(Vt){if(qt)return;ai()}gt&&Pe&&li(),Je=!0,Xe&&(Ni(Xe),Xe=null);var e=pi(t);Qt.emit(mi(t)?"touchStart":"dragStart",Ei(t)),!mi(t)&&0<=["img","a"].indexOf(Gn(hi(t)))&&yi(t),Ke.x=Ye.x=e.clientX,Ke.y=Ye.y=e.clientY,I&&(Qe=parseFloat(V.style[Ot].replace(Dt,"")),Jn(V,"0s"))}function bi(t){if(Je){var e=pi(t);Ke.x=e.clientX,Ke.y=e.clientY,I?Xe||(Xe=Ai(function(){!function t(e){if(!jt)return void(Je=!1);Ni(Xe);Je&&(Xe=Ai(function(){t(e)}));"?"===jt&&(jt=gi());if(jt){!me&&mi(e)&&(me=!0);try{e.type&&Qt.emit(mi(e)?"touchMove":"dragMove",Ei(e))}catch(t){}var n=Qe,i=Ue(Ke,Ye);if(!F||tt||$)n+=i,n+="px";else{var a=r?i*rt*100/((it+nt)*Lt):100*i/(it+nt);n+=a,n+="%"}V.style[Ot]=Dt+n+kt}}(t)})):("?"===jt&&(jt=gi()),jt&&(me=!0)),("boolean"!=typeof t.cancelable||t.cancelable)&&me&&t.preventDefault()}}function wi(i){if(Je){Xe&&(Ni(Xe),Xe=null),I&&Jn(V,""),Je=!1;var t=pi(i);Ke.x=t.clientX,Ke.y=t.clientY;var a=Ue(Ke,Ye);if(Math.abs(a)){if(!mi(i)){var n=hi(i);Ui(n,{click:function t(e){yi(e),_i(n,{click:t})}})}I?Xe=Ai(function(){if(F&&!$){var t=-a*rt/(it+nt);t=0<a?Math.floor(t):Math.ceil(t),It+=t}else{var e=-(Qe+a);if(e<=0)It=zt;else if(e>=N[Lt-1])It=Wt;else for(var n=0;n<Lt&&e>=N[n];)e>N[It=n]&&a<0&&(It+=1),n++}ni(i,a),Qt.emit(mi(i)?"touchEnd":"dragEnd",Ei(i))}):jt&&oi(i,0<a?-1:1)}}"auto"===H.preventScrollOnTouch&&(me=!1),Ft&&(jt="?"),gt&&!Pe&&ui()}function Ci(){(M||j).style.height=N[It+rt]-N[It]+"px"}function Mi(){var t=tt?(tt+nt)*Q/it:Q/rt;return Math.min(Math.ceil(t),Q)}function Ti(){if(ht&&!le&&Le!==Be){var t=Be,e=Le,n=Xi;for(Le<Be&&(t=Le,e=Be,n=Qi);t<e;)n(Ee[t]),t++;Be=Le}}function Ei(t){return{container:V,slideItems:G,navContainer:Ae,navItems:Ee,controlsContainer:xe,hasControls:oe,prevButton:we,nextButton:Ce,items:rt,slideBy:ot,cloneCount:Nt,slideCount:Q,slideCountNew:Lt,index:It,indexCached:Pt,displayIndex:tn(),navCurrentIndex:He,navCurrentIndexCached:Oe,pages:Le,pagesCached:Be,sheet:Mt,isOn:Y,event:t||{}}}f&&console.warn("No slides found in",H.container)};return $i}();
</script>

<script>
    /*
Template Name: HostGrids - Hosting & Domain Services HTML Template.
Author: GrayGrids
*/

(function () {
    //===== Prealoder

    window.onload = function () {
        window.setTimeout(fadeout, 500);
    }

    function fadeout() {
        document.querySelector('.preloader').style.opacity = '0';
        document.querySelector('.preloader').style.display = 'none';
    }

    
    // WOW active
    new WOW().init();


    /*=====================================
    Sticky
    ======================================= */
    window.onscroll = function () {
        var header_navbar = document.querySelector(".navbar-area");
        var sticky = header_navbar.offsetTop;

        var logo = document.querySelector('.navbar-brand img')
        if (window.pageYOffset > sticky) {
          header_navbar.classList.add("sticky");
          logo.src = 'https://digitalhub.fifa.com/transform/412435a1-ebae-4d3c-be0b-b24b30b13c74/FWC-2';
        } else {
          header_navbar.classList.remove("sticky");
          logo.src = 'https://digitalhub.fifa.com/transform/412435a1-ebae-4d3c-be0b-b24b30b13c74/FWC-2';
        }

        // show or hide the back-top-top button
        var backToTo = document.querySelector(".scroll-top");
        if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
            backToTo.style.display = "flex";
        } else {
            backToTo.style.display = "none";
        }
    };


    //===== mobile-menu-btn
    let navbarToggler = document.querySelector(".mobile-menu-btn");
    navbarToggler.addEventListener('click', function () {
        navbarToggler.classList.toggle("active");
    });


})();
</script>

<script>
    !function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e():"function"==typeof define&&define.amd?define(e):(t=t||self).GLightbox=e()}(this,(function(){"use strict";function t(e){return(t="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t})(e)}function e(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}function i(t,e){for(var i=0;i<e.length;i++){var n=e[i];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(t,n.key,n)}}function n(t,e,n){return e&&i(t.prototype,e),n&&i(t,n),t}function s(t){return function(t){if(Array.isArray(t)){for(var e=0,i=new Array(t.length);e<t.length;e++)i[e]=t[e];return i}}(t)||function(t){if(Symbol.iterator in Object(t)||"[object Arguments]"===Object.prototype.toString.call(t))return Array.from(t)}(t)||function(){throw new TypeError("Invalid attempt to spread non-iterable instance")}()}function o(t){return Math.sqrt(t.x*t.x+t.y*t.y)}function l(t,e){var i=function(t,e){var i=o(t)*o(e);if(0===i)return 0;var n=function(t,e){return t.x*e.x+t.y*e.y}(t,e)/i;return n>1&&(n=1),Math.acos(n)}(t,e);return function(t,e){return t.x*e.y-e.x*t.y}(t,e)>0&&(i*=-1),180*i/Math.PI}var r=function(){function t(i){e(this,t),this.handlers=[],this.el=i}return n(t,[{key:"add",value:function(t){this.handlers.push(t)}},{key:"del",value:function(t){t||(this.handlers=[]);for(var e=this.handlers.length;e>=0;e--)this.handlers[e]===t&&this.handlers.splice(e,1)}},{key:"dispatch",value:function(){for(var t=0,e=this.handlers.length;t<e;t++){var i=this.handlers[t];"function"==typeof i&&i.apply(this.el,arguments)}}}]),t}();function a(t,e){var i=new r(t);return i.add(e),i}var h=function(){function t(i,n){e(this,t),this.element="string"==typeof i?document.querySelector(i):i,this.start=this.start.bind(this),this.move=this.move.bind(this),this.end=this.end.bind(this),this.cancel=this.cancel.bind(this),this.element.addEventListener("touchstart",this.start,!1),this.element.addEventListener("touchmove",this.move,!1),this.element.addEventListener("touchend",this.end,!1),this.element.addEventListener("touchcancel",this.cancel,!1),this.preV={x:null,y:null},this.pinchStartLen=null,this.zoom=1,this.isDoubleTap=!1;var s=function(){};this.rotate=a(this.element,n.rotate||s),this.touchStart=a(this.element,n.touchStart||s),this.multipointStart=a(this.element,n.multipointStart||s),this.multipointEnd=a(this.element,n.multipointEnd||s),this.pinch=a(this.element,n.pinch||s),this.swipe=a(this.element,n.swipe||s),this.tap=a(this.element,n.tap||s),this.doubleTap=a(this.element,n.doubleTap||s),this.longTap=a(this.element,n.longTap||s),this.singleTap=a(this.element,n.singleTap||s),this.pressMove=a(this.element,n.pressMove||s),this.twoFingerPressMove=a(this.element,n.twoFingerPressMove||s),this.touchMove=a(this.element,n.touchMove||s),this.touchEnd=a(this.element,n.touchEnd||s),this.touchCancel=a(this.element,n.touchCancel||s),this._cancelAllHandler=this.cancelAll.bind(this),window.addEventListener("scroll",this._cancelAllHandler),this.delta=null,this.last=null,this.now=null,this.tapTimeout=null,this.singleTapTimeout=null,this.longTapTimeout=null,this.swipeTimeout=null,this.x1=this.x2=this.y1=this.y2=null,this.preTapPosition={x:null,y:null}}return n(t,[{key:"start",value:function(t){if(t.touches){this.now=Date.now(),this.x1=t.touches[0].pageX,this.y1=t.touches[0].pageY,this.delta=this.now-(this.last||this.now),this.touchStart.dispatch(t,this.element),null!==this.preTapPosition.x&&(this.isDoubleTap=this.delta>0&&this.delta<=250&&Math.abs(this.preTapPosition.x-this.x1)<30&&Math.abs(this.preTapPosition.y-this.y1)<30,this.isDoubleTap&&clearTimeout(this.singleTapTimeout)),this.preTapPosition.x=this.x1,this.preTapPosition.y=this.y1,this.last=this.now;var e=this.preV;if(t.touches.length>1){this._cancelLongTap(),this._cancelSingleTap();var i={x:t.touches[1].pageX-this.x1,y:t.touches[1].pageY-this.y1};e.x=i.x,e.y=i.y,this.pinchStartLen=o(e),this.multipointStart.dispatch(t,this.element)}this._preventTap=!1,this.longTapTimeout=setTimeout(function(){this.longTap.dispatch(t,this.element),this._preventTap=!0}.bind(this),750)}}},{key:"move",value:function(t){if(t.touches){var e=this.preV,i=t.touches.length,n=t.touches[0].pageX,s=t.touches[0].pageY;if(this.isDoubleTap=!1,i>1){var r=t.touches[1].pageX,a=t.touches[1].pageY,h={x:t.touches[1].pageX-n,y:t.touches[1].pageY-s};null!==e.x&&(this.pinchStartLen>0&&(t.zoom=o(h)/this.pinchStartLen,this.pinch.dispatch(t,this.element)),t.angle=l(h,e),this.rotate.dispatch(t,this.element)),e.x=h.x,e.y=h.y,null!==this.x2&&null!==this.sx2?(t.deltaX=(n-this.x2+r-this.sx2)/2,t.deltaY=(s-this.y2+a-this.sy2)/2):(t.deltaX=0,t.deltaY=0),this.twoFingerPressMove.dispatch(t,this.element),this.sx2=r,this.sy2=a}else{if(null!==this.x2){t.deltaX=n-this.x2,t.deltaY=s-this.y2;var c=Math.abs(this.x1-this.x2),d=Math.abs(this.y1-this.y2);(c>10||d>10)&&(this._preventTap=!0)}else t.deltaX=0,t.deltaY=0;this.pressMove.dispatch(t,this.element)}this.touchMove.dispatch(t,this.element),this._cancelLongTap(),this.x2=n,this.y2=s,i>1&&t.preventDefault()}}},{key:"end",value:function(t){if(t.changedTouches){this._cancelLongTap();var e=this;t.touches.length<2&&(this.multipointEnd.dispatch(t,this.element),this.sx2=this.sy2=null),this.x2&&Math.abs(this.x1-this.x2)>30||this.y2&&Math.abs(this.y1-this.y2)>30?(t.direction=this._swipeDirection(this.x1,this.x2,this.y1,this.y2),this.swipeTimeout=setTimeout((function(){e.swipe.dispatch(t,e.element)}),0)):(this.tapTimeout=setTimeout((function(){e._preventTap||e.tap.dispatch(t,e.element),e.isDoubleTap&&(e.doubleTap.dispatch(t,e.element),e.isDoubleTap=!1)}),0),e.isDoubleTap||(e.singleTapTimeout=setTimeout((function(){e.singleTap.dispatch(t,e.element)}),250))),this.touchEnd.dispatch(t,this.element),this.preV.x=0,this.preV.y=0,this.zoom=1,this.pinchStartLen=null,this.x1=this.x2=this.y1=this.y2=null}}},{key:"cancelAll",value:function(){this._preventTap=!0,clearTimeout(this.singleTapTimeout),clearTimeout(this.tapTimeout),clearTimeout(this.longTapTimeout),clearTimeout(this.swipeTimeout)}},{key:"cancel",value:function(t){this.cancelAll(),this.touchCancel.dispatch(t,this.element)}},{key:"_cancelLongTap",value:function(){clearTimeout(this.longTapTimeout)}},{key:"_cancelSingleTap",value:function(){clearTimeout(this.singleTapTimeout)}},{key:"_swipeDirection",value:function(t,e,i,n){return Math.abs(t-e)>=Math.abs(i-n)?t-e>0?"Left":"Right":i-n>0?"Up":"Down"}},{key:"on",value:function(t,e){this[t]&&this[t].add(e)}},{key:"off",value:function(t,e){this[t]&&this[t].del(e)}},{key:"destroy",value:function(){return this.singleTapTimeout&&clearTimeout(this.singleTapTimeout),this.tapTimeout&&clearTimeout(this.tapTimeout),this.longTapTimeout&&clearTimeout(this.longTapTimeout),this.swipeTimeout&&clearTimeout(this.swipeTimeout),this.element.removeEventListener("touchstart",this.start),this.element.removeEventListener("touchmove",this.move),this.element.removeEventListener("touchend",this.end),this.element.removeEventListener("touchcancel",this.cancel),this.rotate.del(),this.touchStart.del(),this.multipointStart.del(),this.multipointEnd.del(),this.pinch.del(),this.swipe.del(),this.tap.del(),this.doubleTap.del(),this.longTap.del(),this.singleTap.del(),this.pressMove.del(),this.twoFingerPressMove.del(),this.touchMove.del(),this.touchEnd.del(),this.touchCancel.del(),this.preV=this.pinchStartLen=this.zoom=this.isDoubleTap=this.delta=this.last=this.now=this.tapTimeout=this.singleTapTimeout=this.longTapTimeout=this.swipeTimeout=this.x1=this.x2=this.y1=this.y2=this.preTapPosition=this.rotate=this.touchStart=this.multipointStart=this.multipointEnd=this.pinch=this.swipe=this.tap=this.doubleTap=this.longTap=this.singleTap=this.pressMove=this.touchMove=this.touchEnd=this.touchCancel=this.twoFingerPressMove=null,window.removeEventListener("scroll",this._cancelAllHandler),null}}]),t}(),c=function(){function t(i,n){var s=this,o=arguments.length>2&&void 0!==arguments[2]?arguments[2]:null;if(e(this,t),this.img=i,this.slide=n,this.onclose=o,this.img.setZoomEvents)return!1;this.active=!1,this.zoomedIn=!1,this.dragging=!1,this.currentX=null,this.currentY=null,this.initialX=null,this.initialY=null,this.xOffset=0,this.yOffset=0,this.img.addEventListener("mousedown",(function(t){return s.dragStart(t)}),!1),this.img.addEventListener("mouseup",(function(t){return s.dragEnd(t)}),!1),this.img.addEventListener("mousemove",(function(t){return s.drag(t)}),!1),this.img.addEventListener("click",(function(t){if(!s.zoomedIn)return s.zoomIn();s.zoomedIn&&!s.dragging&&s.zoomOut()}),!1),this.img.setZoomEvents=!0}return n(t,[{key:"zoomIn",value:function(){var t=this.widowWidth();if(!(this.zoomedIn||t<=768)){var e=this.img;if(e.setAttribute("data-style",e.getAttribute("style")),e.style.maxWidth=e.naturalWidth+"px",e.style.maxHeight=e.naturalHeight+"px",e.naturalWidth>t){var i=t/2-e.naturalWidth/2;this.setTranslate(this.img.parentNode,i,0)}this.slide.classList.add("zoomed"),this.zoomedIn=!0}}},{key:"zoomOut",value:function(){this.img.parentNode.setAttribute("style",""),this.img.setAttribute("style",this.img.getAttribute("data-style")),this.slide.classList.remove("zoomed"),this.zoomedIn=!1,this.currentX=null,this.currentY=null,this.initialX=null,this.initialY=null,this.xOffset=0,this.yOffset=0,this.onclose&&"function"==typeof this.onclose&&this.onclose()}},{key:"dragStart",value:function(t){t.preventDefault(),this.zoomedIn?("touchstart"===t.type?(this.initialX=t.touches[0].clientX-this.xOffset,this.initialY=t.touches[0].clientY-this.yOffset):(this.initialX=t.clientX-this.xOffset,this.initialY=t.clientY-this.yOffset),t.target===this.img&&(this.active=!0,this.img.classList.add("dragging"))):this.active=!1}},{key:"dragEnd",value:function(t){var e=this;t.preventDefault(),this.initialX=this.currentX,this.initialY=this.currentY,this.active=!1,setTimeout((function(){e.dragging=!1,e.img.isDragging=!1,e.img.classList.remove("dragging")}),100)}},{key:"drag",value:function(t){this.active&&(t.preventDefault(),"touchmove"===t.type?(this.currentX=t.touches[0].clientX-this.initialX,this.currentY=t.touches[0].clientY-this.initialY):(this.currentX=t.clientX-this.initialX,this.currentY=t.clientY-this.initialY),this.xOffset=this.currentX,this.yOffset=this.currentY,this.img.isDragging=!0,this.dragging=!0,this.setTranslate(this.img,this.currentX,this.currentY))}},{key:"onMove",value:function(t){if(this.zoomedIn){var e=t.clientX-this.img.naturalWidth/2,i=t.clientY-this.img.naturalHeight/2;this.setTranslate(this.img,e,i)}}},{key:"setTranslate",value:function(t,e,i){t.style.transform="translate3d("+e+"px, "+i+"px, 0)"}},{key:"widowWidth",value:function(){return window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth}}]),t}(),d="navigator"in window&&window.navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(Android)|(PlayBook)|(BB10)|(BlackBerry)|(Opera Mini)|(IEMobile)|(webOS)|(MeeGo)/i),u=null!==d||void 0!==document.createTouch||"ontouchstart"in window||"onmsgesturechange"in window||navigator.msMaxTouchPoints,g=document.getElementsByTagName("html")[0],p=function(){var t,e=document.createElement("fakeelement"),i={transition:"transitionend",OTransition:"oTransitionEnd",MozTransition:"transitionend",WebkitTransition:"webkitTransitionEnd"};for(t in i)if(void 0!==e.style[t])return i[t]}(),v=function(){var t,e=document.createElement("fakeelement"),i={animation:"animationend",OAnimation:"oAnimationEnd",MozAnimation:"animationend",WebkitAnimation:"webkitAnimationEnd"};for(t in i)if(void 0!==e.style[t])return i[t]}(),f=Date.now(),m={},y={selector:".glightbox",elements:null,skin:"clean",closeButton:!0,startAt:null,autoplayVideos:!0,descPosition:"bottom",width:"900px",height:"506px",videosWidth:"960px",beforeSlideChange:null,afterSlideChange:null,beforeSlideLoad:null,afterSlideLoad:null,slideInserted:null,slideRemoved:null,onOpen:null,onClose:null,loop:!1,touchNavigation:!0,touchFollowAxis:!0,keyboardNavigation:!0,closeOnOutsideClick:!0,plyr:{css:"https://cdn.plyr.io/3.5.6/plyr.css",js:"https://cdn.plyr.io/3.5.6/plyr.js",config:{ratio:"16:9",youtube:{noCookie:!0,rel:0,showinfo:0,iv_load_policy:3},vimeo:{byline:!1,portrait:!1,title:!1,transparent:!1}}},openEffect:"zoomIn",closeEffect:"zoomOut",slideEffect:"slide",moreText:"See more",moreLength:60,lightboxHtml:"",cssEfects:{fade:{in:"fadeIn",out:"fadeOut"},zoom:{in:"zoomIn",out:"zoomOut"},slide:{in:"slideInRight",out:"slideOutLeft"},slide_back:{in:"slideInLeft",out:"slideOutRight"}},svg:{close:'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve"><g><g><path d="M505.943,6.058c-8.077-8.077-21.172-8.077-29.249,0L6.058,476.693c-8.077,8.077-8.077,21.172,0,29.249C10.096,509.982,15.39,512,20.683,512c5.293,0,10.586-2.019,14.625-6.059L505.943,35.306C514.019,27.23,514.019,14.135,505.943,6.058z"/></g></g><g><g><path d="M505.942,476.694L35.306,6.059c-8.076-8.077-21.172-8.077-29.248,0c-8.077,8.076-8.077,21.171,0,29.248l470.636,470.636c4.038,4.039,9.332,6.058,14.625,6.058c5.293,0,10.587-2.019,14.624-6.057C514.018,497.866,514.018,484.771,505.942,476.694z"/></g></g></svg>',next:'<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.175 477.175" xml:space="preserve"> <g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z"/></g></svg>',prev:'<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.175 477.175" xml:space="preserve"><g><path d="M145.188,238.575l215.5-215.5c5.3-5.3,5.3-13.8,0-19.1s-13.8-5.3-19.1,0l-225.1,225.1c-5.3,5.3-5.3,13.8,0,19.1l225.1,225c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4c5.3-5.3,5.3-13.8,0-19.1L145.188,238.575z"/></g></svg>'}};y.slideHtml='<div class="gslide">\n    <div class="gslide-inner-content">\n        <div class="ginner-container">\n            <div class="gslide-media">\n            </div>\n            <div class="gslide-description">\n                <div class="gdesc-inner">\n                    <h4 class="gslide-title"></h4>\n                    <div class="gslide-desc"></div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>';y.lightboxHtml='<div id="glightbox-body" class="glightbox-container">\n    <div class="gloader visible"></div>\n    <div class="goverlay"></div>\n    <div class="gcontainer">\n    <div id="glightbox-slider" class="gslider"></div>\n    <button class="gnext gbtn" tabindex="0">{nextSVG}</button>\n    <button class="gprev gbtn" tabindex="1">{prevSVG}</button>\n    <button class="gclose gbtn" tabindex="2">{closeSVG}</button>\n</div>\n</div>';var b={href:"",title:"",type:"",description:"",descPosition:"",effect:"",width:"",height:"",node:!1,content:!1};function x(){var t={},e=!0,i=0,n=arguments.length;"[object Boolean]"===Object.prototype.toString.call(arguments[0])&&(e=arguments[0],i++);for(var s=function(i){for(var n in i)Object.prototype.hasOwnProperty.call(i,n)&&(e&&"[object Object]"===Object.prototype.toString.call(i[n])?t[n]=x(!0,t[n],i[n]):t[n]=i[n])};i<n;i++){s(arguments[i])}return t}var w={isFunction:function(t){return"function"==typeof t},isString:function(t){return"string"==typeof t},isNode:function(t){return!(!t||!t.nodeType||1!=t.nodeType)},isArray:function(t){return Array.isArray(t)},isArrayLike:function(t){return t&&t.length&&isFinite(t.length)},isObject:function(e){return"object"===t(e)&&null!=e&&!w.isFunction(e)&&!w.isArray(e)},isNil:function(t){return null==t},has:function(t,e){return null!==t&&hasOwnProperty.call(t,e)},size:function(t){if(w.isObject(t)){if(t.keys)return t.keys().length;var e=0;for(var i in t)w.has(t,i)&&e++;return e}return t.length},isNumber:function(t){return!isNaN(parseFloat(t))&&isFinite(t)}};function S(t,e){if((w.isNode(t)||t===window||t===document)&&(t=[t]),w.isArrayLike(t)||w.isObject(t)||(t=[t]),0!=w.size(t))if(w.isArrayLike(t)&&!w.isObject(t))for(var i=t.length,n=0;n<i&&!1!==e.call(t[n],t[n],n,t);n++);else if(w.isObject(t))for(var s in t)if(w.has(t,s)&&!1===e.call(t[s],t[s],s,t))break}function T(t){var e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:null,i=arguments.length>2&&void 0!==arguments[2]?arguments[2]:null,n=t[f]=t[f]||[],s={all:n,evt:null,found:null};return e&&i&&w.size(n)>0&&S(n,(function(t,n){if(t.eventName==e&&t.fn.toString()==i.toString())return s.found=!0,s.evt=n,!1})),s}function k(t){var e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},i=e.onElement,n=e.withCallback,s=e.avoidDuplicate,o=void 0===s||s,l=e.once,r=void 0!==l&&l,a=e.useCapture,h=void 0!==a&&a,c=arguments.length>2?arguments[2]:void 0,d=i||[];function u(t){w.isFunction(n)&&n.call(c,t,this),r&&u.destroy()}return w.isString(d)&&(d=document.querySelectorAll(d)),u.destroy=function(){S(d,(function(e){var i=T(e,t,u);i.found&&i.all.splice(i.evt,1),e.removeEventListener&&e.removeEventListener(t,u,h)}))},S(d,(function(e){var i=T(e,t,u);(e.addEventListener&&o&&!i.found||!o)&&(e.addEventListener(t,u,h),i.all.push({eventName:t,fn:u}))})),u}function E(t,e){S(e.split(" "),(function(e){return t.classList.add(e)}))}function A(t,e){S(e.split(" "),(function(e){return t.classList.remove(e)}))}function C(t,e){return t.classList.contains(e)}function L(t){var e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:"",i=arguments.length>2&&void 0!==arguments[2]&&arguments[2];if(!t||""===e)return!1;if("none"==e)return w.isFunction(i)&&i(),!1;var n=e.split(" ");S(n,(function(e){E(t,"g"+e)})),k(v,{onElement:t,avoidDuplicate:!1,once:!0,withCallback:function(t,e){S(n,(function(t){A(e,"g"+t)})),w.isFunction(i)&&i()}})}function N(t){var e=document.createDocumentFragment(),i=document.createElement("div");for(i.innerHTML=t;i.firstChild;)e.appendChild(i.firstChild);return e}function I(t,e){for(;t!==document.body;){if("function"==typeof(t=t.parentElement).matches?t.matches(e):t.msMatchesSelector(e))return t}}function O(t){t.style.display="block"}function M(t){t.style.display="none"}function q(){return{width:window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth,height:window.innerHeight||document.documentElement.clientHeight||document.body.clientHeight}}function z(t){if(C(t.target,"plyr--html5")){var e=I(t.target,".gslide-media");"enterfullscreen"==t.type&&E(e,"fullscreen"),"exitfullscreen"==t.type&&A(e,"fullscreen")}}function P(t){return w.isNumber(t)?"".concat(t,"px"):t}function D(t,e){var i="video"==t.type?P(e.videosWidth):P(e.width),n=P(e.height);return t.width=w.has(t,"width")&&""!==t.width?P(t.width):i,t.height=w.has(t,"height")&&""!==t.height?P(t.height):n,t}var X=function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null,e=arguments.length>1?arguments[1]:void 0,i=x({descPosition:e.descPosition},b);if(w.isObject(t)&&!w.isNode(t)){w.has(t,"type")||(w.has(t,"content")&&t.content?t.type="inline":w.has(t,"href")&&(t.type=W(t.href)));var n=x(i,t);return D(n,e),n}var s="",o=t.getAttribute("data-glightbox"),l=t.nodeName.toLowerCase();if("a"===l&&(s=t.href),"img"===l&&(s=t.src),i.href=s,S(i,(function(n,s){w.has(e,s)&&"width"!==s&&(i[s]=e[s]);var o=t.dataset[s];w.isNil(o)||(i[s]=o)})),i.content&&(i.type="inline"),!i.type&&s&&(i.type=W(s)),w.isNil(o)){if("a"==l){var r=t.title;w.isNil(r)||""===r||(i.title=r)}if("img"==l){var a=t.alt;w.isNil(a)||""===a||(i.title=a)}var h=t.getAttribute("data-description");w.isNil(h)||""===h||(i.description=h)}else{var c=[];S(i,(function(t,e){c.push(";\\s?"+e)})),c=c.join("\\s?:|"),""!==o.trim()&&S(i,(function(t,e){var n=o,s=new RegExp("s?"+e+"s?:s?(.*?)("+c+"s?:|$)"),l=n.match(s);if(l&&l.length&&l[1]){var r=l[1].trim().replace(/;\s*$/,"");i[e]=r}}))}if(i.description&&"."==i.description.substring(0,1)&&document.querySelector(i.description))i.description=document.querySelector(i.description).innerHTML;else{var d=t.querySelector(".glightbox-desc");d&&(i.description=d.innerHTML)}return D(i,e),i},B=function(){var t=this,e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null,i=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},n=arguments.length>2&&void 0!==arguments[2]&&arguments[2];if(C(e,"loaded"))return!1;w.isFunction(this.settings.beforeSlideLoad)&&this.settings.beforeSlideLoad({index:i.index,slide:e,player:!1});var s=i.type,o=i.descPosition,l=e.querySelector(".gslide-media"),r=e.querySelector(".gslide-title"),a=e.querySelector(".gslide-desc"),h=e.querySelector(".gdesc-inner"),u=n,g="gSlideTitle_"+i.index,p="gSlideDesc_"+i.index;if(w.isFunction(this.settings.afterSlideLoad)&&(u=function(){w.isFunction(n)&&n(),t.settings.afterSlideLoad({index:i.index,slide:e,player:t.getSlidePlayerInstance(i.index)})}),""==i.title&&""==i.description?h&&h.parentNode.parentNode.removeChild(h.parentNode):(r&&""!==i.title?(r.id=g,r.innerHTML=i.title):r.parentNode.removeChild(r),a&&""!==i.description?(a.id=p,d&&this.settings.moreLength>0?(i.smallDescription=$(i.description,this.settings.moreLength,this.settings.moreText),a.innerHTML=i.smallDescription,U.apply(this,[a,i])):a.innerHTML=i.description):a.parentNode.removeChild(a),E(l.parentNode,"desc-".concat(o)),E(h.parentNode,"description-".concat(o))),E(l,"gslide-".concat(s)),E(e,"loaded"),"video"===s)return E(l.parentNode,"gvideo-container"),l.insertBefore(N('<div class="gvideo-wrapper"></div>'),l.firstChild),void F.apply(this,[e,i,u]);if("external"===s){var v=Y({url:i.href,callback:u});return l.parentNode.style.maxWidth=i.width,l.parentNode.style.height=i.height,void l.appendChild(v)}if("inline"!==s){if("image"===s){var f=new Image;return f.addEventListener("load",(function(){f.naturalWidth>f.offsetWidth&&(E(f,"zoomable"),new c(f,e,(function(){t.resize(e)}))),w.isFunction(u)&&u()}),!1),f.src=i.href,f.alt="",""!==i.title&&f.setAttribute("aria-labelledby",g),""!==i.description&&f.setAttribute("aria-describedby",p),void l.insertBefore(f,l.firstChild)}w.isFunction(u)&&u()}else H.apply(this,[e,i,u])};function F(t,e,i){var n=this,s="gvideo"+e.index,o=t.querySelector(".gvideo-wrapper");_(this.settings.plyr.css);var l=e.href,r=location.protocol.replace(":",""),a="",h="",c=!1;"file"==r&&(r="http"),o.parentNode.style.maxWidth=e.width,_(this.settings.plyr.js,"Plyr",(function(){if(l.match(/vimeo\.com\/([0-9]*)/)){var t=/vimeo.*\/(\d+)/i.exec(l);a="vimeo",h=t[1]}if(l.match(/(youtube\.com|youtube-nocookie\.com)\/watch\?v=([a-zA-Z0-9\-_]+)/)||l.match(/youtu\.be\/([a-zA-Z0-9\-_]+)/)||l.match(/(youtube\.com|youtube-nocookie\.com)\/embed\/([a-zA-Z0-9\-_]+)/)){var r=function(t){var e="";e=void 0!==(t=t.replace(/(>|<)/gi,"").split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/))[2]?(e=t[2].split(/[^0-9a-z_\-]/i))[0]:t;return e}(l);a="youtube",h=r}if(null!==l.match(/\.(mp4|ogg|webm|mov)$/)){a="local";var d='<video id="'+s+'" ';d+='style="background:#000; max-width: '.concat(e.width,';" '),d+='preload="metadata" ',d+='x-webkit-airplay="allow" ',d+='webkit-playsinline="" ',d+="controls ",d+='class="gvideo-local">';var u=l.toLowerCase().split(".").pop(),g={mp4:"",ogg:"",webm:""};for(var p in g[u="mov"==u?"mp4":u]=l,g)if(g.hasOwnProperty(p)){var v=g[p];e.hasOwnProperty(p)&&(v=e[p]),""!==v&&(d+='<source src="'.concat(v,'" type="video/').concat(p,'">'))}c=N(d+="</video>")}var f=c||N('<div id="'.concat(s,'" data-plyr-provider="').concat(a,'" data-plyr-embed-id="').concat(h,'"></div>'));E(o,"".concat(a,"-video gvideo")),o.appendChild(f),o.setAttribute("data-id",s),o.setAttribute("data-index",e.index);var y=w.has(n.settings.plyr,"config")?n.settings.plyr.config:{},b=new Plyr("#"+s,y);b.on("ready",(function(t){var e=t.detail.plyr;m[s]=e,w.isFunction(i)&&i()})),b.on("enterfullscreen",z),b.on("exitfullscreen",z)}))}function Y(t){var e=t.url,i=t.allow,n=t.callback,s=t.appendTo,o=document.createElement("iframe");return o.className="vimeo-video gvideo",o.src=e,o.style.width="100%",o.style.height="100%",i&&o.setAttribute("allow",i),o.onload=function(){E(o,"node-ready"),w.isFunction(n)&&n()},s&&s.appendChild(o),o}function _(t,e,i){if(w.isNil(t))console.error("Inject videos api error");else{var n;if(w.isFunction(e)&&(i=e,e=!1),-1!==t.indexOf(".css")){if((n=document.querySelectorAll('link[href="'+t+'"]'))&&n.length>0)return void(w.isFunction(i)&&i());var s=document.getElementsByTagName("head")[0],o=s.querySelectorAll('link[rel="stylesheet"]'),l=document.createElement("link");return l.rel="stylesheet",l.type="text/css",l.href=t,l.media="all",o?s.insertBefore(l,o[0]):s.appendChild(l),void(w.isFunction(i)&&i())}if((n=document.querySelectorAll('script[src="'+t+'"]'))&&n.length>0){if(w.isFunction(i)){if(w.isString(e))return j((function(){return void 0!==window[e]}),(function(){i()})),!1;i()}}else{var r=document.createElement("script");r.type="text/javascript",r.src=t,r.onload=function(){if(w.isFunction(i)){if(w.isString(e))return j((function(){return void 0!==window[e]}),(function(){i()})),!1;i()}},document.body.appendChild(r)}}}function j(t,e,i,n){if(t())e();else{var s;i||(i=100);var o=setInterval((function(){t()&&(clearInterval(o),s&&clearTimeout(s),e())}),i);n&&(s=setTimeout((function(){clearInterval(o)}),n))}}function H(t,e,i){var n,s=this,o=t.querySelector(".gslide-media"),l=!(!w.has(e,"href")||!e.href)&&e.href.split("#").pop().trim(),r=!(!w.has(e,"content")||!e.content)&&e.content;if(r&&(w.isString(r)&&(n=N('<div class="ginlined-content">'.concat(r,"</div>"))),w.isNode(r))){"none"==r.style.display&&(r.style.display="block");var a=document.createElement("div");a.className="ginlined-content",a.appendChild(r),n=a}if(l){var h=document.getElementById(l);if(!h)return!1;var c=h.cloneNode(!0);c.style.height=e.height,c.style.maxWidth=e.width,E(c,"ginlined-content"),n=c}if(!n)return console.error("Unable to append inline slide content",e),!1;o.style.height=e.height,o.style.width=e.width,o.appendChild(n),this.events["inlineclose"+l]=k("click",{onElement:o.querySelectorAll(".gtrigger-close"),withCallback:function(t){t.preventDefault(),s.close()}}),w.isFunction(i)&&i()}var W=function(t){var e=t;if(null!==(t=t.toLowerCase()).match(/\.(jpeg|jpg|jpe|gif|png|apn|webp|svg)$/))return"image";if(t.match(/(youtube\.com|youtube-nocookie\.com)\/watch\?v=([a-zA-Z0-9\-_]+)/)||t.match(/youtu\.be\/([a-zA-Z0-9\-_]+)/)||t.match(/(youtube\.com|youtube-nocookie\.com)\/embed\/([a-zA-Z0-9\-_]+)/))return"video";if(t.match(/vimeo\.com\/([0-9]*)/))return"video";if(null!==t.match(/\.(mp4|ogg|webm|mov)$/))return"video";if(t.indexOf("#")>-1&&""!==e.split("#").pop().trim())return"inline";return t.includes("gajax=true")?"ajax":"external"};function V(){var t=this;if(this.events.hasOwnProperty("keyboard"))return!1;this.events.keyboard=k("keydown",{onElement:window,withCallback:function(e,i){var n=(e=e||window.event).keyCode;if(9==n){var o=!(!document.activeElement||!document.activeElement.nodeName)&&document.activeElement.nodeName.toLocaleLowerCase();if("input"==o||"textarea"==o||"button"==o)return;e.preventDefault();var l=document.querySelectorAll(".gbtn");if(!l||l.length<=0)return;var r=s(l).filter((function(t){return C(t,"focused")}));if(!r.length){var a=document.querySelector('.gbtn[tabindex="0"]');return void(a&&(a.focus(),E(a,"focused")))}l.forEach((function(t){return A(t,"focused")}));var h=r[0].getAttribute("tabindex");h=h||"0";var c=parseInt(h)+1;c>l.length-1&&(c="0");var d=document.querySelector('.gbtn[tabindex="'.concat(c,'"]'));d&&(d.focus(),E(d,"focused"))}39==n&&t.nextSlide(),37==n&&t.prevSlide(),27==n&&t.close()}})}function G(){var t=this;if(this.events.hasOwnProperty("touch"))return!1;var e,i,n,s=q(),o=s.width,l=s.height,r=!1,a=null,c=null,d=null,u=!1,g=1,p=1,v=!1,f=!1,m=null,y=null,b=null,x=null,w=0,S=0,T=!1,k=!1,L={},N={},O=0,M=0,z=this,P=document.getElementById("glightbox-slider"),D=document.querySelector(".goverlay"),X=(this.loop(),new h(P,{touchStart:function(t){if(C(t.targetTouches[0].target,"ginner-container")||I(t.targetTouches[0].target,".gslide-desc"))return r=!1,!1;r=!0,N=t.targetTouches[0],L.pageX=t.targetTouches[0].pageX,L.pageY=t.targetTouches[0].pageY,O=t.targetTouches[0].clientX,M=t.targetTouches[0].clientY,a=z.activeSlide,c=a.querySelector(".gslide-media"),n=a.querySelector(".gslide-inline"),d=null,C(c,"gslide-image")&&(d=c.querySelector("img")),A(D,"greset")},touchMove:function(s){if(r&&(N=s.targetTouches[0],!v&&!f)){if(n&&n.offsetHeight>l){var a=L.pageX-N.pageX;if(Math.abs(a)<=13)return!1}u=!0;var h,g=s.targetTouches[0].clientX,p=s.targetTouches[0].clientY,m=O-g,y=M-p;if(Math.abs(m)>Math.abs(y)?(T=!1,k=!0):(k=!1,T=!0),e=N.pageX-L.pageX,w=100*e/o,i=N.pageY-L.pageY,S=100*i/l,T&&d&&(h=1-Math.abs(i)/l,D.style.opacity=h,t.settings.touchFollowAxis&&(w=0)),k&&(h=1-Math.abs(e)/o,c.style.opacity=h,t.settings.touchFollowAxis&&(S=0)),!d)return R(c,"translate3d(".concat(w,"%, 0, 0)"));R(c,"translate3d(".concat(w,"%, ").concat(S,"%, 0)"))}},touchEnd:function(){if(r){if(u=!1,f||v)return b=m,void(x=y);var e=Math.abs(parseInt(S)),i=Math.abs(parseInt(w));if(!(e>29&&d))return e<29&&i<25?(E(D,"greset"),D.style.opacity=1,Z(c)):void 0;t.close()}},multipointEnd:function(){setTimeout((function(){v=!1}),50)},multipointStart:function(){v=!0,g=p||1},pinch:function(t){if(!d||u)return!1;v=!0,d.scaleX=d.scaleY=g*t.zoom;var e=g*t.zoom;if(f=!0,e<=1)return f=!1,e=1,x=null,b=null,m=null,y=null,void d.setAttribute("style","");e>4.5&&(e=4.5),d.style.transform="scale3d(".concat(e,", ").concat(e,", 1)"),p=e},pressMove:function(t){if(f&&!v){var e=N.pageX-L.pageX,i=N.pageY-L.pageY;b&&(e+=b),x&&(i+=x),m=e,y=i;var n="translate3d(".concat(e,"px, ").concat(i,"px, 0)");p&&(n+=" scale3d(".concat(p,", ").concat(p,", 1)")),R(d,n)}},swipe:function(e){if(!f)if(v)v=!1;else{if("Left"==e.direction){if(t.index==t.elements.length-1)return Z(c);t.nextSlide()}if("Right"==e.direction){if(0==t.index)return Z(c);t.prevSlide()}}}}));this.events.touch=X}function R(t){var e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:"";if(""==e)return t.style.webkitTransform="",t.style.MozTransform="",t.style.msTransform="",t.style.OTransform="",t.style.transform="",!1;t.style.webkitTransform=e,t.style.MozTransform=e,t.style.msTransform=e,t.style.OTransform=e,t.style.transform=e}function Z(t){var e=C(t,"gslide-media")?t:t.querySelector(".gslide-media"),i=t.querySelector(".gslide-description");E(e,"greset"),R(e,"translate3d(0, 0, 0)");k(p,{onElement:e,once:!0,withCallback:function(t,i){A(e,"greset")}});e.style.opacity="",i&&(i.style.opacity="")}function $(t){var e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:50,i=arguments.length>2&&void 0!==arguments[2]&&arguments[2],n=i;if((t=t.trim()).length<=e)return t;var s=t.substr(0,e-1);return n?s+'... <a href="javascript:void(0)" class="desc-more">'+i+"</a>":s}function U(t,e){var i=t.querySelector(".desc-more");if(!i)return!1;k("click",{onElement:i,withCallback:function(t,i){t.preventDefault();var n=document.body,s=I(i,".gslide-desc");if(!s)return!1;s.innerHTML=e.description,E(n,"gdesc-open");var o=k("click",{onElement:[n,I(s,".gslide-description")],withCallback:function(t,i){"a"!==t.target.nodeName.toLowerCase()&&(A(n,"gdesc-open"),E(n,"gdesc-closed"),s.innerHTML=e.smallDescription,U(s,e),setTimeout((function(){A(n,"gdesc-closed")}),400),o.destroy())}})}})}var J=function(){function t(){var i=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};e(this,t),this.settings=x(y,i),this.effectsClasses=this.getAnimationClasses(),this.slidesData={}}return n(t,[{key:"init",value:function(){var t=this;this.baseEvents=k("click",{onElement:this.getSelector(),withCallback:function(e,i){e.preventDefault(),t.open(i)}}),this.elements=this.getElements()}},{key:"open",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null,e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:null;if(0==this.elements.length)return!1;this.activeSlide=null,this.prevActiveSlideIndex=null,this.prevActiveSlide=null;var i=w.isNumber(e)?e:this.settings.startAt;w.isNode(t)&&w.isNil(i)&&(i=this.getElementIndex(t))<0&&(i=0),w.isNumber(i)||(i=0),this.build(),L(this.overlay,"none"==this.settings.openEffect?"none":this.settings.cssEfects.fade.in);var n=document.body,s=window.innerWidth-document.documentElement.clientWidth;if(s>0){var o=document.createElement("style");o.type="text/css",o.className="gcss-styles",o.innerText=".gscrollbar-fixer {margin-right: ".concat(s,"px}"),document.head.appendChild(o),E(n,"gscrollbar-fixer")}if(E(n,"glightbox-open"),E(g,"glightbox-open"),d&&(E(document.body,"glightbox-mobile"),this.settings.slideEffect="slide"),this.showSlide(i,!0),1==this.elements.length?(M(this.prevButton),M(this.nextButton)):(O(this.prevButton),O(this.nextButton)),this.lightboxOpen=!0,w.isFunction(this.settings.onOpen)&&this.settings.onOpen(),u&&this.settings.touchNavigation)return G.apply(this),!1;this.settings.keyboardNavigation&&V.apply(this)}},{key:"openAt",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:0;this.open(null,t)}},{key:"showSlide",value:function(){var t=this,e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:0,i=arguments.length>1&&void 0!==arguments[1]&&arguments[1];O(this.loader),this.index=parseInt(e);var n=this.slidesContainer.querySelector(".current");n&&A(n,"current"),this.slideAnimateOut();var s=this.slidesContainer.querySelectorAll(".gslide")[e];if(C(s,"loaded"))this.slideAnimateIn(s,i),M(this.loader);else{O(this.loader);var o=this.elements[e];o.index=e,this.slidesData[e]=o,B.apply(this,[s,o,function(){M(t.loader),t.resize(),t.slideAnimateIn(s,i)}])}this.slideDescription=s.querySelector(".gslide-description"),this.slideDescriptionContained=this.slideDescription&&C(this.slideDescription.parentNode,"gslide-media"),this.preloadSlide(e+1),this.preloadSlide(e-1),this.updateNavigationClasses(),this.activeSlide=s}},{key:"preloadSlide",value:function(t){var e=this;if(t<0||t>this.elements.length-1)return!1;if(w.isNil(this.elements[t]))return!1;var i=this.slidesContainer.querySelectorAll(".gslide")[t];if(C(i,"loaded"))return!1;var n=this.elements[t];n.index=t,this.slidesData[t]=n;var s=n.sourcetype;"video"==s||"external"==s?setTimeout((function(){B.apply(e,[i,n])}),200):B.apply(this,[i,n])}},{key:"prevSlide",value:function(){this.goToSlide(this.index-1)}},{key:"nextSlide",value:function(){this.goToSlide(this.index+1)}},{key:"goToSlide",value:function(){var t=arguments.length>0&&void 0!==arguments[0]&&arguments[0];this.prevActiveSlide=this.activeSlide,this.prevActiveSlideIndex=this.index;var e=this.loop();if(!e&&(t<0||t>this.elements.length-1))return!1;t<0?t=this.elements.length-1:t>=this.elements.length&&(t=0),this.showSlide(t)}},{key:"insertSlide",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{},e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:-1,i=x({descPosition:this.settings.descPosition},b),n=N(this.settings.slideHtml),s=this.elements.length-1;if(e<0&&(e=this.elements.length),(t=x(i,t)).index=e,t.node=!1,this.elements.splice(e,0,t),this.slidesContainer){if(e>s)this.slidesContainer.appendChild(n);else{var o=this.slidesContainer.querySelectorAll(".gslide")[e];this.slidesContainer.insertBefore(n,o)}(0==this.index&&0==e||this.index-1==e||this.index+1==e)&&this.preloadSlide(e),0==this.index&&0==e&&(this.index=1),this.updateNavigationClasses()}w.isFunction(this.settings.slideInserted)&&this.settings.slideInserted({index:e,slide:this.slidesContainer.querySelectorAll(".gslide")[e],player:this.getSlidePlayerInstance(e)})}},{key:"removeSlide",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:-1;if(t<0||t>this.elements.length-1)return!1;var e=this.slidesContainer&&this.slidesContainer.querySelectorAll(".gslide")[t];e&&(this.getActiveSlideIndex()==t&&(t==this.elements.length-1?this.prevSlide():this.nextSlide()),e.parentNode.removeChild(e)),this.elements.splice(t,1),w.isFunction(this.settings.slideRemoved)&&this.settings.slideRemoved(t)}},{key:"slideAnimateIn",value:function(t,e){var i=this,n=t.querySelector(".gslide-media"),s=t.querySelector(".gslide-description"),o={index:this.prevActiveSlideIndex,slide:this.prevActiveSlide,player:this.getSlidePlayerInstance(this.prevActiveSlideIndex)},l={index:this.index,slide:this.activeSlide,player:this.getSlidePlayerInstance(this.index)};if(n.offsetWidth>0&&s&&(M(s),s.style.display=""),A(t,this.effectsClasses),e)L(t,this.settings.openEffect,(function(){!d&&i.settings.autoplayVideos&&i.playSlideVideo(t),w.isFunction(i.settings.afterSlideChange)&&i.settings.afterSlideChange.apply(i,[o,l])}));else{var r=this.settings.slideEffect,a="none"!==r?this.settings.cssEfects[r].in:r;this.prevActiveSlideIndex>this.index&&"slide"==this.settings.slideEffect&&(a=this.settings.cssEfects.slide_back.in),L(t,a,(function(){!d&&i.settings.autoplayVideos&&i.playSlideVideo(t),w.isFunction(i.settings.afterSlideChange)&&i.settings.afterSlideChange.apply(i,[o,l])}))}setTimeout((function(){i.resize(t)}),100),E(t,"current")}},{key:"slideAnimateOut",value:function(){if(!this.prevActiveSlide)return!1;var t=this.prevActiveSlide;A(t,this.effectsClasses),E(t,"prev");var e=this.settings.slideEffect,i="none"!==e?this.settings.cssEfects[e].out:e;this.stopSlideVideo(t),w.isFunction(this.settings.beforeSlideChange)&&this.settings.beforeSlideChange.apply(this,[{index:this.prevActiveSlideIndex,slide:this.prevActiveSlide,player:this.getSlidePlayerInstance(this.prevActiveSlideIndex)},{index:this.index,slide:this.activeSlide,player:this.getSlidePlayerInstance(this.index)}]),this.prevActiveSlideIndex>this.index&&"slide"==this.settings.slideEffect&&(i=this.settings.cssEfects.slide_back.out),L(t,i,(function(){var e=t.querySelector(".gslide-media"),i=t.querySelector(".gslide-description");e.style.transform="",A(e,"greset"),e.style.opacity="",i&&(i.style.opacity=""),A(t,"prev")}))}},{key:"getAllPlayers",value:function(){return m}},{key:"getSlidePlayerInstance",value:function(t){var e="gvideo"+t;return!(!w.has(m,e)||!m[e])&&m[e]}},{key:"stopSlideVideo",value:function(t){if(w.isNode(t)){var e=t.querySelector(".gvideo-wrapper");e&&(t=e.getAttribute("data-index"))}var i=this.getSlidePlayerInstance(t);i&&i.playing&&i.pause()}},{key:"playSlideVideo",value:function(t){if(w.isNode(t)){var e=t.querySelector(".gvideo-wrapper");e&&(t=e.getAttribute("data-index"))}var i=this.getSlidePlayerInstance(t);i&&!i.playing&&i.play()}},{key:"setElements",value:function(t){var e=this;this.settings.elements=!1;var i=[];S(t,(function(t){var n=X(t,e.settings);i.push(n)})),this.elements=i,this.lightboxOpen&&(this.slidesContainer.innerHTML="",S(this.elements,(function(){var t=N(e.settings.slideHtml);e.slidesContainer.appendChild(t)})),this.showSlide(0,!0))}},{key:"getElementIndex",value:function(t){var e=!1;return S(this.elements,(function(i,n){if(w.has(i,"node")&&i.node==t)return e=n,!0})),e}},{key:"getElements",value:function(){var t=this,e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null,i=[];this.elements=this.elements?this.elements:[],!w.isNil(this.settings.elements)&&w.isArray(this.settings.elements)&&(i=this.settings.elements);var n=!1,s=this.getSelector();if(null!==e){var o=e.getAttribute("data-gallery");o&&""!==o&&(n=document.querySelectorAll('[data-gallery="'.concat(o,'"]')))}return 0==n&&s&&(n=document.querySelectorAll(this.getSelector())),S(n=Array.prototype.slice.call(n),(function(e,n){var s=X(e,t.settings);s.node=e,s.index=n,i.push(s)})),i}},{key:"getSelector",value:function(){return"data-"==this.settings.selector.substring(0,5)?"*[".concat(this.settings.selector,"]"):this.settings.selector}},{key:"getActiveSlide",value:function(){return this.slidesContainer.querySelectorAll(".gslide")[this.index]}},{key:"getActiveSlideIndex",value:function(){return this.index}},{key:"getAnimationClasses",value:function(){var t=[];for(var e in this.settings.cssEfects)if(this.settings.cssEfects.hasOwnProperty(e)){var i=this.settings.cssEfects[e];t.push("g".concat(i.in)),t.push("g".concat(i.out))}return t.join(" ")}},{key:"build",value:function(){var t=this;if(this.built)return!1;var e=w.has(this.settings.svg,"next")?this.settings.svg.next:"",i=w.has(this.settings.svg,"prev")?this.settings.svg.prev:"",n=w.has(this.settings.svg,"close")?this.settings.svg.close:"",s=this.settings.lightboxHtml;s=N(s=(s=(s=s.replace(/{nextSVG}/g,e)).replace(/{prevSVG}/g,i)).replace(/{closeSVG}/g,n)),document.body.appendChild(s);var o=document.getElementById("glightbox-body");this.modal=o;var l=o.querySelector(".gclose");this.prevButton=o.querySelector(".gprev"),this.nextButton=o.querySelector(".gnext"),this.overlay=o.querySelector(".goverlay"),this.loader=o.querySelector(".gloader"),this.slidesContainer=document.getElementById("glightbox-slider"),this.events={},E(this.modal,"glightbox-"+this.settings.skin),this.settings.closeButton&&l&&(this.events.close=k("click",{onElement:l,withCallback:function(e,i){e.preventDefault(),t.close()}})),l&&!this.settings.closeButton&&l.parentNode.removeChild(l),this.nextButton&&(this.events.next=k("click",{onElement:this.nextButton,withCallback:function(e,i){e.preventDefault(),t.nextSlide()}})),this.prevButton&&(this.events.prev=k("click",{onElement:this.prevButton,withCallback:function(e,i){e.preventDefault(),t.prevSlide()}})),this.settings.closeOnOutsideClick&&(this.events.outClose=k("click",{onElement:o,withCallback:function(e,i){C(document.body,"glightbox-mobile")||I(e.target,".ginner-container")||I(e.target,".gbtn")||C(e.target,"gnext")||C(e.target,"gprev")||t.close()}})),S(this.elements,(function(){var e=N(t.settings.slideHtml);t.slidesContainer.appendChild(e)})),u&&E(document.body,"glightbox-touch"),this.events.resize=k("resize",{onElement:window,withCallback:function(){t.resize()}}),this.built=!0}},{key:"resize",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null;if((t=t||this.activeSlide)&&!C(t,"zoomed")){var e=q(),i=t.querySelector(".gvideo-wrapper"),n=t.querySelector(".gslide-image"),s=this.slideDescription,o=e.width,l=e.height;if(o<=768?E(document.body,"glightbox-mobile"):A(document.body,"glightbox-mobile"),i||n){var r=!1;if(s&&(C(s,"description-bottom")||C(s,"description-top"))&&!C(s,"gabsolute")&&(r=!0),n)if(o<=768){var a=n.querySelector("img");a.setAttribute("style","")}else if(r){var h=s.offsetHeight,c=this.slidesData[this.index].width;c=c<=o?c+"px":"100%";var d=n.querySelector("img");d.setAttribute("style","max-height: calc(100vh - ".concat(h,"px)")),s.setAttribute("style","max-width: ".concat(d.offsetWidth,"px;"))}if(i){var u=w.has(this.settings.plyr.config,"ratio")?this.settings.plyr.config.ratio:"16:9",g=u.split(":"),p=this.slidesData[this.index].width,v=p/(parseInt(g[0])/parseInt(g[1]));if(v=Math.floor(v),r&&(l-=s.offsetHeight),l<v&&o>p){var f=i.offsetWidth,m=i.offsetHeight,y=l/m,b={width:f*y,height:m*y};i.parentNode.setAttribute("style","max-width: ".concat(b.width,"px")),r&&s.setAttribute("style","max-width: ".concat(b.width,"px;"))}else i.parentNode.style.maxWidth="".concat(p,"px"),r&&s.setAttribute("style","max-width: ".concat(p,"px;"))}}}}},{key:"reload",value:function(){this.init()}},{key:"updateNavigationClasses",value:function(){var t=this.loop();A(this.nextButton,"disabled"),A(this.prevButton,"disabled"),0==this.index&&this.elements.length-1==0?(E(this.prevButton,"disabled"),E(this.nextButton,"disabled")):0!==this.index||t?this.index!==this.elements.length-1||t||E(this.nextButton,"disabled"):E(this.prevButton,"disabled")}},{key:"loop",value:function(){var t=w.has(this.settings,"loopAtEnd")?this.settings.loopAtEnd:null;return t=w.has(this.settings,"loop")?this.settings.loop:t,t}},{key:"close",value:function(){var t=this;if(!this.lightboxOpen){if(this.events){for(var e in this.events)this.events.hasOwnProperty(e)&&this.events[e].destroy();this.events=null}return!1}if(this.closing)return!1;this.closing=!0,this.stopSlideVideo(this.activeSlide),E(this.modal,"glightbox-closing"),L(this.overlay,"none"==this.settings.openEffect?"none":this.settings.cssEfects.fade.out),L(this.activeSlide,this.settings.closeEffect,(function(){if(t.activeSlide=null,t.prevActiveSlideIndex=null,t.prevActiveSlide=null,t.built=!1,t.events){for(var e in t.events)t.events.hasOwnProperty(e)&&t.events[e].destroy();t.events=null}var i=document.body;A(g,"glightbox-open"),A(i,"glightbox-open touching gdesc-open glightbox-touch glightbox-mobile gscrollbar-fixer"),t.modal.parentNode.removeChild(t.modal),w.isFunction(t.settings.onClose)&&t.settings.onClose();var n=document.querySelector(".gcss-styles");n&&n.parentNode.removeChild(n),t.lightboxOpen=!1,t.closing=null}))}},{key:"destroy",value:function(){this.close(),this.baseEvents.destroy()}}]),t}();return function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{},e=new J(t);return e.init(),e}}));
</script>

<script>
    function counterUp(t){"use strict";this.defaults={duration:3e3,prepend:"",append:"%",selector:".countup",start:0,end:100,intvalues:!1,interval:100};var e=this;this.upating=!1,this.intervalID=null,this.props={};for(var r in this.defaults)"undefined"!=typeof r&&(e.props[r]=e.defaults[r],t.hasOwnProperty(r)&&e.props.hasOwnProperty(r)&&(e.props[r]=t[r]));this.domelems=document.querySelectorAll(this.props.selector),this.elems=[];var n={};this.domelems.forEach(function(t){n.obj=t;var r=parseInt(t.getAttribute("cup-start"));isNaN(r)?n.start=e.props.start:n.start=r;var p=parseInt(t.getAttribute("cup-end"));isNaN(p)?n.end=e.props.end:n.end=p;var a=parseInt(t.getAttribute("cup-duration"));isNaN(a)?n.duration=e.props.duration:n.duration=a;var s=t.getAttribute("cup-prepend");null==s?n.prepend=e.props.prepend:n.prepend=s;var i=t.getAttribute("cup-append");null==i?n.append=e.props.append:n.append=i;var o=t.getAttribute("cup-intval");null==o?n.intvalues=e.props.intvalues:n.intvalues=o,n.step=(n.end-n.start)/(n.duration/e.props.interval),n.val=n.start,e.elems.push(n),n={}})}counterUp.prototype.start=function(){"use strict";var t=this;this.intervalID=setInterval(function(){t.updating||t.update()},t.props.interval)},counterUp.prototype.update=function(){"use strict";this.updating=!0;var t=!0;this.elems.forEach(function(e){e.val+=e.step,e.val<e.end?(1==e.intvalues?e.obj.innerHTML=e.prepend+Math.floor(e.val).toString()+e.append:e.obj.innerHTML=e.prepend+(Math.round(100*e.val)/100).toString()+e.append,t=!1):e.obj.innerHTML=e.prepend+e.end.toString()+e.append}),1==t&&clearInterval(this.intervalID),this.updating=!1};
</script>

<script>
/*!
  * Bootstrap v5.0.0-beta2 (https://getbootstrap.com/)
  * Copyright 2011-2021 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/main/LICENSE)
  */
 !function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e(require("@popperjs/core")):"function"==typeof define&&define.amd?define(["@popperjs/core"],e):(t="undefined"!=typeof globalThis?globalThis:t||self).bootstrap=e(t.Popper)}(this,(function(t){"use strict";function e(t){if(t&&t.__esModule)return t;var e=Object.create(null);return t&&Object.keys(t).forEach((function(n){if("default"!==n){var i=Object.getOwnPropertyDescriptor(t,n);Object.defineProperty(e,n,i.get?i:{enumerable:!0,get:function(){return t[n]}})}})),e.default=t,Object.freeze(e)}var n=e(t);function i(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function o(t,e,n){return e&&i(t.prototype,e),n&&i(t,n),t}function s(){return(s=Object.assign||function(t){for(var e=1;e<arguments.length;e++){var n=arguments[e];for(var i in n)Object.prototype.hasOwnProperty.call(n,i)&&(t[i]=n[i])}return t}).apply(this,arguments)}function r(t,e){var n,i;t.prototype=Object.create(e.prototype),t.prototype.constructor=t,n=t,i=e,(Object.setPrototypeOf||function(t,e){return t.__proto__=e,t})(n,i)}var a,l,c=function(t){do{t+=Math.floor(1e6*Math.random())}while(document.getElementById(t));return t},u=function(t){var e=t.getAttribute("data-bs-target");if(!e||"#"===e){var n=t.getAttribute("href");if(!n||!n.includes("#")&&!n.startsWith("."))return null;n.includes("#")&&!n.startsWith("#")&&(n="#"+n.split("#")[1]),e=n&&"#"!==n?n.trim():null}return e},h=function(t){var e=u(t);return e&&document.querySelector(e)?e:null},d=function(t){var e=u(t);return e?document.querySelector(e):null},f=function(t){if(!t)return 0;var e=window.getComputedStyle(t),n=e.transitionDuration,i=e.transitionDelay,o=Number.parseFloat(n),s=Number.parseFloat(i);return o||s?(n=n.split(",")[0],i=i.split(",")[0],1e3*(Number.parseFloat(n)+Number.parseFloat(i))):0},p=function(t){t.dispatchEvent(new Event("transitionend"))},g=function(t){return(t[0]||t).nodeType},m=function(t,e){var n=!1,i=e+5;t.addEventListener("transitionend",(function e(){n=!0,t.removeEventListener("transitionend",e)})),setTimeout((function(){n||p(t)}),i)},_=function(t,e,n){Object.keys(n).forEach((function(i){var o,s=n[i],r=e[i],a=r&&g(r)?"element":null==(o=r)?""+o:{}.toString.call(o).match(/\s([a-z]+)/i)[1].toLowerCase();if(!new RegExp(s).test(a))throw new TypeError(t.toUpperCase()+': Option "'+i+'" provided type "'+a+'" but expected type "'+s+'".')}))},v=function(t){if(!t)return!1;if(t.style&&t.parentNode&&t.parentNode.style){var e=getComputedStyle(t),n=getComputedStyle(t.parentNode);return"none"!==e.display&&"none"!==n.display&&"hidden"!==e.visibility}return!1},b=function(){return function(){}},y=function(t){return t.offsetHeight},w=function(){var t=window.jQuery;return t&&!document.body.hasAttribute("data-bs-no-jquery")?t:null},E="rtl"===document.documentElement.dir,T=function(t,e){var n;n=function(){var n=w();if(n){var i=n.fn[t];n.fn[t]=e.jQueryInterface,n.fn[t].Constructor=e,n.fn[t].noConflict=function(){return n.fn[t]=i,e.jQueryInterface}}},"loading"===document.readyState?document.addEventListener("DOMContentLoaded",n):n()},A=(a={},l=1,{set:function(t,e,n){void 0===t.bsKey&&(t.bsKey={key:e,id:l},l++),a[t.bsKey.id]=n},get:function(t,e){if(!t||void 0===t.bsKey)return null;var n=t.bsKey;return n.key===e?a[n.id]:null},delete:function(t,e){if(void 0!==t.bsKey){var n=t.bsKey;n.key===e&&(delete a[n.id],delete t.bsKey)}}}),k=function(t,e,n){A.set(t,e,n)},L=function(t,e){return A.get(t,e)},C=/[^.]*(?=\..*)\.|.*/,D=/\..*/,S=/::\d+$/,N={},O=1,I={mouseenter:"mouseover",mouseleave:"mouseout"},j=new Set(["click","dblclick","mouseup","mousedown","contextmenu","mousewheel","DOMMouseScroll","mouseover","mouseout","mousemove","selectstart","selectend","keydown","keypress","keyup","orientationchange","touchstart","touchmove","touchend","touchcancel","pointerdown","pointermove","pointerup","pointerleave","pointercancel","gesturestart","gesturechange","gestureend","focus","blur","change","reset","select","submit","focusin","focusout","load","unload","beforeunload","resize","move","DOMContentLoaded","readystatechange","error","abort","scroll"]);function P(t,e){return e&&e+"::"+O++||t.uidEvent||O++}function x(t){var e=P(t);return t.uidEvent=e,N[e]=N[e]||{},N[e]}function H(t,e,n){void 0===n&&(n=null);for(var i=Object.keys(t),o=0,s=i.length;o<s;o++){var r=t[i[o]];if(r.originalHandler===e&&r.delegationSelector===n)return r}return null}function B(t,e,n){var i="string"==typeof e,o=i?n:e,s=t.replace(D,""),r=I[s];return r&&(s=r),j.has(s)||(s=t),[i,o,s]}function M(t,e,n,i,o){if("string"==typeof e&&t){n||(n=i,i=null);var s=B(e,n,i),r=s[0],a=s[1],l=s[2],c=x(t),u=c[l]||(c[l]={}),h=H(u,a,r?n:null);if(h)h.oneOff=h.oneOff&&o;else{var d=P(a,e.replace(C,"")),f=r?function(t,e,n){return function i(o){for(var s=t.querySelectorAll(e),r=o.target;r&&r!==this;r=r.parentNode)for(var a=s.length;a--;)if(s[a]===r)return o.delegateTarget=r,i.oneOff&&K.off(t,o.type,n),n.apply(r,[o]);return null}}(t,n,i):function(t,e){return function n(i){return i.delegateTarget=t,n.oneOff&&K.off(t,i.type,e),e.apply(t,[i])}}(t,n);f.delegationSelector=r?n:null,f.originalHandler=a,f.oneOff=o,f.uidEvent=d,u[d]=f,t.addEventListener(l,f,r)}}}function R(t,e,n,i,o){var s=H(e[n],i,o);s&&(t.removeEventListener(n,s,Boolean(o)),delete e[n][s.uidEvent])}var K={on:function(t,e,n,i){M(t,e,n,i,!1)},one:function(t,e,n,i){M(t,e,n,i,!0)},off:function(t,e,n,i){if("string"==typeof e&&t){var o=B(e,n,i),s=o[0],r=o[1],a=o[2],l=a!==e,c=x(t),u=e.startsWith(".");if(void 0===r){u&&Object.keys(c).forEach((function(n){!function(t,e,n,i){var o=e[n]||{};Object.keys(o).forEach((function(s){if(s.includes(i)){var r=o[s];R(t,e,n,r.originalHandler,r.delegationSelector)}}))}(t,c,n,e.slice(1))}));var h=c[a]||{};Object.keys(h).forEach((function(n){var i=n.replace(S,"");if(!l||e.includes(i)){var o=h[n];R(t,c,a,o.originalHandler,o.delegationSelector)}}))}else{if(!c||!c[a])return;R(t,c,a,r,s?n:null)}}},trigger:function(t,e,n){if("string"!=typeof e||!t)return null;var i,o=w(),s=e.replace(D,""),r=e!==s,a=j.has(s),l=!0,c=!0,u=!1,h=null;return r&&o&&(i=o.Event(e,n),o(t).trigger(i),l=!i.isPropagationStopped(),c=!i.isImmediatePropagationStopped(),u=i.isDefaultPrevented()),a?(h=document.createEvent("HTMLEvents")).initEvent(s,l,!0):h=new CustomEvent(e,{bubbles:l,cancelable:!0}),void 0!==n&&Object.keys(n).forEach((function(t){Object.defineProperty(h,t,{get:function(){return n[t]}})})),u&&h.preventDefault(),c&&t.dispatchEvent(h),h.defaultPrevented&&void 0!==i&&i.preventDefault(),h}},W=function(){function t(t){t&&(this._element=t,k(t,this.constructor.DATA_KEY,this))}return t.prototype.dispose=function(){var t,e;t=this._element,e=this.constructor.DATA_KEY,A.delete(t,e),this._element=null},t.getInstance=function(t){return L(t,this.DATA_KEY)},o(t,null,[{key:"VERSION",get:function(){return"5.0.0-beta2"}}]),t}(),U=function(t){function e(){return t.apply(this,arguments)||this}r(e,t);var n=e.prototype;return n.close=function(t){var e=t?this._getRootElement(t):this._element,n=this._triggerCloseEvent(e);null===n||n.defaultPrevented||this._removeElement(e)},n._getRootElement=function(t){return d(t)||t.closest(".alert")},n._triggerCloseEvent=function(t){return K.trigger(t,"close.bs.alert")},n._removeElement=function(t){var e=this;if(t.classList.remove("show"),t.classList.contains("fade")){var n=f(t);K.one(t,"transitionend",(function(){return e._destroyElement(t)})),m(t,n)}else this._destroyElement(t)},n._destroyElement=function(t){t.parentNode&&t.parentNode.removeChild(t),K.trigger(t,"closed.bs.alert")},e.jQueryInterface=function(t){return this.each((function(){var n=L(this,"bs.alert");n||(n=new e(this)),"close"===t&&n[t](this)}))},e.handleDismiss=function(t){return function(e){e&&e.preventDefault(),t.close(this)}},o(e,null,[{key:"DATA_KEY",get:function(){return"bs.alert"}}]),e}(W);K.on(document,"click.bs.alert.data-api",'[data-bs-dismiss="alert"]',U.handleDismiss(new U)),T("alert",U);var F=function(t){function e(){return t.apply(this,arguments)||this}return r(e,t),e.prototype.toggle=function(){this._element.setAttribute("aria-pressed",this._element.classList.toggle("active"))},e.jQueryInterface=function(t){return this.each((function(){var n=L(this,"bs.button");n||(n=new e(this)),"toggle"===t&&n[t]()}))},o(e,null,[{key:"DATA_KEY",get:function(){return"bs.button"}}]),e}(W);function z(t){return"true"===t||"false"!==t&&(t===Number(t).toString()?Number(t):""===t||"null"===t?null:t)}function Y(t){return t.replace(/[A-Z]/g,(function(t){return"-"+t.toLowerCase()}))}K.on(document,"click.bs.button.data-api",'[data-bs-toggle="button"]',(function(t){t.preventDefault();var e=t.target.closest('[data-bs-toggle="button"]'),n=L(e,"bs.button");n||(n=new F(e)),n.toggle()})),T("button",F);var X={setDataAttribute:function(t,e,n){t.setAttribute("data-bs-"+Y(e),n)},removeDataAttribute:function(t,e){t.removeAttribute("data-bs-"+Y(e))},getDataAttributes:function(t){if(!t)return{};var e={};return Object.keys(t.dataset).filter((function(t){return t.startsWith("bs")})).forEach((function(n){var i=n.replace(/^bs/,"");i=i.charAt(0).toLowerCase()+i.slice(1,i.length),e[i]=z(t.dataset[n])})),e},getDataAttribute:function(t,e){return z(t.getAttribute("data-bs-"+Y(e)))},offset:function(t){var e=t.getBoundingClientRect();return{top:e.top+document.body.scrollTop,left:e.left+document.body.scrollLeft}},position:function(t){return{top:t.offsetTop,left:t.offsetLeft}}},q=function(t,e){var n;return void 0===e&&(e=document.documentElement),(n=[]).concat.apply(n,Element.prototype.querySelectorAll.call(e,t))},Q=function(t,e){return void 0===e&&(e=document.documentElement),Element.prototype.querySelector.call(e,t)},V=function(t,e){var n;return(n=[]).concat.apply(n,t.children).filter((function(t){return t.matches(e)}))},$=function(t,e){for(var n=t.previousElementSibling;n;){if(n.matches(e))return[n];n=n.previousElementSibling}return[]},G={interval:5e3,keyboard:!0,slide:!1,pause:"hover",wrap:!0,touch:!0},Z={interval:"(number|boolean)",keyboard:"boolean",slide:"(boolean|string)",pause:"(string|boolean)",wrap:"boolean",touch:"boolean"},J=function(t){function e(e,n){var i;return(i=t.call(this,e)||this)._items=null,i._interval=null,i._activeElement=null,i._isPaused=!1,i._isSliding=!1,i.touchTimeout=null,i.touchStartX=0,i.touchDeltaX=0,i._config=i._getConfig(n),i._indicatorsElement=Q(".carousel-indicators",i._element),i._touchSupported="ontouchstart"in document.documentElement||navigator.maxTouchPoints>0,i._pointerEvent=Boolean(window.PointerEvent),i._addEventListeners(),i}r(e,t);var n=e.prototype;return n.next=function(){this._isSliding||this._slide("next")},n.nextWhenVisible=function(){!document.hidden&&v(this._element)&&this.next()},n.prev=function(){this._isSliding||this._slide("prev")},n.pause=function(t){t||(this._isPaused=!0),Q(".carousel-item-next, .carousel-item-prev",this._element)&&(p(this._element),this.cycle(!0)),clearInterval(this._interval),this._interval=null},n.cycle=function(t){t||(this._isPaused=!1),this._interval&&(clearInterval(this._interval),this._interval=null),this._config&&this._config.interval&&!this._isPaused&&(this._updateInterval(),this._interval=setInterval((document.visibilityState?this.nextWhenVisible:this.next).bind(this),this._config.interval))},n.to=function(t){var e=this;this._activeElement=Q(".active.carousel-item",this._element);var n=this._getItemIndex(this._activeElement);if(!(t>this._items.length-1||t<0))if(this._isSliding)K.one(this._element,"slid.bs.carousel",(function(){return e.to(t)}));else{if(n===t)return this.pause(),void this.cycle();var i=t>n?"next":"prev";this._slide(i,this._items[t])}},n.dispose=function(){t.prototype.dispose.call(this),K.off(this._element,".bs.carousel"),this._items=null,this._config=null,this._interval=null,this._isPaused=null,this._isSliding=null,this._activeElement=null,this._indicatorsElement=null},n._getConfig=function(t){return t=s({},G,t),_("carousel",t,Z),t},n._handleSwipe=function(){var t=Math.abs(this.touchDeltaX);if(!(t<=40)){var e=t/this.touchDeltaX;this.touchDeltaX=0,e>0&&(E?this.next():this.prev()),e<0&&(E?this.prev():this.next())}},n._addEventListeners=function(){var t=this;this._config.keyboard&&K.on(this._element,"keydown.bs.carousel",(function(e){return t._keydown(e)})),"hover"===this._config.pause&&(K.on(this._element,"mouseenter.bs.carousel",(function(e){return t.pause(e)})),K.on(this._element,"mouseleave.bs.carousel",(function(e){return t.cycle(e)}))),this._config.touch&&this._touchSupported&&this._addTouchEventListeners()},n._addTouchEventListeners=function(){var t=this,e=function(e){!t._pointerEvent||"pen"!==e.pointerType&&"touch"!==e.pointerType?t._pointerEvent||(t.touchStartX=e.touches[0].clientX):t.touchStartX=e.clientX},n=function(e){!t._pointerEvent||"pen"!==e.pointerType&&"touch"!==e.pointerType||(t.touchDeltaX=e.clientX-t.touchStartX),t._handleSwipe(),"hover"===t._config.pause&&(t.pause(),t.touchTimeout&&clearTimeout(t.touchTimeout),t.touchTimeout=setTimeout((function(e){return t.cycle(e)}),500+t._config.interval))};q(".carousel-item img",this._element).forEach((function(t){K.on(t,"dragstart.bs.carousel",(function(t){return t.preventDefault()}))})),this._pointerEvent?(K.on(this._element,"pointerdown.bs.carousel",(function(t){return e(t)})),K.on(this._element,"pointerup.bs.carousel",(function(t){return n(t)})),this._element.classList.add("pointer-event")):(K.on(this._element,"touchstart.bs.carousel",(function(t){return e(t)})),K.on(this._element,"touchmove.bs.carousel",(function(e){return function(e){e.touches&&e.touches.length>1?t.touchDeltaX=0:t.touchDeltaX=e.touches[0].clientX-t.touchStartX}(e)})),K.on(this._element,"touchend.bs.carousel",(function(t){return n(t)})))},n._keydown=function(t){/input|textarea/i.test(t.target.tagName)||("ArrowLeft"===t.key?(t.preventDefault(),E?this.next():this.prev()):"ArrowRight"===t.key&&(t.preventDefault(),E?this.prev():this.next()))},n._getItemIndex=function(t){return this._items=t&&t.parentNode?q(".carousel-item",t.parentNode):[],this._items.indexOf(t)},n._getItemByDirection=function(t,e){var n="next"===t,i="prev"===t,o=this._getItemIndex(e),s=this._items.length-1;if((i&&0===o||n&&o===s)&&!this._config.wrap)return e;var r=(o+("prev"===t?-1:1))%this._items.length;return-1===r?this._items[this._items.length-1]:this._items[r]},n._triggerSlideEvent=function(t,e){var n=this._getItemIndex(t),i=this._getItemIndex(Q(".active.carousel-item",this._element));return K.trigger(this._element,"slide.bs.carousel",{relatedTarget:t,direction:e,from:i,to:n})},n._setActiveIndicatorElement=function(t){if(this._indicatorsElement){var e=Q(".active",this._indicatorsElement);e.classList.remove("active"),e.removeAttribute("aria-current");for(var n=q("[data-bs-target]",this._indicatorsElement),i=0;i<n.length;i++)if(Number.parseInt(n[i].getAttribute("data-bs-slide-to"),10)===this._getItemIndex(t)){n[i].classList.add("active"),n[i].setAttribute("aria-current","true");break}}},n._updateInterval=function(){var t=this._activeElement||Q(".active.carousel-item",this._element);if(t){var e=Number.parseInt(t.getAttribute("data-bs-interval"),10);e?(this._config.defaultInterval=this._config.defaultInterval||this._config.interval,this._config.interval=e):this._config.interval=this._config.defaultInterval||this._config.interval}},n._slide=function(t,e){var n=this,i=Q(".active.carousel-item",this._element),o=this._getItemIndex(i),s=e||i&&this._getItemByDirection(t,i),r=this._getItemIndex(s),a=Boolean(this._interval),l="next"===t?"carousel-item-start":"carousel-item-end",c="next"===t?"carousel-item-next":"carousel-item-prev",u="next"===t?"left":"right";if(s&&s.classList.contains("active"))this._isSliding=!1;else if(!this._triggerSlideEvent(s,u).defaultPrevented&&i&&s){if(this._isSliding=!0,a&&this.pause(),this._setActiveIndicatorElement(s),this._activeElement=s,this._element.classList.contains("slide")){s.classList.add(c),y(s),i.classList.add(l),s.classList.add(l);var h=f(i);K.one(i,"transitionend",(function(){s.classList.remove(l,c),s.classList.add("active"),i.classList.remove("active",c,l),n._isSliding=!1,setTimeout((function(){K.trigger(n._element,"slid.bs.carousel",{relatedTarget:s,direction:u,from:o,to:r})}),0)})),m(i,h)}else i.classList.remove("active"),s.classList.add("active"),this._isSliding=!1,K.trigger(this._element,"slid.bs.carousel",{relatedTarget:s,direction:u,from:o,to:r});a&&this.cycle()}},e.carouselInterface=function(t,n){var i=L(t,"bs.carousel"),o=s({},G,X.getDataAttributes(t));"object"==typeof n&&(o=s({},o,n));var r="string"==typeof n?n:o.slide;if(i||(i=new e(t,o)),"number"==typeof n)i.to(n);else if("string"==typeof r){if(void 0===i[r])throw new TypeError('No method named "'+r+'"');i[r]()}else o.interval&&o.ride&&(i.pause(),i.cycle())},e.jQueryInterface=function(t){return this.each((function(){e.carouselInterface(this,t)}))},e.dataApiClickHandler=function(t){var n=d(this);if(n&&n.classList.contains("carousel")){var i=s({},X.getDataAttributes(n),X.getDataAttributes(this)),o=this.getAttribute("data-bs-slide-to");o&&(i.interval=!1),e.carouselInterface(n,i),o&&L(n,"bs.carousel").to(o),t.preventDefault()}},o(e,null,[{key:"Default",get:function(){return G}},{key:"DATA_KEY",get:function(){return"bs.carousel"}}]),e}(W);K.on(document,"click.bs.carousel.data-api","[data-bs-slide], [data-bs-slide-to]",J.dataApiClickHandler),K.on(window,"load.bs.carousel.data-api",(function(){for(var t=q('[data-bs-ride="carousel"]'),e=0,n=t.length;e<n;e++)J.carouselInterface(t[e],L(t[e],"bs.carousel"))})),T("carousel",J);var tt={toggle:!0,parent:""},et={toggle:"boolean",parent:"(string|element)"},nt=function(t){function e(e,n){var i;(i=t.call(this,e)||this)._isTransitioning=!1,i._config=i._getConfig(n),i._triggerArray=q('[data-bs-toggle="collapse"][href="#'+e.id+'"],[data-bs-toggle="collapse"][data-bs-target="#'+e.id+'"]');for(var o=q('[data-bs-toggle="collapse"]'),s=0,r=o.length;s<r;s++){var a=o[s],l=h(a),c=q(l).filter((function(t){return t===e}));null!==l&&c.length&&(i._selector=l,i._triggerArray.push(a))}return i._parent=i._config.parent?i._getParent():null,i._config.parent||i._addAriaAndCollapsedClass(i._element,i._triggerArray),i._config.toggle&&i.toggle(),i}r(e,t);var n=e.prototype;return n.toggle=function(){this._element.classList.contains("show")?this.hide():this.show()},n.show=function(){var t=this;if(!this._isTransitioning&&!this._element.classList.contains("show")){var n,i;this._parent&&0===(n=q(".show, .collapsing",this._parent).filter((function(e){return"string"==typeof t._config.parent?e.getAttribute("data-bs-parent")===t._config.parent:e.classList.contains("collapse")}))).length&&(n=null);var o=Q(this._selector);if(n){var s=n.find((function(t){return o!==t}));if((i=s?L(s,"bs.collapse"):null)&&i._isTransitioning)return}if(!K.trigger(this._element,"show.bs.collapse").defaultPrevented){n&&n.forEach((function(t){o!==t&&e.collapseInterface(t,"hide"),i||k(t,"bs.collapse",null)}));var r=this._getDimension();this._element.classList.remove("collapse"),this._element.classList.add("collapsing"),this._element.style[r]=0,this._triggerArray.length&&this._triggerArray.forEach((function(t){t.classList.remove("collapsed"),t.setAttribute("aria-expanded",!0)})),this.setTransitioning(!0);var a="scroll"+(r[0].toUpperCase()+r.slice(1)),l=f(this._element);K.one(this._element,"transitionend",(function(){t._element.classList.remove("collapsing"),t._element.classList.add("collapse","show"),t._element.style[r]="",t.setTransitioning(!1),K.trigger(t._element,"shown.bs.collapse")})),m(this._element,l),this._element.style[r]=this._element[a]+"px"}}},n.hide=function(){var t=this;if(!this._isTransitioning&&this._element.classList.contains("show")&&!K.trigger(this._element,"hide.bs.collapse").defaultPrevented){var e=this._getDimension();this._element.style[e]=this._element.getBoundingClientRect()[e]+"px",y(this._element),this._element.classList.add("collapsing"),this._element.classList.remove("collapse","show");var n=this._triggerArray.length;if(n>0)for(var i=0;i<n;i++){var o=this._triggerArray[i],s=d(o);s&&!s.classList.contains("show")&&(o.classList.add("collapsed"),o.setAttribute("aria-expanded",!1))}this.setTransitioning(!0),this._element.style[e]="";var r=f(this._element);K.one(this._element,"transitionend",(function(){t.setTransitioning(!1),t._element.classList.remove("collapsing"),t._element.classList.add("collapse"),K.trigger(t._element,"hidden.bs.collapse")})),m(this._element,r)}},n.setTransitioning=function(t){this._isTransitioning=t},n.dispose=function(){t.prototype.dispose.call(this),this._config=null,this._parent=null,this._triggerArray=null,this._isTransitioning=null},n._getConfig=function(t){return(t=s({},tt,t)).toggle=Boolean(t.toggle),_("collapse",t,et),t},n._getDimension=function(){return this._element.classList.contains("width")?"width":"height"},n._getParent=function(){var t=this,e=this._config.parent;return g(e)?void 0===e.jquery&&void 0===e[0]||(e=e[0]):e=Q(e),q('[data-bs-toggle="collapse"][data-bs-parent="'+e+'"]',e).forEach((function(e){var n=d(e);t._addAriaAndCollapsedClass(n,[e])})),e},n._addAriaAndCollapsedClass=function(t,e){if(t&&e.length){var n=t.classList.contains("show");e.forEach((function(t){n?t.classList.remove("collapsed"):t.classList.add("collapsed"),t.setAttribute("aria-expanded",n)}))}},e.collapseInterface=function(t,n){var i=L(t,"bs.collapse"),o=s({},tt,X.getDataAttributes(t),"object"==typeof n&&n?n:{});if(!i&&o.toggle&&"string"==typeof n&&/show|hide/.test(n)&&(o.toggle=!1),i||(i=new e(t,o)),"string"==typeof n){if(void 0===i[n])throw new TypeError('No method named "'+n+'"');i[n]()}},e.jQueryInterface=function(t){return this.each((function(){e.collapseInterface(this,t)}))},o(e,null,[{key:"Default",get:function(){return tt}},{key:"DATA_KEY",get:function(){return"bs.collapse"}}]),e}(W);K.on(document,"click.bs.collapse.data-api",'[data-bs-toggle="collapse"]',(function(t){("A"===t.target.tagName||t.delegateTarget&&"A"===t.delegateTarget.tagName)&&t.preventDefault();var e=X.getDataAttributes(this),n=h(this);q(n).forEach((function(t){var n,i=L(t,"bs.collapse");i?(null===i._parent&&"string"==typeof e.parent&&(i._config.parent=e.parent,i._parent=i._getParent()),n="toggle"):n=e,nt.collapseInterface(t,n)}))})),T("collapse",nt);var it=new RegExp("ArrowUp|ArrowDown|Escape"),ot=E?"top-end":"top-start",st=E?"top-start":"top-end",rt=E?"bottom-end":"bottom-start",at=E?"bottom-start":"bottom-end",lt=E?"left-start":"right-start",ct=E?"right-start":"left-start",ut={offset:[0,2],flip:!0,boundary:"clippingParents",reference:"toggle",display:"dynamic",popperConfig:null},ht={offset:"(array|string|function)",flip:"boolean",boundary:"(string|element)",reference:"(string|element|object)",display:"string",popperConfig:"(null|object|function)"},dt=function(e){function i(t,n){var i;return(i=e.call(this,t)||this)._popper=null,i._config=i._getConfig(n),i._menu=i._getMenuElement(),i._inNavbar=i._detectNavbar(),i._addEventListeners(),i}r(i,e);var a=i.prototype;return a.toggle=function(){if(!this._element.disabled&&!this._element.classList.contains("disabled")){var t=this._element.classList.contains("show");i.clearMenus(),t||this.show()}},a.show=function(){if(!(this._element.disabled||this._element.classList.contains("disabled")||this._menu.classList.contains("show"))){var e=i.getParentFromElement(this._element),o={relatedTarget:this._element};if(!K.trigger(this._element,"show.bs.dropdown",o).defaultPrevented){if(this._inNavbar)X.setDataAttribute(this._menu,"popper","none");else{if(void 0===n)throw new TypeError("Bootstrap's dropdowns require Popper (https://popper.js.org)");var s=this._element;"parent"===this._config.reference?s=e:g(this._config.reference)?(s=this._config.reference,void 0!==this._config.reference.jquery&&(s=this._config.reference[0])):"object"==typeof this._config.reference&&(s=this._config.reference);var r=this._getPopperConfig(),a=r.modifiers.find((function(t){return"applyStyles"===t.name&&!1===t.enabled}));this._popper=t.createPopper(s,this._menu,r),a&&X.setDataAttribute(this._menu,"popper","static")}var l;"ontouchstart"in document.documentElement&&!e.closest(".navbar-nav")&&(l=[]).concat.apply(l,document.body.children).forEach((function(t){return K.on(t,"mouseover",null,(function(){}))})),this._element.focus(),this._element.setAttribute("aria-expanded",!0),this._menu.classList.toggle("show"),this._element.classList.toggle("show"),K.trigger(this._element,"shown.bs.dropdown",o)}}},a.hide=function(){if(!this._element.disabled&&!this._element.classList.contains("disabled")&&this._menu.classList.contains("show")){var t={relatedTarget:this._element};K.trigger(this._element,"hide.bs.dropdown",t).defaultPrevented||(this._popper&&this._popper.destroy(),this._menu.classList.toggle("show"),this._element.classList.toggle("show"),X.removeDataAttribute(this._menu,"popper"),K.trigger(this._element,"hidden.bs.dropdown",t))}},a.dispose=function(){e.prototype.dispose.call(this),K.off(this._element,".bs.dropdown"),this._menu=null,this._popper&&(this._popper.destroy(),this._popper=null)},a.update=function(){this._inNavbar=this._detectNavbar(),this._popper&&this._popper.update()},a._addEventListeners=function(){var t=this;K.on(this._element,"click.bs.dropdown",(function(e){e.preventDefault(),e.stopPropagation(),t.toggle()}))},a._getConfig=function(t){if(t=s({},this.constructor.Default,X.getDataAttributes(this._element),t),_("dropdown",t,this.constructor.DefaultType),"object"==typeof t.reference&&!g(t.reference)&&"function"!=typeof t.reference.getBoundingClientRect)throw new TypeError("dropdown".toUpperCase()+': Option "reference" provided type "object" without a required "getBoundingClientRect" method.');return t},a._getMenuElement=function(){return function(t,e){for(var n=t.nextElementSibling;n;){if(n.matches(e))return[n];n=n.nextElementSibling}return[]}(this._element,".dropdown-menu")[0]},a._getPlacement=function(){var t=this._element.parentNode;if(t.classList.contains("dropend"))return lt;if(t.classList.contains("dropstart"))return ct;var e="end"===getComputedStyle(this._menu).getPropertyValue("--bs-position").trim();return t.classList.contains("dropup")?e?st:ot:e?at:rt},a._detectNavbar=function(){return null!==this._element.closest(".navbar")},a._getOffset=function(){var t=this,e=this._config.offset;return"string"==typeof e?e.split(",").map((function(t){return Number.parseInt(t,10)})):"function"==typeof e?function(n){return e(n,t._element)}:e},a._getPopperConfig=function(){var t={placement:this._getPlacement(),modifiers:[{name:"preventOverflow",options:{altBoundary:this._config.flip,boundary:this._config.boundary}},{name:"offset",options:{offset:this._getOffset()}}]};return"static"===this._config.display&&(t.modifiers=[{name:"applyStyles",enabled:!1}]),s({},t,"function"==typeof this._config.popperConfig?this._config.popperConfig(t):this._config.popperConfig)},i.dropdownInterface=function(t,e){var n=L(t,"bs.dropdown");if(n||(n=new i(t,"object"==typeof e?e:null)),"string"==typeof e){if(void 0===n[e])throw new TypeError('No method named "'+e+'"');n[e]()}},i.jQueryInterface=function(t){return this.each((function(){i.dropdownInterface(this,t)}))},i.clearMenus=function(t){if(!t||2!==t.button&&("keyup"!==t.type||"Tab"===t.key))for(var e=q('[data-bs-toggle="dropdown"]'),n=0,i=e.length;n<i;n++){var o=L(e[n],"bs.dropdown"),s={relatedTarget:e[n]};if(t&&"click"===t.type&&(s.clickEvent=t),o){var r,a=o._menu;if(e[n].classList.contains("show")&&!(t&&("click"===t.type&&/input|textarea/i.test(t.target.tagName)||"keyup"===t.type&&"Tab"===t.key)&&a.contains(t.target)||K.trigger(e[n],"hide.bs.dropdown",s).defaultPrevented))"ontouchstart"in document.documentElement&&(r=[]).concat.apply(r,document.body.children).forEach((function(t){return K.off(t,"mouseover",null,(function(){}))})),e[n].setAttribute("aria-expanded","false"),o._popper&&o._popper.destroy(),a.classList.remove("show"),e[n].classList.remove("show"),X.removeDataAttribute(a,"popper"),K.trigger(e[n],"hidden.bs.dropdown",s)}}},i.getParentFromElement=function(t){return d(t)||t.parentNode},i.dataApiKeydownHandler=function(t){if(!(/input|textarea/i.test(t.target.tagName)?"Space"===t.key||"Escape"!==t.key&&("ArrowDown"!==t.key&&"ArrowUp"!==t.key||t.target.closest(".dropdown-menu")):!it.test(t.key))&&(t.preventDefault(),t.stopPropagation(),!this.disabled&&!this.classList.contains("disabled"))){var e=i.getParentFromElement(this),n=this.classList.contains("show");if("Escape"===t.key)return(this.matches('[data-bs-toggle="dropdown"]')?this:$(this,'[data-bs-toggle="dropdown"]')[0]).focus(),void i.clearMenus();if(n||"ArrowUp"!==t.key&&"ArrowDown"!==t.key)if(n&&"Space"!==t.key){var o=q(".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)",e).filter(v);if(o.length){var s=o.indexOf(t.target);"ArrowUp"===t.key&&s>0&&s--,"ArrowDown"===t.key&&s<o.length-1&&s++,o[s=-1===s?0:s].focus()}}else i.clearMenus();else(this.matches('[data-bs-toggle="dropdown"]')?this:$(this,'[data-bs-toggle="dropdown"]')[0]).click()}},o(i,null,[{key:"Default",get:function(){return ut}},{key:"DefaultType",get:function(){return ht}},{key:"DATA_KEY",get:function(){return"bs.dropdown"}}]),i}(W);K.on(document,"keydown.bs.dropdown.data-api",'[data-bs-toggle="dropdown"]',dt.dataApiKeydownHandler),K.on(document,"keydown.bs.dropdown.data-api",".dropdown-menu",dt.dataApiKeydownHandler),K.on(document,"click.bs.dropdown.data-api",dt.clearMenus),K.on(document,"keyup.bs.dropdown.data-api",dt.clearMenus),K.on(document,"click.bs.dropdown.data-api",'[data-bs-toggle="dropdown"]',(function(t){t.preventDefault(),t.stopPropagation(),dt.dropdownInterface(this,"toggle")})),K.on(document,"click.bs.dropdown.data-api",".dropdown form",(function(t){return t.stopPropagation()})),T("dropdown",dt);var ft={backdrop:!0,keyboard:!0,focus:!0},pt={backdrop:"(boolean|string)",keyboard:"boolean",focus:"boolean"},gt=function(t){function e(e,n){var i;return(i=t.call(this,e)||this)._config=i._getConfig(n),i._dialog=Q(".modal-dialog",e),i._backdrop=null,i._isShown=!1,i._isBodyOverflowing=!1,i._ignoreBackdropClick=!1,i._isTransitioning=!1,i._scrollbarWidth=0,i}r(e,t);var n=e.prototype;return n.toggle=function(t){return this._isShown?this.hide():this.show(t)},n.show=function(t){var e=this;if(!this._isShown&&!this._isTransitioning){this._element.classList.contains("fade")&&(this._isTransitioning=!0);var n=K.trigger(this._element,"show.bs.modal",{relatedTarget:t});this._isShown||n.defaultPrevented||(this._isShown=!0,this._checkScrollbar(),this._setScrollbar(),this._adjustDialog(),this._setEscapeEvent(),this._setResizeEvent(),K.on(this._element,"click.dismiss.bs.modal",'[data-bs-dismiss="modal"]',(function(t){return e.hide(t)})),K.on(this._dialog,"mousedown.dismiss.bs.modal",(function(){K.one(e._element,"mouseup.dismiss.bs.modal",(function(t){t.target===e._element&&(e._ignoreBackdropClick=!0)}))})),this._showBackdrop((function(){return e._showElement(t)})))}},n.hide=function(t){var e=this;if(t&&t.preventDefault(),this._isShown&&!this._isTransitioning&&!K.trigger(this._element,"hide.bs.modal").defaultPrevented){this._isShown=!1;var n=this._element.classList.contains("fade");if(n&&(this._isTransitioning=!0),this._setEscapeEvent(),this._setResizeEvent(),K.off(document,"focusin.bs.modal"),this._element.classList.remove("show"),K.off(this._element,"click.dismiss.bs.modal"),K.off(this._dialog,"mousedown.dismiss.bs.modal"),n){var i=f(this._element);K.one(this._element,"transitionend",(function(t){return e._hideModal(t)})),m(this._element,i)}else this._hideModal()}},n.dispose=function(){[window,this._element,this._dialog].forEach((function(t){return K.off(t,".bs.modal")})),t.prototype.dispose.call(this),K.off(document,"focusin.bs.modal"),this._config=null,this._dialog=null,this._backdrop=null,this._isShown=null,this._isBodyOverflowing=null,this._ignoreBackdropClick=null,this._isTransitioning=null,this._scrollbarWidth=null},n.handleUpdate=function(){this._adjustDialog()},n._getConfig=function(t){return t=s({},ft,t),_("modal",t,pt),t},n._showElement=function(t){var e=this,n=this._element.classList.contains("fade"),i=Q(".modal-body",this._dialog);this._element.parentNode&&this._element.parentNode.nodeType===Node.ELEMENT_NODE||document.body.appendChild(this._element),this._element.style.display="block",this._element.removeAttribute("aria-hidden"),this._element.setAttribute("aria-modal",!0),this._element.setAttribute("role","dialog"),this._element.scrollTop=0,i&&(i.scrollTop=0),n&&y(this._element),this._element.classList.add("show"),this._config.focus&&this._enforceFocus();var o=function(){e._config.focus&&e._element.focus(),e._isTransitioning=!1,K.trigger(e._element,"shown.bs.modal",{relatedTarget:t})};if(n){var s=f(this._dialog);K.one(this._dialog,"transitionend",o),m(this._dialog,s)}else o()},n._enforceFocus=function(){var t=this;K.off(document,"focusin.bs.modal"),K.on(document,"focusin.bs.modal",(function(e){document===e.target||t._element===e.target||t._element.contains(e.target)||t._element.focus()}))},n._setEscapeEvent=function(){var t=this;this._isShown?K.on(this._element,"keydown.dismiss.bs.modal",(function(e){t._config.keyboard&&"Escape"===e.key?(e.preventDefault(),t.hide()):t._config.keyboard||"Escape"!==e.key||t._triggerBackdropTransition()})):K.off(this._element,"keydown.dismiss.bs.modal")},n._setResizeEvent=function(){var t=this;this._isShown?K.on(window,"resize.bs.modal",(function(){return t._adjustDialog()})):K.off(window,"resize.bs.modal")},n._hideModal=function(){var t=this;this._element.style.display="none",this._element.setAttribute("aria-hidden",!0),this._element.removeAttribute("aria-modal"),this._element.removeAttribute("role"),this._isTransitioning=!1,this._showBackdrop((function(){document.body.classList.remove("modal-open"),t._resetAdjustments(),t._resetScrollbar(),K.trigger(t._element,"hidden.bs.modal")}))},n._removeBackdrop=function(){this._backdrop.parentNode.removeChild(this._backdrop),this._backdrop=null},n._showBackdrop=function(t){var e=this,n=this._element.classList.contains("fade")?"fade":"";if(this._isShown&&this._config.backdrop){if(this._backdrop=document.createElement("div"),this._backdrop.className="modal-backdrop",n&&this._backdrop.classList.add(n),document.body.appendChild(this._backdrop),K.on(this._element,"click.dismiss.bs.modal",(function(t){e._ignoreBackdropClick?e._ignoreBackdropClick=!1:t.target===t.currentTarget&&("static"===e._config.backdrop?e._triggerBackdropTransition():e.hide())})),n&&y(this._backdrop),this._backdrop.classList.add("show"),!n)return void t();var i=f(this._backdrop);K.one(this._backdrop,"transitionend",t),m(this._backdrop,i)}else if(!this._isShown&&this._backdrop){this._backdrop.classList.remove("show");var o=function(){e._removeBackdrop(),t()};if(this._element.classList.contains("fade")){var s=f(this._backdrop);K.one(this._backdrop,"transitionend",o),m(this._backdrop,s)}else o()}else t()},n._triggerBackdropTransition=function(){var t=this;if(!K.trigger(this._element,"hidePrevented.bs.modal").defaultPrevented){var e=this._element.scrollHeight>document.documentElement.clientHeight;e||(this._element.style.overflowY="hidden"),this._element.classList.add("modal-static");var n=f(this._dialog);K.off(this._element,"transitionend"),K.one(this._element,"transitionend",(function(){t._element.classList.remove("modal-static"),e||(K.one(t._element,"transitionend",(function(){t._element.style.overflowY=""})),m(t._element,n))})),m(this._element,n),this._element.focus()}},n._adjustDialog=function(){var t=this._element.scrollHeight>document.documentElement.clientHeight;(!this._isBodyOverflowing&&t&&!E||this._isBodyOverflowing&&!t&&E)&&(this._element.style.paddingLeft=this._scrollbarWidth+"px"),(this._isBodyOverflowing&&!t&&!E||!this._isBodyOverflowing&&t&&E)&&(this._element.style.paddingRight=this._scrollbarWidth+"px")},n._resetAdjustments=function(){this._element.style.paddingLeft="",this._element.style.paddingRight=""},n._checkScrollbar=function(){var t=document.body.getBoundingClientRect();this._isBodyOverflowing=Math.round(t.left+t.right)<window.innerWidth,this._scrollbarWidth=this._getScrollbarWidth()},n._setScrollbar=function(){var t=this;this._isBodyOverflowing&&(this._setElementAttributes(".fixed-top, .fixed-bottom, .is-fixed, .sticky-top","paddingRight",(function(e){return e+t._scrollbarWidth})),this._setElementAttributes(".sticky-top","marginRight",(function(e){return e-t._scrollbarWidth})),this._setElementAttributes("body","paddingRight",(function(e){return e+t._scrollbarWidth}))),document.body.classList.add("modal-open")},n._setElementAttributes=function(t,e,n){q(t).forEach((function(t){var i=t.style[e],o=window.getComputedStyle(t)[e];X.setDataAttribute(t,e,i),t.style[e]=n(Number.parseFloat(o))+"px"}))},n._resetScrollbar=function(){this._resetElementAttributes(".fixed-top, .fixed-bottom, .is-fixed, .sticky-top","paddingRight"),this._resetElementAttributes(".sticky-top","marginRight"),this._resetElementAttributes("body","paddingRight")},n._resetElementAttributes=function(t,e){q(t).forEach((function(t){var n=X.getDataAttribute(t,e);void 0===n&&t===document.body?t.style[e]="":(X.removeDataAttribute(t,e),t.style[e]=n)}))},n._getScrollbarWidth=function(){var t=document.createElement("div");t.className="modal-scrollbar-measure",document.body.appendChild(t);var e=t.getBoundingClientRect().width-t.clientWidth;return document.body.removeChild(t),e},e.jQueryInterface=function(t,n){return this.each((function(){var i=L(this,"bs.modal"),o=s({},ft,X.getDataAttributes(this),"object"==typeof t&&t?t:{});if(i||(i=new e(this,o)),"string"==typeof t){if(void 0===i[t])throw new TypeError('No method named "'+t+'"');i[t](n)}}))},o(e,null,[{key:"Default",get:function(){return ft}},{key:"DATA_KEY",get:function(){return"bs.modal"}}]),e}(W);K.on(document,"click.bs.modal.data-api",'[data-bs-toggle="modal"]',(function(t){var e=this,n=d(this);"A"!==this.tagName&&"AREA"!==this.tagName||t.preventDefault(),K.one(n,"show.bs.modal",(function(t){t.defaultPrevented||K.one(n,"hidden.bs.modal",(function(){v(e)&&e.focus()}))}));var i=L(n,"bs.modal");if(!i){var o=s({},X.getDataAttributes(n),X.getDataAttributes(this));i=new gt(n,o)}i.toggle(this)})),T("modal",gt);var mt=new Set(["background","cite","href","itemtype","longdesc","poster","src","xlink:href"]),_t=/^(?:(?:https?|mailto|ftp|tel|file):|[^#&/:?]*(?:[#/?]|$))/gi,vt=/^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[\d+/a-z]+=*$/i;function bt(t,e,n){var i;if(!t.length)return t;if(n&&"function"==typeof n)return n(t);for(var o=(new window.DOMParser).parseFromString(t,"text/html"),s=Object.keys(e),r=(i=[]).concat.apply(i,o.body.querySelectorAll("*")),a=function(t,n){var i,o=r[t],a=o.nodeName.toLowerCase();if(!s.includes(a))return o.parentNode.removeChild(o),"continue";var l=(i=[]).concat.apply(i,o.attributes),c=[].concat(e["*"]||[],e[a]||[]);l.forEach((function(t){(function(t,e){var n=t.nodeName.toLowerCase();if(e.includes(n))return!mt.has(n)||Boolean(_t.test(t.nodeValue)||vt.test(t.nodeValue));for(var i=e.filter((function(t){return t instanceof RegExp})),o=0,s=i.length;o<s;o++)if(i[o].test(n))return!0;return!1})(t,c)||o.removeAttribute(t.nodeName)}))},l=0,c=r.length;l<c;l++)a(l);return o.body.innerHTML}var yt=new RegExp("(^|\\s)bs-tooltip\\S+","g"),wt=new Set(["sanitize","allowList","sanitizeFn"]),Et={animation:"boolean",template:"string",title:"(string|element|function)",trigger:"string",delay:"(number|object)",html:"boolean",selector:"(string|boolean)",placement:"(string|function)",offset:"(array|string|function)",container:"(string|element|boolean)",fallbackPlacements:"array",boundary:"(string|element)",customClass:"(string|function)",sanitize:"boolean",sanitizeFn:"(null|function)",allowList:"object",popperConfig:"(null|object|function)"},Tt={AUTO:"auto",TOP:"top",RIGHT:E?"left":"right",BOTTOM:"bottom",LEFT:E?"right":"left"},At={animation:!0,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,selector:!1,placement:"top",offset:[0,0],container:!1,fallbackPlacements:["top","right","bottom","left"],boundary:"clippingParents",customClass:"",sanitize:!0,sanitizeFn:null,allowList:{"*":["class","dir","id","lang","role",/^aria-[\w-]*$/i],a:["target","href","title","rel"],area:[],b:[],br:[],col:[],code:[],div:[],em:[],hr:[],h1:[],h2:[],h3:[],h4:[],h5:[],h6:[],i:[],img:["src","srcset","alt","title","width","height"],li:[],ol:[],p:[],pre:[],s:[],small:[],span:[],sub:[],sup:[],strong:[],u:[],ul:[]},popperConfig:null},kt={HIDE:"hide.bs.tooltip",HIDDEN:"hidden.bs.tooltip",SHOW:"show.bs.tooltip",SHOWN:"shown.bs.tooltip",INSERTED:"inserted.bs.tooltip",CLICK:"click.bs.tooltip",FOCUSIN:"focusin.bs.tooltip",FOCUSOUT:"focusout.bs.tooltip",MOUSEENTER:"mouseenter.bs.tooltip",MOUSELEAVE:"mouseleave.bs.tooltip"},Lt=function(e){function i(t,i){var o;if(void 0===n)throw new TypeError("Bootstrap's tooltips require Popper (https://popper.js.org)");return(o=e.call(this,t)||this)._isEnabled=!0,o._timeout=0,o._hoverState="",o._activeTrigger={},o._popper=null,o.config=o._getConfig(i),o.tip=null,o._setListeners(),o}r(i,e);var a=i.prototype;return a.enable=function(){this._isEnabled=!0},a.disable=function(){this._isEnabled=!1},a.toggleEnabled=function(){this._isEnabled=!this._isEnabled},a.toggle=function(t){if(this._isEnabled)if(t){var e=this._initializeOnDelegatedTarget(t);e._activeTrigger.click=!e._activeTrigger.click,e._isWithActiveTrigger()?e._enter(null,e):e._leave(null,e)}else{if(this.getTipElement().classList.contains("show"))return void this._leave(null,this);this._enter(null,this)}},a.dispose=function(){clearTimeout(this._timeout),K.off(this._element,this.constructor.EVENT_KEY),K.off(this._element.closest(".modal"),"hide.bs.modal",this._hideModalHandler),this.tip&&this.tip.parentNode&&this.tip.parentNode.removeChild(this.tip),this._isEnabled=null,this._timeout=null,this._hoverState=null,this._activeTrigger=null,this._popper&&this._popper.destroy(),this._popper=null,this.config=null,this.tip=null,e.prototype.dispose.call(this)},a.show=function(){var e=this;if("none"===this._element.style.display)throw new Error("Please use show on visible elements");if(this.isWithContent()&&this._isEnabled){var n=K.trigger(this._element,this.constructor.Event.SHOW),i=function t(e){if(!document.documentElement.attachShadow)return null;if("function"==typeof e.getRootNode){var n=e.getRootNode();return n instanceof ShadowRoot?n:null}return e instanceof ShadowRoot?e:e.parentNode?t(e.parentNode):null}(this._element),o=null===i?this._element.ownerDocument.documentElement.contains(this._element):i.contains(this._element);if(!n.defaultPrevented&&o){var s=this.getTipElement(),r=c(this.constructor.NAME);s.setAttribute("id",r),this._element.setAttribute("aria-describedby",r),this.setContent(),this.config.animation&&s.classList.add("fade");var a="function"==typeof this.config.placement?this.config.placement.call(this,s,this._element):this.config.placement,l=this._getAttachment(a);this._addAttachmentClass(l);var u=this._getContainer();k(s,this.constructor.DATA_KEY,this),this._element.ownerDocument.documentElement.contains(this.tip)||u.appendChild(s),K.trigger(this._element,this.constructor.Event.INSERTED),this._popper=t.createPopper(this._element,s,this._getPopperConfig(l)),s.classList.add("show");var h,d,p="function"==typeof this.config.customClass?this.config.customClass():this.config.customClass;p&&(h=s.classList).add.apply(h,p.split(" ")),"ontouchstart"in document.documentElement&&(d=[]).concat.apply(d,document.body.children).forEach((function(t){K.on(t,"mouseover",(function(){}))}));var g=function(){var t=e._hoverState;e._hoverState=null,K.trigger(e._element,e.constructor.Event.SHOWN),"out"===t&&e._leave(null,e)};if(this.tip.classList.contains("fade")){var _=f(this.tip);K.one(this.tip,"transitionend",g),m(this.tip,_)}else g()}}},a.hide=function(){var t=this;if(this._popper){var e=this.getTipElement(),n=function(){"show"!==t._hoverState&&e.parentNode&&e.parentNode.removeChild(e),t._cleanTipClass(),t._element.removeAttribute("aria-describedby"),K.trigger(t._element,t.constructor.Event.HIDDEN),t._popper&&(t._popper.destroy(),t._popper=null)};if(!K.trigger(this._element,this.constructor.Event.HIDE).defaultPrevented){var i;if(e.classList.remove("show"),"ontouchstart"in document.documentElement&&(i=[]).concat.apply(i,document.body.children).forEach((function(t){return K.off(t,"mouseover",b)})),this._activeTrigger.click=!1,this._activeTrigger.focus=!1,this._activeTrigger.hover=!1,this.tip.classList.contains("fade")){var o=f(e);K.one(e,"transitionend",n),m(e,o)}else n();this._hoverState=""}}},a.update=function(){null!==this._popper&&this._popper.update()},a.isWithContent=function(){return Boolean(this.getTitle())},a.getTipElement=function(){if(this.tip)return this.tip;var t=document.createElement("div");return t.innerHTML=this.config.template,this.tip=t.children[0],this.tip},a.setContent=function(){var t=this.getTipElement();this.setElementContent(Q(".tooltip-inner",t),this.getTitle()),t.classList.remove("fade","show")},a.setElementContent=function(t,e){if(null!==t)return"object"==typeof e&&g(e)?(e.jquery&&(e=e[0]),void(this.config.html?e.parentNode!==t&&(t.innerHTML="",t.appendChild(e)):t.textContent=e.textContent)):void(this.config.html?(this.config.sanitize&&(e=bt(e,this.config.allowList,this.config.sanitizeFn)),t.innerHTML=e):t.textContent=e)},a.getTitle=function(){var t=this._element.getAttribute("data-bs-original-title");return t||(t="function"==typeof this.config.title?this.config.title.call(this._element):this.config.title),t},a.updateAttachment=function(t){return"right"===t?"end":"left"===t?"start":t},a._initializeOnDelegatedTarget=function(t,e){var n=this.constructor.DATA_KEY;return(e=e||L(t.delegateTarget,n))||(e=new this.constructor(t.delegateTarget,this._getDelegateConfig()),k(t.delegateTarget,n,e)),e},a._getOffset=function(){var t=this,e=this.config.offset;return"string"==typeof e?e.split(",").map((function(t){return Number.parseInt(t,10)})):"function"==typeof e?function(n){return e(n,t._element)}:e},a._getPopperConfig=function(t){var e=this,n={placement:t,modifiers:[{name:"flip",options:{altBoundary:!0,fallbackPlacements:this.config.fallbackPlacements}},{name:"offset",options:{offset:this._getOffset()}},{name:"preventOverflow",options:{boundary:this.config.boundary}},{name:"arrow",options:{element:"."+this.constructor.NAME+"-arrow"}},{name:"onChange",enabled:!0,phase:"afterWrite",fn:function(t){return e._handlePopperPlacementChange(t)}}],onFirstUpdate:function(t){t.options.placement!==t.placement&&e._handlePopperPlacementChange(t)}};return s({},n,"function"==typeof this.config.popperConfig?this.config.popperConfig(n):this.config.popperConfig)},a._addAttachmentClass=function(t){this.getTipElement().classList.add("bs-tooltip-"+this.updateAttachment(t))},a._getContainer=function(){return!1===this.config.container?document.body:g(this.config.container)?this.config.container:Q(this.config.container)},a._getAttachment=function(t){return Tt[t.toUpperCase()]},a._setListeners=function(){var t=this;this.config.trigger.split(" ").forEach((function(e){if("click"===e)K.on(t._element,t.constructor.Event.CLICK,t.config.selector,(function(e){return t.toggle(e)}));else if("manual"!==e){var n="hover"===e?t.constructor.Event.MOUSEENTER:t.constructor.Event.FOCUSIN,i="hover"===e?t.constructor.Event.MOUSELEAVE:t.constructor.Event.FOCUSOUT;K.on(t._element,n,t.config.selector,(function(e){return t._enter(e)})),K.on(t._element,i,t.config.selector,(function(e){return t._leave(e)}))}})),this._hideModalHandler=function(){t._element&&t.hide()},K.on(this._element.closest(".modal"),"hide.bs.modal",this._hideModalHandler),this.config.selector?this.config=s({},this.config,{trigger:"manual",selector:""}):this._fixTitle()},a._fixTitle=function(){var t=this._element.getAttribute("title"),e=typeof this._element.getAttribute("data-bs-original-title");(t||"string"!==e)&&(this._element.setAttribute("data-bs-original-title",t||""),!t||this._element.getAttribute("aria-label")||this._element.textContent||this._element.setAttribute("aria-label",t),this._element.setAttribute("title",""))},a._enter=function(t,e){e=this._initializeOnDelegatedTarget(t,e),t&&(e._activeTrigger["focusin"===t.type?"focus":"hover"]=!0),e.getTipElement().classList.contains("show")||"show"===e._hoverState?e._hoverState="show":(clearTimeout(e._timeout),e._hoverState="show",e.config.delay&&e.config.delay.show?e._timeout=setTimeout((function(){"show"===e._hoverState&&e.show()}),e.config.delay.show):e.show())},a._leave=function(t,e){e=this._initializeOnDelegatedTarget(t,e),t&&(e._activeTrigger["focusout"===t.type?"focus":"hover"]=!1),e._isWithActiveTrigger()||(clearTimeout(e._timeout),e._hoverState="out",e.config.delay&&e.config.delay.hide?e._timeout=setTimeout((function(){"out"===e._hoverState&&e.hide()}),e.config.delay.hide):e.hide())},a._isWithActiveTrigger=function(){for(var t in this._activeTrigger)if(this._activeTrigger[t])return!0;return!1},a._getConfig=function(t){var e=X.getDataAttributes(this._element);return Object.keys(e).forEach((function(t){wt.has(t)&&delete e[t]})),t&&"object"==typeof t.container&&t.container.jquery&&(t.container=t.container[0]),"number"==typeof(t=s({},this.constructor.Default,e,"object"==typeof t&&t?t:{})).delay&&(t.delay={show:t.delay,hide:t.delay}),"number"==typeof t.title&&(t.title=t.title.toString()),"number"==typeof t.content&&(t.content=t.content.toString()),_("tooltip",t,this.constructor.DefaultType),t.sanitize&&(t.template=bt(t.template,t.allowList,t.sanitizeFn)),t},a._getDelegateConfig=function(){var t={};if(this.config)for(var e in this.config)this.constructor.Default[e]!==this.config[e]&&(t[e]=this.config[e]);return t},a._cleanTipClass=function(){var t=this.getTipElement(),e=t.getAttribute("class").match(yt);null!==e&&e.length>0&&e.map((function(t){return t.trim()})).forEach((function(e){return t.classList.remove(e)}))},a._handlePopperPlacementChange=function(t){var e=t.state;e&&(this.tip=e.elements.popper,this._cleanTipClass(),this._addAttachmentClass(this._getAttachment(e.placement)))},i.jQueryInterface=function(t){return this.each((function(){var e=L(this,"bs.tooltip"),n="object"==typeof t&&t;if((e||!/dispose|hide/.test(t))&&(e||(e=new i(this,n)),"string"==typeof t)){if(void 0===e[t])throw new TypeError('No method named "'+t+'"');e[t]()}}))},o(i,null,[{key:"Default",get:function(){return At}},{key:"NAME",get:function(){return"tooltip"}},{key:"DATA_KEY",get:function(){return"bs.tooltip"}},{key:"Event",get:function(){return kt}},{key:"EVENT_KEY",get:function(){return".bs.tooltip"}},{key:"DefaultType",get:function(){return Et}}]),i}(W);T("tooltip",Lt);var Ct=new RegExp("(^|\\s)bs-popover\\S+","g"),Dt=s({},Lt.Default,{placement:"right",offset:[0,8],trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="popover-arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'}),St=s({},Lt.DefaultType,{content:"(string|element|function)"}),Nt={HIDE:"hide.bs.popover",HIDDEN:"hidden.bs.popover",SHOW:"show.bs.popover",SHOWN:"shown.bs.popover",INSERTED:"inserted.bs.popover",CLICK:"click.bs.popover",FOCUSIN:"focusin.bs.popover",FOCUSOUT:"focusout.bs.popover",MOUSEENTER:"mouseenter.bs.popover",MOUSELEAVE:"mouseleave.bs.popover"},Ot=function(t){function e(){return t.apply(this,arguments)||this}r(e,t);var n=e.prototype;return n.isWithContent=function(){return this.getTitle()||this._getContent()},n.setContent=function(){var t=this.getTipElement();this.setElementContent(Q(".popover-header",t),this.getTitle());var e=this._getContent();"function"==typeof e&&(e=e.call(this._element)),this.setElementContent(Q(".popover-body",t),e),t.classList.remove("fade","show")},n._addAttachmentClass=function(t){this.getTipElement().classList.add("bs-popover-"+this.updateAttachment(t))},n._getContent=function(){return this._element.getAttribute("data-bs-content")||this.config.content},n._cleanTipClass=function(){var t=this.getTipElement(),e=t.getAttribute("class").match(Ct);null!==e&&e.length>0&&e.map((function(t){return t.trim()})).forEach((function(e){return t.classList.remove(e)}))},e.jQueryInterface=function(t){return this.each((function(){var n=L(this,"bs.popover"),i="object"==typeof t?t:null;if((n||!/dispose|hide/.test(t))&&(n||(n=new e(this,i),k(this,"bs.popover",n)),"string"==typeof t)){if(void 0===n[t])throw new TypeError('No method named "'+t+'"');n[t]()}}))},o(e,null,[{key:"Default",get:function(){return Dt}},{key:"NAME",get:function(){return"popover"}},{key:"DATA_KEY",get:function(){return"bs.popover"}},{key:"Event",get:function(){return Nt}},{key:"EVENT_KEY",get:function(){return".bs.popover"}},{key:"DefaultType",get:function(){return St}}]),e}(Lt);T("popover",Ot);var It={offset:10,method:"auto",target:""},jt={offset:"number",method:"string",target:"(string|element)"},Pt=function(t){function e(e,n){var i;return(i=t.call(this,e)||this)._scrollElement="BODY"===e.tagName?window:e,i._config=i._getConfig(n),i._selector=i._config.target+" .nav-link, "+i._config.target+" .list-group-item, "+i._config.target+" .dropdown-item",i._offsets=[],i._targets=[],i._activeTarget=null,i._scrollHeight=0,K.on(i._scrollElement,"scroll.bs.scrollspy",(function(){return i._process()})),i.refresh(),i._process(),i}r(e,t);var n=e.prototype;return n.refresh=function(){var t=this,e=this._scrollElement===this._scrollElement.window?"offset":"position",n="auto"===this._config.method?e:this._config.method,i="position"===n?this._getScrollTop():0;this._offsets=[],this._targets=[],this._scrollHeight=this._getScrollHeight(),q(this._selector).map((function(t){var e=h(t),o=e?Q(e):null;if(o){var s=o.getBoundingClientRect();if(s.width||s.height)return[X[n](o).top+i,e]}return null})).filter((function(t){return t})).sort((function(t,e){return t[0]-e[0]})).forEach((function(e){t._offsets.push(e[0]),t._targets.push(e[1])}))},n.dispose=function(){t.prototype.dispose.call(this),K.off(this._scrollElement,".bs.scrollspy"),this._scrollElement=null,this._config=null,this._selector=null,this._offsets=null,this._targets=null,this._activeTarget=null,this._scrollHeight=null},n._getConfig=function(t){if("string"!=typeof(t=s({},It,"object"==typeof t&&t?t:{})).target&&g(t.target)){var e=t.target.id;e||(e=c("scrollspy"),t.target.id=e),t.target="#"+e}return _("scrollspy",t,jt),t},n._getScrollTop=function(){return this._scrollElement===window?this._scrollElement.pageYOffset:this._scrollElement.scrollTop},n._getScrollHeight=function(){return this._scrollElement.scrollHeight||Math.max(document.body.scrollHeight,document.documentElement.scrollHeight)},n._getOffsetHeight=function(){return this._scrollElement===window?window.innerHeight:this._scrollElement.getBoundingClientRect().height},n._process=function(){var t=this._getScrollTop()+this._config.offset,e=this._getScrollHeight(),n=this._config.offset+e-this._getOffsetHeight();if(this._scrollHeight!==e&&this.refresh(),t>=n){var i=this._targets[this._targets.length-1];this._activeTarget!==i&&this._activate(i)}else{if(this._activeTarget&&t<this._offsets[0]&&this._offsets[0]>0)return this._activeTarget=null,void this._clear();for(var o=this._offsets.length;o--;)this._activeTarget!==this._targets[o]&&t>=this._offsets[o]&&(void 0===this._offsets[o+1]||t<this._offsets[o+1])&&this._activate(this._targets[o])}},n._activate=function(t){this._activeTarget=t,this._clear();var e=this._selector.split(",").map((function(e){return e+'[data-bs-target="'+t+'"],'+e+'[href="'+t+'"]'})),n=Q(e.join(","));n.classList.contains("dropdown-item")?(Q(".dropdown-toggle",n.closest(".dropdown")).classList.add("active"),n.classList.add("active")):(n.classList.add("active"),function(t,e){for(var n=[],i=t.parentNode;i&&i.nodeType===Node.ELEMENT_NODE&&3!==i.nodeType;)i.matches(e)&&n.push(i),i=i.parentNode;return n}(n,".nav, .list-group").forEach((function(t){$(t,".nav-link, .list-group-item").forEach((function(t){return t.classList.add("active")})),$(t,".nav-item").forEach((function(t){V(t,".nav-link").forEach((function(t){return t.classList.add("active")}))}))}))),K.trigger(this._scrollElement,"activate.bs.scrollspy",{relatedTarget:t})},n._clear=function(){q(this._selector).filter((function(t){return t.classList.contains("active")})).forEach((function(t){return t.classList.remove("active")}))},e.jQueryInterface=function(t){return this.each((function(){var n=L(this,"bs.scrollspy");if(n||(n=new e(this,"object"==typeof t&&t)),"string"==typeof t){if(void 0===n[t])throw new TypeError('No method named "'+t+'"');n[t]()}}))},o(e,null,[{key:"Default",get:function(){return It}},{key:"DATA_KEY",get:function(){return"bs.scrollspy"}}]),e}(W);K.on(window,"load.bs.scrollspy.data-api",(function(){q('[data-bs-spy="scroll"]').forEach((function(t){return new Pt(t,X.getDataAttributes(t))}))})),T("scrollspy",Pt);var xt=function(t){function e(){return t.apply(this,arguments)||this}r(e,t);var n=e.prototype;return n.show=function(){var t=this;if(!(this._element.parentNode&&this._element.parentNode.nodeType===Node.ELEMENT_NODE&&this._element.classList.contains("active")||this._element.classList.contains("disabled"))){var e,n=d(this._element),i=this._element.closest(".nav, .list-group");if(i){var o="UL"===i.nodeName||"OL"===i.nodeName?":scope > li > .active":".active";e=(e=q(o,i))[e.length-1]}var s=e?K.trigger(e,"hide.bs.tab",{relatedTarget:this._element}):null;if(!(K.trigger(this._element,"show.bs.tab",{relatedTarget:e}).defaultPrevented||null!==s&&s.defaultPrevented)){this._activate(this._element,i);var r=function(){K.trigger(e,"hidden.bs.tab",{relatedTarget:t._element}),K.trigger(t._element,"shown.bs.tab",{relatedTarget:e})};n?this._activate(n,n.parentNode,r):r()}}},n._activate=function(t,e,n){var i=this,o=(!e||"UL"!==e.nodeName&&"OL"!==e.nodeName?V(e,".active"):q(":scope > li > .active",e))[0],s=n&&o&&o.classList.contains("fade"),r=function(){return i._transitionComplete(t,o,n)};if(o&&s){var a=f(o);o.classList.remove("show"),K.one(o,"transitionend",r),m(o,a)}else r()},n._transitionComplete=function(t,e,n){if(e){e.classList.remove("active");var i=Q(":scope > .dropdown-menu .active",e.parentNode);i&&i.classList.remove("active"),"tab"===e.getAttribute("role")&&e.setAttribute("aria-selected",!1)}t.classList.add("active"),"tab"===t.getAttribute("role")&&t.setAttribute("aria-selected",!0),y(t),t.classList.contains("fade")&&t.classList.add("show"),t.parentNode&&t.parentNode.classList.contains("dropdown-menu")&&(t.closest(".dropdown")&&q(".dropdown-toggle").forEach((function(t){return t.classList.add("active")})),t.setAttribute("aria-expanded",!0)),n&&n()},e.jQueryInterface=function(t){return this.each((function(){var n=L(this,"bs.tab")||new e(this);if("string"==typeof t){if(void 0===n[t])throw new TypeError('No method named "'+t+'"');n[t]()}}))},o(e,null,[{key:"DATA_KEY",get:function(){return"bs.tab"}}]),e}(W);K.on(document,"click.bs.tab.data-api",'[data-bs-toggle="tab"], [data-bs-toggle="pill"], [data-bs-toggle="list"]',(function(t){t.preventDefault(),(L(this,"bs.tab")||new xt(this)).show()})),T("tab",xt);var Ht={animation:"boolean",autohide:"boolean",delay:"number"},Bt={animation:!0,autohide:!0,delay:5e3},Mt=function(t){function e(e,n){var i;return(i=t.call(this,e)||this)._config=i._getConfig(n),i._timeout=null,i._setListeners(),i}r(e,t);var n=e.prototype;return n.show=function(){var t=this;if(!K.trigger(this._element,"show.bs.toast").defaultPrevented){this._clearTimeout(),this._config.animation&&this._element.classList.add("fade");var e=function(){t._element.classList.remove("showing"),t._element.classList.add("show"),K.trigger(t._element,"shown.bs.toast"),t._config.autohide&&(t._timeout=setTimeout((function(){t.hide()}),t._config.delay))};if(this._element.classList.remove("hide"),y(this._element),this._element.classList.add("showing"),this._config.animation){var n=f(this._element);K.one(this._element,"transitionend",e),m(this._element,n)}else e()}},n.hide=function(){var t=this;if(this._element.classList.contains("show")&&!K.trigger(this._element,"hide.bs.toast").defaultPrevented){var e=function(){t._element.classList.add("hide"),K.trigger(t._element,"hidden.bs.toast")};if(this._element.classList.remove("show"),this._config.animation){var n=f(this._element);K.one(this._element,"transitionend",e),m(this._element,n)}else e()}},n.dispose=function(){this._clearTimeout(),this._element.classList.contains("show")&&this._element.classList.remove("show"),K.off(this._element,"click.dismiss.bs.toast"),t.prototype.dispose.call(this),this._config=null},n._getConfig=function(t){return t=s({},Bt,X.getDataAttributes(this._element),"object"==typeof t&&t?t:{}),_("toast",t,this.constructor.DefaultType),t},n._setListeners=function(){var t=this;K.on(this._element,"click.dismiss.bs.toast",'[data-bs-dismiss="toast"]',(function(){return t.hide()}))},n._clearTimeout=function(){clearTimeout(this._timeout),this._timeout=null},e.jQueryInterface=function(t){return this.each((function(){var n=L(this,"bs.toast");if(n||(n=new e(this,"object"==typeof t&&t)),"string"==typeof t){if(void 0===n[t])throw new TypeError('No method named "'+t+'"');n[t](this)}}))},o(e,null,[{key:"DefaultType",get:function(){return Ht}},{key:"Default",get:function(){return Bt}},{key:"DATA_KEY",get:function(){return"bs.toast"}}]),e}(W);return T("toast",Mt),{Alert:U,Button:F,Carousel:J,Collapse:nt,Dropdown:dt,Modal:gt,Popover:Ot,ScrollSpy:Pt,Tab:xt,Toast:Mt,Tooltip:Lt}}));
    //# sourceMappingURL=bootstrap.min.js.map
   
   
   
</script>

<style>
    @charset "UTF-8";

/*!
 * Bootstrap v5.0.0-beta2 (https://getbootstrap.com/)
 * Copyright 2011-2021 The Bootstrap Authors
 * Copyright 2011-2021 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/main/LICENSE)
 */
:root {
    --bs-blue: #fd0d0d;
    --bs-indigo: #6610f2;
    --bs-purple: #6f42c1;
    --bs-pink: #d63384;
    --bs-red: #dc3545;
    --bs-orange: #fd7e14;
    --bs-yellow: #ffc107;
    --bs-green: #198754;
    --bs-teal: #20c997;
    --bs-cyan: #0dcaf0;
    --bs-white: #fff;
    --bs-gray: #6c757d;
    --bs-gray-dark: #343a40;
    --bs-primary: #900909;
    --bs-secondary: #6c757d;
    --bs-success: #198754;
    --bs-info: #0dcaf0;
    --bs-warning: #ffc107;
    --bs-danger: #dc3545;
    --bs-light: #f8f9fa;
    --bs-dark: #212529;
    --bs-font-sans-serif: system-ui, -apple-system, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
    --bs-font-monospace: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
    --bs-gradient: linear-gradient(180deg, rgba(255, 255, 255, 0.15), rgba(255, 255, 255, 0))
}

*,
::after,
::before {
    box-sizing: border-box
}

@media (prefers-reduced-motion:no-preference) {
    :root {
        scroll-behavior: smooth
    }
}

body {
    margin: 0;
    font-family: var(--bs-font-sans-serif);
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    background-color: #fff;
    -webkit-text-size-adjust: 100%;
    -webkit-tap-highlight-color: transparent
}

[tabindex="-1"]:focus:not(:focus-visible) {
    outline: 0 !important
}

hr {
    margin: 1rem 0;
    color: inherit;
    background-color: currentColor;
    border: 0;
    opacity: .25
}

hr:not([size]) {
    height: 1px
}

.h1,
.h2,
.h3,
.h4,
.h5,
.h6,
h1,
h2,
h3,
h4,
h5,
h6 {
    margin-top: 0;
    margin-bottom: .5rem;
    font-weight: 500;
    line-height: 1.2
}

.h1,
h1 {
    font-size: calc(1.375rem + 1.5vw)
}

@media (min-width:1200px) {

    .h1,
    h1 {
        font-size: 2.5rem
    }
}

.h2,
h2 {
    font-size: calc(1.325rem + .9vw)
}

@media (min-width:1200px) {

    .h2,
    h2 {
        font-size: 2rem
    }
}

.h3,
h3 {
    font-size: calc(1.3rem + .6vw)
}

@media (min-width:1200px) {

    .h3,
    h3 {
        font-size: 1.75rem
    }
}

.h4,
h4 {
    font-size: calc(1.275rem + .3vw)
}

@media (min-width:1200px) {

    .h4,
    h4 {
        font-size: 1.5rem
    }
}

.h5,
h5 {
    font-size: 1.25rem
}

.h6,
h6 {
    font-size: 1rem
}

p {
    margin-top: 0;
    margin-bottom: 1rem
}

abbr[data-bs-original-title],
abbr[title] {
    text-decoration: underline;
    -webkit-text-decoration: underline dotted;
    text-decoration: underline dotted;
    cursor: help;
    -webkit-text-decoration-skip-ink: none;
    text-decoration-skip-ink: none
}

address {
    margin-bottom: 1rem;
    font-style: normal;
    line-height: inherit
}

ol,
ul {
    padding-left: 2rem
}

dl,
ol,
ul {
    margin-top: 0;
    margin-bottom: 1rem
}

ol ol,
ol ul,
ul ol,
ul ul {
    margin-bottom: 0
}

dt {
    font-weight: 700
}

dd {
    margin-bottom: .5rem;
    margin-left: 0
}

blockquote {
    margin: 0 0 1rem
}

b,
strong {
    font-weight: bolder
}

.small,
small {
    font-size: .875em
}

.mark,
mark {
    padding: .2em;
    background-color: #fcf8e3
}

sub,
sup {
    position: relative;
    font-size: .75em;
    line-height: 0;
    vertical-align: baseline
}

sub {
    bottom: -.25em
}

sup {
    top: -.5em
}

a {
    color: #fd0d0d;
    text-decoration: underline
}

a:hover {
    color: #0a58ca
}

a:not([href]):not([class]),
a:not([href]):not([class]):hover {
    color: inherit;
    text-decoration: none
}

code,
kbd,
pre,
samp {
    font-family: var(--bs-font-monospace);
    font-size: 1em;
    direction: ltr;
    unicode-bidi: bidi-override
}

pre {
    display: block;
    margin-top: 0;
    margin-bottom: 1rem;
    overflow: auto;
    font-size: .875em
}

pre code {
    font-size: inherit;
    color: inherit;
    word-break: normal
}

code {
    font-size: .875em;
    color: #d63384;
    word-wrap: break-word
}

a>code {
    color: inherit
}

kbd {
    padding: .2rem .4rem;
    font-size: .875em;
    color: #fff;
    background-color: #212529;
    border-radius: .2rem
}

kbd kbd {
    padding: 0;
    font-size: 1em;
    font-weight: 700
}

figure {
    margin: 0 0 1rem
}

img,
svg {
    vertical-align: middle
}

table {
    caption-side: bottom;
    border-collapse: collapse
}

caption {
    padding-top: .5rem;
    padding-bottom: .5rem;
    color: #6c757d;
    text-align: left
}

th {
    text-align: inherit;
    text-align: -webkit-match-parent
}

tbody,
td,
tfoot,
th,
thead,
tr {
    border-color: inherit;
    border-style: solid;
    border-width: 0
}

label {
    display: inline-block
}

button {
    border-radius: 0
}

button:focus:not(:focus-visible) {
    outline: 0
}

button,
input,
optgroup,
select,
textarea {
    margin: 0;
    font-family: inherit;
    font-size: inherit;
    line-height: inherit
}

button,
select {
    text-transform: none
}

[role=button] {
    cursor: pointer
}

select {
    word-wrap: normal
}

[list]::-webkit-calendar-picker-indicator {
    display: none
}

[type=button],
[type=reset],
[type=submit],
button {
    -webkit-appearance: button
}

[type=button]:not(:disabled),
[type=reset]:not(:disabled),
[type=submit]:not(:disabled),
button:not(:disabled) {
    cursor: pointer
}

::-moz-focus-inner {
    padding: 0;
    border-style: none
}

textarea {
    resize: vertical
}

fieldset {
    min-width: 0;
    padding: 0;
    margin: 0;
    border: 0
}

legend {
    float: left;
    width: 100%;
    padding: 0;
    margin-bottom: .5rem;
    font-size: calc(1.275rem + .3vw);
    line-height: inherit
}

@media (min-width:1200px) {
    legend {
        font-size: 1.5rem
    }
}

legend+* {
    clear: left
}

::-webkit-datetime-edit-day-field,
::-webkit-datetime-edit-fields-wrapper,
::-webkit-datetime-edit-hour-field,
::-webkit-datetime-edit-minute,
::-webkit-datetime-edit-month-field,
::-webkit-datetime-edit-text,
::-webkit-datetime-edit-year-field {
    padding: 0
}

::-webkit-inner-spin-button {
    height: auto
}

[type=search] {
    outline-offset: -2px;
    -webkit-appearance: textfield
}

::-webkit-search-decoration {
    -webkit-appearance: none
}

::-webkit-color-swatch-wrapper {
    padding: 0
}

::file-selector-button {
    font: inherit
}

::-webkit-file-upload-button {
    font: inherit;
    -webkit-appearance: button
}

output {
    display: inline-block
}

iframe {
    border: 0
}

summary {
    display: list-item;
    cursor: pointer
}

progress {
    vertical-align: baseline
}

[hidden] {
    display: none !important
}

.lead {
    font-size: 1.25rem;
    font-weight: 300
}

.display-1 {
    font-size: calc(1.625rem + 4.5vw);
    font-weight: 300;
    line-height: 1.2
}

@media (min-width:1200px) {
    .display-1 {
        font-size: 5rem
    }
}

.display-2 {
    font-size: calc(1.575rem + 3.9vw);
    font-weight: 300;
    line-height: 1.2
}

@media (min-width:1200px) {
    .display-2 {
        font-size: 4.5rem
    }
}

.display-3 {
    font-size: calc(1.525rem + 3.3vw);
    font-weight: 300;
    line-height: 1.2
}

@media (min-width:1200px) {
    .display-3 {
        font-size: 4rem
    }
}

.display-4 {
    font-size: calc(1.475rem + 2.7vw);
    font-weight: 300;
    line-height: 1.2
}

@media (min-width:1200px) {
    .display-4 {
        font-size: 3.5rem
    }
}

.display-5 {
    font-size: calc(1.425rem + 2.1vw);
    font-weight: 300;
    line-height: 1.2
}

@media (min-width:1200px) {
    .display-5 {
        font-size: 3rem
    }
}

.display-6 {
    font-size: calc(1.375rem + 1.5vw);
    font-weight: 300;
    line-height: 1.2
}

@media (min-width:1200px) {
    .display-6 {
        font-size: 2.5rem
    }
}

.list-unstyled {
    padding-left: 0;
    list-style: none
}

.list-inline {
    padding-left: 0;
    list-style: none
}

.list-inline-item {
    display: inline-block
}

.list-inline-item:not(:last-child) {
    margin-right: .5rem
}

.initialism {
    font-size: .875em;
    text-transform: uppercase
}

.blockquote {
    margin-bottom: 1rem;
    font-size: 1.25rem
}

.blockquote>:last-child {
    margin-bottom: 0
}

.blockquote-footer {
    margin-top: -1rem;
    margin-bottom: 1rem;
    font-size: .875em;
    color: #6c757d
}

.blockquote-footer::before {
    content: "— "
}

.img-fluid {
    max-width: 100%;
    height: auto
}

.img-thumbnail {
    padding: .25rem;
    background-color: #fff;
    border: 1px solid #dee2e6;
    border-radius: .25rem;
    max-width: 100%;
    height: auto
}

.figure {
    display: inline-block
}

.figure-img {
    margin-bottom: .5rem;
    line-height: 1
}

.figure-caption {
    font-size: .875em;
    color: #6c757d
}

.container,
.container-fluid,
.container-lg,
.container-md,
.container-sm,
.container-xl,
.container-xxl {
    width: 100%;
    padding-right: var(--bs-gutter-x, .75rem);
    padding-left: var(--bs-gutter-x, .75rem);
    margin-right: auto;
    margin-left: auto
}

@media (min-width:576px) {

    .container,
    .container-sm {
        max-width: 540px
    }
}

@media (min-width:768px) {

    .container,
    .container-md,
    .container-sm {
        max-width: 720px
    }
}

@media (min-width:992px) {

    .container,
    .container-lg,
    .container-md,
    .container-sm {
        max-width: 960px
    }
}

@media (min-width:1200px) {

    .container,
    .container-lg,
    .container-md,
    .container-sm,
    .container-xl {
        max-width: 1140px
    }
}

@media (min-width:1400px) {

    .container,
    .container-lg,
    .container-md,
    .container-sm,
    .container-xl,
    .container-xxl {
        max-width: 1320px
    }
}

.row {
    --bs-gutter-x: 1.5rem;
    --bs-gutter-y: 0;
    display: flex;
    flex-wrap: wrap;
    margin-top: calc(var(--bs-gutter-y) * -1);
    margin-right: calc(var(--bs-gutter-x)/ -2);
    margin-left: calc(var(--bs-gutter-x)/ -2)
}

.row>* {
    flex-shrink: 0;
    width: 100%;
    max-width: 100%;
    padding-right: calc(var(--bs-gutter-x)/ 2);
    padding-left: calc(var(--bs-gutter-x)/ 2);
    margin-top: var(--bs-gutter-y)
}

.col {
    flex: 1 0 0%
}

.row-cols-auto>* {
    flex: 0 0 auto;
    width: auto
}

.row-cols-1>* {
    flex: 0 0 auto;
    width: 100%
}

.row-cols-2>* {
    flex: 0 0 auto;
    width: 50%
}

.row-cols-3>* {
    flex: 0 0 auto;
    width: 33.3333333333%
}

.row-cols-4>* {
    flex: 0 0 auto;
    width: 25%
}

.row-cols-5>* {
    flex: 0 0 auto;
    width: 20%
}

.row-cols-6>* {
    flex: 0 0 auto;
    width: 16.6666666667%
}

.col-auto {
    flex: 0 0 auto;
    width: auto
}

.col-1 {
    flex: 0 0 auto;
    width: 8.3333333333%
}

.col-2 {
    flex: 0 0 auto;
    width: 16.6666666667%
}

.col-3 {
    flex: 0 0 auto;
    width: 25%
}

.col-4 {
    flex: 0 0 auto;
    width: 33.3333333333%
}

.col-5 {
    flex: 0 0 auto;
    width: 41.6666666667%
}

.col-6 {
    flex: 0 0 auto;
    width: 50%
}

.col-7 {
    flex: 0 0 auto;
    width: 58.3333333333%
}

.col-8 {
    flex: 0 0 auto;
    width: 66.6666666667%
}

.col-9 {
    flex: 0 0 auto;
    width: 75%
}

.col-10 {
    flex: 0 0 auto;
    width: 83.3333333333%
}

.col-11 {
    flex: 0 0 auto;
    width: 91.6666666667%
}

.col-12 {
    flex: 0 0 auto;
    width: 100%
}

.offset-1 {
    margin-left: 8.3333333333%
}

.offset-2 {
    margin-left: 16.6666666667%
}

.offset-3 {
    margin-left: 25%
}

.offset-4 {
    margin-left: 33.3333333333%
}

.offset-5 {
    margin-left: 41.6666666667%
}

.offset-6 {
    margin-left: 50%
}

.offset-7 {
    margin-left: 58.3333333333%
}

.offset-8 {
    margin-left: 66.6666666667%
}

.offset-9 {
    margin-left: 75%
}

.offset-10 {
    margin-left: 83.3333333333%
}

.offset-11 {
    margin-left: 91.6666666667%
}

.g-0,
.gx-0 {
    --bs-gutter-x: 0
}

.g-0,
.gy-0 {
    --bs-gutter-y: 0
}

.g-1,
.gx-1 {
    --bs-gutter-x: 0.25rem
}

.g-1,
.gy-1 {
    --bs-gutter-y: 0.25rem
}

.g-2,
.gx-2 {
    --bs-gutter-x: 0.5rem
}

.g-2,
.gy-2 {
    --bs-gutter-y: 0.5rem
}

.g-3,
.gx-3 {
    --bs-gutter-x: 1rem
}

.g-3,
.gy-3 {
    --bs-gutter-y: 1rem
}

.g-4,
.gx-4 {
    --bs-gutter-x: 1.5rem
}

.g-4,
.gy-4 {
    --bs-gutter-y: 1.5rem
}

.g-5,
.gx-5 {
    --bs-gutter-x: 3rem
}

.g-5,
.gy-5 {
    --bs-gutter-y: 3rem
}

@media (min-width:576px) {
    .col-sm {
        flex: 1 0 0%
    }

    .row-cols-sm-auto>* {
        flex: 0 0 auto;
        width: auto
    }

    .row-cols-sm-1>* {
        flex: 0 0 auto;
        width: 100%
    }

    .row-cols-sm-2>* {
        flex: 0 0 auto;
        width: 50%
    }

    .row-cols-sm-3>* {
        flex: 0 0 auto;
        width: 33.3333333333%
    }

    .row-cols-sm-4>* {
        flex: 0 0 auto;
        width: 25%
    }

    .row-cols-sm-5>* {
        flex: 0 0 auto;
        width: 20%
    }

    .row-cols-sm-6>* {
        flex: 0 0 auto;
        width: 16.6666666667%
    }

    .col-sm-auto {
        flex: 0 0 auto;
        width: auto
    }

    .col-sm-1 {
        flex: 0 0 auto;
        width: 8.3333333333%
    }

    .col-sm-2 {
        flex: 0 0 auto;
        width: 16.6666666667%
    }

    .col-sm-3 {
        flex: 0 0 auto;
        width: 25%
    }

    .col-sm-4 {
        flex: 0 0 auto;
        width: 33.3333333333%
    }

    .col-sm-5 {
        flex: 0 0 auto;
        width: 41.6666666667%
    }

    .col-sm-6 {
        flex: 0 0 auto;
        width: 50%
    }

    .col-sm-7 {
        flex: 0 0 auto;
        width: 58.3333333333%
    }

    .col-sm-8 {
        flex: 0 0 auto;
        width: 66.6666666667%
    }

    .col-sm-9 {
        flex: 0 0 auto;
        width: 75%
    }

    .col-sm-10 {
        flex: 0 0 auto;
        width: 83.3333333333%
    }

    .col-sm-11 {
        flex: 0 0 auto;
        width: 91.6666666667%
    }

    .col-sm-12 {
        flex: 0 0 auto;
        width: 100%
    }

    .offset-sm-0 {
        margin-left: 0
    }

    .offset-sm-1 {
        margin-left: 8.3333333333%
    }

    .offset-sm-2 {
        margin-left: 16.6666666667%
    }

    .offset-sm-3 {
        margin-left: 25%
    }

    .offset-sm-4 {
        margin-left: 33.3333333333%
    }

    .offset-sm-5 {
        margin-left: 41.6666666667%
    }

    .offset-sm-6 {
        margin-left: 50%
    }

    .offset-sm-7 {
        margin-left: 58.3333333333%
    }

    .offset-sm-8 {
        margin-left: 66.6666666667%
    }

    .offset-sm-9 {
        margin-left: 75%
    }

    .offset-sm-10 {
        margin-left: 83.3333333333%
    }

    .offset-sm-11 {
        margin-left: 91.6666666667%
    }

    .g-sm-0,
    .gx-sm-0 {
        --bs-gutter-x: 0
    }

    .g-sm-0,
    .gy-sm-0 {
        --bs-gutter-y: 0
    }

    .g-sm-1,
    .gx-sm-1 {
        --bs-gutter-x: 0.25rem
    }

    .g-sm-1,
    .gy-sm-1 {
        --bs-gutter-y: 0.25rem
    }

    .g-sm-2,
    .gx-sm-2 {
        --bs-gutter-x: 0.5rem
    }

    .g-sm-2,
    .gy-sm-2 {
        --bs-gutter-y: 0.5rem
    }

    .g-sm-3,
    .gx-sm-3 {
        --bs-gutter-x: 1rem
    }

    .g-sm-3,
    .gy-sm-3 {
        --bs-gutter-y: 1rem
    }

    .g-sm-4,
    .gx-sm-4 {
        --bs-gutter-x: 1.5rem
    }

    .g-sm-4,
    .gy-sm-4 {
        --bs-gutter-y: 1.5rem
    }

    .g-sm-5,
    .gx-sm-5 {
        --bs-gutter-x: 3rem
    }

    .g-sm-5,
    .gy-sm-5 {
        --bs-gutter-y: 3rem
    }
}

@media (min-width:768px) {
    .col-md {
        flex: 1 0 0%
    }

    .row-cols-md-auto>* {
        flex: 0 0 auto;
        width: auto
    }

    .row-cols-md-1>* {
        flex: 0 0 auto;
        width: 100%
    }

    .row-cols-md-2>* {
        flex: 0 0 auto;
        width: 50%
    }

    .row-cols-md-3>* {
        flex: 0 0 auto;
        width: 33.3333333333%
    }

    .row-cols-md-4>* {
        flex: 0 0 auto;
        width: 25%
    }

    .row-cols-md-5>* {
        flex: 0 0 auto;
        width: 20%
    }

    .row-cols-md-6>* {
        flex: 0 0 auto;
        width: 16.6666666667%
    }

    .col-md-auto {
        flex: 0 0 auto;
        width: auto
    }

    .col-md-1 {
        flex: 0 0 auto;
        width: 8.3333333333%
    }

    .col-md-2 {
        flex: 0 0 auto;
        width: 16.6666666667%
    }

    .col-md-3 {
        flex: 0 0 auto;
        width: 25%
    }

    .col-md-4 {
        flex: 0 0 auto;
        width: 33.3333333333%
    }

    .col-md-5 {
        flex: 0 0 auto;
        width: 41.6666666667%
    }

    .col-md-6 {
        flex: 0 0 auto;
        width: 50%
    }

    .col-md-7 {
        flex: 0 0 auto;
        width: 58.3333333333%
    }

    .col-md-8 {
        flex: 0 0 auto;
        width: 66.6666666667%
    }

    .col-md-9 {
        flex: 0 0 auto;
        width: 75%
    }

    .col-md-10 {
        flex: 0 0 auto;
        width: 83.3333333333%
    }

    .col-md-11 {
        flex: 0 0 auto;
        width: 91.6666666667%
    }

    .col-md-12 {
        flex: 0 0 auto;
        width: 100%
    }

    .offset-md-0 {
        margin-left: 0
    }

    .offset-md-1 {
        margin-left: 8.3333333333%
    }

    .offset-md-2 {
        margin-left: 16.6666666667%
    }

    .offset-md-3 {
        margin-left: 25%
    }

    .offset-md-4 {
        margin-left: 33.3333333333%
    }

    .offset-md-5 {
        margin-left: 41.6666666667%
    }

    .offset-md-6 {
        margin-left: 50%
    }

    .offset-md-7 {
        margin-left: 58.3333333333%
    }

    .offset-md-8 {
        margin-left: 66.6666666667%
    }

    .offset-md-9 {
        margin-left: 75%
    }

    .offset-md-10 {
        margin-left: 83.3333333333%
    }

    .offset-md-11 {
        margin-left: 91.6666666667%
    }

    .g-md-0,
    .gx-md-0 {
        --bs-gutter-x: 0
    }

    .g-md-0,
    .gy-md-0 {
        --bs-gutter-y: 0
    }

    .g-md-1,
    .gx-md-1 {
        --bs-gutter-x: 0.25rem
    }

    .g-md-1,
    .gy-md-1 {
        --bs-gutter-y: 0.25rem
    }

    .g-md-2,
    .gx-md-2 {
        --bs-gutter-x: 0.5rem
    }

    .g-md-2,
    .gy-md-2 {
        --bs-gutter-y: 0.5rem
    }

    .g-md-3,
    .gx-md-3 {
        --bs-gutter-x: 1rem
    }

    .g-md-3,
    .gy-md-3 {
        --bs-gutter-y: 1rem
    }

    .g-md-4,
    .gx-md-4 {
        --bs-gutter-x: 1.5rem
    }

    .g-md-4,
    .gy-md-4 {
        --bs-gutter-y: 1.5rem
    }

    .g-md-5,
    .gx-md-5 {
        --bs-gutter-x: 3rem
    }

    .g-md-5,
    .gy-md-5 {
        --bs-gutter-y: 3rem
    }
}

@media (min-width:992px) {
    .col-lg {
        flex: 1 0 0%
    }

    .row-cols-lg-auto>* {
        flex: 0 0 auto;
        width: auto
    }

    .row-cols-lg-1>* {
        flex: 0 0 auto;
        width: 100%
    }

    .row-cols-lg-2>* {
        flex: 0 0 auto;
        width: 50%
    }

    .row-cols-lg-3>* {
        flex: 0 0 auto;
        width: 33.3333333333%
    }

    .row-cols-lg-4>* {
        flex: 0 0 auto;
        width: 25%
    }

    .row-cols-lg-5>* {
        flex: 0 0 auto;
        width: 20%
    }

    .row-cols-lg-6>* {
        flex: 0 0 auto;
        width: 16.6666666667%
    }

    .col-lg-auto {
        flex: 0 0 auto;
        width: auto
    }

    .col-lg-1 {
        flex: 0 0 auto;
        width: 8.3333333333%
    }

    .col-lg-2 {
        flex: 0 0 auto;
        width: 16.6666666667%
    }

    .col-lg-3 {
        flex: 0 0 auto;
        width: 25%
    }

    .col-lg-4 {
        flex: 0 0 auto;
        width: 33.3333333333%
    }

    .col-lg-5 {
        flex: 0 0 auto;
        width: 41.6666666667%
    }

    .col-lg-6 {
        flex: 0 0 auto;
        width: 50%
    }

    .col-lg-7 {
        flex: 0 0 auto;
        width: 58.3333333333%
    }

    .col-lg-8 {
        flex: 0 0 auto;
        width: 66.6666666667%
    }

    .col-lg-9 {
        flex: 0 0 auto;
        width: 75%
    }

    .col-lg-10 {
        flex: 0 0 auto;
        width: 83.3333333333%
    }

    .col-lg-11 {
        flex: 0 0 auto;
        width: 91.6666666667%
    }

    .col-lg-12 {
        flex: 0 0 auto;
        width: 100%
    }

    .offset-lg-0 {
        margin-left: 0
    }

    .offset-lg-1 {
        margin-left: 8.3333333333%
    }

    .offset-lg-2 {
        margin-left: 16.6666666667%
    }

    .offset-lg-3 {
        margin-left: 25%
    }

    .offset-lg-4 {
        margin-left: 33.3333333333%
    }

    .offset-lg-5 {
        margin-left: 41.6666666667%
    }

    .offset-lg-6 {
        margin-left: 50%
    }

    .offset-lg-7 {
        margin-left: 58.3333333333%
    }

    .offset-lg-8 {
        margin-left: 66.6666666667%
    }

    .offset-lg-9 {
        margin-left: 75%
    }

    .offset-lg-10 {
        margin-left: 83.3333333333%
    }

    .offset-lg-11 {
        margin-left: 91.6666666667%
    }

    .g-lg-0,
    .gx-lg-0 {
        --bs-gutter-x: 0
    }

    .g-lg-0,
    .gy-lg-0 {
        --bs-gutter-y: 0
    }

    .g-lg-1,
    .gx-lg-1 {
        --bs-gutter-x: 0.25rem
    }

    .g-lg-1,
    .gy-lg-1 {
        --bs-gutter-y: 0.25rem
    }

    .g-lg-2,
    .gx-lg-2 {
        --bs-gutter-x: 0.5rem
    }

    .g-lg-2,
    .gy-lg-2 {
        --bs-gutter-y: 0.5rem
    }

    .g-lg-3,
    .gx-lg-3 {
        --bs-gutter-x: 1rem
    }

    .g-lg-3,
    .gy-lg-3 {
        --bs-gutter-y: 1rem
    }

    .g-lg-4,
    .gx-lg-4 {
        --bs-gutter-x: 1.5rem
    }

    .g-lg-4,
    .gy-lg-4 {
        --bs-gutter-y: 1.5rem
    }

    .g-lg-5,
    .gx-lg-5 {
        --bs-gutter-x: 3rem
    }

    .g-lg-5,
    .gy-lg-5 {
        --bs-gutter-y: 3rem
    }
}

@media (min-width:1200px) {
    .col-xl {
        flex: 1 0 0%
    }

    .row-cols-xl-auto>* {
        flex: 0 0 auto;
        width: auto
    }

    .row-cols-xl-1>* {
        flex: 0 0 auto;
        width: 100%
    }

    .row-cols-xl-2>* {
        flex: 0 0 auto;
        width: 50%
    }

    .row-cols-xl-3>* {
        flex: 0 0 auto;
        width: 33.3333333333%
    }

    .row-cols-xl-4>* {
        flex: 0 0 auto;
        width: 25%
    }

    .row-cols-xl-5>* {
        flex: 0 0 auto;
        width: 20%
    }

    .row-cols-xl-6>* {
        flex: 0 0 auto;
        width: 16.6666666667%
    }

    .col-xl-auto {
        flex: 0 0 auto;
        width: auto
    }

    .col-xl-1 {
        flex: 0 0 auto;
        width: 8.3333333333%
    }

    .col-xl-2 {
        flex: 0 0 auto;
        width: 16.6666666667%
    }

    .col-xl-3 {
        flex: 0 0 auto;
        width: 25%
    }

    .col-xl-4 {
        flex: 0 0 auto;
        width: 33.3333333333%
    }

    .col-xl-5 {
        flex: 0 0 auto;
        width: 41.6666666667%
    }

    .col-xl-6 {
        flex: 0 0 auto;
        width: 50%
    }

    .col-xl-7 {
        flex: 0 0 auto;
        width: 58.3333333333%
    }

    .col-xl-8 {
        flex: 0 0 auto;
        width: 66.6666666667%
    }

    .col-xl-9 {
        flex: 0 0 auto;
        width: 75%
    }

    .col-xl-10 {
        flex: 0 0 auto;
        width: 83.3333333333%
    }

    .col-xl-11 {
        flex: 0 0 auto;
        width: 91.6666666667%
    }

    .col-xl-12 {
        flex: 0 0 auto;
        width: 100%
    }

    .offset-xl-0 {
        margin-left: 0
    }

    .offset-xl-1 {
        margin-left: 8.3333333333%
    }

    .offset-xl-2 {
        margin-left: 16.6666666667%
    }

    .offset-xl-3 {
        margin-left: 25%
    }

    .offset-xl-4 {
        margin-left: 33.3333333333%
    }

    .offset-xl-5 {
        margin-left: 41.6666666667%
    }

    .offset-xl-6 {
        margin-left: 50%
    }

    .offset-xl-7 {
        margin-left: 58.3333333333%
    }

    .offset-xl-8 {
        margin-left: 66.6666666667%
    }

    .offset-xl-9 {
        margin-left: 75%
    }

    .offset-xl-10 {
        margin-left: 83.3333333333%
    }

    .offset-xl-11 {
        margin-left: 91.6666666667%
    }

    .g-xl-0,
    .gx-xl-0 {
        --bs-gutter-x: 0
    }

    .g-xl-0,
    .gy-xl-0 {
        --bs-gutter-y: 0
    }

    .g-xl-1,
    .gx-xl-1 {
        --bs-gutter-x: 0.25rem
    }

    .g-xl-1,
    .gy-xl-1 {
        --bs-gutter-y: 0.25rem
    }

    .g-xl-2,
    .gx-xl-2 {
        --bs-gutter-x: 0.5rem
    }

    .g-xl-2,
    .gy-xl-2 {
        --bs-gutter-y: 0.5rem
    }

    .g-xl-3,
    .gx-xl-3 {
        --bs-gutter-x: 1rem
    }

    .g-xl-3,
    .gy-xl-3 {
        --bs-gutter-y: 1rem
    }

    .g-xl-4,
    .gx-xl-4 {
        --bs-gutter-x: 1.5rem
    }

    .g-xl-4,
    .gy-xl-4 {
        --bs-gutter-y: 1.5rem
    }

    .g-xl-5,
    .gx-xl-5 {
        --bs-gutter-x: 3rem
    }

    .g-xl-5,
    .gy-xl-5 {
        --bs-gutter-y: 3rem
    }
}

@media (min-width:1400px) {
    .col-xxl {
        flex: 1 0 0%
    }

    .row-cols-xxl-auto>* {
        flex: 0 0 auto;
        width: auto
    }

    .row-cols-xxl-1>* {
        flex: 0 0 auto;
        width: 100%
    }

    .row-cols-xxl-2>* {
        flex: 0 0 auto;
        width: 50%
    }

    .row-cols-xxl-3>* {
        flex: 0 0 auto;
        width: 33.3333333333%
    }

    .row-cols-xxl-4>* {
        flex: 0 0 auto;
        width: 25%
    }

    .row-cols-xxl-5>* {
        flex: 0 0 auto;
        width: 20%
    }

    .row-cols-xxl-6>* {
        flex: 0 0 auto;
        width: 16.6666666667%
    }

    .col-xxl-auto {
        flex: 0 0 auto;
        width: auto
    }

    .col-xxl-1 {
        flex: 0 0 auto;
        width: 8.3333333333%
    }

    .col-xxl-2 {
        flex: 0 0 auto;
        width: 16.6666666667%
    }

    .col-xxl-3 {
        flex: 0 0 auto;
        width: 25%
    }

    .col-xxl-4 {
        flex: 0 0 auto;
        width: 33.3333333333%
    }

    .col-xxl-5 {
        flex: 0 0 auto;
        width: 41.6666666667%
    }

    .col-xxl-6 {
        flex: 0 0 auto;
        width: 50%
    }

    .col-xxl-7 {
        flex: 0 0 auto;
        width: 58.3333333333%
    }

    .col-xxl-8 {
        flex: 0 0 auto;
        width: 66.6666666667%
    }

    .col-xxl-9 {
        flex: 0 0 auto;
        width: 75%
    }

    .col-xxl-10 {
        flex: 0 0 auto;
        width: 83.3333333333%
    }

    .col-xxl-11 {
        flex: 0 0 auto;
        width: 91.6666666667%
    }

    .col-xxl-12 {
        flex: 0 0 auto;
        width: 100%
    }

    .offset-xxl-0 {
        margin-left: 0
    }

    .offset-xxl-1 {
        margin-left: 8.3333333333%
    }

    .offset-xxl-2 {
        margin-left: 16.6666666667%
    }

    .offset-xxl-3 {
        margin-left: 25%
    }

    .offset-xxl-4 {
        margin-left: 33.3333333333%
    }

    .offset-xxl-5 {
        margin-left: 41.6666666667%
    }

    .offset-xxl-6 {
        margin-left: 50%
    }

    .offset-xxl-7 {
        margin-left: 58.3333333333%
    }

    .offset-xxl-8 {
        margin-left: 66.6666666667%
    }

    .offset-xxl-9 {
        margin-left: 75%
    }

    .offset-xxl-10 {
        margin-left: 83.3333333333%
    }

    .offset-xxl-11 {
        margin-left: 91.6666666667%
    }

    .g-xxl-0,
    .gx-xxl-0 {
        --bs-gutter-x: 0
    }

    .g-xxl-0,
    .gy-xxl-0 {
        --bs-gutter-y: 0
    }

    .g-xxl-1,
    .gx-xxl-1 {
        --bs-gutter-x: 0.25rem
    }

    .g-xxl-1,
    .gy-xxl-1 {
        --bs-gutter-y: 0.25rem
    }

    .g-xxl-2,
    .gx-xxl-2 {
        --bs-gutter-x: 0.5rem
    }

    .g-xxl-2,
    .gy-xxl-2 {
        --bs-gutter-y: 0.5rem
    }

    .g-xxl-3,
    .gx-xxl-3 {
        --bs-gutter-x: 1rem
    }

    .g-xxl-3,
    .gy-xxl-3 {
        --bs-gutter-y: 1rem
    }

    .g-xxl-4,
    .gx-xxl-4 {
        --bs-gutter-x: 1.5rem
    }

    .g-xxl-4,
    .gy-xxl-4 {
        --bs-gutter-y: 1.5rem
    }

    .g-xxl-5,
    .gx-xxl-5 {
        --bs-gutter-x: 3rem
    }

    .g-xxl-5,
    .gy-xxl-5 {
        --bs-gutter-y: 3rem
    }
}

.table {
    --bs-table-bg: transparent;
    --bs-table-striped-color: #212529;
    --bs-table-striped-bg: rgba(0, 0, 0, 0.05);
    --bs-table-active-color: #212529;
    --bs-table-active-bg: rgba(0, 0, 0, 0.1);
    --bs-table-hover-color: #212529;
    --bs-table-hover-bg: rgba(0, 0, 0, 0.075);
    width: 100%;
    margin-bottom: 1rem;
    color: #212529;
    vertical-align: top;
    border-color: #dee2e6
}

.table>:not(caption)>*>* {
    padding: .5rem .5rem;
    background-color: var(--bs-table-bg);
    border-bottom-width: 1px;
    box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg)
}

.table>tbody {
    vertical-align: inherit
}

.table>thead {
    vertical-align: bottom
}

.table>:not(:last-child)>:last-child>* {
    border-bottom-color: currentColor
}

.caption-top {
    caption-side: top
}

.table-sm>:not(caption)>*>* {
    padding: .25rem .25rem
}

.table-bordered>:not(caption)>* {
    border-width: 1px 0
}

.table-bordered>:not(caption)>*>* {
    border-width: 0 1px
}

.table-borderless>:not(caption)>*>* {
    border-bottom-width: 0
}

.table-striped>tbody>tr:nth-of-type(odd) {
    --bs-table-accent-bg: var(--bs-table-striped-bg);
    color: var(--bs-table-striped-color)
}

.table-active {
    --bs-table-accent-bg: var(--bs-table-active-bg);
    color: var(--bs-table-active-color)
}

.table-hover>tbody>tr:hover {
    --bs-table-accent-bg: var(--bs-table-hover-bg);
    color: var(--bs-table-hover-color)
}

.table-primary {
    --bs-table-bg: #cfe2ff;
    --bs-table-striped-bg: #c5d7f2;
    --bs-table-striped-color: #000;
    --bs-table-active-bg: #bacbe6;
    --bs-table-active-color: #000;
    --bs-table-hover-bg: #bfd1ec;
    --bs-table-hover-color: #000;
    color: #000;
    border-color: #bacbe6
}

.table-secondary {
    --bs-table-bg: #e2e3e5;
    --bs-table-striped-bg: #d7d8da;
    --bs-table-striped-color: #000;
    --bs-table-active-bg: #cbccce;
    --bs-table-active-color: #000;
    --bs-table-hover-bg: #d1d2d4;
    --bs-table-hover-color: #000;
    color: #000;
    border-color: #cbccce
}

.table-success {
    --bs-table-bg: #d1e7dd;
    --bs-table-striped-bg: #c7dbd2;
    --bs-table-striped-color: #000;
    --bs-table-active-bg: #bcd0c7;
    --bs-table-active-color: #000;
    --bs-table-hover-bg: #c1d6cc;
    --bs-table-hover-color: #000;
    color: #000;
    border-color: #bcd0c7
}

.table-info {
    --bs-table-bg: #cff4fc;
    --bs-table-striped-bg: #c5e8ef;
    --bs-table-striped-color: #000;
    --bs-table-active-bg: #badce3;
    --bs-table-active-color: #000;
    --bs-table-hover-bg: #bfe2e9;
    --bs-table-hover-color: #000;
    color: #000;
    border-color: #badce3
}

.table-warning {
    --bs-table-bg: #fff3cd;
    --bs-table-striped-bg: #f2e7c3;
    --bs-table-striped-color: #000;
    --bs-table-active-bg: #e6dbb9;
    --bs-table-active-color: #000;
    --bs-table-hover-bg: #ece1be;
    --bs-table-hover-color: #000;
    color: #000;
    border-color: #e6dbb9
}

.table-danger {
    --bs-table-bg: #f8d7da;
    --bs-table-striped-bg: #eccccf;
    --bs-table-striped-color: #000;
    --bs-table-active-bg: #dfc2c4;
    --bs-table-active-color: #000;
    --bs-table-hover-bg: #e5c7ca;
    --bs-table-hover-color: #000;
    color: #000;
    border-color: #dfc2c4
}

.table-light {
    --bs-table-bg: #f8f9fa;
    --bs-table-striped-bg: #ecedee;
    --bs-table-striped-color: #000;
    --bs-table-active-bg: #dfe0e1;
    --bs-table-active-color: #000;
    --bs-table-hover-bg: #e5e6e7;
    --bs-table-hover-color: #000;
    color: #000;
    border-color: #dfe0e1
}

.table-dark {
    --bs-table-bg: #212529;
    --bs-table-striped-bg: #2c3034;
    --bs-table-striped-color: #fff;
    --bs-table-active-bg: #373b3e;
    --bs-table-active-color: #fff;
    --bs-table-hover-bg: #323539;
    --bs-table-hover-color: #fff;
    color: #fff;
    border-color: #373b3e
}

.table-responsive {
    overflow-x: auto;
    -webkit-overflow-scrolling: touch
}

@media (max-width:575.98px) {
    .table-responsive-sm {
        overflow-x: auto;
        -webkit-overflow-scrolling: touch
    }
}

@media (max-width:767.98px) {
    .table-responsive-md {
        overflow-x: auto;
        -webkit-overflow-scrolling: touch
    }
}

@media (max-width:991.98px) {
    .table-responsive-lg {
        overflow-x: auto;
        -webkit-overflow-scrolling: touch
    }
}

@media (max-width:1199.98px) {
    .table-responsive-xl {
        overflow-x: auto;
        -webkit-overflow-scrolling: touch
    }
}

@media (max-width:1399.98px) {
    .table-responsive-xxl {
        overflow-x: auto;
        -webkit-overflow-scrolling: touch
    }
}

.form-label {
    margin-bottom: .5rem
}

.col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5
}

.col-form-label-lg {
    padding-top: calc(.5rem + 1px);
    padding-bottom: calc(.5rem + 1px);
    font-size: 1.25rem
}

.col-form-label-sm {
    padding-top: calc(.25rem + 1px);
    padding-bottom: calc(.25rem + 1px);
    font-size: .875rem
}

.form-text {
    margin-top: .25rem;
    font-size: .875em;
    color: #6c757d
}

.form-control {
    display: block;
    width: 100%;
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out
}

@media (prefers-reduced-motion:reduce) {
    .form-control {
        transition: none
    }
}

.form-control[type=file] {
    overflow: hidden
}

.form-control[type=file]:not(:disabled):not([readonly]) {
    cursor: pointer
}

.form-control:focus {
    color: #212529;
    background-color: #fff;
    border-color: #86b7fe;
    outline: 0;
    box-shadow: 0 0 0 .25rem rgba(13, 110, 253, .25)
}

.form-control::-webkit-date-and-time-value {
    height: 1.5em
}

.form-control::-webkit-input-placeholder {
    color: #6c757d;
    opacity: 1
}

.form-control::-moz-placeholder {
    color: #6c757d;
    opacity: 1
}

.form-control::placeholder {
    color: #6c757d;
    opacity: 1
}

.form-control:disabled,
.form-control[readonly] {
    background-color: #e9ecef;
    opacity: 1
}

.form-control::file-selector-button {
    padding: .375rem .75rem;
    margin: -.375rem -.75rem;
    -webkit-margin-end: .75rem;
    margin-inline-end: .75rem;
    color: #212529;
    background-color: #e9ecef;
    pointer-events: none;
    border-color: inherit;
    border-style: solid;
    border-width: 0;
    border-inline-end-width: 1px;
    border-radius: 0;
    transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out
}

@media (prefers-reduced-motion:reduce) {
    .form-control::file-selector-button {
        transition: none
    }
}

.form-control:hover:not(:disabled):not([readonly])::file-selector-button {
    background-color: #dde0e3
}

.form-control::-webkit-file-upload-button {
    padding: .375rem .75rem;
    margin: -.375rem -.75rem;
    -webkit-margin-end: .75rem;
    margin-inline-end: .75rem;
    color: #212529;
    background-color: #e9ecef;
    pointer-events: none;
    border-color: inherit;
    border-style: solid;
    border-width: 0;
    border-inline-end-width: 1px;
    border-radius: 0;
    -webkit-transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out
}

@media (prefers-reduced-motion:reduce) {
    .form-control::-webkit-file-upload-button {
        -webkit-transition: none;
        transition: none
    }
}

.form-control:hover:not(:disabled):not([readonly])::-webkit-file-upload-button {
    background-color: #dde0e3
}

.form-control-plaintext {
    display: block;
    width: 100%;
    padding: .375rem 0;
    margin-bottom: 0;
    line-height: 1.5;
    color: #212529;
    background-color: transparent;
    border: solid transparent;
    border-width: 1px 0
}

.form-control-plaintext.form-control-lg,
.form-control-plaintext.form-control-sm {
    padding-right: 0;
    padding-left: 0
}

.form-control-sm {
    min-height: calc(1.5em + .5rem + 2px);
    padding: .25rem .5rem;
    font-size: .875rem;
    border-radius: .2rem
}

.form-control-sm::file-selector-button {
    padding: .25rem .5rem;
    margin: -.25rem -.5rem;
    -webkit-margin-end: .5rem;
    margin-inline-end: .5rem
}

.form-control-sm::-webkit-file-upload-button {
    padding: .25rem .5rem;
    margin: -.25rem -.5rem;
    -webkit-margin-end: .5rem;
    margin-inline-end: .5rem
}

.form-control-lg {
    min-height: calc(1.5em + 1rem + 2px);
    padding: .5rem 1rem;
    font-size: 1.25rem;
    border-radius: .3rem
}

.form-control-lg::file-selector-button {
    padding: .5rem 1rem;
    margin: -.5rem -1rem;
    -webkit-margin-end: 1rem;
    margin-inline-end: 1rem
}

.form-control-lg::-webkit-file-upload-button {
    padding: .5rem 1rem;
    margin: -.5rem -1rem;
    -webkit-margin-end: 1rem;
    margin-inline-end: 1rem
}

textarea.form-control {
    min-height: calc(1.5em + .75rem + 2px)
}

textarea.form-control-sm {
    min-height: calc(1.5em + .5rem + 2px)
}

textarea.form-control-lg {
    min-height: calc(1.5em + 1rem + 2px)
}

.form-control-color {
    max-width: 3rem;
    height: auto;
    padding: .375rem
}

.form-control-color:not(:disabled):not([readonly]) {
    cursor: pointer
}

.form-control-color::-moz-color-swatch {
    height: 1.5em;
    border-radius: .25rem
}

.form-control-color::-webkit-color-swatch {
    height: 1.5em;
    border-radius: .25rem
}

.form-select {
    display: block;
    width: 100%;
    padding: .375rem 2.25rem .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    background-color: #fff;
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3e%3cpath fill='none' stroke='%23343a40' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M2 5l6 6 6-6'/%3e%3c/svg%3e");
    background-repeat: no-repeat;
    background-position: right .75rem center;
    background-size: 16px 12px;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none
}

.form-select:focus {
    border-color: #86b7fe;
    outline: 0;
    box-shadow: 0 0 0 .25rem rgba(13, 110, 253, .25)
}

.form-select[multiple],
.form-select[size]:not([size="1"]) {
    padding-right: .75rem;
    background-image: none
}

.form-select:disabled {
    color: #6c757d;
    background-color: #e9ecef
}

.form-select:-moz-focusring {
    color: transparent;
    text-shadow: 0 0 0 #212529
}

.form-select-sm {
    padding-top: .25rem;
    padding-bottom: .25rem;
    padding-left: .5rem;
    font-size: .875rem
}

.form-select-lg {
    padding-top: .5rem;
    padding-bottom: .5rem;
    padding-left: 1rem;
    font-size: 1.25rem
}

.form-check {
    display: block;
    min-height: 1.5rem;
    padding-left: 1.5em;
    margin-bottom: .125rem
}

.form-check .form-check-input {
    float: left;
    margin-left: -1.5em
}

.form-check-input {
    width: 1em;
    height: 1em;
    margin-top: .25em;
    vertical-align: top;
    background-color: #fff;
    background-repeat: no-repeat;
    background-position: center;
    background-size: contain;
    border: 1px solid rgba(0, 0, 0, .25);
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    -webkit-print-color-adjust: exact;
    color-adjust: exact
}

.form-check-input[type=checkbox] {
    border-radius: .25em
}

.form-check-input[type=radio] {
    border-radius: 50%
}

.form-check-input:active {
    filter: brightness(90%)
}

.form-check-input:focus {
    border-color: #86b7fe;
    outline: 0;
    box-shadow: 0 0 0 .25rem rgba(13, 110, 253, .25)
}

.form-check-input:checked {
    background-color: #fd0d0d;
    border-color: #fd0d0d
}

.form-check-input:checked[type=checkbox] {
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'%3e%3cpath fill='none' stroke='%23fff' stroke-linecap='round' stroke-linejoin='round' stroke-width='3' d='M6 10l3 3l6-6'/%3e%3c/svg%3e")
}

.form-check-input:checked[type=radio] {
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'%3e%3ccircle r='2' fill='%23fff'/%3e%3c/svg%3e")
}

.form-check-input[type=checkbox]:indeterminate {
    background-color: #fd0d0d;
    border-color: #fd0d0d;
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'%3e%3cpath fill='none' stroke='%23fff' stroke-linecap='round' stroke-linejoin='round' stroke-width='3' d='M6 10h8'/%3e%3c/svg%3e")
}

.form-check-input:disabled {
    pointer-events: none;
    filter: none;
    opacity: .5
}

.form-check-input:disabled~.form-check-label,
.form-check-input[disabled]~.form-check-label {
    opacity: .5
}

.form-switch {
    padding-left: 2.5em
}

.form-switch .form-check-input {
    width: 2em;
    margin-left: -2.5em;
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'%3e%3ccircle r='3' fill='rgba%280, 0, 0, 0.25%29'/%3e%3c/svg%3e");
    background-position: left center;
    border-radius: 2em;
    transition: background-position .15s ease-in-out
}

@media (prefers-reduced-motion:reduce) {
    .form-switch .form-check-input {
        transition: none
    }
}

.form-switch .form-check-input:focus {
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'%3e%3ccircle r='3' fill='%2386b7fe'/%3e%3c/svg%3e")
}

.form-switch .form-check-input:checked {
    background-position: right center;
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'%3e%3ccircle r='3' fill='%23fff'/%3e%3c/svg%3e")
}

.form-check-inline {
    display: inline-block;
    margin-right: 1rem
}

.btn-check {
    position: absolute;
    clip: rect(0, 0, 0, 0);
    pointer-events: none
}

.btn-check:disabled+.btn,
.btn-check[disabled]+.btn {
    pointer-events: none;
    filter: none;
    opacity: .65
}

.form-range {
    width: 100%;
    height: 1.5rem;
    padding: 0;
    background-color: transparent;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none
}

.form-range:focus {
    outline: 0
}

.form-range:focus::-webkit-slider-thumb {
    box-shadow: 0 0 0 1px #fff, 0 0 0 .25rem rgba(13, 110, 253, .25)
}

.form-range:focus::-moz-range-thumb {
    box-shadow: 0 0 0 1px #fff, 0 0 0 .25rem rgba(13, 110, 253, .25)
}

.form-range::-moz-focus-outer {
    border: 0
}

.form-range::-webkit-slider-thumb {
    width: 1rem;
    height: 1rem;
    margin-top: -.25rem;
    background-color: #fd0d0d;
    border: 0;
    border-radius: 1rem;
    -webkit-transition: background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    transition: background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    -webkit-appearance: none;
    appearance: none
}

@media (prefers-reduced-motion:reduce) {
    .form-range::-webkit-slider-thumb {
        -webkit-transition: none;
        transition: none
    }
}

.form-range::-webkit-slider-thumb:active {
    background-color: #b6d4fe
}

.form-range::-webkit-slider-runnable-track {
    width: 100%;
    height: .5rem;
    color: transparent;
    cursor: pointer;
    background-color: #dee2e6;
    border-color: transparent;
    border-radius: 1rem
}

.form-range::-moz-range-thumb {
    width: 1rem;
    height: 1rem;
    background-color: #fd0d0d;
    border: 0;
    border-radius: 1rem;
    -moz-transition: background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    transition: background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    -moz-appearance: none;
    appearance: none
}

@media (prefers-reduced-motion:reduce) {
    .form-range::-moz-range-thumb {
        -moz-transition: none;
        transition: none
    }
}

.form-range::-moz-range-thumb:active {
    background-color: #b6d4fe
}

.form-range::-moz-range-track {
    width: 100%;
    height: .5rem;
    color: transparent;
    cursor: pointer;
    background-color: #dee2e6;
    border-color: transparent;
    border-radius: 1rem
}

.form-range:disabled {
    pointer-events: none
}

.form-range:disabled::-webkit-slider-thumb {
    background-color: #adb5bd
}

.form-range:disabled::-moz-range-thumb {
    background-color: #adb5bd
}

.form-floating {
    position: relative
}

.form-floating>.form-control,
.form-floating>.form-select {
    height: calc(3.5rem + 2px);
    padding: 1rem .75rem
}

.form-floating>label {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    padding: 1rem .75rem;
    pointer-events: none;
    border: 1px solid transparent;
    transform-origin: 0 0;
    transition: opacity .1s ease-in-out, transform .1s ease-in-out
}

@media (prefers-reduced-motion:reduce) {
    .form-floating>label {
        transition: none
    }
}

.form-floating>.form-control::-webkit-input-placeholder {
    color: transparent
}

.form-floating>.form-control::-moz-placeholder {
    color: transparent
}

.form-floating>.form-control::placeholder {
    color: transparent
}

.form-floating>.form-control:not(:-moz-placeholder-shown) {
    padding-top: 1.625rem;
    padding-bottom: .625rem
}

.form-floating>.form-control:focus,
.form-floating>.form-control:not(:placeholder-shown) {
    padding-top: 1.625rem;
    padding-bottom: .625rem
}

.form-floating>.form-control:-webkit-autofill {
    padding-top: 1.625rem;
    padding-bottom: .625rem
}

.form-floating>.form-select {
    padding-top: 1.625rem;
    padding-bottom: .625rem
}

.form-floating>.form-control:not(:-moz-placeholder-shown)~label {
    opacity: .65;
    transform: scale(.85) translateY(-.5rem) translateX(.15rem)
}

.form-floating>.form-control:focus~label,
.form-floating>.form-control:not(:placeholder-shown)~label,
.form-floating>.form-select~label {
    opacity: .65;
    transform: scale(.85) translateY(-.5rem) translateX(.15rem)
}

.form-floating>.form-control:-webkit-autofill~label {
    opacity: .65;
    transform: scale(.85) translateY(-.5rem) translateX(.15rem)
}

.input-group {
    position: relative;
    display: flex;
    flex-wrap: wrap;
    align-items: stretch;
    width: 100%
}

.input-group>.form-control,
.input-group>.form-select {
    position: relative;
    flex: 1 1 auto;
    width: 1%;
    min-width: 0
}

.input-group>.form-control:focus,
.input-group>.form-select:focus {
    z-index: 3
}

.input-group .btn {
    position: relative;
    z-index: 2
}

.input-group .btn:focus {
    z-index: 3
}

.input-group-text {
    display: flex;
    align-items: center;
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    text-align: center;
    white-space: nowrap;
    background-color: #e9ecef;
    border: 1px solid #ced4da;
    border-radius: .25rem
}

.input-group-lg>.btn,
.input-group-lg>.form-control,
.input-group-lg>.form-select,
.input-group-lg>.input-group-text {
    padding: .5rem 1rem;
    font-size: 1.25rem;
    border-radius: .3rem
}

.input-group-sm>.btn,
.input-group-sm>.form-control,
.input-group-sm>.form-select,
.input-group-sm>.input-group-text {
    padding: .25rem .5rem;
    font-size: .875rem;
    border-radius: .2rem
}

.input-group-lg>.form-select,
.input-group-sm>.form-select {
    padding-right: 3rem
}

.input-group:not(.has-validation)>.dropdown-toggle:nth-last-child(n+3),
.input-group:not(.has-validation)>:not(:last-child):not(.dropdown-toggle):not(.dropdown-menu) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0
}

.input-group.has-validation>.dropdown-toggle:nth-last-child(n+4),
.input-group.has-validation>:nth-last-child(n+3):not(.dropdown-toggle):not(.dropdown-menu) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0
}

.input-group>:not(:first-child):not(.dropdown-menu):not(.valid-tooltip):not(.valid-feedback):not(.invalid-tooltip):not(.invalid-feedback) {
    margin-left: -1px;
    border-top-left-radius: 0;
    border-bottom-left-radius: 0
}

.valid-feedback {
    display: none;
    width: 100%;
    margin-top: .25rem;
    font-size: .875em;
    color: #198754
}

.valid-tooltip {
    position: absolute;
    top: 100%;
    z-index: 5;
    display: none;
    max-width: 100%;
    padding: .25rem .5rem;
    margin-top: .1rem;
    font-size: .875rem;
    color: #fff;
    background-color: rgba(25, 135, 84, .9);
    border-radius: .25rem
}

.is-valid~.valid-feedback,
.is-valid~.valid-tooltip,
.was-validated :valid~.valid-feedback,
.was-validated :valid~.valid-tooltip {
    display: block
}

.form-control.is-valid,
.was-validated .form-control:valid {
    border-color: #198754;
    padding-right: calc(1.5em + .75rem);
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3e%3cpath fill='%23198754' d='M2.3 6.73L.6 4.53c-.4-1.04.46-1.4 1.1-.8l1.1 1.4 3.4-3.8c.6-.63 1.6-.27 1.2.7l-4 4.6c-.43.5-.8.4-1.1.1z'/%3e%3c/svg%3e");
    background-repeat: no-repeat;
    background-position: right calc(.375em + .1875rem) center;
    background-size: calc(.75em + .375rem) calc(.75em + .375rem)
}

.form-control.is-valid:focus,
.was-validated .form-control:valid:focus {
    border-color: #198754;
    box-shadow: 0 0 0 .25rem rgba(25, 135, 84, .25)
}

.was-validated textarea.form-control:valid,
textarea.form-control.is-valid {
    padding-right: calc(1.5em + .75rem);
    background-position: top calc(.375em + .1875rem) right calc(.375em + .1875rem)
}

.form-select.is-valid,
.was-validated .form-select:valid {
    border-color: #198754;
    padding-right: 4.125rem;
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3e%3cpath fill='none' stroke='%23343a40' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M2 5l6 6 6-6'/%3e%3c/svg%3e"), url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3e%3cpath fill='%23198754' d='M2.3 6.73L.6 4.53c-.4-1.04.46-1.4 1.1-.8l1.1 1.4 3.4-3.8c.6-.63 1.6-.27 1.2.7l-4 4.6c-.43.5-.8.4-1.1.1z'/%3e%3c/svg%3e");
    background-position: right .75rem center, center right 2.25rem;
    background-size: 16px 12px, calc(.75em + .375rem) calc(.75em + .375rem)
}

.form-select.is-valid:focus,
.was-validated .form-select:valid:focus {
    border-color: #198754;
    box-shadow: 0 0 0 .25rem rgba(25, 135, 84, .25)
}

.form-check-input.is-valid,
.was-validated .form-check-input:valid {
    border-color: #198754
}

.form-check-input.is-valid:checked,
.was-validated .form-check-input:valid:checked {
    background-color: #198754
}

.form-check-input.is-valid:focus,
.was-validated .form-check-input:valid:focus {
    box-shadow: 0 0 0 .25rem rgba(25, 135, 84, .25)
}

.form-check-input.is-valid~.form-check-label,
.was-validated .form-check-input:valid~.form-check-label {
    color: #198754
}

.form-check-inline .form-check-input~.valid-feedback {
    margin-left: .5em
}

.invalid-feedback {
    display: none;
    width: 100%;
    margin-top: .25rem;
    font-size: .875em;
    color: #dc3545
}

.invalid-tooltip {
    position: absolute;
    top: 100%;
    z-index: 5;
    display: none;
    max-width: 100%;
    padding: .25rem .5rem;
    margin-top: .1rem;
    font-size: .875rem;
    color: #fff;
    background-color: rgba(220, 53, 69, .9);
    border-radius: .25rem
}

.is-invalid~.invalid-feedback,
.is-invalid~.invalid-tooltip,
.was-validated :invalid~.invalid-feedback,
.was-validated :invalid~.invalid-tooltip {
    display: block
}

.form-control.is-invalid,
.was-validated .form-control:invalid {
    border-color: #dc3545;
    padding-right: calc(1.5em + .75rem);
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e");
    background-repeat: no-repeat;
    background-position: right calc(.375em + .1875rem) center;
    background-size: calc(.75em + .375rem) calc(.75em + .375rem)
}

.form-control.is-invalid:focus,
.was-validated .form-control:invalid:focus {
    border-color: #dc3545;
    box-shadow: 0 0 0 .25rem rgba(220, 53, 69, .25)
}

.was-validated textarea.form-control:invalid,
textarea.form-control.is-invalid {
    padding-right: calc(1.5em + .75rem);
    background-position: top calc(.375em + .1875rem) right calc(.375em + .1875rem)
}

.form-select.is-invalid,
.was-validated .form-select:invalid {
    border-color: #dc3545;
    padding-right: 4.125rem;
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3e%3cpath fill='none' stroke='%23343a40' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M2 5l6 6 6-6'/%3e%3c/svg%3e"), url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e");
    background-position: right .75rem center, center right 2.25rem;
    background-size: 16px 12px, calc(.75em + .375rem) calc(.75em + .375rem)
}

.form-select.is-invalid:focus,
.was-validated .form-select:invalid:focus {
    border-color: #dc3545;
    box-shadow: 0 0 0 .25rem rgba(220, 53, 69, .25)
}

.form-check-input.is-invalid,
.was-validated .form-check-input:invalid {
    border-color: #dc3545
}

.form-check-input.is-invalid:checked,
.was-validated .form-check-input:invalid:checked {
    background-color: #dc3545
}

.form-check-input.is-invalid:focus,
.was-validated .form-check-input:invalid:focus {
    box-shadow: 0 0 0 .25rem rgba(220, 53, 69, .25)
}

.form-check-input.is-invalid~.form-check-label,
.was-validated .form-check-input:invalid~.form-check-label {
    color: #dc3545
}

.form-check-inline .form-check-input~.invalid-feedback {
    margin-left: .5em
}

.btn {
    display: inline-block;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    text-align: center;
    text-decoration: none;
    vertical-align: middle;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    user-select: none;
    background-color: transparent;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    border-radius: .25rem;
    transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out
}

@media (prefers-reduced-motion:reduce) {
    .btn {
        transition: none
    }
}

.btn:hover {
    color: #212529
}

.btn-check:focus+.btn,
.btn:focus {
    outline: 0;
    box-shadow: 0 0 0 .25rem rgba(13, 110, 253, .25)
}

.btn.disabled,
.btn:disabled,
fieldset:disabled .btn {
    pointer-events: none;
    opacity: .65
}

.btn-primary {
    color: #fff;
    background-color: #fd0d0d;
    border-color: #fd0d0d
}

.btn-primary:hover {
    color: #fff;
    background-color: #0b5ed7;
    border-color: #0a58ca
}

.btn-check:focus+.btn-primary,
.btn-primary:focus {
    color: #fff;
    background-color: #0b5ed7;
    border-color: #0a58ca;
    box-shadow: 0 0 0 .25rem rgba(49, 132, 253, .5)
}

.btn-check:active+.btn-primary,
.btn-check:checked+.btn-primary,
.btn-primary.active,
.btn-primary:active,
.show>.btn-primary.dropdown-toggle {
    color: #fff;
    background-color: #0a58ca;
    border-color: #0a53be
}

.btn-check:active+.btn-primary:focus,
.btn-check:checked+.btn-primary:focus,
.btn-primary.active:focus,
.btn-primary:active:focus,
.show>.btn-primary.dropdown-toggle:focus {
    box-shadow: 0 0 0 .25rem rgba(49, 132, 253, .5)
}

.btn-primary.disabled,
.btn-primary:disabled {
    color: #fff;
    background-color: #fd0d0d;
    border-color: #fd0d0d
}

.btn-secondary {
    color: #fff;
    background-color: #6c757d;
    border-color: #6c757d
}

.btn-secondary:hover {
    color: #fff;
    background-color: #5c636a;
    border-color: #565e64
}

.btn-check:focus+.btn-secondary,
.btn-secondary:focus {
    color: #fff;
    background-color: #5c636a;
    border-color: #565e64;
    box-shadow: 0 0 0 .25rem rgba(130, 138, 145, .5)
}

.btn-check:active+.btn-secondary,
.btn-check:checked+.btn-secondary,
.btn-secondary.active,
.btn-secondary:active,
.show>.btn-secondary.dropdown-toggle {
    color: #fff;
    background-color: #565e64;
    border-color: #51585e
}

.btn-check:active+.btn-secondary:focus,
.btn-check:checked+.btn-secondary:focus,
.btn-secondary.active:focus,
.btn-secondary:active:focus,
.show>.btn-secondary.dropdown-toggle:focus {
    box-shadow: 0 0 0 .25rem rgba(130, 138, 145, .5)
}

.btn-secondary.disabled,
.btn-secondary:disabled {
    color: #fff;
    background-color: #6c757d;
    border-color: #6c757d
}

.btn-success {
    color: #fff;
    background-color: #198754;
    border-color: #198754
}

.btn-success:hover {
    color: #fff;
    background-color: #157347;
    border-color: #146c43
}

.btn-check:focus+.btn-success,
.btn-success:focus {
    color: #fff;
    background-color: #157347;
    border-color: #146c43;
    box-shadow: 0 0 0 .25rem rgba(60, 153, 110, .5)
}

.btn-check:active+.btn-success,
.btn-check:checked+.btn-success,
.btn-success.active,
.btn-success:active,
.show>.btn-success.dropdown-toggle {
    color: #fff;
    background-color: #146c43;
    border-color: #13653f
}

.btn-check:active+.btn-success:focus,
.btn-check:checked+.btn-success:focus,
.btn-success.active:focus,
.btn-success:active:focus,
.show>.btn-success.dropdown-toggle:focus {
    box-shadow: 0 0 0 .25rem rgba(60, 153, 110, .5)
}

.btn-success.disabled,
.btn-success:disabled {
    color: #fff;
    background-color: #198754;
    border-color: #198754
}

.btn-info {
    color: #000;
    background-color: #0dcaf0;
    border-color: #0dcaf0
}

.btn-info:hover {
    color: #000;
    background-color: #31d2f2;
    border-color: #25cff2
}

.btn-check:focus+.btn-info,
.btn-info:focus {
    color: #000;
    background-color: #31d2f2;
    border-color: #25cff2;
    box-shadow: 0 0 0 .25rem rgba(11, 172, 204, .5)
}

.btn-check:active+.btn-info,
.btn-check:checked+.btn-info,
.btn-info.active,
.btn-info:active,
.show>.btn-info.dropdown-toggle {
    color: #000;
    background-color: #3dd5f3;
    border-color: #25cff2
}

.btn-check:active+.btn-info:focus,
.btn-check:checked+.btn-info:focus,
.btn-info.active:focus,
.btn-info:active:focus,
.show>.btn-info.dropdown-toggle:focus {
    box-shadow: 0 0 0 .25rem rgba(11, 172, 204, .5)
}

.btn-info.disabled,
.btn-info:disabled {
    color: #000;
    background-color: #0dcaf0;
    border-color: #0dcaf0
}

.btn-warning {
    color: #000;
    background-color: #ffc107;
    border-color: #ffc107
}

.btn-warning:hover {
    color: #000;
    background-color: #ffca2c;
    border-color: #ffc720
}

.btn-check:focus+.btn-warning,
.btn-warning:focus {
    color: #000;
    background-color: #ffca2c;
    border-color: #ffc720;
    box-shadow: 0 0 0 .25rem rgba(217, 164, 6, .5)
}

.btn-check:active+.btn-warning,
.btn-check:checked+.btn-warning,
.btn-warning.active,
.btn-warning:active,
.show>.btn-warning.dropdown-toggle {
    color: #000;
    background-color: #ffcd39;
    border-color: #ffc720
}

.btn-check:active+.btn-warning:focus,
.btn-check:checked+.btn-warning:focus,
.btn-warning.active:focus,
.btn-warning:active:focus,
.show>.btn-warning.dropdown-toggle:focus {
    box-shadow: 0 0 0 .25rem rgba(217, 164, 6, .5)
}

.btn-warning.disabled,
.btn-warning:disabled {
    color: #000;
    background-color: #ffc107;
    border-color: #ffc107
}

.btn-danger {
    color: #fff;
    background-color: #dc3545;
    border-color: #dc3545
}

.btn-danger:hover {
    color: #fff;
    background-color: #bb2d3b;
    border-color: #b02a37
}

.btn-check:focus+.btn-danger,
.btn-danger:focus {
    color: #fff;
    background-color: #bb2d3b;
    border-color: #b02a37;
    box-shadow: 0 0 0 .25rem rgba(225, 83, 97, .5)
}

.btn-check:active+.btn-danger,
.btn-check:checked+.btn-danger,
.btn-danger.active,
.btn-danger:active,
.show>.btn-danger.dropdown-toggle {
    color: #fff;
    background-color: #b02a37;
    border-color: #a52834
}

.btn-check:active+.btn-danger:focus,
.btn-check:checked+.btn-danger:focus,
.btn-danger.active:focus,
.btn-danger:active:focus,
.show>.btn-danger.dropdown-toggle:focus {
    box-shadow: 0 0 0 .25rem rgba(225, 83, 97, .5)
}

.btn-danger.disabled,
.btn-danger:disabled {
    color: #fff;
    background-color: #dc3545;
    border-color: #dc3545
}

.btn-light {
    color: #000;
    background-color: #f8f9fa;
    border-color: #f8f9fa
}

.btn-light:hover {
    color: #000;
    background-color: #f9fafb;
    border-color: #f9fafb
}

.btn-check:focus+.btn-light,
.btn-light:focus {
    color: #000;
    background-color: #f9fafb;
    border-color: #f9fafb;
    box-shadow: 0 0 0 .25rem rgba(211, 212, 213, .5)
}

.btn-check:active+.btn-light,
.btn-check:checked+.btn-light,
.btn-light.active,
.btn-light:active,
.show>.btn-light.dropdown-toggle {
    color: #000;
    background-color: #f9fafb;
    border-color: #f9fafb
}

.btn-check:active+.btn-light:focus,
.btn-check:checked+.btn-light:focus,
.btn-light.active:focus,
.btn-light:active:focus,
.show>.btn-light.dropdown-toggle:focus {
    box-shadow: 0 0 0 .25rem rgba(211, 212, 213, .5)
}

.btn-light.disabled,
.btn-light:disabled {
    color: #000;
    background-color: #f8f9fa;
    border-color: #f8f9fa
}

.btn-dark {
    color: #fff;
    background-color: #212529;
    border-color: #212529
}

.btn-dark:hover {
    color: #fff;
    background-color: #1c1f23;
    border-color: #1a1e21
}

.btn-check:focus+.btn-dark,
.btn-dark:focus {
    color: #fff;
    background-color: #1c1f23;
    border-color: #1a1e21;
    box-shadow: 0 0 0 .25rem rgba(66, 70, 73, .5)
}

.btn-check:active+.btn-dark,
.btn-check:checked+.btn-dark,
.btn-dark.active,
.btn-dark:active,
.show>.btn-dark.dropdown-toggle {
    color: #fff;
    background-color: #1a1e21;
    border-color: #191c1f
}

.btn-check:active+.btn-dark:focus,
.btn-check:checked+.btn-dark:focus,
.btn-dark.active:focus,
.btn-dark:active:focus,
.show>.btn-dark.dropdown-toggle:focus {
    box-shadow: 0 0 0 .25rem rgba(66, 70, 73, .5)
}

.btn-dark.disabled,
.btn-dark:disabled {
    color: #fff;
    background-color: #212529;
    border-color: #212529
}

.btn-outline-primary {
    color: #fd0d0d;
    border-color: #fd0d0d
}

.btn-outline-primary:hover {
    color: #fff;
    background-color: #fd0d0d;
    border-color: #fd0d0d
}

.btn-check:focus+.btn-outline-primary,
.btn-outline-primary:focus {
    box-shadow: 0 0 0 .25rem rgba(13, 110, 253, .5)
}

.btn-check:active+.btn-outline-primary,
.btn-check:checked+.btn-outline-primary,
.btn-outline-primary.active,
.btn-outline-primary.dropdown-toggle.show,
.btn-outline-primary:active {
    color: #fff;
    background-color: #fd0d0d;
    border-color: #fd0d0d
}

.btn-check:active+.btn-outline-primary:focus,
.btn-check:checked+.btn-outline-primary:focus,
.btn-outline-primary.active:focus,
.btn-outline-primary.dropdown-toggle.show:focus,
.btn-outline-primary:active:focus {
    box-shadow: 0 0 0 .25rem rgba(13, 110, 253, .5)
}

.btn-outline-primary.disabled,
.btn-outline-primary:disabled {
    color: #fd0d0d;
    background-color: transparent
}

.btn-outline-secondary {
    color: #6c757d;
    border-color: #6c757d
}

.btn-outline-secondary:hover {
    color: #fff;
    background-color: #6c757d;
    border-color: #6c757d
}

.btn-check:focus+.btn-outline-secondary,
.btn-outline-secondary:focus {
    box-shadow: 0 0 0 .25rem rgba(108, 117, 125, .5)
}

.btn-check:active+.btn-outline-secondary,
.btn-check:checked+.btn-outline-secondary,
.btn-outline-secondary.active,
.btn-outline-secondary.dropdown-toggle.show,
.btn-outline-secondary:active {
    color: #fff;
    background-color: #6c757d;
    border-color: #6c757d
}

.btn-check:active+.btn-outline-secondary:focus,
.btn-check:checked+.btn-outline-secondary:focus,
.btn-outline-secondary.active:focus,
.btn-outline-secondary.dropdown-toggle.show:focus,
.btn-outline-secondary:active:focus {
    box-shadow: 0 0 0 .25rem rgba(108, 117, 125, .5)
}

.btn-outline-secondary.disabled,
.btn-outline-secondary:disabled {
    color: #6c757d;
    background-color: transparent
}

.btn-outline-success {
    color: #198754;
    border-color: #198754
}

.btn-outline-success:hover {
    color: #fff;
    background-color: #198754;
    border-color: #198754
}

.btn-check:focus+.btn-outline-success,
.btn-outline-success:focus {
    box-shadow: 0 0 0 .25rem rgba(25, 135, 84, .5)
}

.btn-check:active+.btn-outline-success,
.btn-check:checked+.btn-outline-success,
.btn-outline-success.active,
.btn-outline-success.dropdown-toggle.show,
.btn-outline-success:active {
    color: #fff;
    background-color: #198754;
    border-color: #198754
}

.btn-check:active+.btn-outline-success:focus,
.btn-check:checked+.btn-outline-success:focus,
.btn-outline-success.active:focus,
.btn-outline-success.dropdown-toggle.show:focus,
.btn-outline-success:active:focus {
    box-shadow: 0 0 0 .25rem rgba(25, 135, 84, .5)
}

.btn-outline-success.disabled,
.btn-outline-success:disabled {
    color: #198754;
    background-color: transparent
}

.btn-outline-info {
    color: #0dcaf0;
    border-color: #0dcaf0
}

.btn-outline-info:hover {
    color: #000;
    background-color: #0dcaf0;
    border-color: #0dcaf0
}

.btn-check:focus+.btn-outline-info,
.btn-outline-info:focus {
    box-shadow: 0 0 0 .25rem rgba(13, 202, 240, .5)
}

.btn-check:active+.btn-outline-info,
.btn-check:checked+.btn-outline-info,
.btn-outline-info.active,
.btn-outline-info.dropdown-toggle.show,
.btn-outline-info:active {
    color: #000;
    background-color: #0dcaf0;
    border-color: #0dcaf0
}

.btn-check:active+.btn-outline-info:focus,
.btn-check:checked+.btn-outline-info:focus,
.btn-outline-info.active:focus,
.btn-outline-info.dropdown-toggle.show:focus,
.btn-outline-info:active:focus {
    box-shadow: 0 0 0 .25rem rgba(13, 202, 240, .5)
}

.btn-outline-info.disabled,
.btn-outline-info:disabled {
    color: #0dcaf0;
    background-color: transparent
}

.btn-outline-warning {
    color: #ffc107;
    border-color: #ffc107
}

.btn-outline-warning:hover {
    color: #000;
    background-color: #ffc107;
    border-color: #ffc107
}

.btn-check:focus+.btn-outline-warning,
.btn-outline-warning:focus {
    box-shadow: 0 0 0 .25rem rgba(255, 193, 7, .5)
}

.btn-check:active+.btn-outline-warning,
.btn-check:checked+.btn-outline-warning,
.btn-outline-warning.active,
.btn-outline-warning.dropdown-toggle.show,
.btn-outline-warning:active {
    color: #000;
    background-color: #ffc107;
    border-color: #ffc107
}

.btn-check:active+.btn-outline-warning:focus,
.btn-check:checked+.btn-outline-warning:focus,
.btn-outline-warning.active:focus,
.btn-outline-warning.dropdown-toggle.show:focus,
.btn-outline-warning:active:focus {
    box-shadow: 0 0 0 .25rem rgba(255, 193, 7, .5)
}

.btn-outline-warning.disabled,
.btn-outline-warning:disabled {
    color: #ffc107;
    background-color: transparent
}

.btn-outline-danger {
    color: #dc3545;
    border-color: #dc3545
}

.btn-outline-danger:hover {
    color: #fff;
    background-color: #dc3545;
    border-color: #dc3545
}

.btn-check:focus+.btn-outline-danger,
.btn-outline-danger:focus {
    box-shadow: 0 0 0 .25rem rgba(220, 53, 69, .5)
}

.btn-check:active+.btn-outline-danger,
.btn-check:checked+.btn-outline-danger,
.btn-outline-danger.active,
.btn-outline-danger.dropdown-toggle.show,
.btn-outline-danger:active {
    color: #fff;
    background-color: #dc3545;
    border-color: #dc3545
}

.btn-check:active+.btn-outline-danger:focus,
.btn-check:checked+.btn-outline-danger:focus,
.btn-outline-danger.active:focus,
.btn-outline-danger.dropdown-toggle.show:focus,
.btn-outline-danger:active:focus {
    box-shadow: 0 0 0 .25rem rgba(220, 53, 69, .5)
}

.btn-outline-danger.disabled,
.btn-outline-danger:disabled {
    color: #dc3545;
    background-color: transparent
}

.btn-outline-light {
    color: #f8f9fa;
    border-color: #f8f9fa
}

.btn-outline-light:hover {
    color: #000;
    background-color: #f8f9fa;
    border-color: #f8f9fa
}

.btn-check:focus+.btn-outline-light,
.btn-outline-light:focus {
    box-shadow: 0 0 0 .25rem rgba(248, 249, 250, .5)
}

.btn-check:active+.btn-outline-light,
.btn-check:checked+.btn-outline-light,
.btn-outline-light.active,
.btn-outline-light.dropdown-toggle.show,
.btn-outline-light:active {
    color: #000;
    background-color: #f8f9fa;
    border-color: #f8f9fa
}

.btn-check:active+.btn-outline-light:focus,
.btn-check:checked+.btn-outline-light:focus,
.btn-outline-light.active:focus,
.btn-outline-light.dropdown-toggle.show:focus,
.btn-outline-light:active:focus {
    box-shadow: 0 0 0 .25rem rgba(248, 249, 250, .5)
}

.btn-outline-light.disabled,
.btn-outline-light:disabled {
    color: #f8f9fa;
    background-color: transparent
}

.btn-outline-dark {
    color: #212529;
    border-color: #212529
}

.btn-outline-dark:hover {
    color: #fff;
    background-color: #212529;
    border-color: #212529
}

.btn-check:focus+.btn-outline-dark,
.btn-outline-dark:focus {
    box-shadow: 0 0 0 .25rem rgba(33, 37, 41, .5)
}

.btn-check:active+.btn-outline-dark,
.btn-check:checked+.btn-outline-dark,
.btn-outline-dark.active,
.btn-outline-dark.dropdown-toggle.show,
.btn-outline-dark:active {
    color: #fff;
    background-color: #212529;
    border-color: #212529
}

.btn-check:active+.btn-outline-dark:focus,
.btn-check:checked+.btn-outline-dark:focus,
.btn-outline-dark.active:focus,
.btn-outline-dark.dropdown-toggle.show:focus,
.btn-outline-dark:active:focus {
    box-shadow: 0 0 0 .25rem rgba(33, 37, 41, .5)
}

.btn-outline-dark.disabled,
.btn-outline-dark:disabled {
    color: #212529;
    background-color: transparent
}

.btn-link {
    font-weight: 400;
    color: #fd0d0d;
    text-decoration: underline
}

.btn-link:hover {
    color: #0a58ca
}

.btn-link.disabled,
.btn-link:disabled {
    color: #6c757d
}

.btn-group-lg>.btn,
.btn-lg {
    padding: .5rem 1rem;
    font-size: 1.25rem;
    border-radius: .3rem
}

.btn-group-sm>.btn,
.btn-sm {
    padding: .25rem .5rem;
    font-size: .875rem;
    border-radius: .2rem
}

.fade {
    transition: opacity .15s linear
}

@media (prefers-reduced-motion:reduce) {
    .fade {
        transition: none
    }
}

.fade:not(.show) {
    opacity: 0
}

.collapse:not(.show) {
    display: none
}

.collapsing {
    height: 0;
    overflow: hidden;
    transition: height .35s ease
}

@media (prefers-reduced-motion:reduce) {
    .collapsing {
        transition: none
    }
}

.dropdown,
.dropend,
.dropstart,
.dropup {
    position: relative
}

.dropdown-toggle {
    white-space: nowrap
}

.dropdown-toggle::after {
    display: inline-block;
    margin-left: .255em;
    vertical-align: .255em;
    content: "";
    border-top: .3em solid;
    border-right: .3em solid transparent;
    border-bottom: 0;
    border-left: .3em solid transparent
}

.dropdown-toggle:empty::after {
    margin-left: 0
}

.dropdown-menu {
    position: absolute;
    top: 100%;
    z-index: 1000;
    display: none;
    min-width: 10rem;
    padding: .5rem 0;
    margin: 0;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    list-style: none;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid rgba(0, 0, 0, .15);
    border-radius: .25rem
}

.dropdown-menu[data-bs-popper] {
    left: 0;
    margin-top: .125rem
}

.dropdown-menu-start {
    --bs-position: start
}

.dropdown-menu-start[data-bs-popper] {
    right: auto;
    left: 0
}

.dropdown-menu-end {
    --bs-position: end
}

.dropdown-menu-end[data-bs-popper] {
    right: 0;
    left: auto
}

@media (min-width:576px) {
    .dropdown-menu-sm-start {
        --bs-position: start
    }

    .dropdown-menu-sm-start[data-bs-popper] {
        right: auto;
        left: 0
    }

    .dropdown-menu-sm-end {
        --bs-position: end
    }

    .dropdown-menu-sm-end[data-bs-popper] {
        right: 0;
        left: auto
    }
}

@media (min-width:768px) {
    .dropdown-menu-md-start {
        --bs-position: start
    }

    .dropdown-menu-md-start[data-bs-popper] {
        right: auto;
        left: 0
    }

    .dropdown-menu-md-end {
        --bs-position: end
    }

    .dropdown-menu-md-end[data-bs-popper] {
        right: 0;
        left: auto
    }
}

@media (min-width:992px) {
    .dropdown-menu-lg-start {
        --bs-position: start
    }

    .dropdown-menu-lg-start[data-bs-popper] {
        right: auto;
        left: 0
    }

    .dropdown-menu-lg-end {
        --bs-position: end
    }

    .dropdown-menu-lg-end[data-bs-popper] {
        right: 0;
        left: auto
    }
}

@media (min-width:1200px) {
    .dropdown-menu-xl-start {
        --bs-position: start
    }

    .dropdown-menu-xl-start[data-bs-popper] {
        right: auto;
        left: 0
    }

    .dropdown-menu-xl-end {
        --bs-position: end
    }

    .dropdown-menu-xl-end[data-bs-popper] {
        right: 0;
        left: auto
    }
}

@media (min-width:1400px) {
    .dropdown-menu-xxl-start {
        --bs-position: start
    }

    .dropdown-menu-xxl-start[data-bs-popper] {
        right: auto;
        left: 0
    }

    .dropdown-menu-xxl-end {
        --bs-position: end
    }

    .dropdown-menu-xxl-end[data-bs-popper] {
        right: 0;
        left: auto
    }
}

.dropup .dropdown-menu {
    top: auto;
    bottom: 100%
}

.dropup .dropdown-menu[data-bs-popper] {
    margin-top: 0;
    margin-bottom: .125rem
}

.dropup .dropdown-toggle::after {
    display: inline-block;
    margin-left: .255em;
    vertical-align: .255em;
    content: "";
    border-top: 0;
    border-right: .3em solid transparent;
    border-bottom: .3em solid;
    border-left: .3em solid transparent
}

.dropup .dropdown-toggle:empty::after {
    margin-left: 0
}

.dropend .dropdown-menu {
    top: 0;
    right: auto;
    left: 100%
}

.dropend .dropdown-menu[data-bs-popper] {
    margin-top: 0;
    margin-left: .125rem
}

.dropend .dropdown-toggle::after {
    display: inline-block;
    margin-left: .255em;
    vertical-align: .255em;
    content: "";
    border-top: .3em solid transparent;
    border-right: 0;
    border-bottom: .3em solid transparent;
    border-left: .3em solid
}

.dropend .dropdown-toggle:empty::after {
    margin-left: 0
}

.dropend .dropdown-toggle::after {
    vertical-align: 0
}

.dropstart .dropdown-menu {
    top: 0;
    right: 100%;
    left: auto
}

.dropstart .dropdown-menu[data-bs-popper] {
    margin-top: 0;
    margin-right: .125rem
}

.dropstart .dropdown-toggle::after {
    display: inline-block;
    margin-left: .255em;
    vertical-align: .255em;
    content: ""
}

.dropstart .dropdown-toggle::after {
    display: none
}

.dropstart .dropdown-toggle::before {
    display: inline-block;
    margin-right: .255em;
    vertical-align: .255em;
    content: "";
    border-top: .3em solid transparent;
    border-right: .3em solid;
    border-bottom: .3em solid transparent
}

.dropstart .dropdown-toggle:empty::after {
    margin-left: 0
}

.dropstart .dropdown-toggle::before {
    vertical-align: 0
}

.dropdown-divider {
    height: 0;
    margin: .5rem 0;
    overflow: hidden;
    border-top: 1px solid rgba(0, 0, 0, .15)
}

.dropdown-item {
    display: block;
    width: 100%;
    padding: .25rem 1rem;
    clear: both;
    font-weight: 400;
    color: #212529;
    text-align: inherit;
    text-decoration: none;
    white-space: nowrap;
    background-color: transparent;
    border: 0
}

.dropdown-item:focus,
.dropdown-item:hover {
    color: #1e2125;
    background-color: #e9ecef
}

.dropdown-item.active,
.dropdown-item:active {
    color: #fff;
    text-decoration: none;
    background-color: #fd0d0d
}

.dropdown-item.disabled,
.dropdown-item:disabled {
    color: #adb5bd;
    pointer-events: none;
    background-color: transparent
}

.dropdown-menu.show {
    display: block
}

.dropdown-header {
    display: block;
    padding: .5rem 1rem;
    margin-bottom: 0;
    font-size: .875rem;
    color: #6c757d;
    white-space: nowrap
}

.dropdown-item-text {
    display: block;
    padding: .25rem 1rem;
    color: #212529
}

.dropdown-menu-dark {
    color: #dee2e6;
    background-color: #343a40;
    border-color: rgba(0, 0, 0, .15)
}

.dropdown-menu-dark .dropdown-item {
    color: #dee2e6
}

.dropdown-menu-dark .dropdown-item:focus,
.dropdown-menu-dark .dropdown-item:hover {
    color: #fff;
    background-color: rgba(255, 255, 255, .15)
}

.dropdown-menu-dark .dropdown-item.active,
.dropdown-menu-dark .dropdown-item:active {
    color: #fff;
    background-color: #fd0d0d
}

.dropdown-menu-dark .dropdown-item.disabled,
.dropdown-menu-dark .dropdown-item:disabled {
    color: #adb5bd
}

.dropdown-menu-dark .dropdown-divider {
    border-color: rgba(0, 0, 0, .15)
}

.dropdown-menu-dark .dropdown-item-text {
    color: #dee2e6
}

.dropdown-menu-dark .dropdown-header {
    color: #adb5bd
}

.btn-group,
.btn-group-vertical {
    position: relative;
    display: inline-flex;
    vertical-align: middle
}

.btn-group-vertical>.btn,
.btn-group>.btn {
    position: relative;
    flex: 1 1 auto
}

.btn-group-vertical>.btn-check:checked+.btn,
.btn-group-vertical>.btn-check:focus+.btn,
.btn-group-vertical>.btn.active,
.btn-group-vertical>.btn:active,
.btn-group-vertical>.btn:focus,
.btn-group-vertical>.btn:hover,
.btn-group>.btn-check:checked+.btn,
.btn-group>.btn-check:focus+.btn,
.btn-group>.btn.active,
.btn-group>.btn:active,
.btn-group>.btn:focus,
.btn-group>.btn:hover {
    z-index: 1
}

.btn-toolbar {
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start
}

.btn-toolbar .input-group {
    width: auto
}

.btn-group>.btn-group:not(:first-child),
.btn-group>.btn:not(:first-child) {
    margin-left: -1px
}

.btn-group>.btn-group:not(:last-child)>.btn,
.btn-group>.btn:not(:last-child):not(.dropdown-toggle) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0
}

.btn-group>.btn-group:not(:first-child)>.btn,
.btn-group>.btn:nth-child(n+3),
.btn-group>:not(.btn-check)+.btn {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0
}

.dropdown-toggle-split {
    padding-right: .5625rem;
    padding-left: .5625rem
}

.dropdown-toggle-split::after,
.dropend .dropdown-toggle-split::after,
.dropup .dropdown-toggle-split::after {
    margin-left: 0
}

.dropstart .dropdown-toggle-split::before {
    margin-right: 0
}

.btn-group-sm>.btn+.dropdown-toggle-split,
.btn-sm+.dropdown-toggle-split {
    padding-right: .375rem;
    padding-left: .375rem
}

.btn-group-lg>.btn+.dropdown-toggle-split,
.btn-lg+.dropdown-toggle-split {
    padding-right: .75rem;
    padding-left: .75rem
}

.btn-group-vertical {
    flex-direction: column;
    align-items: flex-start;
    justify-content: center
}

.btn-group-vertical>.btn,
.btn-group-vertical>.btn-group {
    width: 100%
}

.btn-group-vertical>.btn-group:not(:first-child),
.btn-group-vertical>.btn:not(:first-child) {
    margin-top: -1px
}

.btn-group-vertical>.btn-group:not(:last-child)>.btn,
.btn-group-vertical>.btn:not(:last-child):not(.dropdown-toggle) {
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0
}

.btn-group-vertical>.btn-group:not(:first-child)>.btn,
.btn-group-vertical>.btn~.btn {
    border-top-left-radius: 0;
    border-top-right-radius: 0
}

.nav {
    display: flex;
    flex-wrap: wrap;
    padding-left: 0;
    margin-bottom: 0;
    list-style: none
}

.nav-link {
    display: block;
    padding: .5rem 1rem;
    text-decoration: none;
    transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out
}

@media (prefers-reduced-motion:reduce) {
    .nav-link {
        transition: none
    }
}

.nav-link.disabled {
    color: #6c757d;
    pointer-events: none;
    cursor: default
}

.nav-tabs {
    border-bottom: 1px solid #dee2e6
}

.nav-tabs .nav-link {
    margin-bottom: -1px;
    background: 0 0;
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem
}

.nav-tabs .nav-link:focus,
.nav-tabs .nav-link:hover {
    border-color: #e9ecef #e9ecef #dee2e6;
    isolation: isolate
}

.nav-tabs .nav-link.disabled {
    color: #6c757d;
    background-color: transparent;
    border-color: transparent
}

.nav-tabs .nav-item.show .nav-link,
.nav-tabs .nav-link.active {
    color: #495057;
    background-color: #fff;
    border-color: #dee2e6 #dee2e6 #fff
}

.nav-tabs .dropdown-menu {
    margin-top: -1px;
    border-top-left-radius: 0;
    border-top-right-radius: 0
}

.nav-pills .nav-link {
    background: 0 0;
    border: 0;
    border-radius: .25rem
}

.nav-pills .nav-link.active,
.nav-pills .show>.nav-link {
    color: #fff;
    background-color: #fd0d0d
}

.nav-fill .nav-item,
.nav-fill>.nav-link {
    flex: 1 1 auto;
    text-align: center
}

.nav-justified .nav-item,
.nav-justified>.nav-link {
    flex-basis: 0;
    flex-grow: 1;
    text-align: center
}

.tab-content>.tab-pane {
    display: none
}

.tab-content>.active {
    display: block
}

.navbar {
    position: relative;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: space-between;
    padding-top: .5rem;
    padding-bottom: .5rem
}

.navbar>.container,
.navbar>.container-fluid,
.navbar>.container-lg,
.navbar>.container-md,
.navbar>.container-sm,
.navbar>.container-xl,
.navbar>.container-xxl {
    display: flex;
    flex-wrap: inherit;
    align-items: center;
    justify-content: space-between
}

.navbar-brand {
    padding-top: .3125rem;
    padding-bottom: .3125rem;
    margin-right: 1rem;
    font-size: 1.25rem;
    text-decoration: none;
    white-space: nowrap
}

.navbar-nav {
    display: flex;
    flex-direction: column;
    padding-left: 0;
    margin-bottom: 0;
    list-style: none
}

.navbar-nav .nav-link {
    padding-right: 0;
    padding-left: 0
}

.navbar-nav .dropdown-menu {
    position: static
}

.navbar-text {
    padding-top: .5rem;
    padding-bottom: .5rem
}

.navbar-collapse {
    flex-basis: 100%;
    flex-grow: 1;
    align-items: center
}

.navbar-toggler {
    padding: .25rem .75rem;
    font-size: 1.25rem;
    line-height: 1;
    background-color: transparent;
    border: 1px solid transparent;
    border-radius: .25rem;
    transition: box-shadow .15s ease-in-out
}

@media (prefers-reduced-motion:reduce) {
    .navbar-toggler {
        transition: none
    }
}

.navbar-toggler:hover {
    text-decoration: none
}

.navbar-toggler:focus {
    text-decoration: none;
    outline: 0;
    box-shadow: 0 0 0 .25rem
}

.navbar-toggler-icon {
    display: inline-block;
    width: 1.5em;
    height: 1.5em;
    vertical-align: middle;
    background-repeat: no-repeat;
    background-position: center;
    background-size: 100%
}

.navbar-nav-scroll {
    max-height: var(--bs-scroll-height, 75vh);
    overflow-y: auto
}

@media (min-width:576px) {
    .navbar-expand-sm {
        flex-wrap: nowrap;
        justify-content: flex-start
    }

    .navbar-expand-sm .navbar-nav {
        flex-direction: row
    }

    .navbar-expand-sm .navbar-nav .dropdown-menu {
        position: absolute
    }

    .navbar-expand-sm .navbar-nav .nav-link {
        padding-right: .5rem;
        padding-left: .5rem
    }

    .navbar-expand-sm .navbar-nav-scroll {
        overflow: visible
    }

    .navbar-expand-sm .navbar-collapse {
        display: flex !important;
        flex-basis: auto
    }

    .navbar-expand-sm .navbar-toggler {
        display: none
    }
}

@media (min-width:768px) {
    .navbar-expand-md {
        flex-wrap: nowrap;
        justify-content: flex-start
    }

    .navbar-expand-md .navbar-nav {
        flex-direction: row
    }

    .navbar-expand-md .navbar-nav .dropdown-menu {
        position: absolute
    }

    .navbar-expand-md .navbar-nav .nav-link {
        padding-right: .5rem;
        padding-left: .5rem
    }

    .navbar-expand-md .navbar-nav-scroll {
        overflow: visible
    }

    .navbar-expand-md .navbar-collapse {
        display: flex !important;
        flex-basis: auto
    }

    .navbar-expand-md .navbar-toggler {
        display: none
    }
}

@media (min-width:992px) {
    .navbar-expand-lg {
        flex-wrap: nowrap;
        justify-content: flex-start
    }

    .navbar-expand-lg .navbar-nav {
        flex-direction: row
    }

    .navbar-expand-lg .navbar-nav .dropdown-menu {
        position: absolute
    }

    .navbar-expand-lg .navbar-nav .nav-link {
        padding-right: .5rem;
        padding-left: .5rem
    }

    .navbar-expand-lg .navbar-nav-scroll {
        overflow: visible
    }

    .navbar-expand-lg .navbar-collapse {
        display: flex !important;
        flex-basis: auto
    }

    .navbar-expand-lg .navbar-toggler {
        display: none
    }
}

@media (min-width:1200px) {
    .navbar-expand-xl {
        flex-wrap: nowrap;
        justify-content: flex-start
    }

    .navbar-expand-xl .navbar-nav {
        flex-direction: row
    }

    .navbar-expand-xl .navbar-nav .dropdown-menu {
        position: absolute
    }

    .navbar-expand-xl .navbar-nav .nav-link {
        padding-right: .5rem;
        padding-left: .5rem
    }

    .navbar-expand-xl .navbar-nav-scroll {
        overflow: visible
    }

    .navbar-expand-xl .navbar-collapse {
        display: flex !important;
        flex-basis: auto
    }

    .navbar-expand-xl .navbar-toggler {
        display: none
    }
}

@media (min-width:1400px) {
    .navbar-expand-xxl {
        flex-wrap: nowrap;
        justify-content: flex-start
    }

    .navbar-expand-xxl .navbar-nav {
        flex-direction: row
    }

    .navbar-expand-xxl .navbar-nav .dropdown-menu {
        position: absolute
    }

    .navbar-expand-xxl .navbar-nav .nav-link {
        padding-right: .5rem;
        padding-left: .5rem
    }

    .navbar-expand-xxl .navbar-nav-scroll {
        overflow: visible
    }

    .navbar-expand-xxl .navbar-collapse {
        display: flex !important;
        flex-basis: auto
    }

    .navbar-expand-xxl .navbar-toggler {
        display: none
    }
}

.navbar-expand {
    flex-wrap: nowrap;
    justify-content: flex-start
}

.navbar-expand .navbar-nav {
    flex-direction: row
}

.navbar-expand .navbar-nav .dropdown-menu {
    position: absolute
}

.navbar-expand .navbar-nav .nav-link {
    padding-right: .5rem;
    padding-left: .5rem
}

.navbar-expand .navbar-nav-scroll {
    overflow: visible
}

.navbar-expand .navbar-collapse {
    display: flex !important;
    flex-basis: auto
}

.navbar-expand .navbar-toggler {
    display: none
}

.navbar-light .navbar-brand {
    color: rgba(0, 0, 0, .9)
}

.navbar-light .navbar-brand:focus,
.navbar-light .navbar-brand:hover {
    color: rgba(0, 0, 0, .9)
}

.navbar-light .navbar-nav .nav-link {
    color: rgba(0, 0, 0, .55)
}

.navbar-light .navbar-nav .nav-link:focus,
.navbar-light .navbar-nav .nav-link:hover {
    color: rgba(0, 0, 0, .7)
}

.navbar-light .navbar-nav .nav-link.disabled {
    color: rgba(0, 0, 0, .3)
}

.navbar-light .navbar-nav .nav-link.active,
.navbar-light .navbar-nav .show>.nav-link {
    color: rgba(0, 0, 0, .9)
}

.navbar-light .navbar-toggler {
    color: rgba(0, 0, 0, .55);
    border-color: rgba(0, 0, 0, .1)
}

.navbar-light .navbar-toggler-icon {
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 30 30'%3e%3cpath stroke='rgba%280, 0, 0, 0.55%29' stroke-linecap='round' stroke-miterlimit='10' stroke-width='2' d='M4 7h22M4 15h22M4 23h22'/%3e%3c/svg%3e")
}

.navbar-light .navbar-text {
    color: rgba(0, 0, 0, .55)
}

.navbar-light .navbar-text a,
.navbar-light .navbar-text a:focus,
.navbar-light .navbar-text a:hover {
    color: rgba(0, 0, 0, .9)
}

.navbar-dark .navbar-brand {
    color: #fff
}

.navbar-dark .navbar-brand:focus,
.navbar-dark .navbar-brand:hover {
    color: #fff
}

.navbar-dark .navbar-nav .nav-link {
    color: rgba(255, 255, 255, .55)
}

.navbar-dark .navbar-nav .nav-link:focus,
.navbar-dark .navbar-nav .nav-link:hover {
    color: rgba(255, 255, 255, .75)
}

.navbar-dark .navbar-nav .nav-link.disabled {
    color: rgba(255, 255, 255, .25)
}

.navbar-dark .navbar-nav .nav-link.active,
.navbar-dark .navbar-nav .show>.nav-link {
    color: #fff
}

.navbar-dark .navbar-toggler {
    color: rgba(255, 255, 255, .55);
    border-color: rgba(255, 255, 255, .1)
}

.navbar-dark .navbar-toggler-icon {
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 30 30'%3e%3cpath stroke='rgba%28255, 255, 255, 0.55%29' stroke-linecap='round' stroke-miterlimit='10' stroke-width='2' d='M4 7h22M4 15h22M4 23h22'/%3e%3c/svg%3e")
}

.navbar-dark .navbar-text {
    color: rgba(255, 255, 255, .55)
}

.navbar-dark .navbar-text a,
.navbar-dark .navbar-text a:focus,
.navbar-dark .navbar-text a:hover {
    color: #fff
}

.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0, 0, 0, .125);
    border-radius: .25rem
}

.card>hr {
    margin-right: 0;
    margin-left: 0
}

.card>.list-group {
    border-top: inherit;
    border-bottom: inherit
}

.card>.list-group:first-child {
    border-top-width: 0;
    border-top-left-radius: calc(.25rem - 1px);
    border-top-right-radius: calc(.25rem - 1px)
}

.card>.list-group:last-child {
    border-bottom-width: 0;
    border-bottom-right-radius: calc(.25rem - 1px);
    border-bottom-left-radius: calc(.25rem - 1px)
}

.card>.card-header+.list-group,
.card>.list-group+.card-footer {
    border-top: 0
}

.card-body {
    flex: 1 1 auto;
    padding: 1rem 1rem
}

.card-title {
    margin-bottom: .5rem
}

.card-subtitle {
    margin-top: -.25rem;
    margin-bottom: 0
}

.card-text:last-child {
    margin-bottom: 0
}

.card-link:hover {
    text-decoration: none
}

.card-link+.card-link {
    margin-left: 1rem
}

.card-header {
    padding: .5rem 1rem;
    margin-bottom: 0;
    background-color: rgba(0, 0, 0, .03);
    border-bottom: 1px solid rgba(0, 0, 0, .125)
}

.card-header:first-child {
    border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0
}

.card-footer {
    padding: .5rem 1rem;
    background-color: rgba(0, 0, 0, .03);
    border-top: 1px solid rgba(0, 0, 0, .125)
}

.card-footer:last-child {
    border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px)
}

.card-header-tabs {
    margin-right: -.5rem;
    margin-bottom: -.5rem;
    margin-left: -.5rem;
    border-bottom: 0
}

.card-header-pills {
    margin-right: -.5rem;
    margin-left: -.5rem
}

.card-img-overlay {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    padding: 1rem;
    border-radius: calc(.25rem - 1px)
}

.card-img,
.card-img-bottom,
.card-img-top {
    width: 100%
}

.card-img,
.card-img-top {
    border-top-left-radius: calc(.25rem - 1px);
    border-top-right-radius: calc(.25rem - 1px)
}

.card-img,
.card-img-bottom {
    border-bottom-right-radius: calc(.25rem - 1px);
    border-bottom-left-radius: calc(.25rem - 1px)
}

.card-group>.card {
    margin-bottom: .75rem
}

@media (min-width:576px) {
    .card-group {
        display: flex;
        flex-flow: row wrap
    }

    .card-group>.card {
        flex: 1 0 0%;
        margin-bottom: 0
    }

    .card-group>.card+.card {
        margin-left: 0;
        border-left: 0
    }

    .card-group>.card:not(:last-child) {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0
    }

    .card-group>.card:not(:last-child) .card-header,
    .card-group>.card:not(:last-child) .card-img-top {
        border-top-right-radius: 0
    }

    .card-group>.card:not(:last-child) .card-footer,
    .card-group>.card:not(:last-child) .card-img-bottom {
        border-bottom-right-radius: 0
    }

    .card-group>.card:not(:first-child) {
        border-top-left-radius: 0;
        border-bottom-left-radius: 0
    }

    .card-group>.card:not(:first-child) .card-header,
    .card-group>.card:not(:first-child) .card-img-top {
        border-top-left-radius: 0
    }

    .card-group>.card:not(:first-child) .card-footer,
    .card-group>.card:not(:first-child) .card-img-bottom {
        border-bottom-left-radius: 0
    }
}

.accordion-button {
    position: relative;
    display: flex;
    align-items: center;
    width: 100%;
    padding: 1rem 1.25rem;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    background-color: transparent;
    border: 1px solid rgba(0, 0, 0, .125);
    border-radius: 0;
    overflow-anchor: none;
    transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out, border-radius .15s ease
}

@media (prefers-reduced-motion:reduce) {
    .accordion-button {
        transition: none
    }
}

.accordion-button.collapsed {
    border-bottom-width: 0
}

.accordion-button:not(.collapsed) {
    color: #0c63e4;
    background-color: #e7f1ff
}

.accordion-button:not(.collapsed)::after {
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%230c63e4'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e");
    transform: rotate(180deg)
}

.accordion-button::after {
    flex-shrink: 0;
    width: 1.25rem;
    height: 1.25rem;
    margin-left: auto;
    content: "";
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23212529'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e");
    background-repeat: no-repeat;
    background-size: 1.25rem;
    transition: transform .2s ease-in-out
}

@media (prefers-reduced-motion:reduce) {
    .accordion-button::after {
        transition: none
    }
}

.accordion-button:hover {
    z-index: 2
}

.accordion-button:focus {
    z-index: 3;
    border-color: #86b7fe;
    outline: 0;
    box-shadow: 0 0 0 .25rem rgba(13, 110, 253, .25)
}

.accordion-header {
    margin-bottom: 0
}

.accordion-item:first-of-type .accordion-button {
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem
}

.accordion-item:last-of-type .accordion-button.collapsed {
    border-bottom-width: 1px;
    border-bottom-right-radius: .25rem;
    border-bottom-left-radius: .25rem
}

.accordion-item:last-of-type .accordion-collapse {
    border-bottom-width: 1px;
    border-bottom-right-radius: .25rem;
    border-bottom-left-radius: .25rem
}

.accordion-collapse {
    border: solid rgba(0, 0, 0, .125);
    border-width: 0 1px
}

.accordion-body {
    padding: 1rem 1.25rem
}

.accordion-flush .accordion-button {
    border-right: 0;
    border-left: 0;
    border-radius: 0
}

.accordion-flush .accordion-collapse {
    border-width: 0
}

.accordion-flush .accordion-item:first-of-type .accordion-button {
    border-top-width: 0;
    border-top-left-radius: 0;
    border-top-right-radius: 0
}

.accordion-flush .accordion-item:last-of-type .accordion-button.collapsed {
    border-bottom-width: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0
}

.breadcrumb {
    display: flex;
    flex-wrap: wrap;
    padding: 0 0;
    margin-bottom: 1rem;
    list-style: none
}

.breadcrumb-item+.breadcrumb-item {
    padding-left: .5rem
}

.breadcrumb-item+.breadcrumb-item::before {
    float: left;
    padding-right: .5rem;
    color: #6c757d;
    content: var(--bs-breadcrumb-divider, "/")
}

.breadcrumb-item.active {
    color: #6c757d
}

.pagination {
    display: flex;
    padding-left: 0;
    list-style: none
}

.page-link {
    position: relative;
    display: block;
    color: #fd0d0d;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #dee2e6;
    transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out
}

@media (prefers-reduced-motion:reduce) {
    .page-link {
        transition: none
    }
}

.page-link:hover {
    z-index: 2;
    color: #0a58ca;
    background-color: #e9ecef;
    border-color: #dee2e6
}

.page-link:focus {
    z-index: 3;
    color: #0a58ca;
    background-color: #e9ecef;
    outline: 0;
    box-shadow: 0 0 0 .25rem rgba(13, 110, 253, .25)
}

.page-item:not(:first-child) .page-link {
    margin-left: -1px
}

.page-item.active .page-link {
    z-index: 3;
    color: #fff;
    background-color: #fd0d0d;
    border-color: #fd0d0d
}

.page-item.disabled .page-link {
    color: #6c757d;
    pointer-events: none;
    background-color: #fff;
    border-color: #dee2e6
}

.page-link {
    padding: .375rem .75rem
}

.page-item:first-child .page-link {
    border-top-left-radius: .25rem;
    border-bottom-left-radius: .25rem
}

.page-item:last-child .page-link {
    border-top-right-radius: .25rem;
    border-bottom-right-radius: .25rem
}

.pagination-lg .page-link {
    padding: .75rem 1.5rem;
    font-size: 1.25rem
}

.pagination-lg .page-item:first-child .page-link {
    border-top-left-radius: .3rem;
    border-bottom-left-radius: .3rem
}

.pagination-lg .page-item:last-child .page-link {
    border-top-right-radius: .3rem;
    border-bottom-right-radius: .3rem
}

.pagination-sm .page-link {
    padding: .25rem .5rem;
    font-size: .875rem
}

.pagination-sm .page-item:first-child .page-link {
    border-top-left-radius: .2rem;
    border-bottom-left-radius: .2rem
}

.pagination-sm .page-item:last-child .page-link {
    border-top-right-radius: .2rem;
    border-bottom-right-radius: .2rem
}

.badge {
    display: inline-block;
    padding: .35em .65em;
    font-size: .75em;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25rem
}

.badge:empty {
    display: none
}

.btn .badge {
    position: relative;
    top: -1px
}

.alert {
    position: relative;
    padding: 1rem 1rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: .25rem
}

.alert-heading {
    color: inherit
}

.alert-link {
    font-weight: 700
}

.alert-dismissible {
    padding-right: 3rem
}

.alert-dismissible .btn-close {
    position: absolute;
    top: 0;
    right: 0;
    z-index: 2;
    padding: 1.25rem 1rem
}

.alert-primary {
    color: #084298;
    background-color: #cfe2ff;
    border-color: #b6d4fe
}

.alert-primary .alert-link {
    color: #06357a
}

.alert-secondary {
    color: #41464b;
    background-color: #e2e3e5;
    border-color: #d3d6d8
}

.alert-secondary .alert-link {
    color: #34383c
}

.alert-success {
    color: #0f5132;
    background-color: #d1e7dd;
    border-color: #badbcc
}

.alert-success .alert-link {
    color: #0c4128
}

.alert-info {
    color: #055160;
    background-color: #cff4fc;
    border-color: #b6effb
}

.alert-info .alert-link {
    color: #04414d
}

.alert-warning {
    color: #664d03;
    background-color: #fff3cd;
    border-color: #ffecb5
}

.alert-warning .alert-link {
    color: #523e02
}

.alert-danger {
    color: #842029;
    background-color: #f8d7da;
    border-color: #f5c2c7
}

.alert-danger .alert-link {
    color: #6a1a21
}

.alert-light {
    color: #636464;
    background-color: #fefefe;
    border-color: #fdfdfe
}

.alert-light .alert-link {
    color: #4f5050
}

.alert-dark {
    color: #141619;
    background-color: #d3d3d4;
    border-color: #bcbebf
}

.alert-dark .alert-link {
    color: #101214
}

@-webkit-keyframes progress-bar-stripes {
    0% {
        background-position-x: 1rem
    }
}

@keyframes progress-bar-stripes {
    0% {
        background-position-x: 1rem
    }
}

.progress {
    display: flex;
    height: 1rem;
    overflow: hidden;
    font-size: .75rem;
    background-color: #e9ecef;
    border-radius: .25rem
}

.progress-bar {
    display: flex;
    flex-direction: column;
    justify-content: center;
    overflow: hidden;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    background-color: #fd0d0d;
    transition: width .6s ease
}

@media (prefers-reduced-motion:reduce) {
    .progress-bar {
        transition: none
    }
}

.progress-bar-striped {
    background-image: linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
    background-size: 1rem 1rem
}

.progress-bar-animated {
    -webkit-animation: 1s linear infinite progress-bar-stripes;
    animation: 1s linear infinite progress-bar-stripes
}

@media (prefers-reduced-motion:reduce) {
    .progress-bar-animated {
        -webkit-animation: none;
        animation: none
    }
}

.list-group {
    display: flex;
    flex-direction: column;
    padding-left: 0;
    margin-bottom: 0;
    border-radius: .25rem
}

.list-group-item-action {
    width: 100%;
    color: #495057;
    text-align: inherit
}

.list-group-item-action:focus,
.list-group-item-action:hover {
    z-index: 1;
    color: #495057;
    text-decoration: none;
    background-color: #f8f9fa
}

.list-group-item-action:active {
    color: #212529;
    background-color: #e9ecef
}

.list-group-item {
    position: relative;
    display: block;
    padding: .5rem 1rem;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, .125)
}

.list-group-item:first-child {
    border-top-left-radius: inherit;
    border-top-right-radius: inherit
}

.list-group-item:last-child {
    border-bottom-right-radius: inherit;
    border-bottom-left-radius: inherit
}

.list-group-item.disabled,
.list-group-item:disabled {
    color: #6c757d;
    pointer-events: none;
    background-color: #fff
}

.list-group-item.active {
    z-index: 2;
    color: #fff;
    background-color: #fd0d0d;
    border-color: #fd0d0d
}

.list-group-item+.list-group-item {
    border-top-width: 0
}

.list-group-item+.list-group-item.active {
    margin-top: -1px;
    border-top-width: 1px
}

.list-group-horizontal {
    flex-direction: row
}

.list-group-horizontal>.list-group-item:first-child {
    border-bottom-left-radius: .25rem;
    border-top-right-radius: 0
}

.list-group-horizontal>.list-group-item:last-child {
    border-top-right-radius: .25rem;
    border-bottom-left-radius: 0
}

.list-group-horizontal>.list-group-item.active {
    margin-top: 0
}

.list-group-horizontal>.list-group-item+.list-group-item {
    border-top-width: 1px;
    border-left-width: 0
}

.list-group-horizontal>.list-group-item+.list-group-item.active {
    margin-left: -1px;
    border-left-width: 1px
}

@media (min-width:576px) {
    .list-group-horizontal-sm {
        flex-direction: row
    }

    .list-group-horizontal-sm>.list-group-item:first-child {
        border-bottom-left-radius: .25rem;
        border-top-right-radius: 0
    }

    .list-group-horizontal-sm>.list-group-item:last-child {
        border-top-right-radius: .25rem;
        border-bottom-left-radius: 0
    }

    .list-group-horizontal-sm>.list-group-item.active {
        margin-top: 0
    }

    .list-group-horizontal-sm>.list-group-item+.list-group-item {
        border-top-width: 1px;
        border-left-width: 0
    }

    .list-group-horizontal-sm>.list-group-item+.list-group-item.active {
        margin-left: -1px;
        border-left-width: 1px
    }
}

@media (min-width:768px) {
    .list-group-horizontal-md {
        flex-direction: row
    }

    .list-group-horizontal-md>.list-group-item:first-child {
        border-bottom-left-radius: .25rem;
        border-top-right-radius: 0
    }

    .list-group-horizontal-md>.list-group-item:last-child {
        border-top-right-radius: .25rem;
        border-bottom-left-radius: 0
    }

    .list-group-horizontal-md>.list-group-item.active {
        margin-top: 0
    }

    .list-group-horizontal-md>.list-group-item+.list-group-item {
        border-top-width: 1px;
        border-left-width: 0
    }

    .list-group-horizontal-md>.list-group-item+.list-group-item.active {
        margin-left: -1px;
        border-left-width: 1px
    }
}

@media (min-width:992px) {
    .list-group-horizontal-lg {
        flex-direction: row
    }

    .list-group-horizontal-lg>.list-group-item:first-child {
        border-bottom-left-radius: .25rem;
        border-top-right-radius: 0
    }

    .list-group-horizontal-lg>.list-group-item:last-child {
        border-top-right-radius: .25rem;
        border-bottom-left-radius: 0
    }

    .list-group-horizontal-lg>.list-group-item.active {
        margin-top: 0
    }

    .list-group-horizontal-lg>.list-group-item+.list-group-item {
        border-top-width: 1px;
        border-left-width: 0
    }

    .list-group-horizontal-lg>.list-group-item+.list-group-item.active {
        margin-left: -1px;
        border-left-width: 1px
    }
}

@media (min-width:1200px) {
    .list-group-horizontal-xl {
        flex-direction: row
    }

    .list-group-horizontal-xl>.list-group-item:first-child {
        border-bottom-left-radius: .25rem;
        border-top-right-radius: 0
    }

    .list-group-horizontal-xl>.list-group-item:last-child {
        border-top-right-radius: .25rem;
        border-bottom-left-radius: 0
    }

    .list-group-horizontal-xl>.list-group-item.active {
        margin-top: 0
    }

    .list-group-horizontal-xl>.list-group-item+.list-group-item {
        border-top-width: 1px;
        border-left-width: 0
    }

    .list-group-horizontal-xl>.list-group-item+.list-group-item.active {
        margin-left: -1px;
        border-left-width: 1px
    }
}

@media (min-width:1400px) {
    .list-group-horizontal-xxl {
        flex-direction: row
    }

    .list-group-horizontal-xxl>.list-group-item:first-child {
        border-bottom-left-radius: .25rem;
        border-top-right-radius: 0
    }

    .list-group-horizontal-xxl>.list-group-item:last-child {
        border-top-right-radius: .25rem;
        border-bottom-left-radius: 0
    }

    .list-group-horizontal-xxl>.list-group-item.active {
        margin-top: 0
    }

    .list-group-horizontal-xxl>.list-group-item+.list-group-item {
        border-top-width: 1px;
        border-left-width: 0
    }

    .list-group-horizontal-xxl>.list-group-item+.list-group-item.active {
        margin-left: -1px;
        border-left-width: 1px
    }
}

.list-group-flush {
    border-radius: 0
}

.list-group-flush>.list-group-item {
    border-width: 0 0 1px
}

.list-group-flush>.list-group-item:last-child {
    border-bottom-width: 0
}

.list-group-item-primary {
    color: #084298;
    background-color: #cfe2ff
}

.list-group-item-primary.list-group-item-action:focus,
.list-group-item-primary.list-group-item-action:hover {
    color: #084298;
    background-color: #bacbe6
}

.list-group-item-primary.list-group-item-action.active {
    color: #fff;
    background-color: #084298;
    border-color: #084298
}

.list-group-item-secondary {
    color: #41464b;
    background-color: #e2e3e5
}

.list-group-item-secondary.list-group-item-action:focus,
.list-group-item-secondary.list-group-item-action:hover {
    color: #41464b;
    background-color: #cbccce
}

.list-group-item-secondary.list-group-item-action.active {
    color: #fff;
    background-color: #41464b;
    border-color: #41464b
}

.list-group-item-success {
    color: #0f5132;
    background-color: #d1e7dd
}

.list-group-item-success.list-group-item-action:focus,
.list-group-item-success.list-group-item-action:hover {
    color: #0f5132;
    background-color: #bcd0c7
}

.list-group-item-success.list-group-item-action.active {
    color: #fff;
    background-color: #0f5132;
    border-color: #0f5132
}

.list-group-item-info {
    color: #055160;
    background-color: #cff4fc
}

.list-group-item-info.list-group-item-action:focus,
.list-group-item-info.list-group-item-action:hover {
    color: #055160;
    background-color: #badce3
}

.list-group-item-info.list-group-item-action.active {
    color: #fff;
    background-color: #055160;
    border-color: #055160
}

.list-group-item-warning {
    color: #664d03;
    background-color: #fff3cd
}

.list-group-item-warning.list-group-item-action:focus,
.list-group-item-warning.list-group-item-action:hover {
    color: #664d03;
    background-color: #e6dbb9
}

.list-group-item-warning.list-group-item-action.active {
    color: #fff;
    background-color: #664d03;
    border-color: #664d03
}

.list-group-item-danger {
    color: #842029;
    background-color: #f8d7da
}

.list-group-item-danger.list-group-item-action:focus,
.list-group-item-danger.list-group-item-action:hover {
    color: #842029;
    background-color: #dfc2c4
}

.list-group-item-danger.list-group-item-action.active {
    color: #fff;
    background-color: #842029;
    border-color: #842029
}

.list-group-item-light {
    color: #636464;
    background-color: #fefefe
}

.list-group-item-light.list-group-item-action:focus,
.list-group-item-light.list-group-item-action:hover {
    color: #636464;
    background-color: #e5e5e5
}

.list-group-item-light.list-group-item-action.active {
    color: #fff;
    background-color: #636464;
    border-color: #636464
}

.list-group-item-dark {
    color: #141619;
    background-color: #d3d3d4
}

.list-group-item-dark.list-group-item-action:focus,
.list-group-item-dark.list-group-item-action:hover {
    color: #141619;
    background-color: #bebebf
}

.list-group-item-dark.list-group-item-action.active {
    color: #fff;
    background-color: #141619;
    border-color: #141619
}

.btn-close {
    box-sizing: content-box;
    width: 1em;
    height: 1em;
    padding: .25em .25em;
    color: #000;
    background: transparent url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23000'%3e%3cpath d='M.293.293a1 1 0 011.414 0L8 6.586 14.293.293a1 1 0 111.414 1.414L9.414 8l6.293 6.293a1 1 0 01-1.414 1.414L8 9.414l-6.293 6.293a1 1 0 01-1.414-1.414L6.586 8 .293 1.707a1 1 0 010-1.414z'/%3e%3c/svg%3e") center/1em auto no-repeat;
    border: 0;
    border-radius: .25rem;
    opacity: .5
}

.btn-close:hover {
    color: #000;
    text-decoration: none;
    opacity: .75
}

.btn-close:focus {
    outline: 0;
    box-shadow: 0 0 0 .25rem rgba(13, 110, 253, .25);
    opacity: 1
}

.btn-close.disabled,
.btn-close:disabled {
    pointer-events: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    user-select: none;
    opacity: .25
}

.btn-close-white {
    filter: invert(1) grayscale(100%) brightness(200%)
}

.toast {
    width: 350px;
    max-width: 100%;
    font-size: .875rem;
    pointer-events: auto;
    background-color: rgba(255, 255, 255, .85);
    background-clip: padding-box;
    border: 1px solid rgba(0, 0, 0, .1);
    box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15);
    border-radius: .25rem
}

.toast:not(.showing):not(.show) {
    opacity: 0
}

.toast.hide {
    display: none
}

.toast-container {
    width: -webkit-max-content;
    width: -moz-max-content;
    width: max-content;
    max-width: 100%;
    pointer-events: none
}

.toast-container>:not(:last-child) {
    margin-bottom: .75rem
}

.toast-header {
    display: flex;
    align-items: center;
    padding: .5rem .75rem;
    color: #6c757d;
    background-color: rgba(255, 255, 255, .85);
    background-clip: padding-box;
    border-bottom: 1px solid rgba(0, 0, 0, .05);
    border-top-left-radius: calc(.25rem - 1px);
    border-top-right-radius: calc(.25rem - 1px)
}

.toast-header .btn-close {
    margin-right: -.375rem;
    margin-left: .75rem
}

.toast-body {
    padding: .75rem;
    word-wrap: break-word
}

.modal-open {
    overflow: hidden
}

.modal-open .modal {
    overflow-x: hidden;
    overflow-y: auto
}

.modal {
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1050;
    display: none;
    width: 100%;
    height: 100%;
    overflow: hidden;
    outline: 0
}

.modal-dialog {
    position: relative;
    width: auto;
    margin: .5rem;
    pointer-events: none
}

.modal.fade .modal-dialog {
    transition: transform .3s ease-out;
    transform: translate(0, -50px)
}

@media (prefers-reduced-motion:reduce) {
    .modal.fade .modal-dialog {
        transition: none
    }
}

.modal.show .modal-dialog {
    transform: none
}

.modal.modal-static .modal-dialog {
    transform: scale(1.02)
}

.modal-dialog-scrollable {
    height: calc(100% - 1rem)
}

.modal-dialog-scrollable .modal-content {
    max-height: 100%;
    overflow: hidden
}

.modal-dialog-scrollable .modal-body {
    overflow-y: auto
}

.modal-dialog-centered {
    display: flex;
    align-items: center;
    min-height: calc(100% - 1rem)
}

.modal-content {
    position: relative;
    display: flex;
    flex-direction: column;
    width: 100%;
    pointer-events: auto;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid rgba(0, 0, 0, .2);
    border-radius: .3rem;
    outline: 0
}

.modal-backdrop {
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1040;
    width: 100vw;
    height: 100vh;
    background-color: #000
}

.modal-backdrop.fade {
    opacity: 0
}

.modal-backdrop.show {
    opacity: .5
}

.modal-header {
    display: flex;
    flex-shrink: 0;
    align-items: center;
    justify-content: space-between;
    padding: 1rem 1rem;
    border-bottom: 1px solid #dee2e6;
    border-top-left-radius: calc(.3rem - 1px);
    border-top-right-radius: calc(.3rem - 1px)
}

.modal-header .btn-close {
    padding: .5rem .5rem;
    margin: -.5rem -.5rem -.5rem auto
}

.modal-title {
    margin-bottom: 0;
    line-height: 1.5
}

.modal-body {
    position: relative;
    flex: 1 1 auto;
    padding: 1rem
}

.modal-footer {
    display: flex;
    flex-wrap: wrap;
    flex-shrink: 0;
    align-items: center;
    justify-content: flex-end;
    padding: .75rem;
    border-top: 1px solid #dee2e6;
    border-bottom-right-radius: calc(.3rem - 1px);
    border-bottom-left-radius: calc(.3rem - 1px)
}

.modal-footer>* {
    margin: .25rem
}

.modal-scrollbar-measure {
    position: absolute;
    top: -9999px;
    width: 50px;
    height: 50px;
    overflow: scroll
}

@media (min-width:576px) {
    .modal-dialog {
        max-width: 500px;
        margin: 1.75rem auto
    }

    .modal-dialog-scrollable {
        height: calc(100% - 3.5rem)
    }

    .modal-dialog-centered {
        min-height: calc(100% - 3.5rem)
    }

    .modal-sm {
        max-width: 300px
    }
}

@media (min-width:992px) {

    .modal-lg,
    .modal-xl {
        max-width: 800px
    }
}

@media (min-width:1200px) {
    .modal-xl {
        max-width: 1140px
    }
}

.modal-fullscreen {
    width: 100vw;
    max-width: none;
    height: 100%;
    margin: 0
}

.modal-fullscreen .modal-content {
    height: 100%;
    border: 0;
    border-radius: 0
}

.modal-fullscreen .modal-header {
    border-radius: 0
}

.modal-fullscreen .modal-body {
    overflow-y: auto
}

.modal-fullscreen .modal-footer {
    border-radius: 0
}

@media (max-width:575.98px) {
    .modal-fullscreen-sm-down {
        width: 100vw;
        max-width: none;
        height: 100%;
        margin: 0
    }

    .modal-fullscreen-sm-down .modal-content {
        height: 100%;
        border: 0;
        border-radius: 0
    }

    .modal-fullscreen-sm-down .modal-header {
        border-radius: 0
    }

    .modal-fullscreen-sm-down .modal-body {
        overflow-y: auto
    }

    .modal-fullscreen-sm-down .modal-footer {
        border-radius: 0
    }
}

@media (max-width:767.98px) {
    .modal-fullscreen-md-down {
        width: 100vw;
        max-width: none;
        height: 100%;
        margin: 0
    }

    .modal-fullscreen-md-down .modal-content {
        height: 100%;
        border: 0;
        border-radius: 0
    }

    .modal-fullscreen-md-down .modal-header {
        border-radius: 0
    }

    .modal-fullscreen-md-down .modal-body {
        overflow-y: auto
    }

    .modal-fullscreen-md-down .modal-footer {
        border-radius: 0
    }
}

@media (max-width:991.98px) {
    .modal-fullscreen-lg-down {
        width: 100vw;
        max-width: none;
        height: 100%;
        margin: 0
    }

    .modal-fullscreen-lg-down .modal-content {
        height: 100%;
        border: 0;
        border-radius: 0
    }

    .modal-fullscreen-lg-down .modal-header {
        border-radius: 0
    }

    .modal-fullscreen-lg-down .modal-body {
        overflow-y: auto
    }

    .modal-fullscreen-lg-down .modal-footer {
        border-radius: 0
    }
}

@media (max-width:1199.98px) {
    .modal-fullscreen-xl-down {
        width: 100vw;
        max-width: none;
        height: 100%;
        margin: 0
    }

    .modal-fullscreen-xl-down .modal-content {
        height: 100%;
        border: 0;
        border-radius: 0
    }

    .modal-fullscreen-xl-down .modal-header {
        border-radius: 0
    }

    .modal-fullscreen-xl-down .modal-body {
        overflow-y: auto
    }

    .modal-fullscreen-xl-down .modal-footer {
        border-radius: 0
    }
}

@media (max-width:1399.98px) {
    .modal-fullscreen-xxl-down {
        width: 100vw;
        max-width: none;
        height: 100%;
        margin: 0
    }

    .modal-fullscreen-xxl-down .modal-content {
        height: 100%;
        border: 0;
        border-radius: 0
    }

    .modal-fullscreen-xxl-down .modal-header {
        border-radius: 0
    }

    .modal-fullscreen-xxl-down .modal-body {
        overflow-y: auto
    }

    .modal-fullscreen-xxl-down .modal-footer {
        border-radius: 0
    }
}

.tooltip {
    position: absolute;
    z-index: 1070;
    display: block;
    margin: 0;
    font-family: var(--bs-font-sans-serif);
    font-style: normal;
    font-weight: 400;
    line-height: 1.5;
    text-align: left;
    text-align: start;
    text-decoration: none;
    text-shadow: none;
    text-transform: none;
    letter-spacing: normal;
    word-break: normal;
    word-spacing: normal;
    white-space: normal;
    line-break: auto;
    font-size: .875rem;
    word-wrap: break-word;
    opacity: 0
}

.tooltip.show {
    opacity: .9
}

.tooltip .tooltip-arrow {
    position: absolute;
    display: block;
    width: .8rem;
    height: .4rem
}

.tooltip .tooltip-arrow::before {
    position: absolute;
    content: "";
    border-color: transparent;
    border-style: solid
}

.bs-tooltip-auto[data-popper-placement^=top],
.bs-tooltip-top {
    padding: .4rem 0
}

.bs-tooltip-auto[data-popper-placement^=top] .tooltip-arrow,
.bs-tooltip-top .tooltip-arrow {
    bottom: 0
}

.bs-tooltip-auto[data-popper-placement^=top] .tooltip-arrow::before,
.bs-tooltip-top .tooltip-arrow::before {
    top: -1px;
    border-width: .4rem .4rem 0;
    border-top-color: #000
}

.bs-tooltip-auto[data-popper-placement^=right],
.bs-tooltip-end {
    padding: 0 .4rem
}

.bs-tooltip-auto[data-popper-placement^=right] .tooltip-arrow,
.bs-tooltip-end .tooltip-arrow {
    left: 0;
    width: .4rem;
    height: .8rem
}

.bs-tooltip-auto[data-popper-placement^=right] .tooltip-arrow::before,
.bs-tooltip-end .tooltip-arrow::before {
    right: -1px;
    border-width: .4rem .4rem .4rem 0;
    border-right-color: #000
}

.bs-tooltip-auto[data-popper-placement^=bottom],
.bs-tooltip-bottom {
    padding: .4rem 0
}

.bs-tooltip-auto[data-popper-placement^=bottom] .tooltip-arrow,
.bs-tooltip-bottom .tooltip-arrow {
    top: 0
}

.bs-tooltip-auto[data-popper-placement^=bottom] .tooltip-arrow::before,
.bs-tooltip-bottom .tooltip-arrow::before {
    bottom: -1px;
    border-width: 0 .4rem .4rem;
    border-bottom-color: #000
}

.bs-tooltip-auto[data-popper-placement^=left],
.bs-tooltip-start {
    padding: 0 .4rem
}

.bs-tooltip-auto[data-popper-placement^=left] .tooltip-arrow,
.bs-tooltip-start .tooltip-arrow {
    right: 0;
    width: .4rem;
    height: .8rem
}

.bs-tooltip-auto[data-popper-placement^=left] .tooltip-arrow::before,
.bs-tooltip-start .tooltip-arrow::before {
    left: -1px;
    border-width: .4rem 0 .4rem .4rem;
    border-left-color: #000
}

.tooltip-inner {
    max-width: 200px;
    padding: .25rem .5rem;
    color: #fff;
    text-align: center;
    background-color: #000;
    border-radius: .25rem
}

.popover {
    position: absolute;
    top: 0;
    left: 0;
    z-index: 1060;
    display: block;
    max-width: 276px;
    font-family: var(--bs-font-sans-serif);
    font-style: normal;
    font-weight: 400;
    line-height: 1.5;
    text-align: left;
    text-align: start;
    text-decoration: none;
    text-shadow: none;
    text-transform: none;
    letter-spacing: normal;
    word-break: normal;
    word-spacing: normal;
    white-space: normal;
    line-break: auto;
    font-size: .875rem;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid rgba(0, 0, 0, .2);
    border-radius: .3rem
}

.popover .popover-arrow {
    position: absolute;
    display: block;
    width: 1rem;
    height: .5rem
}

.popover .popover-arrow::after,
.popover .popover-arrow::before {
    position: absolute;
    display: block;
    content: "";
    border-color: transparent;
    border-style: solid
}

.bs-popover-auto[data-popper-placement^=top]>.popover-arrow,
.bs-popover-top>.popover-arrow {
    bottom: calc(-.5rem - 1px)
}

.bs-popover-auto[data-popper-placement^=top]>.popover-arrow::before,
.bs-popover-top>.popover-arrow::before {
    bottom: 0;
    border-width: .5rem .5rem 0;
    border-top-color: rgba(0, 0, 0, .25)
}

.bs-popover-auto[data-popper-placement^=top]>.popover-arrow::after,
.bs-popover-top>.popover-arrow::after {
    bottom: 1px;
    border-width: .5rem .5rem 0;
    border-top-color: #fff
}

.bs-popover-auto[data-popper-placement^=right]>.popover-arrow,
.bs-popover-end>.popover-arrow {
    left: calc(-.5rem - 1px);
    width: .5rem;
    height: 1rem
}

.bs-popover-auto[data-popper-placement^=right]>.popover-arrow::before,
.bs-popover-end>.popover-arrow::before {
    left: 0;
    border-width: .5rem .5rem .5rem 0;
    border-right-color: rgba(0, 0, 0, .25)
}

.bs-popover-auto[data-popper-placement^=right]>.popover-arrow::after,
.bs-popover-end>.popover-arrow::after {
    left: 1px;
    border-width: .5rem .5rem .5rem 0;
    border-right-color: #fff
}

.bs-popover-auto[data-popper-placement^=bottom]>.popover-arrow,
.bs-popover-bottom>.popover-arrow {
    top: calc(-.5rem - 1px)
}

.bs-popover-auto[data-popper-placement^=bottom]>.popover-arrow::before,
.bs-popover-bottom>.popover-arrow::before {
    top: 0;
    border-width: 0 .5rem .5rem .5rem;
    border-bottom-color: rgba(0, 0, 0, .25)
}

.bs-popover-auto[data-popper-placement^=bottom]>.popover-arrow::after,
.bs-popover-bottom>.popover-arrow::after {
    top: 1px;
    border-width: 0 .5rem .5rem .5rem;
    border-bottom-color: #fff
}

.bs-popover-auto[data-popper-placement^=bottom] .popover-header::before,
.bs-popover-bottom .popover-header::before {
    position: absolute;
    top: 0;
    left: 50%;
    display: block;
    width: 1rem;
    margin-left: -.5rem;
    content: "";
    border-bottom: 1px solid #f0f0f0
}

.bs-popover-auto[data-popper-placement^=left]>.popover-arrow,
.bs-popover-start>.popover-arrow {
    right: calc(-.5rem - 1px);
    width: .5rem;
    height: 1rem
}

.bs-popover-auto[data-popper-placement^=left]>.popover-arrow::before,
.bs-popover-start>.popover-arrow::before {
    right: 0;
    border-width: .5rem 0 .5rem .5rem;
    border-left-color: rgba(0, 0, 0, .25)
}

.bs-popover-auto[data-popper-placement^=left]>.popover-arrow::after,
.bs-popover-start>.popover-arrow::after {
    right: 1px;
    border-width: .5rem 0 .5rem .5rem;
    border-left-color: #fff
}

.popover-header {
    padding: .5rem 1rem;
    margin-bottom: 0;
    font-size: 1rem;
    background-color: #f0f0f0;
    border-bottom: 1px solid #d8d8d8;
    border-top-left-radius: calc(.3rem - 1px);
    border-top-right-radius: calc(.3rem - 1px)
}

.popover-header:empty {
    display: none
}

.popover-body {
    padding: 1rem 1rem;
    color: #212529
}

.carousel {
    position: relative
}

.carousel.pointer-event {
    touch-action: pan-y
}

.carousel-inner {
    position: relative;
    width: 100%;
    overflow: hidden
}

.carousel-inner::after {
    display: block;
    clear: both;
    content: ""
}

.carousel-item {
    position: relative;
    display: none;
    float: left;
    width: 100%;
    margin-right: -100%;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    transition: transform .6s ease-in-out
}

@media (prefers-reduced-motion:reduce) {
    .carousel-item {
        transition: none
    }
}

.carousel-item-next,
.carousel-item-prev,
.carousel-item.active {
    display: block
}

.active.carousel-item-end,
.carousel-item-next:not(.carousel-item-start) {
    transform: translateX(100%)
}

.active.carousel-item-start,
.carousel-item-prev:not(.carousel-item-end) {
    transform: translateX(-100%)
}

.carousel-fade .carousel-item {
    opacity: 0;
    transition-property: opacity;
    transform: none
}

.carousel-fade .carousel-item-next.carousel-item-start,
.carousel-fade .carousel-item-prev.carousel-item-end,
.carousel-fade .carousel-item.active {
    z-index: 1;
    opacity: 1
}

.carousel-fade .active.carousel-item-end,
.carousel-fade .active.carousel-item-start {
    z-index: 0;
    opacity: 0;
    transition: opacity 0s .6s
}

@media (prefers-reduced-motion:reduce) {

    .carousel-fade .active.carousel-item-end,
    .carousel-fade .active.carousel-item-start {
        transition: none
    }
}

.carousel-control-next,
.carousel-control-prev {
    position: absolute;
    top: 0;
    bottom: 0;
    z-index: 1;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 15%;
    padding: 0;
    color: #fff;
    text-align: center;
    background: 0 0;
    border: 0;
    opacity: .5;
    transition: opacity .15s ease
}

@media (prefers-reduced-motion:reduce) {

    .carousel-control-next,
    .carousel-control-prev {
        transition: none
    }
}

.carousel-control-next:focus,
.carousel-control-next:hover,
.carousel-control-prev:focus,
.carousel-control-prev:hover {
    color: #fff;
    text-decoration: none;
    outline: 0;
    opacity: .9
}

.carousel-control-prev {
    left: 0
}

.carousel-control-next {
    right: 0
}

.carousel-control-next-icon,
.carousel-control-prev-icon {
    display: inline-block;
    width: 2rem;
    height: 2rem;
    background-repeat: no-repeat;
    background-position: 50%;
    background-size: 100% 100%
}

.carousel-control-prev-icon {
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23fff'%3e%3cpath d='M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z'/%3e%3c/svg%3e")
}

.carousel-control-next-icon {
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23fff'%3e%3cpath d='M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e")
}

.carousel-indicators {
    position: absolute;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 2;
    display: flex;
    justify-content: center;
    padding: 0;
    margin-right: 15%;
    margin-bottom: 1rem;
    margin-left: 15%;
    list-style: none
}

.carousel-indicators [data-bs-target] {
    box-sizing: content-box;
    flex: 0 1 auto;
    width: 30px;
    height: 3px;
    padding: 0;
    margin-right: 3px;
    margin-left: 3px;
    text-indent: -999px;
    cursor: pointer;
    background-color: #fff;
    background-clip: padding-box;
    border: 0;
    border-top: 10px solid transparent;
    border-bottom: 10px solid transparent;
    opacity: .5;
    transition: opacity .6s ease
}

@media (prefers-reduced-motion:reduce) {
    .carousel-indicators [data-bs-target] {
        transition: none
    }
}

.carousel-indicators .active {
    opacity: 1
}

.carousel-caption {
    position: absolute;
    right: 15%;
    bottom: 1.25rem;
    left: 15%;
    padding-top: 1.25rem;
    padding-bottom: 1.25rem;
    color: #fff;
    text-align: center
}

.carousel-dark .carousel-control-next-icon,
.carousel-dark .carousel-control-prev-icon {
    filter: invert(1) grayscale(100)
}

.carousel-dark .carousel-indicators [data-bs-target] {
    background-color: #000
}

.carousel-dark .carousel-caption {
    color: #000
}

@-webkit-keyframes spinner-border {
    to {
        transform: rotate(360deg)
    }
}

@keyframes spinner-border {
    to {
        transform: rotate(360deg)
    }
}

.spinner-border {
    display: inline-block;
    width: 2rem;
    height: 2rem;
    vertical-align: text-bottom;
    border: .25em solid currentColor;
    border-right-color: transparent;
    border-radius: 50%;
    -webkit-animation: .75s linear infinite spinner-border;
    animation: .75s linear infinite spinner-border
}

.spinner-border-sm {
    width: 1rem;
    height: 1rem;
    border-width: .2em
}

@-webkit-keyframes spinner-grow {
    0% {
        transform: scale(0)
    }

    50% {
        opacity: 1;
        transform: none
    }
}

@keyframes spinner-grow {
    0% {
        transform: scale(0)
    }

    50% {
        opacity: 1;
        transform: none
    }
}

.spinner-grow {
    display: inline-block;
    width: 2rem;
    height: 2rem;
    vertical-align: text-bottom;
    background-color: currentColor;
    border-radius: 50%;
    opacity: 0;
    -webkit-animation: .75s linear infinite spinner-grow;
    animation: .75s linear infinite spinner-grow
}

.spinner-grow-sm {
    width: 1rem;
    height: 1rem
}

@media (prefers-reduced-motion:reduce) {

    .spinner-border,
    .spinner-grow {
        -webkit-animation-duration: 1.5s;
        animation-duration: 1.5s
    }
}

.clearfix::after {
    display: block;
    clear: both;
    content: ""
}

.link-primary {
    color: #fd0d0d
}

.link-primary:focus,
.link-primary:hover {
    color: #0a58ca
}

.link-secondary {
    color: #6c757d
}

.link-secondary:focus,
.link-secondary:hover {
    color: #565e64
}

.link-success {
    color: #198754
}

.link-success:focus,
.link-success:hover {
    color: #146c43
}

.link-info {
    color: #0dcaf0
}

.link-info:focus,
.link-info:hover {
    color: #3dd5f3
}

.link-warning {
    color: #ffc107
}

.link-warning:focus,
.link-warning:hover {
    color: #ffcd39
}

.link-danger {
    color: #dc3545
}

.link-danger:focus,
.link-danger:hover {
    color: #b02a37
}

.link-light {
    color: #f8f9fa
}

.link-light:focus,
.link-light:hover {
    color: #f9fafb
}

.link-dark {
    color: #212529
}

.link-dark:focus,
.link-dark:hover {
    color: #1a1e21
}

.ratio {
    position: relative;
    width: 100%
}

.ratio::before {
    display: block;
    padding-top: var(--bs-aspect-ratio);
    content: ""
}

.ratio>* {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%
}

.ratio-1x1 {
    --bs-aspect-ratio: 100%
}

.ratio-4x3 {
    --bs-aspect-ratio: calc(3 / 4 * 100%)
}

.ratio-16x9 {
    --bs-aspect-ratio: calc(9 / 16 * 100%)
}

.ratio-21x9 {
    --bs-aspect-ratio: calc(9 / 21 * 100%)
}

.fixed-top {
    position: fixed;
    top: 0;
    right: 0;
    left: 0;
    z-index: 1030
}

.fixed-bottom {
    position: fixed;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030
}

.sticky-top {
    position: -webkit-sticky;
    position: sticky;
    top: 0;
    z-index: 1020
}

@media (min-width:576px) {
    .sticky-sm-top {
        position: -webkit-sticky;
        position: sticky;
        top: 0;
        z-index: 1020
    }
}

@media (min-width:768px) {
    .sticky-md-top {
        position: -webkit-sticky;
        position: sticky;
        top: 0;
        z-index: 1020
    }
}

@media (min-width:992px) {
    .sticky-lg-top {
        position: -webkit-sticky;
        position: sticky;
        top: 0;
        z-index: 1020
    }
}

@media (min-width:1200px) {
    .sticky-xl-top {
        position: -webkit-sticky;
        position: sticky;
        top: 0;
        z-index: 1020
    }
}

@media (min-width:1400px) {
    .sticky-xxl-top {
        position: -webkit-sticky;
        position: sticky;
        top: 0;
        z-index: 1020
    }
}

.visually-hidden,
.visually-hidden-focusable:not(:focus):not(:focus-within) {
    position: absolute !important;
    width: 1px !important;
    height: 1px !important;
    padding: 0 !important;
    margin: -1px !important;
    overflow: hidden !important;
    clip: rect(0, 0, 0, 0) !important;
    white-space: nowrap !important;
    border: 0 !important
}

.stretched-link::after {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1;
    content: ""
}

.text-truncate {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap
}

.align-baseline {
    vertical-align: baseline !important
}

.align-top {
    vertical-align: top !important
}

.align-middle {
    vertical-align: middle !important
}

.align-bottom {
    vertical-align: bottom !important
}

.align-text-bottom {
    vertical-align: text-bottom !important
}

.align-text-top {
    vertical-align: text-top !important
}

.float-start {
    float: left !important
}

.float-end {
    float: right !important
}

.float-none {
    float: none !important
}

.overflow-auto {
    overflow: auto !important
}

.overflow-hidden {
    overflow: hidden !important
}

.overflow-visible {
    overflow: visible !important
}

.overflow-scroll {
    overflow: scroll !important
}

.d-inline {
    display: inline !important
}

.d-inline-block {
    display: inline-block !important
}

.d-block {
    display: block !important
}

.d-grid {
    display: grid !important
}

.d-table {
    display: table !important
}

.d-table-row {
    display: table-row !important
}

.d-table-cell {
    display: table-cell !important
}

.d-flex {
    display: flex !important
}

.d-inline-flex {
    display: inline-flex !important
}

.d-none {
    display: none !important
}

.shadow {
    box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15) !important
}

.shadow-sm {
    box-shadow: 0 .125rem .25rem rgba(0, 0, 0, .075) !important
}

.shadow-lg {
    box-shadow: 0 1rem 3rem rgba(0, 0, 0, .175) !important
}

.shadow-none {
    box-shadow: none !important
}

.position-static {
    position: static !important
}

.position-relative {
    position: relative !important
}

.position-absolute {
    position: absolute !important
}

.position-fixed {
    position: fixed !important
}

.position-sticky {
    position: -webkit-sticky !important;
    position: sticky !important
}

.top-0 {
    top: 0 !important
}

.top-50 {
    top: 50% !important
}

.top-100 {
    top: 100% !important
}

.bottom-0 {
    bottom: 0 !important
}

.bottom-50 {
    bottom: 50% !important
}

.bottom-100 {
    bottom: 100% !important
}

.start-0 {
    left: 0 !important
}

.start-50 {
    left: 50% !important
}

.start-100 {
    left: 100% !important
}

.end-0 {
    right: 0 !important
}

.end-50 {
    right: 50% !important
}

.end-100 {
    right: 100% !important
}

.translate-middle {
    transform: translate(-50%, -50%) !important
}

.translate-middle-x {
    transform: translateX(-50%) !important
}

.translate-middle-y {
    transform: translateY(-50%) !important
}

.border {
    border: 1px solid #dee2e6 !important
}

.border-0 {
    border: 0 !important
}

.border-top {
    border-top: 1px solid #dee2e6 !important
}

.border-top-0 {
    border-top: 0 !important
}

.border-end {
    border-right: 1px solid #dee2e6 !important
}

.border-end-0 {
    border-right: 0 !important
}

.border-bottom {
    border-bottom: 1px solid #dee2e6 !important
}

.border-bottom-0 {
    border-bottom: 0 !important
}

.border-start {
    border-left: 1px solid #dee2e6 !important
}

.border-start-0 {
    border-left: 0 !important
}

.border-primary {
    border-color: #fd0d0d !important
}

.border-secondary {
    border-color: #6c757d !important
}

.border-success {
    border-color: #198754 !important
}

.border-info {
    border-color: #0dcaf0 !important
}

.border-warning {
    border-color: #ffc107 !important
}

.border-danger {
    border-color: #dc3545 !important
}

.border-light {
    border-color: #f8f9fa !important
}

.border-dark {
    border-color: #212529 !important
}

.border-white {
    border-color: #fff !important
}

.border-0 {
    border-width: 0 !important
}

.border-1 {
    border-width: 1px !important
}

.border-2 {
    border-width: 2px !important
}

.border-3 {
    border-width: 3px !important
}

.border-4 {
    border-width: 4px !important
}

.border-5 {
    border-width: 5px !important
}

.w-25 {
    width: 25% !important
}

.w-50 {
    width: 50% !important
}

.w-75 {
    width: 75% !important
}

.w-100 {
    width: 100% !important
}

.w-auto {
    width: auto !important
}

.mw-100 {
    max-width: 100% !important
}

.vw-100 {
    width: 100vw !important
}

.min-vw-100 {
    min-width: 100vw !important
}

.h-25 {
    height: 25% !important
}

.h-50 {
    height: 50% !important
}

.h-75 {
    height: 75% !important
}

.h-100 {
    height: 100% !important
}

.h-auto {
    height: auto !important
}

.mh-100 {
    max-height: 100% !important
}

.vh-100 {
    height: 100vh !important
}

.min-vh-100 {
    min-height: 100vh !important
}

.flex-fill {
    flex: 1 1 auto !important
}

.flex-row {
    flex-direction: row !important
}

.flex-column {
    flex-direction: column !important
}

.flex-row-reverse {
    flex-direction: row-reverse !important
}

.flex-column-reverse {
    flex-direction: column-reverse !important
}

.flex-grow-0 {
    flex-grow: 0 !important
}

.flex-grow-1 {
    flex-grow: 1 !important
}

.flex-shrink-0 {
    flex-shrink: 0 !important
}

.flex-shrink-1 {
    flex-shrink: 1 !important
}

.flex-wrap {
    flex-wrap: wrap !important
}

.flex-nowrap {
    flex-wrap: nowrap !important
}

.flex-wrap-reverse {
    flex-wrap: wrap-reverse !important
}

.gap-0 {
    gap: 0 !important
}

.gap-1 {
    gap: .25rem !important
}

.gap-2 {
    gap: .5rem !important
}

.gap-3 {
    gap: 1rem !important
}

.gap-4 {
    gap: 1.5rem !important
}

.gap-5 {
    gap: 3rem !important
}

.justify-content-start {
    justify-content: flex-start !important
}

.justify-content-end {
    justify-content: flex-end !important
}

.justify-content-center {
    justify-content: center !important
}

.justify-content-between {
    justify-content: space-between !important
}

.justify-content-around {
    justify-content: space-around !important
}

.justify-content-evenly {
    justify-content: space-evenly !important
}

.align-items-start {
    align-items: flex-start !important
}

.align-items-end {
    align-items: flex-end !important
}

.align-items-center {
    align-items: center !important
}

.align-items-baseline {
    align-items: baseline !important
}

.align-items-stretch {
    align-items: stretch !important
}

.align-content-start {
    align-content: flex-start !important
}

.align-content-end {
    align-content: flex-end !important
}

.align-content-center {
    align-content: center !important
}

.align-content-between {
    align-content: space-between !important
}

.align-content-around {
    align-content: space-around !important
}

.align-content-stretch {
    align-content: stretch !important
}

.align-self-auto {
    align-self: auto !important
}

.align-self-start {
    align-self: flex-start !important
}

.align-self-end {
    align-self: flex-end !important
}

.align-self-center {
    align-self: center !important
}

.align-self-baseline {
    align-self: baseline !important
}

.align-self-stretch {
    align-self: stretch !important
}

.order-first {
    order: -1 !important
}

.order-0 {
    order: 0 !important
}

.order-1 {
    order: 1 !important
}

.order-2 {
    order: 2 !important
}

.order-3 {
    order: 3 !important
}

.order-4 {
    order: 4 !important
}

.order-5 {
    order: 5 !important
}

.order-last {
    order: 6 !important
}

.m-0 {
    margin: 0 !important
}

.m-1 {
    margin: .25rem !important
}

.m-2 {
    margin: .5rem !important
}

.m-3 {
    margin: 1rem !important
}

.m-4 {
    margin: 1.5rem !important
}

.m-5 {
    margin: 3rem !important
}

.m-auto {
    margin: auto !important
}

.mx-0 {
    margin-right: 0 !important;
    margin-left: 0 !important
}

.mx-1 {
    margin-right: .25rem !important;
    margin-left: .25rem !important
}

.mx-2 {
    margin-right: .5rem !important;
    margin-left: .5rem !important
}

.mx-3 {
    margin-right: 1rem !important;
    margin-left: 1rem !important
}

.mx-4 {
    margin-right: 1.5rem !important;
    margin-left: 1.5rem !important
}

.mx-5 {
    margin-right: 3rem !important;
    margin-left: 3rem !important
}

.mx-auto {
    margin-right: auto !important;
    margin-left: auto !important
}

.my-0 {
    margin-top: 0 !important;
    margin-bottom: 0 !important
}

.my-1 {
    margin-top: .25rem !important;
    margin-bottom: .25rem !important
}

.my-2 {
    margin-top: .5rem !important;
    margin-bottom: .5rem !important
}

.my-3 {
    margin-top: 1rem !important;
    margin-bottom: 1rem !important
}

.my-4 {
    margin-top: 1.5rem !important;
    margin-bottom: 1.5rem !important
}

.my-5 {
    margin-top: 3rem !important;
    margin-bottom: 3rem !important
}

.my-auto {
    margin-top: auto !important;
    margin-bottom: auto !important
}

.mt-0 {
    margin-top: 0 !important
}

.mt-1 {
    margin-top: .25rem !important
}

.mt-2 {
    margin-top: .5rem !important
}

.mt-3 {
    margin-top: 1rem !important
}

.mt-4 {
    margin-top: 1.5rem !important
}

.mt-5 {
    margin-top: 3rem !important
}

.mt-auto {
    margin-top: auto !important
}

.me-0 {
    margin-right: 0 !important
}

.me-1 {
    margin-right: .25rem !important
}

.me-2 {
    margin-right: .5rem !important
}

.me-3 {
    margin-right: 1rem !important
}

.me-4 {
    margin-right: 1.5rem !important
}

.me-5 {
    margin-right: 3rem !important
}

.me-auto {
    margin-right: auto !important
}

.mb-0 {
    margin-bottom: 0 !important
}

.mb-1 {
    margin-bottom: .25rem !important
}

.mb-2 {
    margin-bottom: .5rem !important
}

.mb-3 {
    margin-bottom: 1rem !important
}

.mb-4 {
    margin-bottom: 1.5rem !important
}

.mb-5 {
    margin-bottom: 3rem !important
}

.mb-auto {
    margin-bottom: auto !important
}

.ms-0 {
    margin-left: 0 !important
}

.ms-1 {
    margin-left: .25rem !important
}

.ms-2 {
    margin-left: .5rem !important
}

.ms-3 {
    margin-left: 1rem !important
}

.ms-4 {
    margin-left: 1.5rem !important
}

.ms-5 {
    margin-left: 3rem !important
}

.ms-auto {
    margin-left: auto !important
}

.p-0 {
    padding: 0 !important
}

.p-1 {
    padding: .25rem !important
}

.p-2 {
    padding: .5rem !important
}

.p-3 {
    padding: 1rem !important
}

.p-4 {
    padding: 1.5rem !important
}

.p-5 {
    padding: 3rem !important
}

.px-0 {
    padding-right: 0 !important;
    padding-left: 0 !important
}

.px-1 {
    padding-right: .25rem !important;
    padding-left: .25rem !important
}

.px-2 {
    padding-right: .5rem !important;
    padding-left: .5rem !important
}

.px-3 {
    padding-right: 1rem !important;
    padding-left: 1rem !important
}

.px-4 {
    padding-right: 1.5rem !important;
    padding-left: 1.5rem !important
}

.px-5 {
    padding-right: 3rem !important;
    padding-left: 3rem !important
}

.py-0 {
    padding-top: 0 !important;
    padding-bottom: 0 !important
}

.py-1 {
    padding-top: .25rem !important;
    padding-bottom: .25rem !important
}

.py-2 {
    padding-top: .5rem !important;
    padding-bottom: .5rem !important
}

.py-3 {
    padding-top: 1rem !important;
    padding-bottom: 1rem !important
}

.py-4 {
    padding-top: 1.5rem !important;
    padding-bottom: 1.5rem !important
}

.py-5 {
    padding-top: 3rem !important;
    padding-bottom: 3rem !important
}

.pt-0 {
    padding-top: 0 !important
}

.pt-1 {
    padding-top: .25rem !important
}

.pt-2 {
    padding-top: .5rem !important
}

.pt-3 {
    padding-top: 1rem !important
}

.pt-4 {
    padding-top: 1.5rem !important
}

.pt-5 {
    padding-top: 3rem !important
}

.pe-0 {
    padding-right: 0 !important
}

.pe-1 {
    padding-right: .25rem !important
}

.pe-2 {
    padding-right: .5rem !important
}

.pe-3 {
    padding-right: 1rem !important
}

.pe-4 {
    padding-right: 1.5rem !important
}

.pe-5 {
    padding-right: 3rem !important
}

.pb-0 {
    padding-bottom: 0 !important
}

.pb-1 {
    padding-bottom: .25rem !important
}

.pb-2 {
    padding-bottom: .5rem !important
}

.pb-3 {
    padding-bottom: 1rem !important
}

.pb-4 {
    padding-bottom: 1.5rem !important
}

.pb-5 {
    padding-bottom: 3rem !important
}

.ps-0 {
    padding-left: 0 !important
}

.ps-1 {
    padding-left: .25rem !important
}

.ps-2 {
    padding-left: .5rem !important
}

.ps-3 {
    padding-left: 1rem !important
}

.ps-4 {
    padding-left: 1.5rem !important
}

.ps-5 {
    padding-left: 3rem !important
}

.fs-1 {
    font-size: calc(1.375rem + 1.5vw) !important
}

.fs-2 {
    font-size: calc(1.325rem + .9vw) !important
}

.fs-3 {
    font-size: calc(1.3rem + .6vw) !important
}

.fs-4 {
    font-size: calc(1.275rem + .3vw) !important
}

.fs-5 {
    font-size: 1.25rem !important
}

.fs-6 {
    font-size: 1rem !important
}

.fst-italic {
    font-style: italic !important
}

.fst-normal {
    font-style: normal !important
}

.fw-light {
    font-weight: 300 !important
}

.fw-lighter {
    font-weight: lighter !important
}

.fw-normal {
    font-weight: 400 !important
}

.fw-bold {
    font-weight: 700 !important
}

.fw-bolder {
    font-weight: bolder !important
}

.text-lowercase {
    text-transform: lowercase !important
}

.text-uppercase {
    text-transform: uppercase !important
}

.text-capitalize {
    text-transform: capitalize !important
}

.text-start {
    text-align: left !important
}

.text-end {
    text-align: right !important
}

.text-center {
    text-align: center !important
}

.text-primary {
    color: #fd0d0d !important
}

.text-secondary {
    color: #6c757d !important
}

.text-success {
    color: #198754 !important
}

.text-info {
    color: #0dcaf0 !important
}

.text-warning {
    color: #ffc107 !important
}

.text-danger {
    color: #dc3545 !important
}

.text-light {
    color: #f8f9fa !important
}

.text-dark {
    color: #212529 !important
}

.text-white {
    color: #fff !important
}

.text-body {
    color: #212529 !important
}

.text-muted {
    color: #6c757d !important
}

.text-black-50 {
    color: rgba(0, 0, 0, .5) !important
}

.text-white-50 {
    color: rgba(255, 255, 255, .5) !important
}

.text-reset {
    color: inherit !important
}

.lh-1 {
    line-height: 1 !important
}

.lh-sm {
    line-height: 1.25 !important
}

.lh-base {
    line-height: 1.5 !important
}

.lh-lg {
    line-height: 2 !important
}

.bg-primary {
    background-color: #fd0d0d !important
}

.bg-secondary {
    background-color: #6c757d !important
}

.bg-success {
    background-color: #198754 !important
}

.bg-info {
    background-color: #0dcaf0 !important
}

.bg-warning {
    background-color: #ffc107 !important
}

.bg-danger {
    background-color: #dc3545 !important
}

.bg-light {
    background-color: #f8f9fa !important
}

.bg-dark {
    background-color: #212529 !important
}

.bg-body {
    background-color: #fff !important
}

.bg-white {
    background-color: #fff !important
}

.bg-transparent {
    background-color: transparent !important
}

.bg-gradient {
    background-image: var(--bs-gradient) !important
}

.text-wrap {
    white-space: normal !important
}

.text-nowrap {
    white-space: nowrap !important
}

.text-decoration-none {
    text-decoration: none !important
}

.text-decoration-underline {
    text-decoration: underline !important
}

.text-decoration-line-through {
    text-decoration: line-through !important
}

.text-break {
    word-wrap: break-word !important;
    word-break: break-word !important
}

.font-monospace {
    font-family: var(--bs-font-monospace) !important
}

.user-select-all {
    -webkit-user-select: all !important;
    -moz-user-select: all !important;
    user-select: all !important
}

.user-select-auto {
    -webkit-user-select: auto !important;
    -moz-user-select: auto !important;
    user-select: auto !important
}

.user-select-none {
    -webkit-user-select: none !important;
    -moz-user-select: none !important;
    user-select: none !important
}

.pe-none {
    pointer-events: none !important
}

.pe-auto {
    pointer-events: auto !important
}

.rounded {
    border-radius: .25rem !important
}

.rounded-0 {
    border-radius: 0 !important
}

.rounded-1 {
    border-radius: .2rem !important
}

.rounded-2 {
    border-radius: .25rem !important
}

.rounded-3 {
    border-radius: .3rem !important
}

.rounded-circle {
    border-radius: 50% !important
}

.rounded-pill {
    border-radius: 50rem !important
}

.rounded-top {
    border-top-left-radius: .25rem !important;
    border-top-right-radius: .25rem !important
}

.rounded-end {
    border-top-right-radius: .25rem !important;
    border-bottom-right-radius: .25rem !important
}

.rounded-bottom {
    border-bottom-right-radius: .25rem !important;
    border-bottom-left-radius: .25rem !important
}

.rounded-start {
    border-bottom-left-radius: .25rem !important;
    border-top-left-radius: .25rem !important
}

.visible {
    visibility: visible !important
}

.invisible {
    visibility: hidden !important
}

@media (min-width:576px) {
    .float-sm-start {
        float: left !important
    }

    .float-sm-end {
        float: right !important
    }

    .float-sm-none {
        float: none !important
    }

    .d-sm-inline {
        display: inline !important
    }

    .d-sm-inline-block {
        display: inline-block !important
    }

    .d-sm-block {
        display: block !important
    }

    .d-sm-grid {
        display: grid !important
    }

    .d-sm-table {
        display: table !important
    }

    .d-sm-table-row {
        display: table-row !important
    }

    .d-sm-table-cell {
        display: table-cell !important
    }

    .d-sm-flex {
        display: flex !important
    }

    .d-sm-inline-flex {
        display: inline-flex !important
    }

    .d-sm-none {
        display: none !important
    }

    .flex-sm-fill {
        flex: 1 1 auto !important
    }

    .flex-sm-row {
        flex-direction: row !important
    }

    .flex-sm-column {
        flex-direction: column !important
    }

    .flex-sm-row-reverse {
        flex-direction: row-reverse !important
    }

    .flex-sm-column-reverse {
        flex-direction: column-reverse !important
    }

    .flex-sm-grow-0 {
        flex-grow: 0 !important
    }

    .flex-sm-grow-1 {
        flex-grow: 1 !important
    }

    .flex-sm-shrink-0 {
        flex-shrink: 0 !important
    }

    .flex-sm-shrink-1 {
        flex-shrink: 1 !important
    }

    .flex-sm-wrap {
        flex-wrap: wrap !important
    }

    .flex-sm-nowrap {
        flex-wrap: nowrap !important
    }

    .flex-sm-wrap-reverse {
        flex-wrap: wrap-reverse !important
    }

    .gap-sm-0 {
        gap: 0 !important
    }

    .gap-sm-1 {
        gap: .25rem !important
    }

    .gap-sm-2 {
        gap: .5rem !important
    }

    .gap-sm-3 {
        gap: 1rem !important
    }

    .gap-sm-4 {
        gap: 1.5rem !important
    }

    .gap-sm-5 {
        gap: 3rem !important
    }

    .justify-content-sm-start {
        justify-content: flex-start !important
    }

    .justify-content-sm-end {
        justify-content: flex-end !important
    }

    .justify-content-sm-center {
        justify-content: center !important
    }

    .justify-content-sm-between {
        justify-content: space-between !important
    }

    .justify-content-sm-around {
        justify-content: space-around !important
    }

    .justify-content-sm-evenly {
        justify-content: space-evenly !important
    }

    .align-items-sm-start {
        align-items: flex-start !important
    }

    .align-items-sm-end {
        align-items: flex-end !important
    }

    .align-items-sm-center {
        align-items: center !important
    }

    .align-items-sm-baseline {
        align-items: baseline !important
    }

    .align-items-sm-stretch {
        align-items: stretch !important
    }

    .align-content-sm-start {
        align-content: flex-start !important
    }

    .align-content-sm-end {
        align-content: flex-end !important
    }

    .align-content-sm-center {
        align-content: center !important
    }

    .align-content-sm-between {
        align-content: space-between !important
    }

    .align-content-sm-around {
        align-content: space-around !important
    }

    .align-content-sm-stretch {
        align-content: stretch !important
    }

    .align-self-sm-auto {
        align-self: auto !important
    }

    .align-self-sm-start {
        align-self: flex-start !important
    }

    .align-self-sm-end {
        align-self: flex-end !important
    }

    .align-self-sm-center {
        align-self: center !important
    }

    .align-self-sm-baseline {
        align-self: baseline !important
    }

    .align-self-sm-stretch {
        align-self: stretch !important
    }

    .order-sm-first {
        order: -1 !important
    }

    .order-sm-0 {
        order: 0 !important
    }

    .order-sm-1 {
        order: 1 !important
    }

    .order-sm-2 {
        order: 2 !important
    }

    .order-sm-3 {
        order: 3 !important
    }

    .order-sm-4 {
        order: 4 !important
    }

    .order-sm-5 {
        order: 5 !important
    }

    .order-sm-last {
        order: 6 !important
    }

    .m-sm-0 {
        margin: 0 !important
    }

    .m-sm-1 {
        margin: .25rem !important
    }

    .m-sm-2 {
        margin: .5rem !important
    }

    .m-sm-3 {
        margin: 1rem !important
    }

    .m-sm-4 {
        margin: 1.5rem !important
    }

    .m-sm-5 {
        margin: 3rem !important
    }

    .m-sm-auto {
        margin: auto !important
    }

    .mx-sm-0 {
        margin-right: 0 !important;
        margin-left: 0 !important
    }

    .mx-sm-1 {
        margin-right: .25rem !important;
        margin-left: .25rem !important
    }

    .mx-sm-2 {
        margin-right: .5rem !important;
        margin-left: .5rem !important
    }

    .mx-sm-3 {
        margin-right: 1rem !important;
        margin-left: 1rem !important
    }

    .mx-sm-4 {
        margin-right: 1.5rem !important;
        margin-left: 1.5rem !important
    }

    .mx-sm-5 {
        margin-right: 3rem !important;
        margin-left: 3rem !important
    }

    .mx-sm-auto {
        margin-right: auto !important;
        margin-left: auto !important
    }

    .my-sm-0 {
        margin-top: 0 !important;
        margin-bottom: 0 !important
    }

    .my-sm-1 {
        margin-top: .25rem !important;
        margin-bottom: .25rem !important
    }

    .my-sm-2 {
        margin-top: .5rem !important;
        margin-bottom: .5rem !important
    }

    .my-sm-3 {
        margin-top: 1rem !important;
        margin-bottom: 1rem !important
    }

    .my-sm-4 {
        margin-top: 1.5rem !important;
        margin-bottom: 1.5rem !important
    }

    .my-sm-5 {
        margin-top: 3rem !important;
        margin-bottom: 3rem !important
    }

    .my-sm-auto {
        margin-top: auto !important;
        margin-bottom: auto !important
    }

    .mt-sm-0 {
        margin-top: 0 !important
    }

    .mt-sm-1 {
        margin-top: .25rem !important
    }

    .mt-sm-2 {
        margin-top: .5rem !important
    }

    .mt-sm-3 {
        margin-top: 1rem !important
    }

    .mt-sm-4 {
        margin-top: 1.5rem !important
    }

    .mt-sm-5 {
        margin-top: 3rem !important
    }

    .mt-sm-auto {
        margin-top: auto !important
    }

    .me-sm-0 {
        margin-right: 0 !important
    }

    .me-sm-1 {
        margin-right: .25rem !important
    }

    .me-sm-2 {
        margin-right: .5rem !important
    }

    .me-sm-3 {
        margin-right: 1rem !important
    }

    .me-sm-4 {
        margin-right: 1.5rem !important
    }

    .me-sm-5 {
        margin-right: 3rem !important
    }

    .me-sm-auto {
        margin-right: auto !important
    }

    .mb-sm-0 {
        margin-bottom: 0 !important
    }

    .mb-sm-1 {
        margin-bottom: .25rem !important
    }

    .mb-sm-2 {
        margin-bottom: .5rem !important
    }

    .mb-sm-3 {
        margin-bottom: 1rem !important
    }

    .mb-sm-4 {
        margin-bottom: 1.5rem !important
    }

    .mb-sm-5 {
        margin-bottom: 3rem !important
    }

    .mb-sm-auto {
        margin-bottom: auto !important
    }

    .ms-sm-0 {
        margin-left: 0 !important
    }

    .ms-sm-1 {
        margin-left: .25rem !important
    }

    .ms-sm-2 {
        margin-left: .5rem !important
    }

    .ms-sm-3 {
        margin-left: 1rem !important
    }

    .ms-sm-4 {
        margin-left: 1.5rem !important
    }

    .ms-sm-5 {
        margin-left: 3rem !important
    }

    .ms-sm-auto {
        margin-left: auto !important
    }

    .p-sm-0 {
        padding: 0 !important
    }

    .p-sm-1 {
        padding: .25rem !important
    }

    .p-sm-2 {
        padding: .5rem !important
    }

    .p-sm-3 {
        padding: 1rem !important
    }

    .p-sm-4 {
        padding: 1.5rem !important
    }

    .p-sm-5 {
        padding: 3rem !important
    }

    .px-sm-0 {
        padding-right: 0 !important;
        padding-left: 0 !important
    }

    .px-sm-1 {
        padding-right: .25rem !important;
        padding-left: .25rem !important
    }

    .px-sm-2 {
        padding-right: .5rem !important;
        padding-left: .5rem !important
    }

    .px-sm-3 {
        padding-right: 1rem !important;
        padding-left: 1rem !important
    }

    .px-sm-4 {
        padding-right: 1.5rem !important;
        padding-left: 1.5rem !important
    }

    .px-sm-5 {
        padding-right: 3rem !important;
        padding-left: 3rem !important
    }

    .py-sm-0 {
        padding-top: 0 !important;
        padding-bottom: 0 !important
    }

    .py-sm-1 {
        padding-top: .25rem !important;
        padding-bottom: .25rem !important
    }

    .py-sm-2 {
        padding-top: .5rem !important;
        padding-bottom: .5rem !important
    }

    .py-sm-3 {
        padding-top: 1rem !important;
        padding-bottom: 1rem !important
    }

    .py-sm-4 {
        padding-top: 1.5rem !important;
        padding-bottom: 1.5rem !important
    }

    .py-sm-5 {
        padding-top: 3rem !important;
        padding-bottom: 3rem !important
    }

    .pt-sm-0 {
        padding-top: 0 !important
    }

    .pt-sm-1 {
        padding-top: .25rem !important
    }

    .pt-sm-2 {
        padding-top: .5rem !important
    }

    .pt-sm-3 {
        padding-top: 1rem !important
    }

    .pt-sm-4 {
        padding-top: 1.5rem !important
    }

    .pt-sm-5 {
        padding-top: 3rem !important
    }

    .pe-sm-0 {
        padding-right: 0 !important
    }

    .pe-sm-1 {
        padding-right: .25rem !important
    }

    .pe-sm-2 {
        padding-right: .5rem !important
    }

    .pe-sm-3 {
        padding-right: 1rem !important
    }

    .pe-sm-4 {
        padding-right: 1.5rem !important
    }

    .pe-sm-5 {
        padding-right: 3rem !important
    }

    .pb-sm-0 {
        padding-bottom: 0 !important
    }

    .pb-sm-1 {
        padding-bottom: .25rem !important
    }

    .pb-sm-2 {
        padding-bottom: .5rem !important
    }

    .pb-sm-3 {
        padding-bottom: 1rem !important
    }

    .pb-sm-4 {
        padding-bottom: 1.5rem !important
    }

    .pb-sm-5 {
        padding-bottom: 3rem !important
    }

    .ps-sm-0 {
        padding-left: 0 !important
    }

    .ps-sm-1 {
        padding-left: .25rem !important
    }

    .ps-sm-2 {
        padding-left: .5rem !important
    }

    .ps-sm-3 {
        padding-left: 1rem !important
    }

    .ps-sm-4 {
        padding-left: 1.5rem !important
    }

    .ps-sm-5 {
        padding-left: 3rem !important
    }

    .text-sm-start {
        text-align: left !important
    }

    .text-sm-end {
        text-align: right !important
    }

    .text-sm-center {
        text-align: center !important
    }
}

@media (min-width:768px) {
    .float-md-start {
        float: left !important
    }

    .float-md-end {
        float: right !important
    }

    .float-md-none {
        float: none !important
    }

    .d-md-inline {
        display: inline !important
    }

    .d-md-inline-block {
        display: inline-block !important
    }

    .d-md-block {
        display: block !important
    }

    .d-md-grid {
        display: grid !important
    }

    .d-md-table {
        display: table !important
    }

    .d-md-table-row {
        display: table-row !important
    }

    .d-md-table-cell {
        display: table-cell !important
    }

    .d-md-flex {
        display: flex !important
    }

    .d-md-inline-flex {
        display: inline-flex !important
    }

    .d-md-none {
        display: none !important
    }

    .flex-md-fill {
        flex: 1 1 auto !important
    }

    .flex-md-row {
        flex-direction: row !important
    }

    .flex-md-column {
        flex-direction: column !important
    }

    .flex-md-row-reverse {
        flex-direction: row-reverse !important
    }

    .flex-md-column-reverse {
        flex-direction: column-reverse !important
    }

    .flex-md-grow-0 {
        flex-grow: 0 !important
    }

    .flex-md-grow-1 {
        flex-grow: 1 !important
    }

    .flex-md-shrink-0 {
        flex-shrink: 0 !important
    }

    .flex-md-shrink-1 {
        flex-shrink: 1 !important
    }

    .flex-md-wrap {
        flex-wrap: wrap !important
    }

    .flex-md-nowrap {
        flex-wrap: nowrap !important
    }

    .flex-md-wrap-reverse {
        flex-wrap: wrap-reverse !important
    }

    .gap-md-0 {
        gap: 0 !important
    }

    .gap-md-1 {
        gap: .25rem !important
    }

    .gap-md-2 {
        gap: .5rem !important
    }

    .gap-md-3 {
        gap: 1rem !important
    }

    .gap-md-4 {
        gap: 1.5rem !important
    }

    .gap-md-5 {
        gap: 3rem !important
    }

    .justify-content-md-start {
        justify-content: flex-start !important
    }

    .justify-content-md-end {
        justify-content: flex-end !important
    }

    .justify-content-md-center {
        justify-content: center !important
    }

    .justify-content-md-between {
        justify-content: space-between !important
    }

    .justify-content-md-around {
        justify-content: space-around !important
    }

    .justify-content-md-evenly {
        justify-content: space-evenly !important
    }

    .align-items-md-start {
        align-items: flex-start !important
    }

    .align-items-md-end {
        align-items: flex-end !important
    }

    .align-items-md-center {
        align-items: center !important
    }

    .align-items-md-baseline {
        align-items: baseline !important
    }

    .align-items-md-stretch {
        align-items: stretch !important
    }

    .align-content-md-start {
        align-content: flex-start !important
    }

    .align-content-md-end {
        align-content: flex-end !important
    }

    .align-content-md-center {
        align-content: center !important
    }

    .align-content-md-between {
        align-content: space-between !important
    }

    .align-content-md-around {
        align-content: space-around !important
    }

    .align-content-md-stretch {
        align-content: stretch !important
    }

    .align-self-md-auto {
        align-self: auto !important
    }

    .align-self-md-start {
        align-self: flex-start !important
    }

    .align-self-md-end {
        align-self: flex-end !important
    }

    .align-self-md-center {
        align-self: center !important
    }

    .align-self-md-baseline {
        align-self: baseline !important
    }

    .align-self-md-stretch {
        align-self: stretch !important
    }

    .order-md-first {
        order: -1 !important
    }

    .order-md-0 {
        order: 0 !important
    }

    .order-md-1 {
        order: 1 !important
    }

    .order-md-2 {
        order: 2 !important
    }

    .order-md-3 {
        order: 3 !important
    }

    .order-md-4 {
        order: 4 !important
    }

    .order-md-5 {
        order: 5 !important
    }

    .order-md-last {
        order: 6 !important
    }

    .m-md-0 {
        margin: 0 !important
    }

    .m-md-1 {
        margin: .25rem !important
    }

    .m-md-2 {
        margin: .5rem !important
    }

    .m-md-3 {
        margin: 1rem !important
    }

    .m-md-4 {
        margin: 1.5rem !important
    }

    .m-md-5 {
        margin: 3rem !important
    }

    .m-md-auto {
        margin: auto !important
    }

    .mx-md-0 {
        margin-right: 0 !important;
        margin-left: 0 !important
    }

    .mx-md-1 {
        margin-right: .25rem !important;
        margin-left: .25rem !important
    }

    .mx-md-2 {
        margin-right: .5rem !important;
        margin-left: .5rem !important
    }

    .mx-md-3 {
        margin-right: 1rem !important;
        margin-left: 1rem !important
    }

    .mx-md-4 {
        margin-right: 1.5rem !important;
        margin-left: 1.5rem !important
    }

    .mx-md-5 {
        margin-right: 3rem !important;
        margin-left: 3rem !important
    }

    .mx-md-auto {
        margin-right: auto !important;
        margin-left: auto !important
    }

    .my-md-0 {
        margin-top: 0 !important;
        margin-bottom: 0 !important
    }

    .my-md-1 {
        margin-top: .25rem !important;
        margin-bottom: .25rem !important
    }

    .my-md-2 {
        margin-top: .5rem !important;
        margin-bottom: .5rem !important
    }

    .my-md-3 {
        margin-top: 1rem !important;
        margin-bottom: 1rem !important
    }

    .my-md-4 {
        margin-top: 1.5rem !important;
        margin-bottom: 1.5rem !important
    }

    .my-md-5 {
        margin-top: 3rem !important;
        margin-bottom: 3rem !important
    }

    .my-md-auto {
        margin-top: auto !important;
        margin-bottom: auto !important
    }

    .mt-md-0 {
        margin-top: 0 !important
    }

    .mt-md-1 {
        margin-top: .25rem !important
    }

    .mt-md-2 {
        margin-top: .5rem !important
    }

    .mt-md-3 {
        margin-top: 1rem !important
    }

    .mt-md-4 {
        margin-top: 1.5rem !important
    }

    .mt-md-5 {
        margin-top: 3rem !important
    }

    .mt-md-auto {
        margin-top: auto !important
    }

    .me-md-0 {
        margin-right: 0 !important
    }

    .me-md-1 {
        margin-right: .25rem !important
    }

    .me-md-2 {
        margin-right: .5rem !important
    }

    .me-md-3 {
        margin-right: 1rem !important
    }

    .me-md-4 {
        margin-right: 1.5rem !important
    }

    .me-md-5 {
        margin-right: 3rem !important
    }

    .me-md-auto {
        margin-right: auto !important
    }

    .mb-md-0 {
        margin-bottom: 0 !important
    }

    .mb-md-1 {
        margin-bottom: .25rem !important
    }

    .mb-md-2 {
        margin-bottom: .5rem !important
    }

    .mb-md-3 {
        margin-bottom: 1rem !important
    }

    .mb-md-4 {
        margin-bottom: 1.5rem !important
    }

    .mb-md-5 {
        margin-bottom: 3rem !important
    }

    .mb-md-auto {
        margin-bottom: auto !important
    }

    .ms-md-0 {
        margin-left: 0 !important
    }

    .ms-md-1 {
        margin-left: .25rem !important
    }

    .ms-md-2 {
        margin-left: .5rem !important
    }

    .ms-md-3 {
        margin-left: 1rem !important
    }

    .ms-md-4 {
        margin-left: 1.5rem !important
    }

    .ms-md-5 {
        margin-left: 3rem !important
    }

    .ms-md-auto {
        margin-left: auto !important
    }

    .p-md-0 {
        padding: 0 !important
    }

    .p-md-1 {
        padding: .25rem !important
    }

    .p-md-2 {
        padding: .5rem !important
    }

    .p-md-3 {
        padding: 1rem !important
    }

    .p-md-4 {
        padding: 1.5rem !important
    }

    .p-md-5 {
        padding: 3rem !important
    }

    .px-md-0 {
        padding-right: 0 !important;
        padding-left: 0 !important
    }

    .px-md-1 {
        padding-right: .25rem !important;
        padding-left: .25rem !important
    }

    .px-md-2 {
        padding-right: .5rem !important;
        padding-left: .5rem !important
    }

    .px-md-3 {
        padding-right: 1rem !important;
        padding-left: 1rem !important
    }

    .px-md-4 {
        padding-right: 1.5rem !important;
        padding-left: 1.5rem !important
    }

    .px-md-5 {
        padding-right: 3rem !important;
        padding-left: 3rem !important
    }

    .py-md-0 {
        padding-top: 0 !important;
        padding-bottom: 0 !important
    }

    .py-md-1 {
        padding-top: .25rem !important;
        padding-bottom: .25rem !important
    }

    .py-md-2 {
        padding-top: .5rem !important;
        padding-bottom: .5rem !important
    }

    .py-md-3 {
        padding-top: 1rem !important;
        padding-bottom: 1rem !important
    }

    .py-md-4 {
        padding-top: 1.5rem !important;
        padding-bottom: 1.5rem !important
    }

    .py-md-5 {
        padding-top: 3rem !important;
        padding-bottom: 3rem !important
    }

    .pt-md-0 {
        padding-top: 0 !important
    }

    .pt-md-1 {
        padding-top: .25rem !important
    }

    .pt-md-2 {
        padding-top: .5rem !important
    }

    .pt-md-3 {
        padding-top: 1rem !important
    }

    .pt-md-4 {
        padding-top: 1.5rem !important
    }

    .pt-md-5 {
        padding-top: 3rem !important
    }

    .pe-md-0 {
        padding-right: 0 !important
    }

    .pe-md-1 {
        padding-right: .25rem !important
    }

    .pe-md-2 {
        padding-right: .5rem !important
    }

    .pe-md-3 {
        padding-right: 1rem !important
    }

    .pe-md-4 {
        padding-right: 1.5rem !important
    }

    .pe-md-5 {
        padding-right: 3rem !important
    }

    .pb-md-0 {
        padding-bottom: 0 !important
    }

    .pb-md-1 {
        padding-bottom: .25rem !important
    }

    .pb-md-2 {
        padding-bottom: .5rem !important
    }

    .pb-md-3 {
        padding-bottom: 1rem !important
    }

    .pb-md-4 {
        padding-bottom: 1.5rem !important
    }

    .pb-md-5 {
        padding-bottom: 3rem !important
    }

    .ps-md-0 {
        padding-left: 0 !important
    }

    .ps-md-1 {
        padding-left: .25rem !important
    }

    .ps-md-2 {
        padding-left: .5rem !important
    }

    .ps-md-3 {
        padding-left: 1rem !important
    }

    .ps-md-4 {
        padding-left: 1.5rem !important
    }

    .ps-md-5 {
        padding-left: 3rem !important
    }

    .text-md-start {
        text-align: left !important
    }

    .text-md-end {
        text-align: right !important
    }

    .text-md-center {
        text-align: center !important
    }
}

@media (min-width:992px) {
    .float-lg-start {
        float: left !important
    }

    .float-lg-end {
        float: right !important
    }

    .float-lg-none {
        float: none !important
    }

    .d-lg-inline {
        display: inline !important
    }

    .d-lg-inline-block {
        display: inline-block !important
    }

    .d-lg-block {
        display: block !important
    }

    .d-lg-grid {
        display: grid !important
    }

    .d-lg-table {
        display: table !important
    }

    .d-lg-table-row {
        display: table-row !important
    }

    .d-lg-table-cell {
        display: table-cell !important
    }

    .d-lg-flex {
        display: flex !important
    }

    .d-lg-inline-flex {
        display: inline-flex !important
    }

    .d-lg-none {
        display: none !important
    }

    .flex-lg-fill {
        flex: 1 1 auto !important
    }

    .flex-lg-row {
        flex-direction: row !important
    }

    .flex-lg-column {
        flex-direction: column !important
    }

    .flex-lg-row-reverse {
        flex-direction: row-reverse !important
    }

    .flex-lg-column-reverse {
        flex-direction: column-reverse !important
    }

    .flex-lg-grow-0 {
        flex-grow: 0 !important
    }

    .flex-lg-grow-1 {
        flex-grow: 1 !important
    }

    .flex-lg-shrink-0 {
        flex-shrink: 0 !important
    }

    .flex-lg-shrink-1 {
        flex-shrink: 1 !important
    }

    .flex-lg-wrap {
        flex-wrap: wrap !important
    }

    .flex-lg-nowrap {
        flex-wrap: nowrap !important
    }

    .flex-lg-wrap-reverse {
        flex-wrap: wrap-reverse !important
    }

    .gap-lg-0 {
        gap: 0 !important
    }

    .gap-lg-1 {
        gap: .25rem !important
    }

    .gap-lg-2 {
        gap: .5rem !important
    }

    .gap-lg-3 {
        gap: 1rem !important
    }

    .gap-lg-4 {
        gap: 1.5rem !important
    }

    .gap-lg-5 {
        gap: 3rem !important
    }

    .justify-content-lg-start {
        justify-content: flex-start !important
    }

    .justify-content-lg-end {
        justify-content: flex-end !important
    }

    .justify-content-lg-center {
        justify-content: center !important
    }

    .justify-content-lg-between {
        justify-content: space-between !important
    }

    .justify-content-lg-around {
        justify-content: space-around !important
    }

    .justify-content-lg-evenly {
        justify-content: space-evenly !important
    }

    .align-items-lg-start {
        align-items: flex-start !important
    }

    .align-items-lg-end {
        align-items: flex-end !important
    }

    .align-items-lg-center {
        align-items: center !important
    }

    .align-items-lg-baseline {
        align-items: baseline !important
    }

    .align-items-lg-stretch {
        align-items: stretch !important
    }

    .align-content-lg-start {
        align-content: flex-start !important
    }

    .align-content-lg-end {
        align-content: flex-end !important
    }

    .align-content-lg-center {
        align-content: center !important
    }

    .align-content-lg-between {
        align-content: space-between !important
    }

    .align-content-lg-around {
        align-content: space-around !important
    }

    .align-content-lg-stretch {
        align-content: stretch !important
    }

    .align-self-lg-auto {
        align-self: auto !important
    }

    .align-self-lg-start {
        align-self: flex-start !important
    }

    .align-self-lg-end {
        align-self: flex-end !important
    }

    .align-self-lg-center {
        align-self: center !important
    }

    .align-self-lg-baseline {
        align-self: baseline !important
    }

    .align-self-lg-stretch {
        align-self: stretch !important
    }

    .order-lg-first {
        order: -1 !important
    }

    .order-lg-0 {
        order: 0 !important
    }

    .order-lg-1 {
        order: 1 !important
    }

    .order-lg-2 {
        order: 2 !important
    }

    .order-lg-3 {
        order: 3 !important
    }

    .order-lg-4 {
        order: 4 !important
    }

    .order-lg-5 {
        order: 5 !important
    }

    .order-lg-last {
        order: 6 !important
    }

    .m-lg-0 {
        margin: 0 !important
    }

    .m-lg-1 {
        margin: .25rem !important
    }

    .m-lg-2 {
        margin: .5rem !important
    }

    .m-lg-3 {
        margin: 1rem !important
    }

    .m-lg-4 {
        margin: 1.5rem !important
    }

    .m-lg-5 {
        margin: 3rem !important
    }

    .m-lg-auto {
        margin: auto !important
    }

    .mx-lg-0 {
        margin-right: 0 !important;
        margin-left: 0 !important
    }

    .mx-lg-1 {
        margin-right: .25rem !important;
        margin-left: .25rem !important
    }

    .mx-lg-2 {
        margin-right: .5rem !important;
        margin-left: .5rem !important
    }

    .mx-lg-3 {
        margin-right: 1rem !important;
        margin-left: 1rem !important
    }

    .mx-lg-4 {
        margin-right: 1.5rem !important;
        margin-left: 1.5rem !important
    }

    .mx-lg-5 {
        margin-right: 3rem !important;
        margin-left: 3rem !important
    }

    .mx-lg-auto {
        margin-right: auto !important;
        margin-left: auto !important
    }

    .my-lg-0 {
        margin-top: 0 !important;
        margin-bottom: 0 !important
    }

    .my-lg-1 {
        margin-top: .25rem !important;
        margin-bottom: .25rem !important
    }

    .my-lg-2 {
        margin-top: .5rem !important;
        margin-bottom: .5rem !important
    }

    .my-lg-3 {
        margin-top: 1rem !important;
        margin-bottom: 1rem !important
    }

    .my-lg-4 {
        margin-top: 1.5rem !important;
        margin-bottom: 1.5rem !important
    }

    .my-lg-5 {
        margin-top: 3rem !important;
        margin-bottom: 3rem !important
    }

    .my-lg-auto {
        margin-top: auto !important;
        margin-bottom: auto !important
    }

    .mt-lg-0 {
        margin-top: 0 !important
    }

    .mt-lg-1 {
        margin-top: .25rem !important
    }

    .mt-lg-2 {
        margin-top: .5rem !important
    }

    .mt-lg-3 {
        margin-top: 1rem !important
    }

    .mt-lg-4 {
        margin-top: 1.5rem !important
    }

    .mt-lg-5 {
        margin-top: 3rem !important
    }

    .mt-lg-auto {
        margin-top: auto !important
    }

    .me-lg-0 {
        margin-right: 0 !important
    }

    .me-lg-1 {
        margin-right: .25rem !important
    }

    .me-lg-2 {
        margin-right: .5rem !important
    }

    .me-lg-3 {
        margin-right: 1rem !important
    }

    .me-lg-4 {
        margin-right: 1.5rem !important
    }

    .me-lg-5 {
        margin-right: 3rem !important
    }

    .me-lg-auto {
        margin-right: auto !important
    }

    .mb-lg-0 {
        margin-bottom: 0 !important
    }

    .mb-lg-1 {
        margin-bottom: .25rem !important
    }

    .mb-lg-2 {
        margin-bottom: .5rem !important
    }

    .mb-lg-3 {
        margin-bottom: 1rem !important
    }

    .mb-lg-4 {
        margin-bottom: 1.5rem !important
    }

    .mb-lg-5 {
        margin-bottom: 3rem !important
    }

    .mb-lg-auto {
        margin-bottom: auto !important
    }

    .ms-lg-0 {
        margin-left: 0 !important
    }

    .ms-lg-1 {
        margin-left: .25rem !important
    }

    .ms-lg-2 {
        margin-left: .5rem !important
    }

    .ms-lg-3 {
        margin-left: 1rem !important
    }

    .ms-lg-4 {
        margin-left: 1.5rem !important
    }

    .ms-lg-5 {
        margin-left: 3rem !important
    }

    .ms-lg-auto {
        margin-left: auto !important
    }

    .p-lg-0 {
        padding: 0 !important
    }

    .p-lg-1 {
        padding: .25rem !important
    }

    .p-lg-2 {
        padding: .5rem !important
    }

    .p-lg-3 {
        padding: 1rem !important
    }

    .p-lg-4 {
        padding: 1.5rem !important
    }

    .p-lg-5 {
        padding: 3rem !important
    }

    .px-lg-0 {
        padding-right: 0 !important;
        padding-left: 0 !important
    }

    .px-lg-1 {
        padding-right: .25rem !important;
        padding-left: .25rem !important
    }

    .px-lg-2 {
        padding-right: .5rem !important;
        padding-left: .5rem !important
    }

    .px-lg-3 {
        padding-right: 1rem !important;
        padding-left: 1rem !important
    }

    .px-lg-4 {
        padding-right: 1.5rem !important;
        padding-left: 1.5rem !important
    }

    .px-lg-5 {
        padding-right: 3rem !important;
        padding-left: 3rem !important
    }

    .py-lg-0 {
        padding-top: 0 !important;
        padding-bottom: 0 !important
    }

    .py-lg-1 {
        padding-top: .25rem !important;
        padding-bottom: .25rem !important
    }

    .py-lg-2 {
        padding-top: .5rem !important;
        padding-bottom: .5rem !important
    }

    .py-lg-3 {
        padding-top: 1rem !important;
        padding-bottom: 1rem !important
    }

    .py-lg-4 {
        padding-top: 1.5rem !important;
        padding-bottom: 1.5rem !important
    }

    .py-lg-5 {
        padding-top: 3rem !important;
        padding-bottom: 3rem !important
    }

    .pt-lg-0 {
        padding-top: 0 !important
    }

    .pt-lg-1 {
        padding-top: .25rem !important
    }

    .pt-lg-2 {
        padding-top: .5rem !important
    }

    .pt-lg-3 {
        padding-top: 1rem !important
    }

    .pt-lg-4 {
        padding-top: 1.5rem !important
    }

    .pt-lg-5 {
        padding-top: 3rem !important
    }

    .pe-lg-0 {
        padding-right: 0 !important
    }

    .pe-lg-1 {
        padding-right: .25rem !important
    }

    .pe-lg-2 {
        padding-right: .5rem !important
    }

    .pe-lg-3 {
        padding-right: 1rem !important
    }

    .pe-lg-4 {
        padding-right: 1.5rem !important
    }

    .pe-lg-5 {
        padding-right: 3rem !important
    }

    .pb-lg-0 {
        padding-bottom: 0 !important
    }

    .pb-lg-1 {
        padding-bottom: .25rem !important
    }

    .pb-lg-2 {
        padding-bottom: .5rem !important
    }

    .pb-lg-3 {
        padding-bottom: 1rem !important
    }

    .pb-lg-4 {
        padding-bottom: 1.5rem !important
    }

    .pb-lg-5 {
        padding-bottom: 3rem !important
    }

    .ps-lg-0 {
        padding-left: 0 !important
    }

    .ps-lg-1 {
        padding-left: .25rem !important
    }

    .ps-lg-2 {
        padding-left: .5rem !important
    }

    .ps-lg-3 {
        padding-left: 1rem !important
    }

    .ps-lg-4 {
        padding-left: 1.5rem !important
    }

    .ps-lg-5 {
        padding-left: 3rem !important
    }

    .text-lg-start {
        text-align: left !important
    }

    .text-lg-end {
        text-align: right !important
    }

    .text-lg-center {
        text-align: center !important
    }
}

@media (min-width:1200px) {
    .float-xl-start {
        float: left !important
    }

    .float-xl-end {
        float: right !important
    }

    .float-xl-none {
        float: none !important
    }

    .d-xl-inline {
        display: inline !important
    }

    .d-xl-inline-block {
        display: inline-block !important
    }

    .d-xl-block {
        display: block !important
    }

    .d-xl-grid {
        display: grid !important
    }

    .d-xl-table {
        display: table !important
    }

    .d-xl-table-row {
        display: table-row !important
    }

    .d-xl-table-cell {
        display: table-cell !important
    }

    .d-xl-flex {
        display: flex !important
    }

    .d-xl-inline-flex {
        display: inline-flex !important
    }

    .d-xl-none {
        display: none !important
    }

    .flex-xl-fill {
        flex: 1 1 auto !important
    }

    .flex-xl-row {
        flex-direction: row !important
    }

    .flex-xl-column {
        flex-direction: column !important
    }

    .flex-xl-row-reverse {
        flex-direction: row-reverse !important
    }

    .flex-xl-column-reverse {
        flex-direction: column-reverse !important
    }

    .flex-xl-grow-0 {
        flex-grow: 0 !important
    }

    .flex-xl-grow-1 {
        flex-grow: 1 !important
    }

    .flex-xl-shrink-0 {
        flex-shrink: 0 !important
    }

    .flex-xl-shrink-1 {
        flex-shrink: 1 !important
    }

    .flex-xl-wrap {
        flex-wrap: wrap !important
    }

    .flex-xl-nowrap {
        flex-wrap: nowrap !important
    }

    .flex-xl-wrap-reverse {
        flex-wrap: wrap-reverse !important
    }

    .gap-xl-0 {
        gap: 0 !important
    }

    .gap-xl-1 {
        gap: .25rem !important
    }

    .gap-xl-2 {
        gap: .5rem !important
    }

    .gap-xl-3 {
        gap: 1rem !important
    }

    .gap-xl-4 {
        gap: 1.5rem !important
    }

    .gap-xl-5 {
        gap: 3rem !important
    }

    .justify-content-xl-start {
        justify-content: flex-start !important
    }

    .justify-content-xl-end {
        justify-content: flex-end !important
    }

    .justify-content-xl-center {
        justify-content: center !important
    }

    .justify-content-xl-between {
        justify-content: space-between !important
    }

    .justify-content-xl-around {
        justify-content: space-around !important
    }

    .justify-content-xl-evenly {
        justify-content: space-evenly !important
    }

    .align-items-xl-start {
        align-items: flex-start !important
    }

    .align-items-xl-end {
        align-items: flex-end !important
    }

    .align-items-xl-center {
        align-items: center !important
    }

    .align-items-xl-baseline {
        align-items: baseline !important
    }

    .align-items-xl-stretch {
        align-items: stretch !important
    }

    .align-content-xl-start {
        align-content: flex-start !important
    }

    .align-content-xl-end {
        align-content: flex-end !important
    }

    .align-content-xl-center {
        align-content: center !important
    }

    .align-content-xl-between {
        align-content: space-between !important
    }

    .align-content-xl-around {
        align-content: space-around !important
    }

    .align-content-xl-stretch {
        align-content: stretch !important
    }

    .align-self-xl-auto {
        align-self: auto !important
    }

    .align-self-xl-start {
        align-self: flex-start !important
    }

    .align-self-xl-end {
        align-self: flex-end !important
    }

    .align-self-xl-center {
        align-self: center !important
    }

    .align-self-xl-baseline {
        align-self: baseline !important
    }

    .align-self-xl-stretch {
        align-self: stretch !important
    }

    .order-xl-first {
        order: -1 !important
    }

    .order-xl-0 {
        order: 0 !important
    }

    .order-xl-1 {
        order: 1 !important
    }

    .order-xl-2 {
        order: 2 !important
    }

    .order-xl-3 {
        order: 3 !important
    }

    .order-xl-4 {
        order: 4 !important
    }

    .order-xl-5 {
        order: 5 !important
    }

    .order-xl-last {
        order: 6 !important
    }

    .m-xl-0 {
        margin: 0 !important
    }

    .m-xl-1 {
        margin: .25rem !important
    }

    .m-xl-2 {
        margin: .5rem !important
    }

    .m-xl-3 {
        margin: 1rem !important
    }

    .m-xl-4 {
        margin: 1.5rem !important
    }

    .m-xl-5 {
        margin: 3rem !important
    }

    .m-xl-auto {
        margin: auto !important
    }

    .mx-xl-0 {
        margin-right: 0 !important;
        margin-left: 0 !important
    }

    .mx-xl-1 {
        margin-right: .25rem !important;
        margin-left: .25rem !important
    }

    .mx-xl-2 {
        margin-right: .5rem !important;
        margin-left: .5rem !important
    }

    .mx-xl-3 {
        margin-right: 1rem !important;
        margin-left: 1rem !important
    }

    .mx-xl-4 {
        margin-right: 1.5rem !important;
        margin-left: 1.5rem !important
    }

    .mx-xl-5 {
        margin-right: 3rem !important;
        margin-left: 3rem !important
    }

    .mx-xl-auto {
        margin-right: auto !important;
        margin-left: auto !important
    }

    .my-xl-0 {
        margin-top: 0 !important;
        margin-bottom: 0 !important
    }

    .my-xl-1 {
        margin-top: .25rem !important;
        margin-bottom: .25rem !important
    }

    .my-xl-2 {
        margin-top: .5rem !important;
        margin-bottom: .5rem !important
    }

    .my-xl-3 {
        margin-top: 1rem !important;
        margin-bottom: 1rem !important
    }

    .my-xl-4 {
        margin-top: 1.5rem !important;
        margin-bottom: 1.5rem !important
    }

    .my-xl-5 {
        margin-top: 3rem !important;
        margin-bottom: 3rem !important
    }

    .my-xl-auto {
        margin-top: auto !important;
        margin-bottom: auto !important
    }

    .mt-xl-0 {
        margin-top: 0 !important
    }

    .mt-xl-1 {
        margin-top: .25rem !important
    }

    .mt-xl-2 {
        margin-top: .5rem !important
    }

    .mt-xl-3 {
        margin-top: 1rem !important
    }

    .mt-xl-4 {
        margin-top: 1.5rem !important
    }

    .mt-xl-5 {
        margin-top: 3rem !important
    }

    .mt-xl-auto {
        margin-top: auto !important
    }

    .me-xl-0 {
        margin-right: 0 !important
    }

    .me-xl-1 {
        margin-right: .25rem !important
    }

    .me-xl-2 {
        margin-right: .5rem !important
    }

    .me-xl-3 {
        margin-right: 1rem !important
    }

    .me-xl-4 {
        margin-right: 1.5rem !important
    }

    .me-xl-5 {
        margin-right: 3rem !important
    }

    .me-xl-auto {
        margin-right: auto !important
    }

    .mb-xl-0 {
        margin-bottom: 0 !important
    }

    .mb-xl-1 {
        margin-bottom: .25rem !important
    }

    .mb-xl-2 {
        margin-bottom: .5rem !important
    }

    .mb-xl-3 {
        margin-bottom: 1rem !important
    }

    .mb-xl-4 {
        margin-bottom: 1.5rem !important
    }

    .mb-xl-5 {
        margin-bottom: 3rem !important
    }

    .mb-xl-auto {
        margin-bottom: auto !important
    }

    .ms-xl-0 {
        margin-left: 0 !important
    }

    .ms-xl-1 {
        margin-left: .25rem !important
    }

    .ms-xl-2 {
        margin-left: .5rem !important
    }

    .ms-xl-3 {
        margin-left: 1rem !important
    }

    .ms-xl-4 {
        margin-left: 1.5rem !important
    }

    .ms-xl-5 {
        margin-left: 3rem !important
    }

    .ms-xl-auto {
        margin-left: auto !important
    }

    .p-xl-0 {
        padding: 0 !important
    }

    .p-xl-1 {
        padding: .25rem !important
    }

    .p-xl-2 {
        padding: .5rem !important
    }

    .p-xl-3 {
        padding: 1rem !important
    }

    .p-xl-4 {
        padding: 1.5rem !important
    }

    .p-xl-5 {
        padding: 3rem !important
    }

    .px-xl-0 {
        padding-right: 0 !important;
        padding-left: 0 !important
    }

    .px-xl-1 {
        padding-right: .25rem !important;
        padding-left: .25rem !important
    }

    .px-xl-2 {
        padding-right: .5rem !important;
        padding-left: .5rem !important
    }

    .px-xl-3 {
        padding-right: 1rem !important;
        padding-left: 1rem !important
    }

    .px-xl-4 {
        padding-right: 1.5rem !important;
        padding-left: 1.5rem !important
    }

    .px-xl-5 {
        padding-right: 3rem !important;
        padding-left: 3rem !important
    }

    .py-xl-0 {
        padding-top: 0 !important;
        padding-bottom: 0 !important
    }

    .py-xl-1 {
        padding-top: .25rem !important;
        padding-bottom: .25rem !important
    }

    .py-xl-2 {
        padding-top: .5rem !important;
        padding-bottom: .5rem !important
    }

    .py-xl-3 {
        padding-top: 1rem !important;
        padding-bottom: 1rem !important
    }

    .py-xl-4 {
        padding-top: 1.5rem !important;
        padding-bottom: 1.5rem !important
    }

    .py-xl-5 {
        padding-top: 3rem !important;
        padding-bottom: 3rem !important
    }

    .pt-xl-0 {
        padding-top: 0 !important
    }

    .pt-xl-1 {
        padding-top: .25rem !important
    }

    .pt-xl-2 {
        padding-top: .5rem !important
    }

    .pt-xl-3 {
        padding-top: 1rem !important
    }

    .pt-xl-4 {
        padding-top: 1.5rem !important
    }

    .pt-xl-5 {
        padding-top: 3rem !important
    }

    .pe-xl-0 {
        padding-right: 0 !important
    }

    .pe-xl-1 {
        padding-right: .25rem !important
    }

    .pe-xl-2 {
        padding-right: .5rem !important
    }

    .pe-xl-3 {
        padding-right: 1rem !important
    }

    .pe-xl-4 {
        padding-right: 1.5rem !important
    }

    .pe-xl-5 {
        padding-right: 3rem !important
    }

    .pb-xl-0 {
        padding-bottom: 0 !important
    }

    .pb-xl-1 {
        padding-bottom: .25rem !important
    }

    .pb-xl-2 {
        padding-bottom: .5rem !important
    }

    .pb-xl-3 {
        padding-bottom: 1rem !important
    }

    .pb-xl-4 {
        padding-bottom: 1.5rem !important
    }

    .pb-xl-5 {
        padding-bottom: 3rem !important
    }

    .ps-xl-0 {
        padding-left: 0 !important
    }

    .ps-xl-1 {
        padding-left: .25rem !important
    }

    .ps-xl-2 {
        padding-left: .5rem !important
    }

    .ps-xl-3 {
        padding-left: 1rem !important
    }

    .ps-xl-4 {
        padding-left: 1.5rem !important
    }

    .ps-xl-5 {
        padding-left: 3rem !important
    }

    .text-xl-start {
        text-align: left !important
    }

    .text-xl-end {
        text-align: right !important
    }

    .text-xl-center {
        text-align: center !important
    }
}

@media (min-width:1400px) {
    .float-xxl-start {
        float: left !important
    }

    .float-xxl-end {
        float: right !important
    }

    .float-xxl-none {
        float: none !important
    }

    .d-xxl-inline {
        display: inline !important
    }

    .d-xxl-inline-block {
        display: inline-block !important
    }

    .d-xxl-block {
        display: block !important
    }

    .d-xxl-grid {
        display: grid !important
    }

    .d-xxl-table {
        display: table !important
    }

    .d-xxl-table-row {
        display: table-row !important
    }

    .d-xxl-table-cell {
        display: table-cell !important
    }

    .d-xxl-flex {
        display: flex !important
    }

    .d-xxl-inline-flex {
        display: inline-flex !important
    }

    .d-xxl-none {
        display: none !important
    }

    .flex-xxl-fill {
        flex: 1 1 auto !important
    }

    .flex-xxl-row {
        flex-direction: row !important
    }

    .flex-xxl-column {
        flex-direction: column !important
    }

    .flex-xxl-row-reverse {
        flex-direction: row-reverse !important
    }

    .flex-xxl-column-reverse {
        flex-direction: column-reverse !important
    }

    .flex-xxl-grow-0 {
        flex-grow: 0 !important
    }

    .flex-xxl-grow-1 {
        flex-grow: 1 !important
    }

    .flex-xxl-shrink-0 {
        flex-shrink: 0 !important
    }

    .flex-xxl-shrink-1 {
        flex-shrink: 1 !important
    }

    .flex-xxl-wrap {
        flex-wrap: wrap !important
    }

    .flex-xxl-nowrap {
        flex-wrap: nowrap !important
    }

    .flex-xxl-wrap-reverse {
        flex-wrap: wrap-reverse !important
    }

    .gap-xxl-0 {
        gap: 0 !important
    }

    .gap-xxl-1 {
        gap: .25rem !important
    }

    .gap-xxl-2 {
        gap: .5rem !important
    }

    .gap-xxl-3 {
        gap: 1rem !important
    }

    .gap-xxl-4 {
        gap: 1.5rem !important
    }

    .gap-xxl-5 {
        gap: 3rem !important
    }

    .justify-content-xxl-start {
        justify-content: flex-start !important
    }

    .justify-content-xxl-end {
        justify-content: flex-end !important
    }

    .justify-content-xxl-center {
        justify-content: center !important
    }

    .justify-content-xxl-between {
        justify-content: space-between !important
    }

    .justify-content-xxl-around {
        justify-content: space-around !important
    }

    .justify-content-xxl-evenly {
        justify-content: space-evenly !important
    }

    .align-items-xxl-start {
        align-items: flex-start !important
    }

    .align-items-xxl-end {
        align-items: flex-end !important
    }

    .align-items-xxl-center {
        align-items: center !important
    }

    .align-items-xxl-baseline {
        align-items: baseline !important
    }

    .align-items-xxl-stretch {
        align-items: stretch !important
    }

    .align-content-xxl-start {
        align-content: flex-start !important
    }

    .align-content-xxl-end {
        align-content: flex-end !important
    }

    .align-content-xxl-center {
        align-content: center !important
    }

    .align-content-xxl-between {
        align-content: space-between !important
    }

    .align-content-xxl-around {
        align-content: space-around !important
    }

    .align-content-xxl-stretch {
        align-content: stretch !important
    }

    .align-self-xxl-auto {
        align-self: auto !important
    }

    .align-self-xxl-start {
        align-self: flex-start !important
    }

    .align-self-xxl-end {
        align-self: flex-end !important
    }

    .align-self-xxl-center {
        align-self: center !important
    }

    .align-self-xxl-baseline {
        align-self: baseline !important
    }

    .align-self-xxl-stretch {
        align-self: stretch !important
    }

    .order-xxl-first {
        order: -1 !important
    }

    .order-xxl-0 {
        order: 0 !important
    }

    .order-xxl-1 {
        order: 1 !important
    }

    .order-xxl-2 {
        order: 2 !important
    }

    .order-xxl-3 {
        order: 3 !important
    }

    .order-xxl-4 {
        order: 4 !important
    }

    .order-xxl-5 {
        order: 5 !important
    }

    .order-xxl-last {
        order: 6 !important
    }

    .m-xxl-0 {
        margin: 0 !important
    }

    .m-xxl-1 {
        margin: .25rem !important
    }

    .m-xxl-2 {
        margin: .5rem !important
    }

    .m-xxl-3 {
        margin: 1rem !important
    }

    .m-xxl-4 {
        margin: 1.5rem !important
    }

    .m-xxl-5 {
        margin: 3rem !important
    }

    .m-xxl-auto {
        margin: auto !important
    }

    .mx-xxl-0 {
        margin-right: 0 !important;
        margin-left: 0 !important
    }

    .mx-xxl-1 {
        margin-right: .25rem !important;
        margin-left: .25rem !important
    }

    .mx-xxl-2 {
        margin-right: .5rem !important;
        margin-left: .5rem !important
    }

    .mx-xxl-3 {
        margin-right: 1rem !important;
        margin-left: 1rem !important
    }

    .mx-xxl-4 {
        margin-right: 1.5rem !important;
        margin-left: 1.5rem !important
    }

    .mx-xxl-5 {
        margin-right: 3rem !important;
        margin-left: 3rem !important
    }

    .mx-xxl-auto {
        margin-right: auto !important;
        margin-left: auto !important
    }

    .my-xxl-0 {
        margin-top: 0 !important;
        margin-bottom: 0 !important
    }

    .my-xxl-1 {
        margin-top: .25rem !important;
        margin-bottom: .25rem !important
    }

    .my-xxl-2 {
        margin-top: .5rem !important;
        margin-bottom: .5rem !important
    }

    .my-xxl-3 {
        margin-top: 1rem !important;
        margin-bottom: 1rem !important
    }

    .my-xxl-4 {
        margin-top: 1.5rem !important;
        margin-bottom: 1.5rem !important
    }

    .my-xxl-5 {
        margin-top: 3rem !important;
        margin-bottom: 3rem !important
    }

    .my-xxl-auto {
        margin-top: auto !important;
        margin-bottom: auto !important
    }

    .mt-xxl-0 {
        margin-top: 0 !important
    }

    .mt-xxl-1 {
        margin-top: .25rem !important
    }

    .mt-xxl-2 {
        margin-top: .5rem !important
    }

    .mt-xxl-3 {
        margin-top: 1rem !important
    }

    .mt-xxl-4 {
        margin-top: 1.5rem !important
    }

    .mt-xxl-5 {
        margin-top: 3rem !important
    }

    .mt-xxl-auto {
        margin-top: auto !important
    }

    .me-xxl-0 {
        margin-right: 0 !important
    }

    .me-xxl-1 {
        margin-right: .25rem !important
    }

    .me-xxl-2 {
        margin-right: .5rem !important
    }

    .me-xxl-3 {
        margin-right: 1rem !important
    }

    .me-xxl-4 {
        margin-right: 1.5rem !important
    }

    .me-xxl-5 {
        margin-right: 3rem !important
    }

    .me-xxl-auto {
        margin-right: auto !important
    }

    .mb-xxl-0 {
        margin-bottom: 0 !important
    }

    .mb-xxl-1 {
        margin-bottom: .25rem !important
    }

    .mb-xxl-2 {
        margin-bottom: .5rem !important
    }

    .mb-xxl-3 {
        margin-bottom: 1rem !important
    }

    .mb-xxl-4 {
        margin-bottom: 1.5rem !important
    }

    .mb-xxl-5 {
        margin-bottom: 3rem !important
    }

    .mb-xxl-auto {
        margin-bottom: auto !important
    }

    .ms-xxl-0 {
        margin-left: 0 !important
    }

    .ms-xxl-1 {
        margin-left: .25rem !important
    }

    .ms-xxl-2 {
        margin-left: .5rem !important
    }

    .ms-xxl-3 {
        margin-left: 1rem !important
    }

    .ms-xxl-4 {
        margin-left: 1.5rem !important
    }

    .ms-xxl-5 {
        margin-left: 3rem !important
    }

    .ms-xxl-auto {
        margin-left: auto !important
    }

    .p-xxl-0 {
        padding: 0 !important
    }

    .p-xxl-1 {
        padding: .25rem !important
    }

    .p-xxl-2 {
        padding: .5rem !important
    }

    .p-xxl-3 {
        padding: 1rem !important
    }

    .p-xxl-4 {
        padding: 1.5rem !important
    }

    .p-xxl-5 {
        padding: 3rem !important
    }

    .px-xxl-0 {
        padding-right: 0 !important;
        padding-left: 0 !important
    }

    .px-xxl-1 {
        padding-right: .25rem !important;
        padding-left: .25rem !important
    }

    .px-xxl-2 {
        padding-right: .5rem !important;
        padding-left: .5rem !important
    }

    .px-xxl-3 {
        padding-right: 1rem !important;
        padding-left: 1rem !important
    }

    .px-xxl-4 {
        padding-right: 1.5rem !important;
        padding-left: 1.5rem !important
    }

    .px-xxl-5 {
        padding-right: 3rem !important;
        padding-left: 3rem !important
    }

    .py-xxl-0 {
        padding-top: 0 !important;
        padding-bottom: 0 !important
    }

    .py-xxl-1 {
        padding-top: .25rem !important;
        padding-bottom: .25rem !important
    }

    .py-xxl-2 {
        padding-top: .5rem !important;
        padding-bottom: .5rem !important
    }

    .py-xxl-3 {
        padding-top: 1rem !important;
        padding-bottom: 1rem !important
    }

    .py-xxl-4 {
        padding-top: 1.5rem !important;
        padding-bottom: 1.5rem !important
    }

    .py-xxl-5 {
        padding-top: 3rem !important;
        padding-bottom: 3rem !important
    }

    .pt-xxl-0 {
        padding-top: 0 !important
    }

    .pt-xxl-1 {
        padding-top: .25rem !important
    }

    .pt-xxl-2 {
        padding-top: .5rem !important
    }

    .pt-xxl-3 {
        padding-top: 1rem !important
    }

    .pt-xxl-4 {
        padding-top: 1.5rem !important
    }

    .pt-xxl-5 {
        padding-top: 3rem !important
    }

    .pe-xxl-0 {
        padding-right: 0 !important
    }

    .pe-xxl-1 {
        padding-right: .25rem !important
    }

    .pe-xxl-2 {
        padding-right: .5rem !important
    }

    .pe-xxl-3 {
        padding-right: 1rem !important
    }

    .pe-xxl-4 {
        padding-right: 1.5rem !important
    }

    .pe-xxl-5 {
        padding-right: 3rem !important
    }

    .pb-xxl-0 {
        padding-bottom: 0 !important
    }

    .pb-xxl-1 {
        padding-bottom: .25rem !important
    }

    .pb-xxl-2 {
        padding-bottom: .5rem !important
    }

    .pb-xxl-3 {
        padding-bottom: 1rem !important
    }

    .pb-xxl-4 {
        padding-bottom: 1.5rem !important
    }

    .pb-xxl-5 {
        padding-bottom: 3rem !important
    }

    .ps-xxl-0 {
        padding-left: 0 !important
    }

    .ps-xxl-1 {
        padding-left: .25rem !important
    }

    .ps-xxl-2 {
        padding-left: .5rem !important
    }

    .ps-xxl-3 {
        padding-left: 1rem !important
    }

    .ps-xxl-4 {
        padding-left: 1.5rem !important
    }

    .ps-xxl-5 {
        padding-left: 3rem !important
    }

    .text-xxl-start {
        text-align: left !important
    }

    .text-xxl-end {
        text-align: right !important
    }

    .text-xxl-center {
        text-align: center !important
    }
}

@media (min-width:1200px) {
    .fs-1 {
        font-size: 2.5rem !important
    }

    .fs-2 {
        font-size: 2rem !important
    }

    .fs-3 {
        font-size: 1.75rem !important
    }

    .fs-4 {
        font-size: 1.5rem !important
    }
}

@media print {
    .d-print-inline {
        display: inline !important
    }

    .d-print-inline-block {
        display: inline-block !important
    }

    .d-print-block {
        display: block !important
    }

    .d-print-grid {
        display: grid !important
    }

    .d-print-table {
        display: table !important
    }

    .d-print-table-row {
        display: table-row !important
    }

    .d-print-table-cell {
        display: table-cell !important
    }

    .d-print-flex {
        display: flex !important
    }

    .d-print-inline-flex {
        display: inline-flex !important
    }

    .d-print-none {
        display: none !important
    }
}

/*# sourceMappingURL=bootstrap.min.css.map */
</style>


<style>
    .glightbox-container{width:100%;height:100%;position:fixed;top:0;left:0;z-index:999999!important;overflow:hidden;-ms-touch-action:none;touch-action:none;-webkit-text-size-adjust:100%;-webkit-backface-visibility:hidden;outline:0;overflow:hidden}.glightbox-container.inactive{display:none}.glightbox-container .gcontainer{position:relative;width:100%;height:100%;z-index:9999;overflow:hidden}.glightbox-container .gslider{-webkit-transition:-webkit-transform .4s ease;transition:-webkit-transform .4s ease;transition:transform .4s ease;transition:transform .4s ease,-webkit-transform .4s ease;height:100%;left:0;top:0;width:100%;position:relative;overflow:hidden;display:-webkit-box!important;display:-ms-flexbox!important;display:flex!important;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}.glightbox-container .gslide{width:100%;position:absolute;opacity:1;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;opacity:0}.glightbox-container .gslide.current{opacity:1;z-index:99999;position:relative}.glightbox-container .gslide.prev{opacity:1;z-index:9999}.glightbox-container .gslide-inner-content{width:100%}.glightbox-container .ginner-container{position:relative;width:100%;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;max-width:100%;margin:auto;height:100vh}.glightbox-container .ginner-container.gvideo-container{width:100%}.glightbox-container .ginner-container.desc-bottom,.glightbox-container .ginner-container.desc-top{-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column}.glightbox-container .ginner-container.desc-left,.glightbox-container .ginner-container.desc-right{max-width:100%!important}.gslide iframe,.gslide video{outline:0!important;border:none;min-height:165px;-webkit-overflow-scrolling:touch;overflow-scrolling:touch;-ms-touch-action:auto;touch-action:auto}.gslide-image{-webkit-box-align:center;-ms-flex-align:center;align-items:center}.gslide-image img{max-height:100vh;display:block;max-width:100%;margin:0;padding:0;float:none;outline:0;border:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;max-width:100vw;width:auto;height:auto;-o-object-fit:cover;object-fit:cover;-ms-touch-action:none;touch-action:none;margin:auto;min-width:200px}.desc-bottom .gslide-image img,.desc-top .gslide-image img{width:auto}.desc-left .gslide-image img,.desc-right .gslide-image img{width:auto;max-width:100%}.gslide-image img.zoomable{position:relative}.gslide-image img.dragging{cursor:-webkit-grabbing!important;cursor:grabbing!important;-webkit-transition:none;transition:none}.gslide-video{width:100%;max-width:100%;position:relative;width:100vh;max-width:100vh;width:100%!important}.gslide-video .gvideo-wrapper{width:100%;margin:auto}.gslide-video::before{content:'';display:block;position:absolute;width:100%;height:100%;background:rgba(255,0,0,.34);display:none}.gslide-video.playing::before{display:none}.gslide-video.fullscreen{max-width:100%!important;min-width:100%;height:80vh}.gslide-video.fullscreen video{max-width:100%!important;width:100%!important}.gslide-inline{background:#fff;text-align:left;max-height:calc(100vh - 40px);overflow:auto;max-width:100%}.gslide-inline .ginlined-content{padding:20px;width:100%}.ginlined-content{overflow:auto;display:block!important;opacity:1}.gslide-external{display:-webkit-box;display:-ms-flexbox;display:flex;width:100%;min-width:100%;background:#fff;padding:0;overflow:auto;max-height:75vh;height:100%}.gslide-media{display:block;display:-webkit-inline-box;display:-ms-inline-flexbox;display:inline-flex;display:-webkit-box;display:-ms-flexbox;display:flex;width:auto}.zoomed .gslide-media{-webkit-box-shadow:none!important;box-shadow:none!important}.desc-bottom .gslide-media,.desc-top .gslide-media{margin:0 auto;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column}.gslide-description{position:relative}.gslide-description.description-left,.gslide-description.description-right{max-width:100%}.gslide-description.description-bottom,.gslide-description.description-top{margin:0 auto;width:100%}.gslide-description p{margin-bottom:12px}.gslide-description p::last-child{margin-bottom:0}.zoomed .gslide-description{display:none}.glightbox-mobile .glightbox-container .gslide-description{height:auto!important;width:100%;background:0 0;position:absolute;bottom:15px;padding:19px 11px;max-width:100vw!important;-webkit-box-ordinal-group:3!important;-ms-flex-order:2!important;order:2!important;max-height:78vh;overflow:auto!important;background:-webkit-gradient(linear,left top,left bottom,from(rgba(0,0,0,0)),to(rgba(0,0,0,.75)));background:linear-gradient(to bottom,rgba(0,0,0,0) 0,rgba(0,0,0,.75) 100%);-webkit-transition:opacity .3s linear;transition:opacity .3s linear;padding-bottom:50px}.glightbox-mobile .glightbox-container .gslide-title{color:#fff;font-size:1em}.glightbox-mobile .glightbox-container .gslide-desc{color:#a1a1a1}.glightbox-mobile .glightbox-container .gslide-desc a{color:#fff;font-weight:700}.glightbox-mobile .glightbox-container .gslide-desc *{color:inherit}.glightbox-mobile .glightbox-container .gslide-desc string{color:#fff}.glightbox-mobile .glightbox-container .gslide-desc .desc-more{color:#fff;opacity:.4}.gdesc-open .gslide-media{-webkit-transition:opacity .5s ease;transition:opacity .5s ease;opacity:.4}.gdesc-open .gdesc-inner{padding-bottom:30px}.gdesc-closed .gslide-media{-webkit-transition:opacity .5s ease;transition:opacity .5s ease;opacity:1}.greset{-webkit-transition:all .3s ease;transition:all .3s ease}.gabsolute{position:absolute}.grelative{position:relative}.glightbox-desc{display:none!important}.glightbox-open{overflow:hidden}.gloader{height:25px;width:25px;-webkit-animation:lightboxLoader .8s infinite linear;animation:lightboxLoader .8s infinite linear;border:2px solid #fff;border-right-color:transparent;border-radius:50%;position:absolute;display:block;z-index:9999;left:0;right:0;margin:0 auto;top:47%}.goverlay{width:100%;height:100%;position:fixed;top:0;left:0;background:#000;will-change:opacity}.glightbox-mobile .goverlay{background:#000}.gclose,.gnext,.gprev{background-repeat:no-repeat;z-index:99999;cursor:pointer;width:26px;height:44px;display:block;background-position:0 0;border:none}.gclose svg,.gnext svg,.gprev svg{display:block;width:100%;height:auto}.gclose.disabled,.gnext.disabled,.gprev.disabled{opacity:.1}.gclose .garrow,.gnext .garrow,.gprev .garrow{stroke:#fff}iframe.wait-autoplay{opacity:0}.glightbox-closing .gclose,.glightbox-closing .gnext,.glightbox-closing .gprev{opacity:0!important}.glightbox-clean .gslide-description,.glightbox-modern .gslide-description{background:#fff}.glightbox-clean .gdesc-inner,.glightbox-modern .gdesc-inner{padding:22px 20px}.glightbox-clean .gslide-title,.glightbox-modern .gslide-title{font-size:1em;font-weight:400;font-family:arial;color:#000;margin-bottom:19px;line-height:1.4em}.glightbox-clean .gslide-desc,.glightbox-modern .gslide-desc{font-size:.86em;margin-bottom:0;font-family:arial;line-height:1.4em}.glightbox-clean .gslide-video,.glightbox-modern .gslide-video{background:#000}.glightbox-clean .gclose,.glightbox-clean .gnext,.glightbox-clean .gprev,.glightbox-modern .gclose,.glightbox-modern .gnext,.glightbox-modern .gprev{background-color:rgba(0,0,0,.12)}.glightbox-clean .gclose:hover,.glightbox-clean .gnext:hover,.glightbox-clean .gprev:hover,.glightbox-modern .gclose:hover,.glightbox-modern .gnext:hover,.glightbox-modern .gprev:hover{background-color:rgba(0,0,0,.2)}.glightbox-clean .gclose path,.glightbox-clean .gnext path,.glightbox-clean .gprev path,.glightbox-modern .gclose path,.glightbox-modern .gnext path,.glightbox-modern .gprev path{fill:#fff}.glightbox-clean button:focus:not(.focused):not(.disabled),.glightbox-modern button:focus:not(.focused):not(.disabled){outline:0}.glightbox-clean .gprev,.glightbox-modern .gprev{position:absolute;top:-100%;left:30px;width:40px;height:56px}.glightbox-clean .gnext,.glightbox-modern .gnext{position:absolute;top:-100%;right:30px;width:40px;height:56px}.glightbox-clean .gclose,.glightbox-modern .gclose{width:35px;height:35px;top:15px;right:10px;position:absolute;opacity:.7;background-position:-59px 2px}.glightbox-clean .gclose svg,.glightbox-modern .gclose svg{width:20px}.glightbox-clean .gclose:hover,.glightbox-modern .gclose:hover{opacity:1}.gfadeIn{-webkit-animation:gfadeIn .5s ease;animation:gfadeIn .5s ease}.gfadeOut{-webkit-animation:gfadeOut .5s ease;animation:gfadeOut .5s ease}.gslideOutLeft{-webkit-animation:gslideOutLeft .3s ease;animation:gslideOutLeft .3s ease}.gslideInLeft{-webkit-animation:gslideInLeft .3s ease;animation:gslideInLeft .3s ease}.gslideOutRight{-webkit-animation:gslideOutRight .3s ease;animation:gslideOutRight .3s ease}.gslideInRight{-webkit-animation:gslideInRight .3s ease;animation:gslideInRight .3s ease}.gzoomIn{-webkit-animation:gzoomIn .5s ease;animation:gzoomIn .5s ease}.gzoomOut{-webkit-animation:gzoomOut .5s ease;animation:gzoomOut .5s ease}@-webkit-keyframes lightboxLoader{0%{-webkit-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes lightboxLoader{0%{-webkit-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@-webkit-keyframes gfadeIn{from{opacity:0}to{opacity:1}}@keyframes gfadeIn{from{opacity:0}to{opacity:1}}@-webkit-keyframes gfadeOut{from{opacity:1}to{opacity:0}}@keyframes gfadeOut{from{opacity:1}to{opacity:0}}@-webkit-keyframes gslideInLeft{from{opacity:0;-webkit-transform:translate3d(-60%,0,0);transform:translate3d(-60%,0,0)}to{visibility:visible;-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0);opacity:1}}@keyframes gslideInLeft{from{opacity:0;-webkit-transform:translate3d(-60%,0,0);transform:translate3d(-60%,0,0)}to{visibility:visible;-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0);opacity:1}}@-webkit-keyframes gslideOutLeft{from{opacity:1;visibility:visible;-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}to{-webkit-transform:translate3d(-60%,0,0);transform:translate3d(-60%,0,0);opacity:0;visibility:hidden}}@keyframes gslideOutLeft{from{opacity:1;visibility:visible;-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}to{-webkit-transform:translate3d(-60%,0,0);transform:translate3d(-60%,0,0);opacity:0;visibility:hidden}}@-webkit-keyframes gslideInRight{from{opacity:0;visibility:visible;-webkit-transform:translate3d(60%,0,0);transform:translate3d(60%,0,0)}to{-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0);opacity:1}}@keyframes gslideInRight{from{opacity:0;visibility:visible;-webkit-transform:translate3d(60%,0,0);transform:translate3d(60%,0,0)}to{-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0);opacity:1}}@-webkit-keyframes gslideOutRight{from{opacity:1;visibility:visible;-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}to{-webkit-transform:translate3d(60%,0,0);transform:translate3d(60%,0,0);opacity:0}}@keyframes gslideOutRight{from{opacity:1;visibility:visible;-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}to{-webkit-transform:translate3d(60%,0,0);transform:translate3d(60%,0,0);opacity:0}}@-webkit-keyframes gzoomIn{from{opacity:0;-webkit-transform:scale3d(.3,.3,.3);transform:scale3d(.3,.3,.3)}to{opacity:1}}@keyframes gzoomIn{from{opacity:0;-webkit-transform:scale3d(.3,.3,.3);transform:scale3d(.3,.3,.3)}to{opacity:1}}@-webkit-keyframes gzoomOut{from{opacity:1}50%{opacity:0;-webkit-transform:scale3d(.3,.3,.3);transform:scale3d(.3,.3,.3)}to{opacity:0}}@keyframes gzoomOut{from{opacity:1}50%{opacity:0;-webkit-transform:scale3d(.3,.3,.3);transform:scale3d(.3,.3,.3)}to{opacity:0}}@media (min-width:769px){.glightbox-container .ginner-container{width:auto;height:auto;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row}.glightbox-container .ginner-container.desc-top .gslide-description{-webkit-box-ordinal-group:1;-ms-flex-order:0;order:0}.glightbox-container .ginner-container.desc-top .gslide-image,.glightbox-container .ginner-container.desc-top .gslide-image img{-webkit-box-ordinal-group:2;-ms-flex-order:1;order:1}.glightbox-container .ginner-container.desc-left .gslide-description{-webkit-box-ordinal-group:1;-ms-flex-order:0;order:0}.glightbox-container .ginner-container.desc-left .gslide-image{-webkit-box-ordinal-group:2;-ms-flex-order:1;order:1}.gslide-image img{max-height:97vh;max-width:calc(100% - 20px);max-width:100%}.gslide-image img.zoomable{cursor:-webkit-zoom-in;cursor:zoom-in}.zoomed .gslide-image img.zoomable{cursor:-webkit-grab;cursor:grab}.gslide-inline{max-height:95vh}.gslide-external{max-height:100vh}.gslide-description.description-left,.gslide-description.description-right{max-width:275px}.glightbox-open{height:auto}.goverlay{background:rgba(0,0,0,.92)}.glightbox-clean .gslide-media,.glightbox-modern .gslide-media{-webkit-box-shadow:1px 2px 9px 0 rgba(0,0,0,.65);box-shadow:1px 2px 9px 0 rgba(0,0,0,.65)}.glightbox-clean .gprev,.glightbox-modern .gprev{top:45%}.glightbox-clean .gnext,.glightbox-modern .gnext{top:45%}}@media (min-width:992px){.glightbox-clean .gclose,.glightbox-modern .gclose{right:20px}}@media screen and (max-height:420px){.goverlay{background:#000}}
</style>

<style>
    /*--------------------------------

LineIcons Web Font
Author: lineicons.com

-------------------------------- */
@font-face {
  font-family: 'LineIcons';
  src: url('../fonts/LineIcons.eot');
  src: url('../fonts/LineIcons.eot') format('embedded-opentype'), url('../fonts/LineIcons.woff2') format('woff2'), url('../fonts/LineIcons.woff') format('woff'), url('../fonts/LineIcons.ttf') format('truetype'), url('../fonts/LineIcons.svg') format('svg');
  font-weight: normal;
  font-style: normal;
}
/*------------------------
	base class definition
-------------------------*/
.lni {
  display: inline-block;
  font: normal normal normal 1em/1 'LineIcons';
  color: inherit;
  flex-shrink: 0;
  speak: none;
  text-transform: none;
  line-height: 1;
  vertical-align: -.125em;
  /* Better Font Rendering */
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
/*------------------------
  change icon size
-------------------------*/
/* relative units */
.lni-sm {
  font-size: 0.8em;
}
.lni-lg {
  font-size: 1.2em;
}
/* absolute units */
.lni-16 {
  font-size: 16px;
}
.lni-32 {
  font-size: 32px;
}

/*------------------------
  spinning icons
-------------------------*/
.lni-is-spinning {
  animation: lni-spin 1s infinite linear;
}
@keyframes lni-spin {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
/*------------------------
  rotated/flipped icons
-------------------------*/
.lni-rotate-90 {
  transform: rotate(90deg);
}
.lni-rotate-180 {
  transform: rotate(180deg);
}
.lni-rotate-270 {
  transform: rotate(270deg);
}
.lni-flip-y {
  transform: scaleY(-1);
}
.lni-flip-x {
  transform: scaleX(-1);
}
/*------------------------
	icons
-------------------------*/

.lni-500px::before {
  content: "\ea01";
}

.lni-add-files::before {
  content: "\ea03";
}

.lni-adobe::before {
  content: "\ea06";
}

.lni-agenda::before {
  content: "\ea04";
}

.lni-airbnb::before {
  content: "\ea02";
}

.lni-alarm::before {
  content: "\ea07";
}

.lni-alarmclock::before {
  content: "\ea08";
}

.lni-altassian::before {
  content: "\ea05";
}

.lni-amazon-original::before {
  content: "\ea09";
}

.lni-amazon-pay::before {
  content: "\ea0a";
}

.lni-amazon::before {
  content: "\ea0b";
}

.lni-ambulance::before {
  content: "\ea0c";
}

.lni-amex::before {
  content: "\ea0d";
}

.lni-anchor::before {
  content: "\ea0e";
}

.lni-android-original::before {
  content: "\ea0f";
}

.lni-android::before {
  content: "\ea10";
}

.lni-angellist::before {
  content: "\ea11";
}

.lni-angle-double-down::before {
  content: "\ea12";
}

.lni-angle-double-left::before {
  content: "\ea13";
}

.lni-angle-double-right::before {
  content: "\ea14";
}

.lni-angle-double-up::before {
  content: "\ea15";
}

.lni-angular::before {
  content: "\ea16";
}

.lni-apartment::before {
  content: "\ea17";
}

.lni-app-store::before {
  content: "\ea18";
}

.lni-apple-music::before {
  content: "\ea1a";
}

.lni-apple-pay::before {
  content: "\ea19";
}

.lni-apple::before {
  content: "\ea1b";
}

.lni-archive::before {
  content: "\ea1c";
}

.lni-arrow-down-circle::before {
  content: "\ea1e";
}

.lni-arrow-down::before {
  content: "\ea1d";
}

.lni-arrow-left-circle::before {
  content: "\ea1f";
}

.lni-arrow-left::before {
  content: "\ea20";
}

.lni-arrow-right-circle::before {
  content: "\ea21";
}

.lni-arrow-right::before {
  content: "\ea22";
}

.lni-arrow-top-left::before {
  content: "\ea23";
}

.lni-arrow-top-right::before {
  content: "\ea24";
}

.lni-arrow-up-circle::before {
  content: "\ea25";
}

.lni-arrow-up::before {
  content: "\ea26";
}

.lni-arrows-horizontal::before {
  content: "\ea27";
}

.lni-arrows-vertical::before {
  content: "\ea28";
}

.lni-aws::before {
  content: "\ea29";
}

.lni-azure::before {
  content: "\ea2a";
}

.lni-backward::before {
  content: "\ea2b";
}

.lni-baloon::before {
  content: "\ea2c";
}

.lni-ban::before {
  content: "\ea2d";
}

.lni-bar-chart::before {
  content: "\ea2e";
}

.lni-basketball::before {
  content: "\ea2f";
}

.lni-behance-original::before {
  content: "\ea30";
}

.lni-behance::before {
  content: "\ea31";
}

.lni-bi-cycle::before {
  content: "\ea32";
}

.lni-bitbucket::before {
  content: "\ea33";
}

.lni-bitcoin::before {
  content: "\ea34";
}

.lni-blackboard::before {
  content: "\ea35";
}

.lni-blogger::before {
  content: "\ea36";
}

.lni-bluetooth-original::before {
  content: "\ea37";
}

.lni-bluetooth::before {
  content: "\ea38";
}

.lni-bold::before {
  content: "\ea39";
}

.lni-bolt-alt::before {
  content: "\ea3b";
}

.lni-bolt::before {
  content: "\ea3a";
}

.lni-book::before {
  content: "\ea3c";
}

.lni-bookmark-alt::before {
  content: "\ea3e";
}

.lni-bookmark::before {
  content: "\ea3d";
}

.lni-bootstrap::before {
  content: "\ea3f";
}

.lni-bricks::before {
  content: "\ea40";
}

.lni-bridge::before {
  content: "\ea41";
}

.lni-briefcase::before {
  content: "\ea42";
}

.lni-brush-alt::before {
  content: "\ea43";
}

.lni-brush::before {
  content: "\ea44";
}

.lni-btc::before {
  content: "\ea45";
}

.lni-bubble::before {
  content: "\ea46";
}

.lni-bug::before {
  content: "\ea47";
}

.lni-bulb::before {
  content: "\ea48";
}

.lni-bullhorn::before {
  content: "\ea49";
}

.lni-burger::before {
  content: "\ea4a";
}

.lni-bus::before {
  content: "\ea4b";
}

.lni-cake::before {
  content: "\ea4c";
}

.lni-calculator::before {
  content: "\ea4d";
}

.lni-calendar::before {
  content: "\ea4e";
}

.lni-camera::before {
  content: "\ea4f";
}

.lni-candy-cane::before {
  content: "\ea50";
}

.lni-candy::before {
  content: "\ea51";
}

.lni-capsule::before {
  content: "\ea52";
}

.lni-car-alt::before {
  content: "\ea53";
}

.lni-car::before {
  content: "\ea54";
}

.lni-caravan::before {
  content: "\ea55";
}

.lni-cart-full::before {
  content: "\ea56";
}

.lni-cart::before {
  content: "\ea57";
}

.lni-certificate::before {
  content: "\ea58";
}

.lni-check-box::before {
  content: "\ea59";
}

.lni-checkmark-circle::before {
  content: "\ea5a";
}

.lni-checkmark::before {
  content: "\ea5b";
}

.lni-chef-hat::before {
  content: "\ea5c";
}

.lni-chevron-down-circle::before {
  content: "\ea5d";
}

.lni-chevron-down::before {
  content: "\ea5e";
}

.lni-chevron-left-circle::before {
  content: "\ea5f";
}

.lni-chevron-left::before {
  content: "\ea60";
}

.lni-chevron-right-circle::before {
  content: "\ea61";
}

.lni-chevron-right::before {
  content: "\ea62";
}

.lni-chevron-up-circle::before {
  content: "\ea63";
}

.lni-chevron-up::before {
  content: "\ea64";
}

.lni-chrome::before {
  content: "\ea65";
}

.lni-chromecast::before {
  content: "\ea66";
}

.lni-circle-minus::before {
  content: "\ea67";
}

.lni-circle-plus::before {
  content: "\ea68";
}

.lni-clipboard::before {
  content: "\ea69";
}

.lni-close::before {
  content: "\ea6b";
}

.lni-cloud-check::before {
  content: "\ea6a";
}

.lni-cloud-download::before {
  content: "\ea6c";
}

.lni-cloud-network::before {
  content: "\ea6d";
}

.lni-cloud-sync::before {
  content: "\ea6e";
}

.lni-cloud-upload::before {
  content: "\ea6f";
}

.lni-cloud::before {
  content: "\ea70";
}

.lni-cloudflare::before {
  content: "\ea71";
}

.lni-cloudy-sun::before {
  content: "\ea72";
}

.lni-code-alt::before {
  content: "\ea73";
}

.lni-code::before {
  content: "\ea74";
}

.lni-codepen::before {
  content: "\ea75";
}

.lni-coffee-cup::before {
  content: "\ea76";
}

.lni-cog::before {
  content: "\ea77";
}

.lni-cogs::before {
  content: "\ea78";
}

.lni-coin::before {
  content: "\ea79";
}

.lni-comments-alt::before {
  content: "\ea7a";
}

.lni-comments-reply::before {
  content: "\ea7b";
}

.lni-comments::before {
  content: "\ea7c";
}

.lni-compass::before {
  content: "\ea7d";
}

.lni-connectdevelop::before {
  content: "\ea7e";
}

.lni-construction-hammer::before {
  content: "\ea7f";
}

.lni-construction::before {
  content: "\ea80";
}

.lni-control-panel::before {
  content: "\ea81";
}

.lni-cool::before {
  content: "\ea82";
}

.lni-cpanel::before {
  content: "\ea83";
}

.lni-creative-commons::before {
  content: "\ea84";
}

.lni-credit-cards::before {
  content: "\ea85";
}

.lni-crop::before {
  content: "\ea86";
}

.lni-cross-circle::before {
  content: "\ea87";
}

.lni-crown::before {
  content: "\ea88";
}

.lni-css-3::before {
  content: "\ea89";
}

.lni-cup::before {
  content: "\ea8a";
}

.lni-customer::before {
  content: "\ea8b";
}

.lni-cut::before {
  content: "\ea8c";
}

.lni-dashboard::before {
  content: "\ea8d";
}

.lni-database::before {
  content: "\ea8e";
}

.lni-delivery::before {
  content: "\ea8f";
}

.lni-dev::before {
  content: "\ea90";
}

.lni-diamond-alt::before {
  content: "\ea91";
}

.lni-diamond::before {
  content: "\ea92";
}

.lni-digitalocean::before {
  content: "\ea93";
}

.lni-diners-club::before {
  content: "\ea94";
}

.lni-dinner::before {
  content: "\ea95";
}

.lni-direction-alt::before {
  content: "\ea96";
}

.lni-direction-ltr::before {
  content: "\ea97";
}

.lni-direction-rtl::before {
  content: "\ea98";
}

.lni-direction::before {
  content: "\ea99";
}

.lni-discord::before {
  content: "\ea9a";
}

.lni-discover::before {
  content: "\ea9b";
}

.lni-display-alt::before {
  content: "\ea9c";
}

.lni-display::before {
  content: "\ea9d";
}

.lni-docker::before {
  content: "\ea9e";
}

.lni-dollar::before {
  content: "\ea9f";
}

.lni-domain::before {
  content: "\eaa0";
}

.lni-download::before {
  content: "\eaa1";
}

.lni-dribbble::before {
  content: "\eaa2";
}

.lni-drop::before {
  content: "\eaa3";
}

.lni-dropbox-original::before {
  content: "\eaa4";
}

.lni-dropbox::before {
  content: "\eaa5";
}

.lni-drupal-original::before {
  content: "\eaa8";
}

.lni-drupal::before {
  content: "\eaa6";
}

.lni-dumbbell::before {
  content: "\eaa7";
}

.lni-edge::before {
  content: "\eaa9";
}

.lni-empty-file::before {
  content: "\eaaa";
}

.lni-enter::before {
  content: "\eaab";
}

.lni-envato::before {
  content: "\eaac";
}

.lni-envelope::before {
  content: "\eaad";
}

.lni-eraser::before {
  content: "\eaae";
}

.lni-euro::before {
  content: "\eaaf";
}

.lni-exit-down::before {
  content: "\eab0";
}

.lni-exit-up::before {
  content: "\eab1";
}

.lni-exit::before {
  content: "\eab5";
}

.lni-eye::before {
  content: "\eab2";
}

.lni-facebook-filled::before {
  content: "\eab3";
}

.lni-facebook-line::before {
  content: "\eab4";
}

.lni-facebook-messenger::before {
  content: "\eab6";
}

.lni-facebook-original::before {
  content: "\eab7";
}

.lni-facebook-oval::before {
  content: "\eab8";
}

.lni-figma::before {
  content: "\eab9";
}

.lni-files::before {
  content: "\eaba";
}

.lni-firefox-original::before {
  content: "\eabb";
}

.lni-firefox::before {
  content: "\eabc";
}

.lni-fireworks::before {
  content: "\eabd";
}

.lni-first-aid::before {
  content: "\eabe";
}

.lni-flag-alt::before {
  content: "\eabf";
}

.lni-flag::before {
  content: "\eac0";
}

.lni-flags::before {
  content: "\eac1";
}

.lni-flicker::before {
  content: "\eac2";
}

.lni-flower::before {
  content: "\eac3";
}

.lni-folder::before {
  content: "\eac4";
}

.lni-forward::before {
  content: "\eac6";
}

.lni-frame-expand::before {
  content: "\eac5";
}

.lni-fresh-juice::before {
  content: "\eac7";
}

.lni-friendly::before {
  content: "\eac8";
}

.lni-full-screen::before {
  content: "\eac9";
}

.lni-funnel::before {
  content: "\eaca";
}

.lni-gallery::before {
  content: "\eacb";
}

.lni-game::before {
  content: "\eacc";
}

.lni-gatsby::before {
  content: "\eacd";
}

.lni-gift::before {
  content: "\ead0";
}

.lni-git::before {
  content: "\eace";
}

.lni-github-original::before {
  content: "\eacf";
}

.lni-github::before {
  content: "\ead4";
}

.lni-goodreads::before {
  content: "\ead1";
}

.lni-google-drive::before {
  content: "\ead2";
}

.lni-google-pay::before {
  content: "\ead3";
}

.lni-google-wallet::before {
  content: "\ead5";
}

.lni-google::before {
  content: "\ead6";
}

.lni-graduation::before {
  content: "\ead7";
}

.lni-graph::before {
  content: "\ead8";
}

.lni-grid-alt::before {
  content: "\eadc";
}

.lni-grid::before {
  content: "\ead9";
}

.lni-grow::before {
  content: "\eada";
}

.lni-hacker-news::before {
  content: "\eadb";
}

.lni-hammer::before {
  content: "\eadd";
}

.lni-hand::before {
  content: "\eade";
}

.lni-handshake::before {
  content: "\eadf";
}

.lni-happy::before {
  content: "\eae0";
}

.lni-harddrive::before {
  content: "\eae1";
}

.lni-headphone-alt::before {
  content: "\eae2";
}

.lni-headphone::before {
  content: "\eae3";
}

.lni-heart-filled::before {
  content: "\eae4";
}

.lni-heart-monitor::before {
  content: "\eae5";
}

.lni-heart::before {
  content: "\eae6";
}

.lni-helicopter::before {
  content: "\eae7";
}

.lni-helmet::before {
  content: "\eae8";
}

.lni-help::before {
  content: "\eae9";
}

.lni-highlight-alt::before {
  content: "\eaea";
}

.lni-highlight::before {
  content: "\eaeb";
}

.lni-home::before {
  content: "\eaec";
}

.lni-hospital::before {
  content: "\eaed";
}

.lni-hourglass::before {
  content: "\eaee";
}

.lni-html5::before {
  content: "\eaef";
}

.lni-image::before {
  content: "\eaf0";
}

.lni-imdb::before {
  content: "\eaf1";
}

.lni-inbox::before {
  content: "\eaf2";
}

.lni-indent-decrease::before {
  content: "\eaf3";
}

.lni-indent-increase::before {
  content: "\eaf4";
}

.lni-infinite::before {
  content: "\eaf5";
}

.lni-information::before {
  content: "\eaf6";
}

.lni-instagram-filled::before {
  content: "\eaf7";
}

.lni-instagram-original::before {
  content: "\eaf8";
}

.lni-instagram::before {
  content: "\eaf9";
}

.lni-invention::before {
  content: "\eafa";
}

.lni-invest-monitor::before {
  content: "\eafb";
}

.lni-investment::before {
  content: "\eafc";
}

.lni-island::before {
  content: "\eafd";
}

.lni-italic::before {
  content: "\eafe";
}

.lni-java::before {
  content: "\eaff";
}

.lni-jcb::before {
  content: "\eb00";
}

.lni-joomla-filled::before {
  content: "\eb01";
}

.lni-joomla-line::before {
  content: "\eb02";
}

.lni-js::before {
  content: "\eb03";
}

.lni-jsfiddle::before {
  content: "\eb04";
}

.lni-juice::before {
  content: "\eb05";
}

.lni-key::before {
  content: "\eb06";
}

.lni-keyboard::before {
  content: "\eb07";
}

.lni-keyword-research::before {
  content: "\eb08";
}

.lni-laptop-phone::before {
  content: "\eb09";
}

.lni-laptop::before {
  content: "\eb0a";
}

.lni-laravel::before {
  content: "\eb0b";
}

.lni-layers::before {
  content: "\eb0c";
}

.lni-layout::before {
  content: "\eb0d";
}

.lni-leaf::before {
  content: "\eb0e";
}

.lni-library::before {
  content: "\eb0f";
}

.lni-lifering::before {
  content: "\eb10";
}

.lni-line-dashed::before {
  content: "\eb11";
}

.lni-line-dotted::before {
  content: "\eb12";
}

.lni-line-double::before {
  content: "\eb13";
}

.lni-line-spacing::before {
  content: "\eb14";
}

.lni-line::before {
  content: "\eb15";
}

.lni-lineicons-alt::before {
  content: "\eb16";
}

.lni-lineicons::before {
  content: "\eb17";
}

.lni-link::before {
  content: "\eb18";
}

.lni-linkedin-original::before {
  content: "\eb19";
}

.lni-linkedin::before {
  content: "\eb1a";
}

.lni-list::before {
  content: "\eb1b";
}

.lni-lock-alt::before {
  content: "\eb1c";
}

.lni-lock::before {
  content: "\eb1d";
}

.lni-magento::before {
  content: "\eb1e";
}

.lni-magnet::before {
  content: "\eb1f";
}

.lni-magnifier::before {
  content: "\eb20";
}

.lni-mailchimp::before {
  content: "\eb21";
}

.lni-map-marker::before {
  content: "\eb22";
}

.lni-map::before {
  content: "\eb23";
}

.lni-markdown::before {
  content: "\eb24";
}

.lni-mashroom::before {
  content: "\eb25";
}

.lni-mastercard::before {
  content: "\eb26";
}

.lni-medium::before {
  content: "\eb27";
}

.lni-menu::before {
  content: "\eb28";
}

.lni-mic::before {
  content: "\eb29";
}

.lni-microphone::before {
  content: "\eb2a";
}

.lni-microscope::before {
  content: "\eb2b";
}

.lni-microsoft-edge::before {
  content: "\eb2c";
}

.lni-microsoft::before {
  content: "\eb2d";
}

.lni-minus::before {
  content: "\eb2e";
}

.lni-mobile::before {
  content: "\eb2f";
}

.lni-money-location::before {
  content: "\eb30";
}

.lni-money-protection::before {
  content: "\eb31";
}

.lni-more-alt::before {
  content: "\eb32";
}

.lni-more::before {
  content: "\eb33";
}

.lni-mouse::before {
  content: "\eb34";
}

.lni-move::before {
  content: "\eb35";
}

.lni-music::before {
  content: "\eb36";
}

.lni-netlify::before {
  content: "\eb37";
}

.lni-network::before {
  content: "\eb38";
}

.lni-night::before {
  content: "\eb39";
}

.lni-node-js::before {
  content: "\eb3a";
}

.lni-node::before {
  content: "\eb3b";
}

.lni-notepad::before {
  content: "\eb3c";
}

.lni-npm::before {
  content: "\eb3d";
}

.lni-offer::before {
  content: "\eb3e";
}

.lni-opera::before {
  content: "\eb3f";
}

.lni-package::before {
  content: "\eb40";
}

.lni-page-break::before {
  content: "\eb41";
}

.lni-pagination::before {
  content: "\eb42";
}

.lni-paint-bucket::before {
  content: "\eb43";
}

.lni-paint-roller::before {
  content: "\eb44";
}

.lni-pallet::before {
  content: "\eb45";
}

.lni-paperclip::before {
  content: "\eb46";
}

.lni-patreon::before {
  content: "\eb47";
}

.lni-pause::before {
  content: "\eb48";
}

.lni-paypal-original::before {
  content: "\eb49";
}

.lni-paypal::before {
  content: "\eb4a";
}

.lni-pencil-alt::before {
  content: "\eb4b";
}

.lni-pencil::before {
  content: "\eb4c";
}

.lni-phone-set::before {
  content: "\eb4d";
}

.lni-phone::before {
  content: "\eb4e";
}

.lni-php::before {
  content: "\eb4f";
}

.lni-pie-chart::before {
  content: "\eb50";
}

.lni-pilcrow::before {
  content: "\eb51";
}

.lni-pin::before {
  content: "\eb52";
}

.lni-pinterest::before {
  content: "\eb53";
}

.lni-pizza::before {
  content: "\eb54";
}

.lni-plane::before {
  content: "\eb55";
}

.lni-play-store::before {
  content: "\eb56";
}

.lni-play::before {
  content: "\eb57";
}

.lni-playstation::before {
  content: "\eb58";
}

.lni-plug::before {
  content: "\eb59";
}

.lni-plus::before {
  content: "\eb5a";
}

.lni-pointer-down::before {
  content: "\eb5b";
}

.lni-pointer-left::before {
  content: "\eb5c";
}

.lni-pointer-right::before {
  content: "\eb5d";
}

.lni-pointer-top::before {
  content: "\eb5e";
}

.lni-pointer::before {
  content: "\eb5f";
}

.lni-popup::before {
  content: "\eb60";
}

.lni-postcard::before {
  content: "\eb61";
}

.lni-pound::before {
  content: "\eb63";
}

.lni-power-switch::before {
  content: "\eb62";
}

.lni-printer::before {
  content: "\eb64";
}

.lni-producthunt::before {
  content: "\eb65";
}

.lni-protection::before {
  content: "\eb66";
}

.lni-pulse::before {
  content: "\eb67";
}

.lni-pyramids::before {
  content: "\eb68";
}

.lni-python::before {
  content: "\eb69";
}

.lni-question-circle::before {
  content: "\eb6a";
}

.lni-quora::before {
  content: "\eb6b";
}

.lni-quotation::before {
  content: "\eb6c";
}

.lni-radio-button::before {
  content: "\eb6d";
}

.lni-rain::before {
  content: "\eb6e";
}

.lni-react::before {
  content: "\eb6f";
}

.lni-reddit::before {
  content: "\eb70";
}

.lni-reload::before {
  content: "\eb71";
}

.lni-remove-file::before {
  content: "\eb72";
}

.lni-reply::before {
  content: "\eb73";
}

.lni-restaurant::before {
  content: "\eb74";
}

.lni-revenue::before {
  content: "\eb75";
}

.lni-road::before {
  content: "\eb76";
}

.lni-rocket::before {
  content: "\eb77";
}

.lni-rss-feed::before {
  content: "\eb78";
}

.lni-ruler-alt::before {
  content: "\eb79";
}

.lni-ruler-pencil::before {
  content: "\eb7a";
}

.lni-ruler::before {
  content: "\eb7b";
}

.lni-rupee::before {
  content: "\eb7c";
}

.lni-sad::before {
  content: "\eb7d";
}

.lni-save::before {
  content: "\eb7e";
}

.lni-school-bench-alt::before {
  content: "\eb7f";
}

.lni-school-bench::before {
  content: "\eb80";
}

.lni-scooter::before {
  content: "\eb81";
}

.lni-scroll-down::before {
  content: "\eb82";
}

.lni-search-alt::before {
  content: "\eb83";
}

.lni-search::before {
  content: "\eb84";
}

.lni-select::before {
  content: "\eb85";
}

.lni-seo-consulting::before {
  content: "\eb86";
}

.lni-seo::before {
  content: "\eb87";
}

.lni-service::before {
  content: "\eb88";
}

.lni-share-alt-1::before {
  content: "\eb89";
}

.lni-share-alt::before {
  content: "\eb8a";
}

.lni-share::before {
  content: "\eb8b";
}

.lni-shield::before {
  content: "\eb8c";
}

.lni-shift-left::before {
  content: "\eb8d";
}

.lni-shift-right::before {
  content: "\eb8e";
}

.lni-ship::before {
  content: "\eb8f";
}

.lni-shopify::before {
  content: "\eb90";
}

.lni-shopping-basket::before {
  content: "\eb91";
}

.lni-shortcode::before {
  content: "\eb92";
}

.lni-shovel::before {
  content: "\eb93";
}

.lni-shuffle::before {
  content: "\eb94";
}

.lni-signal::before {
  content: "\eb95";
}

.lni-sketch::before {
  content: "\eb96";
}

.lni-skipping-rope::before {
  content: "\eb97";
}

.lni-skype::before {
  content: "\eb98";
}

.lni-slack-line::before {
  content: "\eb99";
}

.lni-slack::before {
  content: "\eb9a";
}

.lni-slice::before {
  content: "\eb9b";
}

.lni-slideshare::before {
  content: "\eb9c";
}

.lni-slim::before {
  content: "\eb9d";
}

.lni-smile::before {
  content: "\eb9e";
}

.lni-snapchat::before {
  content: "\eb9f";
}

.lni-sort-alpha-asc::before {
  content: "\eba0";
}

.lni-sort-amount-asc::before {
  content: "\eba1";
}

.lni-sort-amount-dsc::before {
  content: "\eba2";
}

.lni-soundcloud-original::before {
  content: "\eba3";
}

.lni-soundcloud::before {
  content: "\eba4";
}

.lni-speechless::before {
  content: "\eba5";
}

.lni-spellcheck::before {
  content: "\eba6";
}

.lni-spinner-arrow::before {
  content: "\eba7";
}

.lni-spinner-solid::before {
  content: "\eba8";
}

.lni-spinner::before {
  content: "\eba9";
}

.lni-spotify-original::before {
  content: "\ebaa";
}

.lni-spotify::before {
  content: "\ebab";
}

.lni-spray::before {
  content: "\ebac";
}

.lni-sprout::before {
  content: "\ebad";
}

.lni-squarespace::before {
  content: "\ebae";
}

.lni-stackoverflow::before {
  content: "\ebaf";
}

.lni-stamp::before {
  content: "\ebb0";
}

.lni-star-empty::before {
  content: "\ebb1";
}

.lni-star-filled::before {
  content: "\ebb2";
}

.lni-star-half::before {
  content: "\ebb3";
}

.lni-star::before {
  content: "\ebb4";
}

.lni-stats-down::before {
  content: "\ebb5";
}

.lni-stats-up::before {
  content: "\ebb6";
}

.lni-steam::before {
  content: "\ebb7";
}

.lni-sthethoscope::before {
  content: "\ebb8";
}

.lni-stop::before {
  content: "\ebb9";
}

.lni-strikethrough::before {
  content: "\ebba";
}

.lni-stripe::before {
  content: "\ebbb";
}

.lni-stumbleupon::before {
  content: "\ebbc";
}

.lni-sun::before {
  content: "\ebbd";
}

.lni-support::before {
  content: "\ebbe";
}

.lni-surf-board::before {
  content: "\ebbf";
}

.lni-suspect::before {
  content: "\ebc0";
}

.lni-swift::before {
  content: "\ebc1";
}

.lni-syringe::before {
  content: "\ebc2";
}

.lni-t-shirt::before {
  content: "\ebc3";
}

.lni-tab::before {
  content: "\ebc4";
}

.lni-tag::before {
  content: "\ebc5";
}

.lni-target-customer::before {
  content: "\ebc6";
}

.lni-target-revenue::before {
  content: "\ebc7";
}

.lni-target::before {
  content: "\ebc8";
}

.lni-taxi::before {
  content: "\ebc9";
}

.lni-teabag::before {
  content: "\ebca";
}

.lni-telegram-plane::before {
  content: "\ebcb";
}

.lni-telegram::before {
  content: "\ebcc";
}

.lni-text-align-center::before {
  content: "\ebcd";
}

.lni-text-align-justify::before {
  content: "\ebce";
}

.lni-text-align-left::before {
  content: "\ebcf";
}

.lni-text-align-right::before {
  content: "\ebd0";
}

.lni-text-format-remove::before {
  content: "\ebd1";
}

.lni-text-format::before {
  content: "\ebd2";
}

.lni-thought::before {
  content: "\ebd3";
}

.lni-thumbs-down::before {
  content: "\ebd4";
}

.lni-thumbs-up::before {
  content: "\ebd5";
}

.lni-thunder-alt::before {
  content: "\ebd6";
}

.lni-thunder::before {
  content: "\ebd7";
}

.lni-ticket-alt::before {
  content: "\ebd8";
}

.lni-ticket::before {
  content: "\ebd9";
}

.lni-tiktok::before {
  content: "\ebda";
}

.lni-timer::before {
  content: "\ebdb";
}

.lni-tounge::before {
  content: "\ebdc";
}

.lni-train-alt::before {
  content: "\ebdd";
}

.lni-train::before {
  content: "\ebde";
}

.lni-trash-can::before {
  content: "\ebdf";
}

.lni-travel::before {
  content: "\ebe0";
}

.lni-tree::before {
  content: "\ebe1";
}

.lni-trees::before {
  content: "\ebe2";
}

.lni-trello::before {
  content: "\ebe3";
}

.lni-trowel::before {
  content: "\ebe4";
}

.lni-tumbler::before {
  content: "\ebe5";
}

.lni-twitch::before {
  content: "\ebe6";
}

.lni-twitter-filled::before {
  content: "\ebe7";
}

.lni-twitter-original::before {
  content: "\ebe8";
}

.lni-twitter::before {
  content: "\ebe9";
}

.lni-ubuntu::before {
  content: "\ebea";
}

.lni-underline::before {
  content: "\ebeb";
}

.lni-unlink::before {
  content: "\ebec";
}

.lni-unlock::before {
  content: "\ebed";
}

.lni-unsplash::before {
  content: "\ebee";
}

.lni-upload::before {
  content: "\ebef";
}

.lni-user::before {
  content: "\ebf0";
}

.lni-users::before {
  content: "\ebf1";
}

.lni-ux::before {
  content: "\ebf2";
}

.lni-vector::before {
  content: "\ebf3";
}

.lni-video::before {
  content: "\ebf4";
}

.lni-vimeo::before {
  content: "\ebf5";
}

.lni-visa::before {
  content: "\ebf6";
}

.lni-vk::before {
  content: "\ebf7";
}

.lni-volume-high::before {
  content: "\ebf8";
}

.lni-volume-low::before {
  content: "\ebf9";
}

.lni-volume-medium::before {
  content: "\ebfa";
}

.lni-volume-mute::before {
  content: "\ebfb";
}

.lni-volume::before {
  content: "\ebfc";
}

.lni-wallet::before {
  content: "\ebfd";
}

.lni-warning::before {
  content: "\ebfe";
}

.lni-website-alt::before {
  content: "\ebff";
}

.lni-website::before {
  content: "\ec00";
}

.lni-wechat::before {
  content: "\ec01";
}

.lni-weight::before {
  content: "\ec02";
}

.lni-whatsapp::before {
  content: "\ec03";
}

.lni-wheelbarrow::before {
  content: "\ec04";
}

.lni-wheelchair::before {
  content: "\ec05";
}

.lni-windows::before {
  content: "\ec06";
}

.lni-wordpress-filled::before {
  content: "\ec07";
}

.lni-wordpress::before {
  content: "\ec08";
}

.lni-world-alt::before {
  content: "\ec09";
}

.lni-world::before {
  content: "\ec0a";
}

.lni-write::before {
  content: "\ec0b";
}

.lni-xbox::before {
  content: "\ec0c";
}

.lni-yahoo::before {
  content: "\ec0d";
}

.lni-ycombinator::before {
  content: "\ec0e";
}

.lni-yen::before {
  content: "\ec0f";
}

.lni-youtube::before {
  content: "\ec10";
}

.lni-zip::before {
  content: "\ec11";
}

.lni-zoom-in::before {
  content: "\ec12";
}

.lni-zoom-out::before {
  content: "\ec13";
}


</style>


<style>
    .tns-outer{padding:0!important}.tns-outer [hidden]{display:none!important}.tns-outer [aria-controls],.tns-outer [data-action]{cursor:pointer}.tns-slider{-webkit-transition:all 0s;-moz-transition:all 0s;transition:all 0s}.tns-slider>.tns-item{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.tns-horizontal.tns-subpixel{white-space:nowrap}.tns-horizontal.tns-subpixel>.tns-item{display:inline-block;vertical-align:top;white-space:normal}.tns-horizontal.tns-no-subpixel:after{content:'';display:table;clear:both}.tns-horizontal.tns-no-subpixel>.tns-item{float:left}.tns-horizontal.tns-carousel.tns-no-subpixel>.tns-item{margin-right:-100%}.tns-no-calc{position:relative;left:0}.tns-gallery{position:relative;left:0;min-height:1px}.tns-gallery>.tns-item{position:absolute;left:-100%;-webkit-transition:transform 0s,opacity 0s;-moz-transition:transform 0s,opacity 0s;transition:transform 0s,opacity 0s}.tns-gallery>.tns-slide-active{position:relative;left:auto!important}.tns-gallery>.tns-moving{-webkit-transition:all .25s;-moz-transition:all .25s;transition:all .25s}.tns-autowidth{display:inline-block}.tns-lazy-img{-webkit-transition:opacity .6s;-moz-transition:opacity .6s;transition:opacity .6s;opacity:.6}.tns-lazy-img.tns-complete{opacity:1}.tns-ah{-webkit-transition:height 0s;-moz-transition:height 0s;transition:height 0s}.tns-ovh{overflow:hidden}.tns-visually-hidden{position:absolute;left:-10000em}.tns-transparent{opacity:0;visibility:hidden}.tns-fadeIn{opacity:1;filter:alpha(opacity=100);z-index:0}.tns-normal,.tns-fadeOut{opacity:0;filter:alpha(opacity=0);z-index:-1}.tns-vpfix{white-space:nowrap}.tns-vpfix>div,.tns-vpfix>li{display:inline-block}.tns-t-subp2{margin:0 auto;width:310px;position:relative;height:10px;overflow:hidden}.tns-t-ct{width:2333.3333333%;width:-webkit-calc(100% * 70/3);width:-moz-calc(100% * 70/3);width:calc(100% * 70/3);position:absolute;right:0}.tns-t-ct:after{content:'';display:table;clear:both}.tns-t-ct>div{width:1.4285714%;width:-webkit-calc(100%/70);width:-moz-calc(100%/70);width:calc(100%/70);height:10px;float:left}
</style>

<style>
    @charset "UTF-8";

.animated {
  -webkit-animation-duration: 1s;
  animation-duration: 1s;
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;
}

.animated.hinge {
  -webkit-animation-duration: 2s;
  animation-duration: 2s;
}

@-webkit-keyframes bounce {
  0%, 20%, 50%, 80%, 100% {
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }

  40% {
    -webkit-transform: translateY(-30px);
    transform: translateY(-30px);
  }

  60% {
    -webkit-transform: translateY(-15px);
    transform: translateY(-15px);
  }
}

@keyframes bounce {
  0%, 20%, 50%, 80%, 100% {
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }

  40% {
    -webkit-transform: translateY(-30px);
    -ms-transform: translateY(-30px);
    transform: translateY(-30px);
  }

  60% {
    -webkit-transform: translateY(-15px);
    -ms-transform: translateY(-15px);
    transform: translateY(-15px);
  }
}

.bounce {
  -webkit-animation-name: bounce;
  animation-name: bounce;
}

@-webkit-keyframes flash {
  0%, 50%, 100% {
    opacity: 1;
  }

  25%, 75% {
    opacity: 0;
  }
}

@keyframes flash {
  0%, 50%, 100% {
    opacity: 1;
  }

  25%, 75% {
    opacity: 0;
  }
}

.flash {
  -webkit-animation-name: flash;
  animation-name: flash;
}

/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */

@-webkit-keyframes pulse {
  0% {
    -webkit-transform: scale(1);
    transform: scale(1);
  }

  50% {
    -webkit-transform: scale(1.1);
    transform: scale(1.1);
  }

  100% {
    -webkit-transform: scale(1);
    transform: scale(1);
  }
}

@keyframes pulse {
  0% {
    -webkit-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
  }

  50% {
    -webkit-transform: scale(1.1);
    -ms-transform: scale(1.1);
    transform: scale(1.1);
  }

  100% {
    -webkit-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
  }
}

.pulse {
  -webkit-animation-name: pulse;
  animation-name: pulse;
}

@-webkit-keyframes shake {
  0%, 100% {
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }

  10%, 30%, 50%, 70%, 90% {
    -webkit-transform: translateX(-10px);
    transform: translateX(-10px);
  }

  20%, 40%, 60%, 80% {
    -webkit-transform: translateX(10px);
    transform: translateX(10px);
  }
}

@keyframes shake {
  0%, 100% {
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }

  10%, 30%, 50%, 70%, 90% {
    -webkit-transform: translateX(-10px);
    -ms-transform: translateX(-10px);
    transform: translateX(-10px);
  }

  20%, 40%, 60%, 80% {
    -webkit-transform: translateX(10px);
    -ms-transform: translateX(10px);
    transform: translateX(10px);
  }
}

.shake {
  -webkit-animation-name: shake;
  animation-name: shake;
}

@-webkit-keyframes swing {
  20% {
    -webkit-transform: rotate(15deg);
    transform: rotate(15deg);
  }

  40% {
    -webkit-transform: rotate(-10deg);
    transform: rotate(-10deg);
  }

  60% {
    -webkit-transform: rotate(5deg);
    transform: rotate(5deg);
  }

  80% {
    -webkit-transform: rotate(-5deg);
    transform: rotate(-5deg);
  }

  100% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
}

@keyframes swing {
  20% {
    -webkit-transform: rotate(15deg);
    -ms-transform: rotate(15deg);
    transform: rotate(15deg);
  }

  40% {
    -webkit-transform: rotate(-10deg);
    -ms-transform: rotate(-10deg);
    transform: rotate(-10deg);
  }

  60% {
    -webkit-transform: rotate(5deg);
    -ms-transform: rotate(5deg);
    transform: rotate(5deg);
  }

  80% {
    -webkit-transform: rotate(-5deg);
    -ms-transform: rotate(-5deg);
    transform: rotate(-5deg);
  }

  100% {
    -webkit-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    transform: rotate(0deg);
  }
}

.swing {
  -webkit-transform-origin: top center;
  -ms-transform-origin: top center;
  transform-origin: top center;
  -webkit-animation-name: swing;
  animation-name: swing;
}

@-webkit-keyframes tada {
  0% {
    -webkit-transform: scale(1);
    transform: scale(1);
  }

  10%, 20% {
    -webkit-transform: scale(0.9) rotate(-3deg);
    transform: scale(0.9) rotate(-3deg);
  }

  30%, 50%, 70%, 90% {
    -webkit-transform: scale(1.1) rotate(3deg);
    transform: scale(1.1) rotate(3deg);
  }

  40%, 60%, 80% {
    -webkit-transform: scale(1.1) rotate(-3deg);
    transform: scale(1.1) rotate(-3deg);
  }

  100% {
    -webkit-transform: scale(1) rotate(0);
    transform: scale(1) rotate(0);
  }
}

@keyframes tada {
  0% {
    -webkit-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
  }

  10%, 20% {
    -webkit-transform: scale(0.9) rotate(-3deg);
    -ms-transform: scale(0.9) rotate(-3deg);
    transform: scale(0.9) rotate(-3deg);
  }

  30%, 50%, 70%, 90% {
    -webkit-transform: scale(1.1) rotate(3deg);
    -ms-transform: scale(1.1) rotate(3deg);
    transform: scale(1.1) rotate(3deg);
  }

  40%, 60%, 80% {
    -webkit-transform: scale(1.1) rotate(-3deg);
    -ms-transform: scale(1.1) rotate(-3deg);
    transform: scale(1.1) rotate(-3deg);
  }

  100% {
    -webkit-transform: scale(1) rotate(0);
    -ms-transform: scale(1) rotate(0);
    transform: scale(1) rotate(0);
  }
}

.tada {
  -webkit-animation-name: tada;
  animation-name: tada;
}

/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */

@-webkit-keyframes wobble {
  0% {
    -webkit-transform: translateX(0%);
    transform: translateX(0%);
  }

  15% {
    -webkit-transform: translateX(-25%) rotate(-5deg);
    transform: translateX(-25%) rotate(-5deg);
  }

  30% {
    -webkit-transform: translateX(20%) rotate(3deg);
    transform: translateX(20%) rotate(3deg);
  }

  45% {
    -webkit-transform: translateX(-15%) rotate(-3deg);
    transform: translateX(-15%) rotate(-3deg);
  }

  60% {
    -webkit-transform: translateX(10%) rotate(2deg);
    transform: translateX(10%) rotate(2deg);
  }

  75% {
    -webkit-transform: translateX(-5%) rotate(-1deg);
    transform: translateX(-5%) rotate(-1deg);
  }

  100% {
    -webkit-transform: translateX(0%);
    transform: translateX(0%);
  }
}

@keyframes wobble {
  0% {
    -webkit-transform: translateX(0%);
    -ms-transform: translateX(0%);
    transform: translateX(0%);
  }

  15% {
    -webkit-transform: translateX(-25%) rotate(-5deg);
    -ms-transform: translateX(-25%) rotate(-5deg);
    transform: translateX(-25%) rotate(-5deg);
  }

  30% {
    -webkit-transform: translateX(20%) rotate(3deg);
    -ms-transform: translateX(20%) rotate(3deg);
    transform: translateX(20%) rotate(3deg);
  }

  45% {
    -webkit-transform: translateX(-15%) rotate(-3deg);
    -ms-transform: translateX(-15%) rotate(-3deg);
    transform: translateX(-15%) rotate(-3deg);
  }

  60% {
    -webkit-transform: translateX(10%) rotate(2deg);
    -ms-transform: translateX(10%) rotate(2deg);
    transform: translateX(10%) rotate(2deg);
  }

  75% {
    -webkit-transform: translateX(-5%) rotate(-1deg);
    -ms-transform: translateX(-5%) rotate(-1deg);
    transform: translateX(-5%) rotate(-1deg);
  }

  100% {
    -webkit-transform: translateX(0%);
    -ms-transform: translateX(0%);
    transform: translateX(0%);
  }
}

.wobble {
  -webkit-animation-name: wobble;
  animation-name: wobble;
}

@-webkit-keyframes bounceIn {
  0% {
    opacity: 0;
    -webkit-transform: scale(.3);
    transform: scale(.3);
  }

  50% {
    opacity: 1;
    -webkit-transform: scale(1.05);
    transform: scale(1.05);
  }

  70% {
    -webkit-transform: scale(.9);
    transform: scale(.9);
  }

  100% {
    -webkit-transform: scale(1);
    transform: scale(1);
  }
}

@keyframes bounceIn {
  0% {
    opacity: 0;
    -webkit-transform: scale(.3);
    -ms-transform: scale(.3);
    transform: scale(.3);
  }

  50% {
    opacity: 1;
    -webkit-transform: scale(1.05);
    -ms-transform: scale(1.05);
    transform: scale(1.05);
  }

  70% {
    -webkit-transform: scale(.9);
    -ms-transform: scale(.9);
    transform: scale(.9);
  }

  100% {
    -webkit-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
  }
}

.bounceIn {
  -webkit-animation-name: bounceIn;
  animation-name: bounceIn;
}

@-webkit-keyframes bounceInDown {
  0% {
    opacity: 0;
    -webkit-transform: translateY(-2000px);
    transform: translateY(-2000px);
  }

  60% {
    opacity: 1;
    -webkit-transform: translateY(30px);
    transform: translateY(30px);
  }

  80% {
    -webkit-transform: translateY(-10px);
    transform: translateY(-10px);
  }

  100% {
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }
}

@keyframes bounceInDown {
  0% {
    opacity: 0;
    -webkit-transform: translateY(-2000px);
    -ms-transform: translateY(-2000px);
    transform: translateY(-2000px);
  }

  60% {
    opacity: 1;
    -webkit-transform: translateY(30px);
    -ms-transform: translateY(30px);
    transform: translateY(30px);
  }

  80% {
    -webkit-transform: translateY(-10px);
    -ms-transform: translateY(-10px);
    transform: translateY(-10px);
  }

  100% {
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }
}

.bounceInDown {
  -webkit-animation-name: bounceInDown;
  animation-name: bounceInDown;
}

@-webkit-keyframes bounceInLeft {
  0% {
    opacity: 0;
    -webkit-transform: translateX(-2000px);
    transform: translateX(-2000px);
  }

  60% {
    opacity: 1;
    -webkit-transform: translateX(30px);
    transform: translateX(30px);
  }

  80% {
    -webkit-transform: translateX(-10px);
    transform: translateX(-10px);
  }

  100% {
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }
}

@keyframes bounceInLeft {
  0% {
    opacity: 0;
    -webkit-transform: translateX(-2000px);
    -ms-transform: translateX(-2000px);
    transform: translateX(-2000px);
  }

  60% {
    opacity: 1;
    -webkit-transform: translateX(30px);
    -ms-transform: translateX(30px);
    transform: translateX(30px);
  }

  80% {
    -webkit-transform: translateX(-10px);
    -ms-transform: translateX(-10px);
    transform: translateX(-10px);
  }

  100% {
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }
}

.bounceInLeft {
  -webkit-animation-name: bounceInLeft;
  animation-name: bounceInLeft;
}

@-webkit-keyframes bounceInRight {
  0% {
    opacity: 0;
    -webkit-transform: translateX(2000px);
    transform: translateX(2000px);
  }

  60% {
    opacity: 1;
    -webkit-transform: translateX(-30px);
    transform: translateX(-30px);
  }

  80% {
    -webkit-transform: translateX(10px);
    transform: translateX(10px);
  }

  100% {
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }
}

@keyframes bounceInRight {
  0% {
    opacity: 0;
    -webkit-transform: translateX(2000px);
    -ms-transform: translateX(2000px);
    transform: translateX(2000px);
  }

  60% {
    opacity: 1;
    -webkit-transform: translateX(-30px);
    -ms-transform: translateX(-30px);
    transform: translateX(-30px);
  }

  80% {
    -webkit-transform: translateX(10px);
    -ms-transform: translateX(10px);
    transform: translateX(10px);
  }

  100% {
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }
}

.bounceInRight {
  -webkit-animation-name: bounceInRight;
  animation-name: bounceInRight;
}

@-webkit-keyframes bounceInUp {
  0% {
    opacity: 0;
    -webkit-transform: translateY(2000px);
    transform: translateY(2000px);
  }

  60% {
    opacity: 1;
    -webkit-transform: translateY(-30px);
    transform: translateY(-30px);
  }

  80% {
    -webkit-transform: translateY(10px);
    transform: translateY(10px);
  }

  100% {
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }
}

@keyframes bounceInUp {
  0% {
    opacity: 0;
    -webkit-transform: translateY(2000px);
    -ms-transform: translateY(2000px);
    transform: translateY(2000px);
  }

  60% {
    opacity: 1;
    -webkit-transform: translateY(-30px);
    -ms-transform: translateY(-30px);
    transform: translateY(-30px);
  }

  80% {
    -webkit-transform: translateY(10px);
    -ms-transform: translateY(10px);
    transform: translateY(10px);
  }

  100% {
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }
}

.bounceInUp {
  -webkit-animation-name: bounceInUp;
  animation-name: bounceInUp;
}

@-webkit-keyframes bounceOut {
  0% {
    -webkit-transform: scale(1);
    transform: scale(1);
  }

  25% {
    -webkit-transform: scale(.95);
    transform: scale(.95);
  }

  50% {
    opacity: 1;
    -webkit-transform: scale(1.1);
    transform: scale(1.1);
  }

  100% {
    opacity: 0;
    -webkit-transform: scale(.3);
    transform: scale(.3);
  }
}

@keyframes bounceOut {
  0% {
    -webkit-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
  }

  25% {
    -webkit-transform: scale(.95);
    -ms-transform: scale(.95);
    transform: scale(.95);
  }

  50% {
    opacity: 1;
    -webkit-transform: scale(1.1);
    -ms-transform: scale(1.1);
    transform: scale(1.1);
  }

  100% {
    opacity: 0;
    -webkit-transform: scale(.3);
    -ms-transform: scale(.3);
    transform: scale(.3);
  }
}

.bounceOut {
  -webkit-animation-name: bounceOut;
  animation-name: bounceOut;
}

@-webkit-keyframes bounceOutDown {
  0% {
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }

  20% {
    opacity: 1;
    -webkit-transform: translateY(-20px);
    transform: translateY(-20px);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(2000px);
    transform: translateY(2000px);
  }
}

@keyframes bounceOutDown {
  0% {
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }

  20% {
    opacity: 1;
    -webkit-transform: translateY(-20px);
    -ms-transform: translateY(-20px);
    transform: translateY(-20px);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(2000px);
    -ms-transform: translateY(2000px);
    transform: translateY(2000px);
  }
}

.bounceOutDown {
  -webkit-animation-name: bounceOutDown;
  animation-name: bounceOutDown;
}

@-webkit-keyframes bounceOutLeft {
  0% {
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }

  20% {
    opacity: 1;
    -webkit-transform: translateX(20px);
    transform: translateX(20px);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(-2000px);
    transform: translateX(-2000px);
  }
}

@keyframes bounceOutLeft {
  0% {
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }

  20% {
    opacity: 1;
    -webkit-transform: translateX(20px);
    -ms-transform: translateX(20px);
    transform: translateX(20px);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(-2000px);
    -ms-transform: translateX(-2000px);
    transform: translateX(-2000px);
  }
}

.bounceOutLeft {
  -webkit-animation-name: bounceOutLeft;
  animation-name: bounceOutLeft;
}

@-webkit-keyframes bounceOutRight {
  0% {
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }

  20% {
    opacity: 1;
    -webkit-transform: translateX(-20px);
    transform: translateX(-20px);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(2000px);
    transform: translateX(2000px);
  }
}

@keyframes bounceOutRight {
  0% {
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }

  20% {
    opacity: 1;
    -webkit-transform: translateX(-20px);
    -ms-transform: translateX(-20px);
    transform: translateX(-20px);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(2000px);
    -ms-transform: translateX(2000px);
    transform: translateX(2000px);
  }
}

.bounceOutRight {
  -webkit-animation-name: bounceOutRight;
  animation-name: bounceOutRight;
}

@-webkit-keyframes bounceOutUp {
  0% {
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }

  20% {
    opacity: 1;
    -webkit-transform: translateY(20px);
    transform: translateY(20px);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(-2000px);
    transform: translateY(-2000px);
  }
}

@keyframes bounceOutUp {
  0% {
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }

  20% {
    opacity: 1;
    -webkit-transform: translateY(20px);
    -ms-transform: translateY(20px);
    transform: translateY(20px);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(-2000px);
    -ms-transform: translateY(-2000px);
    transform: translateY(-2000px);
  }
}

.bounceOutUp {
  -webkit-animation-name: bounceOutUp;
  animation-name: bounceOutUp;
}

@-webkit-keyframes fadeIn {
  0% {
    opacity: 0;
  }

  100% {
    opacity: 1;
  }
}

@keyframes fadeIn {
  0% {
    opacity: 0;
  }

  100% {
    opacity: 1;
  }
}

.fadeIn {
  -webkit-animation-name: fadeIn;
  animation-name: fadeIn;
}

@-webkit-keyframes fadeInDown {
  0% {
    opacity: 0;
    -webkit-transform: translateY(-20px);
    transform: translateY(-20px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }
}

@keyframes fadeInDown {
  0% {
    opacity: 0;
    -webkit-transform: translateY(-20px);
    -ms-transform: translateY(-20px);
    transform: translateY(-20px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }
}

.fadeInDown {
  -webkit-animation-name: fadeInDown;
  animation-name: fadeInDown;
}

@-webkit-keyframes fadeInDownBig {
  0% {
    opacity: 0;
    -webkit-transform: translateY(-2000px);
    transform: translateY(-2000px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }
}

@keyframes fadeInDownBig {
  0% {
    opacity: 0;
    -webkit-transform: translateY(-2000px);
    -ms-transform: translateY(-2000px);
    transform: translateY(-2000px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }
}

.fadeInDownBig {
  -webkit-animation-name: fadeInDownBig;
  animation-name: fadeInDownBig;
}

@-webkit-keyframes fadeInLeft {
  0% {
    opacity: 0;
    -webkit-transform: translateX(-20px);
    transform: translateX(-20px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }
}

@keyframes fadeInLeft {
  0% {
    opacity: 0;
    -webkit-transform: translateX(-20px);
    -ms-transform: translateX(-20px);
    transform: translateX(-20px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }
}

.fadeInLeft {
  -webkit-animation-name: fadeInLeft;
  animation-name: fadeInLeft;
}

@-webkit-keyframes fadeInLeftBig {
  0% {
    opacity: 0;
    -webkit-transform: translateX(-2000px);
    transform: translateX(-2000px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }
}

@keyframes fadeInLeftBig {
  0% {
    opacity: 0;
    -webkit-transform: translateX(-2000px);
    -ms-transform: translateX(-2000px);
    transform: translateX(-2000px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }
}

.fadeInLeftBig {
  -webkit-animation-name: fadeInLeftBig;
  animation-name: fadeInLeftBig;
}

@-webkit-keyframes fadeInRight {
  0% {
    opacity: 0;
    -webkit-transform: translateX(20px);
    transform: translateX(20px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }
}

@keyframes fadeInRight {
  0% {
    opacity: 0;
    -webkit-transform: translateX(20px);
    -ms-transform: translateX(20px);
    transform: translateX(20px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }
}

.fadeInRight {
  -webkit-animation-name: fadeInRight;
  animation-name: fadeInRight;
}

@-webkit-keyframes fadeInRightBig {
  0% {
    opacity: 0;
    -webkit-transform: translateX(2000px);
    transform: translateX(2000px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }
}

@keyframes fadeInRightBig {
  0% {
    opacity: 0;
    -webkit-transform: translateX(2000px);
    -ms-transform: translateX(2000px);
    transform: translateX(2000px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }
}

.fadeInRightBig {
  -webkit-animation-name: fadeInRightBig;
  animation-name: fadeInRightBig;
}

@-webkit-keyframes fadeInUp {
  0% {
    opacity: 0;
    -webkit-transform: translateY(20px);
    transform: translateY(20px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }
}

@keyframes fadeInUp {
  0% {
    opacity: 0;
    -webkit-transform: translateY(20px);
    -ms-transform: translateY(20px);
    transform: translateY(20px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }
}

.fadeInUp {
  -webkit-animation-name: fadeInUp;
  animation-name: fadeInUp;
}

@-webkit-keyframes fadeInUpBig {
  0% {
    opacity: 0;
    -webkit-transform: translateY(2000px);
    transform: translateY(2000px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }
}

@keyframes fadeInUpBig {
  0% {
    opacity: 0;
    -webkit-transform: translateY(2000px);
    -ms-transform: translateY(2000px);
    transform: translateY(2000px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }
}

.fadeInUpBig {
  -webkit-animation-name: fadeInUpBig;
  animation-name: fadeInUpBig;
}

@-webkit-keyframes fadeOut {
  0% {
    opacity: 1;
  }

  100% {
    opacity: 0;
  }
}

@keyframes fadeOut {
  0% {
    opacity: 1;
  }

  100% {
    opacity: 0;
  }
}

.fadeOut {
  -webkit-animation-name: fadeOut;
  animation-name: fadeOut;
}

@-webkit-keyframes fadeOutDown {
  0% {
    opacity: 1;
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(20px);
    transform: translateY(20px);
  }
}

@keyframes fadeOutDown {
  0% {
    opacity: 1;
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(20px);
    -ms-transform: translateY(20px);
    transform: translateY(20px);
  }
}

.fadeOutDown {
  -webkit-animation-name: fadeOutDown;
  animation-name: fadeOutDown;
}

@-webkit-keyframes fadeOutDownBig {
  0% {
    opacity: 1;
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(2000px);
    transform: translateY(2000px);
  }
}

@keyframes fadeOutDownBig {
  0% {
    opacity: 1;
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(2000px);
    -ms-transform: translateY(2000px);
    transform: translateY(2000px);
  }
}

.fadeOutDownBig {
  -webkit-animation-name: fadeOutDownBig;
  animation-name: fadeOutDownBig;
}

@-webkit-keyframes fadeOutLeft {
  0% {
    opacity: 1;
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(-20px);
    transform: translateX(-20px);
  }
}

@keyframes fadeOutLeft {
  0% {
    opacity: 1;
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(-20px);
    -ms-transform: translateX(-20px);
    transform: translateX(-20px);
  }
}

.fadeOutLeft {
  -webkit-animation-name: fadeOutLeft;
  animation-name: fadeOutLeft;
}

@-webkit-keyframes fadeOutLeftBig {
  0% {
    opacity: 1;
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(-2000px);
    transform: translateX(-2000px);
  }
}

@keyframes fadeOutLeftBig {
  0% {
    opacity: 1;
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(-2000px);
    -ms-transform: translateX(-2000px);
    transform: translateX(-2000px);
  }
}

.fadeOutLeftBig {
  -webkit-animation-name: fadeOutLeftBig;
  animation-name: fadeOutLeftBig;
}

@-webkit-keyframes fadeOutRight {
  0% {
    opacity: 1;
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(20px);
    transform: translateX(20px);
  }
}

@keyframes fadeOutRight {
  0% {
    opacity: 1;
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(20px);
    -ms-transform: translateX(20px);
    transform: translateX(20px);
  }
}

.fadeOutRight {
  -webkit-animation-name: fadeOutRight;
  animation-name: fadeOutRight;
}

@-webkit-keyframes fadeOutRightBig {
  0% {
    opacity: 1;
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(2000px);
    transform: translateX(2000px);
  }
}

@keyframes fadeOutRightBig {
  0% {
    opacity: 1;
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(2000px);
    -ms-transform: translateX(2000px);
    transform: translateX(2000px);
  }
}

.fadeOutRightBig {
  -webkit-animation-name: fadeOutRightBig;
  animation-name: fadeOutRightBig;
}

@-webkit-keyframes fadeOutUp {
  0% {
    opacity: 1;
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(-20px);
    transform: translateY(-20px);
  }
}

@keyframes fadeOutUp {
  0% {
    opacity: 1;
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(-20px);
    -ms-transform: translateY(-20px);
    transform: translateY(-20px);
  }
}

.fadeOutUp {
  -webkit-animation-name: fadeOutUp;
  animation-name: fadeOutUp;
}

@-webkit-keyframes fadeOutUpBig {
  0% {
    opacity: 1;
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(-2000px);
    transform: translateY(-2000px);
  }
}

@keyframes fadeOutUpBig {
  0% {
    opacity: 1;
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(-2000px);
    -ms-transform: translateY(-2000px);
    transform: translateY(-2000px);
  }
}

.fadeOutUpBig {
  -webkit-animation-name: fadeOutUpBig;
  animation-name: fadeOutUpBig;
}

@-webkit-keyframes flip {
  0% {
    -webkit-transform: perspective(400px) translateZ(0) rotateY(0) scale(1);
    transform: perspective(400px) translateZ(0) rotateY(0) scale(1);
    -webkit-animation-timing-function: ease-out;
    animation-timing-function: ease-out;
  }

  40% {
    -webkit-transform: perspective(400px) translateZ(150px) rotateY(170deg) scale(1);
    transform: perspective(400px) translateZ(150px) rotateY(170deg) scale(1);
    -webkit-animation-timing-function: ease-out;
    animation-timing-function: ease-out;
  }

  50% {
    -webkit-transform: perspective(400px) translateZ(150px) rotateY(190deg) scale(1);
    transform: perspective(400px) translateZ(150px) rotateY(190deg) scale(1);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }

  80% {
    -webkit-transform: perspective(400px) translateZ(0) rotateY(360deg) scale(.95);
    transform: perspective(400px) translateZ(0) rotateY(360deg) scale(.95);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }

  100% {
    -webkit-transform: perspective(400px) translateZ(0) rotateY(360deg) scale(1);
    transform: perspective(400px) translateZ(0) rotateY(360deg) scale(1);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }
}

@keyframes flip {
  0% {
    -webkit-transform: perspective(400px) translateZ(0) rotateY(0) scale(1);
    -ms-transform: perspective(400px) translateZ(0) rotateY(0) scale(1);
    transform: perspective(400px) translateZ(0) rotateY(0) scale(1);
    -webkit-animation-timing-function: ease-out;
    animation-timing-function: ease-out;
  }

  40% {
    -webkit-transform: perspective(400px) translateZ(150px) rotateY(170deg) scale(1);
    -ms-transform: perspective(400px) translateZ(150px) rotateY(170deg) scale(1);
    transform: perspective(400px) translateZ(150px) rotateY(170deg) scale(1);
    -webkit-animation-timing-function: ease-out;
    animation-timing-function: ease-out;
  }

  50% {
    -webkit-transform: perspective(400px) translateZ(150px) rotateY(190deg) scale(1);
    -ms-transform: perspective(400px) translateZ(150px) rotateY(190deg) scale(1);
    transform: perspective(400px) translateZ(150px) rotateY(190deg) scale(1);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }

  80% {
    -webkit-transform: perspective(400px) translateZ(0) rotateY(360deg) scale(.95);
    -ms-transform: perspective(400px) translateZ(0) rotateY(360deg) scale(.95);
    transform: perspective(400px) translateZ(0) rotateY(360deg) scale(.95);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }

  100% {
    -webkit-transform: perspective(400px) translateZ(0) rotateY(360deg) scale(1);
    -ms-transform: perspective(400px) translateZ(0) rotateY(360deg) scale(1);
    transform: perspective(400px) translateZ(0) rotateY(360deg) scale(1);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }
}

.animated.flip {
  -webkit-backface-visibility: visible;
  -ms-backface-visibility: visible;
  backface-visibility: visible;
  -webkit-animation-name: flip;
  animation-name: flip;
}

@-webkit-keyframes flipInX {
  0% {
    -webkit-transform: perspective(400px) rotateX(90deg);
    transform: perspective(400px) rotateX(90deg);
    opacity: 0;
  }

  40% {
    -webkit-transform: perspective(400px) rotateX(-10deg);
    transform: perspective(400px) rotateX(-10deg);
  }

  70% {
    -webkit-transform: perspective(400px) rotateX(10deg);
    transform: perspective(400px) rotateX(10deg);
  }

  100% {
    -webkit-transform: perspective(400px) rotateX(0deg);
    transform: perspective(400px) rotateX(0deg);
    opacity: 1;
  }
}

@keyframes flipInX {
  0% {
    -webkit-transform: perspective(400px) rotateX(90deg);
    -ms-transform: perspective(400px) rotateX(90deg);
    transform: perspective(400px) rotateX(90deg);
    opacity: 0;
  }

  40% {
    -webkit-transform: perspective(400px) rotateX(-10deg);
    -ms-transform: perspective(400px) rotateX(-10deg);
    transform: perspective(400px) rotateX(-10deg);
  }

  70% {
    -webkit-transform: perspective(400px) rotateX(10deg);
    -ms-transform: perspective(400px) rotateX(10deg);
    transform: perspective(400px) rotateX(10deg);
  }

  100% {
    -webkit-transform: perspective(400px) rotateX(0deg);
    -ms-transform: perspective(400px) rotateX(0deg);
    transform: perspective(400px) rotateX(0deg);
    opacity: 1;
  }
}

.flipInX {
  -webkit-backface-visibility: visible !important;
  -ms-backface-visibility: visible !important;
  backface-visibility: visible !important;
  -webkit-animation-name: flipInX;
  animation-name: flipInX;
}

@-webkit-keyframes flipInY {
  0% {
    -webkit-transform: perspective(400px) rotateY(90deg);
    transform: perspective(400px) rotateY(90deg);
    opacity: 0;
  }

  40% {
    -webkit-transform: perspective(400px) rotateY(-10deg);
    transform: perspective(400px) rotateY(-10deg);
  }

  70% {
    -webkit-transform: perspective(400px) rotateY(10deg);
    transform: perspective(400px) rotateY(10deg);
  }

  100% {
    -webkit-transform: perspective(400px) rotateY(0deg);
    transform: perspective(400px) rotateY(0deg);
    opacity: 1;
  }
}

@keyframes flipInY {
  0% {
    -webkit-transform: perspective(400px) rotateY(90deg);
    -ms-transform: perspective(400px) rotateY(90deg);
    transform: perspective(400px) rotateY(90deg);
    opacity: 0;
  }

  40% {
    -webkit-transform: perspective(400px) rotateY(-10deg);
    -ms-transform: perspective(400px) rotateY(-10deg);
    transform: perspective(400px) rotateY(-10deg);
  }

  70% {
    -webkit-transform: perspective(400px) rotateY(10deg);
    -ms-transform: perspective(400px) rotateY(10deg);
    transform: perspective(400px) rotateY(10deg);
  }

  100% {
    -webkit-transform: perspective(400px) rotateY(0deg);
    -ms-transform: perspective(400px) rotateY(0deg);
    transform: perspective(400px) rotateY(0deg);
    opacity: 1;
  }
}

.flipInY {
  -webkit-backface-visibility: visible !important;
  -ms-backface-visibility: visible !important;
  backface-visibility: visible !important;
  -webkit-animation-name: flipInY;
  animation-name: flipInY;
}

@-webkit-keyframes flipOutX {
  0% {
    -webkit-transform: perspective(400px) rotateX(0deg);
    transform: perspective(400px) rotateX(0deg);
    opacity: 1;
  }

  100% {
    -webkit-transform: perspective(400px) rotateX(90deg);
    transform: perspective(400px) rotateX(90deg);
    opacity: 0;
  }
}

@keyframes flipOutX {
  0% {
    -webkit-transform: perspective(400px) rotateX(0deg);
    -ms-transform: perspective(400px) rotateX(0deg);
    transform: perspective(400px) rotateX(0deg);
    opacity: 1;
  }

  100% {
    -webkit-transform: perspective(400px) rotateX(90deg);
    -ms-transform: perspective(400px) rotateX(90deg);
    transform: perspective(400px) rotateX(90deg);
    opacity: 0;
  }
}

.flipOutX {
  -webkit-animation-name: flipOutX;
  animation-name: flipOutX;
  -webkit-backface-visibility: visible !important;
  -ms-backface-visibility: visible !important;
  backface-visibility: visible !important;
}

@-webkit-keyframes flipOutY {
  0% {
    -webkit-transform: perspective(400px) rotateY(0deg);
    transform: perspective(400px) rotateY(0deg);
    opacity: 1;
  }

  100% {
    -webkit-transform: perspective(400px) rotateY(90deg);
    transform: perspective(400px) rotateY(90deg);
    opacity: 0;
  }
}

@keyframes flipOutY {
  0% {
    -webkit-transform: perspective(400px) rotateY(0deg);
    -ms-transform: perspective(400px) rotateY(0deg);
    transform: perspective(400px) rotateY(0deg);
    opacity: 1;
  }

  100% {
    -webkit-transform: perspective(400px) rotateY(90deg);
    -ms-transform: perspective(400px) rotateY(90deg);
    transform: perspective(400px) rotateY(90deg);
    opacity: 0;
  }
}

.flipOutY {
  -webkit-backface-visibility: visible !important;
  -ms-backface-visibility: visible !important;
  backface-visibility: visible !important;
  -webkit-animation-name: flipOutY;
  animation-name: flipOutY;
}

@-webkit-keyframes lightSpeedIn {
  0% {
    -webkit-transform: translateX(100%) skewX(-30deg);
    transform: translateX(100%) skewX(-30deg);
    opacity: 0;
  }

  60% {
    -webkit-transform: translateX(-20%) skewX(30deg);
    transform: translateX(-20%) skewX(30deg);
    opacity: 1;
  }

  80% {
    -webkit-transform: translateX(0%) skewX(-15deg);
    transform: translateX(0%) skewX(-15deg);
    opacity: 1;
  }

  100% {
    -webkit-transform: translateX(0%) skewX(0deg);
    transform: translateX(0%) skewX(0deg);
    opacity: 1;
  }
}

@keyframes lightSpeedIn {
  0% {
    -webkit-transform: translateX(100%) skewX(-30deg);
    -ms-transform: translateX(100%) skewX(-30deg);
    transform: translateX(100%) skewX(-30deg);
    opacity: 0;
  }

  60% {
    -webkit-transform: translateX(-20%) skewX(30deg);
    -ms-transform: translateX(-20%) skewX(30deg);
    transform: translateX(-20%) skewX(30deg);
    opacity: 1;
  }

  80% {
    -webkit-transform: translateX(0%) skewX(-15deg);
    -ms-transform: translateX(0%) skewX(-15deg);
    transform: translateX(0%) skewX(-15deg);
    opacity: 1;
  }

  100% {
    -webkit-transform: translateX(0%) skewX(0deg);
    -ms-transform: translateX(0%) skewX(0deg);
    transform: translateX(0%) skewX(0deg);
    opacity: 1;
  }
}

.lightSpeedIn {
  -webkit-animation-name: lightSpeedIn;
  animation-name: lightSpeedIn;
  -webkit-animation-timing-function: ease-out;
  animation-timing-function: ease-out;
}

@-webkit-keyframes lightSpeedOut {
  0% {
    -webkit-transform: translateX(0%) skewX(0deg);
    transform: translateX(0%) skewX(0deg);
    opacity: 1;
  }

  100% {
    -webkit-transform: translateX(100%) skewX(-30deg);
    transform: translateX(100%) skewX(-30deg);
    opacity: 0;
  }
}

@keyframes lightSpeedOut {
  0% {
    -webkit-transform: translateX(0%) skewX(0deg);
    -ms-transform: translateX(0%) skewX(0deg);
    transform: translateX(0%) skewX(0deg);
    opacity: 1;
  }

  100% {
    -webkit-transform: translateX(100%) skewX(-30deg);
    -ms-transform: translateX(100%) skewX(-30deg);
    transform: translateX(100%) skewX(-30deg);
    opacity: 0;
  }
}

.lightSpeedOut {
  -webkit-animation-name: lightSpeedOut;
  animation-name: lightSpeedOut;
  -webkit-animation-timing-function: ease-in;
  animation-timing-function: ease-in;
}

@-webkit-keyframes rotateIn {
  0% {
    -webkit-transform-origin: center center;
    transform-origin: center center;
    -webkit-transform: rotate(-200deg);
    transform: rotate(-200deg);
    opacity: 0;
  }

  100% {
    -webkit-transform-origin: center center;
    transform-origin: center center;
    -webkit-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }
}

@keyframes rotateIn {
  0% {
    -webkit-transform-origin: center center;
    -ms-transform-origin: center center;
    transform-origin: center center;
    -webkit-transform: rotate(-200deg);
    -ms-transform: rotate(-200deg);
    transform: rotate(-200deg);
    opacity: 0;
  }

  100% {
    -webkit-transform-origin: center center;
    -ms-transform-origin: center center;
    transform-origin: center center;
    -webkit-transform: rotate(0);
    -ms-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }
}

.rotateIn {
  -webkit-animation-name: rotateIn;
  animation-name: rotateIn;
}

@-webkit-keyframes rotateInDownLeft {
  0% {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(-90deg);
    transform: rotate(-90deg);
    opacity: 0;
  }

  100% {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }
}

@keyframes rotateInDownLeft {
  0% {
    -webkit-transform-origin: left bottom;
    -ms-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(-90deg);
    -ms-transform: rotate(-90deg);
    transform: rotate(-90deg);
    opacity: 0;
  }

  100% {
    -webkit-transform-origin: left bottom;
    -ms-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(0);
    -ms-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }
}

.rotateInDownLeft {
  -webkit-animation-name: rotateInDownLeft;
  animation-name: rotateInDownLeft;
}

@-webkit-keyframes rotateInDownRight {
  0% {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(90deg);
    transform: rotate(90deg);
    opacity: 0;
  }

  100% {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }
}

@keyframes rotateInDownRight {
  0% {
    -webkit-transform-origin: right bottom;
    -ms-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    transform: rotate(90deg);
    opacity: 0;
  }

  100% {
    -webkit-transform-origin: right bottom;
    -ms-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(0);
    -ms-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }
}

.rotateInDownRight {
  -webkit-animation-name: rotateInDownRight;
  animation-name: rotateInDownRight;
}

@-webkit-keyframes rotateInUpLeft {
  0% {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(90deg);
    transform: rotate(90deg);
    opacity: 0;
  }

  100% {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }
}

@keyframes rotateInUpLeft {
  0% {
    -webkit-transform-origin: left bottom;
    -ms-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    transform: rotate(90deg);
    opacity: 0;
  }

  100% {
    -webkit-transform-origin: left bottom;
    -ms-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(0);
    -ms-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }
}

.rotateInUpLeft {
  -webkit-animation-name: rotateInUpLeft;
  animation-name: rotateInUpLeft;
}

@-webkit-keyframes rotateInUpRight {
  0% {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(-90deg);
    transform: rotate(-90deg);
    opacity: 0;
  }

  100% {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }
}

@keyframes rotateInUpRight {
  0% {
    -webkit-transform-origin: right bottom;
    -ms-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(-90deg);
    -ms-transform: rotate(-90deg);
    transform: rotate(-90deg);
    opacity: 0;
  }

  100% {
    -webkit-transform-origin: right bottom;
    -ms-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(0);
    -ms-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }
}

.rotateInUpRight {
  -webkit-animation-name: rotateInUpRight;
  animation-name: rotateInUpRight;
}

@-webkit-keyframes rotateOut {
  0% {
    -webkit-transform-origin: center center;
    transform-origin: center center;
    -webkit-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }

  100% {
    -webkit-transform-origin: center center;
    transform-origin: center center;
    -webkit-transform: rotate(200deg);
    transform: rotate(200deg);
    opacity: 0;
  }
}

@keyframes rotateOut {
  0% {
    -webkit-transform-origin: center center;
    -ms-transform-origin: center center;
    transform-origin: center center;
    -webkit-transform: rotate(0);
    -ms-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }

  100% {
    -webkit-transform-origin: center center;
    -ms-transform-origin: center center;
    transform-origin: center center;
    -webkit-transform: rotate(200deg);
    -ms-transform: rotate(200deg);
    transform: rotate(200deg);
    opacity: 0;
  }
}

.rotateOut {
  -webkit-animation-name: rotateOut;
  animation-name: rotateOut;
}

@-webkit-keyframes rotateOutDownLeft {
  0% {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }

  100% {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(90deg);
    transform: rotate(90deg);
    opacity: 0;
  }
}

@keyframes rotateOutDownLeft {
  0% {
    -webkit-transform-origin: left bottom;
    -ms-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(0);
    -ms-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }

  100% {
    -webkit-transform-origin: left bottom;
    -ms-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    transform: rotate(90deg);
    opacity: 0;
  }
}

.rotateOutDownLeft {
  -webkit-animation-name: rotateOutDownLeft;
  animation-name: rotateOutDownLeft;
}

@-webkit-keyframes rotateOutDownRight {
  0% {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }

  100% {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(-90deg);
    transform: rotate(-90deg);
    opacity: 0;
  }
}

@keyframes rotateOutDownRight {
  0% {
    -webkit-transform-origin: right bottom;
    -ms-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(0);
    -ms-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }

  100% {
    -webkit-transform-origin: right bottom;
    -ms-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(-90deg);
    -ms-transform: rotate(-90deg);
    transform: rotate(-90deg);
    opacity: 0;
  }
}

.rotateOutDownRight {
  -webkit-animation-name: rotateOutDownRight;
  animation-name: rotateOutDownRight;
}

@-webkit-keyframes rotateOutUpLeft {
  0% {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }

  100% {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(-90deg);
    transform: rotate(-90deg);
    opacity: 0;
  }
}

@keyframes rotateOutUpLeft {
  0% {
    -webkit-transform-origin: left bottom;
    -ms-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(0);
    -ms-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }

  100% {
    -webkit-transform-origin: left bottom;
    -ms-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate(-90deg);
    -ms-transform: rotate(-90deg);
    transform: rotate(-90deg);
    opacity: 0;
  }
}

.rotateOutUpLeft {
  -webkit-animation-name: rotateOutUpLeft;
  animation-name: rotateOutUpLeft;
}

@-webkit-keyframes rotateOutUpRight {
  0% {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }

  100% {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(90deg);
    transform: rotate(90deg);
    opacity: 0;
  }
}

@keyframes rotateOutUpRight {
  0% {
    -webkit-transform-origin: right bottom;
    -ms-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(0);
    -ms-transform: rotate(0);
    transform: rotate(0);
    opacity: 1;
  }

  100% {
    -webkit-transform-origin: right bottom;
    -ms-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    transform: rotate(90deg);
    opacity: 0;
  }
}

.rotateOutUpRight {
  -webkit-animation-name: rotateOutUpRight;
  animation-name: rotateOutUpRight;
}

@-webkit-keyframes slideInDown {
  0% {
    opacity: 0;
    -webkit-transform: translateY(-2000px);
    transform: translateY(-2000px);
  }

  100% {
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }
}

@keyframes slideInDown {
  0% {
    opacity: 0;
    -webkit-transform: translateY(-2000px);
    -ms-transform: translateY(-2000px);
    transform: translateY(-2000px);
  }

  100% {
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }
}

.slideInDown {
  -webkit-animation-name: slideInDown;
  animation-name: slideInDown;
}

@-webkit-keyframes slideInLeft {
  0% {
    opacity: 0;
    -webkit-transform: translateX(-2000px);
    transform: translateX(-2000px);
  }

  100% {
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }
}

@keyframes slideInLeft {
  0% {
    opacity: 0;
    -webkit-transform: translateX(-2000px);
    -ms-transform: translateX(-2000px);
    transform: translateX(-2000px);
  }

  100% {
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }
}

.slideInLeft {
  -webkit-animation-name: slideInLeft;
  animation-name: slideInLeft;
}

@-webkit-keyframes slideInRight {
  0% {
    opacity: 0;
    -webkit-transform: translateX(2000px);
    transform: translateX(2000px);
  }

  100% {
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }
}

@keyframes slideInRight {
  0% {
    opacity: 0;
    -webkit-transform: translateX(2000px);
    -ms-transform: translateX(2000px);
    transform: translateX(2000px);
  }

  100% {
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }
}

.slideInRight {
  -webkit-animation-name: slideInRight;
  animation-name: slideInRight;
}

@-webkit-keyframes slideOutLeft {
  0% {
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(-2000px);
    transform: translateX(-2000px);
  }
}

@keyframes slideOutLeft {
  0% {
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(-2000px);
    -ms-transform: translateX(-2000px);
    transform: translateX(-2000px);
  }
}

.slideOutLeft {
  -webkit-animation-name: slideOutLeft;
  animation-name: slideOutLeft;
}

@-webkit-keyframes slideOutRight {
  0% {
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(2000px);
    transform: translateX(2000px);
  }
}

@keyframes slideOutRight {
  0% {
    -webkit-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(2000px);
    -ms-transform: translateX(2000px);
    transform: translateX(2000px);
  }
}

.slideOutRight {
  -webkit-animation-name: slideOutRight;
  animation-name: slideOutRight;
}

@-webkit-keyframes slideOutUp {
  0% {
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(-2000px);
    transform: translateY(-2000px);
  }
}

@keyframes slideOutUp {
  0% {
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(-2000px);
    -ms-transform: translateY(-2000px);
    transform: translateY(-2000px);
  }
}

.slideOutUp {
  -webkit-animation-name: slideOutUp;
  animation-name: slideOutUp;
}

@-webkit-keyframes hinge {
  0% {
    -webkit-transform: rotate(0);
    transform: rotate(0);
    -webkit-transform-origin: top left;
    transform-origin: top left;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
  }

  20%, 60% {
    -webkit-transform: rotate(80deg);
    transform: rotate(80deg);
    -webkit-transform-origin: top left;
    transform-origin: top left;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
  }

  40% {
    -webkit-transform: rotate(60deg);
    transform: rotate(60deg);
    -webkit-transform-origin: top left;
    transform-origin: top left;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
  }

  80% {
    -webkit-transform: rotate(60deg) translateY(0);
    transform: rotate(60deg) translateY(0);
    opacity: 1;
    -webkit-transform-origin: top left;
    transform-origin: top left;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
  }

  100% {
    -webkit-transform: translateY(700px);
    transform: translateY(700px);
    opacity: 0;
  }
}

@keyframes hinge {
  0% {
    -webkit-transform: rotate(0);
    -ms-transform: rotate(0);
    transform: rotate(0);
    -webkit-transform-origin: top left;
    -ms-transform-origin: top left;
    transform-origin: top left;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
  }

  20%, 60% {
    -webkit-transform: rotate(80deg);
    -ms-transform: rotate(80deg);
    transform: rotate(80deg);
    -webkit-transform-origin: top left;
    -ms-transform-origin: top left;
    transform-origin: top left;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
  }

  40% {
    -webkit-transform: rotate(60deg);
    -ms-transform: rotate(60deg);
    transform: rotate(60deg);
    -webkit-transform-origin: top left;
    -ms-transform-origin: top left;
    transform-origin: top left;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
  }

  80% {
    -webkit-transform: rotate(60deg) translateY(0);
    -ms-transform: rotate(60deg) translateY(0);
    transform: rotate(60deg) translateY(0);
    opacity: 1;
    -webkit-transform-origin: top left;
    -ms-transform-origin: top left;
    transform-origin: top left;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
  }

  100% {
    -webkit-transform: translateY(700px);
    -ms-transform: translateY(700px);
    transform: translateY(700px);
    opacity: 0;
  }
}

.hinge {
  -webkit-animation-name: hinge;
  animation-name: hinge;
}

/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */

@-webkit-keyframes rollIn {
  0% {
    opacity: 0;
    -webkit-transform: translateX(-100%) rotate(-120deg);
    transform: translateX(-100%) rotate(-120deg);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateX(0px) rotate(0deg);
    transform: translateX(0px) rotate(0deg);
  }
}

@keyframes rollIn {
  0% {
    opacity: 0;
    -webkit-transform: translateX(-100%) rotate(-120deg);
    -ms-transform: translateX(-100%) rotate(-120deg);
    transform: translateX(-100%) rotate(-120deg);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateX(0px) rotate(0deg);
    -ms-transform: translateX(0px) rotate(0deg);
    transform: translateX(0px) rotate(0deg);
  }
}

.rollIn {
  -webkit-animation-name: rollIn;
  animation-name: rollIn;
}

/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */

@-webkit-keyframes rollOut {
  0% {
    opacity: 1;
    -webkit-transform: translateX(0px) rotate(0deg);
    transform: translateX(0px) rotate(0deg);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(100%) rotate(120deg);
    transform: translateX(100%) rotate(120deg);
  }
}

@keyframes rollOut {
  0% {
    opacity: 1;
    -webkit-transform: translateX(0px) rotate(0deg);
    -ms-transform: translateX(0px) rotate(0deg);
    transform: translateX(0px) rotate(0deg);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateX(100%) rotate(120deg);
    -ms-transform: translateX(100%) rotate(120deg);
    transform: translateX(100%) rotate(120deg);
  }
}

.rollOut {
  -webkit-animation-name: rollOut;
  animation-name: rollOut;
}
@-webkit-keyframes zoomIn {
  from {
    opacity: 0;
    -webkit-transform: scale3d(.3, .3, .3);
    transform: scale3d(.3, .3, .3);
  }

  50% {
    opacity: 1;
  }
}

@keyframes zoomIn {
  from {
    opacity: 0;
    -webkit-transform: scale3d(.3, .3, .3);
    transform: scale3d(.3, .3, .3);
  }

  50% {
    opacity: 1;
  }
}

.zoomIn {
  -webkit-animation-name: zoomIn;
  animation-name: zoomIn;
}

@-webkit-keyframes zoomInDown {
  from {
    opacity: 0;
    -webkit-transform: scale3d(.1, .1, .1) translate3d(0, -1000px, 0);
    transform: scale3d(.1, .1, .1) translate3d(0, -1000px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
    animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(0, 60px, 0);
    transform: scale3d(.475, .475, .475) translate3d(0, 60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
  }
}

@keyframes zoomInDown {
  from {
    opacity: 0;
    -webkit-transform: scale3d(.1, .1, .1) translate3d(0, -1000px, 0);
    transform: scale3d(.1, .1, .1) translate3d(0, -1000px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
    animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(0, 60px, 0);
    transform: scale3d(.475, .475, .475) translate3d(0, 60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
  }
}

.zoomInDown {
  -webkit-animation-name: zoomInDown;
  animation-name: zoomInDown;
}

@-webkit-keyframes zoomInLeft {
  from {
    opacity: 0;
    -webkit-transform: scale3d(.1, .1, .1) translate3d(-1000px, 0, 0);
    transform: scale3d(.1, .1, .1) translate3d(-1000px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
    animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(10px, 0, 0);
    transform: scale3d(.475, .475, .475) translate3d(10px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
  }
}

@keyframes zoomInLeft {
  from {
    opacity: 0;
    -webkit-transform: scale3d(.1, .1, .1) translate3d(-1000px, 0, 0);
    transform: scale3d(.1, .1, .1) translate3d(-1000px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
    animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(10px, 0, 0);
    transform: scale3d(.475, .475, .475) translate3d(10px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
  }
}

.zoomInLeft {
  -webkit-animation-name: zoomInLeft;
  animation-name: zoomInLeft;
}

@-webkit-keyframes zoomInRight {
  from {
    opacity: 0;
    -webkit-transform: scale3d(.1, .1, .1) translate3d(1000px, 0, 0);
    transform: scale3d(.1, .1, .1) translate3d(1000px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
    animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(-10px, 0, 0);
    transform: scale3d(.475, .475, .475) translate3d(-10px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
  }
}

@keyframes zoomInRight {
  from {
    opacity: 0;
    -webkit-transform: scale3d(.1, .1, .1) translate3d(1000px, 0, 0);
    transform: scale3d(.1, .1, .1) translate3d(1000px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
    animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(-10px, 0, 0);
    transform: scale3d(.475, .475, .475) translate3d(-10px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
  }
}

.zoomInRight {
  -webkit-animation-name: zoomInRight;
  animation-name: zoomInRight;
}

@-webkit-keyframes zoomInUp {
  from {
    opacity: 0;
    -webkit-transform: scale3d(.1, .1, .1) translate3d(0, 1000px, 0);
    transform: scale3d(.1, .1, .1) translate3d(0, 1000px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
    animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(0, -60px, 0);
    transform: scale3d(.475, .475, .475) translate3d(0, -60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
  }
}

@keyframes zoomInUp {
  from {
    opacity: 0;
    -webkit-transform: scale3d(.1, .1, .1) translate3d(0, 1000px, 0);
    transform: scale3d(.1, .1, .1) translate3d(0, 1000px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
    animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(0, -60px, 0);
    transform: scale3d(.475, .475, .475) translate3d(0, -60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
  }
}

.zoomInUp {
  -webkit-animation-name: zoomInUp;
  animation-name: zoomInUp;
}

@-webkit-keyframes zoomOut {
  from {
    opacity: 1;
  }

  50% {
    opacity: 0;
    -webkit-transform: scale3d(.3, .3, .3);
    transform: scale3d(.3, .3, .3);
  }

  to {
    opacity: 0;
  }
}

@keyframes zoomOut {
  from {
    opacity: 1;
  }

  50% {
    opacity: 0;
    -webkit-transform: scale3d(.3, .3, .3);
    transform: scale3d(.3, .3, .3);
  }

  to {
    opacity: 0;
  }
}

.zoomOut {
  -webkit-animation-name: zoomOut;
  animation-name: zoomOut;
}

@-webkit-keyframes zoomOutDown {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(0, -60px, 0);
    transform: scale3d(.475, .475, .475) translate3d(0, -60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
    animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
  }

  to {
    opacity: 0;
    -webkit-transform: scale3d(.1, .1, .1) translate3d(0, 2000px, 0);
    transform: scale3d(.1, .1, .1) translate3d(0, 2000px, 0);
    -webkit-transform-origin: center bottom;
    transform-origin: center bottom;
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
  }
}

@keyframes zoomOutDown {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(0, -60px, 0);
    transform: scale3d(.475, .475, .475) translate3d(0, -60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
    animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
  }

  to {
    opacity: 0;
    -webkit-transform: scale3d(.1, .1, .1) translate3d(0, 2000px, 0);
    transform: scale3d(.1, .1, .1) translate3d(0, 2000px, 0);
    -webkit-transform-origin: center bottom;
    transform-origin: center bottom;
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
  }
}

.zoomOutDown {
  -webkit-animation-name: zoomOutDown;
  animation-name: zoomOutDown;
}

@-webkit-keyframes zoomOutLeft {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(42px, 0, 0);
    transform: scale3d(.475, .475, .475) translate3d(42px, 0, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: scale(.1) translate3d(-2000px, 0, 0);
    transform: scale(.1) translate3d(-2000px, 0, 0);
    -webkit-transform-origin: left center;
    transform-origin: left center;
  }
}

@keyframes zoomOutLeft {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(42px, 0, 0);
    transform: scale3d(.475, .475, .475) translate3d(42px, 0, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: scale(.1) translate3d(-2000px, 0, 0);
    transform: scale(.1) translate3d(-2000px, 0, 0);
    -webkit-transform-origin: left center;
    transform-origin: left center;
  }
}

.zoomOutLeft {
  -webkit-animation-name: zoomOutLeft;
  animation-name: zoomOutLeft;
}

@-webkit-keyframes zoomOutRight {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(-42px, 0, 0);
    transform: scale3d(.475, .475, .475) translate3d(-42px, 0, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: scale(.1) translate3d(2000px, 0, 0);
    transform: scale(.1) translate3d(2000px, 0, 0);
    -webkit-transform-origin: right center;
    transform-origin: right center;
  }
}

@keyframes zoomOutRight {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(-42px, 0, 0);
    transform: scale3d(.475, .475, .475) translate3d(-42px, 0, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: scale(.1) translate3d(2000px, 0, 0);
    transform: scale(.1) translate3d(2000px, 0, 0);
    -webkit-transform-origin: right center;
    transform-origin: right center;
  }
}

.zoomOutRight {
  -webkit-animation-name: zoomOutRight;
  animation-name: zoomOutRight;
}

@-webkit-keyframes zoomOutUp {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(0, 60px, 0);
    transform: scale3d(.475, .475, .475) translate3d(0, 60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
    animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
  }

  to {
    opacity: 0;
    -webkit-transform: scale3d(.1, .1, .1) translate3d(0, -2000px, 0);
    transform: scale3d(.1, .1, .1) translate3d(0, -2000px, 0);
    -webkit-transform-origin: center bottom;
    transform-origin: center bottom;
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
  }
}

@keyframes zoomOutUp {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(.475, .475, .475) translate3d(0, 60px, 0);
    transform: scale3d(.475, .475, .475) translate3d(0, 60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
    animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
  }

  to {
    opacity: 0;
    -webkit-transform: scale3d(.1, .1, .1) translate3d(0, -2000px, 0);
    transform: scale3d(.1, .1, .1) translate3d(0, -2000px, 0);
    -webkit-transform-origin: center bottom;
    transform-origin: center bottom;
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
  }
}

.zoomOutUp {
  -webkit-animation-name: zoomOutUp;
  animation-name: zoomOutUp;
}
</style>

<style>
    /*======================================
    Variables
========================================*/
/*======================================
    Normalize CSS
========================================*/
@import url("https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap");
@import url("https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap");
html {
  scroll-behavior: smooth;
}

body {
  font-family: "DM Sans", sans-serif;
  font-weight: normal;
  font-style: normal;
  color: #727272;
  overflow-x: hidden;
  font-size: 15px;
}

p {
  margin: 0;
  padding: 0;
  font-size: 16px;
  line-height: 28px;
}

* {
  margin: 0;
  padding: 0;
}

.navbar-toggler:focus,
a:focus,
input:focus,
textarea:focus,
button:focus,
.btn:focus,
.btn.focus,
.btn:not(:disabled):not(.disabled).active,
.btn:not(:disabled):not(.disabled):active {
  text-decoration: none;
  outline: none;
  outline: none !important;
  border-color: none !important;
  -webkit-box-shadow: none !important;
          box-shadow: none !important;
}

.form-check-input:checked {
  background-color: #fd0d0d;
  border-color: #fd0d0d;
}

select {
  -webkit-writing-mode: horizontal-tb !important;
  text-rendering: auto;
  color: #27314F;
  letter-spacing: normal;
  word-spacing: normal;
  text-transform: none;
  text-indent: 0px;
  text-shadow: none;
  display: inline-block;
  text-align: start;
  -webkit-appearance: menulist;
     -moz-appearance: menulist;
          appearance: menulist;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  white-space: pre;
  -webkit-rtl-ordering: logical;
  background-color: -internal-light-dark(white, #3b3b3b);
  cursor: default;
  margin: 0em;
  font: 400 14px;
  border-radius: 0px;
  border-width: 1px;
  border-style: solid;
  border-color: -internal-light-dark(#767676, #858585);
  -o-border-image: initial;
     border-image: initial;
}

span,
a {
  display: inline-block;
  text-decoration: none;
  transition: all 0.4s ease;
  -webkit-transition: all 0.4s ease;
  -moz-transition: all 0.4s ease;
}

audio,
canvas,
iframe,
img,
svg,
video {
  vertical-align: middle;
}

img {
  max-width: 100%;
}

h1,
h2,
h3,
h4,
h5,
h6 {
  font-weight: 600;
  margin: 0px;
  color: #27314F;
  font-family: 'Poppins', sans-serif;
}

h1 a,
h2 a,
h3 a,
h4 a,
h5 a,
h6 a {
  color: inherit;
}

h1 {
  font-size: 50px;
}

h2 {
  font-size: 40px;
}

h3 {
  font-size: 30px;
}

h4 {
  font-size: 25px;
}

h5 {
  font-size: 20px;
}

h6 {
  font-size: 16px;
}

ul,
ol {
  margin: 0px;
  padding: 0px;
  list-style-type: none;
}

.mt-5 {
  margin-top: 5px;
}

.mt-10 {
  margin-top: 10px;
}

.mt-15 {
  margin-top: 15px;
}

.mt-20 {
  margin-top: 20px;
}

.mt-25 {
  margin-top: 25px;
}

.mt-30 {
  margin-top: 30px;
}

.mt-35 {
  margin-top: 35px;
}

.mt-40 {
  margin-top: 40px;
}

.mt-45 {
  margin-top: 45px;
}

.mt-50 {
  margin-top: 50px;
}

.mt-55 {
  margin-top: 55px;
}

.mt-60 {
  margin-top: 60px;
}

.mt-65 {
  margin-top: 65px;
}

.mt-70 {
  margin-top: 70px;
}

.mt-75 {
  margin-top: 75px;
}

.mt-80 {
  margin-top: 80px;
}

.mt-85 {
  margin-top: 85px;
}

.mt-90 {
  margin-top: 90px;
}

.mt-95 {
  margin-top: 95px;
}

.mt-100 {
  margin-top: 100px;
}

.mt-105 {
  margin-top: 105px;
}

.mt-110 {
  margin-top: 110px;
}

.mt-115 {
  margin-top: 115px;
}

.mt-120 {
  margin-top: 120px;
}

.mt-125 {
  margin-top: 125px;
}

.mt-130 {
  margin-top: 130px;
}

.mt-135 {
  margin-top: 135px;
}

.mt-140 {
  margin-top: 140px;
}

.mt-145 {
  margin-top: 145px;
}

.mt-150 {
  margin-top: 150px;
}

.mt-155 {
  margin-top: 155px;
}

.mt-160 {
  margin-top: 160px;
}

.mt-165 {
  margin-top: 165px;
}

.mt-170 {
  margin-top: 170px;
}

.mt-175 {
  margin-top: 175px;
}

.mt-180 {
  margin-top: 180px;
}

.mt-185 {
  margin-top: 185px;
}

.mt-190 {
  margin-top: 190px;
}

.mt-195 {
  margin-top: 195px;
}

.mt-200 {
  margin-top: 200px;
}

.mt-205 {
  margin-top: 205px;
}

.mt-210 {
  margin-top: 210px;
}

.mt-215 {
  margin-top: 215px;
}

.mt-220 {
  margin-top: 220px;
}

.mt-225 {
  margin-top: 225px;
}

.mb-5 {
  margin-bottom: 5px;
}

.mb-10 {
  margin-bottom: 10px;
}

.mb-15 {
  margin-bottom: 15px;
}

.mb-20 {
  margin-bottom: 20px;
}

.mb-25 {
  margin-bottom: 25px;
}

.mb-30 {
  margin-bottom: 30px;
}

.mb-35 {
  margin-bottom: 35px;
}

.mb-40 {
  margin-bottom: 40px;
}

.mb-45 {
  margin-bottom: 45px;
}

.mb-50 {
  margin-bottom: 50px;
}

.mb-55 {
  margin-bottom: 55px;
}

.mb-60 {
  margin-bottom: 60px;
}

.mb-65 {
  margin-bottom: 65px;
}

.mb-70 {
  margin-bottom: 70px;
}

.mb-75 {
  margin-bottom: 75px;
}

.mb-80 {
  margin-bottom: 80px;
}

.mb-85 {
  margin-bottom: 85px;
}

.mb-90 {
  margin-bottom: 90px;
}

.mb-95 {
  margin-bottom: 95px;
}

.mb-100 {
  margin-bottom: 100px;
}

.mb-105 {
  margin-bottom: 105px;
}

.mb-110 {
  margin-bottom: 110px;
}

.mb-115 {
  margin-bottom: 115px;
}

.mb-120 {
  margin-bottom: 120px;
}

.mb-125 {
  margin-bottom: 125px;
}

.mb-130 {
  margin-bottom: 130px;
}

.mb-135 {
  margin-bottom: 135px;
}

.mb-140 {
  margin-bottom: 140px;
}

.mb-145 {
  margin-bottom: 145px;
}

.mb-150 {
  margin-bottom: 150px;
}

.mb-155 {
  margin-bottom: 155px;
}

.mb-160 {
  margin-bottom: 160px;
}

.mb-165 {
  margin-bottom: 165px;
}

.mb-170 {
  margin-bottom: 170px;
}

.mb-175 {
  margin-bottom: 175px;
}

.mb-180 {
  margin-bottom: 180px;
}

.mb-185 {
  margin-bottom: 185px;
}

.mb-190 {
  margin-bottom: 190px;
}

.mb-195 {
  margin-bottom: 195px;
}

.mb-200 {
  margin-bottom: 200px;
}

.mb-205 {
  margin-bottom: 205px;
}

.mb-210 {
  margin-bottom: 210px;
}

.mb-215 {
  margin-bottom: 215px;
}

.mb-220 {
  margin-bottom: 220px;
}

.mb-225 {
  margin-bottom: 225px;
}

.pt-5 {
  padding-top: 5px;
}

.pt-10 {
  padding-top: 10px;
}

.pt-15 {
  padding-top: 15px;
}

.pt-20 {
  padding-top: 20px;
}

.pt-25 {
  padding-top: 25px;
}

.pt-30 {
  padding-top: 30px;
}

.pt-35 {
  padding-top: 35px;
}

.pt-40 {
  padding-top: 40px;
}

.pt-45 {
  padding-top: 45px;
}

.pt-50 {
  padding-top: 50px;
}

.pt-55 {
  padding-top: 55px;
}

.pt-60 {
  padding-top: 60px;
}

.pt-65 {
  padding-top: 65px;
}

.pt-70 {
  padding-top: 70px;
}

.pt-75 {
  padding-top: 75px;
}

.pt-80 {
  padding-top: 80px;
}

.pt-85 {
  padding-top: 85px;
}

.pt-90 {
  padding-top: 90px;
}

.pt-95 {
  padding-top: 95px;
}

.pt-100 {
  padding-top: 100px;
}

.pt-105 {
  padding-top: 105px;
}

.pt-110 {
  padding-top: 110px;
}

.pt-115 {
  padding-top: 115px;
}

.pt-120 {
  padding-top: 120px;
}

.pt-125 {
  padding-top: 125px;
}

.pt-130 {
  padding-top: 130px;
}

.pt-135 {
  padding-top: 135px;
}

.pt-140 {
  padding-top: 140px;
}

.pt-145 {
  padding-top: 145px;
}

.pt-150 {
  padding-top: 150px;
}

.pt-155 {
  padding-top: 155px;
}

.pt-160 {
  padding-top: 160px;
}

.pt-165 {
  padding-top: 165px;
}

.pt-170 {
  padding-top: 170px;
}

.pt-175 {
  padding-top: 175px;
}

.pt-180 {
  padding-top: 180px;
}

.pt-185 {
  padding-top: 185px;
}

.pt-190 {
  padding-top: 190px;
}

.pt-195 {
  padding-top: 195px;
}

.pt-200 {
  padding-top: 200px;
}

.pt-205 {
  padding-top: 205px;
}

.pt-210 {
  padding-top: 210px;
}

.pt-215 {
  padding-top: 215px;
}

.pt-220 {
  padding-top: 220px;
}

.pt-225 {
  padding-top: 225px;
}

.pb-5 {
  padding-bottom: 5px;
}

.pb-10 {
  padding-bottom: 10px;
}

.pb-15 {
  padding-bottom: 15px;
}

.pb-20 {
  padding-bottom: 20px;
}

.pb-25 {
  padding-bottom: 25px;
}

.pb-30 {
  padding-bottom: 30px;
}

.pb-35 {
  padding-bottom: 35px;
}

.pb-40 {
  padding-bottom: 40px;
}

.pb-45 {
  padding-bottom: 45px;
}

.pb-50 {
  padding-bottom: 50px;
}

.pb-55 {
  padding-bottom: 55px;
}

.pb-60 {
  padding-bottom: 60px;
}

.pb-65 {
  padding-bottom: 65px;
}

.pb-70 {
  padding-bottom: 70px;
}

.pb-75 {
  padding-bottom: 75px;
}

.pb-80 {
  padding-bottom: 80px;
}

.pb-85 {
  padding-bottom: 85px;
}

.pb-90 {
  padding-bottom: 90px;
}

.pb-95 {
  padding-bottom: 95px;
}

.pb-100 {
  padding-bottom: 100px;
}

.pb-105 {
  padding-bottom: 105px;
}

.pb-110 {
  padding-bottom: 110px;
}

.pb-115 {
  padding-bottom: 115px;
}

.pb-120 {
  padding-bottom: 120px;
}

.pb-125 {
  padding-bottom: 125px;
}

.pb-130 {
  padding-bottom: 130px;
}

.pb-135 {
  padding-bottom: 135px;
}

.pb-140 {
  padding-bottom: 140px;
}

.pb-145 {
  padding-bottom: 145px;
}

.pb-150 {
  padding-bottom: 150px;
}

.pb-155 {
  padding-bottom: 155px;
}

.pb-160 {
  padding-bottom: 160px;
}

.pb-165 {
  padding-bottom: 165px;
}

.pb-170 {
  padding-bottom: 170px;
}

.pb-175 {
  padding-bottom: 175px;
}

.pb-180 {
  padding-bottom: 180px;
}

.pb-185 {
  padding-bottom: 185px;
}

.pb-190 {
  padding-bottom: 190px;
}

.pb-195 {
  padding-bottom: 195px;
}

.pb-200 {
  padding-bottom: 200px;
}

.pb-205 {
  padding-bottom: 205px;
}

.pb-210 {
  padding-bottom: 210px;
}

.pb-215 {
  padding-bottom: 215px;
}

.pb-220 {
  padding-bottom: 220px;
}

.pb-225 {
  padding-bottom: 225px;
}

.img-bg {
  background-position: center center;
  background-size: cover;
  background-repeat: no-repeat;
  width: 100%;
  height: 100%;
}

@media only screen and (min-width: 480px) and (max-width: 767px) {
  .container {
    width: 450px;
  }
}

.bg-gray {
  background-color: #F4F7FA !important;
}

/* Bread Crumbs */
.breadcrumbs {
  position: relative;
  padding-top: 160px;
  padding-bottom: 120px;
  z-index: 2;
  text-align: left;
  background-color: #27314F;
  background-image: url("../images/footer/footer-bg.png");
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  -webkit-box-shadow: 0px 7px 30px rgba(0, 0, 0, 0.075);
          box-shadow: 0px 7px 30px rgba(0, 0, 0, 0.075);
}

.breadcrumbs.custom-padding {
  padding-bottom: 250px;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .breadcrumbs.custom-padding {
    padding-bottom: 200px;
  }
}

@media (max-width: 767px) {
  .breadcrumbs.custom-padding {
    padding-bottom: 180px;
  }
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .breadcrumbs {
    padding-top: 110px;
    padding-bottom: 60px;
  }
}

@media (max-width: 767px) {
  .breadcrumbs {
    padding-top: 100px;
    padding-bottom: 50px;
  }
}

.breadcrumbs .breadcrumbs-content {
  position: relative;
  text-align: center;
}

.breadcrumbs .breadcrumbs-content .page-title {
  font-size: 28px;
  color: #fff;
  font-weight: 700;
  position: relative;
  line-height: 28px;
  text-transform: capitalize;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .breadcrumbs .breadcrumbs-content .page-title {
    font-size: 24px;
    line-height: 28px;
  }
}

@media (max-width: 767px) {
  .breadcrumbs .breadcrumbs-content .page-title {
    font-size: 22px;
    text-align: center;
    line-height: 26px;
  }
}

.breadcrumbs .breadcrumbs-content .breadcrumb-nav {
  display: inline-block;
  margin-top: 10px;
  background: #ffffff24;
  padding: 13px 30px;
  border-radius: 5px;
  margin-top: 20px;
}

.breadcrumbs .breadcrumb-nav {
  text-align: right;
}

@media (max-width: 767px) {
  .breadcrumbs .breadcrumb-nav {
    text-align: center;
    margin-top: 15px;
  }
}

.breadcrumbs .breadcrumb-nav li {
  display: inline-block;
  position: relative;
  padding-right: 14px;
  margin-right: 14px;
  text-transform: capitalize;
  color: #fff;
}

.breadcrumbs .breadcrumb-nav li:after {
  content: "\ea14";
  font-family: lineIcons;
  font-size: 10px;
  position: absolute;
  top: 4px;
  right: -7px;
}

.breadcrumbs .breadcrumb-nav li:last-child {
  margin: 0;
  padding: 0;
}

.breadcrumbs .breadcrumb-nav li:last-child::after {
  display: none;
}

.breadcrumbs .breadcrumb-nav li,
.breadcrumbs .breadcrumb-nav li a {
  color: #fff;
  font-size: 14px;
  font-weight: 500;
}

.breadcrumbs .breadcrumb-nav li i,
.breadcrumbs .breadcrumb-nav li a i {
  font-size: 13px;
  display: inline-block;
  margin-right: 3px;
  position: relative;
  top: -1px;
}

.breadcrumbs .breadcrumb-nav li a {
  position: relative;
}

.breadcrumbs .breadcrumb-nav li a:hover {
  color: #e4e4e4;
}

.section {
  padding-top: 110px;
  padding-bottom: 110px;
  position: relative;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .section {
    padding-top: 70px;
    padding-bottom: 70px;
  }
}

@media (max-width: 767px) {
  .section {
    padding-top: 60px;
    padding-bottom: 60px;
  }
}

/* Section Title */
.section-title {
  text-align: center;
  margin-bottom: 80px;
  padding: 0 310px;
  position: relative;
  z-index: 5;
}

.section-title h3 {
  font-size: 15px;
  font-weight: 500;
  display: inline-block;
  margin-bottom: 15px;
  color: #800e0e;
  text-transform: capitalize;
}

.section-title h2 {
  font-size: 30px;
  margin-bottom: 20px;
  line-height: 45px;
  text-transform: capitalize;
  position: relative;
  font-weight: 700;
}

.section-title p {
  font-size: 16px;
  line-height: 28px;
}

@media only screen and (min-width: 992px) and (max-width: 1199px) {
  .section-title {
    padding: 0px 200px;
    margin-bottom: 70px;
  }
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .section-title {
    padding: 0px 20px;
    margin-bottom: 50px;
  }
  .section-title h3 {
    font-size: 14px;
    font-weight: 500;
    margin-bottom: 12px;
  }
  .section-title h2 {
    font-size: 26px;
    line-height: 32px;
    margin-bottom: 12px;
  }
  .section-title p {
    font-size: 15px;
  }
}

@media (max-width: 767px) {
  .section-title {
    padding: 0px 10px;
    margin-bottom: 40px;
  }
  .section-title h3 {
    font-size: 14px;
    font-weight: 500;
    margin-bottom: 12px;
  }
  .section-title h2 {
    font-size: 22px;
    line-height: 30px;
    margin-bottom: 10px;
  }
  .section-title p {
    font-size: 15px;
  }
}

.section-title.align-right {
  padding: 0;
  padding-left: 600px;
}

.section-title.align-right h2:before {
  display: none;
}

.section-title.align-right h2:after {
  position: absolute;
  right: 0;
  bottom: -1px;
  height: 2px;
  width: 50px;
  background: #0069ff;
  content: "";
}

.section-title.align-left {
  padding: 0;
  padding-right: 600px;
}

.section-title.align-left h2:before {
  left: 0;
  margin-left: 0;
}

/* One Click Scrool Top Button*/
.scroll-top {
  width: 48px;
  height: 48px;
  line-height: 48px;
  background: #27314F;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  font-size: 22px;
  color: #fff !important;
  position: fixed;
  bottom: 30px;
  right: 30px;
  z-index: 9;
  cursor: pointer;
  -webkit-transition: all .3s ease-out 0s;
  transition: all .3s ease-out 0s;
  border-radius: 50%;
}

.scroll-top:hover {
  -webkit-transform: scale(1.08);
          transform: scale(1.08);
  background-color: #0069ff;
}

/* Overlay */
.overlay {
  position: relative;
  z-index: 1;
}

.overlay::before {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  opacity: 0.4;
  background: #27314F;
  content: "";
  -webkit-transition: all 0.4s ease;
  transition: all 0.4s ease;
  z-index: -1;
}

/* Pagination CSS */
.pagination {
  text-align: left;
  margin: 60px 0 0 0;
  display: block;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .pagination {
    margin-top: 50px;
  }
}

@media (max-width: 767px) {
  .pagination {
    margin-top: 40px;
  }
}

.pagination.center {
  text-align: center;
}

.pagination.right {
  text-align: right;
}

.pagination.left {
  text-align: left;
}

.pagination .pagination-list {
  display: inline-block;
  overflow: hidden;
}

.pagination .pagination-list li {
  margin-right: 5px;
  display: inline-block;
  margin-top: 10px;
}

.pagination .pagination-list li:last-child {
  margin-right: 0px;
}

.pagination .pagination-list li a {
  background: #fff;
  color: #081828;
  font-weight: 500;
  font-size: 15px;
  border-radius: 5px;
  padding: 8px 20px;
  text-align: center;
  border: 1px solid #eee;
}

.pagination .pagination-list li.active a,
.pagination .pagination-list li:hover a {
  background: #0069ff;
  color: #fff;
  border-color: transparent;
}

.pagination .pagination-list li a i {
  font-size: 13px;
}

.blog-grids.pagination {
  margin-top: 50px;
  text-align: center;
}

.button .btn {
  display: inline-block;
  text-transform: capitalize;
  font-size: 15px;
  font-weight: 500;
  padding: 14px 30px;
  background-color: #9e0c0cd6;
  color: #fff;
  border: none;
  -webkit-transition: all 0.4s ease;
  transition: all 0.4s ease;
  border-radius: 30px;
  position: relative;
  z-index: 1;
  margin-right: 7px;
  overflow: hidden;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .button .btn {
    padding: 14px 25px;
    font-size: 14px;
    font-weight: 500;
  }
}

@media (max-width: 767px) {
  .button .btn {
    padding: 14px 25px;
    font-size: 14px;
    font-weight: 500;
  }
}

.button .btn i {
  display: inline-block;
  margin-right: 5px;
}

.button .btn:last-child {
  margin: 0;
}

.button .btn:hover {
  color: #fff;
  background-color: #27314F;
}

.button {
  margin-left: 0 !important;
}

.button .btn-alt {
  background-color: #27314F !important;
  color: #fff !important;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .button .btn-alt {
    padding: 14px 25px;
    font-size: 14px;
    font-weight: 500;
  }
}

@media (max-width: 767px) {
  .button .btn-alt {
    padding: 14px 25px;
    font-size: 14px;
    font-weight: 500;
  }
}

.button .btn-alt:hover {
  background-color: #0069ff !important;
  color: #fff !important;
}

.align-left {
  text-align: left;
}

.align-right {
  text-align: right;
}

.align-center {
  text-align: center;
}

/* Preloader */
.preloader {
  position: fixed;
  top: 0;
  left: 0;
  z-index: 999999999;
  width: 100%;
  height: 100%;
  background-color: #fff;
  overflow: hidden;
}

.preloader-inner {
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}

.preloader-icon {
  width: 100px;
  height: 100px;
  display: inline-block;
  padding: 0px;
}

.preloader-icon span {
  position: absolute;
  display: inline-block;
  width: 100px;
  height: 100px;
  border-radius: 100%;
  background: #0069ff;
  -webkit-animation: preloader-fx 1.6s linear infinite;
  animation: preloader-fx 1.6s linear infinite;
}

.preloader-icon span:last-child {
  animation-delay: -0.8s;
  -webkit-animation-delay: -0.8s;
}

@keyframes preloader-fx {
  0% {
    -webkit-transform: scale(0, 0);
            transform: scale(0, 0);
    opacity: 0.5;
  }
  100% {
    -webkit-transform: scale(1, 1);
            transform: scale(1, 1);
    opacity: 0;
  }
}

@-webkit-keyframes preloader-fx {
  0% {
    -webkit-transform: scale(0, 0);
    opacity: 0.5;
  }
  100% {
    -webkit-transform: scale(1, 1);
    opacity: 0;
  }
}

/*======================================
	01. Start Header CSS
========================================*/
.header {
  width: 100%;
  background: transparent;
  position: absolute;
  width: 100%;
  left: 0;
  top: 0;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .header {
    padding: 18px 0 !important;
  }
  .header .mobile-menu-btn .toggler-icon {
    background-color: #fff;
  }
  .header .button {
    margin: 0 !important;
  }
  .header .navbar-collapse {
    position: absolute;
    top: 67px !important;
    left: 0;
    width: 100%;
    background-color: #fff;
    z-index: 9;
    -webkit-box-shadow: 0px 15px 20px 0px rgba(0, 0, 0, 0.1);
            box-shadow: 0px 15px 20px 0px rgba(0, 0, 0, 0.1);
    padding: 10px 20px;
    max-height: 350px;
    overflow-y: scroll;
    border-top: 1px solid #eee;
    border-radius: 8px;
  }
  .header .navbar .navbar-nav .nav-item a:hover {
    color: #0069ff !important;
  }
  .header .navbar .navbar-nav .nav-item a.active {
    color: #0069ff !important;
  }
  .header .navbar-nav .nav-item {
    margin: 0;
  }
  .header .navbar-nav .nav-item:hover a {
    color: #0069ff;
  }
  .header .navbar-nav .nav-item a {
    padding: 12px 16px !important;
  }
  .header .navbar-nav .nav-item a::before {
    display: none;
  }
  .header .navbar-nav .nav-item .sub-menu {
    position: static;
    width: 100%;
    opacity: 1;
    visibility: visible;
    -webkit-box-shadow: none;
            box-shadow: none;
    padding: 0;
    border: none;
    margin-left: 15px;
    margin-right: 15px;
  }
  .header .navbar-nav .nav-item .sub-menu .nav-item a {
    padding: 12px 12px;
  }
  .header .navbar-nav .nav-item .sub-menu .nav-item a:hover {
    color: #0069ff !important;
  }
  .header .navbar-nav .nav-item a {
    color: #051441;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
        -ms-flex-pack: justify;
            justify-content: space-between;
    padding: 10px 0;
  }
  .header .navbar-nav .nav-item a::after {
    opacity: 1;
    visibility: visible;
  }
  .header .navbar-nav .nav-item .sub-menu li.active {
    background: #fff !important;
    color: #0069ff !important;
  }
  .header .navbar-nav .nav-item .sub-menu .nav-item {
    margin: 0 !important;
  }
  .header .navbar-nav .nav-item .sub-menu .nav-item a {
    padding: 10px 12px !important;
  }
  .header .navbar-nav .nav-item .sub-menu li:hover {
    background: #fff !important;
    color: #0069ff !important;
  }
  .header .navbar-nav .nav-item a {
    font-size: 14px;
  }
  .header .navbar-nav .nav-item a:hover {
    color: #0069ff;
  }
}

@media (max-width: 767px) {
  .header {
    padding: 18px 0 !important;
  }
  .header .mobile-menu-btn .toggler-icon {
    background-color: #fff;
  }
  .header .navbar-collapse {
    position: absolute;
    top: 62px !important;
    left: 0;
    width: 100%;
    background-color: #fff;
    z-index: 9;
    -webkit-box-shadow: 0px 15px 20px 0px rgba(0, 0, 0, 0.1);
            box-shadow: 0px 15px 20px 0px rgba(0, 0, 0, 0.1);
    padding: 10px 20px;
    max-height: 350px;
    overflow-y: scroll;
    border-top: 1px solid #eee;
    border-radius: 8px;
  }
  .header .navbar .navbar-nav .nav-item a:hover {
    color: #0069ff !important;
  }
  .header .navbar .navbar-nav .nav-item a.active {
    color: #0069ff !important;
  }
  .header .navbar-nav .nav-item {
    margin: 0;
  }
  .header .navbar-nav .nav-item:hover a {
    color: #0069ff;
  }
  .header .navbar-nav .nav-item a {
    padding: 12px 16px !important;
  }
  .header .navbar-nav .nav-item a::before {
    display: none;
  }
  .header .navbar-nav .nav-item .sub-menu {
    position: static;
    width: 100%;
    opacity: 1;
    visibility: visible;
    -webkit-box-shadow: none;
            box-shadow: none;
    padding: 0;
    border: none;
    margin-left: 15px;
    margin-right: 15px;
  }
  .header .navbar-nav .nav-item .sub-menu .nav-item a {
    padding: 12px 12px;
  }
  .header .navbar-nav .nav-item .sub-menu .nav-item a:hover {
    color: #0069ff !important;
  }
  .header .navbar-nav .nav-item a {
    color: #051441;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
        -ms-flex-pack: justify;
            justify-content: space-between;
    padding: 10px 0;
  }
  .header .navbar-nav .nav-item a::after {
    opacity: 1;
    visibility: visible;
  }
  .header .navbar-nav .nav-item .sub-menu li.active {
    background: #fff !important;
    color: #0069ff !important;
  }
  .header .navbar-nav .nav-item .sub-menu .nav-item {
    margin: 0 !important;
  }
  .header .navbar-nav .nav-item .sub-menu .nav-item a {
    padding: 10px 12px !important;
  }
  .header .navbar-nav .nav-item .sub-menu li:hover {
    background: #fff !important;
    color: #0069ff !important;
  }
  .header .navbar-nav .nav-item a {
    font-size: 14px;
  }
  .header .navbar-nav .nav-item a:hover {
    color: #0069ff;
  }
}

.header .navbar-brand img {
  width: 190px;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .header .navbar-brand img {
    width: 190px;
  }
}

@media (max-width: 767px) {
  .header .navbar-brand img {
    width: 180px;
  }
}

.header.sticky .button .btn {
  background-color: #9d0e0ec1;
  color: #fff;
}

.header.sticky .button .btn:hover {
  background-color: #27314F;
  color: #fff;
}

.sticky {
  position: fixed;
  z-index: 99;
  -webkit-box-shadow: 0px 20px 50px 0px rgba(0, 0, 0, 0.05);
          box-shadow: 0px 20px 50px 0px rgba(0, 0, 0, 0.05);
  -webkit-transition: all 0.3s ease-out 0s;
  transition: all 0.3s ease-out 0s;
  top: 0;
  background-color: #fff;
}

.navbar-expand-lg .navbar-nav {
  margin: 0;
  margin-left: auto !important;
  margin-right: auto !important;
}

.header .navbar .navbar-nav .nav-item a.active {
  color: #fff;
}

.sticky .navbar .navbar-nav .nav-item a.active {
  color: #810808;
}

.sticky .navbar .navbar-nav .nav-item a {
  color: #27314F;
}

.header .navbar .navbar-nav .nav-item .sub-menu a.active {
  color: #fff;
}

.sticky .navbar .navbar-nav .nav-item .sub-menu a.active {
  color: #fff;
}

.sticky .navbar .mobile-menu-btn .toggler-icon {
  background: #333;
}

/*===== NAVBAR =====*/
.navbar-area {
  width: 100%;
  z-index: 99;
  -webkit-transition: all 0.3s ease-out 0s;
  transition: all 0.3s ease-out 0s;
  padding: 0;
}

.navbar-area.sticky {
  position: fixed;
  z-index: 99;
  -webkit-box-shadow: 0px 20px 50px 0px rgba(0, 0, 0, 0.05);
          box-shadow: 0px 20px 50px 0px rgba(0, 0, 0, 0.05);
  -webkit-transition: all 0.3s ease-out 0s;
  transition: all 0.3s ease-out 0s;
  padding: 0px 0;
}

.navbar {
  padding: 0;
  position: relative;
  -webkit-transition: all 0.3s ease-out 0s;
  transition: all 0.3s ease-out 0s;
}

.navbar-brand {
  padding-left: 0;
  border-radius: 0;
}

.mobile-menu-btn {
  padding: 0px;
}

.mobile-menu-btn:focus {
  text-decoration: none;
  outline: none;
  -webkit-box-shadow: none;
          box-shadow: none;
}

.mobile-menu-btn .toggler-icon {
  width: 30px;
  height: 2px;
  background-color: #222;
  display: block;
  margin: 5px 0;
  position: relative;
  -webkit-transition: all 0.3s ease-out 0s;
  transition: all 0.3s ease-out 0s;
}

.mobile-menu-btn.active .toggler-icon:nth-of-type(1) {
  -webkit-transform: rotate(45deg);
          transform: rotate(45deg);
  top: 7px;
}

.mobile-menu-btn.active .toggler-icon:nth-of-type(2) {
  opacity: 0;
}

.mobile-menu-btn.active .toggler-icon:nth-of-type(3) {
  -webkit-transform: rotate(135deg);
          transform: rotate(135deg);
  top: -7px;
}

.navbar-nav .nav-item {
  z-index: 1;
  position: relative;
  margin-right: 40px;
}

.navbar-nav .nav-item:last-child {
  margin-right: 0 !important;
}

.navbar-nav .nav-item:hover a {
  color: #fff;
}

.sticky .navbar-nav .nav-item:hover a {
  color: #0069ff;
}

.navbar-nav .nav-item a {
  font-size: 15px;
  color: #ffffffa1;
  -webkit-transition: all 0.3s ease-out 0s;
  transition: all 0.3s ease-out 0s;
  position: relative;
  padding: 35px 0;
  display: -webkit-inline-box;
  display: -ms-inline-flexbox;
  display: inline-flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  font-weight: 500;
  transition: all 0.3s ease-out 0s;
  position: relative;
  text-transform: capitalize;
}

.navbar-nav .nav-item:hover a:before {
  width: 100%;
}

.navbar-nav .nav-item a.active {
  color: #fff;
}

.navbar-nav .nav-item a.dd-menu::after {
  content: "\ea5e";
  font: normal normal normal 1em/1 "LineIcons";
  font-size: 1em;
  position: absolute;
  right: -18px;
  font-size: 11px;
  top: 50%;
  margin-left: 5px;
  margin-top: -5px;
  -webkit-transition: all 0.3s ease-out 0s;
  transition: all 0.3s ease-out 0s;
  height: 10px;
}

.navbar-nav .nav-item:hover a.dd-menu::after {
  -webkit-transform: rotate(180deg);
          transform: rotate(180deg);
}

@media only screen and (min-width: 992px) and (max-width: 1199px) {
  .navbar-nav .nav-item a.dd-menu::after {
    right: 13px;
  }
}

@media only screen and (min-width: 768px) and (max-width: 991px), (max-width: 767px) {
  .navbar-nav .nav-item a.dd-menu::after {
    top: 18px;
    right: 0;
    -webkit-transform: rotate(180deg);
            transform: rotate(180deg);
  }
}

@media only screen and (min-width: 768px) and (max-width: 991px), (max-width: 767px) {
  .navbar-nav .nav-item a.collapsed::after {
    -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
  }
}

.navbar-nav .nav-item:hover > .sub-menu {
  top: 100%;
  opacity: 1;
  visibility: visible;
}

.navbar-nav .nav-item:hover > .sub-menu .sub-menu {
  left: 100%;
  top: 0;
}

.navbar-nav .nav-item .sub-menu {
  padding: 30px;
  min-width: 240px;
  background: #fff;
  -webkit-box-shadow: 0px 5px 20px #0000001a;
          box-shadow: 0px 5px 20px #0000001a;
  position: absolute;
  top: 110% !important;
  left: 0;
  opacity: 0;
  visibility: hidden;
  -webkit-transition: all 0.3s ease-out 0s;
  transition: all 0.3s ease-out 0s;
  border-radius: 0 0 4px 4px;
  border-radius: 5px;
}

.navbar-nav .nav-item:hover .sub-menu {
  top: 100% !important;
}

.navbar-nav .nav-item .sub-menu .nav-item {
  width: 100%;
  margin-bottom: 15px;
}

.navbar-nav .nav-item .sub-menu .nav-item:last-child {
  margin: 0;
}

.navbar-nav .nav-item .sub-menu .nav-item a {
  padding: 0;
  color: #888;
  display: block;
  width: 100%;
  font-size: 14px;
  font-weight: 500;
  text-transform: capitalize;
  position: relative;
  z-index: 1;
  border-radius: 4px;
  overflow: hidden;
  -webkit-transition: all 0.1s ease;
  transition: all 0.1s ease;
}

.navbar-nav .nav-item .sub-menu .nav-item a:hover {
  color: #0069ff;
}

.navbar-nav .nav-item .sub-menu.left-menu {
  left: -100%;
}

.navbar-nav .nav-item .sub-menu.collapse:not(.show) {
  display: block;
}

@media only screen and (min-width: 768px) and (max-width: 991px), (max-width: 767px) {
  .navbar-nav .nav-item .sub-menu.collapse:not(.show) {
    display: none;
  }
}

.navbar-nav .nav-item .sub-menu > li {
  display: block;
  margin-left: 0;
}

.navbar-nav .nav-item .sub-menu > li:last-child {
  border: none;
}

.navbar-nav .nav-item .sub-menu > li.active > a {
  color: #0069ff !important;
}

.navbar-nav .nav-item .sub-menu > li > a {
  font-weight: 400;
  display: block;
  padding: 12px 15px;
  font-size: 14px;
  color: #222;
  font-weight: 400;
}

.navbar-nav .nav-item .sub-menu > li:first-child a {
  border: none;
}

.header .button {
  display: inline-block;
  margin-left: 10px;
}

@media (max-width: 767px) {
  .header .button {
    display: none;
  }
}

.header .button .btn {
  background-color: #fff;
  color: #27314F;
  font-weight: 500;
}

.header .button .btn:hover {
  color: #27314F;
  background-color: #e9e9e9;
}

.sticky .button .btn {
  background-color: #0069ff;
  color: #fff;
}

.sticky .button .btn:hover {
  background-color: #0069ff;
  color: #fff;
}

/*======================================
     End Header CSS
  ========================================*/
/*======================================
    Hero Area CSS
========================================*/
.hero-area {
  position: relative;
  background-color: #27314F;
  padding: 280px 0 260px 0;
  z-index: 0;
  background-color: #0069ff;
  background-image: url("https://hiraoka.com.pe/media/mageplaza/blog/post/g/r/grupos_mundial_qatar_2022.jpg");
  background-size: cover;
  background-position: center;
}

.hero-area .hero-shape {
  position: absolute;
  left: 0;
  bottom: -1px;
  z-index: -1;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .hero-area {
    padding: 130px 0 180px 0;
  }
}

@media (max-width: 767px) {
  .hero-area {
    padding: 110px 0 150px 0;
  }
}

.hero-area .hero-content {
  border-radius: 0;
  position: relative;
  z-index: 1;
  text-align: center;
  padding: 0px 150px;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .hero-area .hero-content {
    padding: 0px 140px;
  }
}

@media (max-width: 767px) {
  .hero-area .hero-content {
    padding: 0 10px;
    text-align: center;
  }
}

.hero-area .hero-content h4 {
  color: #fff;
  font-weight: 500;
  font-size: 14px;
  margin-bottom: 20px;
  background-color: rgba(255, 255, 255, 0.178);
  padding: 12px 16px;
  border-radius: 5px;
  display: inline-block;
}

.hero-area .hero-content h1 {
  font-weight: 700;
  font-size: 35px;
  line-height: 52px;
  color: #fff;
  text-transform: capitalize;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .hero-area .hero-content h1 {
    font-size: 30px;
    font-weight: 700;
    line-height: 38px;
  }
}

@media (max-width: 767px) {
  .hero-area .hero-content h1 {
    font-size: 22px;
    line-height: 32px;
  }
}

.hero-area .hero-content p {
  font-weight: 400;
  font-size: 16px;
  line-height: 28px;
  color: #fff;
  margin-top: 20px;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .hero-area .hero-content p {
    font-size: 15px;
  }
}

@media (max-width: 767px) {
  .hero-area .hero-content p {
    margin-top: 15px;
    font-size: 15px;
    line-height: 26px;
  }
}

.hero-area .hero-content h5 {
  font-size: 15px;
  font-weight: 600;
  color: #fff;
  margin-top: 20px;
}

.hero-area .hero-content .button {
  margin-top: 50px;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .hero-area .hero-content .button {
    margin-top: 30px !important;
    width: 100%;
  }
}

@media (max-width: 767px) {
  .hero-area .hero-content .button {
    margin-top: 30px;
    width: 100%;
  }
}

.hero-area .hero-content .button a {
  height: 45px;
  width: 45px;
  line-height: 45px;
  text-align: center;
  display: inline-block;
  background-color: #fff;
  color: #0069ff;
  border-radius: 50%;
  font-size: 22px;
  -webkit-transition: all 0.3s ease;
  transition: all 0.3s ease;
}

.hero-area .hero-content .button a:hover {
  -webkit-transform: scale(1.07);
          transform: scale(1.07);
}

@media (max-width: 767px) {
  .hero-area .hero-content .button .btn {
    width: 60%;
    margin: 0;
    margin-bottom: 8px;
  }
  .hero-area .hero-content .button .btn:last-child {
    margin: 0;
  }
}

.hero-area .hero-image {
  text-align: right;
  position: relative;
  z-index: 0;
}

@media only screen and (min-width: 768px) and (max-width: 991px), (max-width: 767px) {
  .hero-area .hero-image {
    display: none;
  }
}

.hero-area .hero-image .main-image {
  width: 90%;
}

/*======================================
    Domain Search CSS
========================================*/
.domain-search {
  background-color: #fff;
}

.domain-search .inner-cotainer {
  background-color: #fff;
  border-radius: 10px;
  border: 1px solid #eee;
  padding: 75px 80px;
  text-align: center;
  position: relative;
  margin-top: -120px;
  z-index: 5;
  -webkit-box-shadow: 0px 10px 30px #0000000f;
          box-shadow: 0px 10px 30px #0000000f;
  overflow: hidden;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .domain-search .inner-cotainer {
    padding: 60px 60px;
  }
}

@media (max-width: 767px) {
  .domain-search .inner-cotainer {
    padding: 35px 20px;
  }
}

.domain-search .inner-cotainer .sahpe {
  height: 100px;
  width: 100px;
  border-radius: 50%;
  position: absolute;
  right: -40px;
  top: -40px;
  z-index: -1;
}

@media (max-width: 767px) {
  .domain-search .inner-cotainer .sahpe {
    height: 70px;
    width: 70px;
    right: -35px;
    top: -35px;
  }
}

.domain-search .inner-cotainer .sahpe2 {
  height: 100px;
  width: 100px;
  border-radius: 50%;
  position: absolute;
  left: -40px;
  bottom: -40px;
  z-index: -1;
}

@media (max-width: 767px) {
  .domain-search .inner-cotainer .sahpe2 {
    height: 70px;
    width: 70px;
    left: -35px;
    bottom: -35px;
  }
}

.domain-search .content h2 {
  font-size: 28px;
  font-weight: 700;
  margin-bottom: 20px;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .domain-search .content h2 {
    font-size: 22px;
  }
}

@media (max-width: 767px) {
  .domain-search .content h2 {
    font-size: 18px;
  }
}

.domain-search .content h2 span {
  color: #838383;
  font-weight: 400;
  display: block;
  margin-bottom: 10px;
  font-size: 15px;
}

@media only screen and (min-width: 1200px) and (max-width: 1399px), only screen and (min-width: 1400px) {
  .domain-search .content p {
    padding: 0px 20px;
  }
}

.domain-search .content .extention-list {
  display: block;
  margin-top: 40px;
}

@media (max-width: 767px) {
  .domain-search .content .extention-list {
    margin-top: 20px;
  }
}

.domain-search .content .extention-list li {
  display: inline-block;
  margin-right: 12px;
}

@media (max-width: 767px) {
  .domain-search .content .extention-list li {
    margin-bottom: 12px;
  }
  .domain-search .content .extention-list li:last-child {
    margin: 0;
  }
}

.domain-search .content .extention-list li:last-child {
  margin: 0;
  padding: 0;
  border: none;
}

.domain-search .content .extention-list li a {
  color: #27314F;
  -webkit-box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.075);
          box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.075);
  padding: 20px;
  border-radius: 6px;
  font-size: 18px;
  font-weight: 600;
  text-align: center;
  -webkit-transition: 0.3s ease;
  transition: 0.3s ease;
  background-color: #fff;
  border-bottom: 3px solid transparent;
}

@media (max-width: 767px) {
  .domain-search .content .extention-list li a {
    font-size: 15px;
    -webkit-box-shadow: none;
            box-shadow: none;
    border: none;
    padding: 0;
  }
  .domain-search .content .extention-list li a span {
    margin: 0;
  }
  .domain-search .content .extention-list li a:hover {
    color: #0069ff;
    -webkit-box-shadow: none !important;
            box-shadow: none !important;
  }
}

.domain-search .content .extention-list li a:hover {
  -webkit-box-shadow: 0px 10px 30px rgba(0, 0, 0, 0.11);
          box-shadow: 0px 10px 30px rgba(0, 0, 0, 0.11);
  border-bottom-color: #0069ff;
}

.domain-search .content .extention-list li a span {
  display: block;
  margin-left: 4px;
  font-size: 14px;
  font-weight: 400;
  color: #999;
  margin-top: 4px;
}

@media (max-width: 767px) {
  .domain-search .content .extention-list li a span {
    margin: 0;
  }
}

/* Search Form */
.search-form {
  margin-top: 40px;
  -webkit-box-shadow: 0px 10px 30px #0000000f;
          box-shadow: 0px 10px 30px #0000000f;
  border-radius: 5px;
  padding: 20px;
}

@media (max-width: 767px) {
  .search-form {
    padding: 15px;
  }
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .search-form {
    margin-top: 30px;
  }
}

@media (max-width: 767px) {
  .search-form {
    margin-top: 20px;
  }
}

.search-form .search-input {
  position: relative;
}

@media (max-width: 767px) {
  .search-form .search-input {
    margin-bottom: 15px;
  }
}

.search-form .search-input input {
  width: 100%;
  background: #fff;
  border-radius: 4px;
  border: none;
  padding: 0 25px;
  height: 55px;
  font-size: 15px;
  border: 1px solid #eee;
}

.search-form .search-input input:focus {
  outline: none !important;
}

.search-form .search-input label {
  position: absolute;
  right: 25px;
  top: 50%;
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
  font-size: 13px;
}

.search-form .search-input select {
  width: 100%;
  background: #fff;
  border-radius: 4px;
  border: 1px solid #eee;
  padding: 0 25px;
  height: 55px;
  -webkit-appearance: none;
     -moz-appearance: none;
          appearance: none;
  font-size: 15px;
}

.search-form .search-input select:focus {
  outline: none !important;
}

.search-form button {
  width: 100%;
  border: none;
  padding: 0 25px;
  height: 55px;
  border-radius: 4px !important;
}

.search-form button.btn:hover {
  background: #27314F;
  color: #fff;
}

.search-form button i {
  display: inline-block;
  margin-right: 5px;
}

/*======================================
    Why Choose Area CSS
========================================*/
.features {
  background-color: #fff;
  padding-top: 120px;
  position: relative;
}

@media only screen and (min-width: 768px) and (max-width: 991px), (max-width: 767px) {
  .features .responsive-space {
    margin-top: 40px;
  }
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .features {
    padding-top: 80px;
  }
}

@media (max-width: 767px) {
  .features {
    padding-top: 60px;
  }
}

.features .section-title {
  margin-bottom: 50px;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .features .section-title {
    margin-bottom: 40px;
  }
}

@media (max-width: 767px) {
  .features .section-title {
    margin-bottom: 30px;
  }
}

.features.style2 .single-feature {
  padding: 40px 35px;
}

.features.style2 .single-feature .list-icon {
  position: relative;
  display: inline-block;
  top: 0;
  left: 0;
  margin-bottom: 25px;
}

.features.style3 .section-title {
  margin: 0;
}

.features.style3 .single-item {
  margin-top: 100px;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .features.style3 .single-item {
    margin-top: 80px;
  }
}

@media (max-width: 767px) {
  .features.style3 .single-item {
    margin-top: 50px;
  }
}

.features.style3 .content {
  padding: 0;
}

@media only screen and (min-width: 1200px) and (max-width: 1399px), only screen and (min-width: 1400px) {
  .features.style3 .content.pl {
    padding-left: 60px;
  }
}

@media only screen and (min-width: 1200px) and (max-width: 1399px), only screen and (min-width: 1400px) {
  .features.style3 .content.pr {
    padding-right: 60px;
  }
}

.features.style3 .content h5 {
  color: #0069ff;
  margin-bottom: 15px;
  font-size: 15px;
  font-weight: 500;
}

.features.style3 .content h3 {
  margin-bottom: 20px;
  font-size: 30px;
  line-height: 42px;
  text-transform: capitalize;
  position: relative;
  font-weight: 700;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .features.style3 .content h3 {
    font-size: 26px;
    line-height: 35px;
  }
}

@media (max-width: 767px) {
  .features.style3 .content h3 {
    font-size: 22px;
    line-height: 30px;
  }
}

.features.style3 .content p {
  margin-bottom: 20px;
}

.features.style3 .content p:last-child {
  margin: 0;
}

.features.style3 .image {
  padding: 50px;
  background-color: #F4F7FA;
  border-radius: 8px;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .features.style3 .image {
    padding: 30px;
  }
}

@media (max-width: 767px) {
  .features.style3 .image {
    padding: 20px;
  }
}

.features.style3 .image img {
  width: 100%;
}

.features .shape {
  position: absolute;
  left: -76px;
  bottom: -74px;
  height: 150px;
  width: 150px;
  z-index: 5;
  opacity: 0.6;
  border-radius: 50%;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .features .shape {
    height: 120px;
    width: 120px;
    left: -60px;
    bottom: -60px;
  }
}

@media (max-width: 767px) {
  .features .shape {
    height: 100px;
    width: 100px;
    left: -50px;
    bottom: -50px;
  }
}

.features .explore-all {
  margin-top: 80px;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .features .explore-all {
    margin-top: 60px;
  }
}

@media (max-width: 767px) {
  .features .explore-all {
    margin-top: 40px;
  }
}

@media only screen and (min-width: 1200px) and (max-width: 1399px), only screen and (min-width: 1400px) {
  .features .content {
    padding-right: 90px;
  }
}

.features .content h2 {
  font-size: 30px;
  font-weight: 700;
  line-height: 45px;
  color: #27314F;
  margin-bottom: 30px;
}

.features .content h2 span {
  color: #0069ff;
  font-size: 15px;
  font-weight: 500;
  display: block;
  margin-bottom: 5px;
}

.features .content p {
  margin-bottom: 20px;
}

.features .content p:last-child {
  margin: 0;
}

.features .content .button {
  margin-top: 40px;
}

.features .single-feature {
  margin-top: 30px;
  padding: 50px 40px;
  padding-right: 60px;
  border-radius: 8px;
  text-align: left;
  background-color: #fff;
  position: relative;
  overflow: hidden;
  -webkit-transition: all 0.4s ease;
  transition: all 0.4s ease;
  padding-left: 130px;
  border: 1px solid #eeeeee80;
}

@media only screen and (min-width: 768px) and (max-width: 991px), (max-width: 767px) {
  .features .single-feature {
    padding: 40px 35px;
  }
}

.features .single-feature:hover {
  -webkit-box-shadow: 0px 10px 30px rgba(0, 0, 0, 0.096);
          box-shadow: 0px 10px 30px rgba(0, 0, 0, 0.096);
  -webkit-transform: translateY(-8px);
          transform: translateY(-8px);
}

.features .single-feature .list-icon {
  height: 65px;
  width: 65px;
  text-align: center;
  line-height: 65px;
  display: inline-block;
  background-color: #ff000036;
  color: #950e0ec7;
  font-size: 26px;
  border-radius: 50%;
  -webkit-transition: all 0.4s ease;
  transition: all 0.4s ease;
  position: absolute;
  left: 40px;
  top: 50px;
}

@media only screen and (min-width: 768px) and (max-width: 991px), (max-width: 767px) {
  .features .single-feature .list-icon {
    position: relative;
    display: inline-block;
    top: 0;
    left: 0;
    margin-bottom: 25px;
  }
}

.features .single-feature h3 {
  font-size: 16px;
  font-weight: 600;
  line-height: 26px;
  -webkit-transition: all 0.4s ease;
  transition: all 0.4s ease;
}

.features .single-feature p {
  margin-top: 15px;
}

/*======================================
	Pricing CSS
========================================*/
.pricing {
  background-color: #F6FBFF;
}

.pricing .nav {
  background-color: #fff;
  border: 1px solid #eee;
  padding: 10px 13px !important;
  border-radius: 40px !important;
}

.pricing .pricing-tabs {
  margin-bottom: 40px !important;
  text-align: center;
}

@media (max-width: 767px) {
  .pricing .pricing-tabs {
    margin-bottom: 20px !important;
  }
}

.pricing .nav-pills .nav-link {
  color: #27314F;
  border-radius: 30px !important;
  padding: 10px 25px !important;
}

.pricing .nav-pills .nav-link:hover {
  color: #0069ff;
}

.pricing .nav-pills .nav-link.active {
  background: #0069ff;
  color: #fff;
}

.pricing .nav-pills .nav-link.active .badge {
  background-color: #fff !important;
  color: #0069ff !important;
}

.pricing .section-title {
  margin-bottom: 60px;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .pricing .section-title {
    margin-bottom: 40px;
  }
}

@media (max-width: 767px) {
  .pricing .section-title {
    margin-bottom: 30px;
  }
}

.pricing .single-pricing {
  padding: 45px 40px;
  border-radius: 8px;
  -webkit-box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.068);
          box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.068);
  background-color: #fff;
  -webkit-transition: all 0.4s ease;
  transition: all 0.4s ease;
  text-align: center;
  position: relative;
  overflow: hidden;
  margin-top: 30px;
  z-index: 2;
}

.pricing .single-pricing .badge {
  position: absolute;
  left: -50px;
  top: 22px;
  background-color: #0069ff;
  color: #fff;
  padding: 6px 50px;
  -webkit-transform: rotate(-45deg);
          transform: rotate(-45deg);
}

.pricing .single-pricing .sahpe {
  height: 100px;
  width: 100px;
  border-radius: 50%;
  position: absolute;
  right: -46px;
  top: -46px;
  z-index: -1;
}

.pricing .single-pricing .sahpe2 {
  height: 100px;
  width: 100px;
  border-radius: 50%;
  position: absolute;
  left: -46px;
  bottom: -46px;
  z-index: -1;
}

.pricing .single-pricing:hover {
  -webkit-box-shadow: 0px 10px 40px rgba(0, 0, 0, 0.116);
          box-shadow: 0px 10px 40px rgba(0, 0, 0, 0.116);
  -webkit-transform: translateY(-8px);
          transform: translateY(-8px);
}

.pricing .single-pricing .title {
  font-size: 20px;
  font-weight: 600;
  margin-bottom: 20px;
  margin-top: 25px;
}

.pricing .single-pricing .start-at {
  display: block;
  margin-bottom: 6px;
}

.pricing .single-pricing .price {
  color: #0069ff;
  font-size: 38px;
  font-weight: 600;
  font-family: DM Sans;
}

.pricing .single-pricing .price .sent {
  font-size: 26px;
}

.pricing .single-pricing .price .duration {
  font-size: 14px;
  font-weight: 400;
  color: #727272;
}

.pricing .single-pricing .old-price {
  color: #727272;
  text-decoration: line-through;
  display: block;
  margin-top: 5px;
}

.pricing .single-pricing .save-price {
  font-size: 15px;
  font-weight: 500;
  color: #27314F;
  display: block;
  margin-top: 10px;
}

.pricing .single-pricing .button {
  margin-top: 30px;
}

.pricing .single-pricing .button .btn {
  background-color: #fff;
  border: 2px solid #eee;
  color: #27314F;
  padding: 12px 30px;
  font-size: 14px;
}

.pricing .single-pricing .button .btn:hover {
  color: #fff;
  background-color: #0069ff;
  border-color: transparent;
}

.pricing .single-pricing .item-list {
  margin-top: 25px;
  padding-top: 25px;
  border-top: 1px solid #eee;
}

.pricing .single-pricing .item-list li {
  display: block;
  margin-bottom: 10px;
  font-weight: 500;
  font-size: 14px;
  color: #27314F;
}

.pricing .single-pricing .item-list li:last-child {
  margin: 0;
}

/* Pricing Sryle 2 */
.pricing-style2 .section-title {
  margin-bottom: 50px;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .pricing-style2 .section-title {
    margin-bottom: 40px;
  }
}

@media (max-width: 767px) {
  .pricing-style2 .section-title {
    margin-bottom: 30px;
  }
}

.pricing-style2 .single-pricing {
  margin-top: 30px;
  padding: 45px 40px;
  border-radius: 8px;
  -webkit-box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.068);
          box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.068);
  background-color: #fff;
  -webkit-transition: all 0.4s ease;
  transition: all 0.4s ease;
}

.pricing-style2 .single-pricing:hover {
  -webkit-box-shadow: 0px 10px 40px rgba(0, 0, 0, 0.116);
          box-shadow: 0px 10px 40px rgba(0, 0, 0, 0.116);
  -webkit-transform: translateY(-8px);
          transform: translateY(-8px);
}

.pricing-style2 .single-pricing .p-icon {
  height: 70px;
  width: 70px;
  text-align: center;
  display: block;
  border-radius: 7px;
  background: #ff000052;
  line-height: 90px;
}

.pricing-style2 .single-pricing .p-icon i {
  color: #9e1414c7;
  font-size: 30px;
}

.pricing-style2 .single-pricing .title {
  font-size: 20px;
  font-weight: 600;
  margin-bottom: 12px;
  margin-top: 25px;
}

.pricing-style2 .single-pricing .des {
  margin-bottom: 25px;
}

.pricing-style2 .single-pricing .start-at {
  display: block;
  margin-bottom: 6px;
}

.pricing-style2 .single-pricing .price {
  color: #27314F;
  font-size: 38px;
  font-weight: 600;
  font-family: DM Sans;
}

.pricing-style2 .single-pricing .price .sent {
  font-size: 26px;
}

.pricing-style2 .single-pricing .price .duration {
  font-size: 14px;
  font-weight: 400;
  color: #727272;
}

.pricing-style2 .single-pricing .button {
  margin-top: 30px;
}

.pricing-style2 .single-pricing .button .btn {
  background-color: #fff;
  border: 2px solid #eee;
  color: #27314F;
  padding: 12px 30px;
  font-size: 14px;
}

.pricing-style2 .single-pricing .button .btn:hover {
  color: #fff;
  background-color: #0069ff;
  border-color: transparent;
}

/*======================================
    Call To Action CSS
========================================*/
.call-action {
  background-color: #fff;
}

.call-action .inner-content {
  position: relative;
  padding: 80px 0;
  border-radius: 10px;
  z-index: 0;
  overflow: hidden;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .call-action .inner-content {
    padding: 50px 0;
  }
}

@media (max-width: 767px) {
  .call-action .inner-content {
    padding: 50px 0;
    text-align: center;
  }
}

.call-action .inner-content .bg-shape {
  position: absolute;
  right: 0;
  bottom: 0;
  z-index: -1;
}

.call-action .text h2 {
  font-size: 28px;
  font-weight: 700;
  line-height: 42px;
  color: #081828;
}

.call-action .text h2 span {
  color: #be1818bc;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .call-action .text h2 {
    font-size: 25px;
    line-height: 38px;
  }
}

@media (max-width: 767px) {
  .call-action .text h2 {
    font-size: 22px;
    line-height: 32px;
  }
}

.call-action .text h2 span {
  display: block;
  color: #f11616c0;
}

.call-action .button {
  float: right;
}

@media (max-width: 767px) {
  .call-action .button {
    float: none;
    margin-top: 30px;
  }
}

/*======================================
	Footer CSS
========================================*/
.footer {
  position: relative;
  padding-bottom: 0;
  background-image: url("https://assets.adnradio.cl/2022/04/GettyImages-1389021061-e1648858142601-925x470.jpg");
  background-size: cover;
  background-position: center;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .footer {
    padding-top: 30px;
  }
}

@media (max-width: 767px) {
  .footer {
    padding-top: 20px;
  }
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .footer .single-footer {
    margin-top: 40px;
  }
}

@media (max-width: 767px) {
  .footer .single-footer {
    margin-top: 40px;
    text-align: center;
  }
}

.footer .single-footer.f-about {
  padding-right: 30px;
}

@media (max-width: 767px) {
  .footer .single-footer.f-about {
    padding: 0;
  }
}

.footer .single-footer.f-about .logo {
  margin-bottom: 20px;
}

.footer .single-footer.f-about .logo img {
  width: 200px;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .footer .single-footer.f-about .logo img {
    width: 190px;
  }
}

@media (max-width: 767px) {
  .footer .single-footer.f-about .logo img {
    width: 180px;
  }
}

.footer .single-footer.f-about p {
  color: #fff;
  margin-top: 20px;
  font-size: 15px;
  line-height: 24px;
}

.footer .single-footer.f-about .call {
  color: #fff;
  font-weight: 500;
  margin-top: 15px;
}

.footer .single-footer.f-about .call i {
  font-size: 18px;
  display: inline-block;
  margin-right: 5px;
}

.footer .single-footer.f-about .payments {
  margin-top: 30px;
}

.footer .single-footer.f-about .payments img {
  width: 70%;
}

.footer .single-footer.f-about .social-title {
  color: #fff;
  font-size: 13px;
  font-weight: 600;
  display: block;
  margin-bottom: 20px;
}

.footer .single-footer.f-about .social {
  margin-top: 30px;
}

.footer .single-footer.f-about .social li {
  display: inline-block;
  margin-right: 18px;
}

.footer .single-footer.f-about .social li:last-child {
  margin: 0;
}

.footer .single-footer.f-about .social li a {
  color: #fff;
  font-size: 16px;
}

.footer .single-footer.f-about .social li a:hover {
  color: #0069ff;
}

.footer .single-footer.f-about .copyright-text {
  color: #fff;
  font-size: 14px;
  margin-top: 30px;
}

.footer .single-footer.f-about .copyright-text span {
  display: block;
}

@media (max-width: 767px) {
  .footer .single-footer.f-about .copyright-text {
    margin-top: 20px;
  }
}

.footer .single-footer.f-about .copyright-text a {
  color: #fff;
  text-decoration: underline;
}

.footer .single-footer.f-link li {
  display: block;
  margin-bottom: 15px;
}

.footer .single-footer.f-link li:last-child {
  margin: 0;
}

.footer .single-footer.f-link li a {
  font-size: 15px;
  font-weight: 400;
  color: #fff;
}

.footer .single-footer.f-link li a:hover {
  color: #0069ff;
}

@media only screen and (min-width: 1200px) and (max-width: 1399px), only screen and (min-width: 1400px) {
  .footer .single-footer.latest-news {
    padding-left: 30px;
  }
}

.footer .single-footer.latest-news .single-head {
  padding: 30px;
  background-color: rgba(255, 255, 255, 0.089);
  border-radius: 7px;
}

.footer .single-footer.latest-news .single-news {
  margin-bottom: 20px;
}

.footer .single-footer.latest-news .single-news:last-child {
  margin: 0;
}

.footer .single-footer.latest-news .single-news .date {
  display: block;
  margin-bottom: 10px;
}

.footer .single-footer.latest-news .single-news .date a {
  font-size: 12px;
  color: #fff;
}

.footer .single-footer.latest-news .single-news .date a:hover {
  color: #0069ff;
}

.footer .single-footer.latest-news .single-news .post-title {
  line-height: 22px;
}

.footer .single-footer.latest-news .single-news .post-title a {
  font-size: 14px;
  font-weight: 500;
  color: #fff;
}

.footer .single-footer.latest-news .single-news .post-title a:hover {
  color: #0069ff;
}

.footer .single-footer h3 {
  font-size: 20px;
  font-weight: 600;
  display: block;
  margin-bottom: 35px;
  color: #fff;
}

@media only screen and (min-width: 768px) and (max-width: 991px) {
  .footer .single-footer h3 {
    margin-bottom: 25px;
  }
}

@media (max-width: 767px) {
  .footer .single-footer h3 {
    margin-bottom: 25px;
  }
}

.footer .footer-bottom-area .inner-content {
  border-top: 1px solid rgba(238, 238, 238, 0.288);
  padding: 35px 0;
  margin-top: 80px;
}

@media (max-width: 767px) {
  .footer .footer-bottom-area .inner-content {
    margin-top: 50px;
    text-align: center;
  }
}

.footer .footer-bottom-area p {
  color: #fff;
  font-size: 15px;
}

@media only screen and (min-width: 768px) and (max-width: 991px), (max-width: 767px) {
  .footer .footer-bottom-area p {
    line-height: 24px;
  }
}

@media (max-width: 767px) {
  .footer .footer-bottom-area p {
    margin-bottom: 20px;
  }
}

.footer .footer-bottom-area p a {
  text-decoration: underline;
  color: #fff;
}

.footer .footer-bottom-area p a:hover {
  color: #0069ff;
}

</style>