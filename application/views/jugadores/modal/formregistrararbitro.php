<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" id="modal_regitrar_arbitro">
  <div class="modal-dialog modal-col-md-10">
    <div class="modal-content">

          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Formulario Registro Asociados</h5>

            <h5 class="modal-title text-muted ml-1" id="nombre_socio"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form name="form_registrar_socios_asociados" id="form_registrar_socios_asociados" method="post" novalidate="">
                <div class="row">

                            <div class="col-md-6"> 
                              <div class="form-group "> 
                                <label class="control-label">DNI: <span class="symbol required"></span></label> 
                                <input type="text" class="form-control form-control-sm" data-form="form_registrar_socios_asociados" name="dni" id="dni" onkeyup="this.value=this.value.toUpperCase();" autocomplete="off" placeholder="Ingrese DNI" required="" aria-required="true">
                              </div> 
                            </div>

                            <div class="col-md-6"> 
                              <div class="form-group "> 
                                <label class="control-label">Fecha vencimiento (DNI): <span class="symbol required"></span></label> 
                                <input type="date" class="form-control-sm form-control" name="fecha_venc" id="fecha_venc" value="<?php echo date('Y-m-d')?>">
                              </div> 
                            </div>

                            
                            <div class="col-md-6"> 
                              <div class="form-group "> 
                                <label class="control-label">Nombres: <span class="symbol required"></span></label> 
                                <input type="text" class="form-control form-control-sm" name="nombres" id="nombres" onkeyup="this.value=this.value.toUpperCase();" autocomplete="off" placeholder="Ingrese nombres" required="" aria-required="true">
                              </div> 
                            </div>
                            
                            
                            <div class="col-md-6"> 
                              <div class="form-group "> 
                                <label class="control-label">Apellidos: <span class="symbol required"></span></label> 
                                <input type="text" class="form-control form-control-sm" name="apellidos" id="apellidos" onkeyup="this.value=this.value.toUpperCase();" autocomplete="off" placeholder="Ingrese apellidos" required="" aria-required="true">
                              </div> 
                            </div>

                            <div class="col-md-6"> 
                              <div class="form-group "> 
                                <label class="control-label">Fecha nacimiento: <span class="symbol required"></span></label> 
                                <input type="date" class="form-control-sm form-control" name="fecha_nac" id="fecha_nac" value="<?php echo date('Y-m-d')?>">
                              </div> 
                            </div>

                            <div class="col-md-6"> 
                              <div class="form-group "> 
                                <label class="control-label">Edad: <span class="symbol required"></span></label> 
                                <input type="number" class="form-control form-control-sm" name="edad" id="edad" onkeyup="this.value=this.value.toUpperCase();" autocomplete="off" placeholder="Ingrese edad" required="" aria-required="true">
                              </div> 
                            </div>


                            <div class="col-md-12"> 
                              <div class="form-group "> 
                                <label class="control-label">Dirección: <span class="symbol required"></span></label> 
                                <input type="text" class="form-control form-control-sm" name="direccion" id="direccion" onkeyup="this.value=this.value.toUpperCase();" autocomplete="off" placeholder="Ingrese su direccion" required="" aria-required="true">
                              </div> 
                            </div>

                            <div class="col-md-6"> 
                              <div class="form-group "> 
                                <label class="control-label">Distrito: <span class="symbol required"></span></label> 
                                <input type="text" class="form-control form-control-sm" name="distrito" id="distrito" onkeyup="this.value=this.value.toUpperCase();"  autocomplete="off" placeholder="Ingrese distrito" required="" aria-required="true">
                              </div> 
                            </div>

                            <div class="col-md-6"> 
                              <div class="form-group "> 
                                <label class="control-label">Telefono: <span class="symbol required"></span></label> 
                                <input type="number" class="form-control form-control-sm" name="telefono" id="telefono" onkeyup="this.value=this.value.toUpperCase();" autocomplete="off" placeholder="Ingrese numero celular o fijo" required="" aria-required="true">
                              </div> 
                            </div>

                            <div class="col-md-6"> 
                              <div class="form-group "> 
                                <label class="control-label">Operador: <span class="symbol required"></span></label> 
                                <input type="text" class="form-control form-control-sm" name="operador" id="operador" onkeyup="this.value=this.value.toUpperCase();" autocomplete="off" placeholder="Ingrese operador" required="" aria-required="true">
                              </div> 
                            </div>

                            <div class="col-md-6"> 
                              <div class="form-group "> 
                                <label class="control-label">¿Quien compro el chip? <span class="symbol required"></span></label> 
                                <input type="text" class="form-control form-control-sm" name="comprador" id="comprador" onkeyup="this.value=this.value.toUpperCase();" autocomplete="off" placeholder="Ingrese comprador" required="" aria-required="true">
                              </div> 
                            </div>



                            <div class="col-md-6"> 
                              <div class="form-group "> 
                                <label class="control-label">Correo Electronico: <span class="symbol required"></span></label> 
                                <input type="email" class="form-control form-control-sm" name="correo_electronico" id="correo_electronico" onkeyup="this.value=this.value.toUpperCase();"  autocomplete="off" placeholder="Ingrese correo electronico" required="" aria-required="true">
                              </div> 
                            </div>


                            <div class="col-md-6"> 
                              <div class="form-group "> 
                                <label class="control-label">Contraseña: <span class="symbol required"></span></label> 
                                <input type="text" placeholder="Ingrese Contraseña" class="form-control form-control-sm" name="contrasena" id="contrasena" autocomplete="off"  required="" aria-required="true">
                              </div> 
                            </div>
                            

                            <div class="col-md-6"> 
                              <div class="form-group "> 
                                <label class="control-label">CCI Soles: <span class="symbol required"></span></label> 
                                <input type="text" class="form-control form-control-sm" name="cci_soles" id="cci_soles" onkeyup="this.value=this.value.toUpperCase();" autocomplete="off" placeholder="Ingrese CCI " required="" aria-required="true">
                              </div> 
                            </div>


                          </div>
                        </form>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Cerrar</button>
              <button type="button" data-form="form_registrar_socios_asociados"  class="btn btn-sm btn-success btn-save-modal" data-action="registrarAsociado_asociados">Guardar Cambios</button>
          </div>
    </div>
  </div>
</div>

