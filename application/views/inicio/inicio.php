<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
        <div class="card-block">
			<div class="table-responsive">
			<table  style="width:100%; font-size: 10px;" class="table table-bordered col-md-12 text-center" id="tabla">
				<thead style="background-color:#ccc;">
					<tr>
						<th class="align-middle" scope="col"><a href="<?php echo site_url('Jugadores/jugadores'); ?>">jugadores</a></th>

						<?php  
						// 	$alumnos = json_decode(json_encode($alumnos),true);
						// 	foreach ($alumnos as $alumno)
						// 	{
						// 	echo '<tr>';
						// 	echo '<th class="align-middle" scope="col">'.$alumno['codigo'].'</th>';
						// 	echo '<th class="align-middle" scope="col">'.$alumno['apellidos'].'</th>';
						// 	echo '<th class="align-middle" scope="col">'.$alumno['nombres'].'</th>';
						// 	echo '<th class="align-middle" scope="col">'.$alumno['dni'].'</th>';
						// 	echo '<th class="align-middle" scope="col">'.$alumno['direccion'].'</th>';
						// 	echo '</tr>';
						// }
						?>
					</tr>
				</thead>
				<tbody>
				</tbody>
				</table>
			</div>
		</div>
</body>
</html> -->


<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
	<title>Mundial Qatar - 2022</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.svg" />

    <!-- ========================= CSS ========================= -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/LineIcons.3.0.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/tiny-slider.css" />
    <link rel="stylesheet" href="assets/css/glightbox.min.css" />
    <link rel="stylesheet" href="assets/css/main.css" />

</head>

<body>

    <!-- Empezador -->
    <div class="preloader">
        <div class="preloader-inner">
            <div class="preloader-icon">
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <!--Terminado -->

    <!-- Empezado -->
    <header class="header navbar-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="nav-inner">
                        <nav class="navbar navbar-expand-lg">
                            <a class="navbar-brand" href="index.html">
                                <!-- <img src="https://digitalhub.fifa.com/transform/412435a1-ebae-4d3c-be0b-b24b30b13c74/FWC-2" alt="Logo"> -->
                            </a>
                            <button class="navbar-toggler mobile-menu-btn" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                                <span class="toggler-icon"></span>
                                <span class="toggler-icon"></span>
                                <span class="toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
                                <ul id="nav" class="navbar-nav ms-auto">
                                    <li class="nav-item">
                                        <a href="http://proyecto_db.test/" class="active" aria-label="Toggle navigation">INICIO</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="dd-menu collapsed" href="javascript:void(0)" data-bs-toggle="collapse"
                                            data-bs-target="#submenu-1-1" aria-controls="navbarSupportedContent"
                                            aria-expanded="false" aria-label="Toggle navigation">TABLAS</a>
                                        <ul class="sub-menu collapse" id="submenu-1-1">
                                        <li class="nav-item"><a href="<?php echo site_url('Jugadores/jugadores'); ?>">Arbitros</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Estadios/estadios'); ?>">Estadios</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Fases/fases'); ?>">Fases</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Goles/goles'); ?>">Goles</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Grupos/grupos'); ?>">Grupos</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Jugadores2/jugadores2'); ?>">Jugadores</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Partidos/partidos'); ?>">Partidos</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Poscicionesjugador/poscicionesjugador'); ?>">Posciciones</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Puntaje/puntaje'); ?>">Puntaje</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Selecciones/selecciones'); ?>">Selecciones</a></li>
                                            <li class="nav-item"><a href="<?php echo site_url('Tecnicos/tecnicos'); ?>">Tecnicos</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item">
                                        <a class="dd-menu collapsed" href="javascript:void(0)" data-bs-toggle="collapse"
                                            data-bs-target="#submenu-1-2" aria-controls="navbarSupportedContent"
                                            aria-expanded="false" aria-label="Toggle navigation">PARTIDOS</a>
                                        <ul class="sub-menu collapse" id="submenu-1-2">
                                            <li class="nav-item"><a href="shared-hosting.html">Partidos de Hoy</a></li>
                                            <li class="nav-item"><a href="dedicated-hosting.html">Alineaciones</a>
                                            </li>
                                            <li class="nav-item"><a href="vps-hosting.html">Convocados</a></li>
                                            <li class="nav-item"><a href="reseller-hosting.html">Mejores Goleadores del mundial</a>
                                        </ul>
                                    </li>
                                    <li class="nav-item">
                                        <a class="dd-menu collapsed" href="javascript:void(0)" data-bs-toggle="collapse"
                                            data-bs-target="#submenu-1-3" aria-controls="navbarSupportedContent"
                                            aria-expanded="false" aria-label="Toggle navigation">NOTICIAS</a>
                                        <ul class="sub-menu collapse" id="submenu-1-3">
                                            <li class="nav-item"><a href="blog-grid.html">Ptrocinadores del mundial</a>
                                            </li>
                                            <li class="nav-item"><a href="blog-single.html">Comentarios de los Jugadores</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item">
                                        <a href="contact.html" aria-label="Toggle navigation">SOBRE NOSOTROS</a>
                                    </li>
                                </ul>
                            </div> 
                            <div class="button">
                                <a href="signup.html" class="btn">INICIAR SESIÓN</a>
                            </div>
                        </nav>
                   
                    </div>
                </div>
            </div> 
        </div> 
    </header>
    <!-- Terminado -->

    <!-- Empezado -->
    <section class="hero-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-8 offset-lg-2 col-md-12 col-12">
                    <div class="hero-content">
                        <h4 class="wow fadeInUp" data-wow-delay=".2s">50% de descuento por Black Friday en nuestros serivicios
                        </h4>
                        <h1 class="wow fadeInUp" data-wow-delay=".4s">
							BIENVENIDO A QATAR - 2022 
                        </h1>
                        <p class="wow fadeInUp" data-wow-delay=".6s">explora e informate los últimos acontecimientos en el mundial más esperado por los fans.</p>
                        <h5 class="wow fadeInUp" data-wow-delay=".8s">Empieza a vivirlo solo por S/. 15/Mes</h5>
                        <div class="button wow zoomIn" data-wow-delay="1s">
                            <a href="#pricing"><i class="lni lni-arrow-down-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Termiando -->

    <!-- Inicio -->
    <section class="features section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h3 class="wow zoomIn" data-wow-delay=".2s">Nuestros Principios</h3>
                        <h2 class="wow fadeInUp" data-wow-delay=".4s">Estamos comprometidos con el mundial y queremos que tu tambien lo estes</h2>
                        <p class="wow fadeInUp" data-wow-delay=".6s">Explora y aprende más sobre el mundial pero sobre nuestras reglas tambiien .</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="single-feature wow fadeInUp" data-wow-delay=".2s">
                        <div class="list-icon">
                            <i class="lni lni-bolt"></i>
                        </div>
                        <h3>INFORMACIÓN RÁPIDA Y SEGURA</h3>
                        <p>Ofrecemos los últimos acontecimiento de el mundial Qatar 2022, declarados y aprobados por CNN y por los patrocinadores del Mundial.
                        </p>
                    </div>
                    <div class="single-feature wow fadeInUp" data-wow-delay=".2s">
                        <div class="list-icon">
                            <i class="lni lni-lock"></i>
                        </div>
                        <h3>100% SEGURIDAD EN TU DATOS PERSONALES</h3>
                        <p>Tus datos estás seguros con nosotros, nuestra politica protegue tus datos.
                        </p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="single-feature wow fadeInUp" data-wow-delay=".4s">
                        <div class="list-icon">
                            <i class="lni lni-dashboard"></i>
                        </div>
                        <h3>TE DAMOS GARANTIA</h3>
                        <p>Si no estás satisfecho con tu subscripción te devolvemos tu dinero.
                        </p>
                    </div>
                    <div class="single-feature wow fadeInUp" data-wow-delay=".4s">
                        <div class="list-icon">
                            <i class="lni lni-headphone"></i>
                        </div>
                        <h3>24/7 SOPORTE DE LA PAGINA</h3>
                        <p>hacemos que vivas el mundial desde tu casa de la mejor manera.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 align-center">
                    <div class="explore-all button">
                        <a href="javascript:void(0)" class="btn">Explora Más</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Empeza -->

    <!-- Inica -->
    <div class="pricing-style2 section bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h3 class="wow zoomIn" data-wow-delay=".2s">Plan</h3>
                        <h2 class="wow fadeInUp" data-wow-delay=".4s">Elije el plan ideal para ti</h2>
                        <p class="wow fadeInUp" data-wow-delay=".6s">Explora más sobre nuestros planes.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="single-pricing">
                        <div class="p-icon">
                            <i class="lni lni-folder"></i>
                        </div>
                        <h3 class="title">Básico</h3>
                        <p class="des">Funciones normales y expectitativas del mundial en tus manos.</p>
                        <span class="start-at">Empiezas con:</span>
                        <h3 class="price">
                            <span class="sent">S/. 15</span><span class="duration">/mes</span>
                        </h3>
                        <div class="button">
                            <a class="btn" href="javascript:void(0)">Explorar o Comprar</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="single-pricing">
                        <div class="p-icon">
                            <i class="lni lni-handshake"></i>
                        </div>
                        <h3 class="title">Pro</h3>
                        <p class="des">Podrás tener mayor capacidad y mejorar tu experiencia.</p>
                        <span class="start-at">Empiezas con:</span>
                        <h3 class="price">
                            <span class="sent">S/. 20</span><span class="duration">/mes</span>
                        </h3>
                        <div class="button">
                            <a class="btn" href="javascript:void(0)">Explorar o Comprar</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="single-pricing">
                        <div class="p-icon">
                            <i class="lni lni-layers"></i>
                        </div>
                        <h3 class="title">Pro Max</h3>
                        <p class="des">Vive el mundial con las expectivas en alto junto a tus amigos.</p>
                        <span class="start-at">Empiezas con:</span>
                        <h3 class="price">
                            <span class="sent">S/. 30</span><span class="duration">/mes</span>
                        </h3>
                        <div class="button">
                            <a class="btn" href="javascript:void(0)">Explorar o Comprar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Terminado-->

    <!-- Iniciado -->
    <section class="call-action">
        <div class="container">
            <div class="inner-content">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-7 col-12">
                        <div class="text">
                            <h2>Tu estás usando la versión GRATIS
                                <br> <span>Solo puedes ver más no experimentar.</span>
                            </h2>
                            <p style="display: block;margin-top: 10px;">Empieza una subscripción y vive de la mejor manera el mundial. </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-5 col-12">
                        <div class="button">
                            <a href="#" class="btn">Comprar una Versión
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Terminado -->

    <!-- Iniciado -->
    <footer class="footer section">
        <div class="footer-top">
            <div class="container">
                <div class="inner-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="single-footer f-about">
                                <div class="logo">
                                    <a href="index.html">
                                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVFBcUFRUYGBcaGxodGxobHCEbGxsbGhsYGh0gICQbISwkHR0pIBsbJjYmKS4wMzMzGyI5PjkxPSwyMzIBCwsLEA4QHhISHj4pJCkyMjIyMjQyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMv/AABEIAKgBLAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAEAQIDBQYABwj/xABCEAACAQIEAwUEBwYGAgIDAAABAhEAAwQSITEFQVETImFxgQYykaFCUrHB0eHwFCNicoKSBxUzU6Lx0uJjwhY0sv/EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EACkRAAICAgICAQQCAgMAAAAAAAABAhESIQNBMVETBCJhcTKhgZEUscH/2gAMAwEAAhEDEQA/APMSkU2Ktb9kaA0NfwwG1RGdnVPjaBAKkUNypu1TWh0qmZpi27bE61OLHganw1ljttV7ZwYySfjXNycmLo6+PjyVlAmGU71P/lyESCas3srlmNY0HlSYDBFzA0qM35sv41dUVh4dFN7ONK1XZlCUNsMPHUz1B6VXXcCSdBrVR5L8ky468FWlqNTRlnChiAoNGpw0mQR8KJ4XgmBlgRFRPk0XCFMMwnCgFkgTQmLwzM0DRRz2FXTPAyLqSN+QFB/5Y894+OhrKL7Zc96RFhrIRWmNdAd4qbCQWkrI5fnRmHwaqVJOnQ7afbVlZS0xOqxHIUZCaKm9w8tqoGs6D86Hbh4+rBrRY60gQuWCxuxMQOpPKs0/HLKGFJeOYELp4mqybM/tiGpgmgSJHKrHheGK3Q5WBzjx0qms8fY6ra08GB++rzA+0FokC4rIepGny+6spS6KyibezaAGlOxWDt3UKXEV0O6sAR8+fjQWExRZQyMrL1Bkf9+FF9oxrTjaXRxTi7PMvab/AAz7xfBMfG0508kc/Y0+dYG9bu4e4bd1GtuN1YQY6jqPEaV9JIRVTxzhFrFLkuojryzDvA9VYaqfKtraVsywt60ePcF9oblqAIa3IJRvdMdI1U7bGNBpW84XxWzfAZS+dQsqcvaKqCe79B1nmZOp5xGd45/hvetS+FbtF+odHHkdn+R86ytt3ttlcFWU6ggqykfMEfGk0pbKUnHTPSr5QzIiYBObuN9JpkZlJk6KI21Owis4a0e6R4lCF0JiSvUAMdWP4VPwDFLcWHXtDEF/pnmQRp2gMAamY60b/l6kwMpWTpyB55dJRsxiCp20NYSTOiPIiMkMFg517veEBlMrlUlYjqQokePOO5g7bEKzchDqCJBy7iTqzTq3d0nrNl2ES06jdgO+BOuYbx3QoMrzihmwcNIyqW88j+4CV0ieQEMZ671nTRSmn2DNwjYsVB1y3AREw3OBsOS6Sd6JGCX/AE7gggyDGnvFwBOYKYQM2bXXbUVLbDKpUAkQAULe6O7pJ3VROkgE0ObRKyQXtwdCO8g0B215sSQJHWkK2/LOtYa3myMYeO6wbVjlgssEMT70CAoAJ2AonslbuPAfdSJg5cpEEAaKSoypqSOk0GQBpcgoZhp2MRrmkBve1kwBECnPeM5GLAnZwCvLTUEQR3oBgaTFNV2DT9hVnBrJVxLCTJAgwTrLZgDEZiZPeIjWpDLA2rk5oBzaS0gjMASe9ox7wAUQY5UiWswyOAG+gwg8swI2IgjaBOSZiqzjfGUQdmcpuz3CIKjUMGltttdy0HWK3g0lZk7bC72LVAUusAd1MjvKNVIjKYWIEAL3CxOtUV7juIUhbeXKBANx2RiORgFZkRqRqZ1Ig1T3eLhyTcJa5mBBABGmxXMd9Nz6FRUv/wCWuui21UeZk+LEMJY8yZPjTzQ8WYXE4VwJOlBOCN694497G2LiFkUhgugGxMk69N68q43wG5aMFY+fzrrjLpkv7toy0UbgrbMYUT4UxsM2bWils5WhSR1pyZUI7st8E6KgJGvMVMvEBDDL5DrXcNwPjNS4ezbRjm9K5aTb7OzJpLorHxhnUEb0dw7iCAqIO2tNxzoW7qiIiiMNdtJlIUk860lxtx0jCPKlJ2y1waMHzuwM8vCn3ipZgomenKhf82X6NoepmkPEmJkAL5VEeKV7RU/qIeEyyGKTIyyQ4EbaiPOq8Y25sH08hrTMViu0gxrGp01qJFraPGkto5p80pPTLIYm4BoVPWAKT9odjJMzQ1sUSi0YJdC+ST7C8NhWfTXancQsph7ZuXW20AG7NyUDqaZauMNiR5VmOP4xrt7ISSqBguv0pGc+ew9DWUo1s1jO1QFjOJXL7A3GhV91OSxPxPjRuBsc9+Wmuvnt+FV+Qgb/AI8/yo+3dIG+o0gSJ0G/h4c+e0HGTsYattS0IveDHM0lZEQVYjU8tRJBFTFVBGUE6sMoYuHUAgk9odAG0neRAmqtMTf94oCOitBgchO3PbrVlgLbvbVl7RlZm7pmVjRxAEFvdnLoRBMTpGLYPRZ8NxdzDsGQwDupMq3WdBOswRqK9DwGLF1A66TuOh6V5wcO8BgM6gmQCSRlAM6DXZtfGtL7K4nK2WZVtvs+2iLcZb8Ce0avKaeqVLFIFrrxMchVFVXHPZ3D4tYuoM3J17rr5HmPAyKtxS1aiQ2eZYr2VxuEJNhhdt8iNHUeK7HzE+lVWC4hdt3C2Zlee8Dz/mDaNvzr2SqrifArN85nSH+suh9evrUPiraHGXsocDxlHAD/ALthoNTl+qMrboY5GVJNWmQdBBgnSVPe0kbTpo0QdT0rP8X9lbqCbXfA5DRvhVTgONXcOwtuCVB9xpBHkdx5beFRS8SRT/BtIBgQdB3Ru6CCJUz3100g+JnaiCB7wgTHeHusBsGB5947jfWeVA4HGW7igry7xU6QZ3ge6f4l06wNKMzkbk+JiSQNDmGzrB94a9elJQFZXYq2cxHMjW22oaTJy+HvdQZkgChbmS2pO6AmV+kvWNdRsYEe9O2lE47EQpCoXbfKpkD+K22/Q5fPyqiTilsS7h2edCAB/S2ug5TvsfLmmknR0QtoNZrlxQM37jQ5z7wA5k/MnRt5G1SNw9ez7EAEMYF1xOp+rtL+Ok85nLRuC7yhu7JglFjIRvI+sw11POdvep967bRYYzbPdKH3l6QN2A6DVdCOZO0Y6tmcnukU1j2Ww4/d3FYv9Fix70RMZYEg8txIME6CG5wPBAxcY2n5qLigH+IdoZg/LbcGre9dYgAnKumV5BboJOwHLnvGm1MzRoyEnmQMwPjJM/GT406j6HcvZXcM9pmWQxzbaHaBp9lXWOexibTSVRogLpJO4g+hry+3cHLTwOo/H7ae+MYaTH2GuvD0ZZdsmu4a2jspXblzOo/GqtmUk92NdBUjgmSTJ8fx3FQXcZr3xMaQd/Q86a40tsp87ekHYbFi2NN/yoGZpUAbVdR0O9Koq4QjHaMuTllKk+hVWpEWuQVMi1ZByLUqLXKtTIlS2CORKnRK5EqdEqGykORKnRKREohEqGyxD3VLdAT8BNZHB2WZwdzlJJ8XMfOD6itrcs5kZeqsPiCKpOEoDatqgBa4EBbMAVYqgffYTOmsZhGu+HI9G3GU2IVRAJ3MDxOp9dJ+Bp2FTQAmfs9K6/dGXtEOZC5VWgHUkgaHckD1mm2MQoyzz8vx2/EVztOjVNF9gsODR911tslpTIRWUiAQzu+e4ZMiFOQAwdUB5UDhsQoGu33UBw3HBU7Md7K5WZPup7vXXKY8fUyuLyw5PBr8NZt2radq0G4GCREADWRvGhMjodANqm4aQtw5PdlQhn3gQDPwIHXasDiMS93FWUDEqkAg6BczuWIA5QwPWCRyrdcIgEJmzZXfIcsd1znAgaCFZRyA202q+UzSN2pkDypRSJsPKlrrXgwOBpRXCuqkAtOrq6qJOqv4hwm1eH7xATybZh61YUxmqZVWxo8z9qOH3cIwZCxt8nEgqehI2PjUXD/azNC3TpOrrv6gbea6+Br01kDAhgGB0IIkH051iOP/AOH6PNzDP2b75G9xvIjVPmKxx1rwVkWOGdHSbZBRuY1Jjy2PIkaidRTWwNrLBtr8ATHj9ceI19a83/aMTgbmV1a0x6iUcD/i48Rt4VqeFe1Fq5pdIR+pPdPltB8zPidqmo14Gm/YXiOEnMDZm2pH1zqZ3Q78tiRtyiguwvYdu2JV+RedfJp1B/WlX63M22x6iZ8wB89D1BqJ3UEEmTyJGYkdNPfH8uorJxXlGqk/DKTEe0BEhLYBIMqxlT1gQNKsrIVlBF1XBG5J08IHugdOVQ4vFI65Sq5eU6x/Idh5Gqh8FZJkkk9Zy/KKW/2Vr9GWVydI1/XKnvjFXS4N+cSPxqtvYovoqHMOYk/CBOtF2OGscrXHQ+DMQfsn5V6ZyEOZmY9nOXx2H4VJbwwH+oDPXcfL76J7FVOgH9Lfon4U9ddj8fxH4UCB/wBnjVSRHKfsjan2xGjfr7qe4Ych57fkagfFMvIEc5qhbLHC4VnmNfkY9aI/Y2GkVL7L+0S2c6lFKMBIaTseU7VucJisJiSSSEJIgDYispTaZaijBiwRyqUW63mO9mu7KQdzG3Sstj+H3Lf0Pj4VLn7GoX4AUSjLFtTuYqou8QyGCAD03rrXFtdFY+kCplIuPHsvxZHIg1Mlug8DiHaO4w9AaunAEcp35f8AVZfJ0aPirZFbSsNxVXw9x0Uaq3aW/wCWSwjlKywmNwNta9GsoDrVP7X8Kt3LQuFgjp7jHnP0NN55eNJyTEk0ZS9aS4jW7a5R2CFYOUSqsWPjBGbr9lZsYiFGbcSCmndIkEabQZ+PPatBhcUbdy1c0KoyhlP0gShjyOo9azftIiWsZetKe4H7p6qQCjeqlSTzMnnT443aHKVBS4wxlfvpzGoOvQiPn1qBbnYXGyMYABB9QwkA7wW2+VILFzLtpzMHYSfXagsOxvXFVQACZPIATVRitvoHLrs1/sth7t1rl5GVbzGUnQAlgq+Zll+Fb72PwUvccABc7gfyqzKPHYD4VmuE20tFXGUsqultfp9pkOZ9dAAGAnqhr0vgS2+yBQAb5gNYbdp9T8651986/wAhJ4osjSUppK6mzFCilFIKUU0xCinUyaRWmrUl4FRJTGWaU1C93L7x061M5JefAJN+BlxmX3RmH2fjTUxKkSWHiNoPSKmVswkbGg8ZhVOuobcMDseVc87SuL16f/hpGnpjsdg7d5Cly2roeTCfUdD471517Qf4flJfBvm/+Jzr/S+3o3xrbtibisM5GTqv386lTK4zTCyfDb7Kx+W9Iv468nkGG41iMOTZuZhG6OIZf5SRIHTcdK0FvjVu4oVM73GjuxLEzz3nz18xWo9r+FWrmEus6KTbtuyM3vAqpYZTvy251gPZO/btvcDgnuKVghYKv1IPWtLuNsS1KkX68Mv3N4tTEDR7hE5ToDAjxJjTajLXstZjvmWkyWdpMEidBHLlpVVd4sVzZWMnNJnXRpGp1qvbHsSTpv1rD5fSNvj9srrt84G52d/Dtbb6yXDlboQD7w9TXXOLWrvu3GBPJ+fxigsH7e4pVyXcl+3zW6gefXQ/GaHx3EcJfaVw62NNRbMqT1g6DyFer+0cmumH/sxOq6/y/h+FPtWW845GVb9etQ8HtM027LI8wYzC24I5gvI9OdWl3h2OCOWslmtjN9W5l6oUzJc8hB8KLXiwxZT4nFBQTMjms6j0/KosPhWuSxzKu4UqxH4AVDgMVZd899tdtULKfPs2HjV7duJcEW3VgPqk6D4mqdoSBraWzvHwgfr1qxw1zIQbZynz/R+FCJgXOp26nUfEbVDxB+y7pAkx3DMN4g7VD2NaNphfbC4hW0RJEeOk79flV/iOO2ntkMoLZTryn0rz3g+Gt2lDXGYO28MrCOW06VoLFy2w0dW8CYPz/Csmi0zLYzBOWZxHOSJ2+B+6p+CY+2CQTtzykj4jlVlx3iBtJlQlXfRQVDAg6EhoJBE9fSi+CcPtJbVGRCw3dNyeuon51Ela2aRm4vQdhbwcAqy+m/20fZtTpvVRxDC2FWczDzUyPGRP21H7GY452d7oZMxAQtGg5wZgHpUYUNzbNTZwsbCPlVV7Y4MthHYDVGR/gYP/ABYn0rVjH2shaRA3jWKTGYVL1m5bOgdGXyzqRPzow9MjN9ninvZlGhKBgehU/carvaeyty3ZxQSCALdxZ2ZNUMeKgrH8HKRU1+4UvJbYZWC3VOuqsuWR5gqabh7AdHVx7+VgDOUPLTEarBLa67CRrTi8NlyV6JLKn/Lr7nNoqMhzQFZ2RCpEd8MJOsRkH1qD9lsCxfNbQu7tkRYjM2pAk6DxO2hpb3BibbqTNyE30XSCxBMQIH0oETOsVcLZfDYYXFUQmbI3dZWuAZQNTIKZhc2IIU6jm7TVLt/0TW7J/ZpLd3GdmrsMqQhP0zobmcD/AHEFxoGxdd4r1T2WWLW531B5bGPEa/8AdeN+zKC1cS8SSUdCY1PdMtHUkV7RwIZUzOe+5zka6Tyg6gCaytfIqHNNRLY11AY3iAt5ZU95gs8gTtoJO+m3Om44XSkoRI1y7BgNxO+vpWkproyUX2H9oJidf18KcTI0/KgbFy3cQFSGU9Ov3Hz1qLDYplbs3IB0yRqWXXfxAGugo+SvPY8fQZZxInIx745dR1HhFTOJ5xQ1+1OoIVhs0SabZxM90ghhA1+kfDqKWfTDHtEqX9SGEQOfPyp73BHWhbtnPow15dR8KjGIFskOZPUbActBsazfI0qfj2VhfjyFKhAGXb6tP0I1+FVd3jiL7vz/AC0qkx/GnbwFZS5Yx/jv/o0jwyl50X93HW03afAbfHaqDG8YTMCgywZgGRPXXSqfE3idyaFbrXPKcpfg6I8UY7JuMY97lu4CSZRx13UjnWO4W37zzRvtU/dWqzgqwnUg/ZWQ4bei4h6hh8VJ+6ungX2STMuXUlRdXdDHn9lQFz1Pz/GmvdJ+X2RTRbboaSSQ22U+L4OgYgKRG4IyMD0Ikifh5UA/CmiRMeK16jhsbwm8AFxSxsFuOVYfytcAuDyDR4Ut/wBkm1bC3VdCNQxGvhKgo48wK9Vc671+ziwfR5G2HcGN/LX5b/Kj8D7QYmxpbvOo+qSY/tbT5Vssb7Juffw5U/WtgXF8yqksP6T/AE1Q4/2da33nRinWWKjznVD4MBVpwl4YqlEa/tNbuf8A7WEs3TzcL2dw/wBSfhTVbAt3ka7abkCFuL8SQ3rNQXrFjvRYdFkwUusSo5TnVgx9B6VXvg0Mm3cMDcXFC/8AJSQfWKWHrQZPvZscGzXLTvbxA7cEZEV1y3F551uSVbyMHw3qst4i/ZudpdsFo299FXyKyPnWYa0wE6EeBqXDcRuW/cuOvkxH30sJIeUTbWeLW8Q3uup10YhpjXQasTUi3LIkC6snkZtt8HIB8qyeF49dRxcBBcEEMwBYEbEHekvcRW4zPcSWYliQdyxkmNRvUUO0ajg2Fe5cN0EhUkJGYDoSBBHhuKtbuLvgwRI8R+IFYO1dtgyjPbPUb/FSKvRx8rbRbV582vaFzIJnuhcwaFjqd6lopFrxXHOtvUEFhCkNGp+IIojBOtq2ikttp7v/ANfxrOXOLXnKszW3KmRssnlOUiryxxsuhe5ZQlYEB2zMTMZRJJAjU8qhoYXxLGjswqsO+VG5TmPQ+taTDcUcKFzgnT6Un4nesPieNW2a3Nm4qo0mDJMAx9U79atBj8MQLxNxFzRqukgBiO428GocSrKT2lss+NW7BA7SGMR3nRfSSAT46mqvg99WtlYkqwdT1kEePNQfH0EXuIxtg28QQ4LFlZFIImCuvujxPLaqXg+CNu27iSunejQGNAes5W0qZfxplR8lxgbQZR3Q0lQBO5ZgFAnQzOk6TFTe1RlcHYuBVWyrBxmHvoLYJEA6ZXWNCQBrMUHwXHKjoxgqt3DuQdVKM4tsfSQem1a/iFqx+0t27ardvi3mWVBY2rup2BDZAJj3qiP2psJbkiv9geDL2ga4AwWWCyDkaYAb+LnHKR109A4kwQLcAHcOsn6J0PwmfSq7htqbb3AVzZsxYCMzBsx0M76epOpqwxN1YlriBCIMsBuPMVMLad9ik9i4lu0QrJAYbjfwIzUJw7GkqUuHvpo2YgkxHegbAyKqcPi7OQq98SjEBsxIYDUHSZ0jn4Ux+L2BcFy2GcRlfu5Q0aqe8RJ3GvWhvspR6Le3iVt3NDNtz4KqOdT4wY+PnRWIts/dHcI1RhEg/AkCs/e9pkIKqgAP1mkz4BdBHnQj+1F0nKGgbaDXz6/OocktFYN7NXaugL+8IVl0IJPxndid9OtB47jNse5q42YACsrjOIm5uxPjyoftJ0XU/L8h41GcmtItcau2aO9xi44MNlH6/XOqm9iW5mq8X2BhTPiNR86Rsx1J+NZSW/uZrGl4QWt4kx19TTb1wAx061EmKVFIDQx3YHvR0HQUIcXbGwJ/XjRrodk1y6SdyfKnMGI2jzoC7xI8gBQlzHufpfDSjbFaLa2ApJaD06DxrF8PYK9udgYPwIqzuXidzVKh7w/m/wDtXX9PF4tM5uZq0y/ONUbDp4bVF/mB6D50G2n68aZmqfjHmVNzgzCQVk/zZfkymfiKFFm9ZabfaoeqEqfihrXqlwDu5bqj6sOOZ23Gi9BQrPr7oHhr99ezSZwAuE9p+II627eMa5pP7yI2kgm+J0+fKatsL/iTjgcr27N0bGBlkc9VbL8qBZEacw+U06xg8PJ7Swt0HoSjiOalT9oI8Kzlwxflf0Wptdh49t8Jc/1uGgGYJtsJn0CmfWpsPxzhYbMpxOHYiCCpYEHcMO9mHgapcbwbCqQ1m4+/utmR0PmJVvMEeQoLE4W44h71xwNsz5v/AOqS4fTa/wAhm+y/xnD+H3Gz4fG20B+hcEQfAnKQPAg1XY3hDW4i5h7oOwR8x89tKz9/h2UAyT10mOmxpP8AL9CUOaNSMrKfnv6U8ZLv/aHlF9GqxHDmOEtBbILl2ZmUljljQRlEDbmdqr8Bwe5cupb7PvEjRpQQN5J2Ec6phduCMrOkDk7D76suHYLE3xce3my21zXLjuMijxZhuenhSakvIXFj+I4S1auNbt3BcVTGfLlkjeNTpPOosLhHuOEtqzMdgPz5eNDDF3PrD4CjuGX8S7OlkTKnPl7oyc85kAL5mk7S8FaGXLQXQEkj3jynoI3HjXIuk5pjcDQgCNdfOoziiPoqfKalw19mm2lrMWMwoLOcsnQxMb/Cpf6DXscl5uTH4mizduBVYsYaY1n3YBoBcQpMZI9fyom9cGVAZgA5QGB0LGeU71m0vRSf5JTfJENEHwEmKvFxwXCW8MgGVc7Pp7zuDlP9Ihf6RWfw2KVGDgNoecQfA+lWGEfKpBHvCIPTQ/dWM0ax2M7MKA4giNQOjGSOh70MPM8q9I9ocMobtEXtFdc/Qd9MjvPIDJZMeJ8a81/ZnFt3SQERGOpAIQuhEct10rdcE4nOECHN3UvBCYOZGRmCqdJZWCjLv6bZT/iFbshfiKhFXIIA0knmSSdI3JNVeL4mWgQsDQCDA+NRY3G2yE0YQgGka7676flVZcvKVJA0HVlBPpufSs4xLYU2ObYHygAfZSdsTqSfU0Eb4jZf7p6dPP5GlOKbkR5gfjVOP4BMs0J6U9HA95gPCaqWuMd2NNBpYSHki6/arY5k+QgUrcRABCr8TVWrDrJppuUvjb8srMsH4g55x5UwXmI1JNN/ZQi5rjZPqpu7eY+iPEmg2xJ5aDwpfEhZhV4R4eFQ9pQ7Xaja941a4kLMJL1GzVAblXvAOCG8e0uaWh13by8PGrUCXIrsNgbt33LbkfWCkj47VR3e67RydoPkxrZe0ntEMpw9ghbYEMy6A+A/h8edYi4fe863440Y8j0Gs9Nz1CXpM1GI2zVD2RxgOloyOj2//OpxwTH7NYNwdHNpuYO/aZuQ51CnH740F66B0Brj7QYj/fu/3Gk/q5v1/owuJIPZm8d8NdtnqrW3XkNi8/WO/MUNiPZq+hAKbidSAQBvInSnn2gv/wC/d/uNBXseXUozMytuCZBPjO9NfVS7BuPoI/yXFR3bZeNwBnimtwTEc8Ld9FYfcRQ2ExXZEm0TbJ3Kd2Y2mKKPHb3+9d/vNN/Vy6Qk49kFzgV3/aur/Nbb7QDQHEOH3bS5jYaACWJbKRAkmCNqs24xeOhu3T/WaHN9iSe8SRE7kjQx5bUL6uXaBuPQDiOHHs7dxSozxKlhoTJ3Mcq4YFUX95fADa5LYNwnziEnzajLkkd5CR4iR9lIE0gWz/b+VP8A5T9AmgLtLKe5YzH611if+CZR8SaHvYi4y5JIQmci91J6wNDVx2Z52z/b+VOWyf8AaP8Aafwo/wCS/Q7RmWtNvBrSXbQweHCgJcxN5ZZ0cOLFs7ppIFxtieQmKeLR52vl6Uq2iBpbA9AP1zpP6lvoaZm7TOCIEEajTpr5VdcfwVuybS2iGBs22uFWDDtDmzeXLSi8n/xr8v1zpypof3a/FR8al/UP0OwD2fW29xrNwALdUoHY5Rbub23J6SIP81TPZZchJG63NDIZTodt9M3wNSYhYRu4mx5qdxpoKnxOIythWtgns7dvMD9I953H8p7RhHSKmXJn1Rtx+GG8FxNthew76ZrT78xKmB1JAOn20Z7HYleyuWbhAMBkU7500YDkCQInz8apvZS7ZGJi8oIzSCw0ytvEjdScw/lFX/E+D9jiv3S57eYNAO9t1lmQj3gJbbUFZ2Omc19pSezMYhcpZQScrETETHrQDPrNa/E5QWUi2zCe8dZ1MGQNjVViGU/Qtf8AL8KFyP0S5FJJqVGJ0EUQ8dbfpm/CooWfeT4NT+R+hZDi4Gglj1ggenM1GXPM+lLnH1h/afxpmZfrf8fzoyfoeQVbUxJqNzJ0Pp/3UfbD6x/t/wDamG4vU/2/+1Df4DImYGuAP6/7qHtR9f8A4/nSFx9cfBqVseaJGQ+P69aYyge8Y8OdNz/xr/y/Cmn+ZPn94qk2JyQVgLtkOGu5io1ygb+eu1XPFfaVLidnbUouxM5SR00BgVmyPFPiPvppU/w/3LTtk5EhtdBP9U/dQN3dh5fYKKCNyA9CtB4iQxnQ6frStONuyZyTROF0Gg5czXZPAfP8aagaBCsdBsJ+6lyv9Vvh+VFsFJGgN7eEQxyyLJGn1hvqP7hTmuEHQJEBp7NBoSOqkCJG9CPcGUAnMBMkHcxmy9N8o0+t1ipWWcxB159SJ7voTAnXWB1rKkYjhfaJhQIOoRY5fw+PWKcbz6gbhfqgeH4n4daZccKwU6CZDbETtpOkgTufeNQYhgI0IzCTCxsGbXOein0mnQBLYi5zY6zyG7cvh9lNW85nvvz68h18p+NNUyQ0RM9GIOXQDpoAZ12PoOjhu6VMR0Jg5lkCNpJ0J5gDzKAJTE3Nf3j7a94+HQ+B/LWkbF3Ni77CJLTuOp20+ZqF9QufQkiATGb/AMRmJHp8Va2D3dCsDVRoDMw3MaTttJ6U6QHNcYmC7HoCT6bnmdPvpC5iZJ20G/j99DK8L3m3E6ypEggwJOkHXlJXoI4OOU90gQAe62uozR3oBJ5b08RWEvPMxznlv41GHMa7kmPHxpjPppvsQTBENzleYkmPvmms7BgTqZmJXu6wZ2MTpTxQ7JlYDx0n7uXnXZxMaA+fidNvA61Amu7Eg6DWJ+lHTloeg6Vy3Z7zAgZjrGhU5dI5biBoN55UYILJReEkHoT4xHz6U8XROWYMxE6yf186GcgATJgxO5OpPmPdn50ouL3SFjSdZBA8fnpRgikywwaNcdbaRmaV/uDCDHgTQ9vFZQpKmBA08NANdOW9P4Zj2w57cPbtkAqhYFmOcFcyhVMNAfVtBPlTsFjltqiXbgS26l/9PtGZXXKDGnIGJI3mKiUfRtCcUtskucPe4+W2jvcMtbKazCs8EfR90wfIbit57P32xWDVgoNyye6s6lZ1Qc10Aynl3NdKw93iNhStvtGAEiVXs2brmgxGsRMRUycYwphLd5lJAQZg0DUmO6PrHeoqWNUVnF7TR3tS3Z4hokIwVkPVCoHrDBh6VTXMQTrvWix2Jt4jBOAQb+Gl8wLMHQkdp/qd7lmPIFehNY53EH56+Jq4RtGc3vQR2k01rlCG511/IeHnTGvfZV4EZBjXKb2lB9pXdpRgGQZnppeh81OCsZMEgbkA6TtPSjAeRKXpO0qZeE4gjN2NyNNcpgSQBM+7JI36ijE9msRk7RkFtZgdowUmNSQDuB1oxQZFb2lIblWd7g1u3lFzFWZJIYW27TJBESRodDr0g7017WBUD99cdo1yp3SY5Ewd/DY70YoLK03KTOOtWR4jg1VcmFZ21zNcuGNzsEgdPnUR4/lXLbw9lP4suZjHKWOx5g6eFVi/QWCIjNooLHoBJ+VRYhGQwylTGxBB+dHXvafFsI7UpuP3arbJmJkoATsPhVVfvs5LOxYncsSSfU1cItMluyyw2Aa4uYXLSiCe84B0nl1MaDnRFvAW472LRT0CXGHnIWs9J5Uv7Q3U/GqwFZsbbW90YBN2IEhScsxmEDSI0OvkBUcq8z3JGckptOp2AGXWeXIeFSvZykZ++vv5QCe4xAb4rm1EwVMUlwZsqG6kMe8rAZgqieZ18uWo8+RMQhXtA3fHM6EQCGiQx2U5gTJ+mBuTTbzm2EgkgiCxZXTLmgEAnVgI7s/fCJf72Zlm2x0kglQFz7e7EwQI1gdKkYAkBSYcsyZp74cjNPRgVB0FPwIHCsB3AAGBgkbnvBY7uqsGzaxsp2WpLVi3pKkNlBBDEsHEkkqNCDMHpGkxNQXLYVAMwD5mUkAoe6AsAcozRl0kDTxa+IVViUBWZyTmEE7azm0OhjUzyqv0AQ12QAQFXckxoInWZGuXaYMxG8txDMBMBWYsJmZEgZgG1AjKdYEtOoiR8NZWA4DAtK5equWg6kAMC+w3Ck86KtYe2GcsS4+jlQCAywRocxWNARtEeRpADsSqks7lG8pBDfwx9Ajbw16pZd8isACo0BYgKAq97aTzGu5kge9pE95gykWCBlGXMdrfdg9+dyG105da5LhOVHcSZa5J0VnzAe8CARmBjXUTymrAlxL5STlZg0wIIUE5D3sugygcxrr1Ndh2YsoiBmbMcpIyr3RuTmMlj0g7aRQuJ4gqkS2do5SM3STJJ0JGYHURtFQ28aCuuYxqJJXXuyBrtoN/mNKai6EGXbep0gFfpNMSGGmTcwIOmmXaRoxdBqYCmBpJbVs/eOhIy5Z18oFApxAqJBfXQCYA0jlG6mCfHwqM3Hczbtw0kmBMEyT8N/QdKrBhZYq8C3DGCylhEEhhMAjQHvRyGszvU2AQXbotLPaEhRAlv4iQJ0/MzzFC7sTJJg7Dx10EbCSaN4Li2s3syauJVQd+8OuwMaetNw0NM9gs+yfCzbi9bZ2SFdmuMIKoDMq4yqAR0ncg70NxD2Y4MLbXEWWiFi/cyltFUSX0HeXyFYC57VXbedDbV9WzktuWOuwgryHhFV17jtxmgyqmTGbbNvqRJ0j4VCjJLRdwL/j2DwS3CoV5UKDLsYZQqnYCBCk7tMjbaqmzZwxICWyWnSGZtfCedVmI4kzEmBrvrNJw7iXZsWCBpiATAGsn0PunwJpLilXn+ysoLwv6PQsBwtLJe5ctd11Crlc5kziCrKSARJy6ydPE1g+JYRrVy5bgwD3SRplOoP3EjmDVnd9sbjKVa2m0e8SfPz50FxDjZuqPdBYFXkSY1jX6upMeFHHCUXsJyi1obhOC3bjKO6ivOViSVhRM7bbDzIop/Zm8BLPaPeCDLcWMzgFNTGhmqZsXc075AGwB0GhEjpoKGJjxEfL8ta1pszs0uC4dhhlN7EL3TFxEkuO8R3SRkePA6gyDOlT4HFcNtsTcS7dGYEbABTErEiW/i08qyjafrlXLbJBI2ETz3pYe2GVGqve0OGtz+y4QDVsr3SLhClg2UrEGNQDMgddTUQ9tMQqlba2bYy5e5bEwNhLSdDqOkDoKzLGkn0qlBDyLW7x/EsMjXrhXTSYAyiBoOcGgHvsd2JiYkkxO+/p8KhLeNIW+NNRQrJc1IXqPNSA08Qslz0maos1caKFY/PXF6jBpadBY8mm5qSkINOgNut2YtK6pbYyYBMHKAdfpa8vGkxODlv3mfM0KGAmFQkMxAgjSI611dXn9jI7llmgIirI7MZB3nLd4ajYwRqehFEpYZLkFGBRC3vhSWnvEESSRsR4V1dQw6Kp7oZVi3mfNCk5iSywQOkcpqTDsTklQozMGRe65aT011zQDMa0ldWtKhE/ftWywt9xCM5Oj9/Mq6jeGQgHqabeS4bpd84hUGY93W5OWcomIJ+FdXUKKH2MxGGxbEFbcLnGpWEJkgb7roPhXcd9nb1myt13BZiS6qsBAunvc4JGnjS11UnQPwM4TwoXEBdgikNrIzTGsTygg0i8Ow6NLXCw+iJ1nWeXQCurqL2yUDXHw5tyWbPoDOuYEz8R1pV4hbthzb0JIGgjTYn1gn1rq6roAC9jVLo4QDLvE94jY+elNTiBVy6qJ3M+X50tdV4oQK95ixJ3Jn4kekUwAwdzr+vvrq6mvAxWQ6jmKQrGvL9RXV1NdAd4T8OlKTt8uWvSurqGAtwa6ncb7Tp/0PSm5jG/w5/r8a6upLwBxbr1/CnpfiV3HmR9h18qWupuKEyNj0PKm11dTGdSTXV1ACUprq6gDuVJXV1ADmWOYPiPA/o+tIxrq6gB0RSV1dQB//9k=" alt="#">
                                    </a>
                                </div>
                                <p>La mejor experiencia, en la mejor epoca del año.
                                </p>
                                <a class="call" href="tel:8884014678"><i class="lni lni-phone-set"></i>01 - 497 - 1991</a>
                                <div class="payments">
                                    <img src="assets/images/footer/cards.png" alt="#">
                                </div>
                                <ul class="social">
                                    <li><a href="javascript:void(0)"><i class="lni lni-facebook-filled"></i></a></li>
                                    <li><a href="javascript:void(0)"><i class="lni lni-instagram"></i></a></li>
                                    <li><a href="javascript:void(0)"><i class="lni lni-twitter-original"></i></a></li>
                                    <li><a href="javascript:void(0)"><i class="lni lni-linkedin-original"></i></a></li>
                                    <li><a href="javascript:void(0)"><i class="lni lni-youtube"></i></a></li>
                                </ul>
                                <p class="copyright-text">© 2023 Mundial Qatar.<br>
                                    Diseñado y Desarrollado por : Dave Ryan, Yeffer Sime y Aaron Mendez</p>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-12">
                            <div class="single-footer f-link">
                                <h3>Páginas</h3>
                                <ul>
                                    <li><a href="javascript:void(0)">Sobre Nosotros</a></li>
                                    <li><a href="javascript:void(0)">Servicios</a></li>
                                    <li><a href="javascript:void(0)">Versiones <span style="margin-left: 4px;"
                                                class="badge bg-success rounded text-white">Comprame</span></a></li>
                                    <li><a href="javascript:void(0)">Contactanos</a></li>
                                </ul>
                                <h4 class="mt-40 mb-20 text-white" style="font-size: 18px;">Tablas</h4>
                                <ul>
                                    <li><a href="javascript:void(0)">Tablas Generales</a></li>
                                    <li><a href="javascript:void(0)">Posciciones</a></li>
                                    <li><a href="javascript:void(0)">Goles</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-12">
                            <div class="single-footer f-link">
                                <h3>Security</h3>
                                <ul>
                                    <li><a href="javascript:void(0)">Privacidad y Politicas</a></li>
                                    <li><a href="javascript:void(0)">Terminos y Condiciones</a></li>
                                    <li><a href="javascript:void(0)">Declaraciones</a></li>
                                    <li><a href="javascript:void(0)">FACTORES</a></li>
                                </ul>
                                <h4 class="mt-40 mb-20 text-white" style="font-size: 18px;">SOPORTE</h4>
                                <ul>
                                    <li><a href="javascript:void(0)">Centro de soporte</a></li>
                                    <li><a href="javascript:void(0)">Actualizaciones </a></li>
                                    <li><a href="javascript:void(0)">Base de conocimientos</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="single-footer latest-news">
                                <h3>Últimas Noticias</h3>
                                <div class="single-head">
                                    <div class="single-news">
                                        <span class="date"><a href="javascript:void(0)">NOVEMBER 25, 2022</a></span>
                                        <h4 class="post-title"><a href="blog-single.html">Hoy Argentina se viste de negro, Messi no podrá jugar en el mundial</a>
                                        </h4>
                                    </div>
                                    <div class="single-news">
                                        <span class="date"><a href="javascript:void(0)">NOVEMBER 26, 2022</a></span>
                                        <h4 class="post-title"><a href="blog-single.html">Cristiano Ronaldo declara: Me siento decepcionado de mi equipo.</a>
                                        </h4>
                                    </div>
                                    <div class="single-news">
                                        <span class="date"><a href="javascript:void(0)">NOVEMBER 18, 2022</a></span>
                                        <h4 class="post-title"><a href="blog-single.html">Los mejores Goles del Mundial ya ¡ DISPONIBLES ES NUESTRO SITIO WEB !</a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--tERMINADO -->
        <!-- Iniciado -->
        <div class="footer-bottom-area">
            <div class="container">
                <div class="inner-content">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6 col-12">
                            <p class="text">Nuestras Ofertas Son Validas Solo Hasta el Fin del Black Friday. <a href="javascript:void(0)">Click Aquí</a>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--Terminado -->

    <!-- ========================= scroll ========================= -->
    <a href="#" class="scroll-top">
        <i class="lni lni-arrow-up-circle"></i>
    </a>

    <!-- ========================= JS ========================= -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/tiny-slider.js"></script>
    <script src="assets/js/glightbox.min.js"></script>
    <script src="assets/js/count-up.min.js"></script>
    <script src="assets/js/main.js"></script>
</body>

</html>