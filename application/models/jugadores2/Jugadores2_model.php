<?php

class Jugadores2_model extends CI_Model {

    protected $lab;

    public function __construct() {
        parent::__construct();
        $this->lab = $this->load->database('default', TRUE);
    }

    public function registrarnewArbitro($datos1)
    {
        $rpta = $this->lab->insert('arbitros', $datos1);
        return $rpta;
    }  

    public function EliminarArbitro($datos1,$where)
    {
        $rpta = $this->lab->update('arbitros', $datos1,$where);
        return $rpta;
    }  
    
    public function actualizandoArbitros($datos1,$where)
    {
        $rpta = $this->lab->update('arbitros', $datos1,$where);
        return $rpta;
    }  

    // public function eliminarAlumnos($datos1,$where)
    // {
    //     $rpta = $this->lab->update('jugadores', $datos1,$where);
    //     return $rpta;
    // }  

    public function buscarArbitrosall($arbitro_id,$desde,$hasta){
        $and  = '';

        if ($estado != "")
        {
            $and .= " AND estado = '{$estado}'"; 
        } 

        if ($desde != "")
        {
            $and .= " AND DATE_FORMAT(created_at,'%Y-%m-%d') >= '{$desde}'"; 
        } 

        if ($hasta != "")
        {
            $and .= " AND DATE_FORMAT(updated_at,'%Y-%m-%d') <= '{$hasta}'"; 
        } 

        $sql = " SELECT * from jugadores 
                where id_jugador > 0 $and";
        $query = $this->lab->query($sql);
        return $query->result_array();
    }
}
?>