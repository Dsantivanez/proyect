<?php

class Estadios_model extends CI_Model {

    protected $lab;

    public function __construct() {
        parent::__construct();
        $this->lab = $this->load->database('default', TRUE);
    }


    public function ObtenerEstadios(){
        $this->lab->select('*');
        $this->lab->from('estadios');
        return $this->lab->get()->result();
    } 

    public function registrarnewEstadio($datos1)
    {
        $rpta = $this->lab->insert('estadios', $datos1);
        return $rpta;
    }  

    public function eliminarEstadio($datos1,$where)
    {
        $rpta = $this->lab->update('estadios', $datos1,$where);
        return $rpta;
    }  
    
    public function actualizandoEstadios($datos1,$where)
    {
        $rpta = $this->lab->update('estadios', $datos1,$where);
        return $rpta;
    }  

    // public function eliminarAlumnos($datos1,$where)
    // {
    //     $rpta = $this->lab->update('jugadores', $datos1,$where);
    //     return $rpta;
    // }  

    public function buscarEstadiosall($id_estadio,$desde,$hasta,$estado){
        $and  = '';

        if ($id_estadio != "")
        {
            $and .= " AND id_estadio = '{$id_estadio}'"; 
        } 
        
        if ($estado != "")
        {
            $and .= " AND estado = '{$estado}'"; 
        } 

        if ($desde != "")
        {
            $and .= " AND DATE_FORMAT(created_at,'%Y-%m-%d') >= '{$desde}'"; 
        } 

        if ($hasta != "")
        {
            $and .= " AND DATE_FORMAT(updated_at,'%Y-%m-%d') <= '{$hasta}'"; 
        } 

        $sql = " SELECT * from estadios  
                where id_estadio > 0 $and";
        $query = $this->lab->query($sql);
        return $query->result_array();
    }
}
?>