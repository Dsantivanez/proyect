<?php

class Partidos_model extends CI_Model {

    protected $lab;

    public function __construct() {
        parent::__construct();
        $this->lab = $this->load->database('default', TRUE);
    }


    public function registrarnewFases($datos1)
    {
        $rpta = $this->lab->insert('partidos', $datos1);
        return $rpta;   
    }  

    public function eliminar_fase($datos1,$where)
    {
        $rpta = $this->lab->update('partidos', $datos1,$where);
        return $rpta;
    }  
    
    public function actualizandoFases($datos1,$where)
    {
        $rpta = $this->lab->update('partidos', $datos1,$where);
        return $rpta;
    }  

    // public function eliminarAlumnos($datos1,$where)
    // {
    //     $rpta = $this->lab->update('jugadores', $datos1,$where);
    //     return $rpta;
    // }  

    public function buscarGolesall($desde,$hasta,$estado){
        $and  = '';
        
        if ($estado != "")
        {
            $and .= " AND estado = '{$estado}'"; 
        } 

        if ($desde != "")
        {
            $and .= " AND DATE_FORMAT(created_at,'%Y-%m-%d') >= '{$desde}'"; 
        } 

        if ($hasta != "")
        {
            $and .= " AND DATE_FORMAT(updated_at,'%Y-%m-%d') <= '{$hasta}'"; 
        } 

        $sql = " SELECT * from partidos 
                where id_partido > 0 $and";
        $query = $this->lab->query($sql);
        return $query->result_array();
    }
}
?>