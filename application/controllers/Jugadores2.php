<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jugadores2 extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('jugadores2/Jugadores2_model');
        $this->load->library('Lib_utils');
    }
	public function jugadores2()
{
		 //echo 'hola';

         $data['arbitros'] = $this->Jugadores_model->ObtenerArbitros();
        $this->load->view('jugadores2/jugadores2',$data);
	}


    public function registrarnewArbitro()
    {
        $id_posicion           = $this->input->post('id_posicion');
        $id_seleccion               = $this->input->post('id_seleccion');
        $nombre_jugador               = $this->input->post('nombre_jugador');
        $apellidos_jugador               = $this->input->post('apellidos_jugador');
        $dorsal               = $this->input->post('dorsal');

        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("id_posicion" => $id_posicion,
                    "id_seleccion" => $id_seleccion,
                    "nombre_jugador" => $nombre_jugador,
                    "apellidos_jugador" => $apellidos_jugador,
                    "dorsal" => $dorsal,
                    "created_at" => $tiempo,
                    "updated_at" => $tiempo,
                    "estado" => 1,
                );

        $rpta = $this->Jugadores2_model->registrarnewArbitro($datos1);

        echo 'Se inserto correctamente el registro';

    }

    public function actualizandoArbitros()
    {
        $id_posicion           = $this->input->post('id_posicion');
        $id_seleccion               = $this->input->post('id_seleccion');
        $nombre_jugador               = $this->input->post('nombre_jugador');
        $apellidos_jugador               = $this->input->post('apellidos_jugador');
        $dorsal               = $this->input->post('dorsal');
        $id           = $this->input->post('id'); 
        $estado = $this->input->post('estado'); 
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("id_posicion" => $id_posicion,
        "id_seleccion" => $id_seleccion,
        "nombre_jugador" => $nombre_jugador,
        "apellidos_jugador" => $apellidos_jugador,
        "dorsal" => $dorsal,
        "created_at" => $tiempo,
        "updated_at" => $tiempo,
        "estado" =>$estado,
    );
        $where          = "id_jugador= $id";

        $rpta = $this->Jugadores2_model->actualizandoArbitros($datos1,$where);

        echo 'Se edito correctamente el registro';

    }

    public function eliminarArbitro()
    {
        $id           = $this->input->post('id');

        $datos1  = array(
                    "estado" => 0,
                );
        $where          = "id_jugador= $id";

        $rpta = $this->Jugadores2_model->EliminarArbitro($datos1,$where);

        echo 'Se eliminando correctamente el registro';

    }


    // public function buscarporID()
    // {
    //     $id   = $this->input->post('id');
    //     $rpta   = $this->Jugadores_model->buscarporID($id);
    

    //     $retorno = [];
    //     $retorno['TOTAL'] = count($rpta);
    //     $retorno['LISTA'] = $rpta;
    
    //     echo $this->lib_utils->vistaJson($retorno);
    // }
    
    public function buscarArbitrosall()
    {
        $desde   = $this->input->post('desde');
        $hasta   = $this->input->post('hasta');
        $estado   = $this->input->post('estado');
        $rpta  = $this->Jugadores2_model->buscarArbitrosall($desde,$hasta,$estado);
    

        $retorno = [];
        $retorno['TOTAL'] = count($rpta);
        $retorno['LISTA'] = $rpta;
    
        echo $this->lib_utils->vistaJson($retorno);
    }

}
