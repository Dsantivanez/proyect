<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tecnicos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('tecnicos/Tecnicos_model');
        $this->load->library('Lib_utils');
    }
	public function tecnicos()
{

        $this->load->view('tecnicos/tecnicos');
	}

    public function registrarnewFases()
    {
        $nombre_tecnico           = $this->input->post('nombre_tecnico');
        $img           = $this->input->post('img');
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("nombre_tecnico" => $nombre_tecnico,
        "imagen_tecnico" => $img,
                    "created_at" => $tiempo,
                    "updated_at" => $tiempo,
                    "estado" => 1,
                );

        $rpta = $this->Tecnicos_model->registrarnewFases($datos1);

        echo 'Se inserto correctamente el registro';

    }

    public function actualizandoFases()
    {
        $nombre_tecnico           = $this->input->post('nombre_tecnico');
        $img           = $this->input->post('img');
        $estado           = $this->input->post('estado');
        $id           = $this->input->post('id');
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("nombre_tecnico" => $nombre_tecnico,
        "imagen_tecnico" => $img,
                    "updated_at" => $tiempo,
                    "estado" => $estado,
                );
        $where          = "id_tecnico= $id";

        $rpta = $this->Tecnicos_model->actualizandoFases($datos1,$where);

        echo 'Se edito correctamente el registro';

    }

    public function eliminar_fase()
    {
        $id           = $this->input->post('id');

        $datos1  = array(
                    "estado" => 0,
                );
        $where          = "id_tecnico= $id";

        $rpta = $this->Tecnicos_model->eliminar_fase($datos1,$where);

        echo 'Se eliminando correctamente el registro';

    }


    // public function buscarporID()
    // {
    //     $id   = $this->input->post('id');
    //     $rpta   = $this->Jugadores_model->buscarporID($id);
    

    //     $retorno = [];
    //     $retorno['TOTAL'] = count($rpta);
    //     $retorno['LISTA'] = $rpta;
    
    //     echo $this->lib_utils->vistaJson($retorno);
    // }
    
    public function buscarPartidosall()
    {
        $desde   = $this->input->post('desde');
        $hasta   = $this->input->post('hasta');
        $estado   = $this->input->post('estado');
        $rpta  = $this->Tecnicos_model->buscarGolesall($desde,$hasta,$estado);
    

        $retorno = [];
        $retorno['TOTAL'] = count($rpta);
        $retorno['LISTA'] = $rpta;
    
        echo $this->lib_utils->vistaJson($retorno);
    }

}
