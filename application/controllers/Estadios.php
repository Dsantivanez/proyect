<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estadios extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('estadios/Estadios_model');
        $this->load->library('Lib_utils');
    }
	public function estadios()
{
		 //echo 'hola';

         $data['estadios'] = $this->Estadios_model->ObtenerEstadios();
        $this->load->view('estadios/estadios',$data);
	}


    public function registrarnewEstadio()
    {
        $nombres_arbitro           = $this->input->post('nombres_arbitro');
        $apellidos_arbitro               = $this->input->post('apellidos_arbitro');
        $ubicacion_estado = $this->input->post('ubicacion_estado'); 
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("nombre_estadio" => $nombres_arbitro,
                    "capacidad_estadio" => $apellidos_arbitro,
                    "created_at" => $tiempo,
                    "updated_at" => $tiempo,
                    "ubicacion_estadio" => $ubicacion_estado,
                    "estado" => 1,
                );

        $rpta = $this->Estadios_model->registrarnewEstadio($datos1);

        echo 'Se inserto correctamente el registro';

    }

    public function actualizandoEstadios()
    {
        $nombres_arbitro           = $this->input->post('nombres_arbitro');
        $id           = $this->input->post('id');
        $apellidos_arbitro               = $this->input->post('apellidos_arbitro');
        $ubicacion_estado = $this->input->post('ubicacion_estado'); 
        $estado = $this->input->post('estado'); 
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("nombre_estadio" => $nombres_arbitro,
                    "capacidad_estadio" => $apellidos_arbitro,
                    "updated_at" => $tiempo,
                    "ubicacion_estadio" => $ubicacion_estado,
                    "estado" => 1,
                );
        $where          = "id_estadio= $id";

        $rpta = $this->Estadios_model->actualizandoEstadios($datos1,$where);

        echo 'Se edito correctamente el registro';

    }

    public function eliminarEstadio()
    {
        $id           = $this->input->post('id');

        $datos1  = array(
                    "estado" => 0,
                );
        $where          = "id_estadio= $id";

        $rpta = $this->Estadios_model->eliminarEstadio($datos1,$where);

        echo 'Se eliminando correctamente el registro';

    }


    // public function buscarporID()
    // {
    //     $id   = $this->input->post('id');
    //     $rpta   = $this->Jugadores_model->buscarporID($id);
    

    //     $retorno = [];
    //     $retorno['TOTAL'] = count($rpta);
    //     $retorno['LISTA'] = $rpta;
    
    //     echo $this->lib_utils->vistaJson($retorno);
    // }
    
    public function buscarEstadiosall()
    {
        $id_estadio   = $this->input->post('id_estadio');
        $desde   = $this->input->post('desde');
        $hasta   = $this->input->post('hasta');
        $estado   = $this->input->post('estado');
        $rpta  = $this->Estadios_model->buscarEstadiosall($id_estadio,$desde,$hasta,$estado);
    

        $retorno = [];
        $retorno['TOTAL'] = count($rpta);
        $retorno['LISTA'] = $rpta;
    
        echo $this->lib_utils->vistaJson($retorno);
    }

}
