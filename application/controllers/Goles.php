<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Goles extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('goles/Goles_model');
        $this->load->library('Lib_utils');
    }
	public function goles()
{

        $this->load->view('goles/goles');
	}


    public function registrarnewFases()
    {
        $nombres_arbitro           = $this->input->post('nombres_arbitro');
        $id_jugador           = $this->input->post('id_jugador');
        $minuto           = $this->input->post('minuto');
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("id_partido" => $nombres_arbitro,
                    "id_jugador" => $id_jugador,
                    "minuto" => $minuto,
                    "created_at" => $tiempo,
                    "updated_at" => $tiempo,
                    "estado" => 1,
                );

        $rpta = $this->Goles_model->registrarnewFases($datos1);

        echo 'Se inserto correctamente el registro';

    }

    public function actualizandoFases()
    {
        $nombres_arbitro           = $this->input->post('nombres_arbitro');
        $id_jugador           = $this->input->post('id_jugador');
        $minuto           = $this->input->post('minuto');
        $id           = $this->input->post('id');
        $estado = $this->input->post('estado'); 
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("id_partido" => $nombres_arbitro,
        "id_jugador" => $id_jugador,
        "minuto" => $minuto,
                    "updated_at" => $tiempo,
                    "estado" => $estado,
                );
        $where          = "id_goles= $id";

        $rpta = $this->Goles_model->actualizandoFases($datos1,$where);

        echo 'Se edito correctamente el registro';

    }

    public function eliminar_fase()
    {
        $id           = $this->input->post('id');

        $datos1  = array(
                    "estado" => 0,
                );
        $where          = "id_goles= $id";

        $rpta = $this->Goles_model->eliminar_fase($datos1,$where);

        echo 'Se eliminando correctamente el registro';

    }


    // public function buscarporID()
    // {
    //     $id   = $this->input->post('id');
    //     $rpta   = $this->Jugadores_model->buscarporID($id);
    

    //     $retorno = [];
    //     $retorno['TOTAL'] = count($rpta);
    //     $retorno['LISTA'] = $rpta;
    
    //     echo $this->lib_utils->vistaJson($retorno);
    // }
    
    public function buscarGolesall()
    {
        $desde   = $this->input->post('desde');
        $hasta   = $this->input->post('hasta');
        $estado   = $this->input->post('estado');
        $rpta  = $this->Goles_model->buscarGolesall($desde,$hasta,$estado);
    

        $retorno = [];
        $retorno['TOTAL'] = count($rpta);
        $retorno['LISTA'] = $rpta;
    
        echo $this->lib_utils->vistaJson($retorno);
    }

}
