<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jugadores extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('jugadores/Jugadores_model');
        $this->load->library('Lib_utils');
    }
	public function jugadores()
{
		 //echo 'hola';

         $data['arbitros'] = $this->Jugadores_model->ObtenerArbitros();
        $this->load->view('jugadores/jugadores',$data);
	}


    public function registrarnewArbitro()
    {
        $nombres_arbitro           = $this->input->post('nombres_arbitro');
        $apellidos_arbitro               = $this->input->post('apellidos_arbitro');
        $link_arbitro = $this->input->post('link_arbitro'); 
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("nombres_arbitro" => $nombres_arbitro,
                    "apellidos_arbitro" => $apellidos_arbitro,
                    "created_at" => $tiempo,
                    "updated_at" => $tiempo,
                    "link_arbitrol" => $link_arbitro,
                    "estado" => 1,
                );

        $rpta = $this->Jugadores_model->registrarnewArbitro($datos1);

        echo 'Se inserto correctamente el registro';

    }

    public function actualizandoArbitros()
    {
        $nombres_arbitro           = $this->input->post('nombres_arbitro');
        $id           = $this->input->post('id');
        $apellidos_arbitro               = $this->input->post('apellidos_arbitro');
        $link_arbitro = $this->input->post('link_arbitro'); 
        $estado = $this->input->post('estado'); 
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("nombres_arbitro" => $nombres_arbitro,
                    "apellidos_arbitro" => $apellidos_arbitro,
                    "updated_at" => $tiempo,
                    "link_arbitrol" => $link_arbitro,
                    "estado" => 1,
                );
        $where          = "id_arbitros= $id";

        $rpta = $this->Jugadores_model->actualizandoArbitros($datos1,$where);

        echo 'Se edito correctamente el registro';

    }

    public function eliminarArbitro()
    {
        $id           = $this->input->post('id');

        $datos1  = array(
                    "estado" => 0,
                );
        $where          = "id_arbitros= $id";

        $rpta = $this->Jugadores_model->EliminarArbitro($datos1,$where);

        echo 'Se eliminando correctamente el registro';

    }


    // public function buscarporID()
    // {
    //     $id   = $this->input->post('id');
    //     $rpta   = $this->Jugadores_model->buscarporID($id);
    

    //     $retorno = [];
    //     $retorno['TOTAL'] = count($rpta);
    //     $retorno['LISTA'] = $rpta;
    
    //     echo $this->lib_utils->vistaJson($retorno);
    // }
    
    public function buscarArbitrosall()
    {
        $arbitro_id   = $this->input->post('arbitro_id');
        $desde   = $this->input->post('desde');
        $hasta   = $this->input->post('hasta');
        $estado   = $this->input->post('estado');
        $rpta  = $this->Jugadores_model->buscarArbitrosall($arbitro_id,$desde,$hasta,$estado);
    

        $retorno = [];
        $retorno['TOTAL'] = count($rpta);
        $retorno['LISTA'] = $rpta;
    
        echo $this->lib_utils->vistaJson($retorno);
    }

}
