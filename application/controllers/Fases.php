<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fases extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('fases/Fases_model');
        $this->load->library('Lib_utils');
    }
	public function fases()
{
		 //echo 'hola';

         $data['fases'] = $this->Fases_model->ObtenerFases();
        $this->load->view('fases/fases',$data);
	}


    public function registrarnewFases()
    {
        $nombres_arbitro           = $this->input->post('nombres_arbitro');
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("nombre_fase" => $nombres_arbitro,
                    "created_at" => $tiempo,
                    "updated_at" => $tiempo,
                    "estado" => 1,
                );

        $rpta = $this->Fases_model->registrarnewFases($datos1);

        echo 'Se inserto correctamente el registro';

    }

    public function actualizandoFases()
    {
        $nombres_arbitro           = $this->input->post('nombres_arbitro');
        $id           = $this->input->post('id');
        $estado = $this->input->post('estado'); 
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("nombre_fase" => $nombres_arbitro,
                    "updated_at" => $tiempo,
                    "estado" => $estado,
                );
        $where          = "id_fase= $id";

        $rpta = $this->Fases_model->actualizandoFases($datos1,$where);

        echo 'Se edito correctamente el registro';

    }

    public function eliminar_fase()
    {
        $id           = $this->input->post('id');

        $datos1  = array(
                    "estado" => 0,
                );
        $where          = "id_fase= $id";

        $rpta = $this->Fases_model->eliminar_fase($datos1,$where);

        echo 'Se eliminando correctamente el registro';

    }


    // public function buscarporID()
    // {
    //     $id   = $this->input->post('id');
    //     $rpta   = $this->Jugadores_model->buscarporID($id);
    

    //     $retorno = [];
    //     $retorno['TOTAL'] = count($rpta);
    //     $retorno['LISTA'] = $rpta;
    
    //     echo $this->lib_utils->vistaJson($retorno);
    // }
    
    public function buscarFasesall()
    {
        $id_fase   = $this->input->post('id_fase');
        $desde   = $this->input->post('desde');
        $hasta   = $this->input->post('hasta');
        $estado   = $this->input->post('estado');
        $rpta  = $this->Fases_model->buscarFasesall($id_fase,$desde,$hasta,$estado);
    

        $retorno = [];
        $retorno['TOTAL'] = count($rpta);
        $retorno['LISTA'] = $rpta;
    
        echo $this->lib_utils->vistaJson($retorno);
    }

}
