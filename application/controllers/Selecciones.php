<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Selecciones extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('selecciones/Selecciones_model');
        $this->load->library('Lib_utils');
    }
	public function selecciones()
{

        $this->load->view('selecciones/selecciones');
	}


    public function registrarnewFases()
    {
        $id_grupo           = $this->input->post('id_grupo');
        $nombre_seleccion           = $this->input->post('nombre_seleccion');
        $imagen_seleccion           = $this->input->post('imagen_seleccion');
        $id_tecnico           = $this->input->post('id_tecnico');
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("id_grupo" => $id_grupo,
                    "nombre_seleccion" => $nombre_seleccion,
                    "imagen_seleccion" => $imagen_seleccion,
                    "id_tecnico" => $id_tecnico,
                    "created_at" => $tiempo,
                    "updated_at" => $tiempo,
                    "estado" => 1,
                );

        $rpta = $this->Selecciones_model->registrarnewFases($datos1);

        echo 'Se inserto correctamente el registro';

    }

    public function actualizandoFases()
    {
        $id_grupo           = $this->input->post('id_grupo');
        $nombre_seleccion           = $this->input->post('nombre_seleccion');
        $imagen_seleccion           = $this->input->post('imagen_seleccion');
        $id_tecnico           = $this->input->post('id_tecnico');
        $id           = $this->input->post('id');
        $estado = $this->input->post('estado'); 
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("id_grupo" => $id_grupo,
                    "nombre_seleccion" => $nombre_seleccion,
                    "imagen_seleccion" => $imagen_seleccion,
                    "id_tecnico" => $id_tecnico,
                    "created_at" => $tiempo,
                    "updated_at" => $tiempo,
                    "estado" => $estado,
                );
        $where          = "id_seleccion= $id";

        $rpta = $this->Selecciones_model->actualizandoFases($datos1,$where);

        echo 'Se edito correctamente el registro';

    }

    public function eliminar_fase()
    {
        $id           = $this->input->post('id');

        $datos1  = array(
                    "estado" => 0,
                );
        $where          = "id_seleccion= $id";

        $rpta = $this->Selecciones_model->eliminar_fase($datos1,$where);

        echo 'Se eliminando correctamente el registro';

    }


    // public function buscarporID()
    // {
    //     $id   = $this->input->post('id');
    //     $rpta   = $this->Jugadores_model->buscarporID($id);
    

    //     $retorno = [];
    //     $retorno['TOTAL'] = count($rpta);
    //     $retorno['LISTA'] = $rpta;
    
    //     echo $this->lib_utils->vistaJson($retorno);
    // }
    
    public function buscarGolesall()
    {
        $desde   = $this->input->post('desde');
        $hasta   = $this->input->post('hasta');
        $estado   = $this->input->post('estado');
        $rpta  = $this->Selecciones_model->buscarGolesall($desde,$hasta,$estado);
    

        $retorno = [];
        $retorno['TOTAL'] = count($rpta);
        $retorno['LISTA'] = $rpta;
    
        echo $this->lib_utils->vistaJson($retorno);
    }

}
