<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partidos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('partidos/Partidos_model');
        $this->load->library('Lib_utils');
    }
	public function partidos()
{

        $this->load->view('partidos/partidos');
	}

    public function registrarnewFases()
    {
        $id_estadio           = $this->input->post('id_estadio');
        $id_seleccion           = $this->input->post('id_seleccion');
        $id_arbitro           = $this->input->post('id_arbitro');
        $id_fase           = $this->input->post('id_fase');
        $fecha_partido           = $this->input->post('fecha_partido');
        $hora_partido           = $this->input->post('hora_partido');
        $resultado           = $this->input->post('resultado');
        $id_seleccion2           = $this->input->post('id_seleccion2');
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("id_estadio" => $id_estadio,
                    "id_seleccion1" => $id_seleccion,
                    "id_arbitro" => $id_arbitro,
                    "id_fase" => $id_fase,
                    "fecha_partido" => $fecha_partido,
                    "hora_partido" => $hora_partido,
                    "created_at" => $tiempo,
                    "id_seleccion2" => $id_seleccion2,
                    "resultado" => $resultado,
                    "updated_at" => $tiempo,
                    "estado" => 1,
                );

        $rpta = $this->Partidos_model->registrarnewFases($datos1);

        echo 'Se inserto correctamente el registro';

    }

    public function actualizandoFases()
    {
        $id_estadio           = $this->input->post('id_estadio');
        $id_seleccion           = $this->input->post('id_seleccion');
        $id_arbitro           = $this->input->post('id_arbitro');
        $id_fase           = $this->input->post('id_fase');
        $fecha_partido           = $this->input->post('fecha_partido');
        $hora_partido           = $this->input->post('hora_partido');
        $resultado           = $this->input->post('resultado');
        $id_seleccion2           = $this->input->post('id_seleccion2');
        $estado           = $this->input->post('estado');
        $id           = $this->input->post('id');
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("id_estadio" => $id_estadio,
                    "id_seleccion1" => $id_seleccion,
                    "id_arbitro" => $id_arbitro,
                    "id_fase" => $id_fase,
                    "fecha_partido" => $fecha_partido,
                    "hora_partido" => $hora_partido,
                    "id_seleccion2" => $id_seleccion2,
                    "resultado" => $resultado,
                    "updated_at" => $tiempo,
                    "estado" => $estado,
                );
        $where          = "id_partido= $id";

        $rpta = $this->Partidos_model->actualizandoFases($datos1,$where);

        echo 'Se edito correctamente el registro';

    }

    public function eliminar_fase()
    {
        $id           = $this->input->post('id');

        $datos1  = array(
                    "estado" => 0,
                );
        $where          = "id_partido= $id";

        $rpta = $this->Partidos_model->eliminar_fase($datos1,$where);

        echo 'Se eliminando correctamente el registro';

    }


    // public function buscarporID()
    // {
    //     $id   = $this->input->post('id');
    //     $rpta   = $this->Jugadores_model->buscarporID($id);
    

    //     $retorno = [];
    //     $retorno['TOTAL'] = count($rpta);
    //     $retorno['LISTA'] = $rpta;
    
    //     echo $this->lib_utils->vistaJson($retorno);
    // }
    
    public function buscarPartidosall()
    {
        $desde   = $this->input->post('desde');
        $hasta   = $this->input->post('hasta');
        $estado   = $this->input->post('estado');
        $rpta  = $this->Partidos_model->buscarGolesall($desde,$hasta,$estado);
    

        $retorno = [];
        $retorno['TOTAL'] = count($rpta);
        $retorno['LISTA'] = $rpta;
    
        echo $this->lib_utils->vistaJson($retorno);
    }

}
