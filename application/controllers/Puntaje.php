<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Puntaje extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('puntaje/Puntaje_model');
        $this->load->library('Lib_utils');
    }
	public function puntaje()
{

        $this->load->view('puntaje/puntaje');
	}

    public function registrarnewFases()
    {
        $id_partido           = $this->input->post('id_partido');
        $id_grupo           = $this->input->post('id_grupo');
        $id_seleccion           = $this->input->post('id_seleccion');
        $puntos           = $this->input->post('puntos');
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("id_partido" => $id_partido,
        "id_grupo" => $id_grupo,
        "id_seleccion" => $id_seleccion,
        "puntos" => $puntos,
                    "created_at" => $tiempo,
                    "updated_at" => $tiempo,
                    "estado" => 1,
                );

        $rpta = $this->Puntaje_model->registrarnewFases($datos1);

        echo 'Se inserto correctamente el registro';

    }

    public function actualizandoFases()
    {
        $id_partido           = $this->input->post('id_partido');
        $id_grupo           = $this->input->post('id_grupo');
        $id_seleccion           = $this->input->post('id_seleccion');
        $puntos           = $this->input->post('puntos');
        $estado           = $this->input->post('estado');
        $id           = $this->input->post('id');
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("id_partido" => $id_partido,
        "id_grupo" => $id_grupo,
        "id_seleccion" => $id_seleccion,
                    "updated_at" => $tiempo,
                    "estado" => $estado,
                );
        $where          = "id_puntaje= $id";

        $rpta = $this->Puntaje_model->actualizandoFases($datos1,$where);

        echo 'Se edito correctamente el registro';

    }

    public function eliminar_fase()
    {
        $id           = $this->input->post('id');

        $datos1  = array(
                    "estado" => 0,
                );
        $where          = "id_puntaje= $id";

        $rpta = $this->Puntaje_model->eliminar_fase($datos1,$where);

        echo 'Se eliminando correctamente el registro';

    }


    // public function buscarporID()
    // {
    //     $id   = $this->input->post('id');
    //     $rpta   = $this->Jugadores_model->buscarporID($id);
    

    //     $retorno = [];
    //     $retorno['TOTAL'] = count($rpta);
    //     $retorno['LISTA'] = $rpta;
    
    //     echo $this->lib_utils->vistaJson($retorno);
    // }
    
    public function buscarPartidosall()
    {
        $desde   = $this->input->post('desde');
        $hasta   = $this->input->post('hasta');
        $estado   = $this->input->post('estado');
        $rpta  = $this->Puntaje_model->buscarGolesall($desde,$hasta,$estado);
    

        $retorno = [];
        $retorno['TOTAL'] = count($rpta);
        $retorno['LISTA'] = $rpta;
    
        echo $this->lib_utils->vistaJson($retorno);
    }

}
