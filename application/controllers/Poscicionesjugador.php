<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Poscicionesjugador extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('poscicionesjugador/Poscicionesjugador_model');
        $this->load->library('Lib_utils');
    }
	public function poscicionesjugador()
{

        $this->load->view('poscicionesjugador/poscicionesjugador');
	}

    public function registrarnewFases()
    {
        $posicion           = $this->input->post('posicion');
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("nombre_posicion" => $posicion,
                    "created_at" => $tiempo,
                    "updated_at" => $tiempo,
                    "estado" => 1,
                );

        $rpta = $this->Poscicionesjugador_model->registrarnewFases($datos1);

        echo 'Se inserto correctamente el registro';

    }

    public function actualizandoFases()
    {
        $posicion           = $this->input->post('posicion');
        $estado           = $this->input->post('estado');
        $id           = $this->input->post('id');
        $tiempo = date('Y-m-d H:i:s');

        $tiempo = strtotime ( '-18000 second' , strtotime ( $tiempo ) ) ;
        $tiempo = date ( 'Y-m-d H:i:s' , $tiempo );

        $datos1  = array("nombre_posicion" => $posicion,
                    "updated_at" => $tiempo,
                    "estado" => $estado,
                );
        $where          = "id_posicion= $id";

        $rpta = $this->Poscicionesjugador_model->actualizandoFases($datos1,$where);

        echo 'Se edito correctamente el registro';

    }

    public function eliminar_fase()
    {
        $id           = $this->input->post('id');

        $datos1  = array(
                    "estado" => 0,
                );
        $where          = "id_posicion= $id";

        $rpta = $this->Poscicionesjugador_model->eliminar_fase($datos1,$where);

        echo 'Se eliminando correctamente el registro';

    }


    // public function buscarporID()
    // {
    //     $id   = $this->input->post('id');
    //     $rpta   = $this->Jugadores_model->buscarporID($id);
    

    //     $retorno = [];
    //     $retorno['TOTAL'] = count($rpta);
    //     $retorno['LISTA'] = $rpta;
    
    //     echo $this->lib_utils->vistaJson($retorno);
    // }
    
    public function buscarPartidosall()
    {
        $desde   = $this->input->post('desde');
        $hasta   = $this->input->post('hasta');
        $estado   = $this->input->post('estado');
        $rpta  = $this->Poscicionesjugador_model->buscarGolesall($desde,$hasta,$estado);
    

        $retorno = [];
        $retorno['TOTAL'] = count($rpta);
        $retorno['LISTA'] = $rpta;
    
        echo $this->lib_utils->vistaJson($retorno);
    }

}
