<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Lib_utils {
	public function vistaJson($data)
	    {
	        return json_encode($data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
	    }

	    public function pre($data)
	    {
	        echo "<pre>";
	        print_r($data);
	        echo "</pre>";
	    }

	    public function manejasDias($fecha, $dias)
	    {
	    	return date("Y-m-d",strtotime($fecha.$dias)); 
	    	
	    }

	    public function validateDate($fecha)
	    {
	    
	        $f = explode('-', $fecha);
		    $anio   = $f[0];
		    $mes    = $f[1];
		    $dia    = $f[2];

		    return $dia.'/'.$mes.'/'.$anio;
	    }

}

?>